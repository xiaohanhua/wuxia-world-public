�]q (cskill
skill
q)�q}q(X   nameqX   Shaolin Inner Energy TechniqueqX   levelqK
X   descriptionqX_  Shaolin Inner Energy Technique (level 10)
----------------------------------------

Level 1: in battle, increases defence by 2.Level 2: Increases stamina upper limit by 20. In battle, increases defence by 4.
Level 3: Increases stamina upper limit by 30. In battle, increases defence by 6.
Level 4: Increases stamina upper limit by 40. In battle, increases defence by 8.
Level 5: Increases stamina upper limit by 50. In battle, increases defence by 10.
Level 6: Increases stamina upper limit by 60. In battle, increases defence by 12.
Level 7: Increases stamina upper limit by 70. In battle, increases defence by 14.
Level 8: Increases stamina upper limit by 80. In battle, increases defence by 16.
Level 9: Increases stamina upper limit by 90. In battle, increases defence by 18.
Level 10: Increases stamina upper limit by 100. In battle, increases defence by 20.qX	   file_nameq	X'   shaolin_inner_energy_technique_icon.ppmq
ubh)�q}q(hX   Shaolin Mind Clearing MethodqhKhXY  Shaolin Mind Clearing Method (level 1)
----------------------------------------

Level 1: in battle, reduces chance of gaining negative status effects by 2%.
Level 2: in battle, reduces chance of gaining negative status effects by 4%.
Level 3: in battle, reduces chance of gaining negative status effects by 6%.
Level 4: in battle, reduces chance of gaining negative status effects by 8%.
Level 5: in battle, reduces chance of gaining negative status effects by 10%.
Level 6: in battle, reduces chance of gaining negative status effects by 12%.
Level 7: in battle, reduces chance of gaining negative status effects by 14%.
Level 8: in battle, reduces chance of gaining negative status effects by 16%.
Level 9: in battle, reduces chance of gaining negative status effects by 18%.
Level 10: in battle, reduces chance of gaining negative status effects by 20%.qh	X    Shaolin_Mind_Clearing_Method.pngqubh)�q}q(hX   Spreading WingsqhK
hX�  Spreading Wings (level 10)
----------------------------------------

Level 1: in battle, increases speed by 2 and attack by 1.
Level 2: in battle, increases speed by 4 and attack by 2.
Level 3: in battle, increases speed by 6 and attack by 3.
Level 4: in battle, increases speed by 8 and attack by 4.
Level 5: in battle, increases speed by 10 and attack by 5.
Level 6: in battle, increases speed by 12 and attack by 6.
Level 7: in battle, increases speed by 14 and attack by 7.
Level 8: in battle, increases speed by 16 and attack by 8.
Level 9: in battle, increases speed by 18 and attack by 9.
Level 10: in battle, increases speed by 20 and attack by 10.qh	X   spreading_wings_icon.ppmqubh)�q}q(hX	   Toad FormqhK
hX�  Toad Form (level 10)
----------------------------------------

Level 1: in battle, opponent takes +3% damage from poison (applied to base poison damage).
Level 2: in battle, opponent takes +6% damage from poison (applied to base poison damage).
Level 3: in battle, opponent takes +9% damage from poison (applied to base poison damage).
Level 4: in battle, opponent takes +12% damage from poison (applied to base poison damage).
Level 5: in battle, opponent takes +15% damage from poison (applied to base poison damage).
Level 6: in battle, opponent takes +18% damage from poison (applied to base poison damage).
Level 7: in battle, opponent takes +21% damage from poison (applied to base poison damage).
Level 8: in battle, opponent takes +24% damage from poison (applied to base poison damage).
Level 9: in battle, opponent takes +27% damage from poison (applied to base poison damage).
Level 10: in battle, opponent takes +30% damage from poison (applied to base poison damage).qh	X   toad_form_icon.pngqubh)�q}q(hX   Splitting Mountains and EarthqhK
hX�  Splitting Mountains and Earth (level 10)
----------------------------------------

Level 1: in battle, increases strength by 3 and increases chance of critical hit by 3%.
Level 2: in battle, increases strength by 6 and increases chance of critical hit by 6%.
Level 3: in battle, increases strength by 9 and increases chance of critical hit by 9%.
Level 4: in battle, increases strength by 12 and increases chance of critical hit by 12%.
Level 5: in battle, increases strength by 15 and increases chance of critical hit by 15%.
Level 6: in battle, increases strength by 18 and increases chance of critical hit by 18%.
Level 7: in battle, increases strength by 21 and increases chance of critical hit by 21%.
Level 8: in battle, increases strength by 24 and increases chance of critical hit by 24%.
Level 9: in battle, increases strength by 27 and increases chance of critical hit by 27%.
Level 10: in battle, increases strength by 30 and increases chance of critical hit by 30%.qh	X&   splitting_mountains_and_earth_icon.ppmqubh)�q}q (hX
   Ninjutsu Iq!hK
hX�  Ninjutsu I (level 10)
----------------------------------------
Art of the Ninja

Level 1: in battle, increases attack, speed, and dexterity by 1.
Level 2: in battle, increases attack, speed, and dexterity by 2.
Level 3: in battle, increases attack, speed, and dexterity by 3.
Level 4: in battle, increases attack, speed, and dexterity by 4.
Level 5: in battle, increases attack, speed, and dexterity by 5.
Level 6: in battle, increases attack, speed, and dexterity by 6.
Level 7: in battle, increases attack, speed, and dexterity by 7.
Level 8: in battle, increases attack, speed, and dexterity by 8.
Level 9: in battle, increases attack, speed, and dexterity by 9.
Level 10: in battle, increases attack, speed, and dexterity by 10.q"h	X   ninjutsu_i_icon.pngq#ubh)�q$}q%(hX   Ninjutsu IIq&hK
hX�  Ninjutsu II (level 10)
----------------------------------------
Art of the Ninja

Level 1: in battle, increases attack, speed, and dexterity by 3.
Level 2: in battle, increases attack, speed, and dexterity by 6.
Level 3: in battle, increases attack, speed, and dexterity by 9.
Level 4: in battle, increases attack, speed, and dexterity by 12.
Level 5: in battle, increases attack, speed, and dexterity by 15.
Level 6: in battle, increases attack, speed, and dexterity by 18.
Level 7: in battle, increases attack, speed, and dexterity by 21.
Level 8: in battle, increases attack, speed, and dexterity by 24.
Level 9: in battle, increases attack, speed, and dexterity by 27.
Level 10: in battle, increases attack, speed, and dexterity by 30.q'h	X   ninjutsu_ii_icon.pngq(ubh)�q)}q*(hX   Ninjutsu IIIq+hK
hX�  Ninjutsu III (level 10)
----------------------------------------
Art of the Ninja

Increases health and stamina upper limit by 100 for each upgrade.

Level 1: in battle, increases chance of critical hit by 5%.
Level 2: in battle, increases chance of critical hit by 10%.
Level 3: in battle, increases chance of critical hit by 15%.
Level 4: in battle, increases chance of critical hit by 20%.
Level 5: in battle, increases chance of critical hit by 25%.
Level 6: in battle, increases chance of critical hit by 30%.
Level 7: in battle, increases chance of critical hit by 35%.
Level 8: in battle, increases chance of critical hit by 40%.
Level 9: in battle, increases chance of critical hit by 45%.
Level 10: in battle, increases chance of critical hit by 50%.q,h	X   ninjutsu_iii_icon.pngq-ube.