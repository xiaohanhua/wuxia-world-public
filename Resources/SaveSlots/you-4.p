�ccharacter
character
q )�q}q(X   nameqX   WandererqX   levelqKX   itemListq]qX   equipmentListq]q	(citem
Equipment
q
)�q}q(X   attributes_dicq}q(X   Golden Glovesq]q(X
   hands_leftqK K
K K
K K G?�      X   SealqG?�������eX   Bronze Gauntletsq]q(hK KK KK K G?�������NK eX   Venomous Viper Glovesq]q(hK KK KK K G?�������X   PoisonqG?�      eX   Golden Chainmailq]q(X   bodyqK K K K2K K K NK eX   Steel Platelegsq]q(X   legsqK K J����KK K K NK eX   Steel Platebodyq]q(hK K J����K2K K K NK eX   Hardened Steel Platebodyq ]q!(hK K J����KFK K K NK eX   Light Armorq"]q#(hK K J����KK K K NK eX
   Ninja Suitq$]q%(hK K K K KK K NK eX   Leather Bodyq&]q'(hK K K K
K K K NK eX   Leather Pantsq(]q)(hK K K KK K K NK eX   Linen Shirtq*]q+(hK K K KK K K NK eX   Heavy Adamantite Swordq,]q-(X   hands_rightq.KK2J����KK G?�      K NK eX   Black Salamanderq/]q0(h.KKK
K
K G?ə�����K X   Critical Hitq1G?�������eX   Golden Snakeq2]q3(h.KK K KK K K NK eX   Moonlight Bladeq4]q5(h.K
KKKK K K X   Healthq6G?��Q��eX   Phoenix Bladeq7]q8(h.K
KKK K K K NK eX   Iron Daggerq9]q:(h.KKKK K K K NK eX
   Iron Swordq;]q<(h.KKKK K K K NK eX   Steel Longswordq=]q>(h.K
K
K K K K K NK eX   Seven-inch Serpent Staffq?]q@(h.K K K K K K K hG?�      eX   Wings of the DragonflyqA]qB(h.K
K
K(K
K(G?�������K h1G?�������eX   Splinter StaffqC]qD(h.KKKK(KK K NK eX   Lightning SaberqE]qF(h.K
K
KK K G?�������K X   BlindqGG?�      eX   Flame BladeqH]qI(h.K
KK K K G?�������K X   Internal InjuryqJG?�333333eX   Thunder BladeqK]qL(h.K
KK K K G?�      K hJG?�333333eX	   Ice ShardqM]qN(h.K K KK K G?�������K X   DisableqOG?�      eX   Granite BootsqP]qQ(X   feetqRK KJ����KJ����K G?�      h1G?�      eX   Babao BladeqS]qT(h.KKKKKG?陙����K h1G?陙����eX
   Wool BootsqU]qV(hRK K KKK K K h6G?��Q��eX   Leather BootsqW]qX(hRK KKKKK K NK eX   Explorer BootsqY]qZ(hRKKKKKK K X   Staminaq[G?�������eX   Explorer Ringq\]q](hK K K K K K K h1G?�333333eX   Silk Fanq^]q_(hK K K KK K K h[G?��Q��eX   Buddha Beadsq`]qa(X   headqbK K K K K K K X   StatusqcG?�������eX   Blood Diamond Pendantqd]qe(hbK K K K K K K h6G?��Q��eX   Amethyst Necklaceqf]qg(hbKK K K K K K h1G?�������eX   Maskqh]qi(hbK K K KK K K NK euX   selling_price_dicqj}qk(hM�hMxhM�hM�
hM�h M�h"M�h,M�*h/MLh2M�h7Mh4M�h9Kdh;M�h=M�h?M�:hAM�:hCM�:hEM�:hHM�:hKM�:hMM�:h$M�hPM�:hSJ8[ hUM�hWM�h&M�h(M�h*Kh^K�h`M,hdMphfMLhhK hM'h\K hYK uX   description_dicql}qm(hX<   A light-weight chain mail formed from steel and gold mesh!

qnh"XZ   A light platebody made from a thin layer of tin and bronze that offers basic protection.

qoh,Xl   Forged from adamantite rock, this large blade may be
difficult to wield but can crush anything in its way.

qph/X-   A thin, sharp blade forged from black iron.

qqh2Xi   A curved sword that gives the wielder a distinct advantage
in both attack and defence due to its shape.

qrh7Xb   Forged from high-quality metals, this blade's light weight allows its wielder to attack quickly.

qsh9X)   Slightly better than being bare-handed.

qth;X   A basic sword made of iron.

quh=X   A longsword made from steel.

qvh?Xp   A highly toxic staff crafted in the shape of a serpant that, despite its short length, can deal a lethal blow.

qwhAXg   A blade that is both light and easy to handle, allowing its user to perform rapid, consecutive strikes.qxhCXx   A staff with many sharp arms protruding from its body..

In battle, reflects 25% of damage taken back to the opponent.

qyhEX   Quick as lightning.

qzhHX^   A legendary blade that burns to the touch and can partially melt normal metals upon contact.

q{hKX]   As thunder is to the ears, so is this blade to the heart of anyone who opposes its wielder.

q|hMXP   A sharp dagger that pierces the bone as easily as a knife goes through butter.

q}h4X+   A swordwith a glow like that of the moon.

q~hX!   Great protection for the hands!

qhX'   Gives some extra power to your punch.

q�hX6   Hard leather gloves layered with deadly viper venom.

q�h$X_   It's much harder to be detected while wearing this.
Useful for sneaking into places at night.

q�hYXH   A pair of boots from Scrub as a gift for beta-testing. Beta-only item!

q�hPX   Boots coated with a thick layer of granite, making it more difficult to move quickly but multiplies the force of each attack.

q�hSX+   A powerful blade imbued with 8 rare gems.

q�hUX   Keeps my feet warm!

q�hWX'   Crafted from top-quality cow leather.

q�h&h�h(h�h*X   A plain, linen shirt.

q�hX    Great protection for the legs!

q�hX>   A bit heavy, but shields your organs from devastating blows.

q�h X,   Solid platebody made of high carbon steel.

q�h\X?   A ring from Scrub as a gift for beta-testing. Beta-only item!

q�h^X,   A delicate fan crafted from mulberry silk.

q�h`X-   A set of beads made from high-quality opal.

q�hdX:   A valuable pendant crafted using the rare blood diamond.

q�hfXF   A rare, enchanted necklace and one of three treasures of Eagle Sect.

q�hhXO   A creepy-looking mask. Maybe I can scare my opponents to death by wearing it.

q�uhX   Explorer Bootsq�X   name_keyq�h�X   gradeq�X   Standardq�X   slotq�hRX   datkq�KX   dstrq�KX   dspdq�KX   ddefq�KX   ddexq�KX   armed_dmg_bonusq�K X   unarmed_dmg_bonusq�K X   effectq�h[X   effect_probq�G?�������X   descriptionq�X  Explorer Boots
-------------------------------------
A pair of boots from Scrub as a gift for beta-testing. Beta-only item!

Gives the following bonuses in battle:
Attack + 5
Strength + 5
Speed + 5
Defence + 5
Dexterity + 5
        
Recovers an additional 5.0% Stamina when resting.
q�X   selling_priceq�K X   attribute_multiplierq�KX   price_multiplierq�Kubh
)�q�}q�(h}q�(h]q�(hK K
K K
K K G?�      hG?�������eh]q�(hK KK KK K G?�������NK eh]q�(hK KK KK K G?�������hG?�      eh]q�(hK K K K2K K K NK eh]q�(hK K J����KK K K NK eh]q�(hK K J����K2K K K NK eh ]q�(hK K J����KFK K K NK eh"]q�(hK K J����KK K K NK eh$]q�(hK K K K KK K NK eh&]q�(hK K K K
K K K NK eh(]q�(hK K K KK K K NK eh*]q�(hK K K KK K K NK eh,]q�(h.KK2J����KK G?�      K NK eh/]q�(h.KKK
K
K G?ə�����K h1G?�������eh2]q�(h.KK K KK K K NK eh4]q�(h.K
KKKK K K h6G?��Q��eh7]q�(h.K
KKK K K K NK eh9]q�(h.KKKK K K K NK eh;]q�(h.KKKK K K K NK eh=]q�(h.K
K
K K K K K NK eh?]q�(h.K K K K K K K hG?�      ehA]q�(h.K
K
K(K
K(G?�������K h1G?�������ehC]q�(h.KKKK(KK K NK ehE]q�(h.K
K
KK K G?�������K hGG?�      ehH]q�(h.K
KK K K G?�������K hJG?�333333ehK]q�(h.K
KK K K G?�      K hJG?�333333ehM]q�(h.K K KK K G?�������K hOG?�      ehP]q�(hRK KJ����KJ����K G?�      h1G?�      ehS]q�(h.KKKKKG?陙����K h1G?陙����ehU]q�(hRK K KKK K K h6G?��Q��ehW]q�(hRK KKKKK K NK ehY]q�(hRKKKKKK K h[G?�������eh\]q�(hK K K K K K K h1G?�333333eh^]q�(hK K K KK K K h[G?��Q��eh`]q�(hbK K K K K K K hcG?�������ehd]q�(hbK K K K K K K h6G?��Q��ehf]q�(hbKK K K K K K h1G?�������ehh]q�(hbK K K KK K K NK euhj}q�(hM�hMxhM�hM�
hM�h M�h"M�h,M�*h/MLh2M�h7Mh4M�h9Kdh;M�h=M�h?M�:hAM�:hCM�:hEM�:hHM�:hKM�:hMM�:h$M�hPM�:hSJ8[ hUM�hWM�h&M�h(M�h*Kh^K�h`M,hdMphfMLhhK hM'h\K hYK uhl}q�(hhnh"hoh,hph/hqh2hrh7hsh9hth;huh=hvh?hwhAhxhChyhEhzhHh{hKh|hMh}h4h~hhhh�hh�h$h�hYh�hPh�hSh�hUh�hWh�h&h�h(h�h*h�hh�hh�h h�h\h�h^h�h`h�hdh�hfh�hhh�uhX   Golden Glovesq�h�h�h�h�h�hh�K h�K
h�K h�K
h�K h�K h�G?�      h�hh�G?�������h�XW  Golden Gloves
-------------------------------------
Great protection for the hands!

Gives the following bonuses in battle:
Attack + 0
Strength + 10
Speed + 0
Defence + 10
Dexterity + 0
        
+25.0% damage bonus for unarmed attacks.
            
+10.0% chance of inflicting Seal to opponent on hit (Unarmed Attacks).



Selling price: 3300
q�h�M�h�Kh�Kubh
)�q�}q�(h}q�(h]q�(hK K
K K
K K G?�      hG?�������eh]q�(hK KK KK K G?�������NK eh]q�(hK KK KK K G?�������hG?�      eh]q�(hK K K K2K K K NK eh]q�(hK K J����KK K K NK eh]q�(hK K J����K2K K K NK eh ]q�(hK K J����KFK K K NK eh"]q�(hK K J����KK K K NK eh$]q�(hK K K K KK K NK eh&]q�(hK K K K
K K K NK eh(]q�(hK K K KK K K NK eh*]q�(hK K K KK K K NK eh,]q�(h.KK2J����KK G?�      K NK eh/]q�(h.KKK
K
K G?ə�����K h1G?�������eh2]q�(h.KK K KK K K NK eh4]q�(h.K
KKKK K K h6G?��Q��eh7]q�(h.K
KKK K K K NK eh9]q�(h.KKKK K K K NK eh;]q�(h.KKKK K K K NK eh=]q�(h.K
K
K K K K K NK eh?]q�(h.K K K K K K K hG?�      ehA]q�(h.K
K
K(K
K(G?�������K h1G?�������ehC]q�(h.KKKK(KK K NK ehE]q�(h.K
K
KK K G?�������K hGG?�      ehH]q�(h.K
KK K K G?�������K hJG?�333333ehK]q�(h.K
KK K K G?�      K hJG?�333333ehM]q�(h.K K KK K G?�������K hOG?�      ehP]q�(hRK KJ����KJ����K G?�      h1G?�      ehS]q�(h.KKKKKG?陙����K h1G?陙����ehU]q�(hRK K KKK K K h6G?��Q��ehW]q�(hRK KKKKK K NK ehY]q�(hRKKKKKK K h[G?�������eh\]q�(hK K K K K K K h1G?�333333eh^]q�(hK K K KK K K h[G?��Q��eh`]q�(hbK K K K K K K hcG?�������ehd]q�(hbK K K K K K K h6G?��Q��ehf]q�(hbKK K K K K K h1G?�������ehh]q�(hbK K K KK K K NK euhj}q�(hM�hMxhM�hM�
hM�h M�h"M�h,M�*h/MLh2M�h7Mh4M�h9Kdh;M�h=M�h?M�:hAM�:hCM�:hEM�:hHM�:hKM�:hMM�:h$M�hPM�:hSJ8[ hUM�hWM�h&M�h(M�h*Kh^K�h`M,hdMphfMLhhK hM'h\K hYK uhl}q�(hhnh"hoh,hph/hqh2hrh7hsh9hth;huh=hvh?hwhAhxhChyhEhzhHh{hKh|hMh}h4h~hhhh�hh�h$h�hYh�hPh�hSh�hUh�hWh�h&h�h(h�h*h�hh�hh�h h�h\h�h^h�h`h�hdh�hfh�hhh�uhX   Phoenix Blade [Ultra Rare]q�h�X   Phoenix Bladeq�h�X
   Ultra Rareq�h�h.h�Kh�Kh�K(h�K h�K h�K h�K h�Nh�K h�X)  Phoenix Blade [Ultra Rare]
-------------------------------------
Forged from high-quality metals, this blade's light weight allows its wielder to attack quickly.

Gives the following bonuses in battle:
Attack + 20
Strength + 30
Speed + 40
Defence + 0
Dexterity + 0
        


Selling price: 10800
r   h�M0*h�Kh�Kubh
)�r  }r  (h}r  (h]r  (hK K
K K
K K G?�      hG?�������eh]r  (hK KK KK K G?�������NK eh]r  (hK KK KK K G?�������hG?�      eh]r  (hK K K K2K K K NK eh]r  (hK K J����KK K K NK eh]r	  (hK K J����K2K K K NK eh ]r
  (hK K J����KFK K K NK eh"]r  (hK K J����KK K K NK eh$]r  (hK K K K KK K NK eh&]r  (hK K K K
K K K NK eh(]r  (hK K K KK K K NK eh*]r  (hK K K KK K K NK eh,]r  (h.KK2J����KK G?�      K NK eh/]r  (h.KKK
K
K G?ə�����K h1G?�������eh2]r  (h.KK K KK K K NK eh4]r  (h.K
KKKK K K h6G?��Q��eh7]r  (h.K
KKK K K K NK eh9]r  (h.KKKK K K K NK eh;]r  (h.KKKK K K K NK eh=]r  (h.K
K
K K K K K NK eh?]r  (h.K K K K K K K hG?�      ehA]r  (h.K
K
K(K
K(G?�������K h1G?�������ehC]r  (h.KKKK(KK K NK ehE]r  (h.K
K
KK K G?�������K hGG?�      ehH]r  (h.K
KK K K G?�������K hJG?�333333ehK]r  (h.K
KK K K G?�      K hJG?�333333ehM]r  (h.K K KK K G?�������K hOG?�      ehP]r  (hRK KJ����KJ����K G?�      h1G?�      ehS]r   (h.KKKKKG?陙����K h1G?陙����ehU]r!  (hRK K KKK K K h6G?��Q��ehW]r"  (hRK KKKKK K NK ehY]r#  (hRKKKKKK K h[G?�������eh\]r$  (hK K K K K K K h1G?�333333eh^]r%  (hK K K KK K K h[G?��Q��eh`]r&  (hbK K K K K K K hcG?�������ehd]r'  (hbK K K K K K K h6G?��Q��ehf]r(  (hbKK K K K K K h1G?�������ehh]r)  (hbK K K KK K K NK euhj}r*  (hM�hMxhM�hM�
hM�h M�h"M�h,M�*h/MLh2M�h7Mh4M�h9Kdh;M�h=M�h?M�:hAM�:hCM�:hEM�:hHM�:hKM�:hMM�:h$M�hPM�:hSJ8[ hUM�hWM�h&M�h(M�h*Kh^K�h`M,hdMphfMLhhK hM'h\K hYK uhl}r+  (hhnh"hoh,hph/hqh2hrh7hsh9hth;huh=hvh?hwhAhxhChyhEhzhHh{hKh|hMh}h4h~hhhh�hh�h$h�hYh�hPh�hSh�hUh�hWh�h&h�h(h�h*h�hh�hh�h h�h\h�h^h�h`h�hdh�hfh�hhh�uhX   Blood Diamond Pendantr,  h�j,  h�h�h�hbh�K h�K h�K h�K h�K h�K h�K h�h6h�G?��Q��h�X*  Blood Diamond Pendant
-------------------------------------
A valuable pendant crafted using the rare blood diamond.

Gives the following bonuses in battle:
Attack + 0
Strength + 0
Speed + 0
Defence + 0
Dexterity + 0
        
Recovers an additional 6.0% Health when resting.



Selling price: 6000
r-  h�Mph�Kh�Kubh
)�r.  }r/  (h}r0  (X   Golden Glovesr1  ]r2  (X
   hands_leftr3  K K
K K
K K G?�      X   Sealr4  G?�������eX   Bronze Gauntletsr5  ]r6  (j3  K KK KK K G?�������NK eX   Venomous Viper Glovesr7  ]r8  (j3  K KK KK K G?�������X   Poisonr9  G?�      eX   Golden Chainmailr:  ]r;  (X   bodyr<  K K K K2K K K NK eX   Steel Platelegsr=  ]r>  (X   legsr?  K K J����KK K K NK eX   Steel Platebodyr@  ]rA  (j<  K K J����K2K K K NK eX   Hardened Steel PlatebodyrB  ]rC  (j<  K K J����KFK K K NK eX   Light ArmorrD  ]rE  (j<  K K J����KK K K NK eX
   Ninja SuitrF  ]rG  (j<  K K K K KK K NK eX   Leather BodyrH  ]rI  (j<  K K K K
K K K NK eX   Leather PantsrJ  ]rK  (j?  K K K KK K K NK eX   Linen ShirtrL  ]rM  (j<  K K K KK K K NK eX   Heavy Adamantite SwordrN  ]rO  (X   hands_rightrP  KK2J����KK G?�      K NK eX   Black SalamanderrQ  ]rR  (jP  KKK
K
K G?ə�����K X   Critical HitrS  G?�������eX   Golden SnakerT  ]rU  (jP  KK K KK K K NK eX   Moonlight BladerV  ]rW  (jP  K
KKKK K K X   HealthrX  G?��Q��eX   Phoenix BladerY  ]rZ  (jP  K
KKK K K K NK eX   Iron Daggerr[  ]r\  (jP  KKKK K K K NK eX
   Iron Swordr]  ]r^  (jP  KKKK K K K NK eX   Steel Longswordr_  ]r`  (jP  K
K
K K K K K NK eX   Seven-inch Serpent Staffra  ]rb  (jP  K K K K K K K j9  G?�      eX   Wings of the Dragonflyrc  ]rd  (jP  K
K
K(K
K(G?�������K jS  G?�������eX   Splinter Staffre  ]rf  (jP  KKKK(KK K NK eX   Lightning Saberrg  ]rh  (jP  K
K
KK K G?�������K X   Blindri  G?�      eX   Flame Bladerj  ]rk  (jP  K
KK K K G?�������K X   Internal Injuryrl  G?�333333eX   Thunder Bladerm  ]rn  (jP  K
KK K K G?�      K jl  G?�333333eX	   Ice Shardro  ]rp  (jP  K K KK K G?�������K X   Disablerq  G?�      eX   Granite Bootsrr  ]rs  (X   feetrt  K KJ����KJ����K G?�      jS  G?�      eX   Babao Bladeru  ]rv  (jP  KKKKKG?陙����K jS  G?陙����eX
   Wool Bootsrw  ]rx  (jt  K K KKK K K jX  G?��Q��eX   Leather Bootsry  ]rz  (jt  K KKKKK K NK eX   Explorer Bootsr{  ]r|  (jt  KKKKKK K X   Staminar}  G?�������eX   Explorer Ringr~  ]r  (j3  K K K K K K K jS  G?�333333eX   Silk Fanr�  ]r�  (j3  K K K KK K K j}  G?��Q��eX   Buddha Beadsr�  ]r�  (X   headr�  K K K K K K K X   Statusr�  G?�������eX   Blood Diamond Pendantr�  ]r�  (j�  K K K K K K K jX  G?��Q��eX   Amethyst Necklacer�  ]r�  (j�  KK K K K K K jS  G?�������eX   Maskr�  ]r�  (j�  K K K KK K K NK euhj}r�  (j1  M�j5  Mxj:  M�j=  M�
j@  M�jB  M�jD  M�jN  M�*jQ  MLjT  M�jY  MjV  M�j[  Kdj]  M�j_  M�ja  M�:jc  M�:je  M�:jg  M�:jj  M�:jm  M�:jo  M�:jF  M�jr  M�:ju  J8[ jw  M�jy  M�jH  M�jJ  M�jL  Kj�  K�j�  M,j�  Mpj�  MLj�  K j7  M'j~  K j{  K uhl}r�  (j:  X<   A light-weight chain mail formed from steel and gold mesh!

r�  jD  XZ   A light platebody made from a thin layer of tin and bronze that offers basic protection.

r�  jN  Xl   Forged from adamantite rock, this large blade may be
difficult to wield but can crush anything in its way.

r�  jQ  X-   A thin, sharp blade forged from black iron.

r�  jT  Xi   A curved sword that gives the wielder a distinct advantage
in both attack and defence due to its shape.

r�  jY  Xb   Forged from high-quality metals, this blade's light weight allows its wielder to attack quickly.

r�  j[  X)   Slightly better than being bare-handed.

r�  j]  X   A basic sword made of iron.

r�  j_  X   A longsword made from steel.

r�  ja  Xp   A highly toxic staff crafted in the shape of a serpant that, despite its short length, can deal a lethal blow.

r�  jc  Xg   A blade that is both light and easy to handle, allowing its user to perform rapid, consecutive strikes.r�  je  Xx   A staff with many sharp arms protruding from its body..

In battle, reflects 25% of damage taken back to the opponent.

r�  jg  X   Quick as lightning.

r�  jj  X^   A legendary blade that burns to the touch and can partially melt normal metals upon contact.

r�  jm  X]   As thunder is to the ears, so is this blade to the heart of anyone who opposes its wielder.

r�  jo  XP   A sharp dagger that pierces the bone as easily as a knife goes through butter.

r�  jV  X+   A swordwith a glow like that of the moon.

r�  j1  X!   Great protection for the hands!

r�  j5  X'   Gives some extra power to your punch.

r�  j7  X6   Hard leather gloves layered with deadly viper venom.

r�  jF  X_   It's much harder to be detected while wearing this.
Useful for sneaking into places at night.

r�  j{  XH   A pair of boots from Scrub as a gift for beta-testing. Beta-only item!

r�  jr  X   Boots coated with a thick layer of granite, making it more difficult to move quickly but multiplies the force of each attack.

r�  ju  X+   A powerful blade imbued with 8 rare gems.

r�  jw  X   Keeps my feet warm!

r�  jy  X'   Crafted from top-quality cow leather.

r�  jH  j�  jJ  j�  jL  X   A plain, linen shirt.

r�  j=  X    Great protection for the legs!

r�  j@  X>   A bit heavy, but shields your organs from devastating blows.

r�  jB  X,   Solid platebody made of high carbon steel.

r�  j~  X?   A ring from Scrub as a gift for beta-testing. Beta-only item!

r�  j�  X,   A delicate fan crafted from mulberry silk.

r�  j�  X-   A set of beads made from high-quality opal.

r�  j�  X:   A valuable pendant crafted using the rare blood diamond.

r�  j�  XF   A rare, enchanted necklace and one of three treasures of Eagle Sect.

r�  j�  XO   A creepy-looking mask. Maybe I can scare my opponents to death by wearing it.

r�  uhX   Steel Platelegs [Ultra Rare]r�  h�X   Steel Platelegsr�  h�X
   Ultra Rarer�  h�j?  h�K h�K h�J����h�K<h�K h�K h�K h�Nh�K h�X�   Steel Platelegs [Ultra Rare]
-------------------------------------
Great protection for the legs!

Gives the following bonuses in battle:
Attack + 0
Strength + 0
Speed -10
Defence + 60
Dexterity + 0
        


Selling price: 5400
r�  h�Mh�Kh�Kubh
)�r�  }r�  (h}r�  (X   Golden Glovesr�  ]r�  (X
   hands_leftr�  K K
K K
K K G?�      X   Sealr�  G?�������eX   Bronze Gauntletsr�  ]r�  (j�  K KK KK K G?�������NK eX   Venomous Viper Glovesr�  ]r�  (j�  K KK KK K G?�������X   Poisonr�  G?�      eX   Golden Chainmailr�  ]r�  (X   bodyr�  K K K K2K K K NK eX   Steel Platelegsr�  ]r�  (X   legsr�  K K J����KK K K NK eX   Steel Platebodyr�  ]r�  (j�  K K J����K2K K K NK eX   Hardened Steel Platebodyr�  ]r�  (j�  K K J����KFK K K NK eX   Light Armorr�  ]r�  (j�  K K J����KK K K NK eX
   Ninja Suitr�  ]r�  (j�  K K K K KK K NK eX   Leather Bodyr�  ]r�  (j�  K K K K
K K K NK eX   Leather Pantsr�  ]r�  (j�  K K K KK K K NK eX   Linen Shirtr�  ]r�  (j�  K K K KK K K NK eX   Heavy Adamantite Swordr�  ]r�  (X   hands_rightr�  KK2J����KK G?�      K NK eX   Black Salamanderr�  ]r�  (j�  KKK
K
K G?ə�����K X   Critical Hitr�  G?�������eX   Golden Snaker�  ]r�  (j�  KK K KK K K NK eX   Moonlight Blader�  ]r�  (j�  K
KKKK K K X   Healthr�  G?��Q��eX   Phoenix Blader�  ]r�  (j�  K
KKK K K K NK eX   Iron Daggerr�  ]r�  (j�  KKKK K K K NK eX
   Iron Swordr�  ]r�  (j�  KKKK K K K NK eX   Steel Longswordr�  ]r�  (j�  K
K
K K K K K NK eX   Seven-inch Serpent Staffr�  ]r�  (j�  K K K K K K K j�  G?�      eX   Wings of the Dragonflyr�  ]r�  (j�  K
K
K(K
K(G?�������K j�  G?�������eX   Splinter Staffr�  ]r�  (j�  KKKK(KK K NK eX   Lightning Saberr�  ]r�  (j�  K
K
KK K G?�������K X   Blindr�  G?�      eX   Flame Blader�  ]r�  (j�  K
KK K K G?�������K X   Internal Injuryr�  G?�333333eX   Thunder Blader�  ]r�  (j�  K
KK K K G?�      K j�  G?�333333eX	   Ice Shardr�  ]r�  (j�  K K KK K G?�������K X   Disabler�  G?�      eX   Granite Bootsr�  ]r�  (X   feetr�  K KJ����KJ����K G?�      j�  G?�      eX   Babao Blader�  ]r�  (j�  KKKKKG?陙����K j�  G?陙����eX
   Wool Bootsr�  ]r   (j�  K K KKK K K j�  G?��Q��eX   Leather Bootsr  ]r  (j�  K KKKKK K NK eX   Explorer Bootsr  ]r  (j�  KKKKKK K X   Staminar  G?�������eX   Explorer Ringr  ]r  (j�  K K K K K K K j�  G?�333333eX   Silk Fanr  ]r	  (j�  K K K KK K K j  G?��Q��eX   Buddha Beadsr
  ]r  (X   headr  K K K K K K K X   Statusr  G?�������eX   Blood Diamond Pendantr  ]r  (j  K K K K K K K j�  G?��Q��eX   Amethyst Necklacer  ]r  (j  KK K K K K K j�  G?�������eX   Maskr  ]r  (j  K K K KK K K NK euhj}r  (j�  M�j�  Mxj�  M�j�  M�
j�  M�j�  M�j�  M�j�  M�*j�  MLj�  M�j�  Mj�  M�j�  Kdj�  M�j�  M�j�  M�:j�  M�:j�  M�:j�  M�:j�  M�:j�  M�:j�  M�:j�  M�j�  M�:j�  J8[ j�  M�j  M�j�  M�j�  M�j�  Kj  K�j
  M,j  Mpj  MLj  K j�  M'j  K j  K uhl}r  (j�  X<   A light-weight chain mail formed from steel and gold mesh!

r  j�  XZ   A light platebody made from a thin layer of tin and bronze that offers basic protection.

r  j�  Xl   Forged from adamantite rock, this large blade may be
difficult to wield but can crush anything in its way.

r  j�  X-   A thin, sharp blade forged from black iron.

r  j�  Xi   A curved sword that gives the wielder a distinct advantage
in both attack and defence due to its shape.

r  j�  Xb   Forged from high-quality metals, this blade's light weight allows its wielder to attack quickly.

r  j�  X)   Slightly better than being bare-handed.

r  j�  X   A basic sword made of iron.

r  j�  X   A longsword made from steel.

r  j�  Xp   A highly toxic staff crafted in the shape of a serpant that, despite its short length, can deal a lethal blow.

r  j�  Xg   A blade that is both light and easy to handle, allowing its user to perform rapid, consecutive strikes.r   j�  Xx   A staff with many sharp arms protruding from its body..

In battle, reflects 25% of damage taken back to the opponent.

r!  j�  X   Quick as lightning.

r"  j�  X^   A legendary blade that burns to the touch and can partially melt normal metals upon contact.

r#  j�  X]   As thunder is to the ears, so is this blade to the heart of anyone who opposes its wielder.

r$  j�  XP   A sharp dagger that pierces the bone as easily as a knife goes through butter.

r%  j�  X+   A swordwith a glow like that of the moon.

r&  j�  X!   Great protection for the hands!

r'  j�  X'   Gives some extra power to your punch.

r(  j�  X6   Hard leather gloves layered with deadly viper venom.

r)  j�  X_   It's much harder to be detected while wearing this.
Useful for sneaking into places at night.

r*  j  XH   A pair of boots from Scrub as a gift for beta-testing. Beta-only item!

r+  j�  X   Boots coated with a thick layer of granite, making it more difficult to move quickly but multiplies the force of each attack.

r,  j�  X+   A powerful blade imbued with 8 rare gems.

r-  j�  X   Keeps my feet warm!

r.  j  X'   Crafted from top-quality cow leather.

r/  j�  j/  j�  j/  j�  X   A plain, linen shirt.

r0  j�  X    Great protection for the legs!

r1  j�  X>   A bit heavy, but shields your organs from devastating blows.

r2  j�  X,   Solid platebody made of high carbon steel.

r3  j  X?   A ring from Scrub as a gift for beta-testing. Beta-only item!

r4  j  X,   A delicate fan crafted from mulberry silk.

r5  j
  X-   A set of beads made from high-quality opal.

r6  j  X:   A valuable pendant crafted using the rare blood diamond.

r7  j  XF   A rare, enchanted necklace and one of three treasures of Eagle Sect.

r8  j  XO   A creepy-looking mask. Maybe I can scare my opponents to death by wearing it.

r9  uhX   Golden Chainmailr:  h�j:  h�X   Standardr;  h�j�  h�K h�K h�K h�K2h�K h�K h�K h�Nh�K h�X�   Golden Chainmail
-------------------------------------
A light-weight chain mail formed from steel and gold mesh!

Gives the following bonuses in battle:
Attack + 0
Strength + 0
Speed + 0
Defence + 50
Dexterity + 0
        


Selling price: 4500
r<  h�M�h�Kh�KubeX   smlr=  ]r>  (cspecial_move
special_move
r?  )�r@  }rA  (X   level_exp_thresholdsrB  ]rC  (K KdK�M�M�M�MFMM�M�ehKX   exprD  M[X   exp_raterE  KhX
   Scrub FistrF  X   effectsrG  NX	   file_namerH  X   scrub_fist_icon.ppmrI  X   sfx_file_namerJ  X   sfx_SGSHitSoundLoud.mp3rK  X   animation_file_listrL  ]rM  (X   a_Scrub_Fist1.gifrN  X   a_Scrub_Fist2.gifrO  X   a_Scrub_Fist3.gifrP  X   a_Scrub_Fist4.gifrQ  jP  jQ  eX   base_staminarR  KX   base_damagerS  K
X   staminarT  K5X   damagerU  K#X   damage_multiplierrV  G?�333333X   description_templaterW  Xm  Scrub Fist
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
A technique invented by Scrub as a starter move.

At level 5, gains 20% chance of stunning opponent.
At level 7, gains 30% chance of stunning opponent.
At level 10, gains 50% chance of stunning opponent.rX  X   typerY  X   UnarmedrZ  h�Xu  Scrub Fist
-------------------------
Proficiency Level: 8
Exp: 1883/2300
Base Damage: 35
Damage Growth: 20%
Stamina Cost: 53
Move Type: Unarmed
-------------------------
A technique invented by Scrub as a starter move.

At level 5, gains 20% chance of stunning opponent.
At level 7, gains 30% chance of stunning opponent.
At level 10, gains 50% chance of stunning opponent.r[  ubj?  )�r\  }r]  (jB  ]r^  (K KdK�M�M�M�MFMM�M�ehK
jD  MBjE  KhX   Taichi Fistr_  jG  NjH  X   taichi_fist_icon.pngr`  jJ  X   sfx_five_poison_palm.mp3ra  jL  ]rb  (X   a_taichi_fist1.gifrc  X   a_taichi_fist2.gifrd  X   a_taichi_fist3.gifre  X   a_taichi_fist4.gifrf  X   a_taichi_fist5.gifrg  X   a_taichi_fist6.gifrh  X   a_taichi_fist7.gifri  X   a_taichi_fist8.gifrj  X   a_taichi_fist9.gifrk  X   a_taichi_fist10.gifrl  X   a_taichi_fist11.gifrm  ejR  KjS  KjT  K3jU  K*jV  G?񙙙���jW  X  Taichi Fist
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
A highly defensive technique that incorporates the essence of Tai Chi.

At level 3, gains +1 defence on hit.
At level 5, gains +2 defence on hit.
At level 7, gains +3 defence on hit.
At level 10, gains +5 defence on hit.rn  jY  X   Unarmedro  h�X�  Taichi Fist
-------------------------
Proficiency Level: 10
Exp: 3000/3000
Base Damage: 42
Damage Growth: 10%
Stamina Cost: 51
Move Type: Unarmed
-------------------------
A highly defensive technique that incorporates the essence of Tai Chi.

At level 3, gains +1 defence on hit.
At level 5, gains +2 defence on hit.
At level 7, gains +3 defence on hit.
At level 10, gains +5 defence on hit.rp  ubj?  )�rq  }rr  (jB  ]rs  (K KdK�M�M�M�MFMM�M�ehK
jD  M�jE  KhX   Taichi Swordrt  jG  NjH  X   taichi_sword_icon.ppmru  jJ  X   sfx_taichi_sword.mp3rv  jL  ]rw  (X   a_taichi_sword1.gifrx  X   a_taichi_sword2.gifry  X   a_taichi_sword3.gifrz  X   a_taichi_sword4.gifr{  X   a_taichi_sword5.gifr|  X   a_taichi_sword6.gifr}  X   a_taichi_sword7.gifr~  X   a_taichi_sword8.gifr  X   a_taichi_sword9.gifr�  ejR  K#jS  KjT  K{jU  KMjV  G?�ffffffjW  X�  Taichi Sword
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
A defensive technique that relies on continuous, circular sword movement

At level 3, gains +1 defence on hit.
At level 5, gains +2 defence on hit.
At level 7, gains +3 defence on hit.
At level 10, gains +5 defence on hit.r�  jY  X   Armedr�  h�X�  Taichi Sword
-------------------------
Proficiency Level: 10
Exp: 3013/3000
Base Damage: 77
Damage Growth: 14%
Stamina Cost: 123
Move Type: Armed
-------------------------
A defensive technique that relies on continuous, circular sword movement

At level 3, gains +1 defence on hit.
At level 5, gains +2 defence on hit.
At level 7, gains +3 defence on hit.
At level 10, gains +5 defence on hit.r�  ubj?  )�r�  }r�  (jB  ]r�  (K KdK�M�M�M�MFMM�M�ehKjD  M�jE  KhX	   Ice Blader�  jG  NjH  X   ice_blade_icon.ppmr�  jJ  X   sfx_taichi_sword.mp3r�  jL  ]r�  (X   a_ice_blade1.gifr�  X   a_ice_blade2.gifr�  X   a_ice_blade3.gifr�  X   a_ice_blade4.gifr�  X   a_ice_blade5.gifr�  X   a_ice_blade6.gifr�  X   a_ice_blade7.gifr�  X   a_ice_blade8.gifr�  X   a_ice_blade9.gifr�  ejR  KKjS  K jT  K�jU  KijV  G?��Q�jW  X[  Ice Blade
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
A technique invented by Xiao Yong after spending many years dwelling in the extreme cold climates in the Snowy Mountains.
Each strike is infused with cold 'Qi' that disrupts the opponent's ability to harness their energy.

At level 2, gains 10% chance of causing internal injury; reduces opponent's speed by 1 on hit.
At level 4, gains 20% chance of causing internal injury; reduces opponent's speed by 2 on hit.
At level 6, gains 30% chance of causing internal injury; reduces opponent's speed by 3 on hit.
At level 8, gains 40% chance of causing internal injury; reduces opponent's speed by 4 on hit.
At level 10, gains 50% chance of causing internal injury; reduces opponent's speed by 5 on hit.r�  jY  X   Armedr�  h�Xc  Ice Blade
-------------------------
Proficiency Level: 7
Exp: 1518/1800
Base Damage: 105
Damage Growth: 22%
Stamina Cost: 247
Move Type: Armed
-------------------------
A technique invented by Xiao Yong after spending many years dwelling in the extreme cold climates in the Snowy Mountains.
Each strike is infused with cold 'Qi' that disrupts the opponent's ability to harness their energy.

At level 2, gains 10% chance of causing internal injury; reduces opponent's speed by 1 on hit.
At level 4, gains 20% chance of causing internal injury; reduces opponent's speed by 2 on hit.
At level 6, gains 30% chance of causing internal injury; reduces opponent's speed by 3 on hit.
At level 8, gains 40% chance of causing internal injury; reduces opponent's speed by 4 on hit.
At level 10, gains 50% chance of causing internal injury; reduces opponent's speed by 5 on hit.r�  ubj?  )�r�  }r�  (jB  ]r�  (K KdK�M�M�M�MFMM�M�ehK
jD  M�jE  KhX   Whirlwind Staffr�  jG  NjH  X   whirlwind_staff_icon.ppmr�  jJ  X   sfx_whirlwind_staff.wavr�  jL  ]r�  (X   a_whirlwind_staff1.gifr�  X   a_whirlwind_staff2.gifr�  X   a_whirlwind_staff3.gifr�  X   a_whirlwind_staff4.gifr�  X   a_whirlwind_staff5.gifr�  X   a_whirlwind_staff6.gifr�  X   a_whirlwind_staff7.gifr�  X   a_whirlwind_staff8.gifr�  X   a_whirlwind_staff9.gifr�  X   a_whirlwind_staff10.gifr�  X   a_whirlwind_staff11.gifr�  X   a_whirlwind_staff12.gifr�  X   a_whirlwind_staff13.gifr�  X   a_whirlwind_staff14.gifr�  X   a_whirlwind_staff15.gifr�  X   a_whirlwind_staff16.gifr�  X   a_whirlwind_staff17.gifr�  X   a_whirlwind_staff18.gifr�  X   a_whirlwind_staff19.gifr�  X   a_whirlwind_staff20.gifr�  X   a_whirlwind_staff21.gifr�  X   a_whirlwind_staff22.gifr�  X   a_whirlwind_staff23.gifr�  X   a_whirlwind_staff24.gifr�  X   a_whirlwind_staff25.gifr�  X   a_whirlwind_staff26.gifr�  X   a_whirlwind_staff27.gifr�  ejR  K(jS  KjT  K�jU  K[jV  G?�\(�jW  X�  Whirlwind Staff
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
At level 2, gains +1 attack and +1 strength on hit; 20% chance of blinding opponent for 1 turn.
At level 4, gains +1 attack and +1 strength on hit; 40% chance of blinding opponent for 1 turn.
At level 6, gains +2 attack and +2 strength on hit; 60% chance of blinding opponent for 1 turn.
At level 8, gains +2 attack and +2 strength on hit; 80% chance of blinding opponent for 1 turn.
At level 10, gains +3 attack and +3 strength on hit; 100% chance of blinding opponent for 1 turn.r�  jY  j�  h�X�  Whirlwind Staff
-------------------------
Proficiency Level: 10
Exp: 3013/3000
Base Damage: 91
Damage Growth: 15%
Stamina Cost: 152
Move Type: Armed
-------------------------
At level 2, gains +1 attack and +1 strength on hit; 20% chance of blinding opponent for 1 turn.
At level 4, gains +1 attack and +1 strength on hit; 40% chance of blinding opponent for 1 turn.
At level 6, gains +2 attack and +2 strength on hit; 60% chance of blinding opponent for 1 turn.
At level 8, gains +2 attack and +2 strength on hit; 80% chance of blinding opponent for 1 turn.
At level 10, gains +3 attack and +3 strength on hit; 100% chance of blinding opponent for 1 turn.r�  ubj?  )�r�  }r�  (jB  ]r�  (K KdK�M�M�M�MFMM�M�ehKjD  K jE  KhX'   Falling Cherry Blossom Finger Techniquer�  jG  NjH  X0   falling_cherry_blossom_finger_technique_icon.pngr�  jJ  X   sfx_finger.mp3r�  jL  ]r�  (X.   a_falling_cherry_blossom_finger_technique1.pngr�  X.   a_falling_cherry_blossom_finger_technique2.pngr�  X.   a_falling_cherry_blossom_finger_technique3.pngr�  X.   a_falling_cherry_blossom_finger_technique4.pngr�  X.   a_falling_cherry_blossom_finger_technique5.pngr�  X.   a_falling_cherry_blossom_finger_technique6.pngr�  X.   a_falling_cherry_blossom_finger_technique7.pngr�  X.   a_falling_cherry_blossom_finger_technique8.pngr�  X.   a_falling_cherry_blossom_finger_technique9.pngr�  X/   a_falling_cherry_blossom_finger_technique10.pngr�  X/   a_falling_cherry_blossom_finger_technique11.pngr�  X/   a_falling_cherry_blossom_finger_technique12.pngr�  X/   a_falling_cherry_blossom_finger_technique13.pngr�  X/   a_falling_cherry_blossom_finger_technique14.pngr�  X/   a_falling_cherry_blossom_finger_technique15.pngr�  X/   a_falling_cherry_blossom_finger_technique16.pngr�  X/   a_falling_cherry_blossom_finger_technique17.pngr�  X/   a_falling_cherry_blossom_finger_technique18.pngr�  X/   a_falling_cherry_blossom_finger_technique19.pngr�  ejR  KjS  KjT  KjU  KjV  G?���Q�jW  X�  Falling Cherry Blossom Finger Technique
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
A technique, invented by Huang Xiaodong, that has enough precision to
strike through a falling cherry blossom from 10 feet away.

At level 3, gains 10% chance of sealing opponent's acupuncture points for 3 turns.
At level 5, gains 20% chance of sealing opponent's acupuncture points for 3 turns.
At level 7, gains 30% chance of sealing opponent's acupuncture points for 3 turns.
At level 10, gains 40% chance of sealing opponent's acupuncture points for 3 turns.r�  jY  X   Unarmedr�  h�X�  Falling Cherry Blossom Finger Technique
-------------------------
Proficiency Level: 1
Exp: 0/100
Base Damage: 15
Damage Growth: 12%
Stamina Cost: 21
Move Type: Unarmed
-------------------------
A technique, invented by Huang Xiaodong, that has enough precision to
strike through a falling cherry blossom from 10 feet away.

At level 3, gains 10% chance of sealing opponent's acupuncture points for 3 turns.
At level 5, gains 20% chance of sealing opponent's acupuncture points for 3 turns.
At level 7, gains 30% chance of sealing opponent's acupuncture points for 3 turns.
At level 10, gains 40% chance of sealing opponent's acupuncture points for 3 turns.r�  ubj?  )�r�  }r�  (jB  ]r�  (K KdK�M�M�M�MFMM�M�ehKjD  MSjE  KhX   Dragon Roarr�  jG  NjH  X   dragon_roar_icon.ppmr�  jJ  X   sfx_roar.wavr�  jL  ]r�  (X   a_dragon_roar1.gifr�  X   a_dragon_roar2.gifr�  X   a_dragon_roar3.gifr�  X   a_dragon_roar4.gifr�  X   a_dragon_roar5.gifr�  X   a_dragon_roar6.gifr�  X   a_dragon_roar7.gifr�  X   a_dragon_roar8.gifr�  X   a_dragon_roar9.gifr�  X   a_dragon_roar10.gifr�  X   a_dragon_roar11.gifr�  X   a_dragon_roar12.gifr�  ejR  KZjS  KjT  K�jU  K0jV  G?��G�z�jW  X�  Dragon Roar
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
A deafening roar that requires much energy and concentration.

Move has 100% accuracy.

At level 3: 10% chance of causing internal injury to the opponent. Lowers opponent's defence by 3.
At level 5: 20% chance of causing internal injury to the opponent. Lowers opponent's defence by 3.
At level 7: 30% chance of causing internal injury to the opponent. Lowers opponent's defence by 3.
At level 10: 50% chance of causing internal injury to the opponent. Lowers opponent's defence by 3.r�  jY  X   Unarmedr�  h�X�  Dragon Roar
-------------------------
Proficiency Level: 5
Exp: 851/1000
Base Damage: 48
Damage Growth: 18%
Stamina Cost: 174
Move Type: Unarmed
-------------------------
A deafening roar that requires much energy and concentration.

Move has 100% accuracy.

At level 3: 10% chance of causing internal injury to the opponent. Lowers opponent's defence by 3.
At level 5: 20% chance of causing internal injury to the opponent. Lowers opponent's defence by 3.
At level 7: 30% chance of causing internal injury to the opponent. Lowers opponent's defence by 3.
At level 10: 50% chance of causing internal injury to the opponent. Lowers opponent's defence by 3.r�  ubj?  )�r�  }r�  (jB  ]r�  (K KdK�M�M�M�MFMM�M�ehKjD  MjE  KhX   Fairy Sword Techniquer�  jG  NjH  X   fairy_sword_technique_icon.ppmr�  jJ  X!   sfx_thousand_swords_technique.mp3r�  jL  ]r�  (X   a_fairy_sword_technique1.gifr�  X   a_fairy_sword_technique2.gifr�  X   a_fairy_sword_technique3.gifr�  X   a_fairy_sword_technique4.gifr�  X   a_fairy_sword_technique5.gifr�  X   a_fairy_sword_technique6.gifr�  X   a_fairy_sword_technique7.gifr�  ejR  KjS  KjT  K#jU  KjV  G?�ffffffjW  X�  Fairy Sword Technique
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
An elegant sword technique that is very suitable for women.

Recovers 3% health when used.

At level 2, increases dexterity by 1.
At level 4, increases dexterity by 2.
At level 6, increases dexterity by 3.
At level 8, increases dexterity by 4.
At level 10, increases dexterity by 5.r�  jY  X   Armedr�  h�X�  Fairy Sword Technique
-------------------------
Proficiency Level: 3
Exp: 276/450
Base Damage: 23
Damage Growth: 14%
Stamina Cost: 35
Move Type: Armed
-------------------------
An elegant sword technique that is very suitable for women.

Recovers 3% health when used.

At level 2, increases dexterity by 1.
At level 4, increases dexterity by 2.
At level 6, increases dexterity by 3.
At level 8, increases dexterity by 4.
At level 10, increases dexterity by 5.r�  ubj?  )�r�  }r   (jB  ]r  (K KdK�M�M�M�MFMM�M�ehKjD  MPjE  KhX   Demon Suppressing Blader  jG  NjH  X    demon_suppressing_blade_icon.ppmr  jJ  X*   sfx_cherry_blossom_drifting_snow_blade.mp3r  jL  ]r  (X   a_demon_suppressing_blade1.gifr  X   a_demon_suppressing_blade2.gifr  X   a_demon_suppressing_blade3.gifr  X   a_demon_suppressing_blade4.gifr	  X   a_demon_suppressing_blade5.gifr
  X   a_demon_suppressing_blade6.gifr  X   a_demon_suppressing_blade7.gifr  X   a_demon_suppressing_blade8.gifr  ejR  K-jS  KjT  KfjU  K0jV  G?��G�z�jW  X�  Demon Suppressing Blade
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
Drains the opponent's energy while inflicting damage.

At level 3, reduces opponent's stamina by 25% of damage dealt.
At level 5, reduces opponent's stamina by 50% of damage dealt.
At level 7, reduces opponent's stamina by 100% of damage dealt.
At level 10, reduces opponent's stamina by 200% of damage dealt.r  jY  j�  h�X�  Demon Suppressing Blade
-------------------------
Proficiency Level: 6
Exp: 1104/1350
Base Damage: 48
Damage Growth: 18%
Stamina Cost: 102
Move Type: Armed
-------------------------
Drains the opponent's energy while inflicting damage.

At level 3, reduces opponent's stamina by 25% of damage dealt.
At level 5, reduces opponent's stamina by 50% of damage dealt.
At level 7, reduces opponent's stamina by 100% of damage dealt.
At level 10, reduces opponent's stamina by 200% of damage dealt.r  ubj?  )�r  }r  (jB  ]r  (K KdK�M�M�M�MFMM�M�ehKjD  M\jE  KhX
   Smoke Bombr  jG  NjH  X   smoke_bomb_icon.ppmr  jJ  X   sfx_smoke_bomb.mp3r  jL  ]r  (X   a_smoke_bomb1.pngr  X   a_smoke_bomb2.pngr  X   a_smoke_bomb3.pngr  X   a_smoke_bomb4.pngr  X   a_smoke_bomb5.pngr  X   a_smoke_bomb6.pngr  X   a_smoke_bomb7.pngr  X   a_smoke_bomb8.pngr  X   a_smoke_bomb9.pngr  X   a_smoke_bomb10.pngr   X   a_smoke_bomb11.pngr!  X   a_smoke_bomb12.pngr"  X   a_smoke_bomb13.pngr#  X   a_smoke_bomb14.pngr$  X   a_smoke_bomb15.pngr%  X   a_smoke_bomb16.pngr&  X   a_smoke_bomb17.pngr'  ejR  K
jS  K2jT  KjU  KIjV  G?񙙙���jW  X�  Smoke Bomb
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
Uses item: <Smoke Bomb> x 1.
At level 3, gains 15% chance of blinding opponent for 2 turns.
At level 5, gains 25% chance of blinding opponent for 2 turns.
At level 7, gains 35% chance of blinding opponent for 2 turns.
At level 10, gains 50% chance of blinding opponent for 2 turns.r(  jY  X   Hidden Weaponr)  h�X�  Smoke Bomb
-------------------------
Proficiency Level: 5
Exp: 860/1000
Base Damage: 73
Damage Growth: 10%
Stamina Cost: 14
Move Type: Hidden Weapon
-------------------------
Uses item: <Smoke Bomb> x 1.
At level 3, gains 15% chance of blinding opponent for 2 turns.
At level 5, gains 25% chance of blinding opponent for 2 turns.
At level 7, gains 35% chance of blinding opponent for 2 turns.
At level 10, gains 50% chance of blinding opponent for 2 turns.r*  ubj?  )�r+  }r,  (jB  ]r-  (K KdK�M�M�M�MFMM�M�ehK
jD  M�jE  KhX   Restr.  jG  NjH  X   rest_icon.ppmr/  jJ  X   sfx_recover.mp3r0  jL  ]r1  (X   a_rest1.gifr2  X   a_rest2.gifr3  X   a_rest3.gifr4  X   a_rest4.gifr5  X   a_rest5.gifr6  X   a_rest6.gifr7  X   a_rest7.gifr8  X   a_rest8.gifr9  X   a_rest9.gifr:  X   a_rest10.gifr;  X   a_rest11.gifr<  X   a_rest12.gifr=  X   a_rest13.gifr>  X   a_rest14.gifr?  X   a_rest15.gifr@  X   a_rest16.gifrA  X   a_rest17.gifrB  X   a_rest18.gifrC  X   a_rest19.gifrD  ejR  K jS  K jT  K jU  K jV  G?񙙙���jW  X�   Rest
-------------------------
Recovers 5% of your health and 20% stamina (based on upper limit);
actual amount can increase based on character skills obtained and/or equipment.rE  jY  X   BasicrF  h�jE  ubj?  )�rG  }rH  (jB  ]rI  (K KdK�M�M�M�MFMM�M�ehKjD  K jE  KhX   ItemsrJ  jG  NjH  X   inventory_icon.ppmrK  jJ  X   button-20.mp3rL  jL  ]rM  jR  K jS  K jT  K jU  K jV  G?񙙙���jW  X    Use an item from your inventory.rN  jY  jF  h�jN  ubj?  )�rO  }rP  (jB  ]rQ  (K KdK�M�M�M�MFMM�M�ehKjD  K�jE  KhX   FleerR  jG  NjH  X   flee_icon.ppmrS  jJ  jL  jL  ]rT  jR  K jS  K jT  K jU  K jV  G?񙙙���jW  Xb   Flee from battle! Chance of success depends on your
and opponent's speed and difference in levels.rU  jY  jF  h�jU  ubeX   skillsrV  ]rW  (cskill
skill
rX  )�rY  }rZ  (hX%   Cherry Blossoms Floating on the Waterr[  hK
h�X�  Cherry Blossoms Floating on the Water (level 10)
----------------------------------------

Level 1: in battle, increases speed by 2 and dexterity by 1.
Level 2: in battle, increases speed by 4 and dexterity by 2.
Level 3: in battle, increases speed by 6 and dexterity by 3.
Level 4: in battle, increases speed by 8 and dexterity by 4.
Level 5: in battle, increases speed by 10 and dexterity by 5.
Level 6: in battle, increases speed by 12 and dexterity by 6.
Level 7: in battle, increases speed by 14 and dexterity by 7.
Level 8: in battle, increases speed by 16 and dexterity by 8.
Level 9: in battle, increases speed by 18 and dexterity by 9.
Level 10: in battle, increases speed by 20 and dexterity by 10.r\  jH  X.   Cherry Blossoms Floating on the Water_icon.ppmr]  ubjX  )�r^  }r_  (hX   Wudang Agility Techniquer`  hK
h�X�  Wudang Agility Technique (level 10)
----------------------------------------

Level 1: in battle, increases speed by 1 and dexterity by 2.
Level 2: in battle, increases speed by 2 and dexterity by 4.
Level 3: in battle, increases speed by 3 and dexterity by 6.
Level 4: in battle, increases speed by 4 and dexterity by 8.
Level 5: in battle, increases speed by 5 and dexterity by 10.
Level 6: in battle, increases speed by 6 and dexterity by 12.
Level 7: in battle, increases speed by 7 and dexterity by 14.
Level 8: in battle, increases speed by 8 and dexterity by 16.
Level 9: in battle, increases speed by 9 and dexterity by 18.
Level 10: in battle, increases speed by 10 and dexterity by 20.ra  jH  X!   wudang_agility_technique_icon.ppmrb  ubjX  )�rc  }rd  (hX   Splitting Mountains and Earthre  hK
h�X�  Splitting Mountains and Earth (level 10)
----------------------------------------

Level 1: in battle, increases strength by 3 and increases chance of critical hit by 3%.
Level 2: in battle, increases strength by 6 and increases chance of critical hit by 6%.
Level 3: in battle, increases strength by 9 and increases chance of critical hit by 9%.
Level 4: in battle, increases strength by 12 and increases chance of critical hit by 12%.
Level 5: in battle, increases strength by 15 and increases chance of critical hit by 15%.
Level 6: in battle, increases strength by 18 and increases chance of critical hit by 18%.
Level 7: in battle, increases strength by 21 and increases chance of critical hit by 21%.
Level 8: in battle, increases strength by 24 and increases chance of critical hit by 24%.
Level 9: in battle, increases strength by 27 and increases chance of critical hit by 27%.
Level 10: in battle, increases strength by 30 and increases chance of critical hit by 30%.rf  jH  X&   splitting_mountains_and_earth_icon.ppmrg  ubjX  )�rh  }ri  (hX   Recuperationrj  hK
h�X�  Recuperation (level 10)
----------------------------------------

Level 1: in battle, recovers additional 1% health when using 'Rest'.
Level 2: in battle, recovers additional 2% health when using 'Rest'.
Level 3: in battle, recovers additional 3% health when using 'Rest'.
Level 4: in battle, recovers additional 4% health when using 'Rest'.
Level 5: in battle, recovers additional 5% health when using 'Rest'.
Level 6: in battle, recovers additional 6% health when using 'Rest'.
Level 7: in battle, recovers additional 7% health when using 'Rest'.
Level 8: in battle, recovers additional 8% health when using 'Rest'.
Level 9: in battle, recovers additional 9% health when using 'Rest'.
Level 10: in battle, recovers additional 10% health when using 'Rest'.rk  jH  X   recuperation_icon.ppmrl  ubjX  )�rm  }rn  (hX   Taichi 18 Formsro  hK
h�X�   Taichi 18 Forms (level 10)
----------------------------------------

In battle, receives damage bonus based on defence.
Increases stamina upper limit by 200 for each upgrade.rp  jH  X   taichi_eighteen_forms_icon.ppmrq  ubjX  )�rr  }rs  (hX   Tendon Changing Techniquert  hK
h�X�  Tendon Changing Technique (level 10)
----------------------------------------

At end of turn, 20% chance of restoring status to 'Normal'.
Level 2: Increases health and stamina upper limits by 40.
Level 3: Increases health and stamina upper limits by 60.
Level 4: Increases health and stamina upper limits by 80.
Level 5: Increases health and stamina upper limits by 100.
Level 6: Increases health and stamina upper limits by 120.
Level 7: Increases health and stamina upper limits by 140.
Level 8: Increases health and stamina upper limits by 160.
Level 9: Increases health and stamina upper limits by 180.
Level 10: Increases health and stamina upper limits by 200.ru  jH  X"   tendon_changing_technique_icon.pngrv  ubjX  )�rw  }rx  (hX   Basic Agility Techniquery  hKh�X  Basic Agility Technique (level 1)
----------------------------------------

Level 1: in battle, increases dexterity by 1.
Level 2: in battle, increases dexterity by 3.
Level 3: in battle, increases dexterity by 4.
Level 4: in battle, increases dexterity by 6.
Level 5: in battle, increases dexterity by 7.
Level 6: in battle, increases dexterity by 9.
Level 7: in battle, increases dexterity by 10.
Level 8: in battle, increases dexterity by 12.
Level 9: in battle, increases dexterity by 13.
Level 10: in battle, increases dexterity by 15.rz  jH  X    basic_agility_technique_icon.ppmr{  ubjX  )�r|  }r}  (hX   Burst of Potentialr~  hK
h�X�  Burst of Potential (level 10)
----------------------------------------

Level 1: in battle, increases attack by 3 and strength by 3.
Level 2: in battle, increases attack by 6 and strength by 6.
Level 3: in battle, increases attack by 9 and strength by 9.
Level 4: in battle, increases attack by 12 and strength by 12.
Level 5: in battle, increases attack by 15 and strength by 15.
Level 6: in battle, increases attack by 18 and strength by 18.
Level 7: in battle, increases attack by 21 and strength by 21.
Level 8: in battle, increases attack by 24 and strength by 24.
Level 9: in battle, increases attack by 27 and strength by 27.
Level 10: in battle, increases attack by 30 and strength by 30.r  jH  X   burst_of_potential_icon.ppmr�  ubjX  )�r�  }r�  (hX   Divine Protectionr�  hK
h�X�   Divine Protection (level 10)
----------------------------------------
25% of damage taken is dealt back to opponent.

Increases health and stamina upper limit by 150 for each upgrade.

In battle, increases defence by 5 for each level.r�  jH  X   divine_protection_icon.ppmr�  ubjX  )�r�  }r�  (hX   Dragonfly Dancer�  hKh�X4  Dragonfly Dance (level 1)
----------------------------------------

At level 1: in battle, increases dexterity by 2.
At level 2: in battle, increases dexterity by 4.
At level 3: in battle, increases dexterity by 6.
At level 4: in battle, increases dexterity by 8.
At level 5: in battle, increases dexterity by 10.
At level 6: in battle, increases dexterity by 12.
At level 7: in battle, increases dexterity by 14.
At level 8: in battle, increases dexterity by 16.
At level 9: in battle, increases dexterity by 18.
At level 10: in battle, increases dexterity by 20.r�  jH  X   dragonfly_dance_icon.ppmr�  ubjX  )�r�  }r�  (hX   Marrow Cleansing Techniquer�  hK
h�X�   Marrow Cleansing Technique (level 10)
----------------------------------------

In battle, reduces chance of gaining negative status effects by 50%.
Increases health upper limit by 50 and stamina upper limit by 150 for each upgrade.r�  jH  X#   marrow_cleansing_technique_icon.ppmr�  ubjX  )�r�  }r�  (hX
   Ninjutsu Ir�  hK
h�X�  Ninjutsu I (level 10)
----------------------------------------
Art of the Ninja

Level 1: in battle, increases attack, speed, and dexterity by 1.
Level 2: in battle, increases attack, speed, and dexterity by 2.
Level 3: in battle, increases attack, speed, and dexterity by 3.
Level 4: in battle, increases attack, speed, and dexterity by 4.
Level 5: in battle, increases attack, speed, and dexterity by 5.
Level 6: in battle, increases attack, speed, and dexterity by 6.
Level 7: in battle, increases attack, speed, and dexterity by 7.
Level 8: in battle, increases attack, speed, and dexterity by 8.
Level 9: in battle, increases attack, speed, and dexterity by 9.
Level 10: in battle, increases attack, speed, and dexterity by 10.r�  jH  X   ninjutsu_i_icon.pngr�  ubjX  )�r�  }r�  (hX   Ninjutsu IIr�  hK
h�X�  Ninjutsu II (level 10)
----------------------------------------
Art of the Ninja

Level 1: in battle, increases attack, speed, and dexterity by 3.
Level 2: in battle, increases attack, speed, and dexterity by 6.
Level 3: in battle, increases attack, speed, and dexterity by 9.
Level 4: in battle, increases attack, speed, and dexterity by 12.
Level 5: in battle, increases attack, speed, and dexterity by 15.
Level 6: in battle, increases attack, speed, and dexterity by 18.
Level 7: in battle, increases attack, speed, and dexterity by 21.
Level 8: in battle, increases attack, speed, and dexterity by 24.
Level 9: in battle, increases attack, speed, and dexterity by 27.
Level 10: in battle, increases attack, speed, and dexterity by 30.r�  jH  X   ninjutsu_ii_icon.pngr�  ubjX  )�r�  }r�  (hX   Ninjutsu IIIr�  hK
h�X�  Ninjutsu III (level 10)
----------------------------------------
Art of the Ninja

Increases health and stamina upper limit by 100 for each upgrade.

Level 1: in battle, increases chance of critical hit by 5%.
Level 2: in battle, increases chance of critical hit by 10%.
Level 3: in battle, increases chance of critical hit by 15%.
Level 4: in battle, increases chance of critical hit by 20%.
Level 5: in battle, increases chance of critical hit by 25%.
Level 6: in battle, increases chance of critical hit by 30%.
Level 7: in battle, increases chance of critical hit by 35%.
Level 8: in battle, increases chance of critical hit by 40%.
Level 9: in battle, increases chance of critical hit by 45%.
Level 10: in battle, increases chance of critical hit by 50%.r�  jH  X   ninjutsu_iii_icon.pngr�  ubjX  )�r�  }r�  (hX	   Toad Formr�  hKh�X�  Toad Form (level 1)
----------------------------------------

Level 1: in battle, opponent takes +3% damage from poison (applied to base poison damage).
Level 2: in battle, opponent takes +6% damage from poison (applied to base poison damage).
Level 3: in battle, opponent takes +9% damage from poison (applied to base poison damage).
Level 4: in battle, opponent takes +12% damage from poison (applied to base poison damage).
Level 5: in battle, opponent takes +15% damage from poison (applied to base poison damage).
Level 6: in battle, opponent takes +18% damage from poison (applied to base poison damage).
Level 7: in battle, opponent takes +21% damage from poison (applied to base poison damage).
Level 8: in battle, opponent takes +24% damage from poison (applied to base poison damage).
Level 9: in battle, opponent takes +27% damage from poison (applied to base poison damage).
Level 10: in battle, opponent takes +30% damage from poison (applied to base poison damage).r�  jH  X   toad_form_icon.pngr�  ubjX  )�r�  }r�  (hX   Spreading Wingsr�  hK
h�X�  Spreading Wings (level 10)
----------------------------------------

Level 1: in battle, increases speed by 2 and attack by 1.
Level 2: in battle, increases speed by 4 and attack by 2.
Level 3: in battle, increases speed by 6 and attack by 3.
Level 4: in battle, increases speed by 8 and attack by 4.
Level 5: in battle, increases speed by 10 and attack by 5.
Level 6: in battle, increases speed by 12 and attack by 6.
Level 7: in battle, increases speed by 14 and attack by 7.
Level 8: in battle, increases speed by 16 and attack by 8.
Level 9: in battle, increases speed by 18 and attack by 9.
Level 10: in battle, increases speed by 20 and attack by 10.r�  jH  X   spreading_wings_icon.ppmr�  ubjX  )�r�  }r�  (hX   Huashan Agility Techniquer�  hKh�Xd  Huashan Agility Technique (level 1)
----------------------------------------

At level 1: in battle, increases speed by 1 and dexterity by 1 (stack effect).
At level 2: in battle, increases speed by 1 and dexterity by 1 (stack effect).
At level 3: in battle, increases speed by 1 and dexterity by 1 (stack effect).
At level 4: in battle, increases speed by 1 and dexterity by 1 (stack effect).
At level 5: in battle, increases speed by 2 and dexterity by 1 (stack effect).
At level 6: in battle, increases speed by 2 and dexterity by 1 (stack effect).
At level 7: in battle, increases speed by 2 and dexterity by 1 (stack effect).
At level 8: in battle, increases speed by 2 and dexterity by 1 (stack effect).
At level 9: in battle, increases speed by 2 and dexterity by 1 (stack effect).
At level 10: in battle, increases speed by 3 and dexterity by 1 (stack effect).r�  jH  X"   huashan_agility_technique_icon.ppmr�  ubjX  )�r�  }r�  (hX   Bushidor�  hK
h�X�   Bushido (level 10)
----------------------------------------
Way of the Warrior

Increases health and stamina upper limit by 100 for each upgrade.

In battle, increases strength by 4 for each level.r�  jH  X   bushido_icon.pngr�  ubjX  )�r�  }r�  (hX
   Kenjutsu Ir�  hK
h�X�  Kenjutsu I (level 10)
----------------------------------------
Art of the Sword

Level 1: in battle, increases attack by 2 and gives +2% damage bonus for armed attacks.
Level 2: in battle, increases attack by 4 and gives +4% damage bonus for armed attacks.
Level 3: in battle, increases attack by 6 and gives +6% damage bonus for armed attacks.
Level 4: in battle, increases attack by 8 and gives +8% damage bonus for armed attacks.
Level 5: in battle, increases attack by 10 and gives +10% damage bonus for armed attacks.
Level 6: in battle, increases attack by 12 and gives +12% damage bonus for armed attacks.
Level 7: in battle, increases attack by 14 and gives +14% damage bonus for armed attacks.
Level 8: in battle, increases attack by 16 and gives +16% damage bonus for armed attacks.
Level 9: in battle, increases attack by 18 and gives +18% damage bonus for armed attacks.
Level 10: in battle, increases attack by 20 and gives +20% damage bonus for armed attacks.r�  jH  X   kenjutsu_i_icon.pngr�  ubeX   statusr�  X   Normalr�  X   health_recoveryr�  G?�������X   stamina_recoveryr�  G?ə�����X   healthr�  M�X	   healthMaxr�  MB'jT  MoX
   staminaMaxr�  M�2X   attackr�  K�X   strengthr�  K�X   speedr�  K�X   defencer�  MX	   dexterityr�  KUub.