�ccharacter
character
q )�q}q(X   nameqX   2344qX   levelqKX   itemListq]qX   equipmentListq]q	X   smlq
]q(cspecial_move
special_move
q)�q}q(X   level_exp_thresholdsq]q(K KdK�M�M�M�MFMM�M�ehKX   expqK X   exp_rateqK
hX
   Scrub FistqX   effectsqNX	   file_nameqX   scrub_fist_icon.ppmqX   sfx_file_nameqX   sfx_SGSHitSoundLoud.mp3qX   animation_file_listq]q(X   a_Scrub_Fist1.gifqX   a_Scrub_Fist2.gifqX   a_Scrub_Fist3.gifqX   a_Scrub_Fist4.gifqhheX   base_staminaqKX   base_damageq K
X   staminaq!KX   damageq"K
X   damage_multiplierq#G?�333333X   description_templateq$Xm  Scrub Fist
-------------------------
Proficiency Level: {}
Exp: {}/{}
Base Damage: {}
Damage Growth: {}%
Stamina Cost: {}
Move Type: {}
-------------------------
A technique invented by Scrub as a starter move.

At level 5, gains 20% chance of stunning opponent.
At level 7, gains 30% chance of stunning opponent.
At level 10, gains 50% chance of stunning opponent.q%X   typeq&X   Unarmedq'X   descriptionq(Xq  Scrub Fist
-------------------------
Proficiency Level: 1
Exp: 0/100
Base Damage: 10
Damage Growth: 20%
Stamina Cost: 15
Move Type: Unarmed
-------------------------
A technique invented by Scrub as a starter move.

At level 5, gains 20% chance of stunning opponent.
At level 7, gains 30% chance of stunning opponent.
At level 10, gains 50% chance of stunning opponent.q)ubh)�q*}q+(h]q,(K KdK�M�M�M�MFMM�M�ehKhK hK
hX   Restq-hNhX   rest_icon.ppmq.hX   sfx_recover.mp3q/h]q0(X   a_rest1.gifq1X   a_rest2.gifq2X   a_rest3.gifq3X   a_rest4.gifq4X   a_rest5.gifq5X   a_rest6.gifq6X   a_rest7.gifq7X   a_rest8.gifq8X   a_rest9.gifq9X   a_rest10.gifq:X   a_rest11.gifq;X   a_rest12.gifq<X   a_rest13.gifq=X   a_rest14.gifq>X   a_rest15.gifq?X   a_rest16.gifq@X   a_rest17.gifqAX   a_rest18.gifqBX   a_rest19.gifqCehK h K h!K h"K h#G?񙙙���h$X�   Rest
-------------------------
Recovers 5% of your health and 20% stamina (based on upper limit);
actual amount can increase based on character skills obtained and/or equipment.qDh&X   BasicqEh(hDubh)�qF}qG(h]qH(K KdK�M�M�M�MFMM�M�ehKhK hK
hX   ItemsqIhNhX   inventory_icon.ppmqJhX   button-20.mp3qKh]qLhK h K h!K h"K h#G?񙙙���h$X@   Use an item from your inventory. Spends 33% fewer action points.qMh&hEh(hMubh)�qN}qO(h]qP(K KdK�M�M�M�MFMM�M�ehKhK hK
hX   FleeqQhNhX   flee_icon.ppmqRhhKh]qShK h K h!K h"K h#G?񙙙���h$Xb   Flee from battle! Chance of success depends on your
and opponent's speed and difference in levels.qTh&hEh(hTubeX   skillsqU}qVX   statusqWX   NormalqXX   health_recoveryqYG?�������X   stamina_recoveryqZG?ə�����X   healthq[K�X	   healthMaxq\K�h!K�X
   staminaMaxq]K�X   attackq^K8X   strengthq_K7X   speedq`K7X   defenceqaK8X	   dexterityqbK1ub.