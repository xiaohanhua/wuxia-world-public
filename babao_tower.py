from gettext import gettext
from functools import partial
from helper_funcs import *
import tkinter.messagebox as messagebox


from mini_game_babao_tower_wind import*
from mini_game_babao_tower_fire import*
from mini_game_babao_tower_thunder import*
from mini_game_babao_tower_lightning import*
from mini_game_babao_tower_metal import*
from mini_game_babao_tower_wood import*
from mini_game_babao_tower_water import*
from mini_game_babao_tower_earth import*
from mini_game_babao_tower_final import*
from mini_game_imperial_palace_digging import*



_ = gettext


class Scene_babao_tower:

    def __init__(self, game):
        self.game = game


        if self.game.taskProgressDic["babao_tower"] == -1:
            self.game.taskProgressDic["babao_tower"] = 0
            self.game.generate_dialogue_sequence(
                None,
                "Babao Tower Dialogue BG.png",
                [_("Nonsense! How can 'Wood' be the most powerful element?"),
                 _("Oh yeah? Your 'Fire' might have an advantage over me, but it's easily quenched by water."),
                 _("So you are certainly not the strongest either!"),
                 _("Step aside, weaklings! Neither of you are a match for my 'Thunder'!"),
                 _("All talk and no action... Just a bunch of noise, that's what you are."),
                 _("What was that? You sound like you want a taste of my Thunder Blade..."),
                 _("If we're bring out our weapons, then my Lightning Saber's speed has no rival!"), #Row 2
                 _("I will disable you all before you can even draw your weapons!"),
                 _("Enough guys... We're here to discuss serious matters, not to argue about who--"),
                 _("Have you forgotten about the very ground upon which you stand? Surely 'Earth' is of utmost importance!"),
                 _("Ugh... At this rate, we're never going to reach an agreement..."),
                 _("Erm... Excuse me... Sorry to interrupt, but what's going on here?"),
                 _("And you are....?"), #Row 3
                 _("Ah, my name is {}. I just happened to pass by and saw you guys, er, having a heated... discussion...").format(self.game.character),
                 _("*Sigh*... How embarrassing... The eight of us have been friends for almost 2 decades."),
                 _("We grew up in the same village and also studied martial arts together."),
                 _("Three years ago, we built this tower and decided to make this our primary residence."),
                 _("But recently, we decided to form our own sect so that we can make our names great among the people."),
                 _("We will become the most powerful sect in Wulin, and all will fear and respect the Eight Treasures of Hunan!"), #Row 4
                 _("The Eight Treasures of Hunan...?"),
                 _("That's right; the eight of us each represent an element, and together, we are known as the Eight Treasures of Hunan!"),
                 _("I am Wen Yulan: Wind."),
                 _("I am Zhang Andi: Fire."),
                 _("I am Lei Mengde: Thunder."),
                 _("I am Li Mianzhuo: Lightning."), #Row 5
                 _("I am Chen Jin: Metal."),
                 _("I am Han Lin: Wood."),
                 _("I am Li Xiaoyang: Water."),
                 _("I am Nie Tu: Earth."),
                 _("Interesting... Nice to meet you all."),
                 _("Anyway, back to the issue at hand, which of us should become leader?"), #Row 6
                 _("Why don't we just have a duel with each other and see who comes out on top?"),
                 _("That's not going to work. Our styles all counter and complement each other."),
                 _("It will be a huge mess..."),
                 _("Hmmm... true... in that case, I have a proposal..."),
                 _("Why don't each of us set up a challenge for {} and have him try out the challenges?").format(self.game.character),
                 _("Afterwards, he can choose which one of us is the strongest based on the difficulty of our challenges."), #Row 7
                 _("He is an outsider without any special interests, so we can trust him to be unbiased."),
                 _("Good idea!"),
                 _("Yeah, I'm fine with that."),
                 _("No objections here."),
                 _("Perfect; that's at least half of us in agreement!"),
                 _("Hold on a second! I think you guys are forgetting about something."), #Row 8
                 _("I haven't agreed yet! Why should I do this? What's in it for me?"),
                 _("True... we gotta make it worth your time..."),
                 _("How about this... To encourage you to test out our challenges, each of us will give you a reward when you pass our challenge."),
                 _("I guarantee you will not regret it!"),
                 _("Hmmmm... even though I'm a busy person, for the sake of peace in the Martial Arts Community, I'll do it."),
                 _("I can only imagine what sort of chaos would ensue if the 8 of you split into 8 different sects..."), #Row 9
                 _("....."),
                 _("Alright then, it's a deal! We're ready whenever you are!")
                 ],
                ["Zhang Andi", "Han Lin", "Han Lin", "Lei Mengde", "Chen Jin", "Lei Mengde",
                 "Li Mianzhuo", "Li Mianzhuo", "Li Xiaoyang", "Nie Tu", "Wen Yulan", "you",
                 "Li Xiaoyang", "you", "Li Xiaoyang", "Li Xiaoyang", "Li Xiaoyang", "Li Xiaoyang",
                 "Li Xiaoyang", "you", "Wen Yulan", "Wen Yulan", "Zhang Andi", "Lei Mengde",
                 "Li Mianzhuo", "Chen Jin", "Han Lin", "Li Xiaoyang", "Nie Tu", "you",
                 "Zhang Andi", "Lei Mengde", "Wen Yulan", "Wen Yulan", "Li Xiaoyang", "Li Xiaoyang",
                 "Li Xiaoyang", "Li Xiaoyang", "Li Mianzhuo", "Chen Jin", "Han Lin", "Wen Yulan",
                 "you", "you", "Nie Tu", "Lei Mengde", "Li Xiaoyang", "you",
                 "you", "Wen Yulan", "Zhang Andi",
                 ]
            )


        self.game.babao_tower_win = Toplevel(self.game.mapWin)
        self.game.babao_tower_win.title(_("Babao Tower"))

        bg = PhotoImage(file="Babao Tower.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.babao_tower_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.tower_characters = [_("Wen Yulan"), _("Zhang Andi"), _("Lei Mengde"), _("Li Mianzhuo"),
                            _("Chen Jin"), _("Han Lin"), _("Li Xiaoyang"), _("Nie Tu")]
        self.tower_elements = [_("Wind"), _("Fire"), _("Thunder"), _("Lightning"),
                               _("Metal"), _("Wood"), _("Water"), _("Earth")]
        self.tower_mini_games = [babao_tower_wind_mini_game, babao_tower_fire_mini_game, babao_tower_thunder_mini_game,
                                 babao_tower_lightning_mini_game, babao_tower_metal_mini_game, babao_tower_wood_mini_game,
                                 babao_tower_water_mini_game, babao_tower_earth_mini_game]
        self.all_possible_opps = [character(_("Wen Yulan"), 18,
                                            sml=[special_move(_("Babao Blade"), level=10),
                                                 special_move(_("Whirlwind Staff"), level=10)],
                                            skills=[skill(_("Basic Agility Technique"), level=10), ],
                                            equipmentList=[Equipment(_("Wings of the Dragonfly"))],
                                            preferences=[3, 2, 3, 1, 1]),
                                  character(_("Zhang Andi"), 18,
                                            sml=[special_move(_("Babao Blade"), level=10),
                                                 special_move(_("Fire Palm"), level=10)],
                                            skills=[skill(_("Basic Agility Technique"), level=10), ],
                                            equipmentList=[Equipment(_("Flame Blade"))],
                                            preferences=[3, 3, 1, 1, 1]),
                                  character(_("Lei Mengde"), 18,
                                            sml=[special_move(_("Babao Blade"), level=10),
                                                 special_move(_("Thunder Palm"), level=10)],
                                            skills=[skill(_("Basic Agility Technique"), level=10), ],
                                            equipmentList=[Equipment(_("Thunder Blade"))],
                                            preferences=[3, 5, 3, 3, 3]),
                                  character(_("Li Mianzhuo"), 18,
                                            sml=[special_move(_("Babao Blade"), level=10),
                                                 special_move(_("Lightning Finger Technique"), level=10)],
                                            skills=[skill(_("Basic Agility Technique"), level=10), ],
                                            equipmentList=[Equipment(_("Lightning Saber"))],
                                            preferences=[1, 1, 4, 1, 4]),
                                  character(_("Chen Jin"), 18,
                                            sml=[special_move(_("Babao Blade"), level=10),
                                                 special_move(_("Iron Sand Palm"), level=10)],
                                            skills=[skill(_("Basic Agility Technique"), level=10), ],
                                            equipmentList=[Equipment(_("Hardened Steel Platebody"))],
                                            preferences=[1, 1, 1, 4, 1]),
                                  character(_("Han Lin"), 18,
                                            sml=[special_move(_("Babao Blade"), level=10),
                                                 special_move(_("Wood Combustion Blade"), level=10)],
                                            skills=[skill(_("Basic Agility Technique"), level=10), ],
                                            equipmentList=[Equipment(_("Splinter Staff"))],
                                            preferences=[1, 1, 1, 1, 1]),
                                  character(_("Li Xiaoyang"), 18,
                                            sml=[special_move(_("Babao Blade"), level=10),
                                                 special_move(_("Ice Palm"), level=10)],
                                            skills=[skill(_("Basic Agility Technique"), level=10), ],
                                            equipmentList=[Equipment(_("Ice Shard"))],
                                            preferences=[1, 1, 1, 1, 1]),
                                  character(_("Nie Tu"), 18,
                                            sml=[special_move(_("Babao Blade"), level=10), ],
                                            skills=[skill(_("Basic Agility Technique"), level=10),
                                                    skill(_("Splitting Mountains and Earth"), level=10), ],
                                            equipmentList=[Equipment(_("Granite Boots"))],
                                            preferences=[1, 1, 1, 1, 1]),
                                  ]
        self.character_avatars = []

        for i in range(8):
            tower_char = self.tower_characters[i]
            if tower_char == _("Nie Tu") and self.game.taskProgressDic["blind_sniper"] == 7:
                pass
            else:
                frame = Frame(canvas)
                pic = PhotoImage(file=tower_char+"_icon_large.ppm")
                imageButton = Button(frame, command=partial(self.interact_tower_char, tower_char))
                imageButton.grid(row=0, column=0)
                imageButton.config(image=pic)
                label = Label(frame, text=tower_char)
                label.grid(row=1, column=0)
                frame.place(x=w*(2*(i%4)+1)//9, y=h*(2*(i//4)+1)//5)
                self.character_avatars.append(pic)


        menu_frame = self.game.create_menu_frame(self.game.babao_tower_win)
        menu_frame.pack()
        self.game.babao_tower_win_F1 = Frame(self.game.babao_tower_win)
        self.game.babao_tower_win_F1.pack()

        self.game.babao_tower_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.babao_tower_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.babao_tower_win.winfo_width(), self.game.babao_tower_win.winfo_height()
        self.game.babao_tower_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.babao_tower_win.focus_force()
        self.game.babao_tower_win.mainloop()  ###



    def check_passed_status(self, targ):
        char_index = self.tower_characters.index(targ)
        if self.game.taskProgressDic["babao_tower"] <= 0:
            return False
        else:
            str_progress = str(bin(self.game.taskProgressDic["babao_tower"])).replace("0b","")
            str_progress = (8 - len(str_progress)) * "0" + str_progress
            if len(str_progress) <= char_index:
                return False
            elif str_progress[char_index] == "0":
                return False
            else:
                return True


    def ask_nie_tu_for_help(self):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        if self.game.taskProgressDic["babao_tower"] < 1000:
            self.game.generate_dialogue_sequence(
                self.game.babao_tower_win,
                "Babao Tower Dialogue BG.png",
                [_("We've got serious business to settle first. Let's discuss other matters later.")],
                ["Nie Tu"])

        else:
            if self.game.taskProgressDic["blind_sniper"] == 2:
                self.game.taskProgressDic["blind_sniper"] = 3

                if self.game.taskProgressDic["babao_tower"] == 1000:
                    self.game.generate_dialogue_sequence(
                        self.game.babao_tower_win,
                        "Babao Tower Dialogue BG.png",
                        [_("Yes, Leader, what is it?"),
                         _("I have to find a way to sneak into the Imperial Palace and find the Empress, but the palace is so big and heavily guarded."),
                         _("You got any ideas on how to safely and successfully locate the Empress?"),
                         _("Why would you need to do that?"),
                         _("A friend of mine, the Blind Sniper, is friends with the Empress, and they need to talk..."),
                         _("WHAT??? The Blind Sniper??!!"),
                         _("Hah... I would sooner die than do anything which might benefit him!"),
                         _("Sorry, Leader... Anything else you ask, I would happily oblige, but I cannot help you this time."),
                         _("Why is that? How has he offended you?")],
                        ["Nie Tu", "you", "you", "Nie Tu", "you", "Nie Tu", "Nie Tu", "Nie Tu", "you"])

                else:
                    self.game.generate_dialogue_sequence(
                        self.game.babao_tower_win,
                        "Babao Tower Dialogue BG.png",
                        [_("What do you need help with?"),
                         _("I have to find a way to sneak into the Imperial Palace and find the Empress."),
                         _("Why would you need to do that?"),
                         _("A friend of mine, the Blind Sniper, is friends with the Empress, and they need to talk..."),
                         _("WHAT??? The Blind Sniper??!!"),
                         _("Hah... I would sooner die than do anything which might benefit him!"),
                         _("Why is that? How has he offended you?")],
                        ["Nie Tu", "you", "Nie Tu", "you", "Nie Tu", "Nie Tu", "you"])


                self.game.stopSoundtrack()
                self.game.currentBGM = "jy_xiaoyaopai.mp3"
                self.game.startSoundtrackThread()

                year_var = calculate_month_day_year(self.game.gameDate)["Year"] + 11
                self.game.generate_dialogue_sequence(
                    self.game.babao_tower_win,
                    "Babao Tower Dialogue BG.png",
                    [_("If... if it weren't for his obsession with sparring anyone that he crosses paths with, my dad would still be alive..."),
                     _("Did he... accidentally kill your father in a spar?"),
                     _("No, but he was undeniably responsible."),
                     _("Mind telling me what happened?"),
                     _("{} years ago, when I was in my early teens, my father was on his way to duel Chen Wei, one of the Three Great Evils...").format(year_var),
                     _("My father, Nie Yun, was one of the most chivalrous men of his time and had spent 3 years hunting down Chen Wei."),
                     _("They had finally agreed to duel at the Falling Rain Pavillion, but..."),
                     _("On his way there, he ran into the Blind Sniper, who insisted that they spar."),
                     _("My father refused, stating that he had urgent business to take care of."),
                     _("However, the Blind Sniper was stubborn and refused to let him go without seeing which of them was stronger."),
                     _("The spar lasted for 2 hours, resulting in a narrow victory for my dad, but the damage was already done."), #Row 2
                     _("He had spent a good amount of his energy and was not in his best shape when he dueled Chen Wei."),
                     _("That's why he... he... Even though I cannot say with confidence that my father can defeat Chen Wei"),
                     _("Based on their previous brief encounters, there's no way that villain could've killed my dad without a scratch."),
                     _("I'm sorry to hear that... Were you there to witness the fight?"),
                     _("No, I was home at the time, but this story has been passed around for years within the Martial Arts Community..."),
                     _("I think we can agree that Chen Wei is the main culprit. If I help you avenge your father, will you be willing to let go of your grudge against the Blind Sniper?"),
                     _("Don't expect me to be his friend, but if you help me slay Chen Wei with my own hands, I will help the Blind Sniper this once..."),
                     _("Deal... Any idea where he is or how we can find him?"),
                     _("Chen Wei's primary motivation for his cruel deeds is learning new, powerful techniques. Maybe we can lure him..."),
                     _("Sounds good; how do we do that?"), #Row 3
                     _("I'm going to spread word around that we'll be hosting a local tournament at Babao Tower with a grand prize of 3000 Gold."),
                     _("This should attract some attention, and if Chen Wei is nearby, he might come and observe."),
                     _("You will participate in the tournament as a 'passerby'. If you defeat everyone and win, I will come challenge you and display my Babao Blade technique."),
                     _("Afterwards, I'll pretend to be impressed by your skills and offer to share the Babao Blade Manual with you after we have dinner together."),
                     _("We will pretend to have a meal together for the first time, and then I'll take you to an inner room."),
                     _("If Chen Wei was present at the tournament, he should've overheard everything and will be following us."),
                     _("When he comes out and tries to take the decoy manual, my buddies will jump out and surround him."),
                     _("There are 9 of us. No way he can escape!"),
                     _("What if someone else comes out on top during the tournament?"),
                     _("Then... uh... we'll just have to give them the prize money, which you will provide, right?"), #Row 4
                     _("3000 Gold? Ay.... Yeah, ok, I guess that's fair."),
                     _("Brilliant. Once you have the gold, come talk to me; then I'll start announcing the tournament.")],
                    ["Nie Tu", "you", "Nie Tu", "you", "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu",
                     "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu", "you", "Nie Tu", "you", "Nie Tu", "you", "Nie Tu",
                     "you", "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu", "you",
                     "Nie Tu", "you", "Nie Tu"])

            elif self.game.taskProgressDic["blind_sniper"] == 3:
                if self.game.check_for_item(_("Gold"), 3000):
                    self.game.add_item(_("Gold"), -3000)
                    self.game.gameDate += 5
                    self.game.babao_tower_win.withdraw()
                    self.game.generate_dialogue_sequence(
                        None,
                        "Babao Tower Dialogue BG.png",
                        [_("Great! I'll start getting the word out there. The tournament will take place in 5 days!"),
                         _("*5 days later*"),
                         _("Welcome, everyone, to Babao Tower! We, the Eight Treasures of Hunan, are most honored to host this tournament!"),
                         _("Our hope is that through the exchange of martial arts techniques, we can make new friends and learn from each other."),
                         _("To give everyone a bit of motivation, the winner of this tournament will receive 3000 gold!"),
                         _("The rules are simple. Whoever defeats 3 consecutive challengers wins the tournament!"),
                         _("Now, let's begin! Who is brave enough to fight the first round?"),
                         _("Don't mind if I do!"),
                         _("Hah, little boy, let me crack your bones as a warm up...")],
                        ["Nie Tu", "Blank", "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu", "Nie Tu", "you", "Hooligan"])

                    self.local_tournament(0)

                else:
                    messagebox.showinfo("", _("You don't have the 3000 gold needed for the tournament prize."))
                    self.game.babao_tower_win.deiconify()


            elif self.game.taskProgressDic["blind_sniper"] == 8:
                self.game.taskProgressDic["blind_sniper"] = 9
                self.game.generate_dialogue_sequence(
                    self.game.babao_tower_win,
                    "Babao Tower Dialogue BG.png",
                    [_("I've got a plan! I spent some time surveying the landscape and the map of the Imperial Palace."),
                     _("I know the perfect place to dig an underground tunnel straight to the Empress's room!"),
                     _("Underground tunnel??"),
                     _("Yep, it's the safest way to avoid all the imperial guards."),
                     _("I found a lightly-guarded location with loose soil; we can start from there and dig a tunnel that's a few hundred meters long."),
                     _("Once you've gone far enough, just dig straight up and you should be able to break the floor in the Empress's Palace and jump up right into her room!"),
                     _("Wow, sounds like a crazy plan..."),
                     _("I'll distract the guards near our starting spot so you will have time to start digging without being noticed."),
                     _("Here, let me show you the map I drew..."),
                     _("*Nie Tu begins to describe in detail how and where to dig...*"),
                     _("Just let me know when you're ready to go!")],
                    ["Nie Tu", "Nie Tu", "you", "Nie Tu", "Nie Tu", "Nie Tu", "you", "Nie Tu", "Nie Tu", "Blank", "Nie Tu"])

            elif self.game.taskProgressDic["blind_sniper"] == 9:
                self.game.babao_tower_win.quit()
                self.game.babao_tower_win.destroy()
                self.game.update_map_location("imperial_city")
                self.game.gameDate += 9
                self.game.taskProgressDic["blind_sniper"] = self.game.gameDate
                self.game.generate_dialogue_sequence(
                    None,
                    "Babao Tower Dialogue BG.png",
                    [_("I'm ready!"),
                     _("Ok, follow me!"),],
                    ["you", "Nie Tu"])

                self.game.generate_dialogue_sequence(
                    None,
                    "battleground_imperial_palace_night.png",
                    [_("Ok, wait here until I distract some of the guards..."),
                     _("Eeeelephant~ Eeeeelephant~ Whyyyy is your nose so loooong~~"),
                     _("Eeeelephant... Elephant.... Whyyy.... why...."),
                     _("Hey! What are you doing here?!"),
                     _("Wha.... whoooo are you?"),
                     _("Get out! This is a forbidden area!"),
                     _("Y-yoouuuu guys look... fu... funny... Hahahaha..."),
                     _("This guy is drunk. Let's throw him in prison for a few days to sober up."),
                     _("Youuu guys wanna... p-play? Co...come catch m-me then... Ehehehehe~"), #Row 2
                     _("Hey, come back here!"),
                     _("(Now's the perfect chance!)")],
                    ["Nie Tu", "Nie Tu", "Nie Tu", "Spearmen", "Nie Tu", "Spearmen", "Nie Tu", "Spearmen",
                     "Nie Tu", "Spearmen", "you"])

                self.game.stopSoundtrack()
                self.game.currentBGM = "jy_shanguxingjin2.mp3"
                self.game.startSoundtrackThread()
                self.game.mini_game_in_progress = True
                self.game.mini_game = imperial_palace_digging_mini_game(self.game, self)
                self.game.mini_game.new()


    def post_imperial_palace_digging(self):
        self.game.mini_game_in_progress = False
        self.game.generate_dialogue_sequence(
            None,
            "battleground_imperial_palace_night.png",
            [_("I think I will go to sleep now; thank you Head Eunuch Liu. You may go now."),
             _("Rest well, Your Highness. Your servant goeth now."),
             _("*You wait in your hiding place for another 30 minutes before jumping out.*"),
             _("Your Highness! I need to speak to you."),
             _("Wh-who are you??!! Help! Assass--"),
             _("*You press on her Shui Tu Acupressure Point and her voice is silenced immediately.*"),
             _("*No matter how hard she tries to scream, no sound comes out.*"),
             _("Don't worry; I won't harm you. I have something urgent to tell you. This is the only way I could reach you."),
             _("Please forgive me, your Highness."),
             _("*After hearing your words and seeing you on your knees, the panic in her eyes subsided, though she is still visibly scared.*"),
             _("I'm going to let you speak again, but please don't scream."), #Row 2
             _("*The Empress nods in agreement, and you tap her Shui Tu Point once again.*"),
             _("What do you want? If it's gold, gems, land, whatever it is, just ask."),
             _("I don't want anything from you. I'm here to deliver a message for my friend, who happens to be an old friend of yours..."),
             _("Old friend...?"),
             _("Perhaps the name Liang Zhe will ring a bell?"),
             _("*At the mention of the Blind Sniper's name, the Empress's expression turned from fear to shock.*"),
             _("He... He's still... alive....?"),
             _("Yes, when you guys were ambushed by the assassins, he was severely injured but eventually rescued."),
             _("Due to his handicap, he had no way of reaching you, and for many years, he did not want to appear in front of you for fear that you would despise him."),
             _("But now, he has a good friend like me who is willing to help him reunite with his lover."),
             _("So come with me, Your Highness. I will take you to meet him."),
             _("Once you guys reunite, I'll escort you to a safe place so you guys can enjoy the rest of your lives together."), #Row 3
             _("I... I... Sorry, all of this is too sudden for my mind to absorb..."),
             _("Where is Liang Zhe right now, and how is he doing?"),
             _("He's blind in both eyes, but still chivalrous as ever!"),
             _("Surely you don't mind his handicap? After all, he suffered those injuries to protect you..."),
             _("N-no... Of course not... I'm glad to hear that he's ok."),
             _("Good, then let's go! I dug an underground tunnel to get here. We can escape from the palace through the same tunnel."),
             _("No! W-wait... I... I can't leave tonight."),
             _("Why not?"),
             _("The Emperor requested my presence tomorrow night and for the rest of this week. I... I have to attend to him."),
             _("If he finds that I'm gone, he will certainly order the guards to search for me."),
             _("With only a 1 day head start, we won't make it too far before getting caught."), #Row 4
             _("Hmmm, that's true. I won't have much difficulty escaping alone, but bringing you will slow us down tremendously."),
             _("What should we do then?"),
             _("I have an idea. Let me spend the night with the Emperor tomorrow."),
             _("Then, I'll arrange for his concubines to serve him the next 2-3 weeks so he will not get suspicious if he doesn't see me for a few days."),
             _("Why don't you return here 10 nights from now, and then we'll escape together."),
             _("This will give us at least a few days' head start, which should be enough to get far away from the Imperial City."),
             _("Brilliant! Sounds like a good plan. I'll take my leave for now then. Make sure no one knows about this."),
             _("Thank you, whoever you are, for helping us. You are very brave."),
             _("I'm willing to risk anything to help my friends! Anyway, I'm gonna go now. Take care!")],
            ["Empress", "Imperial Eunuch", "Blank", "you", "Empress", "Blank", "Blank", "you", "you", "Blank",
             "you", "Blank", "Empress", "you", "Empress", "you", "Blank", "Empress", "you", "you", "you", "you",
             "you", "Empress", "Empress", "you", "you", "Empress", "you", "Empress", "you", "Empress", "Empress",
             "Empress", "you", "you", "Empress", "Empress", "Empress", "Empress", "you", "Empress", "you"]
        )

        self.game.mapWin.deiconify()


    def local_tournament(self, stage):
        if stage == 0:
            opp = character(_("Hooligan"), 10,
                            sml=[special_move(_("Basic Punching Technique"), level=10)])

            self.game.battleID = "babao_tower_tournament"
            self.game.battleMenu(self.game.you, opp, "Babao Tower Dialogue BG.png",
                                 "linhai_huanqin.mp3", fromWin=self.game.babao_tower_win, battleType="task",
                                 postBattleCmd=self.local_tournament)

        elif stage == 1:
            if self.game.taskProgressDic["blind_sniper"] == 3:
                self.game.generate_dialogue_sequence(
                    self.game.babao_tower_win,
                    "Babao Tower Dialogue BG.png",
                    [_("Sorry, {}. I'm afraid you've been eliminated..."),
                     _("(Dang it, there goes my 3000 gold...)"),],
                    ["Nie Tu", "you"])

            elif self.game.taskProgressDic["blind_sniper"] == 4:
                self.game.generate_dialogue_sequence(
                    None,
                    "Babao Tower Dialogue BG.png",
                    [_("This round goes to {}! Who's next?").format(self.game.character),
                     _("Hey, I wanna have some fun, hehe~"), ],
                    ["Nie Tu", "Huang Yufei"])

                opp = character(_("Huang Yufei"), 16,
                                sml=[special_move(_("Falling Cherry Blossom Finger Technique"), level=10)],
                                skills=[skill(_("Cherry Blossoms Floating on the Water"), level=10)])

                self.game.battleID = "babao_tower_tournament"
                self.game.battleMenu(self.game.you, opp, "Babao Tower Dialogue BG.png",
                                     "linhai_huanqin.mp3", fromWin=self.game.babao_tower_win, battleType="task",
                                     postBattleCmd=self.local_tournament)

            elif self.game.taskProgressDic["blind_sniper"] == 5:
                self.game.generate_dialogue_sequence(
                    None,
                    "Babao Tower Dialogue BG.png",
                    [_("This round goes to {}! Who's next?").format(self.game.character),
                     _("I'd like to give it a try..."), ],
                    ["Nie Tu", "Cheng Youde"])

                opp = character(_("Cheng Youde"), 18,
                                sml=[special_move(_("Purity Blade"), level=10),
                                     special_move(_("Taichi Sword"), level=10)],
                                skills=[skill(_("Wudang Agility Technique"), level=10),
                                        skill(_("Pure Yang Qi Skill"), level=8)])

                self.game.battleID = "babao_tower_tournament"
                self.game.battleMenu(self.game.you, opp, "Babao Tower Dialogue BG.png",
                                     "linhai_huanqin.mp3", fromWin=self.game.babao_tower_win, battleType="task",
                                     postBattleCmd=self.local_tournament)

            elif self.game.taskProgressDic["blind_sniper"] == 6:
                self.game.add_item(_("Gold"), 3000)
                self.game.generate_dialogue_sequence(
                    None,
                    "Babao Tower Dialogue BG.png",
                    [_("Congratulations, {}! You are the winner of this tournament!").format(self.game.character),
                     _("Before I give you your reward, I'd like to show you my technique: the Babao Blade!")],
                    ["Nie Tu", "Nie Tu"])

                opp = character(_("Nie Tu"), 18,
                                sml=[special_move(_("Babao Blade"), level=10)],
                                skills=[skill(_("Basic Agility Technique"), level=10),
                                        skill(_("Splitting Mountains and Earth"), level=10), ],
                                equipmentList=[Equipment(_("Granite Boots"))],
                                preferences=[1, 1, 1, 1, 1])

                self.game.battleID = "babao_tower_tournament"
                self.game.battleMenu(self.game.you, opp, "Babao Tower Dialogue BG.png",
                                     "linhai_huanqin.mp3", fromWin=self.game.babao_tower_win, battleType="task",
                                     postBattleCmd=self.local_tournament)

            elif self.game.taskProgressDic["blind_sniper"] == 7:
                self.game.battleID = None
                self.game.generate_dialogue_sequence(
                    None,
                    "Babao Tower Dialogue BG.png",
                    [_("Brilliant! I have not had such a good fight in a long time!"),
                     _("Say, would you be interested in learning the Babao Blade?"),
                     _("You're willing to teach me? Why, I'd be most honored!"),
                     _("If I can gain a friend like you just by sharing a manual, it'll be the best trade I've ever made!"),
                     _("Everyone, thank you all for participating in this tournament. Please enjoy some refreshments afterwards."),
                     _("{}, let us have dinner together. Afterwards, I will take you to see the manual.").format(self.game.character),
                     _("Sounds good!"),
                     _("*After dinner, Nie Tu pulls you to the side and leads you up the stairs to the upper floors of the tower.*"),
                     _("*Pulling out a chest from under his bed, he opens it to reveal a book.**"),
                     _("Here it is: the Babao Blade Manual. I now lend it to you for as long as you need to learn this technique."),
                     _("Thanks, Nie Tu!"),#Row 2
                     _("Ahahahahaha! Thanks, you two... I'll take that manual, if you don't mind."),
                     _("Chen Wei!")],
                    ["Nie Tu", "Nie Tu", "you", "Nie Tu", "Nie Tu", "Nie Tu", "you", "Blank", "Blank", "Nie Tu",
                     "you", "Chen Wei", "Nie Tu"])

                self.game.stopSoundtrack()
                self.game.currentBGM = "jy_suspenseful1.mp3"
                self.game.startSoundtrackThread()

                if self.game.taskProgressDic["big_beard"] >= 0:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Babao Tower Dialogue BG.png",
                        [_("Uncle Big Beard???"),
                         _("Ahahahaha, yes, it's me, boy..."),
                         _("I... I can't believe it's you...")],
                        ["you", "Chen Wei", "you"])

                self.game.generate_dialogue_sequence(
                    None,
                    "Babao Tower Dialogue BG.png",
                    [_("Alright boys, listen up. I ain't got time to waste..."),
                     _("You hand over the manual, and maybe I'll spare you..."),
                     _("Today, you will pay for killing my father!"),
                     _("And which fellow was so unfortunate to have you as a son?"),
                     _("Nie Yun, the Chivalrous Swordsman."),
                     _("Ahhh, him... quite a formidable opponent... a long time rival of mine..."),
                     _("I'm afraid you are no match for me..."),
                     _("Then what about 8 of us together?"), #Row 2
                     _("Do you want to take your own life or are you going to force us to get physical?"),
                     _("Oh, what is this? It was all a set up, eh? Just to trap me? I am so flattered..."),
                     _("Enough, it's time to suffer the consequences of your actions!"),
                     _("HAHAHAHAHA! 8 young brats think they can kill me... Foolish fools..."),
                     _("9... You forgot about me..."),
                     _("If you all jumped in at the same time, I admit I'm not confident I can kill all of you."),
                     _("However, escaping with nothing more than a few minor scratches will not be a problem for me."), #Row 3
                     _("You've really underestimated me if you think you can kill me like this. Now hand over the manual."),
                     _("Hahahaha! What manual? You really think I'd expose the real manual like this?"),
                     _("Ok, now you're making me angry... but no worries... Soon, you will be dead, just like your father."),
                     _("You shut up!!! Die!!!"),
                     _("Wait--"),
                     _("*Rushing forward with his sword, Nie Tu lunges at Chen Wei, who side steps the attack with ease and disarms him with a tap to his Qu Chi pressure point.*"),
                     _("*Fearing for their friend's life, Lei Mengde and Zhang Andi attack Chen Wei from both sides using Thunder Palm and Fire Palm.*"),
                     _("*Just as their palms were about to make contact with Chen Wei's chest, his whole body suddenly shifts forward as though he teleported a couple of feet.*"),
                     _("*Lei Mengde and Zhang Andi tried to pull back, but it was too late.*"), #Row 4
                     _("*Their palms make contact and after a loud bang, both stagger back 5 steps with blood dripping from their lips.*"),
                     _("*Nie Tu bends down to pick up his sword while the others try to jump into the fight, but it's already too late.*"),
                     _("*Moving like a ghost, Chen Wei dashes forward and kicks Nie Tu's sword into the air.*"),
                     _("*Then with one swift motion, he grabs the sword and places it on Nie Tu's throat.*"),
                     _("Phantom Steps?? You..."),
                     _("Ah, yes, a gift from my friend Lu Xifeng... He taught me Phantom Steps many years ago."),
                     _("Now, no one better make any funny moves, or I'll paint this room with your friend's blood."),
                     _("What do you want??"), #Row 5
                     _("Hah, you know very well what I want: the Babao Blade Manual."),
                     _("Ok, the manual is not here. Just give us some time to fetch it..."),
                     _("I don't think so... Who knows what tricks you have up your sleeves? Or what kind of mechanical traps are in this tower?"),
                     _("Tell you what, I'm going to go now. You take as long as you need to bring me the manual, but don't make me lose my patience."),
                     _("When you're ready for the exchange, come find me at the Tibetan Plains."),
                     _("*Pressing the sword firmly against Nie Tu's neck, Big Beard walks out of the tower with the hostage.*"),
                     _("I'll be waiting! Hahahahaha!"),
                     _("*Gliding with ghost-like movements, Chen Wei disappears into the night with Nie Tu.*"), #Row 6
                     _("What do we do?"),
                     _("I'll go. The rest of you stay here and take care of Lei Mengde and Zhang Andi."),
                     _("They are injured, and we need to be vigilant in case Chen Wei returns and tries to ambush us."),
                     _("But Chen Wei is really dangerous... I..."),
                     _("I know; don't worry. I got this. Just keep yourselves safe, ok?"),
                     _("Thank you, {}... We'll do as you say.").format(self.game.character)],
                    ["Chen Wei", "Chen Wei", "Nie Tu", "Chen Wei", "Nie Tu", "Chen Wei", "Chen Wei",
                     "Wen Yulan", "Zhang Andi", "Chen Wei", "Li Mianzhuo", "Chen Wei", "you", "Chen Wei",
                     "Chen Wei", "Chen Wei", "Nie Tu", "Chen Wei", "Nie Tu", "you", "Blank", "Blank", "Blank",
                     "Blank", "Blank", "Blank", "Blank", "Blank", "Chen Jin", "Chen Wei", "Chen Wei",
                     "Han Lin", "Chen Wei", "Han Lin", "Chen Wei", "Chen Wei", "Chen Wei", "Blank", "Chen Wei",
                     "Blank", "Wen Yulan", "you", "you", "Li Xiaoyang", "you", "Chen Jin"])

            self.game.babao_tower_win.quit()
            self.game.babao_tower_win.destroy()
            self.game.mapWin.deiconify()


    def interact_tower_char(self, targ):
        self.game.babao_tower_win.withdraw()

        random_greeting = choice([_("Greetings!"), _("Hello there!"), _("Yes, what is it?")])
        if self.game.taskProgressDic["babao_tower"] >= 1000:
            options = [[_("Challenge"), partial(self.tower_challenge, targ)],
                       [_("Nevermind"), partial(self.tower_challenge, None)]]
        else:
            options = [[_("I am ready to make my choice."), partial(self.choose_leader, targ)],
                        [_("Challenge"), partial(self.tower_challenge, targ)],
                        [_("Nevermind"), partial(self.tower_challenge, None)]]


        if self.game.taskProgressDic["blind_sniper"] == 7:
            self.game.generate_dialogue_sequence(
                self.game.babao_tower_win,
                "Babao Tower Dialogue BG.png",
                [_("We're counting on you to bring Nie Tu back safely, but please be careful!"),
                 _("Don't worry! I'm heading over to the Tibetan Plains to find Chen Wei. You guys stay safe!")],
                [targ, "you"],
            )
        elif self.game.taskProgressDic["blind_sniper"] in [2,3,8] and targ == _("Nie Tu"):
            self.game.generate_dialogue_sequence(
                None,
                "Babao Tower Dialogue BG.png",
                [random_greeting],
                [targ],
                options + [[_("Talk about the Blind Sniper."), partial(self.ask_nie_tu_for_help)]]
            )
        elif self.game.taskProgressDic["blind_sniper"] == 9 and targ == _("Nie Tu"):
            self.game.generate_dialogue_sequence(
                None,
                "Babao Tower Dialogue BG.png",
                [random_greeting],
                [targ],
                options + [[_("I'm ready to sneak into the Imperial Palace!"), partial(self.ask_nie_tu_for_help)]]
            )
        else:
            self.game.generate_dialogue_sequence(
                None,
                "Babao Tower Dialogue BG.png",
                [random_greeting],
                [targ],
                options
            )


    def tower_challenge(self, targ):

        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        if not targ:
            self.game.babao_tower_win.deiconify()
            return


        #self.game.dialogueWin.quit()
        #self.game.dialogueWin.destroy()
        if self.check_passed_status(targ):
            self.game.generate_dialogue_sequence(
                None,
                "Babao Tower Dialogue BG.png",
                [_("You've already claimed the reward for passing my level."),
                 _("However, if you want to try the challenge again, be my guest.")],
                [targ, targ]
            )

        targ_index = self.tower_characters.index(targ)
        element = self.tower_elements[targ_index]
        response = messagebox.askquestion("", _("Challenge {} ({})?").format(targ, element))
        if response == "yes":
            if targ in [_("Li Mianzhuo"), _("Lei Mengde")]:
                self.game.stopSoundtrack()
                self.game.currentBGM = "sfx_rain_3.mp3"
                self.game.startSoundtrackThread()
            elif targ != _("Wen Yulan"):
                self.game.stopSoundtrack()
                self.game.currentBGM = "jy_battle6.mp3"
                self.game.startSoundtrackThread()

            challenge_dialogue = [_("Get ready... You're about to get knocked off of your feet..."),
                                  _("The challenge is simple. If you can endure 200 strikes of my Fire Palm, you pass."),
                                  _("I hope you're prepared for this..."),
                                  _("Let's do it! I won't go easy on you!"),
                                  _("Stay alert! My mechanical traps don't show mercy."),
                                  _("Think you've got what it takes to pass my challenge? Bring it!"),
                                  _("Water... but not in liquid form..."),
                                  _("The solid ground on which you stand will soon turn into sinking sand...")]

            self.game.generate_dialogue_sequence(
                None,
                "Babao Tower Dialogue BG.png",
                [challenge_dialogue[targ_index]],
                [targ]
            )
            self.game.mini_game_in_progress = True
            self.game.mini_game = self.tower_mini_games[targ_index](self.game, self)
            self.game.mini_game.new()

        else:
            self.game.babao_tower_win.deiconify()


    def post_final_tower_challenge(self):
        self.game.mini_game_in_progress = False
        self.game.generate_dialogue_sequence(
            None,
            "Babao Tower Dialogue BG.png",
            [_("In-incredible!"),
             _("But it's not over yet!"),
             _("Yeah, you still have to get past us..."),
             _("You can rest for a bit first."),
             _("Just to make it fair."),
             _("Then, we fight..."),],
            ["Wen Yulan", "Zhang Andi", "Lei Mengde", "Li Mianzhuo", "Li Xiaoyang", "Nie Tu"]
        )
        self.game.restore(0, 0, full=True)
        self.game.battleMenu(self.game.you, self.all_possible_opps, "Babao Tower Dialogue BG.png", "linhai_huanqin.mp3",
                             fromWin=self.game.babao_tower_win, battleType="task", postBattleCmd=self.post_final_tower_battle)


    def post_final_tower_battle(self):
        self.game.taskProgressDic["babao_tower"] = 1000
        self.game.generate_dialogue_sequence(
            self.game.babao_tower_win,
            "Babao Tower Dialogue BG.png",
            [_("It's meant to be..."),
             _("I have nothing to say. Your combat skills are far above ours."),
             _("Yeah, I definitely have no more objections."),
             _("As our leader, you'll need to learn the Babao Blade technique!"),
             _("Yeah, and also, the prized treasure of our sect, the Babao Blade, belongs to you now."),
             _("Wow, I am most honored! Don't worry guys, I'll make sure we become the best clan in the land!"),],
            ["Wen Yulan", "Chen Jin", "Han Lin", "Li Mianzhuo", "Li Xiaoyang", "you"]
        )

        self.game.learn_move(special_move(_("Babao Blade")))
        self.game.add_item(_("Babao Blade"))
        messagebox.showinfo("", _("Received 'Babao Blade' x 1! You learned a new move: Babao Blade!"))



    def post_tower_battle(self, choice):
        targ = self.tower_characters[choice]
        rewards = [_("Wings of the Dragonfly"), _("Flame Blade"), _("Thunder Blade"), _("Lightning Saber"),
                   _("Hardened Steel Platebody"), _("Splinter Staff"), _("Ice Shard"), _("Granite Boots")]
        self.game.generate_dialogue_sequence(
            self.game.babao_tower_win,
            "Babao Tower Dialogue BG.png",
            [_("Ha! What have you guys to say now?"),
             _("It is clear that {}'s choice is most reliable!").format(self.game.character),
             _("Hehehehe..."),
             _("It is decided then; from now on, I am the leader of Babao Sect, the greatest Sect in all of Wulin!"),
             _("{}, please accept this gift for helping us settle this troublesome matter!").format(self.game.character)],
            [targ, targ, "you", targ, targ]
        )
        reward = rewards[choice]
        self.game.add_item(reward)
        messagebox.showinfo("", _("Received '{}' x 1!").format(reward))


    def post_tower_challenge(self, targ):
        self.game.mini_game_in_progress = False
        self.game.babao_tower_win.deiconify()

        char_index = self.tower_characters.index(targ)
        str_progress = str(bin(self.game.taskProgressDic["babao_tower"])).replace("0b", "")
        str_progress = (8-len(str_progress))*"0" + str_progress
        updated_str_progress = ""
        for i in range(len(str_progress)):
            if i == char_index:
                updated_str_progress += "1"
            else:
                updated_str_progress += str_progress[i]

        self.game.taskProgressDic["babao_tower"] = int(updated_str_progress, 2)
        self.game.generate_dialogue_sequence(
            self.game.babao_tower_win,
            "Babao Tower Dialogue BG.png",
            [_("Passing my level is no ordinary feat. You should be proud of yourself!"),
             _("You can try the others if you'd like, but none of them will provide the same level of challenge that I did...")],
            [targ, targ]
        )

        #, _("Zhang Andi"), _("Lei Mengde"), _("Li Mianzhuo"),
        #_("Chen Jin"), _("Han Lin"), _("Li Xiaoyang"), _("Nie Tu")
        if str_progress[char_index] != "1":
            if targ == _("Wen Yulan"):
                new_move = special_move(_("Whirlwind Staff"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("As your reward for passing her level, Wen Yulan teaches you a new move: Whirlwind Staff!"))
            elif targ == _("Zhang Andi"):
                new_move = special_move(_("Fire Palm"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("As your reward for passing his level, Zhang Andi teaches you a new move: Fire Palm!"))
            elif targ == _("Lei Mengde"):
                new_move = special_move(_("Thunder Palm"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("As your reward for passing his level, Lei Mengde teaches you a new move: Thunder Palm!"))
            elif targ == _("Li Mianzhuo"):
                new_move = special_move(_("Lightning Finger Technique"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("As your reward for passing his level, Li Mianzhuo teaches you a new move: Lightning Finger Technique!"))
            elif targ == _("Chen Jin"):
                new_move = special_move(_("Iron Sand Palm"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("As your reward for passing his level, Chen Jin teaches you a new move: Iron Sand Palm!"))
            elif targ == _("Han Lin"):
                new_move = special_move(_("Wood Combustion Blade"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("As your reward for passing his level, Han Lin teaches you a new move: Wood Combustion Blade!"))
            elif targ == _("Li Xiaoyang"):
                if _("Ice Palm") in [m.name for m in self.game.sml]:
                    self.game.generate_dialogue_sequence(
                        self.game.babao_tower_win,
                        "Babao Tower Dialogue BG.png",
                        [_("Hmmmm, I see that you've already learned 'Ice Palm'! Did you learn it from someone named Xiao Yong?"),
                         _("Yep, why? Do you know him too?"),
                         _("Yep, he was my mentor for a brief period of time when I was still a teenager."),
                         _("How did you guys meet?"),
                         _("Well, actually, all 8 of us know Xiao Yong. He used to come visit our village periodically."),
                         _("So you grew up on the West side? Near the Snowy Mountains?"),
                         _("That's right! Anyway, ever since he taught me Ice Palm, I've been able to derive another technique."),
                         _("It builds upon the core methods of Ice Palm, so the more proficient your Ice Palm is, the stronger this technique will be.")],
                        [targ, "you", targ, "you", targ, "you", targ, targ]
                    )
                    new_move = special_move(_("Ice Burst"))
                    self.game.learn_move(new_move)
                    messagebox.showinfo("", _("Li Xiaoyang teaches you a new move: Ice Burst!"))
                else:
                    new_move = special_move(_("Ice Palm"))
                    self.game.learn_move(new_move)
                    messagebox.showinfo("", _("As your reward for passing his level, Li Xiaoyang teaches you a new move: Ice Palm!"))
            elif targ == _("Nie Tu"):
                new_skill = skill(_("Splitting Mountains and Earth"))
                self.game.learn_skill(new_skill)
                messagebox.showinfo("", _("As your reward for passing his level, Nie Tu teaches you a new skill: Splitting Mountains and Earth!"))


        else:
            r = randrange(1, self.game.luck//50+1)
            if randrange(2) == 0:
                self.game.speed += r
                messagebox.showinfo("", _("Your speed increased by {}!").format(r))
            else:
                self.game.dexterity += r
                messagebox.showinfo("", _("Your dexterity increased by {}!").format(r))


    def choose_leader(self, targ, choice=-1):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        options = [_("Wen Yulan (Wind)"),
                   _("Zhang Andi (Fire)"),
                   _("Lei Mengde (Thunder)"),
                   _("Li Mianzhuo (Lightning)"),
                   _("Chen Jin (Metal)"),
                   _("Han Lin (Wood)"),
                   _("Li Xiaoyang (Water)"),
                   _("Nie Tu (Earth)"),
                   _("I should be your leader!"),
                   _("I need some more time to decide.")]


        print(choice)
        if choice == -1:
            self.game.generate_dialogue_sequence(
                self.game.babao_tower_win,
                "Babao Tower Dialogue BG.png",
                [_("Ah, you're ready? Great!"),
                 _("Tell us, then, who should be the leader?"),
                 ],
                [targ, targ],
                [[options[i], partial(self.choose_leader, targ, i)] for i in range(len(options))],
            )

        elif choice in range(0,8):
            self.game.taskProgressDic["babao_tower"] = (1+choice)*1000
            chosen_targ = options[choice].split(" (")[0]
            self.game.generate_dialogue_sequence(
                None,
                "Babao Tower Dialogue BG.png",
                [_("Brilliant choice, {}!").format(self.game.character),
                 _("Hold on a second!"),
                 _("You must've made a mistake! There's no way {} can be leader...").format(chosen_targ),
                 _("Sorry, {}, but it seems like a few of us don't agree with your decision...").format(self.game.character),
                 _("If you want to convince us that you're worthy to make that decision, then we'll have to test your skills in combat."),
                 _("I gladly accept...")
                 ],
                [chosen_targ, options[(choice+1)%8].split(" (")[0], options[(choice+2)%8].split(" (")[0],
                 options[(choice+3)%8].split(" (")[0], options[(choice+4)%8].split(" (")[0],"you"],
            )

            opp_list = [self.all_possible_opps[(choice+i)%8] for i in range(1,5)]
            cmd = lambda: self.post_tower_battle(choice)
            self.game.battleMenu(self.game.you, opp_list, "Babao Tower Dialogue BG.png", "linhai_huanqin.mp3",
                                 fromWin=self.game.babao_tower_win, battleType="task", postBattleCmd=cmd)


        elif choice == 8:
            if self.game.taskProgressDic["babao_tower"] == 255:  # check if already passed all levels
                self.game.generate_dialogue_sequence(
                    None,
                    "Babao Tower Dialogue BG.png",
                    [_("What a bold request..."),
                     _("Don't think that you're stronger than all of us just because you passed each of our challenges."),
                     _("Yeah, if we fought, who knows what will happen?"),
                     _("If you really want to convince us that you are worthy, you'll have to do more than that."),
                     _("Oh yeah? Like what?"),
                     _("When we first built this tower, we designed a special challenge room."),
                     _("Didn't think it would actually come in use but..."),
                     _("If you can pass it, then we'll consider making you our leader."),
                     _("Hahahaha! Think I'd be afraid of 1 more challenge? Pshhhhh... Let's go!")
                     ],
                    ["Wen Yulan", "Chen Jin", "Han Lin", "Chen Jin", "you", "Nie Tu", "Nie Tu", "Li Mianzhuo", "you"],
                )
                self.game.mini_game_in_progress = True
                self.game.mini_game = babao_tower_final_mini_game(self.game, self)
                self.game.mini_game.new()
            else:
                self.game.generate_dialogue_sequence(
                    self.game.babao_tower_win,
                    "Babao Tower Dialogue BG.png",
                    [_("Hahahahaha! What a joke!"),
                     _("You haven't even passed all our challenges yet. Why should you be our leader?"),
                     ],
                    [targ, targ],
                )

        elif choice == 9:
            self.game.babao_tower_win.deiconify()