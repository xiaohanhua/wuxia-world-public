from tkinter import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
from mini_game_huang_yuwei_birthday_maze import*
import tkinter.messagebox as messagebox

_ = gettext

class Scene_bamboo_forest:
    def __init__(self, game):
        self.game = game

        #self.mini_game_in_progress = True
        #self.mini_game = huang_yuwei_birthday_maze_mini_game(self.game, self)
        #self.mini_game.new()

        if self.game.taskProgressDic["xiao_han_jia_wen"] == -1 and self.game.taskProgressDic["supreme_leader"] != 1:
            self.game.generate_dialogue_sequence(
                None,
                "Bamboo Forest.png",
                [_("Big Sister, am I going to die soon?"),
                 _("Don't say that, Xiao Mei. You're going to be ok..."),
                 _("As long as I'm here, I'll do whatever it takes to help you get better."),
                 _("Hey there, what's going on?"),
                 _("Who is this Big Brother?"),
                 _("(Aha! She called me 'Big Brother' and not 'Uncle'! I like this kid already.)"),
                 _("My name is {}. I'm just a traveler who happened to pass by.").format(self.game.character),
                 _("Little Sister, are you ill?"),
                 _("Nice to meet you, {}. My name is Jia Wen, and this is Xiao Mei.").format(self.game.character),
                 _("Her parents abandoned her because they are too poor to treat her illness."),
                 _("I found her lying on the ground by the side of the road and adopted her."),
                 _("What's wrong with her?"),
                 _("She has pneumonia, and if not treated soon, she...")],
                ["Xiao Mei", "Jia Wen", "Jia Wen", "you", "Xiao Mei", "you", "you", "you",
                 "Jia Wen", "Jia Wen", "Jia Wen", "you", "Jia Wen"],
                [[_("I'll help you!"), partial(self.save_xiao_mei, 1)],
                 [_("I'd help you, but I'm busy with something..."), partial(self.save_xiao_mei, 2)]]
            )

        else:
            if random() <= .33 and self.game.taskProgressDic["xiao_han_jia_wen"] == 0:
                self.game.generate_dialogue_sequence(
                    None,
                    "Bamboo Forest.png",
                    [_("My... my head hurts... *cough*"),
                     _("*cough* *cough* *cough*"),
                     _("I... *cough* ..."),
                     _("Hang in there, Xiao Mei!"),
                     _("*cough* *cough*"),
                     _("Xiao Mei! Are you ok? Xiao--"),
                     _("..............."),
                     _("Xiao Mei..."),
                     _("You poor child... rest in peace..."),
                     _("...............................")
                     ],
                    ["Xiao Mei", "Xiao Mei", "Xiao Mei", "Jia Wen", "Xiao Mei", "Jia Wen",
                     "Jia Wen", "Jia Wen", "Jia Wen", "you"]
                )
                self.game.taskProgressDic["xiao_han_jia_wen"] = 20  # Xiao Mei dies

            self.game.bamboo_forestWin = Toplevel(self.game.mapWin)
            self.game.bamboo_forestWin.title(_("Bamboo Forest"))

            bg = PhotoImage(file="Bamboo Forest.png")
            w = bg.width()
            h = bg.height()

            canvas = Canvas(self.game.bamboo_forestWin, width=w, height=h)
            canvas.pack()
            canvas.create_image(0, 0, anchor=NW, image=bg)

            if self.game.taskProgressDic["supreme_leader"] != 1:
                self.game.bamboo_forestF1 = Frame(canvas)
                pic1 = PhotoImage(file="Jia Wen_icon_large.ppm")
                imageButton1 = Button(self.game.bamboo_forestF1, command=self.clickedOnJiaWen)
                imageButton1.grid(row=0, column=0)
                imageButton1.config(image=pic1)
                label = Label(self.game.bamboo_forestF1, text=_("Jia Wen"))
                label.grid(row=1, column=0)
                self.game.bamboo_forestF1.place(x=w // 4 - pic1.width() // 2, y=h // 4 - pic1.height() // 2)

            if self.game.taskProgressDic["xiao_han_jia_wen"] != 20 and self.game.taskProgressDic["supreme_leader"] != 1:
                self.game.bamboo_forestF2 = Frame(canvas)
                pic2 = PhotoImage(file="Xiao Mei_icon_large.ppm")
                imageButton2 = Button(self.game.bamboo_forestF2, command=self.clickedOnXiaoMei)
                imageButton2.grid(row=0, column=0)
                imageButton2.config(image=pic2)
                label = Label(self.game.bamboo_forestF2, text=_("Xiao Mei"))
                label.grid(row=2, column=0)
                self.game.bamboo_forestF2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 4 - pic2.height() // 2)

            self.game.bamboo_forest_win_F1 = Frame(self.game.bamboo_forestWin)
            self.game.bamboo_forest_win_F1.pack()

            menu_frame = self.game.create_menu_frame(self.game.bamboo_forestWin)
            menu_frame.pack()

            self.game.bamboo_forestWin.protocol("WM_DELETE_WINDOW", self.game.on_exit)
            self.game.bamboo_forestWin.update_idletasks()
            toplevel_w, toplevel_h = self.game.bamboo_forestWin.winfo_width(), self.game.bamboo_forestWin.winfo_height()
            self.game.bamboo_forestWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
            self.game.bamboo_forestWin.focus_force()
            self.game.bamboo_forestWin.mainloop()###


    def save_xiao_mei(self, choice):

        if self.game.taskProgressDic["xiao_han_jia_wen"] == -1:
            self.game.taskProgressDic["xiao_han_jia_wen"] = 0

        if choice == 1:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.chivalry += 20
            messagebox.showinfo("", _("Your chivalry +20!"))

            self.game.generate_dialogue_sequence(
                None,
                "Bamboo Forest.png",
                [_("Really? If that's the case, then there's hope for Xiao Mei!"),
                 _("Of course! Just tell me what I need to do."),
                 _("Please go West from here to a place called Nongyuan Valley."),
                 _("The renowned doctor Su Ling makes all kinds of medicine. The one Xiao Mei needs is called the Snow Lotus Pill."),
                 _("If you bring back one of these for her, she will recover in a week!"),
                 _("Got it! I'll get those right away!"),
                 _("Thank you so much!")],
                ["Jia Wen", "you", "Jia Wen", "Jia Wen", "Jia Wen", "you", "Jia Wen"]
            )
            self.game.taskProgressDic["xiao_han_jia_wen"] = 10

        else:
            if choice == 0:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()

            elif choice == 2:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.taskProgressDic["xiao_han_jia_wen"] = 0
                self.game.generate_dialogue_sequence(
                    None,
                    "Bamboo Forest.png",
                    [_("That's ok, I understand; I can't expect you to help random strangers."),
                     _("Well, best of luck to you. Hope she gets better...")],
                    ["Jia Wen", "you"]
                )
                self.game.chivalry -= 20
                messagebox.showinfo("", _("Your chivalry -20!"))


            if choice == 3:
                self.game.taskProgressDic["xiao_han_jia_wen"] = 14
                self.game.generate_dialogue_sequence(
                    self.game.bamboo_forestWin,
                    "Bamboo Forest.png",
                    [_("I've got the Snow Lotus Pill! Quick, give it to Xiao Mei!"),
                     _("Xiao Mei, here, take this..."),
                     _("There... now just rest and you should be fine in a few days."),
                     _("Thank you Big Brother!"),
                     _("Ahaha... don't mention it; I'm just glad you're fine now.")],
                    ["you", "Jia Wen", "Jia Wen", "Xiao Mei", "you"]
                )


        try:
            self.game.bamboo_forestWin.deiconify()
        except:
            self.__init__(self.game)


    def clickedOnJiaWen(self):

        if self.game.taskProgressDic["xiao_han_jia_wen"] == 13 and self.game.inv[_("Snow Lotus Pill")] > 0:
            self.game.inv[_("Snow Lotus Pill")] -= 1
            self.save_xiao_mei(3)

        elif self.game.taskProgressDic["xiao_han_jia_wen"] < 13 and self.game.taskProgressDic["xiao_han_jia_wen"] >= 10:
            self.game.generate_dialogue_sequence(
                self.game.bamboo_forestWin,
                "Bamboo Forest.png",
                [_("Xiao Mei's fate is now in your hands. She's depending on you!"),
                 _("Please go quickly and find Doctor Su in Nongyuan Valley!")],
                ["Jia Wen", "Jia Wen"]
            )

        elif self.game.taskProgressDic["xiao_han_jia_wen"] == 14:
            if self.game.taskProgressDic['wu_hua_que'] == 0:
                self.game.generate_dialogue_sequence(
                    self.game.bamboo_forestWin,
                    "Bamboo Forest.png",
                    [_("Oh by the way, some guy that I met while looking for the snow lotus wanted me to give this flute to you..."),
                     _("His name is Xiao Yong..."),
                     _("X-Xiao Yong... You... You really saw him?"),
                     _("Yep, he's doing great and wanted me to say hi to you for him."),
                     _("Thank you, {}. I don't know how to repay you...").format(self.game.character),
                     _("Well, now that you've brought that up, I think a ---")],
                    ["you", "you", "Jia Wen", "you", "Jia Wen", "you"]
                )

                self.game.stopSoundtrack()
                self.game.currentBGM = "jy_suspenseful1.mp3"
                self.game.startSoundtrackThread()
                self.game.bamboo_forestWin.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Bamboo Forest.png",
                    [_("Ah ha! Finally found you, beautiful~"),
                     _("(Dang it... Who's this weirdo? Just as I was about to ask for my compensation...)"),
                     _("Wu Hua Que... Can't you leave me alone?"),
                     _("I already told you I'm not interested."),
                     _("Ahahaha, come on, darling, I know you are just playing hard to get..."),
                     _("No worries, I can be quite persistent... No woman can escape my grasp~"),
                     _(".........."),
                     _("From the moment I saw you in that restaurant a week ago, I knew you were mine!"),
                     _("Come on, how long must you make me wait?"),
                     _("Wait, Jia Wen, who is this guy?"),
                     _("Wu Hua Que, the Flower Staining Sparrow..."),
                     _("A well-known pervert whose fighting abilities are mediocre but has incredible agility..."),
                     _("Though he's unable to defeat me, I can't do anything to him either if all he does is run and dodge.")],
                    ["Wu Hua Que", "you", "Jia Wen", "Jia Wen", "Wu Hua Que", "Wu Hua Que", "Jia Wen",
                    "Wu Hua Que", "Wu Hua Que", "you", "Jia Wen", "Jia Wen", "Jia Wen"
                     ],
                    [[_("I'll make sure he stops harrassing you!"), partial(self.wu_hua_que_decision, 1)],
                     [_("I'd better stay out of this."), partial(self.wu_hua_que_decision, 2)]]
                )

            else:
                self.game.bamboo_forestWin.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Bamboo Forest.png",
                    [_("So anyway, like I was saying. Here's the bamboo flute from Xiao Yong.")],
                    ["you"]
                )
                self.game.inv[_("Bamboo Flute")] = 0
                messagebox.showinfo("", _("You give the bamboo flute to Jia Wen."))
                self.game.generate_dialogue_sequence(
                    self.game.bamboo_forestWin,
                    "Bamboo Forest.png",
                    [_("Thank you, {}!").format(self.game.character),
                     _("Can you do me another favor and bring this gold necklace to him, please?"),
                     _("Yeah, no problem!")],
                    ["Jia Wen", "Jia Wen", "you"]
                )
                self.game.inv[_("Gold Necklace")] = 1
                self.game.taskProgressDic['xiao_han_jia_wen'] = 15
                messagebox.showinfo("", _("Received Gold Necklace x 1!"))

        else:
            for widget in self.game.bamboo_forest_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.bamboo_forest_win_F1, text=_("Spar"),
                   command=partial(self.spar_with_jia_wen, 12)).grid(row=1, column=0)
            Button(self.game.bamboo_forest_win_F1, text=_("Talk"),
                   command=self.talkToJiaWen).grid(row=2, column=0)
            Button(self.game.bamboo_forest_win_F1, text=_("Ask for task"),
                   command=partial(self.talkToJiaWen, True)).grid(row=3, column=0)


    def talkToJiaWen(self, task=False):
        if task and self.game.taskProgressDic["huang_yuwei_birthday"] == -1:
            self.game.taskProgressDic["huang_yuwei_birthday"] = 0
            self.game.generate_dialogue_sequence(
                self.game.bamboo_forestWin,
                "Bamboo Forest.png",
                [_("Huang Yuwei, Huang Xiaodong's older daughter, is having a birthday celebration soon."),
                 _("I need to prepare a birthday present for her. Would you like to help?"),
                 _("Sure! What do you need?"),
                 _("I need a Chrysocolla and Amethyst. Both are rare gems and extremely hard to find."),
                 _("Hmmm, let me look for them."),
                 _("Thanks!")],
                ["Jia Wen", "Jia Wen", "you", "Jia Wen", "you", "Jia Wen"]
            )

        elif task and self.game.taskProgressDic["huang_yuwei_birthday"] == 0:
            if self.game.check_for_item(_("Chrysocolla")) and self.game.check_for_item(_("Amethyst")):
                self.game.bamboo_forestWin.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Bamboo Forest.png",
                    [_("I've got the gems!"),
                     _("Perfect! Thank you so much, {}!").format(self.game.character),
                     _("Since you helped me, why don't you come with me to the celebration?"),
                     _("Awesome, I would love to!"),
                     _("Great, let's go!"),],
                    ["you", "Jia Wen", "Jia Wen", "you", "Jia Wen", ]
                )
                self.game.stopSoundtrack()
                self.game.currentBGM = "xlfd_cheerful.mp3"
                self.game.startSoundtrackThread()
                self.game.add_item(_("Chrysocolla"), -1)
                self.game.add_item(_("Amethyst"), -1)
                self.game.generate_dialogue_sequence(
                    None,
                    "cherry_blossom_island.png",
                    [_("Welcome, everyone, to the celebration for my daughter's 16th birthday!"),
                     _("We are honored to have you all join us."),
                     _("Please, make yourselves at home and enjoy the food and wine!"),
                     _("Yuwei! You've grown into a beautiful young lady in just a few months!"),
                     _("Jia Wen-jie~~~ I've missed you so much!!!")],
                    ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "Jia Wen", "Huang Yuwei"]
                )

                if self.game.taskProgressDic["join_sect"] == 100:
                    self.game.generate_dialogue_sequence(
                        None,
                        "cherry_blossom_island.png",
                        [_("And {}!!! Hey! Glad you could come too!").format(self.game.character),
                         _("Hehehe, happy birthday Yuwei!"),
                         _("Hehehe thanks!"),
                         _("I brought you this necklace made from Chrysocolla and Amethyst! Hope you like it!"),
                         _("{} spent a lot of time looking for these gems! He deserves most of the credit.").format(self.game.character),
                         _("Awww thank you guys so much!~"),
                         _("Hehehehehe..."), #Row 2
                         _("Alright guys, don't just stand there! Come have a seat and have some food!"),
                         _("That's what I love to hear!")],
                        ["Huang Yuwei", "you", "Huang Yuwei", "Jia Wen", "Jia Wen", "Huang Yuwei",
                         "you", "Huang Yuwei", "you"]
                    )
                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "cherry_blossom_island.png",
                        [_("I brought you this necklace made from Chrysocolla and Amethyst! Hope you like it!"),
                         _("{} spent a lot of time looking for these gems! He deserves most of the credit.").format(self.game.character),
                         _("Awww thank you guys so much!~"),
                         _("Hehehe, happy birthday!"),
                         _("Alright guys, don't just stand there! Come have a seat and have some food!"),
                         _("That's what I love to hear!")],
                        ["Jia Wen", "Jia Wen", "Huang Yuwei", "you", "Huang Yuwei", "you"]
                    )

                self.game.restore(0,0,full=True)
                self.game.generate_dialogue_sequence(
                    None,
                    "cherry_blossom_island.png",
                    [_("*2 Hours Later*"),
                     _("Thank you, everyone, for coming from afar to celebrate my daughter's birthday."),
                     _("It is a great honor to have so many respected guests."),
                     _("Now that you've all eaten and drunk to your heart's desire, let us have some fun with a couple of challenges for you all."),
                     _("If you participate, there's a chance to win some prizes. Even if you don't do well, I will provide consolation prizes for everyone."),
                     _("Now, let's begin with the first challenge..."),
                     _("Participants will be split into groups based on age range."), #Row 2
                     _("They will then spar each other, and the winner of each group proceeds to the next round."),
                     _("I'm in!"),
                     _("This sounds fun!"),
                     _("I guess it's a good opportunity to see if I've improved from my training."),
                     _("Brilliant, let us begin!")
                     ],
                    ["Blank", "Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong",
                     "Huang Xiaodong", "Huang Xiaodong", "you", "Xu Qing", "Xu Jun", "Huang Xiaodong"]
                )

                opp_list = []
                opp = character(_("Xu Qing"), 8,
                                sml=[special_move(_("Shaolin Luohan Fist"), level=3),
                                     special_move(_("Basic Punching Technique"), min([10, self.game.level]))],
                                skills=[skill(_("Shaolin Mind Clearing Method"), level=min([self.game.level, 4])),
                                        skill(_("Shaolin Inner Energy Technique"), level=4)])
                opp_list.append(opp)
                opp = character(_("Xu Jun"), 10,
                                sml=[special_move(_("Taichi Fist"), level=min([8, self.game.level]))],
                                skills=[skill(_("Wudang Agility Technique"), level=4),
                                        skill(_("Pure Yang Qi Skill"), level=4)])
                opp_list.append(opp)

                self.game.taskProgressDic["huang_yuwei_birthday"] = 1
                self.game.battleID = "huang_yuwei_birthday_challenge_battle"
                cmd = self.post_birthday_party_spar
                self.game.battleMenu(self.game.you, opp_list, "battleground_cherry_blossom_island.png",
                                     "xlfd_cheerful.mp3", fromWin=None,
                                     battleType="training", destinationWinList=[], postBattleCmd=cmd)


            else:
                self.game.generate_dialogue_sequence(
                    self.game.bamboo_forestWin,
                    "Bamboo Forest.png",
                    [_("Have you found the Chrysocolla and Amethyst?"),
                     _("Not yet; still working on it.")],
                    ["Jia Wen", "you"]
                )


        else:
            r = randrange(4)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.bamboo_forestWin,
                    "Bamboo Forest.png",
                    [_("Huang Xiaodong, who resides in Cherry Blossom Island, is very hospitable."),
                     _("I was invited on the island for a couple of months. He has 2 daughters, both of whom are very cute."),
                     _("You should go there to check it out when you get a chance.")],
                    ["Jia Wen", "Jia Wen", "Jia Wen"]
                )

            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.bamboo_forestWin,
                    "Bamboo Forest.png",
                    [_("There are many interesting people in the Martial Arts Community."),
                     _("I once met a blind man who uses a very powerful sword technique."),
                     _("He calls himself the 'Blind Sniper'.")],
                    ["Jia Wen", "Jia Wen", "Jia Wen"]
                )

            elif r == 2:
                self.game.generate_dialogue_sequence(
                    self.game.bamboo_forestWin,
                    "Bamboo Forest.png",
                    [_("I heard many people in Xiao Mei's village and the surrounding area also got sick from pneumonia."),
                     _("Many of them are on their way to see Doctor Su for help.")],
                    ["Jia Wen", "Jia Wen"]
                )

            elif r == 3:
                self.game.generate_dialogue_sequence(
                    self.game.bamboo_forestWin,
                    "Bamboo Forest.png",
                    [_("Be careful who you interact with."),
                     _("Some wicked people will offer lucrative rewards for you to do their bidding."),
                     _("Don't fall into temptation!")],
                    ["Jia Wen", "Jia Wen", "Jia Wen"]
                )



    def post_birthday_party_maze(self, cherry_blossoms_collected):
        self.game.mini_game_in_progress = False
        pg.display.quit()
        self.game.generate_dialogue_sequence(
            None,
            "cherry_blossom_island.png",
            [_("Nicely done! For the last challenge, you'll have to spar me."),
             _("Wait what??"),
             _("Hahahaha... Don't worry. I definitely won't use my full strength against you."),
             _("This will just be a friendly spar."),
             _("Alright then, please go easy on me!")],
            ["Huang Xiaodong", "you", "Huang Xiaodong", "Huang Xiaodong", "you"]
        )
        opp = character(_("Huang Xiaodong"), 20-cherry_blossoms_collected//2,
                        sml=[special_move(_("Falling Cherry Blossom Finger Technique"), level=10),
                             special_move(_("Cherry Blossom Drifting Snow Blade"), level=10)],
                        skills=[skill(_("Cherry Blossoms Floating on the Water"), level=10)])

        self.game.battleID = "huang_yuwei_birthday_challenge_battle"
        cmd = self.post_birthday_party_spar
        self.game.battleMenu(self.game.you, opp, "battleground_cherry_blossom_island.png",
                             "xlfd_cheerful.mp3", fromWin=None,
                             battleType="training", destinationWinList=[], postBattleCmd=cmd)


    def post_birthday_party_spar(self):

        if self.game.taskProgressDic["huang_yuwei_birthday"] == -20:
            self.game.add_item(_("Cherry Blossom Papaya Tremella Soup"), 5)
            self.game.generate_dialogue_sequence(
                None,
                "cherry_blossom_island.png",
                [_("Ahhh dang, as expected, I am no match, even when you are holding back!"),
                 _("Don't be discouraged; keep training; I see some potential in you."),
                 _("Here, why don't you have some of this special soup my wife made? It's very healthy and should restore your stamina quickly."),
                 _("Thanks, Master Huang!")],
                ["you", "Huang Xiaodong", "Huang Xiaodong", "you"]
            )
            messagebox.showinfo("", _("Received 'Cherry Blossom Papaya Tremella Soup' x 5!"))

        elif self.game.taskProgressDic["huang_yuwei_birthday"] == -10:
            self.game.generate_dialogue_sequence(
                None,
                "cherry_blossom_island.png",
                [_("Dang it... No hope for me..."),
                 _("I guess I can only spectate now..."),
                 _("*1 Hour Later*"),
                 _("Yes! I did it!!!"),
                 _("Excellent, young man! You have some real potential! Let me teach you something as your reward."),
                 _("Thank you, Master Huang!")],
                ["you", "you", "Blank", "Xu Jun", "Huang Xiaodong", "Xu Jun"]
            )


        elif self.game.taskProgressDic["huang_yuwei_birthday"] == 1:
            self.game.taskProgressDic["huang_yuwei_birthday"] = 10
            self.game.generate_dialogue_sequence(
                None,
                "cherry_blossom_island.png",
                [_("Well done, {}. For the next challenge, you will have to enter into a maze that I've designed and collect some cherry blossoms.").format(self.game.character),
                 _("Maze? But I've been through the maze before, so this will be too easy, no?"),
                 _("Hahahaha... This one will be quite different from the one that you had to get through to come to this island."),
                 _("You'll see what I mean. I've placed 10 distinct cherry blossoms in the maze for you to collect."),
                 _("You don't have to find all of them, but collect as many as you can."),
                 _("No problem; let's go!")],
                ["Huang Xiaodong", "you", "Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "you"]
            )

            self.game.mini_game_in_progress = True
            self.game.mini_game = huang_yuwei_birthday_maze_mini_game(self.game, self)
            self.game.mini_game.new()
            return


        elif self.game.taskProgressDic["huang_yuwei_birthday"] == 10:
            self.game.taskProgressDic["huang_yuwei_birthday"] = 20
            self.game.generate_dialogue_sequence(
                None,
                "cherry_blossom_island.png",
                [_("Hahahahaha! Brilliant! You have some real potential!"),
                 _("Thank you for your mercy, Master Huang!"),
                 _("It's rare to see a young man with such potential."),
                 _("As promised, here's your reward!"),],
                ["Huang Xiaodong", "you", "Huang Xiaodong", "Huang Xiaodong"]
            )


            total = self.game.luck + 100*(self.game.taskProgressDic["join_sect"] == 100)
            self.game.add_item(_("Cherry Blossom Island Map"))

            if total >= 200:
                self.game.learn_skill(skill(_("Flower and Body Unite")))
                self.game.learn_move(special_move(_("Cherry Blossom Drifting Snow Blade")))
                messagebox.showinfo("", _("Huang Xiaodong teaches you a new skill: 'Flower and Body Unite' and a new move: 'Cherry Blossom Drifting Snow Blade'!"))

            elif total >= 150:
                self.game.learn_move(special_move(_("Cherry Blossom Drifting Snow Blade")))
                messagebox.showinfo("", _("Huang Xiaodong teaches you a new move: 'Cherry Blossom Drifting Snow Blade'!"))

            else:
                if _("Falling Cherry Blossom Finger Technique") in [m.name for m in self.game.sml]:
                    self.game.add_item(_("Golden Gloves"))
                    messagebox.showinfo("", _("Huang Xiaodong gives you a pair of 'Golden Gloves'!"))

                else:
                    self.game.learn_move(special_move(_("Falling Cherry Blossom Finger Technique")))
                    messagebox.showinfo("", _("Huang Xiaodong teaches you a new move: 'Falling Cherry Blossom Finger Technique'!"))

            self.game.generate_dialogue_sequence(
                None,
                "cherry_blossom_island.png",
                [_("Wow, thank you, Master Huang! You are too generous!"),
                 _("Don't mention it. Thank you for joining us today.")],
                ["you", "Huang Xiaodong"]
            )

        self.game.generate_dialogue_sequence(
            None,
            "cherry_blossom_island.png",
            [_("Alright, everyone, I'd like to thank you all once again for coming."),
             _("To express my gratitude, every guest will receive a copy of a map of the island."),
             _("Take care everyone, and hope to see you again soon!")],
            ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong"]
        )

        self.game.mapWin.deiconify()


    def spar_with_jia_wen(self, level=12):

        opp = character("Jia Wen",level,
                        sml=[special_move(_("Shanhu Fist"), level=6),
                             special_move(_("Basic Sword Technique"), level=10),
                             special_move(_("Fairy Sword Technique"), level=10),
                             special_move(_("Falling Cherry Blossom Finger Technique"), level=5),],
                        skills=[skill(_("Divine Protection"), level=4),
                                skill(_("Leaping Over Mountain Peaks"), level=7),
                                skill(_("Cherry Blossoms Floating on the Water"), level=7)],
                        preferences=None)

        self.game.battleMenu(self.game.you, opp, "battleground_bamboo_forest.png",
                             "jy_shendiaozhuti.mp3", fromWin=self.game.bamboo_forestWin,
                             battleType="training", destinationWinList=[self.game.bamboo_forestWin] * 3)


    def wu_hua_que_decision(self, choice):
        if choice == 1:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.chivalry += 10
            messagebox.showinfo("", _("Your chivalry +10!"))
            self.game.generate_dialogue_sequence(
                None,
                "Bamboo Forest.png",
                [_("Hey you, creepy dude, give it up already..."),
                 _("This young lady is taken."),
                 _("By whom? You? Hahahaha!"),
                 _("Of course not me..."),
                 _("I thought so... In that case, why don't you stay out of this."),
                 _("Hah, too bad; when I see a scum like you, I feel an irresistible urge to smash your face.")],
                ["you", "you", "Wu Hua Que", "you", "Wu Hua Que", "you"]
            )

            opp = character(_("Wu Hua Que"), 12,
                            sml=[special_move(_("Centipede Poison Powder")),
                                 special_move(_("Basic Punching Technique"), level=10)],
                            skills=[skill(_("Dragonfly Dance"), level=10)],
                            preferences=[1,1,4,1,4]
                            )

            self.game.battleMenu(self.game.you, opp, "battleground_bamboo_forest.png",
                            postBattleSoundtrack="jy_suspenseful1.mp3",
                            fromWin=None,
                            battleType="task", destinationWinList=[] * 3,
                            postBattleCmd=self.post_wu_hua_que_battle
            )


        elif choice == 2:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.chivalry -= 20
            messagebox.showinfo("", _("Your chivalry -20!"))
            self.game.generate_dialogue_sequence(
                self.game.bamboo_forestWin,
                "Bamboo Forest.png",
                [_("Xiao Mei, let's get away from this creep..."),
                 _("Hey, where are you going, pretty lady~ Come back~"),
                 _(".........")],
                ["Jia Wen", "Wu Hua Que", "you"]
            )
            self.game.bamboo_forestF1.place(x=-3000, y=-3000)
            self.game.bamboo_forestF2.place(x=-3000, y=-3000)
            self.game.taskProgressDic['wu_hua_que'] = 1


    def post_wu_hua_que_battle(self, choice=0):

        if choice == 0:
            self.game.generate_dialogue_sequence(
                self.game.bamboo_forestWin,
                "Bamboo Forest.png",
                [_("Wait! Spare me! I was wrong!"),
                 _("I won't do it again! Please, if you spare me I will repay you!"),
                 _("{}, don't listen to him. If you let him go, he'll pick on other victims.").format(self.game.character),
                 _("He's been like this for years; he won't change."),],
                ["Wu Hua Que", "Wu Hua Que", "Jia Wen", "Jia Wen"],
                [[_("Get rid of this evil once and for all."), partial(self.post_wu_hua_que_battle, 1)],
                 [_("Spare him and see what he has to offer."), partial(self.post_wu_hua_que_battle, 2)]]
            )

        elif choice == 1:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.generate_dialogue_sequence(
                self.game.bamboo_forestWin,
                "Bamboo Forest.png",
                [_("People like you will never chance... You must die."),
                 _("No, please, don't--- AHHHHHHHHHHH!")],
                ["you", "Wu Hua Que"]
            )
            self.game.chivalry += 30
            self.game.taskProgressDic['wu_hua_que'] = -1
            self.game.taskProgressDic["wudang_task"] = 3
            messagebox.showinfo("", _("Your chivalry +30!"))


        elif choice == 2:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.generate_dialogue_sequence(
                self.game.bamboo_forestWin,
                "Bamboo Forest.png",
                [_("Thank you, young hero!"),
                 _("In return, please take this Dragonfly Dance Manual. Good bye!")],
                ["Wu Hua Que", "Wu Hua Que"]
            )
            self.game.chivalry -= 30
            self.game.inv[_("Dragonfly Dance Manual")] = 1
            messagebox.showinfo("", _("Your chivalry -30!\nYou received 'Dragonfly Dance Manual' x 1!"))
            self.game.taskProgressDic['wu_hua_que'] = 1




    def clickedOnXiaoMei(self):
        if self.game.taskProgressDic["xiao_han_jia_wen"] == 0:
            if random() <= .33:
                self.game.generate_dialogue_sequence(
                    self.game.bamboo_forestWin,
                    "Bamboo Forest.png",
                    [_("Big Brother... my head hurts... *cough*"),
                     _("*cough* *cough* *cough*"),
                     _("I... *cough* ..."),
                     _("Hang in there, Xiao Mei!"),
                     _("*cough* *cough*"),
                     _("Xiao Mei! Are you ok? Xiao--"),
                     _("..............."),
                     _("Xiao Mei..."),
                     _("You poor child... rest in peace..."),
                     _("...............................")
                     ],
                    ["Xiao Mei", "Xiao Mei", "Xiao Mei", "Jia Wen", "Xiao Mei", "Jia Wen",
                     "Jia Wen", "Jia Wen", "Jia Wen", "you"]
                )
                self.game.taskProgressDic["xiao_han_jia_wen"] = 20
                self.game.bamboo_forestF2.place(x=-3000,y=-3000)

            else:
                self.game.generate_dialogue_sequence(
                    self.game.bamboo_forestWin,
                    "Bamboo Forest.png",
                    [_("Big Brother... my head hurts... *cough*"),
                     _("........")],
                    ["Xiao Mei", "you"],
                    [[_("I change my mind! I'll help you!"), partial(self.save_xiao_mei, 1)],
                     [_("Ignore"), partial(self.save_xiao_mei, 0)]]
                )


        elif self.game.taskProgressDic["xiao_han_jia_wen"] == 13 and self.game.inv[_("Snow Lotus Pill")] > 0:
            self.game.inv[_("Snow Lotus Pill")] -= 1
            self.save_xiao_mei(3)

        elif self.game.taskProgressDic["xiao_han_jia_wen"] >= 14:
            self.game.generate_dialogue_sequence(
                self.game.bamboo_forestWin,
                "Bamboo Forest.png",
                [_("Big Brother, thank you for saving my life!"),
                 _("I will do my best to repay you when I grow up!")],
                ["Xiao Mei", "Xiao Mei"]
            )

        else:
            self.game.generate_dialogue_sequence(
                self.game.bamboo_forestWin,
                "Bamboo Forest.png",
                [_("*Cough cough*...")],
                ["Xiao Mei"]
            )
