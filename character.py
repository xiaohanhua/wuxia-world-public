from random import*

class character:

    def __init__(self, name, level, attributeList=[], sml=[], skills=[], itemList=[], equipmentList=[],
                 status="Normal", preferences=None, multiple=1):

        self.name = name
        self.level = level
        self.itemList = itemList
        self.equipmentList = equipmentList
        self.sml = sml.copy()
        self.skills = skills.copy()
        self.status = status
        self.health_recovery = .05
        self.stamina_recovery = .2
        #upgrade_points = sum([int(4.5*1.115**i) for i in range(1, self.level+1)])
        upgrade_points = sum([int(6.5*1.11**i) for i in range(1, self.level + 1)])

        if len(attributeList) == 0:
            self.health = randrange(100 + 30 * self.level, 100 + 45 * self.level) + sum([10*i for i in range(self.level)])
            self.healthMax = self.health
            self.stamina = randrange(100 + 30 * self.level, 100 + 45 * self.level) + sum([10*i for i in range(self.level)])
            self.staminaMax = self.stamina

            if not preferences:
                self.attack = randrange(45, 56) + randrange(upgrade_points//5 - 5, upgrade_points//5 + 5)
                self.strength = randrange(45, 56) + randrange(upgrade_points//5 - 5, upgrade_points//5 + 5)
                self.speed = randrange(45, 56) + randrange(upgrade_points//5 - 5, upgrade_points//5 + 5)
                self.defence = randrange(45, 56) + randrange(upgrade_points//5 - 5, upgrade_points//5 + 5)
                self.dexterity = randrange(45, 56) + randrange(upgrade_points//5 - 5, upgrade_points//5 + 5)
            else:
                self.attack = randrange(45, 56) + randrange(upgrade_points*preferences[0]//sum(preferences) - 5,
                                                            upgrade_points*preferences[0]//sum(preferences) + 5)
                self.strength = randrange(45, 56) + randrange(upgrade_points*preferences[1]//sum(preferences) - 5,
                                                            upgrade_points*preferences[1]//sum(preferences) + 5)
                self.speed = randrange(45, 56) + randrange(upgrade_points*preferences[2]//sum(preferences) - 5,
                                                            upgrade_points*preferences[2]//sum(preferences) + 5)
                self.defence = randrange(45, 56) + randrange(upgrade_points*preferences[3]//sum(preferences) - 5,
                                                            upgrade_points*preferences[3]//sum(preferences) + 5)
                self.dexterity = randrange(45, 56) + randrange(upgrade_points*preferences[4]//sum(preferences) - 5,
                                                            upgrade_points*preferences[4]//sum(preferences) + 5)

            #if self.attack >= 200:
            #    self.attack = 200
            #if self.strength >= 200:
            #    self.strength = 200
            #if self.speed >= 200:
            #    self.speed = 200
            #if self.defence >= 200:
            #    self.defence = 200
            #if self.dexterity >= 200:
            #    self.dexterity = 200


        else:
            self.health = attributeList[0]
            self.healthMax = attributeList[1]
            self.stamina = attributeList[2]
            self.staminaMax = attributeList[3]
            self.attack = attributeList[4]
            self.strength = attributeList[5]
            self.speed = attributeList[6]
            self.defence = attributeList[7]
            self.dexterity = attributeList[8]


        if multiple > 1:
            stat_multiplier = multiple**(1/(1.5+min([multiple,4])/2))
            self.attack = int(self.attack*stat_multiplier)
            self.strength = int(self.strength*stat_multiplier)
            self.speed = int(self.speed*stat_multiplier)
            self.defence = int(self.defence*stat_multiplier)
            self.dexterity = int(self.dexterity*stat_multiplier)

            self.healthMax = int(self.healthMax*multiple**.9)
            self.health = self.healthMax
            self.staminaMax = int(self.staminaMax*stat_multiplier)
            self.stamina = self.staminaMax


    def equip_item(self, item):
        self.attack += item.datk
        self.strength += item.dstr
        self.speed += item.dspd
        self.defence += item.ddef
        self.dexterity += item.ddex