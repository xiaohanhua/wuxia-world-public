from tkinter import*
from datetime import*
from maze_generator import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
import tkinter.messagebox as messagebox


def isPrime(number):
    count = 0
    if number <= 1:
        return False
    for i in range(2, number):
        test = number%i
        if test == 0:
            count += 1
    if count < 1:
        return True
    else:
        return False

_ = gettext


class Scene_cherry_blossom_island_train:

    def __init__(self, game):
        self.game = game
        self.game.days_spent_in_training = int(self.game.taskProgressDic["join_sect"] * 100)
        try:
            if self.game.taskProgressDic["join_sect"] == 0.00:
                self.game.cherry_blossom_island_train_win = Toplevel(self.game.scrubHouseWin)
            else:
                self.game.cherry_blossom_island_train_win = Toplevel(self.game.continueGameWin)
        except:
            self.game.cherry_blossom_island_train_win = Toplevel(self.game.continueGameWin)

        self.game.cherry_blossom_island_train_win.title(_("Cherry Blossom Island"))

        bg = PhotoImage(file="cherry_blossom_island.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.cherry_blossom_island_train_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Huang Yuwei_icon_large.ppm")
        imageButton1 = Button(F1, command=partial(self.clickedOnHuangYuwei, True))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("Huang Yuwei"))
        label.grid(row=1, column=0)
        F1.place(x=w // 5, y=h // 2)

        F2 = Frame(canvas)
        pic2 = PhotoImage(file="Huang Yufei_icon_large.ppm")
        imageButton2 = Button(F2, command=partial(self.clickedOnHuangYufei, True))
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(F2, text=_("Huang Yufei"))
        label.grid(row=1, column=0)
        F2.place(x=w * 3 // 5, y=h // 2)

        self.game.days_spent_in_training_label = Label(self.game.cherry_blossom_island_train_win,
                                                  text=_("Days trained: {}").format(self.game.days_spent_in_training))
        self.game.days_spent_in_training_label.pack()
        self.game.cherry_blossom_island_train_F1 = Frame(self.game.cherry_blossom_island_train_win)
        self.game.cherry_blossom_island_train_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.cherry_blossom_island_train_win,
                                            options=["Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.cherry_blossom_island_train_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.cherry_blossom_island_train_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.cherry_blossom_island_train_win.winfo_width(), self.game.cherry_blossom_island_train_win.winfo_height()
        self.game.cherry_blossom_island_train_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.cherry_blossom_island_train_win.focus_force()
        self.game.cherry_blossom_island_train_win.mainloop()  ###


    def clickedOnHuangYuwei(self, training=False):
        if training:
            self.game.currentEffect = "button-20.mp3"
            self.game.startSoundEffectThread()
            for widget in self.game.cherry_blossom_island_train_F1.winfo_children():
                widget.destroy()

            Button(self.game.cherry_blossom_island_train_F1, text=_("Spar (Huang Yuwei)"),
                   command=partial(self.cherry_blossom_island_spar, _("Huang Yuwei"))
                   ).grid(row=2, column=0)
            Button(self.game.cherry_blossom_island_train_F1, text=_("Who are you?"),
                   command=partial(self.talk_to_huang_yuwei, 1)).grid(row=3, column=0)
            Button(self.game.cherry_blossom_island_train_F1, text=_("Teach me something please."),
                   command=self.huang_yuwei_teach).grid(row=4, column=0)

        else:
            self.game.currentEffect = "button-20.mp3"
            self.game.startSoundEffectThread()
            for widget in self.game.cherry_blossom_island_F1.winfo_children():
                widget.destroy()

            Button(self.game.cherry_blossom_island_F1, text=_("Spar (Huang Yuwei)"),
                   command=partial(self.cherry_blossom_island_spar, _("Huang Yuwei"), training=False)
                   ).grid(row=1, column=0)
            Button(self.game.cherry_blossom_island_F1, text=_("Who are you?"),
                   command=partial(self.talk_to_huang_yuwei, 2)
                   ).grid(row=2, column=0)
            Button(self.game.cherry_blossom_island_F1, text=_("Where's your dad?"),
                   command=partial(self.talk_to_huang_yuwei, 3)
                   ).grid(row=3, column=0)
            Button(self.game.cherry_blossom_island_F1, text=_("Got anything fun to do around here?"),
                   command=partial(self.talk_to_huang_yuwei, 4)
                   ).grid(row=4, column=0)


    def talk_to_huang_yuwei(self, choice):
        if choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.cherry_blossom_island_train_win,
                "cherry_blossom_island.png",
                [_("Hey there, I am Huang Yuwei, the older daughter of this island's owner!"),
                 _("If you have any questions about mathematics, music, calligraphy or other arts, feel free to ask me!"),
                 _("If you're more interested in improving your Kung Fu, we can spar or you can consult my younger sister."),
                 _("She spends much more time practicing Kung Fu than I do, and her agility techniques are superb!")],
                ["Huang Yuwei", "Huang Yuwei", "Huang Yuwei", "Huang Yuwei"]
            )
        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.cherry_blossom_island_win,
                "cherry_blossom_island.png",
                [_("Hey there, I am Huang Yuwei, the older daughter of this island's owner!")],
                ["Huang Yuwei"]
            )
        elif choice == 3:
            self.game.generate_dialogue_sequence(
                self.game.cherry_blossom_island_win,
                "cherry_blossom_island.png",
                [_("He's busy right now... I haven't seen him yet today.")],
                ["Huang Yuwei"]
            )
        elif choice == 4:
            if self.game.taskProgressDic["huang_yuwei"] == -1:
                self.game.generate_dialogue_sequence(
                    self.game.cherry_blossom_island_win,
                    "cherry_blossom_island.png",
                    [_("Here's a puzzle box. See if you can solve it.")],
                    ["Huang Yuwei"]
                )
                self.examineMysteriousBox()

            else:
                self.game.generate_dialogue_sequence(
                    self.game.cherry_blossom_island_win,
                    "cherry_blossom_island.png",
                    [_("Would you like to run around in the maze with my sister? She loves doing that!")],
                    ["Huang Yuwei"]
                )



    def huang_yuwei_teach(self):
        self.game.days_spent_in_training = int(self.game.taskProgressDic["join_sect"] * 100)
        self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
        r = randrange(1, 3)
        self.game.perception += r
        messagebox.showinfo("", _("Huang Yuwei spends 2 days teaching you various topics, including chess, music, calligraphy, mathematics, and more...\nYour perception +{}.").format(
            r))
        self.cherry_blossom_island_training_update(.02)


    def cherry_blossom_island_spar(self, targ, training=True):

        if training:
            if targ == _("Huang Yuwei"):
                opp = character(_("Huang Yuwei"), self.game.level,
                                sml=[special_move(_("Falling Cherry Blossom Finger Technique"), level=min([self.game.level + 1, 5])),
                                     special_move(_("Basic Sword Technique"), level=min([self.game.level + 1, 5]))],
                                skills=[skill(_("Cherry Blossoms Floating on the Water"), level=5)])
            elif targ == _("Huang Yufei"):
                opp = character(_("Huang Yufei"), self.game.level, sml=[special_move(_("Falling Cherry Blossom Finger Technique"),
                                                                                level=min([self.game.level + 1, 7])),
                                                                   special_move(_("Basic Sword Technique"),
                                                                                level=min([self.game.level + 1, 7]))],
                                skills=[skill(_("Cherry Blossoms Floating on the Water"), level=8)])
            else:
                opp = None

            self.game.battleMenu(self.game.you, opp, "battleground_cherry_blossom_island.png",
                            "jy_pipayu.mp3", fromWin=self.game.cherry_blossom_island_train_win,
                            battleType="training", destinationWinList=[self.game.cherry_blossom_island_train_win] * 3)

            if self.game.taskProgressDic["join_sect"] < 100:
                self.cherry_blossom_island_training_update(.01)

        else:
            year = calculate_month_day_year(self.game.gameDate)["Year"]
            if targ == _("Huang Yuwei"):
                opp = character(_("Huang Yuwei"), min([12+year*2, 16]),
                                sml=[special_move(_("Falling Cherry Blossom Finger Technique"), level=7),
                                     special_move(_("Basic Sword Technique"), level=10)],
                                skills=[skill(_("Cherry Blossoms Floating on the Water"), level=6)])
            elif targ == _("Huang Yufei"):
                opp = character(_("Huang Yufei"), min([14+year*2, 18]),
                                sml=[special_move(_("Falling Cherry Blossom Finger Technique"), level=9),
                                     special_move(_("Basic Sword Technique"), level=10)],
                                skills=[skill(_("Cherry Blossoms Floating on the Water"), level=10)])
            else:
                opp = None

            self.game.battleMenu(self.game.you, opp, "battleground_cherry_blossom_island.png",
                            "jy_pipayu.mp3", fromWin=self.game.cherry_blossom_island_win,
                            battleType="training", destinationWinList=[self.game.cherry_blossom_island_win] * 3)



    def clickedOnHuangYufei(self, training=False):
        if training:
            self.game.currentEffect = "button-20.mp3"
            self.game.startSoundEffectThread()
            for widget in self.game.cherry_blossom_island_train_F1.winfo_children():
                widget.destroy()

            Button(self.game.cherry_blossom_island_train_F1, text=_("Spar (Huang Yufei)"),
                   command=partial(self.cherry_blossom_island_spar, targ=_("Huang Yufei"))).grid(row=2, column=0)
            Button(self.game.cherry_blossom_island_train_F1, text=_("Who are you?"),
                   command=partial(self.talk_to_huang_yufei, 1)).grid(row=3, column=0)
            Button(self.game.cherry_blossom_island_train_F1, text=_("Teach me something please."),
                   command=self.huang_yufei_teach).grid(row=4, column=0)

        else:
            self.game.currentEffect = "button-20.mp3"
            self.game.startSoundEffectThread()
            for widget in self.game.cherry_blossom_island_F1.winfo_children():
                widget.destroy()

            Button(self.game.cherry_blossom_island_F1, text=_("Spar (Huang Yufei)"),
                   command=partial(self.cherry_blossom_island_spar, targ=_("Huang Yufei"), training=False)).grid(row=1, column=0)
            Button(self.game.cherry_blossom_island_F1, text=_("Who are you?"),
                   command=partial(self.talk_to_huang_yufei, 2)).grid(row=2, column=0)
            Button(self.game.cherry_blossom_island_F1, text=_("Let's play a game!"),
                   command=partial(self.talk_to_huang_yufei, 3)).grid(row=3, column=0)



    def talk_to_huang_yufei(self, choice):
        if choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.cherry_blossom_island_train_win,
                "cherry_blossom_island.png",
                [_("Hi, I am Huang Yufei, the younger daughter of this island's owner!"),
                 _("I love practicing Kung Fu while my sister likes to read and study other arts..."),
                 _("If you want to learn something interesting, just talk to her! She loves to share her knowledge."),
                 _("If you're looking for someone to practice with, I'm always down for a fun fight!")],
                ["Huang Yufei", "Huang Yufei", "Huang Yufei", "Huang Yufei"]
            )
        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.cherry_blossom_island_win,
                "cherry_blossom_island.png",
                [_("Hi, I am Huang Yufei, the younger daughter of this island's owner!")],
                ["Huang Yufei"]
            )
        elif choice == 3:
            self.game.cherry_blossom_island_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "cherry_blossom_island.png",
                [_("Ok! Let's race to see who can get out of the maze first!"),
                 _("Bring it on!")],
                ["Huang Yufei", "you"]
            )
            self.cherry_blossom_maze(training=False, yufei_chase=True)


    def huang_yufei_teach(self):
        self.game.cherry_blossom_island_train_win.withdraw()
        self.game.generate_dialogue_sequence(
            None,
            "cherry_blossom_island.png",
            [_("You want me to teach you something? Hmmm, well, you gotta play with me first!"),
             _("See if you can catch me! I'll meet you at the pavillion in the middle of the maze hehehe~")],
            ["Huang Yufei", "Huang Yufei"],
        )
        self.cherry_blossom_maze(training=True)
        self.cherry_blossom_island_training_update(.01)


    def cherry_blossom_island_training_update(self, increment=.01):
        self.game.taskProgressDic["join_sect"] += increment
        self.game.taskProgressDic["join_sect"] = round(self.game.taskProgressDic["join_sect"], 2)
        self.game.days_spent_in_training = int(self.game.taskProgressDic["join_sect"] * 100)
        self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
        if self.game.days_spent_in_training >= 60:
            self.game.days_spent_in_training = 60
            self.game.taskProgressDic["join_sect"] = .6
            self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
            self.game.cherry_blossom_island_train_win.withdraw()

            self.game.generate_dialogue_sequence(
                None,
                "cherry_blossom_island.png",
                [_("Your 60-day training period has come to an end. I hope you've used this time wisely."),
                 _("Before I send you off, let me test your skills to see how much progress you've made..."),
                 _("Don't worry; I will not go all out on you.")],
                ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong"]
            )

            self.game.restore(0, 0, full=True)

            opp = character(_("Huang Xiaodong"), 20,
                            sml=[special_move(_("Falling Cherry Blossom Finger Technique"), level=10),
                                 special_move(_("Basic Sword Technique"), level=10),
                                 special_move(_("Cherry Blossom Drifting Snow Blade"), level=10)],
                            skills=[skill(_("Cherry Blossoms Floating on the Water"), level=10)])

            self.game.battleID = "HuangXiaodongTrainingTest"

            self.game.battleMenu(self.game.you, opp, "battleground_cherry_blossom_island.png",
                            postBattleSoundtrack="jy_pipayu.mp3",
                            fromWin=self.game.cherry_blossom_island_train_win,
                            battleType="test", destinationWinList=[self.game.cherry_blossom_island_train_win] * 3,
                            postBattleCmd=self.cherry_blossom_island_training_reward
                            )



    def cherry_blossom_maze(self, training=False, yufei_chase=False):

        print("Loading map... Please wait.")
        self.game.maze_movement_speed = 10
        self.game.yufei_chase = yufei_chase
        self.game.maze_training = training
        self.game.maze_time_start = datetime.now()

        self.game.maze_canvas_width = 800
        self.game.maze_canvas_height = 600

        if training:
            self.game.mazeWin = Toplevel(self.game.cherry_blossom_island_train_win)
        else:
            self.game.mazeWin = Toplevel(self.game.mapWin)

        self.game.maze_canvas = Canvas(self.game.mazeWin, width=self.game.maze_canvas_width, height=self.game.maze_canvas_height,
                                  bg='black')
        self.game.maze_canvas.pack(side=LEFT)

        self.game.maze_instructions_frame = Frame(self.game.mazeWin, borderwidth = 2, relief = SUNKEN)
        self.game.maze_instructions_frame.pack(fill=Y)

        Label(self.game.maze_instructions_frame, text = _("Use the arrow keys to move.\nLook for the pavillion in the\nmiddle of the maze.")).pack()
        if self.game.maze_training:
            Button(self.game.maze_instructions_frame, text=_("Back"), command=partial(self.game.winSwitch, self.game.mazeWin, self.game.cherry_blossom_island_train_win)).pack()
        elif self.game.yufei_chase:
            Button(self.game.maze_instructions_frame, text=_("Back"), command=partial(self.game.winSwitch, self.game.mazeWin, self.game.cherry_blossom_island_win)).pack()
        else:
            Button(self.game.maze_instructions_frame, text=_("Back"), command=partial(self.game.winSwitch, self.game.mazeWin, self.game.mapWin)).pack()



        bg_img = PhotoImage(file="maze_bg_stone_big2.png")
        self.game.bg_img = self.game.maze_canvas.create_image(self.game.maze_canvas_width - bg_img.width(),
                                                    self.game.maze_canvas_height - bg_img.height(),
                                                    anchor=NW, image=bg_img)

        self.game.walls = []
        wall_img = PhotoImage(file="cherry_blossom_piece.ppm")
        w = wall_img.width()
        h = wall_img.height()

        starting_x = self.game.maze_canvas_width - bg_img.width() - w // 2
        starting_y = self.game.maze_canvas_height - bg_img.height() - w // 2
        potential_targ_coords = []
        maze = generate_maze(bg_img.width() // w, bg_img.height() // h, .9, .9)

        for x in range(maze.shape[1]):
            for y in range(maze.shape[0]):
                if maze[y][x]:
                    wall = self.game.maze_canvas.create_image(starting_x + x * w, starting_y + y * h, anchor=NW,
                                                         image=wall_img)
                    self.game.walls.append(wall)
                else:
                    # neighbors = [maze[y][x-1], maze[y][x+1], maze[y-1][x], maze[y+1][x]]
                    if abs(y - bg_img.height() // h // 2) <= 5 and x - bg_img.width() // w // 2 <= -7:  # and sum(neighbors) <= 2:
                        potential_targ_coords.append([x, y])

        targ_coords = choice(potential_targ_coords)
        targ_img = PhotoImage(file="cherry_blossom_island_hut.ppm")
        self.game.maze_targ = self.game.maze_canvas.create_image(starting_x + targ_coords[0] * w,
                                                       starting_y + targ_coords[1] * h, anchor=NW, image=targ_img)

        you_img = PhotoImage(file="you_icon_small.ppm")
        self.game.you_x, self.game.you_y = self.game.maze_canvas_width - you_img.width() - w // 2 - 5, self.game.maze_canvas_height - you_img.height() - h // 2 - 5
        self.game.you_img = self.game.maze_canvas.create_image(self.game.you_x, self.game.you_y, anchor=NW, image=you_img)

        self.game.mazeWin.bind('<Key>', self.move)
        self.game.mazeWin.update_idletasks()
        toplevel_w, toplevel_h = self.game.mazeWin.winfo_width(), self.game.mazeWin.winfo_height()
        self.game.mazeWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.mazeWin.focus_force()
        self.game.mazeWin.mainloop()###

    def reached_targ(self):
        x1, y1, x2, y2 = self.game.maze_canvas.bbox(self.game.you_img)
        tx1, ty1, tx2, ty2 = self.game.maze_canvas.bbox(self.game.maze_targ)
        return ((x1 >= tx1 and x1 <= tx2) or (x2 >= tx1 and x2 <= tx2)) and (
                    (y1 >= ty1 and y1 <= ty2) or (y2 >= ty1 and y2 <= ty2))

    def collision_detection(self, dx, dy):
        x1, y1, x2, y2 = self.game.maze_canvas.bbox(self.game.you_img)
        for wall in self.game.walls:
            wx1, wy1, wx2, wy2 = self.game.maze_canvas.bbox(wall)

            if dx > 0:
                if x2 + dx >= wx1 and x2 + dx <= wx2 and ((y1 >= wy1 and y1 <= wy2) or (y2 >= wy1 and y2 <= wy2)):
                    return True
            if dx < 0:
                if x1 + dx <= wx2 and x1 + dx >= wx1 and ((y1 >= wy1 and y1 <= wy2) or (y2 >= wy1 and y2 <= wy2)):
                    return True

            if dy > 0:
                if y2 + dy >= wy1 and y2 + dy <= wy2 and ((x1 >= wx1 and x1 <= wx2) or (x2 <= wx2 and x2 >= wx1)):
                    return True
            if dy < 0:
                if y1 + dy <= wy2 and y1 + dy >= wy1 and ((x1 >= wx1 and x1 <= wx2) or (x2 <= wx2 and x2 >= wx1)):
                    return True

    def bg_boundary_detection(self, dx, dy):

        x1, y1, x2, y2 = self.game.maze_canvas.bbox(self.game.you_img)
        bx1, by1, bx2, by2 = self.game.maze_canvas.bbox(self.game.bg_img)

        current_x = (x1 + x2) // 2
        current_y = (y1 + y2) // 2

        if dx > 0:
            if current_x + self.game.maze_canvas_width // 2 + (
                    x2 - x1) // 2 + dx > bx2 or current_x - self.game.maze_canvas_width // 2 - (
                    x2 - x1) // 2 + 25 < bx1:
                return "x"
        if dx < 0:
            if current_x - self.game.maze_canvas_width // 2 - (
                    x2 - x1) // 2 + dx + 25 < bx1 or current_x + self.game.maze_canvas_width // 2 + (x2 - x1) // 2 > bx2:
                return "x"

        if dy > 0:
            if current_y + (y2 - y1) // 2 + self.game.maze_canvas_height // 2 + dy > by2 or current_y - (
                    y2 - y1) // 2 - self.game.maze_canvas_height // 2 + 25 < by1:
                return "y"
        if dy < 0:
            if current_y - (y2 - y1) // 2 - self.game.maze_canvas_height // 2 + dy + 25 < by1 or current_y + (
                    y2 - y1) // 2 + self.game.maze_canvas_height // 2 > by2:
                return "y"

    def obj_boundary_detection(self, obj, dx, dy):
        x1, y1, x2, y2 = self.game.maze_canvas.bbox(obj)
        current_x = (x1 + x2) // 2
        current_y = (y1 + y2) // 2
        obj_w = x2 - x1
        obj_h = y2 - y1
        targ_x = current_x + dx
        targ_y = current_y + dy
        if targ_x + obj_w // 2 > self.game.maze_canvas_width or targ_x - obj_w // 2 < 0:
            return "x"
        elif targ_y + obj_h // 2 > self.game.maze_canvas_height or targ_y - obj_h // 2 < 0:
            return "y"
        else:
            return

    def move(self, event):

        if event.keysym == "Left":
            if not self.bg_boundary_detection(-self.game.maze_movement_speed, 0):
                if not self.collision_detection(-self.game.maze_movement_speed, 0):
                    self.game.maze_canvas.move(self.game.bg_img, self.game.maze_movement_speed, 0)
                    for item in [self.game.maze_targ] + self.game.walls:
                        self.game.maze_canvas.move(item, self.game.maze_movement_speed, 0)

            elif not self.obj_boundary_detection(self.game.you_img, -self.game.maze_movement_speed,
                                                 0) and not self.collision_detection(
                    -self.game.maze_movement_speed, 0):
                self.game.maze_canvas.move(self.game.you_img, -self.game.maze_movement_speed, 0)

        elif event.keysym == "Right":
            if not self.bg_boundary_detection(self.game.maze_movement_speed, 0):
                if not self.collision_detection(self.game.maze_movement_speed, 0):
                    self.game.maze_canvas.move(self.game.bg_img, -self.game.maze_movement_speed, 0)
                    for item in [self.game.maze_targ] + self.game.walls:
                        self.game.maze_canvas.move(item, -self.game.maze_movement_speed, 0)

            elif not self.obj_boundary_detection(self.game.you_img, self.game.maze_movement_speed,
                                                 0) and not self.collision_detection(
                    self.game.maze_movement_speed, 0):
                self.game.maze_canvas.move(self.game.you_img, self.game.maze_movement_speed, 0)

        elif event.keysym == "Down":
            if not self.bg_boundary_detection(0, self.game.maze_movement_speed):
                if not self.collision_detection(0, self.game.maze_movement_speed):
                    self.game.maze_canvas.move(self.game.bg_img, 0, -self.game.maze_movement_speed)
                    for item in [self.game.maze_targ] + self.game.walls:
                        self.game.maze_canvas.move(item, 0, -self.game.maze_movement_speed)

            elif not self.obj_boundary_detection(self.game.you_img, 0,
                                                 self.game.maze_movement_speed) and not self.collision_detection(0,
                                                                                                            self.game.maze_movement_speed):
                self.game.maze_canvas.move(self.game.you_img, 0, self.game.maze_movement_speed)

        elif event.keysym == "Up":
            if not self.bg_boundary_detection(0, -self.game.maze_movement_speed):
                if not self.collision_detection(0, -self.game.maze_movement_speed):
                    self.game.maze_canvas.move(self.game.bg_img, 0, self.game.maze_movement_speed)
                    for item in [self.game.maze_targ] + self.game.walls:
                        self.game.maze_canvas.move(item, 0, self.game.maze_movement_speed)

            elif not self.obj_boundary_detection(self.game.you_img, 0,
                                                 -self.game.maze_movement_speed) and not self.collision_detection(0,
                                                                                                             -self.game.maze_movement_speed):
                self.game.maze_canvas.move(self.game.you_img, 0, -self.game.maze_movement_speed)

        if self.reached_targ():

            dd = datetime.now() - self.game.maze_time_start
            self.game.passed_cherry_blossom_maze = True

            if self.game.maze_training:
                self.game.mazeWin.quit()
                self.game.mazeWin.destroy()

                if dd.seconds <= 45:
                    if self.game.luck*1.5 >= random()*100 and _("Cherry Blossoms Floating on the Water") not in [skill.name for skill in self.game.skills]:
                        self.game.learn_skill(skill(_("Cherry Blossoms Floating on the Water")))
                        messagebox.showinfo("", _("Huang Yufei taught you an agility technique: 'Cherry Blossoms Floating on the Water'"))
                    else:
                        self.game.speed += 2
                        self.game.dexterity += 2
                        messagebox.showinfo("", _("After running through the maze, you feel lighter and swifter. Your speed +2, dexterity +2."))


                elif dd.seconds <= 50:
                    if self.game.luck >= random()*100 and _("Cherry Blossoms Floating on the Water") not in [skill.name for skill in self.game.skills]:
                        self.game.learn_skill(skill(_("Cherry Blossoms Floating on the Water")))
                        messagebox.showinfo("", _("Huang Yufei taught you an agility technique: 'Cherry Blossoms Floating on the Water'"))
                    else:
                        self.game.speed += 1
                        self.game.dexterity += 1
                        messagebox.showinfo("", _("After running through the maze, you feel lighter and swifter. Your speed +1, dexterity +1."))

                elif dd.seconds <= 60:
                    self.game.speed += 1
                    self.game.dexterity += 1
                    messagebox.showinfo("", _("After running through the maze, you feel lighter and swifter. Your speed +1, dexterity +1."))

                self.game.cherry_blossom_island_train_win.deiconify()

            else:

                self.game.mazeWin.quit()
                self.game.mazeWin.destroy()

                if self.game.yufei_chase:
                    if dd.seconds <= 45:
                        if self.game.luck >= random() * 100 + 50 and _("Cherry Blossoms Floating on the Water") not in [skill.name for skill in self.game.skills]:
                            self.game.generate_dialogue_sequence(
                                None,
                                "cherry_blossom_island.png",
                                [_("Yay! That was fun! Thanks for playing with me hehe~")],
                                ["Huang Yufei"]
                            )
                            self.game.learn_skill(skill(_("Cherry Blossoms Floating on the Water")))
                            messagebox.showinfo("", _("Huang Yufei taught you an agility technique: 'Cherry Blossoms Floating on the Water'"))
                        else:
                            self.game.generate_dialogue_sequence(
                                None,
                                "cherry_blossom_island.png",
                                [_("Yay! That was fun! Thanks for playing with me hehe~"),
                                 _("Here, have some of this soup that my mom made!")],
                                ["Huang Yufei", "Huang Yufei"]
                            )
                            self.game.add_item(_("Cherry Blossom Papaya Tremella Soup"), 1)
                            messagebox.showinfo("", _("Received 'Cherry Blossom Papaya Tremella Soup' x 1!"))

                    else:
                        self.game.generate_dialogue_sequence(
                            None,
                            "cherry_blossom_island.png",
                            [_("You were a bit slow this time, but don't give up! Keep practicing; you'll get better!")],
                            ["Huang Yufei"]
                        )

                    self.game.cherry_blossom_island_win.deiconify()

                return



    def cherry_blossom_island_training_reward(self):
        self.game.taskProgressDic["join_sect"] = 100
        total_levels = 0
        for move in self.game.sml:
            if "Cherry Blossom" in move.name:
                total_levels += move.level
        for skill in self.game.skills:
            if "Cherry Blossom" in skill.name:
                total_levels += skill.level

        self.game.restore(0, 0, full=True)
        self.game.inv[_('Cherry Blossom Island Map')] = 1
        #print(total_levels)
        if self.game.luck * total_levels / 10 >= 95:
            self.game.generate_dialogue_sequence(
                self.game.cherry_blossom_island_train_win,
                "cherry_blossom_island.png",
                [_("Hmmmm, I am very pleased with the progress you've made."),
                 _("As your reward, I will teach you the 'Cherry Blossom Drifting Snow Blade'."),
                 _("Finally, here's a map of the island; with this map, you will have no trouble getting through the maze if you should ever come back to visit."),
                 _("Thank you, Master, for your kindness and hospitality!"),
                 _("You're welcome. I hope you can use what you've learned for good, and not evil."),
                 _("Don't worry; I won't disappoint you! Take care, Master!"),
                 _("Farewell, Yuwei and Yufei!"),
                 _("Goodbye, {}!".format(self.game.character)),
                 _("Come back and visit us when you get a chance!")],
                ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "you", "Huang Xiaodong", "you",
                 "you", "Huang Yuwei", "Huang Yufei"]
            )

            new_move = special_move(name=_("Cherry Blossom Drifting Snow Blade"))
            self.game.learn_move(new_move)
            print(_("Learned"), new_move.name)


        elif self.game.luck * total_levels / 10 >= random() * 100:
            self.game.generate_dialogue_sequence(
                self.game.cherry_blossom_island_train_win,
                "cherry_blossom_island.png",
                [_("Hmmmm, I am very pleased with the progress you've made."),
                 _("As your reward, here are some Golden Gloves and also some Cherry Blossom Papaya Tremella Soup that my wife and daughters made."),
                 _("Finally, here's a map of the island; with this map, you will have no trouble getting through the maze if you should ever come back to visit."),
                 _("Thank you, Master, for your kindness and hospitality!"),
                 _("You're welcome. I hope you can use what you've learned for good, and not evil."),
                 _("Don't worry; I won't disappoint you! Take care, Master!"),
                 _("Farewell, Yuwei and Yufei!"),
                 _("Goodbye, {}!".format(self.game.character)),
                 _("Come back and visit us when you get a chance!")],
                ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "you", "Huang Xiaodong", "you",
                 "you", "Huang Yuwei", "Huang Yufei"]
            )
            self.game.inv["Cherry Blossom Papaya Tremella Soup"] = 5
            self.game.inv["Golden Gloves"] = 1


        elif self.game.luck * total_levels / 10 >= random() * 80:
            self.game.generate_dialogue_sequence(
                self.game.cherry_blossom_island_train_win,
                "cherry_blossom_island.png",
                [_("Hmmmm, I am very pleased with the progress you've made."),
                 _("As your reward, here's some Cherry Blossom Papaya Tremella Soup that my wife and daughters made."),
                 _("Finally, here's a map of the island; with this map, you will have no trouble getting through the maze if you should ever come back to visit."),
                 _("Thank you, Master, for your kindness and hospitality!"),
                 _("You're welcome. I hope you can use what you've learned for good, and not evil."),
                 _("Don't worry; I won't disappoint you! Take care, Master!"),
                 _("Farewell, Yuwei and Yufei!"),
                 _("Goodbye, {}!".format(self.game.character)),
                 _("Come back and visit us when you get a chance!")],
                ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "you", "Huang Xiaodong", "you",
                 "you", "Huang Yuwei", "Huang Yufei"]
            )
            self.game.inv["Cherry Blossom Papaya Tremella Soup"] = 5


        else:
            self.game.generate_dialogue_sequence(
                self.game.cherry_blossom_island_train_win,
                "cherry_blossom_island.png",
                [_("Hmmmm, you didn't meet my expectations..."),
                 _("However, I will not let you go empty handed."),
                 _("Here's a map of the island; with this map, you will have no trouble getting through the maze if you should ever come back to visit."),
                 _("Thank you, Master, for your kindness and hospitality!"),
                 _("You're welcome. I hope you can use what you've learned for good, and not evil."),
                 _("Don't worry; I won't disappoint you! Take care, Master!"),
                 _("Farewell, Yuwei and Yufei!"),
                 _("Goodbye, {}!".format(self.game.character)),
                 _("Come back and visit us when you get a chance!")],
                ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "you", "Huang Xiaodong", "you",
                 "you", "Huang Yuwei", "Huang Yufei"]
            )

        self.game.cherry_blossom_island_train_win.quit()
        self.game.cherry_blossom_island_train_win.destroy()
        self.game.generateGameWin()


    def examineMysteriousBox(self):

        self.game.cherry_blossom_island_win.withdraw()
        self.mysteriousBoxWin = Toplevel(self.game.cherry_blossom_island_win)

        instructions = "Fill the boxes with the numbers 1-9 so that\n the sum of each row and column is a prime number."
        Label(self.mysteriousBoxWin, text=instructions).pack()

        F1 = Frame(self.mysteriousBoxWin)
        F1.pack()
        F2 = Frame(self.mysteriousBoxWin)
        F2.pack()

        self.mysteriousBoxEntry1 = Entry(F1, width=10)
        self.mysteriousBoxEntry2 = Entry(F1, width=10)
        self.mysteriousBoxEntry3 = Entry(F1, width=10)
        self.mysteriousBoxEntry4 = Entry(F1, width=10)
        self.mysteriousBoxEntry5 = Entry(F1, width=10)
        self.mysteriousBoxEntry6 = Entry(F1, width=10)
        self.mysteriousBoxEntry7 = Entry(F1, width=10)
        self.mysteriousBoxEntry8 = Entry(F1, width=10)
        self.mysteriousBoxEntry9 = Entry(F1, width=10)

        self.mysteriousBoxEntry1.grid(row=1, column=0)
        self.mysteriousBoxEntry2.grid(row=1, column=1)
        self.mysteriousBoxEntry3.grid(row=1, column=2)
        Label(F1, text="").grid(row=2, column=1)
        self.mysteriousBoxEntry4.grid(row=3, column=0)
        self.mysteriousBoxEntry5.grid(row=3, column=1)
        self.mysteriousBoxEntry6.grid(row=3, column=2)
        Label(F1, text="").grid(row=4, column=1)
        self.mysteriousBoxEntry7.grid(row=5, column=0)
        self.mysteriousBoxEntry8.grid(row=5, column=1)
        self.mysteriousBoxEntry9.grid(row=5, column=2)

        Button(F2, text=_("Open Box"), command=self.checkMysteriousBox).grid(row=9, column=0)
        Button(F2, text=_("Back"), command=partial(self.game.winSwitch, self.mysteriousBoxWin, self.game.cherry_blossom_island_win)).grid(row=10, column=0)


    def checkMysteriousBox(self):

        num1 = self.mysteriousBoxEntry1.get()
        num2 = self.mysteriousBoxEntry2.get()
        num3 = self.mysteriousBoxEntry3.get()
        num4 = self.mysteriousBoxEntry4.get()
        num5 = self.mysteriousBoxEntry5.get()
        num6 = self.mysteriousBoxEntry6.get()
        num7 = self.mysteriousBoxEntry7.get()
        num8 = self.mysteriousBoxEntry8.get()
        num9 = self.mysteriousBoxEntry9.get()

        try:

            usedList = []

            numList = [int(num1), int(num2), int(num3), int(num4), int(num5), int(num6), int(num7), int(num8),
                       int(num9)]

            for number in numList:

                if number < 1 or number > 9:
                    messagebox.showinfo("", "Invalid inputs; must be integers from 1-9.")
                    return None

                elif number in usedList:
                    messagebox.showinfo("", "Invalid inputs; can not repeat numbers.")
                    return None

                else:
                    usedList.append(number)

        except:
            messagebox.showinfo("", "Invalid inputs; must be integers from 1-9.")
            return None

        row1 = numList[0] + numList[1] + numList[2]
        row2 = numList[3] + numList[4] + numList[5]
        row3 = numList[6] + numList[7] + numList[8]

        column1 = numList[0] + numList[3] + numList[6]
        column2 = numList[1] + numList[4] + numList[7]
        column3 = numList[2] + numList[5] + numList[8]

        solutionIsCorrect = isPrime(row1) and isPrime(row2) and isPrime(row3) and isPrime(column1) and isPrime(
            column2) and isPrime(column3)

        if solutionIsCorrect:
            self.solved_puzzle()

        else:
            print("You try to open the box, but it's tightly shut!\n")


    def solved_puzzle(self):
        self.mysteriousBoxWin.quit()
        self.mysteriousBoxWin.destroy()
        self.game.generate_dialogue_sequence(
            self.game.cherry_blossom_island_win,
            "cherry_blossom_island.png",
            [_("Hah! Got it!"),
             _("Wow! Nicely done!"),
             _("Hehehe, it's quite a challenge, but I finally figured it out after some time.")],
            ["you", "Huang Yuwei", "you"]
        )
        self.game.taskProgressDic["huang_yuwei"] = 1
        self.game.perception += 15
        messagebox.showinfo("", _("Your perception increased by 15!"))



class Scene_cherry_blossom_island(Scene_cherry_blossom_island_train):
    def __init__(self, game):
        self.game = game
        self.game.passed_cherry_blossom_maze = False
        if _("Cherry Blossom Island Map") not in self.game.inv:
            self.cherry_blossom_maze()
        else:
            self.game.passed_cherry_blossom_maze = True

        if self.game.passed_cherry_blossom_maze:
            self.go_cherry_blossom_island()


    def go_cherry_blossom_island(self):
        self.game.cherry_blossom_island_win = Toplevel(self.game.mapWin)

        self.game.cherry_blossom_island_win.title(_("Cherry Blossom Island"))

        bg = PhotoImage(file="cherry_blossom_island.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.cherry_blossom_island_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        if self.game.taskProgressDic["supreme_leader"] != 1:
            F1 = Frame(canvas)
            pic1 = PhotoImage(file="Huang Yuwei_icon_large.ppm")
            imageButton1 = Button(F1, command=partial(self.clickedOnHuangYuwei, False))
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            label = Label(F1, text=_("Huang Yuwei"))
            label.grid(row=1, column=0)
            F1.place(x=w // 5, y=h // 2)

            F2 = Frame(canvas)
            pic2 = PhotoImage(file="Huang Yufei_icon_large.ppm")
            imageButton2 = Button(F2, command=partial(self.clickedOnHuangYufei, False))
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            label = Label(F2, text=_("Huang Yufei"))
            label.grid(row=1, column=0)
            F2.place(x=w * 3 // 5, y=h // 2)

        self.game.cherry_blossom_island_F1 = Frame(self.game.cherry_blossom_island_win)
        self.game.cherry_blossom_island_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.cherry_blossom_island_win,
                                                 options=["Map", "Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.cherry_blossom_island_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.cherry_blossom_island_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.cherry_blossom_island_win.winfo_width(), self.game.cherry_blossom_island_win.winfo_height()
        self.game.cherry_blossom_island_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.cherry_blossom_island_win.focus_force()
        self.game.cherry_blossom_island_win.mainloop()