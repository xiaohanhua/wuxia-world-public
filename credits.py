DISCLAIMER = """
This is an open-source, non-commercial project created by 《LonelyScrub8》/《孤独的Scrub 8》.

Assets used in the game come from Creative Commons and other sources on the internet. All asset copyrights belong to 
their respective owners. Click 'Attribution' to see attribution information. To request modification to attribution 
info and/or removal of assets, please contact 《LonelyScrub8》/《孤独的Scrub 8》 at LonelyScrub8@gmail.com

Game copyright belongs to《LonelyScrub8》/《孤独的Scrub 8》. This project can NOT be re-distributed for commercial 
purposes under any circumstances.

本游戏是《孤独的Scrub 8》创作的免费，开源的同人游戏。游戏素材来自于网络，所有版权归原主。
请点击'Attribution'查看素材来源。若有侵权或需要更改信息，请联络LonelyScrub8@gmail.com
"""


IMAGE_ATTRIBUTION_UNSPLASH = """
https://unsplash.com/photos/de0P588zgls Photo by Tom Coomer on Unsplash
https://unsplash.com/photos/VCRci_usn0w Photo by Mirko Blicke on Unsplash
https://unsplash.com/photos/_OQ8Jc7kBmA Photo by JR Korpa on Unsplash
https://unsplash.com/photos/1L71sPT5XKc Photo by Rosie Fraser on Unsplash
https://unsplash.com/photos/v-4hCZPj1Ks Photo by Yifan Liu on Unsplash
https://unsplash.com/photos/l6dDiMFabrg Photo by Justin Lim on Unsplash
https://unsplash.com/photos/7Jkb1_Lg2Eg Photo by Jimmy Chang on Unsplash
https://unsplash.com/photos/caS4xEOtL_k Photo by Ezra Jeffrey-Comeau on Unsplash
https://unsplash.com/photos/fvbVdB5n2I8 Photo by Michael Brunk on Unsplash
https://unsplash.com/photos/sp-p7uuT0tw Photo by Sebastian Unrau on Unsplash
https://unsplash.com/photos/2Y4dE8sdhlc Photo by Irina Iriser on Unsplash
https://unsplash.com/photos/JxR2QUrEuaQ Photo by Mike Petrucci on Unsplash
https://unsplash.com/photos/vsxOcxtWig4 Photo by Adrian Pereira on Unsplash
https://unsplash.com/photos/-HDbl_akROQ Photo by Annie Spratt on Unsplash
https://unsplash.com/photos/fzHaSRdAi68 Photo by Masaaki Komori on Unsplash
https://unsplash.com/photos/_41JjnrSeHU Photo by Yustinus Subiakto on Unsplash
https://unsplash.com/photos/K8g-HOaJaxw Photo by Stacey Gabrielle Koenitz Rozells on Unsplash
https://unsplash.com/photos/s20nA2KB1N4 Photo by Andrey Andreyev on Unsplash
https://unsplash.com/photos/jh2KTqHLMjE Photo by Jeremy Thomas on Unsplash
https://unsplash.com/photos/aCHBgtcE7D8 Photo by silvana amicone on Unsplash
https://unsplash.com/photos/Cm5zI68Wdew Photo by Eugene Triguba on Unsplash
https://unsplash.com/photos/SwbsW4uf1Qs Photo by Nick Fewings on Unsplash
https://unsplash.com/photos/Eydo2lQNfgU Photo by 301+ Kim on Unsplash
https://unsplash.com/photos/xvzslxskOw4 Photo by Krisztián Korhetz on Unsplash
https://unsplash.com/photos/ULYY9HMKI6Q Photo by Timur M on Unsplash
https://unsplash.com/photos/Y_e_3q4bErg Photo by zhu wei on Unsplash
https://unsplash.com/photos/Dw7k47LynmI Photo by Rafik Wahba on Unsplash
https://unsplash.com/photos/uTTABF3mSpc Photo by Yuriy Rzhemovskiy on Unsplash
https://unsplash.com/photos/4mNV6RJcEu8 Photo by Alexander Hörl on Unsplash
https://unsplash.com/photos/uqYy29Sfb8Q Photo by Ryan Cheng on Unsplash
"""


IMAGE_ATTRIBUTION_OTHER = """
http://img.1ppt.com/uploads/allimg/1712/1_171203090756_1.jpg Painting by ?
https://pic.17qq.com/uploads/utrhuarwcx.jpeg 
https://imgsrc.baidu.com/baike/pic/item/7acb0a46f21fbe090e7a270a64600c338744ad8c.jpg
http://a4.att.hudong.com/62/90/01300000025788122195906673825_s.jpg 
https://www.flickr.com/photos/13798849@N00/299376335 "Kimono Model" by Stefan_- is licensed under CC BY-NC-ND 2.0. 
To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-nd/2.0/
https://upload.wikimedia.org/wikipedia/commons/c/c3/Kusarigama.jpg 
https://g-search1.alicdn.com/img/bao/uploaded/i2/TB1BKbIPVXXXXXXXVXXYXGcGpXX_M2.SS2_300x300.jpg
https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Medv_jelch.jpg/1200px-Medv_jelch.jpg

《射雕英雄传》 2017
《天下第一》 2005
《倚天屠龙记》 2019
《锦衣卫之初露锋芒》
《陈真》1982
《八大豪侠》

Maze Generator code: https://github.com/timothygordon32/mazes/blob/master/maze.py
"""


SPRITE_ATTRIBUTION_OGA_1 = """
https://opengameart.org/content/flare-hud (Mumu)
https://opengameart.org/content/rpg-gui-contstruction-kit-vmoon (Under the moon)
https://opengameart.org/content/flare-item-variation-60x60-only (Mumu)
https://opengameart.org/content/hero-spritesheets-ars-notoria (Balmer)
https://opengameart.org/content/asteroids (phaelax)
https://opengameart.org/content/texture-to-tile-project (The Chayed)
https://opengameart.org/content/platformer-or-fighter-sprites (Fracture)
https://opengameart.org/content/four-characters-my-lpc-entries (Redshrike, Stephen Challener)
https://opengameart.org/content/chest-and-mimic (Redshrike, Stephen Challener)
https://opengameart.org/content/country-side-platform-tiles (ansimuz)
https://opengameart.org/content/lpc-heroine-2 (Yamilian)
https://opengameart.org/content/lpc-heroine (Yamilian)
https://opengameart.org/content/armor-icons-by-equipment-slot-with-transparency (Clint Bellanger, Blarumyrran, crowline, Justin Nichol)
https://opengameart.org/content/wandering-vendor-npc (Tiziana)
https://opengameart.org/content/isometric-hero-and-creatures (Clint Bellanger)
https://opengameart.org/content/pixel-art-contest-entry-armor-and-hair-for-anime-style-base (LokiF)
https://opengameart.org/content/cabbit-collection (AntumDeluge)
https://opengameart.org/content/shuriken-pixel-art (Loel)
https://opengameart.org/content/jump-and-run-tileset-24x24 (tamashihoshi)
https://opengameart.org/content/light-themed-weapons (ScratchIO)
https://opengameart.org/content/spike-ball © 2005-2013 Julien Jorge <julien.jorge@stuff-o-matic.com>
https://opengameart.org/content/skill-item-and-spell-icons (pauliuw)
https://opengameart.org/content/walking-ant-with-parts-and-rigged-spriter-file (DudeMan)
https://opengameart.org/content/spider-flare-sprite-sheets (Wciow and John.d.h)
https://opengameart.org/content/weapon-icons-32x32px-painterly (Scrittl)
https://opengameart.org/content/armor-icons-32x32-px-painterly (Scrittl)
https://opengameart.org/content/character-equipment-slots (lukems-br)
https://opengameart.org/content/gold-treasure-icons (Clint Bellanger)
http://opengameart.org/users/varkalandar (Hansjörg Malthaner)
https://opengameart.org/content/various-stones-and-oregem-veins-16x16 (Senmou)
"""


SPRITE_ATTRIBUTION_OGA_2 = """
https://opengameart.org/content/poison-skull (Jorge Avila)
https://opengameart.org/content/arcane-magic-effect (Cethiel)
https://opengameart.org/content/castle-door (Castle door by Tuomo Untinen)
https://opengameart.org/content/whirlwind (Spring)
https://opengameart.org/content/lightning-shock-spell (Clint Bellanger)
https://opengameart.org/content/recursive-cave-entrance (Makrohn)
https://opengameart.org/content/conveyor-belts-spritesheet-anims (Color Optimist)
https://opengameart.org/content/spikey-stuff (dravenx)
https://opengameart.org/content/skeleton-0 (Andrew Garcell)
https://opengameart.org/content/lpc-animated-torch (William.Thompsonj, Hugh Spectrum, and Sharm)
https://opengameart.org/content/a-creepy-dead-tree (pixeltroid)
https://opengameart.org/content/cementery-gate (zerberros)
https://opengameart.org/content/parchment (Mattias Lejbrink)
https://opengameart.org/content/knarled-trees (Onsemeliot)
https://opengameart.org/content/key-icons (BizmasterStudios)
https://opengameart.org/content/shovel-1 (AntumDeluge)
https://opengameart.org/content/the-spookiest-skeleton (RedVoxel)
https://opengameart.org/content/samurai-japan (WarmGuy)
https://opengameart.org/content/loyalty-lies-equipment-bows Heather Lee Harvey (EmeraldActivities.com) & http://opengameart.org/users/emerald
https://opengameart.org/content/wesnoth-attack-icons  
"""


SPRITE_ATTRIBUTION_OTHER = """
《三国杀Online》https://web.sanguosha.com/ 
《剑网3》https://jx3.xoyo.com/
《九阴九阳》https://www.9game.cn/jyjy/
《笑傲江湖OL》http://xa.wanmei.com/
《铁血武林》,《武林英雄传》https://txwl.hcplay.com.cn/
https://neverwintervault.org/project/nwn2/images/icons/new-nwn2-icons
https://neverwintervault.org/project/nwn1/images/icons/new-bag-icons
http://dustychest.blogspot.com/ (DustyChest@gmail.com)
http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/ (GPL3 and CC-BY-SA3)
https://opengameart.org/content/arrow-0 (Michael J Pierce)
https://en.wikipedia.org/wiki/File:Male_Missulena_occatoria_spider_-_cropped.JPG (https://creativecommons.org/licenses/by-sa/3.0/deed.en)
https://en.wikipedia.org/wiki/File:01_Schwarzb%C3%A4r.jpg (https://creativecommons.org/licenses/by-sa/3.0/deed.en)
https://en.wikipedia.org/wiki/File:Echis_carinatus_sal.jpg (https://creativecommons.org/licenses/by/2.5/deed.en)
https://commons.wikimedia.org/wiki/File:Steinl%C3%A4ufer_(Lithobius_forficatus)_1.jpg (https://creativecommons.org/licenses/by-sa/2.5/deed.en)
https://commons.wikimedia.org/wiki/File:Saturnid_moth_(Lonomia_electra).jpg (https://creativecommons.org/licenses/by-sa/4.0/deed.en)
https://commons.wikimedia.org/wiki/File:Hyles_gallii,_Lodz(Poland)04(js).jpg (https://creativecommons.org/licenses/by-sa/3.0/deed.en)
https://en.wikipedia.org/wiki/Automeris_io#/media/File:Automeris_ioFMPCCA20040704-2974B1.jpg (https://creativecommons.org/licenses/by-sa/2.5/deed.en)
https://en.wikipedia.org/wiki/Cinnabar_moth#/media/File:Cinnabar_moth_(Tyria_jacobaeae).jpg (https://creativecommons.org/licenses/by-sa/4.0/deed.en)
https://en.wikipedia.org/wiki/Saturniidae#/media/File:Saturnia_pavonia_01.jpg (https://creativecommons.org/licenses/by-sa/3.0/deed.en)
https://en.wikipedia.org/wiki/Centipede#/media/File:Steinl%C3%A4ufer_(Lithobius_forficatus)_1.jpg (https://creativecommons.org/licenses/by/2.5/deed.en)
https://ccsearch.creativecommons.org/photos/6eabb363-1b42-41b1-9f9b-c6c6dfd300c5 "Scolopendra Heros" by CykoFlickr 
    is licensed under CC BY-NC 2.0 (https://creativecommons.org/licenses/by-nc/2.0/?ref=ccsearch)
http://www.softicons.com/toolbar-icons/soft-scraps-icons-by-deleket/save-icon (stockicons@deleket.com)
https://bkimg.cdn.bcebos.com/pic/f603918fa0ec08fa16dffd7657ee3d6d54fbda00?x-bce-process=image/watermark,g_7,image_d2F0ZXIvYmFpa2UxMTY=,xp_5,yp_5
https://en.wikipedia.org/wiki/Tai_chi#/media/File:Taijiquan_Symbol.png
https://open3dmodel.com/wp-content/uploads/2019/04/20190419_5cb9dd47c14ee.jpg
https://www.clipartmax.com/png/middle/340-3409704_tire-clipart-19-footsteps-clipart-freeuse-stock-transparent-foot-steps.png
'Rest' icon made by Freepik https://www.flaticon.com/authors/freepik
https://opengameart.org/content/computers-and-network-from-qnetwalk (Andi Peredri)
"""


SPRITE_ATTRIBUTION_BLOGNAVER = """
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220758840424&categoryNo=191&parentCategoryNo=0# (zzangogo7)
https://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220803168997&categoryNo=&parentCategoryNo=186&from=thumbnailList (zzangogo7)
https://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220755375870&categoryNo=&parentCategoryNo=186&from=thumbnailList (zzangogo7)
https://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220629596177&categoryNo=&parentCategoryNo=186&from=thumbnailList (zzangogo7)
https://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220617905735&categoryNo=&parentCategoryNo=186&from=thumbnailList (zzangogo7)
https://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220747533426&categoryNo=&parentCategoryNo=186&from=thumbnailList (zzangogo7)
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220831287447&categoryNo=&parentCategoryNo=186&from=thumbnailList
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220620529163&categoryNo=&parentCategoryNo=186&from=thumbnailList
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220663655095&categoryNo=&parentCategoryNo=186&from=thumbnailList
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220620057635&categoryNo=0&parentCategoryNo=186
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220626106747&categoryNo=0&parentCategoryNo=186
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220820133287&categoryNo=0&parentCategoryNo=186
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220819100355&categoryNo=0&parentCategoryNo=186
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220754746317&categoryNo=0&parentCategoryNo=186
"""


ANIMATION_ATTRIBUTION = """
http://www.mrtiredmedia.com/lets-discuss-weapon-types-and-basic-attacks/
https://www.pinterest.com/pin/681099143621907078/
https://www.pinterest.com/pin/663295851355925832/
https://www.pinterest.com/pin/289426713533081637/
https://www.pinterest.com/pin/329748003950428989/
https://ar.pinterest.com/pin/721561171537399677/
https://i.pinimg.com/originals/da/b1/f5/dab1f5216d739fd75751704f7efdb065.gif
https://orangemushroom.files.wordpress.com/2013/04/shadow-burn-effect.gif
https://orangemushroom.files.wordpress.com/2012/07/dragon-slash-2nd-enhancement-effect.gif?w=376&h=197
https://orangemushroom.files.wordpress.com/2013/05/panic-effect.gif?w=595
https://orangemushroom.files.wordpress.com/2013/05/rush-effect.gif?w=595
https://orangemushroom.files.wordpress.com/2012/12/blade-dancing-effect.gif?w=600
https://orangemushroom.files.wordpress.com/2013/05/blizzard-charge-effect.gif
http://orangemushroom.files.wordpress.com/2013/08/phantom-blow-effect.gif?w=595
https://gifer.com/en/517G
https://gifer.com/en/4pDP
https://gifimage.net/anime-effect-gif-3/
https://gifer.com/en/4pDP
https://gifer.com/en/4v9H
https://gifer.com/en/YlW9
https://gifer.com/en/4rFB
https://gifer.com/en/J9a9
https://gifer.com/en/1PZe
https://gifer.com/en/Gkgn
https://i.gifer.com/3klP.gif
https://giphy.com/gifs/trippy-gif-artist-ericaofanderson-e6aGKFKIf0SnpbOoLy
https://gfycat.com/gifs/search/trail
https://gfycat.com/obesereflectingbettong
https://gfycat.com/masculinezealousiraniangroundjay 
https://himekosutori.com/wp-content/uploads/2017/10/ClawSlashBlack.gif
https://cdn2.scratch.mit.edu/get_image/gallery/4602985_200x130.png
https://tenor.com/view/explosion-boom-gif-13902355
http://i982.photobucket.com/albums/ae307/x3TheAran59/35111011hit0.gif
https://www.artstation.com/artwork/6w2VN
"""


SFX_ATTRIBUTION = """
https://www.fesliyanstudios.com
http://soundbible.com/
https://www.zapsplat.com/
https://www.soundjay.com/
https://freesound.org/people/AmeAngelofSin/sounds/264982/
https://opengameart.org/content/large-monster (Michael Klier)
https://opengameart.org/content/sea-and-river-wave-sounds (RandomMind)
http://soundbible.com/2053-Thunder-Sound-FX.html (Grant Evans)
http://opengameart.org/users/varkalandar (Hansjörg Malthaner)
https://www.freesound.org/people/tcrocker68/sounds/235592/
https://opengameart.org/content/male-gruntyelling-sounds (HaelDB)
https://opengameart.org/content/rain-loopable (Ylmir)
https://opengameart.org/content/thunder-very-close-rain-01 (Inspector J)
https://www.freesoundeffects.com/free-sounds/thunder-sounds-10040/
https://opengameart.org/content/ghost (Ogrebane)
https://opengameart.org/content/evil-creature (A New Room)
https://opengameart.org/content/horror-scream1 (Vinrax)
https://freesound.org/people/hykenfreak/sounds/331621/
https://opengameart.org/content/35-wooden-crackshitsdestructions (Independent.nu)
https://freesound.org/people/MorneDelport/sounds/326407/ (MorneDelport)
https://opengameart.org/content/whip-sound (Whipping sound by Tuomo Untinen)
http://freesound.org/people/CGEffex/sounds/98341/ (CGEffex)
笑傲江湖 2001电视剧
"""


MUSIC_ATTRIBUTION = """
金庸群侠传 (蔡志展)
武林群侠传
水浒传之梁山好汉游戏
仙剑奇侠转 (蔡志展)
世间始终你好 （罗文，甄妮）
罗汉阵 (Rock Records 滾石國際音樂股份有限公司)
初识太极 (Rock Records 滾石國際音樂股份有限公司)
男儿当自强 （鲍比达，黄沾）
《金田一少年事件薄》 (见岳章)
欢沁 （林海）
琵琶语 （林海）
小鱼儿的思绪 (麦振鸿)
飘絮与天涯 (麦振鸿)
缘尽大海 (智冠超)
误入迷失森林 (韦启良)
张根失踪 (韦启良)
心殇 (麦振鸿)
波折
三个人的时光 (曾志豪，吴欣睿)
手拉手 (丁薇)
"""



UNUSED_EXTRAS = """
https://www.mop.com/shouyou/imgfile/202002/27/1582780678_3.jpg
"""