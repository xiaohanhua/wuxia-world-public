from tkinter import *
from character import *
from item import *
from random import *
import time
from gettext import gettext
from functools import partial
from helper_funcs import *
from special_move import *
from skill import *
from mini_game_dark_forest import*
import tkinter.messagebox as messagebox

_ = gettext


class Scene_dark_forest:

    def __init__(self, game):
        self.game = game

        if self.game.taskProgressDic["liu_village"] == 13:
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("If it wasn't for that random guy who showed up, we might've found it..."),
                 _("We'll have to go back at a later time, but for now, let's regroup with the others."), ],
                ["Masked Assassin", "Masked Assassin"],
                [[_("Follow them"), partial(self.dark_forest_ninja_encounter, 1)],
                 [_("Come back later"), partial(self.dark_forest_ninja_encounter, 2)]],
            )

        elif self.game.taskProgressDic["liu_village"] in [14,15]:
            if self.game.taskProgressDic["join_sect"] == 401:
                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("Leader, we're back!"),
                     _("Any progress?"),
                     _("Still haven't found the manual but we caught this kid. He's Liu Zhiyuan's youngest grandson."),
                     _("WAHHHHHHHH! Please let me go! Please..."),
                     _("Shut up, you brat..."),
                     _("Let me go! Let me go!!!"),
                     _("Hey! Get back here!"),  # row 2
                     _("*Riiiiiiip! The little boy's shirt rips as one of the masked assassins grabs him by the shoulder.*"),
                     _("Wait! Don't move!"),
                     _("Stop moving, you brat! Try anything funny again, and I'll chop your feet off!"),
                     _("Right there... There's something in the shirt that this boy is wearing..."),
                     _("Ah! Stay still kid... Let's see... Got it!"),
                     _("This... Leader! Could this be it?!"),  # row 3
                     _("Let me see..."),
                     _("Yes! AHAHAHAHA! This must be it!!! We've got the manual!"),
                     _("Come on, let's bring this back to the Boss..."),
                     _("Oh, as for the kid... put him to sleep... it's way past his bedtime...")
                     ],
                    ["Masked Assassin", "Masked Thief", "Masked Assassin", "Kid", "Masked Assassin", "Kid",
                     "Masked Assassin", "Blank", "Masked Thief", "Masked Assassin", "Masked Thief", "Masked Assassin",
                     "Masked Assassin", "Masked Thief", "Masked Thief", "Masked Thief", "Masked Thief"],
                )

                self.game.currentEffect = "rzc_laugh.wav"
                self.game.startSoundEffectThread()
                time.sleep(2)
                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("Who the heck was that?!"),
                     _("*A black shadow rushes out from the trees and takes the manual from the leader of the masked assassins, disappearing again almost immediately.*"),
                     _("The manual! Who is it?? Come out!!!"),
                     _("I... I don't know... I didn't see anyone..."),
                     _("It must be this guy's accomplice!"),
                     _("Wait, me?! I don't know that guy... He was so fast that I could barely see where he came from."),
                     _("Now that we've lost the manual, we have to capture this guy and bring it back to the boss."), #Row 2
                     _("He's going to demand an explanation..."),
                     _("Oh come on, really?")],
                    ["Masked Assassin", "Mysterious", "Masked Thief", "Masked Assassin", "Masked Thief", "you",
                     "Masked Thief", "Masked Thief", "you"]
                )

                self.game.mini_game_in_progress = True
                self.game.mini_game = dark_forest_mini_game(self.game, self)
                self.game.mini_game.new()

            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("Leader, we're back!"),
                     _("Any progress?"),
                     _("Still haven't found the manual but we caught this kid. He's Liu Zhiyuan's youngest grandson."),
                     _("WAHHHHHHHH! Please let me go! Please..."),
                     _("Shut up, you brat..."),
                     _("Let me go! Let me go!!!"),
                     _("Hey! Get back here!"), #row 2
                     _("*Riiiiiiip! The little boy's shirt rips as one of the masked assassins grabs him by the shoulder.*"),
                     _("Wait! Don't move!"),
                     _("Stop moving, you brat! Try anything funny again, and I'll chop your feet off!"),
                     _("Right there... There's something in the shirt that this boy is wearing..."),
                     _("Ah! Stay still kid... Let's see... Got it!"),
                     _("This... Leader! Could this be it?!"), #row 3
                     _("Let me see..."),
                     _("Yes! AHAHAHAHA! This must be it!!! We've got the manual!"),
                     _("Come on, let's bring this back to the Boss..."),
                     _("Oh, as for the kid... put him to sleep... it's way past his bedtime...")
                     ],
                    ["Masked Assassin", "Masked Thief", "Masked Assassin", "Kid", "Masked Assassin", "Kid",
                     "Masked Assassin", "Blank", "Masked Thief", "Masked Assassin", "Masked Thief", "Masked Assassin",
                     "Masked Assassin", "Masked Thief", "Masked Thief", "Masked Thief", "Masked Thief"],
                    [[_("Hey! Hand over the manual now!"), partial(self.dark_forest_ninja_encounter, 11)],
                     [_("Hey! Give me the kid!"), partial(self.dark_forest_ninja_encounter, 12)]],
                )

        elif self.game.taskProgressDic["liu_village"] == 1000 and self.game.taskProgressDic["join_sect"] == 401 and self.game.level >= 25:
            time.sleep(.5)
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("..."),
                 _("WHOA! Who are you....?"),
                 _("{}...").format(self.game.character),
                 _("H-how do you know my name????"),
                 _("Are you no longer able to recognize me simply because I switched a mask?"),
                 _("Or perhaps, it's how much stronger I've become since we last met?"),
                 _("............... (What's with his voice? It sounds strange... almost like Lu Xifeng's... but it can't be him...)"),  #Row 2
                 _("I suppose I can't blame you... "),
                 _("After all, it's natural for you to think I'm already dead since you personally tossed me into the cave..."),
                 _("Ren Zhichu???"),
                 _("HAHAHAHAHAHAHAHAHAHA!!!"),
                 _("Correct..."),
                 _("I-impossible... Even if you didn't starve, there's no way you could've escaped..."),  #Row 3
                 _("Hahahahahahaha! That day, when you threw me into the cave, I thought I was dead for sure..."),
                 _("But I didn't want to die yet... at the very least, I needed to take you and Li Guiping down with me..."),
                 _("In desperation, I started feeling around the walls of the cave looking for a soft spot."),
                 _("I was lucky and found a hollow spot. After punching through the rocks, I found a tunnel that led to the outside, near the base of the mountain."),
                 _("It appears that God was on my side. Not only did I escape, I found the manual for Phantom Sect's secret technique inside the tunnel..."),
                 _("The Yin Yang Soul Absorption Technique..."), #Row 4
                 _("This is the same technique that allowed Master to secure his place in the Elite Four."),
                 _("However, to practice this technique, one must first... first..."),
                 _("Heh... one needs to undergo castration..."),
                 _("C-castration...??? So that's why your voice..."),
                 _("Yep... but in order to get revenge, I was willing to do anything... HAHAHAHAHAHA..."),
                 _("Fearing I would be found before I had mastered the technique, I hid in these woods and practiced day and night..."), #Row 5
                 _("As fate would have it, a couple of weeks ago, I overheard those masked men talking about some manual..."),
                 _("When they found it, I snatched it away with ease, and now..."),
                 _("I've mastered the 2 most powerful techniques ever invented! AHAHAHAHAHAHAHA!!!"),
                 _("Which, by the way, means that there's not a single person alive who can save you now..."),
                 _("........."),
                 _("I'm going to kill you, and then I'll go find that witch and torture her slowly... HAHAHAHAHAHA!!!!"), #Row 6
                 _("You can do whatever you want to me, but I'm not letting you touch my wife..."),
                 _("It's too bad I can no longer do much more than inflict physical pain to her..."),
                 _("But I suppose slicing her flesh off piece by piece should quench my thirst for revenge..."),
                 _("I'll make sure Li Guiping gets what she deserves for all those years of humiliation I endured..."),
                 _("That's it, Ren Zhichu! You've gone too far! I will end your miserable life right now...")
                 ],
                ["Mysterious", "you", "Mysterious", "you", "Mysterious", "Mysterious",
                 "you", "Mysterious", "Mysterious", "you", "Mysterious", "Mysterious",
                 "you", "Mysterious", "Mysterious", "Mysterious", "Mysterious", "Mysterious",
                 "Mysterious", "Mysterious", "Mysterious", "Mysterious", "you", "Mysterious",
                 "Mysterious", "Mysterious", "Mysterious", "Mysterious", "Mysterious", "you",
                 "Mysterious", "you", "Mysterious", "Mysterious", "Mysterious", "you",
                 ]
            )

            opp = character(_("Mysterious"), 25,
                            sml=[special_move(_("Devil Crescent Moon Blade"), level=10), ],
                            skills=[skill(_("Yin Yang Soul Absorption Technique"), level=10),
                                    skill(_("Basic Agility Technique"), level=10),
                                    skill(_("Phantom Steps"), level=10)])
            self.game.battleMenu(self.game.you, opp, "battleground_dark_forest.png", "jy_suspenseful1.mp3", fromWin=None,
                                 battleType="task", postBattleCmd=self.post_ren_zhichu_battle)


        elif self.game.level <= 10 and self.game.taskProgressDic["join_sect"] == -1 and random() <= .75:
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("You there!"),
                 _("Whoaaa, what do you want?"),
                 _("Hand over 100 gold and you live... Otherwise, hehe..."),],
                ["Masked Robber", "you", "Masked Robber"],
                [[_("Pay 100 gold."), partial(self.dark_forest_robber_encounter, 1)],
                 [_("Fight!"), partial(self.dark_forest_robber_encounter, 2)]],
            )


        else:

            if self.game.taskProgressDic["liu_village"] == 14 and self.game.taskProgressDic["guo_zhiqiang"] != 0:
                self.game.taskProgressDic["liu_village"] += 1

            self.game.dark_forest_win = Toplevel(self.game.mapWin)
            self.game.dark_forest_win.title(_("Dark Forest"))

            bg = PhotoImage(file="Dark Forest.png")
            w = bg.width()
            h = bg.height()

            canvas = Canvas(self.game.dark_forest_win, width=w, height=h)
            canvas.pack()
            canvas.create_image(0, 0, anchor=NW, image=bg)

            #self.game.dark_forestF1 = Frame(canvas)
            #pic1 = PhotoImage(file="Xiao Yong_icon_large.ppm")
            #imageButton1 = Button(self.game.dark_forestF1, command=self.clickedOnXiaoHan)
            #imageButton1.grid(row=0, column=0)
            #imageButton1.config(image=pic1)
            #label = Label(self.game.dark_forestF1, text=_("Xiao Yong"))
            #label.grid(row=1, column=0)
            #self.game.dark_forestF1.place(x=w // 4 - pic1.width() // 2, y=h // 4 - pic1.height() // 2)

            menu_frame = self.game.create_menu_frame(self.game.dark_forest_win)
            menu_frame.pack()
            self.game.dark_forest_win_F1 = Frame(self.game.dark_forest_win)
            self.game.dark_forest_win_F1.pack()

            self.game.dark_forest_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
            self.game.dark_forest_win.update_idletasks()
            toplevel_w, toplevel_h = self.game.dark_forest_win.winfo_width(), self.game.dark_forest_win.winfo_height()
            self.game.dark_forest_win.geometry(
                "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
            self.game.dark_forest_win.focus_force()
            self.game.dark_forest_win.mainloop()  ###


    def dark_forest_ninja_encounter(self, choice):

        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        if choice == 1:
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Our client paid a large sum of money... We cannot screw this up."),
                 _("We need to provide him an update in 2 days. If we don't ---"),
                 _("...... We're being followed..."),
                 _("Who is it? Why not come out and say hi?"),
                 _("Hi...")],
                ["Masked Assassin", "Masked Assassin", "Masked Assassin", "Masked Assassin", "you"],
            )

            if self.game.taskProgressDic["liu_village"] == 13:
                self.game.taskProgressDic["liu_village"] = 14
                self.game.mini_game_in_progress = True
                self.game.mini_game = dark_forest_mini_game(self.game, self)
                self.game.mini_game.new()

        elif choice == 2:
            self.game.mapWin.deiconify()

        elif choice == 11:
            self.game.chivalry -= 30
            messagebox.showinfo("", _("Your chivalry -30!"))
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Not so fast, boys... Hand over that manual before I wipe the floor with your faces."),
                 _("Who the heck are you?"),
                 _("Don't waste time with this guy. I'm taking the manual back to the Boss. You guys get rid of him."),
                 _("Hey! Get back here!"),
                 _("Hah... You'll have to get past us first...")
                ],
                ["you", "Masked Assassin", "Masked Thief", "you", "Masked Assassin"],
            )
            self.game.mini_game_in_progress = True
            self.game.mini_game = dark_forest_mini_game(self.game, self)
            self.game.mini_game.new()

        elif choice == 12:
            self.game.chivalry += 30
            messagebox.showinfo("", _("Your chivalry +30!"))
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Hold it! You can take the manual, but leave the boy with me..."),
                 _("Who the heck are you?"),
                 _("Don't waste time with this guy. I'm taking the manual back to the Boss. You guys get rid of him."),
                 ],
                ["you", "Masked Assassin", "Masked Thief"],
            )
            self.save_kid()


    def dark_forest_ninja_battle(self):

        if self.game.taskProgressDic["liu_village"] == 15:

            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Stop him!"),
                 _("Annoying...")],
                ["Masked Assassin", "you"],
            )

            opp_list = []
            opp = character(_("Masked Assassin"), 15,
                            sml=[special_move(_("Smoke Bomb"), level=10),
                                 special_move(_("Spider Poison Powder")),
                                 special_move(_("Basic Sword Technique"), level=10)],
                            skills=[skill(_("Basic Agility Technique"))])
            opp_list.append(opp)
            opp = character(_("Masked Assassin"), 15,
                            sml=[special_move(_("Smoke Bomb"), level=10),
                                 special_move(_("Spider Poison Powder")),
                                 special_move(_("Basic Sword Technique"), level=10)],
                            skills=[skill(_("Basic Agility Technique"))])
            opp_list.append(opp)
            opp = character(_("Masked Assassin"), 16,
                            sml=[special_move(_("Smoke Bomb"), level=10),
                                 special_move(_("Spider Poison Powder")),
                                 special_move(_("Basic Sword Technique"), level=10)],
                            skills=[skill(_("Basic Agility Technique"))])
            opp_list.append(opp)

            if self.game.taskProgressDic["join_sect"] == 401:
                opp = character(_("Masked Thief"), 25,
                                sml=[special_move(_("Shaolin Diamond Finger"), level=10),
                                     special_move(_("Guan Yin Palm"), level=10),
                                     special_move(_("Smoke Bomb"), level=10),
                                     special_move(_("Eagle Claws"), level=10), ],
                                skills=[skill(_("Tendon Changing Technique"), level=7),
                                        skill(_("Basic Agility Technique"), level=10),
                                        skill(_("Spreading Wings"), level=10)])
                opp_list.append(opp)

            self.game.battleMenu(self.game.you, opp_list, "battleground_dark_forest.png",
                                 postBattleSoundtrack="jy_suspenseful2.mp3",
                                 fromWin=None,
                                 battleType="task", destinationWinList=[] * 3,
                                 postBattleCmd=self.post_dark_forest_ninja_battle
                                 )

        else:
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("You're not getting out of here alive today!"),
                 _("Oh really?")],
                ["Masked Assassin", "you"],
            )

            opp_list = []
            opp = character(_("Masked Assassin"), 10,
                            sml=[special_move(_("Smoke Bomb"), level=5),
                                 special_move(_("Spider Poison Powder")),
                                 special_move(_("Basic Sword Technique"), level=10)])
            opp_list.append(opp)
            opp = character(_("Masked Assassin"), 12,
                            sml=[special_move(_("Smoke Bomb"), level=5),
                                 special_move(_("Spider Poison Powder")),
                                 special_move(_("Basic Sword Technique"), level=10)])
            opp_list.append(opp)
            opp = character(_("Masked Assassin"), 12,
                            sml=[special_move(_("Smoke Bomb"), level=7),
                                 special_move(_("Spider Poison Powder")),
                                 special_move(_("Basic Sword Technique"), level=10)])
            opp_list.append(opp)

            self.game.battleMenu(self.game.you, opp_list, "battleground_dark_forest.png",
                                 postBattleSoundtrack="jy_suspenseful2.mp3",
                                 fromWin=None,
                                 battleType="task", destinationWinList=[] * 3,
                                 postBattleCmd=self.post_dark_forest_ninja_battle
                                 )


    def post_dark_forest_ninja_battle(self, choice=0):

        if self.game.taskProgressDic["liu_village"] == 15:
            if self.game.taskProgressDic["join_sect"] == 401:
                self.game.taskProgressDic["liu_village"] = 1000
                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("I lost... Do what you will; just make it quick..."),
                     _("Who are you, and why do you know Shaolin Kung-Fu?"),
                     _("I stole some manuals from their Scripture Library... Don't tell me you've never done that before."),
                     _("Who is the 'Boss' giving you all the orders?"),
                     _("Hahahaha... You will never find out... at least not in this life..."),
                     _("You'd better spit it out; otherwise, I'm going to turn you into a eunuch..."),
                     _("You can kill me, but I will not be humiliated!"),
                     _("Hold on, I---")
                    ],
                    ["Masked Thief", "you", "Masked Thief", "you", "Masked Thief", "you", "Masked Thief", "you"],
                )

                self.game.currentEffect = "sfx_sword6.mp3"
                self.game.startSoundEffectThread()
                time.sleep(1)

                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("*The masked thief stabs himself.*"),
                     _("... was just kidding..."),
                     _("WHY DID YOU TAKE ME SERIOUSLY??? YOU IDIOT!!!"),
                     _("Ah great... Maybe I really won't find out the truth... at least not in this life..."),
                     _("This is all Scrub's fault..."),
                     _("Well, surely this guy has something valuable on him for me to take...?")],
                    ["Blank", "you", "you", "you", "you", "you"],
                )

                self.game.add_item(_("Smoke Bomb"), 100)
                self.game.add_item(_("Gold"), 5000)
                messagebox.showinfo("", _("You search the body and find 'Smoke Bomb' x 100 and 'Gold' x 5000!"))
                self.game.mapWin.deiconify()
                return

            else:
                self.game.taskProgressDic["liu_village"] = 16
                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("Dang it! Where did he go???"),
                     _("Looks like I'll have to go to Phantom Sect and get the manual by force...")],
                    ["you", "you"],
                )
                self.game.mapWin.deiconify()
                return

        if choice == 0:
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Who are you guys working for?"),
                 _("Tell me the truth, and I'll let you live..."),
                 _("Haha, fool... you will never know the truth...")],
                ["you", "you", "Masked Assassin"],
                [[_("Beg him to tell you"), partial(self.post_dark_forest_ninja_battle,1)],
                 [_("Threaten to castrate him"), partial(self.post_dark_forest_ninja_battle,2)]],
            )

        if choice == 1:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Come on, man, please... My family's life depends on this..."),
                 _("I'm begging you man!!!"),
                 _("Ha... you're pathetic...")],
                ["you", "you", "Masked Assassin"],
            )
            self.game.currentEffect = "sfx_sword6.mp3"
            self.game.startSoundEffectThread()
            time.sleep(1)
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("*The masked assassin stabs himself.*"),
                 _("....................."),
                 _("Dang it... still no progress..."),
                 _("But it seems like this is one of their meeting locations. Maybe I should come back periodically to check for more activity.")],
                ["Blank", "you", "you", "you"],
            )
            self.game.mapWin.deiconify()

        if choice == 2:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Listen up, you better spit it out or I'm going to turn you into a eunuch..."),
                 _("I'm afraid you won't get a chance...")],
                ["you", "Masked Assassin"],
            )
            self.game.currentEffect = "sfx_sword6.mp3"
            self.game.startSoundEffectThread()
            time.sleep(1)
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("*The masked assassin stabs himself.*"),
                 _("....................."),
                 _("Dang it... still no progress..."),
                 _("But it seems like this is one of their meeting locations. Maybe I should come back periodically to check for more activity.")],
                ["Blank", "you", "you", "you"],
            )
            self.game.mapWin.deiconify()


    def save_kid(self):
        if self.game.taskProgressDic["liu_village"] == 16:
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Uncle! Uncle! Thank you for saving me!"),
                 _("No problem, kiddo..."),
                 _("Hey, I'm going to take you somewhere safe.")],
                ["Kid", "you", "you"],
            )
            self.game.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("Scrub, do me a favor, would ya? Keep this kid safe for me please."),
                 _("Ah, the kid from Liu Village? Sure..."),
                 _(".........."),
                 _("This is all your fault..."),
                 _("Hahaha... so, what's your plan now?"),
                 _("I gotta get the manual from Lu Xifeng; otherwise, it's going to be devastating for all of Wulin."),
                 _("I don't think the Martial Arts Community will benefit much if it falls into your hands either..."),
                 _("As long as that creep doesn't get it, I'm ok with any alternative..."),
                 _("Alright, no time to waste. I gotta go to Phantom Sect..."),
                 _("Hmmm... Ok... Have fun~")],
                ["you", "Scrub", "you", "you", "Scrub", "you", "Scrub", "you", "you", "Scrub"],
            )
            self.game.mapWin.deiconify()

        else:
            self.game.taskProgressDic["liu_village"] = 16
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("You want the kid? Come get him...")],
                ["Masked Assassin"],
            )

            opp_list = []
            opp = character(_("Masked Assassin"), 15,
                            sml=[special_move(_("Smoke Bomb"), level=10),
                                 special_move(_("Spider Poison Powder")),
                                 special_move(_("Basic Sword Technique"), level=10)],
                            skills=[skill(_("Basic Agility Technique"))])
            opp_list.append(opp)
            opp = character(_("Masked Assassin"), 15,
                            sml=[special_move(_("Smoke Bomb"), level=10),
                                 special_move(_("Spider Poison Powder")),
                                 special_move(_("Basic Sword Technique"), level=10)],
                            skills=[skill(_("Basic Agility Technique"))])
            opp_list.append(opp)
            opp = character(_("Masked Assassin"), 16,
                            sml=[special_move(_("Smoke Bomb"), level=10),
                                 special_move(_("Spider Poison Powder")),
                                 special_move(_("Basic Sword Technique"), level=10)],
                            skills=[skill(_("Basic Agility Technique"))])
            opp_list.append(opp)
            opp = character(_("Masked Assassin"), 16,
                            sml=[special_move(_("Smoke Bomb"), level=10),
                                 special_move(_("Spider Poison Powder")),
                                 special_move(_("Basic Sword Technique"), level=10)],
                            skills=[skill(_("Basic Agility Technique"))])
            opp_list.append(opp)

            self.game.battleMenu(self.game.you, opp_list, "battleground_dark_forest.png",
                                 postBattleSoundtrack="jy_suspenseful2.mp3",
                                 fromWin=None,
                                 battleType="task", destinationWinList=[] * 3,
                                 postBattleCmd=self.save_kid
            )


    def post_ren_zhichu_battle(self):
        self.game.taskProgressDic["liu_village"] = 1001
        self.game.taskProgressDic["potters_field"] = 0
        self.game.generate_dialogue_sequence(
            None,
            "Dark Forest.png",
            [_("Why... Why is life so unfair..."),
             _("Why do evil people like you and Li Guiping get away with everything..."),
             _("WHYYYYYYYYYYYYY????!!?!!!! AHHHHHHHHHHHH!!!"),
             _("............... How pitiful... You want to take your own life? Or do you want me to do it for you?"),
             _("I will be waiting for you in the potter's field near the foot of the mountain..."),
             _("*With lightning speed, Ren Zhichu dashes his head against a nearby tree, breaking his neck.*"),
             _("*As you stare into his lifeless eyes, you remember his final words and shudder.*"),
             _("(Hmmm, maybe I ought to ask Scrub what the heck he meant by that...)")
            ],
            ["Mysterious", "Mysterious", "Mysterious", "you", "Mysterious", "Blank", "Blank", "you"],
        )
        self.game.mapWin.deiconify()


    def dark_forest_robber_encounter(self, choice = 0):
        
        if choice == 0:
            r_amount = randrange(50,101)
            self.game.add_item(_("Gold"), r_amount)
            self.game.add_item(_("Iron Sword"))
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("You slay the robber and pick up: Gold x {}, Iron Sword x 1.").format(r_amount)],
                ["Blank"],
            )
            self.game.mapWin.deiconify()
        
        elif choice == 1:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            if self.game.check_for_item(_("Gold"), 100):
                messagebox.showinfo("", _("You pay the robber 100 gold and flee from the woods..."))
                self.game.mapWin.deiconify()
            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("I... here, this is all I have, man. Please let me go."),
                     _("I said 100! I will not settle for less!")],
                    ["you", "Masked Robber"],
                )

                opp = character(_("Masked Robber"), randrange(7, 9),
                                sml=[special_move(_("Basic Sword Technique"), level=8)])

                self.game.battleMenu(self.game.you, opp, "battleground_dark_forest.png", "jy_suspenseful1.mp3",
                                     fromWin=None, battleType="task", postBattleCmd=self.dark_forest_robber_encounter)
        
        elif choice == 2:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("How about... you hand me 100 gold, and I'll let you live?"),
                 _("You're asking for it...")],
                ["you", "Masked Robber"],
            )

            opp = character(_("Masked Robber"), randrange(7,9),
                                sml=[special_move(_("Basic Sword Technique"), level=8)])

            self.game.battleMenu(self.game.you, opp, "battleground_dark_forest.png", "jy_suspenseful1.mp3",
                                 fromWin=None, battleType="task", postBattleCmd=self.dark_forest_robber_encounter)
