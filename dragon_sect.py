from tkinter import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
import time
import tkinter.messagebox as messagebox


_ = gettext


class Scene_dragon_sect:

    def __init__(self, game, stage=None):
        self.game = game

        if stage == "Wudang":
            self.game.generate_dialogue_sequence(
                None,
                "Wudang Forest.png",
                [_("Alright! We're at the foot of the Wudang mountains. Almost there!"),
                 _("Thanks, {}! You're a really good friend.").format(self.game.character),
                 _("Haha, just doing what I can... Anyway, we should be there in about an hour."),
                ],
                ["you", "Ouyang Nana", "you"]
            )

            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Wudang Forest.png",
                [_("I'm afraid not..."),
                 _("And who are you, old man?"),
                 _("U-uncle?!"),
                 _("Uncle...???"),
                 _("Nana, we were worried to death about you!"),
                 _("(This must be Nana's cousin...)"),
                 _("Sorry, Shanshan-jie, I... but... how did you find us?"),
                 _("I knew you'd be around here, looking for that Xu Jun. I see you've found him, eh?"),
                 ],
                ["Ouyang Xiong", "you", "Ouyang Nana", "you", "Ouyang Shanshan", "you", "Ouyang Nana", "Ouyang Xiong"],
                [[_("Yep, we're together now, and I'm going to marry Nana!"), partial(self.talk_to_ouyang_xiong_at_wudang, 1)],
                 [_("I'm not Xu Jun. I'm just a friend of hers."), partial(self.talk_to_ouyang_xiong_at_wudang, 2)],
                 [_("Do I look like I'm wearing Wudang priest robes?"), partial(self.talk_to_ouyang_xiong_at_wudang, 3)]]
            )


        elif stage == "Dragon Sect":
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect.png",
                [_("Uncle!!! I'm back!"),
                 _("Nana?? Is that you?!"),
                 _("Yes, Uncle! I'm back! Sorry for making you worry about me..."),
                 _("Where have you been, my child? I've looked everywhere for you!"),
                 _("And who is this, may I ask?"),
                 _("Ah, this is a new friend that I made. His name is {}.").format(self.game.character),
                 _("My utmost honor, Uncle Ouyang!"), #row 2
                 _("Nana! I'm so glad you're back!"),
                 _("Shanshan-jie~ Hehehe..."),
                 _("{}, this is my cousin, Ouyang Shanshan.").format(self.game.character),
                 _("Ah, greetings, Miss Ouyang~"),
                 _("Hi there, {}. Thank you for bringing Nana back safely.").format(self.game.character),
                 _("(Wow, another pretty lady... Wonder if she's already taken...)"), #row 3
                 _("So, tell me about this new friend of yours. How did you meet? Why have you brought him back to meet us?"),
                 _("Ah, {} and I just met recently. He's a super nice person, and I told him about... about my situation...").format(self.game.character),
                 _("And in fact, he... he has something to say to you, Uncle!"),
                 _("Is that so? What do you have to tell me, young man?")
                ],
                ["Ouyang Nana", "Ouyang Xiong", "Ouyang Nana", "Ouyang Xiong", "Ouyang Xiong", "Ouyang Nana",
                 "you", "Ouyang Shanshan", "Ouyang Nana", "Ouyang Nana", "you", "Ouyang Shanshan",
                 "you", "Ouyang Xiong", "Ouyang Nana", "Ouyang Nana", "Ouyang Xiong"],
                [[_("Please allow Nana to marry Xu Jun!"), partial(self.talk_to_ouyang_xiong, 1)],
                 [_("Your daughter is beautiful."), partial(self.talk_to_ouyang_xiong, 2)]]
            )


        elif stage == "Normal":
            self.game.mapWin.withdraw()
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_xiaoyaopai.mp3"
            self.game.startSoundtrackThread()
            self.game.dragon_sect_win = Toplevel(self.game.mapWin)

            bg = PhotoImage(file="Dragon Sect.png")
            w = bg.width()
            h = bg.height()

            canvas = Canvas(self.game.dragon_sect_win, width=w, height=h)
            canvas.pack()
            canvas.create_image(0, 0, anchor=NW, image=bg)

            F1 = Frame(canvas)
            pic1 = PhotoImage(file="Ouyang Xiong_icon_large.ppm")
            imageButton1 = Button(F1, command=self.clickedOnOuyangXiong)
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            label = Label(F1, text=_("Ouyang Xiong"))
            label.grid(row=1, column=0)
            F1.place(x=w // 4 - pic1.width() // 1.8, y=h // 2)

            if self.game.taskProgressDic["ouyang_nana"] != 100:
                F2 = Frame(canvas)
                pic2 = PhotoImage(file="Ouyang Nana_icon_large.ppm")
                imageButton2 = Button(F2, command=self.clickedOnOuyangNana)
                imageButton2.grid(row=0, column=0)
                imageButton2.config(image=pic2)
                label = Label(F2, text=_("Ouyang Nana"))
                label.grid(row=1, column=0)
                F2.place(x=w // 2 - pic1.width() // 2, y=h // 2)

            F3 = Frame(canvas)
            pic3 = PhotoImage(file="Ouyang Shanshan_icon_large.ppm")
            imageButton3 = Button(F3, command=self.clickedOnOuyangShanShan)
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=pic3)
            label = Label(F3, text=_("Ouyang Shanshan"))
            label.grid(row=1, column=0)
            F3.place(x=w * 3 // 4 - pic1.width() // 2.2, y=h // 2)


            self.game.dragon_sect_win_F1 = Frame(self.game.dragon_sect_win)
            self.game.dragon_sect_win_F1.pack()

            menu_frame = self.game.create_menu_frame(master=self.game.dragon_sect_win,
                                                     options=["Map", "Profile", "Inv", "Save", "Settings"])
            menu_frame.pack()

            self.game.dragon_sect_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
            self.game.dragon_sect_win.update_idletasks()
            toplevel_w, toplevel_h = self.game.dragon_sect_win.winfo_width(), self.game.dragon_sect_win.winfo_height()
            self.game.dragon_sect_win.geometry(
                "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
            self.game.dragon_sect_win.focus_force()
            self.game.dragon_sect_win.mainloop()


        else:
            self.game.generate_dialogue_sequence(
                self.game.mapWin,
                "Dragon Sect.png",
                [_("Sorry, but we're busy looking for my little cousin who went missing a few days ago."),
                 _("She's skinny, wearing white clothes, very pretty, and is in her late teens."),
                 _("If you see her, please bring her back. Thank you in advance!"),],
                ["Ouyang Shanshan", "Ouyang Shanshan", "Ouyang Shanshan"],
            )


    def clickedOnOuyangXiong(self):
        for widget in self.game.dragon_sect_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.dragon_sect_win_F1, text=_("Spar (Ouyang Xiong)"), command=partial(self.dragon_sect_spar, "Ouyang Xiong")).pack()


    def clickedOnOuyangNana(self):
        self.game.generate_dialogue_sequence(
            None,
            "Dragon Sect.png",
            [_("{}! You're back!").format(self.game.character),
             _("Good to see you again, Nana!"),
             _("Likwise, hehe~"),
             _("It gets a bit boring here... I wish I could go on an adventure with you..."),
             _("Make sure you practice diligently whatever Uncle teaches you.")],
            ["Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana", "you"],
        )


    def clickedOnOuyangShanShan(self):

        for widget in self.game.dragon_sect_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.dragon_sect_win_F1, text=_("Spar (Ouyang Shanshan)"), command=partial(self.dragon_sect_spar, "Ouyang Shanshan")).pack()
        Button(self.game.dragon_sect_win_F1, text=_("Talk"), command=self.talkToOuyangShanshan).pack()



    def talkToOuyangShanshan(self):
        if self.game.taskProgressDic["ouyang_nana"] == 100:
            self.game.generate_dialogue_sequence(
                self.game.dragon_sect_win,
                "Dragon Sect.png",
                [_("Nana, {}, welcome back.").format(self.game.character),
                 _("Shanshan-jie~!"),
                 _("Is {} treating you well?").format(self.game.character),
                 _("Of course! He doesn't dare bully me; isn't that right, {}?~").format(self.game.character),
                 _("Yes, my princess Nana, whatever you say~"),],
                ["Ouyang Shanshan", "Ouyang Nana", "Ouyang Shanshan", "Ouyang Nana", "you"],
            )

        else:
            self.game.generate_dialogue_sequence(
                self.game.dragon_sect_win,
                "Dragon Sect.png",
                [_("{}, welcome back. Nana's been talking about you recently, wondering when you'd visit.").format(self.game.character),
                 _(""),],
                ["Ouyang Shanshan", "you"],
            )


    def dragon_sect_spar(self, targ):
        if targ == "Ouyang Xiong":
            self.game.dragon_sect_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect.png",
                [_("Let's see if you've made any progress lately..."),],
                ["Ouyang Xiong"],
            )

            opp = character(_("Ouyang Xiong"), 24,
                            sml=[special_move(_("Violent Dragon Palm"), level=10),
                                 special_move(_("Dragon Roar"), level=10)],
                            skills=[skill(_("Burst of Potential"), level=10),
                                    skill(_("Basic Agility Technique"), level=10)],
                            preferences=[5, 5, 3, 3, 2])

        elif targ == "Ouyang Shanshan":
            opp = character(_("Ouyang Shanshan"), 14,
                            sml=[special_move(_("Violent Dragon Palm"), level=4),
                                 special_move(_("Basic Sword Technique"), level=10),
                                 special_move(_("Fairy Sword Technique"), level=7)],
                            skills=[skill(_("Burst of Potential"), level=5),
                                    skill(_("Basic Agility Technique"), level=8)],)

        self.game.battleMenu(self.game.you, opp, "Dragon Sect.png",
                             postBattleSoundtrack="jy_xiaoyaopai.mp3",
                             fromWin=self.game.dragon_sect_win,
                             battleType="training", destinationWinList=[self.game.dragon_sect_win]*3,
                             )


    def talk_to_ouyang_xiong(self, choice):

        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        if choice == 1:
            self.game.taskProgressDic["ouyang_nana"] += 5

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect.png",
                [_("Ah, your daughter Shanshan is exceedingly beautiful."),
                 _("................."),
                 _("Haha... thank you for the compliment, {}, but surely you have more to say?").format(self.game.character),
                 _("Erm, yes... I actually want to talk about Nana and Xu Jun."),
                 ],
                ["you", "Ouyang Nana", "Ouyang Shanshan", "you"]
            )

        self.game.generate_dialogue_sequence(
            None,
            "Dragon Sect.png",
            [_("Hahahaha... {}, you are a guest here. If you wish to look around, have a meal with us, or anything of that sort, you are more than welcome.").format(self.game.character),
             _("But I would advise you not to get involved with our family matters..."),
             _("Uncle, I understand your concerns, but I guarantee you, Wudang Disciples are very chivalrous."),# Row 2
             _("Furthermore, their skills in combat are excellent, so Xu Jun will be able to take good care of---"),
             _("Enough! Do you think that I am unaware of everything you just said?"),
             _("Plus, I have other plans for who my niece will marry..."),
             _("No! I'm not going to marry anyone else! I like Xu Jun, and that's that!"),
             _("Nana, do you really think you know better than me?"),
             _("Uncle, I think Nana is old enough to make her own decisions, especially something important like marriage."),# Row 3
             _("Please, just let her choose for herself."),
             _("It's my fault... I've spoiled this girl too much..."),
             _("Nana, I'm afraid I'm going to have to assign some of my disciples to supervise you to make sure you don't run off again."),
             _("As for this friend of yours, I will have him stay in the stone chamber that I use for training."),
             _("I will find a suitable husband for you within a month. Once you are married, your heart will settle down."),
             _("Uncle, please, don't do this to me!"),  # Row 4
             _("Don't try anything funny; otherwise, this friend of yours will have to suffer for it."),
             _("(Dang it... what did I do to deserve this kind of treatment?)"),
             _("Sorry, {}, I really didn't want to do this, but it's for Nana's sake. I hope you understand.").format(
                 self.game.character),
             _("As long as you don't try to escape, you can enjoy your stay here for a month."),
             _("I'll make sure you are well fed and taken care of."),
             _("Alright, I guess I'll treat it like a vacation..."),  # Row 5
             _("Then please come this way to the chamber..")
             ],
            ["Ouyang Xiong", "Ouyang Xiong",
             "you", "you", "Ouyang Xiong", "Ouyang Xiong", "Ouyang Nana", "Ouyang Xiong",
             "you", "you", "Ouyang Xiong", "Ouyang Xiong", "Ouyang Xiong", "Ouyang Xiong",
             "Ouyang Nana", "Ouyang Xiong", "you", "Ouyang Xiong", "Ouyang Xiong", "Ouyang Xiong",
             "you", "Ouyang Xiong"
             ]
        )

        self.game.generate_dialogue_sequence(
            None,
            "Dragon Sect Cave.png",
            [_("Here we are. This is where you will be staying, {}.").format(self.game.character),
             _("I will make sure to have someone bring you meals daily, and if you need something just tell one of the disciples."),
             _("I'll try to enjoy my stay then, thanks..."),
             _("I'm sorry, {}... Don't worry, I'll visit you so you don't get bored!").format(self.game.character),
             _("Haha, sounds good!"),
             _("(Hmmm, I gotta find something to do to keep me busy...)")
            ],
            ["Ouyang Xiong", "Ouyang Xiong", "you", "Ouyang Nana", "you", "you"]
        )

        self.dragon_sect_cave()


    def talk_to_ouyang_xiong_at_wudang(self, choice):

        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        if choice == 1:
            self.game.generate_dialogue_sequence(
                None,
                "Wudang Forest.png",
                [_("Oh good, that saves me from having to look for you."),
                 _("I'm going to end your life now so you can stop being a bad influence to Nana."),
                 _("Wait! Uncle! He's not Xu Jun! He's just a friend I met recently."),
                 _("His name is {}. I told him my story, and he agreed to take me to Wudang to find Xu Jun...").format(self.game.character),
                 _("Even so, he's still being a bad influence, is he not?"),
                 _("Both of you, come back with me to Dragon Sect immediately."),
                 _("I'm going to make sure you don't run away from home again. As for this 'friend' of yours..."), #Row 2
                 _("He can stay in the stone chamber until I confirm his identity. Don't worry, he will be fed and clothed."),
                 _("Nice, free hotel? I don't mind, as long as there's something to do."),
                 _("You can do whatever you'd like, as long as you don't try to leave the chamber without my permission."),
                 _("Now come on, let's go."),
                 _("*You travel with the Ouyang family back to Dragon Sect and are escorted to the stone chamber*")
                 ],
                ["Ouyang Xiong", "Ouyang Xiong", "Ouyang Nana", "Ouyang Nana", "Ouyang Xiong", "Ouyang Xiong",
                 "Ouyang Xiong", "Ouyang Xiong", "you", "Ouyang Xiong", "Ouyang Xiong", "Blank"],
            )

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                None,
                "Wudang Forest.png",
                [_("His name is {}. I told him my story, and he agreed to take me to Wudang to find Xu Jun...").format(self.game.character),
                 _("Just what type of friends are you making, Nana?"),
                 _("Both of you, come back with me to Dragon Sect immediately."),
                 _("I'm going to make sure you don't run away from home again. As for this 'friend' of yours..."), #Row 2
                 _("He can stay in the stone chamber until I confirm his identity. Don't worry, he will be fed and clothed."),
                 _("Nice, free hotel? I don't mind, as long as there's something to do."),
                 _("You can do whatever you'd like, as long as you don't try to leave the chamber without my permission."),
                 _("Now come on, let's go."),
                 _("*You travel with the Ouyang family back to Dragon Sect and are escorted to the stone chamber*")
                 ],
                ["Ouyang Nana", "Ouyang Xiong", "Ouyang Xiong",
                 "Ouyang Xiong", "Ouyang Xiong", "you", "Ouyang Xiong", "Ouyang Xiong", "Blank"],
            )

        elif choice == 3:
            self.game.generate_dialogue_sequence(
                None,
                "Wudang Forest.png",
                [_("Hahahaha... you have a point, but how do I know you didn't change your clothes before running away with Nana?"),
                 _("Uncle, he's not Xu Jun. He's a friend that I met recently."),
                 _("His name is {}. I told him my story, and he agreed to take me to Wudang to find Xu Jun...").format(self.game.character),
                 _("Just what type of friends are you making, Nana?"),
                 _("Both of you, come back with me to Dragon Sect immediately."),
                 _("I'm going to make sure you don't run away from home again. As for this 'friend' of yours..."), #Row 2
                 _("He can stay in the stone chamber until I confirm his identity. Don't worry, he will be fed and clothed."),
                 _("Nice, free hotel? I don't mind, as long as there's something to do."),
                 _("You can do whatever you'd like, as long as you don't try to leave the chamber without my permission."),
                 _("Now come on, let's go."),
                 _("*You travel with the Ouyang family back to Dragon Sect and are escorted to the stone chamber*")
                 ],
                ["Ouyang Xiong", "Ouyang Nana", "Ouyang Nana", "Ouyang Xiong", "Ouyang Xiong",
                 "Ouyang Xiong", "Ouyang Xiong", "you", "Ouyang Xiong", "Ouyang Xiong", "Blank"],
            )


        self.dragon_sect_cave()


    def dragon_sect_cave(self):
        self.chamber_count = 0
        self.game.stopSoundtrack()
        self.game.currentBGM = "jy_xiaoyaopai.mp3"
        self.game.startSoundtrackThread()
        self.game.dragon_sect_chamber_win = Toplevel(self.game.mapWin)
        self.game.dragon_sect_chamber_win.title(_("Stone Chamber"))

        bg = PhotoImage(file="Dragon Sect Cave.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.dragon_sect_chamber_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        Button(self.game.dragon_sect_chamber_win, text="Punch the walls", command=self.dragon_sect_cave_punch_walls).pack()
        Button(self.game.dragon_sect_chamber_win, text="Do backflips off of the walls", command=self.dragon_sect_cave_backflips).pack()
        Button(self.game.dragon_sect_chamber_win, text="Train alone", command=self.dragon_sect_cave_train).pack()

        menu_frame = self.game.create_menu_frame(master=self.game.dragon_sect_chamber_win,
                                                 options=["Profile", "Inv", "Settings"])
        menu_frame.pack()

        self.game.dragon_sect_chamber_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.dragon_sect_chamber_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.dragon_sect_chamber_win.winfo_width(), self.game.dragon_sect_chamber_win.winfo_height()
        self.game.dragon_sect_chamber_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.dragon_sect_chamber_win.focus_force()
        self.game.dragon_sect_chamber_win.mainloop()


    def dragon_sect_cave_punch_walls(self):
        r = pickOne([1,1,2])
        self.game.strength += r
        messagebox.showinfo("", _("You spend some time punching the walls. Your strength + {}.").format(r))
        self.dragon_sect_cave_day_increment()


    def dragon_sect_cave_backflips(self):
        r = pickOne([1,1,2])
        self.game.dexterity += r
        messagebox.showinfo("", _("You spend some time practicing backflips. Your dexterity + {}.").format(r))
        self.dragon_sect_cave_day_increment()

    def dragon_sect_cave_train(self):
        try:
            random_move = pickOne([m for m in self.game.sml if m.stamina > 0 and m.level < 10])
            random_move.gain_exp(200)
            messagebox.showinfo("", _("You practice for 1 day and gain 200 exp in {}.").format(random_move.name))
        except:
            self.game.gainExp(1000)
            messagebox.showinfo("", _("You practice for 1 day and gain 1000 exp."))

        self.dragon_sect_cave_day_increment()


    def dragon_sect_cave_day_increment(self):
        self.chamber_count += 1
        if self.chamber_count == 2:
            self.game.dragon_sect_chamber_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("Hey, {}!!!").format(self.game.character),
                 _("Nana! What are you doing here?"),
                 _("Shhhh... I'm not supposed to be here, but I wanted to see how you were doing."),
                 _("I'm alright; keeping myself busy."),
                 _("I'm sorry... It's all my fault that you're stuck here..."),
                 ],
                ["Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana"],
                [[_("Don't say that! I'm willing to do anything for you!"), partial(self.dragon_sect_cave_dialogue, 1)],
                 [_("I'm hungry... Do you have any food?"), partial(self.dragon_sect_cave_dialogue, 2)]]
            )

        elif self.chamber_count == 6:
            self.game.dragon_sect_chamber_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("Hey, {}!!!").format(self.game.character),
                 _("Nana, welcome back!"),
                 _("Good to see you're still alive and well hehe..."),
                 ],
                ["Ouyang Nana", "you", "Ouyang Nana"],
                [[_("Did your uncle change his mind?"), partial(self.dragon_sect_cave_dialogue, 11)],
                 [_("How's your cousin, Shanshan?"), partial(self.dragon_sect_cave_dialogue, 12)]]
            )

        elif self.chamber_count == 11:
            self.game.dragon_sect_chamber_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("{}... Hey... I have something urgent to tell you...").format(self.game.character),
                 _("Hey, Nana! What's up?"),
                 _("My uncle... he... he arranged for me to get married to General Feng Pan's son..."),
                 _("They chose the date to be 20 days from now."),
                 _("Wait, your uncle wants to get involved with the government? Is this some sort of political move?"),
                 _("I think so... General Feng has military power in 3 provinces."),
                 _("Uncle probably wants to strengthen his position in Wulin through relationships with the government."),
                 _("And he's sacrificing you to do it..."),
                 _("{}, what should I do???").format(self.game.character)
                 ],
                ["Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana","you", "Ouyang Nana",
                 "Ouyang Nana", "you", "Ouyang Nana"],
                [[_("Nana, marry me instead!"), partial(self.dragon_sect_cave_dialogue, 21)],
                 [_("Well, it might not be that bad. You'll have power and riches."), partial(self.dragon_sect_cave_dialogue, 22)]]
            )

        elif self.chamber_count == 13:
            self.game.dragon_sect_chamber_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("*You hear the sound of footsteps approaching...*"),
                 _("Nana!"),
                 _("Sorry, {}. It's me; I brought you some food.").format(self.game.character),
                 _("Oh hi, thanks, Shanshan. Where's Nana?"),
                 _("Well, she spoke to my father about marrying you. He wasn't happy to hear that at all."),
                 _("As expected... I suppose it's difficult for him to cancel the arrangements with General Feng's son..."),
                 _("{}, Nana has told me a lot about you. Almost every day she mentions your name.").format(self.game.character),
                 _("Growing up, I've never met a man who's so brave and selfless like you..."),
                 _("Nana sure is a lucky girl..."),
                 _("Ah, she probably exaggerated a bit..."),
                 _("I know I'm not as pretty as Nana, and we've just met recently..."),
                 _("But I'm not expecting you to marry me. I just... I just want to spend a night with you, {}.").format(self.game.character),
                 _("Afterwards, I'll sneak you out of here."),
                 _("*Ouyang Shanshan steps forward and places her right hand on your cheek.*"),
                 ],
                ["Blank", "you", "Ouyang Shanshan", "you", "Ouyang Shanshan", "you", "Ouyang Shanshan", "Ouyang Shanshan",
                "Ouyang Shanshan", "you", "Ouyang Shanshan", "Ouyang Shanshan", "Ouyang Shanshan", "Blank"],
                [[_("Actually, I fell for you the moment I laid my eyes on you"), partial(self.dragon_sect_cave_dialogue, 31)],
                 [_("Take her hostage to escape and go find Nana"), partial(self.dragon_sect_cave_dialogue, 32)]]
            )


    def dragon_sect_cave_dialogue(self, choice):
        try:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
        except:
            pass
        if choice == 1:
            self.game.taskProgressDic["ouyang_nana"] += 10
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("{}... You're so nice to me... Thank you...").format(self.game.character),
                 _("Nana..."),
                 _("Oh! I almost forgot... You must be hungry. Here, I brought you some food hehe..."),
                 _("Thanks! I'm starving haha..."),
                 _("Ok, I have to go now before my uncle finds out. I'll try to visit your more!"),
                 _("Ok! Be careful, Nana! Come visit again soon!")
                 ],
                ["Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana", "you"],
            )

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("Oh! I almost forgot... Here, I brought you some food..."),
                 _("Thanks!"),
                 _("Ok, I have to go now before my uncle finds out. I'll try to visit your more!"),
                 _("Ok! See you later!")
                 ],
                ["Ouyang Nana", "you", "Ouyang Nana", "you"],
            )

        elif choice == 11:
            self.game.taskProgressDic["ouyang_nana"] += 10
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("Well... he seems to be very busy recently..."),
                 _("We've had a couple of guests over, but I'm not sure what they're discussing."),
                 _("Hmmm... I need to find a way to convince him..."),
                 _("Well, before that, let's get you out of here."),
                 _("Let me try to find out where the key to the chamber is kept. Hopefully, it's not with my uncle..."),
                 _("Ok! Thanks, Nana!")
                 ],
                ["Ouyang Nana", "Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana", "you"],
            )

        elif choice == 12:
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("She's doing fine... Why? Do you like her?"),
                 _("Huh? What? N-no... of course not... I'm just curious is all..."),
                 _("Uh-huh, sure... hey, it wouldn't be too bad if you married her. We would become one family hehe!"),
                 _("Ahahaha... yeah, that might be pretty cool..."),
                 _("Anyway, gotta run, see you soon!"),
                 _("Bye!")
                 ],
                ["Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana", "you"],
            )

        elif choice == 21:
            self.game.taskProgressDic["ouyang_nana"] += 10
            if self.game.taskProgressDic["ouyang_nana"] >= 35 and self.game.taskProgressDic["eagle_sect"] not in range(10,101) and self.game.taskProgressDic["wife_intimacy"] == -1 and _("Yin Yang Soul Absorption Technique") not in [s.name for s in self.game.skills]:
                self.game.generate_dialogue_sequence(
                    None,
                    "Dragon Sect Cave.png",
                    [_("{}, you... you're serious?").format(self.game.character),
                     _("Of course I am! Your uncle won't let you marry Xu Jun, but he might agree to our marriage."),
                     _("I know I am unworthy but... but... at least it's better than seeing you end up with a stranger."),
                     _("{}... After being with you the past couple of weeks and seeing how much you are willing to sacrifice for me, I...").format(self.game.character),
                     _("I've fallen in love with you..."),
                     _("Nana..."),
                     _("{}...").format(self.game.character),
                     _("I'm going to talk to my uncle and tell him about us. Surely he will release you then."),
                     _("Wait for me, {}...").format(self.game.character),
                     _("Be careful, Nana...")
                     ],
                    ["Ouyang Nana", "you", "you", "Ouyang Nana", "Ouyang Nana", "you", "Ouyang Nana",
                     "Ouyang Nana", "Ouyang Nana", "you"],
                )
            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Dragon Sect Cave.png",
                    [_("Quit joking, {}... We're just friends...").format(self.game.character),
                     _("Ahahaha... yeah, I was just kidding..."),
                     _("But what we should do is talk your uncle out of this. Tell him I need to see him."),
                     _("Ok, I'll try but... I doubt he's going to change his mind... Wait here for me."),
                     _("*2 hours later*"),
                     _("{}, how have you been enjoying your stay?").format(self.game.character),
                     _("Besides not being able to get fresh air, not as bad as I thought..."), #Row 2
                     _("I believe you've heard about my niece's upcoming marriage..."),
                     _("Yes... Do you really want her to marry some guy that she doesn't even know?"),
                     _("You've raised Nana since she was a child. Surely you love her and want what's best for her."),
                     _("Do you really think she will be happy in this forced marriage?"),
                     _("Sometimes, sacrifices have to be made for the bigger picture..."),
                     _("As a man, you can sacrifice yourself, but to sacrifice your own niece, where's the honor and dignity in that?"), #Row 3
                     _("Hah, so you speak of honor and dignity... well then... let me ask you."),
                     _("Are you man enough to have a duel with me? 1 on 1? If you win, I'll cancel this arrangement and let you go."),
                     _("Alternatively, you can get on the ground and beg for your life. I'll let you go, but don't ever return or get involved in our family business again!"),
                     _("{}, just go! You can't beat him! My uncle's too strong for you!").format(self.game.character),
                     ],
                    ["Ouyang Nana", "you", "you", "Ouyang Nana", "Blank", "Ouyang Xiong",
                     "you", "Ouyang Xiong", "you", "you", "you", "Ouyang Xiong",
                     "you", "Ouyang Xiong", "Ouyang Xiong", "Ouyang Xiong", "Ouyang Nana",],
                    [[_("Accept duel"), self.duel_with_ouyang_xiong],
                     [_("Beg for mercy"), partial(self.dragon_sect_cave_dialogue, 300)]]
                )

        elif choice == 22:
            self.game.chivalry -= 20
            messagebox.showinfo("", "Your chivalry -20!")
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("E-even you... think so...?"),
                 _("Perhaps... perhaps I ought to listen to uncle after all..."),
                 _("Well, he did raise you since you were a kid. I think he knows what's best for you."),
                 _("Yeah... you're right... Xu Jun and I are not meant to be together anyway..."),
                 _("Thanks, {}...").format(self.game.character),
                 _("*Ouyang Nana leaves with a lifeless expression on her face.*"),
                 _("*3 days later...*"),
                 _("{}... my father wants to see you... please come with me...").format(self.game.character),
                 ],
                ["Ouyang Nana", "Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana", "Blank", "Blank", "Ouyang Shanshan"],
            )
            self.dragon_sect_cave_dialogue(200)
            return

        elif choice == 31:
            self.game.chivalry -= 50
            messagebox.showinfo("", "Your chivalry -50!")
            self.game.healthMax += 200
            self.game.staminaMax += 200
            self.game.stamina = 0
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("{}... Let's waste no time and enjoy ourselves...").format(self.game.character),
                 _("*You spend an unforgettable night with Ouyang Shanshan.*"),
                 _("........................"),
                 _("Shanshan, that was amazing... By the way, you won't tell Nana about this, will you?"),
                 _("I don't want her to get hurt..."),
                 _("Of course I won't tell her..."),
                 _("Because... she already knows..."),
                 _("Wh-what?! What do you mean???"), #Row 2
                 _("Well, she probably saw the whole thing... I told her to stay outside when I came in and observe what happens..."),
                 _("As expected, you showed your true self..."),
                 _("You..."),
                 _("Nana's feelings for you were too strong that no matter what we said, she would not listen."),
                 _("So we decided to show her... Now that you've broken her heart, she will have no regrets marrying General Feng's son."),
                 _("How could you do this to your cousin?!"),
                 _("And how could you do this to the girl who trusted you and gave her heart to you?"),
                 _("I... I need to find her... Nana!!!"),
                 _("*You run to the main hall to look for Nana.*"),
                 _("Nana! Nana, where are you?"),
                 _("Hey, Uncle, have you seen Nana?"),
                 _("... U-uncle...?")
                 ],
                ["Ouyang Shanshan", "Blank", "Blank", "you", "you", "Ouyang Shanshan", "Ouyang Shanshan",
                 "you", "Ouyang Shanshan", "Ouyang Shanshan", "you", "Ouyang Shanshan", "Ouyang Shanshan",
                 "you", "Ouyang Shanshan", "you", "Blank", "you", "you", "you"],
            )
            self.dragon_sect_cave_dialogue(200)
            return

        elif choice == 32:
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("*You slowly caress Ouyang Shanshan's cheeks with your hand. As she leans forward to kiss you, you quickly grab her by the neck.*"),
                 _("H-hey! What... What are you doing?!"),
                 _("You really think I'd betray Nana? Keep dreaming..."),
                 _("Listen, you'd better let me out and take me to see Nana."),
                 _("You are Nana's cousin, so please don't make me hurt you..."),
                 _("{}!!!").format(self.game.character),
                 _("Nana!!!"),
                 _("I knew I didn't make a mistake in choosing you! Uncle and Shanshan-jie kept telling me you could not be trusted."),
                 _("But I had complete confidence in you!"),
                 _("Alright, you win, Nana... Now can you let me go, {}?").format(self.game.character),
                 _("*You release Ouyang Shanshan.*"),
                 _("Want to speak to my father? Come with me...")
                 ],
                ["Blank", "Ouyang Shanshan", "you", "you", "you", "Ouyang Nana", "you",
                 "Ouyang Nana", "Ouyang Nana", "Ouyang Shanshan", "Blank", "Ouyang Shanshan"],
            )

            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect.png",
                [_("Father... I've brought them here."),
                 _("You surprise me, {}... It seems you really love Nana.").format(self.game.character),
                 _("Of course, may I marry her now?"),
                 _("Yes, yes..."),
                 _("If you have the guts to fight me 1 on 1, that is..."),
                 _("I'm willing to do anything for Nana, even if it means losing my life to you."),
                 _("{}... Don't...").format(self.game.character),
                 _("Nana, if I can't be with you, I'd rather die, so if I have any chance at all, I'm going to try..."),
                 _("HAHAHAHAHA!!! I like that attitude. Come on then."),
                 _("Since I am an elder, I won't bully you... I'll only use 70% of my full strength against you."),
                 _("Let's go...")
                 ],
                ["Ouyang Shanshan", "Ouyang Xiong", "you", "Ouyang Xiong", "Ouyang Xiong", "you",
                 "Ouyang Nana", "you", "Ouyang Xiong", "Ouyang Xiong", "you"],
            )
            self.duel_with_ouyang_xiong()
            return

        elif choice == 200:
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect.png",
                [_("Nana... she... she killed herself..."),
                 _("Wh-what...?"),
                 _("Before she died, she left behind a note asking me to release you..."),
                 _("So go ahead. Leave, and never come back. Don't let me see you again; otherwise, you're dead."),
                 _("(Nana... ... ... what have I done...?)")
                 ],
                ["Ouyang Xiong", "you", "Ouyang Xiong", "Ouyang Xiong", "you"],
            )
            self.game.taskProgressDic["ouyang_nana"] = 200
            self.game.mapWin.deiconify()
            return

        elif choice == 300:
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect Cave.png",
                [_("Sorry Nana, your uncle's too strong."),
                 _("It would be suicide to fight him..."),
                 _("It's ok, {}, I shouldn't have gotten you involved in this.").format(self.game.character),
                 _("Mr. Ouyang, please let me go! I won't ever bother you again!"),
                 _("HAHAHAHAHA! Like I thought, a coward! Very well, get out of my sight... HAHAHAHAHA..."),
                 ],
                ["you", "you", "Ouyang Nana", "you", "Ouyang Xiong"],
            )
            self.game.taskProgressDic["ouyang_nana"] = 300
            self.game.mapWin.deiconify()
            return

        self.game.dragon_sect_chamber_win.deiconify()


    def duel_with_ouyang_xiong(self):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        opp = character(_("Ouyang Xiong"), 22,
                        sml=[special_move(_("Violent Dragon Palm"), level=10),
                             special_move(_("Dragon Roar"), level=10)],
                        skills=[skill(_("Burst of Potential"), level=10)],
                        preferences=[5,5,3,3,2])

        self.game.battleMenu(self.game.you, opp, "Dragon Sect.png",
                             postBattleSoundtrack="jy_xiaoyaopai.mp3",
                             fromWin=None,
                             battleType="test", destinationWinList=[],
                             postBattleCmd=self.post_duel_with_ouyang_xiong
        )


    def post_duel_with_ouyang_xiong(self):
        if self.game.taskProgressDic["ouyang_nana"] >= 35:
            time.sleep(.5)
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_romantic.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect.png",
                [_("Hahahahaha! I admire your courage, young man... Very few would dare to engage me in a fight."),
                 _("Though I wasn't sure about you at first, I am slowly warming up to you. I like your personality!"),
                 _("Tonight, I will host your wedding here. Go on now, you two, and get prepared."),
                 _("Thank you, Uncle!!!"),
                 _("Thanks, Uncle!!!"),
                 _("*That night, Ouyang Xiong officiated the wedding between you and Ouyang Nana, and you are now officially husband and wife.*"),
                 _("Ahahaha!!! It's a joyful day, indeed! Now, {}, my good nephew-in-law...").format(self.game.character),
                 _("As your wedding gift, I am going to teach you something that only senior disciples of Dragon Sect can learn...")
                 ],
                ["Ouyang Xiong", "Ouyang Xiong", "Ouyang Xiong", "Ouyang Nana", "you", "Blank",
                 "Ouyang Xiong", "Ouyang Xiong"],
            )
            
            if self.game.taskProgressDic["join_sect"] == 800 or self.game.luck >= 120:
                new_move = special_move(name=_("Dragon Roar"))
                self.game.learn_move(new_move)
                new_move = special_move(name=_("Violent Dragon Palm"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("You learned 2 new moves: 'Dragon Roar' and 'Violent Dragon Palm'!"))
            else:
                new_move = special_move(name=_("Dragon Roar"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("You learned a new move: 'Dragon Roar'!"))

            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect.png",
                [_("Thank you, Uncle!!!"),
                 _("Hahahaha... Now that you and Nana are married, go and fulfill your duties as husband and wife."),
                 _("I am getting quite sleepy, so good night."),
                 _("Good night, Uncle!"),
                 _("Thanks again, Uncle!"),
                 _("Hehe~"),
                 _("So..."),
                 _("Yes?~"),
                 _("Ahem... Uncle has been so good to us, so we ought to be very obedient and..."),
                 _("Hahaha~ You pervert~"),
                 _("Hey, we're married now hehehe... Are you ready?"),
                 _("*Ouyang Nana stares deeply into your eyes. Her eyes do all the talking. You embrace her passionately and begin to kiss her.*"),
                 _(".............................."),
                 _("*The next morning, after a lovely night with your wife, the two of you bid farewell to Ouyang Xiong and Ouyang Shanshan and begin your next adventure.*")
                 ],
                ["you", "Ouyang Xiong", "Ouyang Xiong", "Ouyang Nana", "you", "Ouyang Nana", "you",
                 "Ouyang Nana", "you",  "Ouyang Nana", "you", "Blank", "Blank", "Blank"],
            )
            self.game.healthMax += 200
            self.game.staminaMax += 200
            self.game.attack += 20
            self.game.strength += 20
            self.game.speed += 20
            self.game.defence += 20
            self.game.dexterity += 20
            self.game.taskProgressDic["ouyang_nana"] = 100
            self.game.taskProgressDic["wife_intimacy"] = 0

        else:
            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect.png",
                [_("I admire your courage, young man..."),
                 _("Very well, I will not force Nana to marry anyone against her will."),
                 _("Thanks Uncle! Thank you, {}!!!").format(self.game.character),
                 _("No problem, Nana."),
                 _("Well, since everything is finally resolved, I will take my leave."),
                 _("Wait! {}, I want you to have this, so you can remember me.").format(self.game.character),
                 ],
                ["Ouyang Xiong", "Ouyang Xiong", "Ouyang Nana", "you", "you", "Ouyang Nana"],
            )
            self.game.add_item(_("Phoenix Blade"))
            messagebox.showinfo("", _("Received item: 'Phoenix Blade'!"))

            self.game.generate_dialogue_sequence(
                None,
                "Dragon Sect.png",
                [_("Thanks Nana, I will try to visit when possible! Take care, Uncle and Shanshan!"),
                 _("Good-bye, young man."),
                 _("Bye, {}").format(self.game.character),
                 ],
                ["you", "Ouyang Xiong", "Ouyang Shanshan"],
            )
            self.game.taskProgressDic["ouyang_nana"] = 400

        self.game.stopSoundtrack()
        self.game.currentBGM = pickOne(["jy_huha.mp3", "jy_heroic.mp3", "jy_xiaoyaogu.mp3", "jy_xiaoyaogu3.mp3"])
        self.game.startSoundtrackThread()
        self.game.mapWin.deiconify()

