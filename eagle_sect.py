from tkinter import *
from character import *
from item import *
from random import *
import time
from gettext import gettext
from functools import partial
from helper_funcs import *
from special_move import *
from skill import *
from mini_game_explore_eagle_sect import*
import tkinter.messagebox as messagebox

_ = gettext

class Scene_eagle_sect:

    def __init__(self, game):

        self.game = game

        if self.game.taskProgressDic["eagle_sect"] == -1:
            self.game.taskProgressDic["eagle_sect"] = 0
            self.game.generate_dialogue_sequence(
                None,
                "battleground_eagle_sect.png",
                [_("Hmmm, strange... why is this place empty?"),
                 _("Did this sect disband or something?")],
                ["you", "you"]
            )


        self.game.eagle_sect_win = Toplevel(self.game.mapWin)
        self.game.eagle_sect_win.title(_("Eagle Sect"))

        bg = PhotoImage(file="battleground_eagle_sect.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.eagle_sect_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)


        self.game.eagle_sectF2 = Frame(canvas)
        pic2 = PhotoImage(file="forest_explore_icon.ppm")
        imageButton2 = Button(self.game.eagle_sectF2, command=self.explore)
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(self.game.eagle_sectF2, text=_("Explore"))
        label.grid(row=1, column=0)
        self.game.eagle_sectF2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 4 - pic2.height() // 2)


        menu_frame = self.game.create_menu_frame(self.game.eagle_sect_win)
        menu_frame.pack()
        self.game.eagle_sect_win_F1 = Frame(self.game.eagle_sect_win)
        self.game.eagle_sect_win_F1.pack()

        self.game.eagle_sect_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.eagle_sect_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.eagle_sect_win.winfo_width(), self.game.eagle_sect_win.winfo_height()
        self.game.eagle_sect_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.eagle_sect_win.focus_force()
        self.game.eagle_sect_win.mainloop()  ###


    def explore(self):
        response = messagebox.askquestion("", _("There may be some expensive herbs near the walls of the cliff. Explore?"))
        if response == "yes":
            self.game.eagle_sect_win.withdraw()
            self.game.mini_game_in_progress = True
            self.game.mini_game = explore_eagle_sect_mini_game(self.game, self)
            self.game.mini_game.new()


    def done_exploring(self, drops):
        self.game.mini_game_in_progress = False
        pg.display.quit()
        for drop in drops:
            self.game.add_item(drop)
        day = calculate_month_day_year(self.game.gameDate)["Day"]
        if self.game.taskProgressDic["eagle_sect"] == 0 and day >= 24:
            self.game.taskProgressDic["eagle_sect"] = 1
            if random() <= (self.game.speed + self.game.dexterity)/250:
                response = messagebox.askquestion("", _("You hear someone approaching. Hide and observe?"))
                if response == "yes":
                    self.interact_with_jiang_yuqi(battle=False)
                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "battleground_eagle_sect.png",
                        [_("Hey! What are you doing here?"),
                         _("I... uh... I'm just looking around..."),
                         _("You're not from Eagle Sect. Leave!"), ],
                        ["Jiang Yuqi", "you", "Jiang Yuqi"]
                    )

                    opp = character(_("Jiang Yuqi"), 14,
                                    sml=[special_move(_("Eagle Claws"), level=7),
                                         special_move(_("Fairy Sword Technique"), level=7)],
                                    skills=[skill(_("Spreading Wings"), level=5),
                                            skill(_("Basic Agility Technique"), level=10)],
                                    preferences=[5,3,5,3,3])

                    self.game.battleMenu(self.game.you, opp, "battleground_eagle_sect.png",
                                    postBattleSoundtrack="jy_shanguxingjin.mp3",
                                    fromWin=None,
                                    battleType="task", destinationWinList=[] * 3,
                                    postBattleCmd=self.interact_with_jiang_yuqi)
                return

            elif random() <= .5:
                self.game.generate_dialogue_sequence(
                    None,
                    "battleground_eagle_sect.png",
                    [_("Hey! What are you doing here?"),
                     _("I... uh... I'm just looking around..."),
                     _("You're not from Eagle Sect. Leave!"), ],
                    ["Jiang Yuqi", "you", "Jiang Yuqi"]
                )

                opp = character(_("Jiang Yuqi"), 14,
                                sml=[special_move(_("Eagle Claws"), level=7),
                                     special_move(_("Fairy Sword Technique"), level=7)],
                                skills=[skill(_("Spreading Wings"), level=5),
                                        skill(_("Basic Agility Technique"), level=10)],
                                preferences=[5,3,5,3,3])

                self.game.battleMenu(self.game.you, opp, "battleground_eagle_sect.png",
                                     postBattleSoundtrack="jy_shanguxingjin.mp3",
                                     fromWin=None,
                                     battleType="task", destinationWinList=[] * 3,
                                     postBattleCmd=self.interact_with_jiang_yuqi)

                return

        self.game.eagle_sect_win.deiconify()


    def interact_with_jiang_yuqi(self, battle=True):

        if battle:
            self.game.generate_dialogue_sequence(
                None,
                "battleground_eagle_sect.png",
                [_("*Using 2 fingers, you press into the Shenshu Acupuncture point on her waist, using 70% of your full force.*"),
                 _("*The young woman cries out in pain and falls limp to the ground, unable to move.*"),
                 _("Who... who are you? What do you want?")],
                ["Blank", "Blank", "Jiang Yuqi"],
                [[_("What do you think I want? Ehehehehe..."), partial(self.jiang_yuqi_choice, 1)],
                 [_("Sorry, Miss, I just want to ask you a few questions."), partial(self.jiang_yuqi_choice, 2)]]
            )

        else:
            self.game.generate_dialogue_sequence(
                None,
                "battleground_eagle_sect.png",
                [_("Father? Hello? Anyone home?"),
                 _("Where is everyone? Even if they left for something important, they should be back by now..."),
                 _("*The young woman, with a pensive look on her face, stops a few feet in front of the rock behind which you are hiding.*"),
                 _("*Noticing that she is still deep in thought and distracted, you rush out quickly from behind her.*"),
                 _("*Using 2 fingers, you press into the Shenshu Acupuncture point on her waist, using 70% of your full force.*"),
                 _("*The young woman cries out in pain and falls limp to the ground, unable to move.*"),
                 _("Who... who are you? What do you want?")],
                ["Jiang Yuqi", "Jiang Yuqi", "Blank", "Blank", "Blank", "Blank", "Jiang Yuqi"],
                [[_("What do you think I want? Ehehehehe..."), partial(self.jiang_yuqi_choice, 1)],
                 [_("Sorry, Miss, I just want to ask you a few questions."), partial(self.jiang_yuqi_choice, 2)]]
            )


    def jiang_yuqi_choice(self, choice):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        condition_1 = self.game.taskProgressDic["wife_intimacy"] >= 0 or self.game.taskProgressDic["join_sect"] == -1
        condition_2 = self.game.level <= 10 - self.game.luck//60
        condition_3 = self.game.taskProgressDic["castrated"] == 1 #_("Yin Yang Soul Absorption Technique") in [s.name for s in self.game.skills]

        if choice == 1:
            self.game.taskProgressDic["eagle_sect"] = 1000
            if self.game.luck < 2500 or self.game.chivalry >= -50 or condition_1 or condition_2 or condition_3 or VERSION != "FF":
                self.game.generate_dialogue_sequence(
                    None,
                    "battleground_eagle_sect.png",
                    [_("We're in the middle of nowhere, there's only the 2 of us, and you can't move right now..."),
                     _("What do you think I want? Ehehehehehe..."),
                     _("You... you shameless pervert! Even if I die, I will not let you get your way!"),
                     _("What? Wait, I---"),
                     _("*A stream of blood bursts from her mouth, and soon she stops breathing.*"),
                     _("*It appears that she committed suicide by biting her tongue.*"),
                     _("What were you thinking??? All I wanted to do was take your money... Geez..."),
                     _("You didn't have to kill yourself over that...")],
                    ["you", "you", "Jiang Yuqi", "you", "Blank", "Blank", "you", "you"],
                    [[_("Give her a proper burial"), partial(self.jiang_yuqi_choice, 11)],
                     [_("Search her body for some valuable items"), partial(self.jiang_yuqi_choice, 12)]]
                )
            else:
                self.game.chivalry -= 50
                messagebox.showinfo("", _("Your chivalry - 50!"))
                self.game.generate_dialogue_sequence(
                    None,
                    "battleground_eagle_sect.png",
                    [_("We're in the middle of nowhere, there's only the 2 of us, and you can't move right now..."),
                     _("What do you think I want? Ehehehehehe..."),
                     _("*You grab her feet by the ankles and proceed to remove her boots one by one.*"),
                     _("*As you bring her right foot towards your face, you detect an intoxicating scent of foot sweat mixed with leather.*"),
                     _("*You slowly caress her soles with your fingers, feeling her soft, smooth skin and a slight wetness from her sweat.*"),
                     _("*As you continue to inhale the scent from her feet, your face turns hot and your heart beat quickens.*"),
                     _("I can't resist anymore! Today, I'm going to make you mine..."),
                     _("You... you shameless pervert! Even if I die, I will not let you get your way!"),
                     _("*A stream of blood bursts from her mouth, and soon she stops breathing.*"),
                     _("*It appears that she committed suicide by biting her tongue.*"),],
                    ["you", "you", "Blank", "Blank", "Blank", "Blank", "you", "Jiang Yuqi", "Blank", "Blank"],
                    [[_("Give her a proper burial"), partial(self.jiang_yuqi_choice, 11)],
                     [_("Search her body for some valuable items"), partial(self.jiang_yuqi_choice, 12)]]
                )


        elif choice == 2:
            self.game.generate_dialogue_sequence(
                None,
                "battleground_eagle_sect.png",
                [_("Sorry, Miss, I don't mean you any harm. I just arrived here not long ago and found this place desserted."),
                 _("It seems rather strange, so as a precaution, I need to figure out who you are first."),
                 _("My name is Jiang Yuqi, the daughter of the head of Eagle Sect, Jiang Yuanqing."),
                 _("2 months ago, my dad sent me on a short errand, but when I returned, Eagle Sect was completely empty."),
                 _("I've gone to many places in the country to look for them but haven't had any luck so far."),
                 _("I come back here periodically to check if they've returned."),
                 _("And that's how you ran into me..."), #Row 2
                 _("Yep..."),
                 _("I see, sorry if I've offended you, Miss. Let me help you..."),
                 _("*You press on Jiang Yuqi's 'Taiyi' and 'Shuidao' acupuncture points. She lets out a soft groan and begins to slowly move her body again.*"),
                 _("Oww... Ok, much better... Thanks..."),
                 _("How should I address you?"),
                 _("You can call me {}.").format(self.game.character), #Row 3
                 _("Ok, {}, what brings you to Eagle Sect? Do you have any news of my father? Or anyone in Eagle Sect?").format(self.game.character),
                 _("I'm afraid not... Honestly, I just passed by here and decided to take a look around."),
                 _("So, tell me about Eagle Sect."),
                 _("Well, my dad created the sect 10 years ago, so we're still fairly new in the Martial Arts Community."),
                 _("As of now, we have about 35-40 members, but I believe that in a few years, that number will grow by 10 times."),
                 _("Quite ambitious, but to do that, we had better find your dad first."), #Row 4
                 _("We...?"),
                 _("Yeah, I'm always looking for adventures! I can help you find your dad!")],
                ["you", "you", "Jiang Yuqi", "Jiang Yuqi", "Jiang Yuqi", "Jiang Yuqi",
                 "you", "Jiang Yuqi", "you", "Blank", "Jiang Yuqi", "Jiang Yuqi",
                 "you", "Jiang Yuqi", "you", "you", "Jiang Yuqi", "Jiang Yuqi",
                 "you", "Jiang Yuqi", "you"],
            )

            if condition_1 or condition_2 or condition_3:
                self.game.generate_dialogue_sequence(
                    None,
                    "battleground_eagle_sect.png",
                    [_("Thanks, {}, but since we just met, I really can't bother you with such an important request.").format(self.game.character),
                     _("Plus, who knows what dangers lie ahead? I don't think you should put yourself at risk like that..."),
                     _("(Hmmm, there must be a reason she doesn't want me involved. I'd better respect her privacy.)"),
                     _("Ah, no worries, I understand. In that case, I wish you the best and hope you find him soon!"),
                     _("Thanks! I'll continue my search then. Take care!")],
                    ["Jiang Yuqi", "Jiang Yuqi", "you", "you", "Jiang Yuqi"]
                )
                self.game.taskProgressDic["eagle_sect"] = 1000
                self.game.eagle_sect_win.deiconify()

            else:
                self.game.taskProgressDic["eagle_sect"] = 11
                self.game.generate_dialogue_sequence(
                    None,
                    "battleground_eagle_sect.png",
                    [_("Thanks, {}! I'd love to have a companion join me!").format(self.game.character),
                     _("I'll definitely need all the help I can get!"),
                     _("Great! Let me take you to a friend's house and get you situated there."),
                     _("A young lady like you shouldn't be traveling around on her own."),
                     _("He has many connections in the Martial Arts Community, so if there's any news, he'll be one of the first to know."),
                     _("Thanks! You are very considerate."),
                     _("*You take Jiang Yuqi to Scrub's house. You can find and have further interactions with her there.*")],
                    ["Jiang Yuqi", "Jiang Yuqi", "you", "you", "you", "Jiang Yuqi", "Blank"]
                )
                self.game.winSwitch(self.game.eagle_sect_win, self.game.mapWin)

        elif choice == 11:
            self.game.chivalry += 10
            messagebox.showinfo("", _("Your chivalry + 10!"))
            self.game.generate_dialogue_sequence(
                None,
                "battleground_eagle_sect.png",
                [_("Digging a hole, digging a hole"),
                 _("Lay her to rest before I go"),
                 _("Pity she had to die this way"),
                 _("The world has lost a beauty today"),
                 _("I hate to bury her in this place"),
                 _("With her slim, little figure and beautiful face"),
                 _("If only she could somehow become my wife..."),
                 _("Maybe it's possible... in another life?"),
                 _("Dig-- wait, what is that??"),],
                ["you", "you", "you", "you", "you", "you", "you", "you", "you"]
            )

            self.game.add_item(_("Caterpillar Fungus"), 10)
            s = _("You notice something hard in the soil. As you continue to dig, you uncover the dirt and find a metal chest containing a pile of Caterpillar Fungus. Obtained 'Caterpillar Fungus' x 10!")
            messagebox.showinfo("", s)
            self.game.eagle_sect_win.deiconify()


        elif choice == 12:
            self.game.chivalry -= 20
            messagebox.showinfo("", _("Your chivalry - 20!"))
            if random() <= .7 or self.game.luck >= 250 or self.game.chivalry >= -50:
                self.game.add_item(_("Leather Boots [Ultra Rare]"))
                messagebox.showinfo("", _("You search her body and find 'Leather Boots [Ultra Rare]' x 1!"))
            else:
                self.game.add_item(_("Amethyst Necklace"))
                messagebox.showinfo("", _("You search her body and find 'Amethyst Necklace' x 1!"))
            self.game.eagle_sect_win.deiconify()
