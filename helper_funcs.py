from tkinter import*
from re import findall
from random import*
from datetime import*
from settings import*

def get_curr_screen_geometry():
    """
    Workaround to get the size of the current screen in a multi-screen setup.

    Returns:
        geometry (str): The standard Tk geometry string.
            [width]x[height]+[left]+[top]
    """
    root = Tk()
    root.update_idletasks()
    root.attributes('-fullscreen', True)
    root.state('iconic')
    width = root.winfo_width()
    height = root.winfo_height()
    root.destroy()
    return width, height


def center_all_windows(source_code):
    spaces_window_names = findall("([\t ]+)self\.([A-z]+)\.mainloop\(\)", source_code)
    replacement_text = """self.{}.update_idletasks()
{}toplevel_w, toplevel_h = self.{}.winfo_width(), self.{}.winfo_height()
{}self.{}.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
{}self.{}.mainloop()###"""
    for space, name in spaces_window_names:
        replacement_parameters = [name, space, name, name, space, name, space, name]
        targ = "self.{}.mainloop()\n".format(name)
        source_code = source_code.replace(targ, replacement_text.format(*replacement_parameters))
    return source_code


def force_focus_all_windows(source_code):
    spaces_window_names = findall("([\t ]+)self\.([A-z]+)\.mainloop\(\)", source_code)
    replacement_text = """self.{}.focus_force()
{}self.{}.mainloop()"""
    for space, name in spaces_window_names:
        replacement_parameters = [name, space, name]
        targ = "self.{}.mainloop()".format(name)
        source_code = source_code.replace(targ, replacement_text.format(*replacement_parameters))
    return source_code


def pickOne(aList):
    l = len(aList)
    i = randrange(l)
    return aList[i]


def average(aList):
    return sum(aList)/len(aList)


def isPrime(number):
    count = 0
    if number <= 1:
        return False
    for i in range(2, number):
        test = number % i
        if test == 0:
            count += 1
    if count < 1:
        return True
    else:
        return False


def calculate_month_day_year(day_number):
    dt = datetime(2020, 1, 1) + timedelta(day_number - 1)
    year = int(day_number // 365 + 1)
    month = dt.month
    day = dt.day

    game_date_dic = {}
    game_date_dic["Year"] = year
    game_date_dic["Month"] = month
    game_date_dic["Month_Str"] = MONTHS[month - 1]
    game_date_dic["Day"] = day
    return game_date_dic


def calculate_distance(a, b, x=None, y=None):
    if x and y:
        return ((x-a)**2 + (y-b)**2)**.5
    else:
        return ((a[0]-b[0])**2 + (a[1]-b[1])**2)**.5


SCREEN_WIDTH, SCREEN_HEIGHT = get_curr_screen_geometry()

if __name__ == "__main__":
    src = open("main.txt", "r").read()
    #new_src = center_all_windows(src)
    new_src = force_focus_all_windows(src)
    h = open("main_edited.txt", "w")
    h.write(new_src)
    h.close()