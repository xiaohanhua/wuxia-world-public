from tkinter import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
import tkinter.messagebox as messagebox
import time


_ = gettext


class Scene_huashan_train:

    def __init__(self, game):
        self.game = game
        
        self.game.days_spent_in_training = int((self.game.taskProgressDic["join_sect"] - 6) * 100)
        try:
            if self.game.taskProgressDic["join_sect"] == 6.0:
                try:
                    self.game.huashan_train_win = Toplevel(self.game.scrubHouseWin)
                except:
                    self.game.huashan_train_win = Toplevel(self.game.continueGameWin)

            else:
                self.game.huashan_train_win = Toplevel(self.game.continueGameWin)
        except:
            self.game.huashan_train_win = Toplevel(self.game.continueGameWin)

        self.game.huashan_train_win.title(_("Huashan"))

        bg = PhotoImage(file="Huashan.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.huashan_train_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.game.huashan_F1 = Frame(canvas)
        pic1 = PhotoImage(file="Guo Junda_icon_large.ppm")
        imageButton1 = Button(self.game.huashan_F1, command=partial(self.clickedOnGuoJunda, True))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.game.huashan_F1, text=_("Guo Junda"))
        label.grid(row=1, column=0)
        self.game.huashan_F1.place(x=w // 4 - pic1.width(), y=h // 3 - pic1.height())

        if self.game.days_spent_in_training < 58:
            self.game.huashan_F2 = Frame(canvas)
            pic2 = PhotoImage(file="Guo Zhiqiang_icon_large.ppm")
            imageButton2 = Button(self.game.huashan_F2, command=partial(self.clickedOnGuoZhiqiang, True))
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            label = Label(self.game.huashan_F2, text=_("Guo Zhiqiang"))
            label.grid(row=1, column=0)
            self.game.huashan_F2.place(x=w // 4 - pic2.width(), y=h // 3 + pic2.height())

        if  self.game.days_spent_in_training < 58:
            self.game.huashan_F3 = Frame(canvas)
            pic3 = PhotoImage(file="Zhao Huilin_icon_large.ppm")
            imageButton3 = Button(self.game.huashan_F3, command=partial(self.clickedOnZhaoHuilin, True))
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=pic3)
            label = Label(self.game.huashan_F3, text=_("Zhao Huilin"))
            label.grid(row=1, column=0)
            self.game.huashan_F3.place(x=w * 3 // 4, y=h // 3 - pic3.height())

        if True:
            self.game.huashan_F4 = Frame(canvas)
            pic4 = PhotoImage(file="huashan_cliff_icon_large.ppm")
            imageButton4 = Button(self.game.huashan_F4, command=self.huashan_practice)
            imageButton4.grid(row=0, column=0)
            imageButton4.config(image=pic4)
            label = Label(self.game.huashan_F4, text=_("Practice Alone"))
            label.grid(row=1, column=0)
            self.game.huashan_F4.place(x=w * 3 // 4, y=h // 3 + pic4.height())

        self.game.days_spent_in_training_label = Label(self.game.huashan_train_win,
                                                  text=_("Days trained: {}").format(self.game.days_spent_in_training))
        self.game.days_spent_in_training_label.pack()
        self.game.huashan_train_win_F1 = Frame(self.game.huashan_train_win)
        self.game.huashan_train_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.huashan_train_win,
                                            options=["Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.huashan_train_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.huashan_train_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.huashan_train_win.winfo_width(), self.game.huashan_train_win.winfo_height()
        self.game.huashan_train_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.huashan_train_win.focus_force()
        self.game.huashan_train_win.mainloop()###


    def clickedOnGuoJunda(self, training):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            if self.game.days_spent_in_training >= 58:
                self.game.generate_dialogue_sequence(
                    self.game.huashan_train_win,
                    "Huashan.png",
                    [_("I haven't seen Zhiqiang or Huilin today..."),
                     _("Can you find them for me please?")],
                    ["Guo Junda", "Guo Junda"]
                )
                return

            for widget in self.game.huashan_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.huashan_train_win_F1, text=_("Spar (Guo Junda)"),
                   command=partial(self.huashan_train_spar, targ=_("Guo Junda"))).grid(row=1, column=0)
            Button(self.game.huashan_train_win_F1, text=_("Learn"),
                   command=self.huashan_train_learn).grid(row=2, column=0)

        else:
            for widget in self.game.huashan_win_F1.winfo_children():
                widget.destroy()
            Button(self.game.huashan_win_F1, text=_("Spar (Guo Junda)"),
                   command=partial(self.huashan_train_spar, targ=_("Guo Junda"), training=False)).grid(row=1, column=0)


    def clickedOnGuoZhiqiang(self, training):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.huashan_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.huashan_train_win_F1, text=_("Spar (Guo Zhiqiang)"),
                   command=partial(self.huashan_train_spar, targ=_("Guo Zhiqiang"))).grid(row=0, column=0)
            Button(self.game.huashan_train_win_F1, text=_("What do you think about Zhao Huilin?"),
                   command=partial(self.talkToGuoZhiqiang, 1)).grid(row=1, column=0)
            Button(self.game.huashan_train_win_F1, text=_("How does it feel to be the son of a Sect leader?"),
                   command=partial(self.talkToGuoZhiqiang, 2)).grid(row=2, column=0)

        else:
            for widget in self.game.huashan_win_F1.winfo_children():
                widget.destroy()
            Button(self.game.huashan_win_F1, text=_("Spar (Guo Zhiqiang)"),
                   command=partial(self.huashan_train_spar, targ=_("Guo Zhiqiang"), training=False)).grid(row=1, column=0)


    def talkToGuoZhiqiang(self, choice):
        if choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.huashan_train_win,
                "Huashan.png",
                [_("Huilin is very smart. She is a quick learner and has a lot of potential."),
                 _("She's also very kind and ... and..."),
                 _("Wait, why do you ask? Do.. do you like her?"),
                 _("I was just curious is all...")],
                ["Guo Zhiqiang", "Guo Zhiqiang", "Guo Zhiqiang", "you"]
            )

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.huashan_train_win,
                "Huashan.png",
                [_("Well, it can be overwhelming sometimes..."),
                 _("Everything I do or say can impact Huashan Sect's reputation."),
                 _("So I have to be very careful not to disappoint my father.")],
                ["Guo Zhiqiang", "Guo Zhiqiang", "Guo Zhiqiang"]
            )


    def clickedOnZhaoHuilin(self, training):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.huashan_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.huashan_train_win_F1, text=_("Spar (Zhao Huilin)"),
                   command=partial(self.huashan_train_spar, targ=_("Zhao Huilin"))).grid(row=0, column=0)
            Button(self.game.huashan_train_win_F1, text=_("Why did you join Huashan Sect?"),
                   command=partial(self.talkToZhaoHuilin, 1)).grid(row=1, column=0)
            Button(self.game.huashan_train_win_F1, text=_("Anything fun to do around here?"),
                   command=partial(self.talkToZhaoHuilin, 2)).grid(row=2, column=0)

        else:
            if self.game.taskProgressDic["guo_zhiqiang"] == 16:
                self.game.generate_dialogue_sequence(
                    self.game.huashan_win,
                    "Huashan.png",
                    [_("Can you find Zhiqiang and bring him back here please?")],
                    ["Zhao Huilin"]
                )
                return
            for widget in self.game.huashan_win_F1.winfo_children():
                widget.destroy()
            Button(self.game.huashan_win_F1, text=_("Spar (Zhao Huilin)"),
                   command=partial(self.huashan_train_spar, targ=_("Zhao Huilin"), training=False)).grid(row=1, column=0)


    def talkToZhaoHuilin(self, choice):
        if choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.huashan_train_win,
                "Huashan.png",
                [_("Master adopted me when I was 12, after both my parents were killed by bandits..."),
                 _("And how old are you, may I ask?"),
                 _("I'm 18, 2 years younger than Zhiqiang.")],
                ["Zhao Huilin", "you", "Zhao Huilin"]
            )

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.huashan_train_win,
                "Huashan.png",
                [_("Sometimes I like to train near the cliff."),
                 _("The view there is great and makes it really enjoyable.")],
                ["Zhao Huilin", "Zhao Huilin"]
            )


    def huashan_train_spar(self, targ, training=True):

        if training:
            if self.game.taskProgressDic["join_sect"] < 100 and self.game.taskProgressDic["join_sect"] - 6 < .59 and random() <= .5:
                self.huashan_training_update(.01)

            if targ == _("Guo Zhiqiang"):
                opp = character(_("Guo Zhiqiang"), min([10, self.game.level]),
                                sml=[special_move(_("Basic Sword Technique"), level=min([10, self.game.level])),
                                     special_move(_("Huashan Sword Technique"), level=min([8, self.game.level])),
                                     special_move(_("Thousand Swords Technique"), level=min([5, self.game.level]))],
                                skills=[skill(_("Huashan Agility Technique"), level=5)])

            elif targ == _("Zhao Huilin"):
                opp = character(_("Zhao Huilin"), min([8, self.game.level]),
                                sml=[special_move(_("Basic Sword Technique"), level=min([10, self.game.level])),
                                     special_move(_("Huashan Sword Technique"), level=min([6, self.game.level]))],
                                skills=[skill(_("Huashan Agility Technique"), level=4)])

            elif targ == _("Guo Junda"):
                opp = character(_("Guo Junda"), 20,
                                sml=[special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Huashan Sword Technique"), level=10),
                                     special_move(_("Thousand Swords Technique"), level=10)],
                                skills=[skill(_("Huashan Agility Technique"), level=10)])

            self.game.battleMenu(self.game.you, opp, "battleground_huashan.png",
                            "jy_shendiaozhuti.mp3", fromWin=self.game.huashan_train_win,
                            battleType="training", destinationWinList=[self.game.huashan_train_win] * 3)


        else:
            if targ == _("Guo Zhiqiang"):
                opp = character(_("Guo Zhiqiang"), 12,
                                sml=[special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Huashan Sword Technique"), level=10),
                                     special_move(_("Thousand Swords Technique"), level=7)],
                                skills=[skill(_("Huashan Agility Technique"), level=7)])

            elif targ == _("Zhao Huilin"):
                if self.game.taskProgressDic["guo_zhiqiang"] in [16, 17]:
                    opp = character(_("Zhao Huilin"), 18,
                                    attributeList=[2200, 2200, 3100, 3100, 115, 95, 120, 85, 80],
                                    sml=[special_move(_("Basic Sword Technique"), level=10),
                                         special_move(_("Huashan Sword Technique"), level=10),
                                         special_move(_("Wild Wind Blade"), level=10)],
                                    skills=[skill(_("Huashan Agility Technique"), level=8)]
                                    )
                else:
                    opp = character(_("Zhao Huilin"), min([8, self.game.level]),
                                    sml=[special_move(_("Basic Sword Technique"), level=min([10, self.game.level])),
                                         special_move(_("Huashan Sword Technique"), level=min([6, self.game.level]))],
                                    skills=[skill(_("Huashan Agility Technique"), level=4)])

            elif targ == _("Guo Junda"):
                opp = character(_("Guo Junda"), 22,
                                sml=[special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Huashan Sword Technique"), level=10),
                                     special_move(_("Thousand Swords Technique"), level=10)],
                                skills=[skill(_("Huashan Agility Technique"), level=10)],
                                preferences=[1,3,3,1,1])

            self.game.battleMenu(self.game.you, opp, "battleground_huashan.png",
                                 "jy_shendiaozhuti.mp3", fromWin=self.game.huashan_win,
                                 battleType="training", destinationWinList=[self.game.huashan_win] * 3)


    def huashan_train_learn(self):

        move_names = [m.name for m in self.game.sml]
        if _("Basic Sword Technique") not in move_names:
            self.game.generate_dialogue_sequence(
                self.game.huashan_train_win,
                "Huashan.png",
                [_("In order to learn more advanced techniques, you need to first master the fundamentals."),
                 _("I will teach you the Basic Sword Technique."),
                 _("Practice diligently, and once you make sufficient progress, I will teach you something else."),],
                ["Guo Junda", "Guo Junda", "Guo Junda"]
            )

            new_move = special_move(name=_("Basic Sword Technique"))
            self.game.learn_move(new_move)
            messagebox.showinfo("", _("Guo Junda taught you a new move: Basic Sword Technique!"))


        elif average([move.level for move in self.game.sml if move.name in [_("Basic Sword Technique"), _("Huashan Sword Technique")]]) < 5:
            self.game.generate_dialogue_sequence(
                self.game.huashan_train_win,
                "Huashan.png",
                [_("Keep practicing. You are not yet ready to learn a new technique.")],
                ["Guo Junda"]
            )

        else:
            if _("Huashan Sword Technique") not in move_names:
                self.game.generate_dialogue_sequence(
                    self.game.huashan_train_win,
                    "Huashan.png",
                    [_("Very good; you are ready to learn the Huashan Sword Technique."),
                     _("Watch carefully...")],
                    ["Guo Junda", "Guo Junda"]
                )

                new_move = special_move(name=_("Huashan Sword Technique"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("Guo Junda taught you a new move: Huashan Sword Technique!"))


            elif _("Huashan Agility Technique") not in [s.name for s in self.game.skills]:
                self.game.generate_dialogue_sequence(
                    self.game.huashan_train_win,
                    "Huashan.png",
                    [_("Very good; you are ready to learn the Huashan Agility Technique."),
                     _("Watch carefully...")],
                    ["Guo Junda", "Guo Junda"]
                )

                new_skill = skill(name=_("Huashan Agility Technique"))
                self.game.learn_skill(new_skill)
                messagebox.showinfo("", _("Guo Junda taught you a new skill: Huashan Agility Technique!"))


            else:
                self.game.generate_dialogue_sequence(
                    self.game.huashan_train_win,
                    "Huashan.png",
                    [_("I have nothing else to teach you at the moment."),
                     _("Keep practicing and honing your skills.")],
                    ["Guo Junda", "Guo Junda"]
                )


    def huashan_practice(self):

        if self.game.days_spent_in_training >= 58:
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_ambient2.mp3"
            self.game.startSoundtrackThread()
            self.game.huashan_train_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Finally, we can spend some alone time together..."),
                 _("(Interesting... Look what I found hehehe...)"),
                 _("Yeah, your dad's probably busy teaching {} right now.").format(self.game.character),
                 _("Huilin, you don't know how difficult it is for me to see you every day and to hold back my feelings."),
                 _("I wish we could just be like this forever..."),
                 _("Zhiqiang, you promise to say some good things about me to your dad, right?"),
                 _("Yes, of course, don't worry, I'll make sure he teaches you the Thousand Swords Technique."),
                 _("I love you, Zhiqiang. You always want the best for me."),
                 _("I love you too, Huilin. You are the most beautiful woman I've ever seen."),
                 _("(... Awww, look at these two love birds...)"),
                 _("I don't think we have much time. Let's hurry and do it."),
                 _("Ah yes, I've been thinking about you day and night, even while training."),
                 _("(Hmmm, surely this type of relationship is forbidden... should I say something?)")
                 ],
                ["Guo Zhiqiang", "you", "Zhao Huilin", "Guo Zhiqiang", "Guo Zhiqiang", "Zhao Huilin", "Guo Zhiqiang",
                 "Zhao Huilin", "Guo Zhiqiang", "you", "Zhao Huilin", "Guo Zhiqiang", "you"],
                [[_("Warn them before they commit a grave mistake!"), partial(self.huashan_ending,1)],
                 [_("Wait a bit and catch them in the act, then blackmail them!"), partial(self.huashan_ending,2)]]
            )

        else:
            self.game.generate_dialogue_sequence(
                self.game.huashan_train_win,
                "Huashan Cliff.png",
                [_("What a majestic view! This place is perfect for training!")],
                ["you"]
            )
            nonmax_moves = [m for m in self.game.sml if m.stamina > 0 and m.level < 10]
            if nonmax_moves:
                random_move = pickOne(nonmax_moves)
                random_move.gain_exp(300)
                messagebox.showinfo("", _("You practice for 2 days and gain 300 exp in {}.").format(random_move.name))
            else:
                self.game.gainExp(500)
                messagebox.showinfo("", _("You practice for 2 days and gain 500 exp."))

        self.huashan_training_update(.02)


    def huashan_ending(self, choice):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        if choice == 1:
            self.game.chivalry += 10
            messagebox.showinfo("", "Your chivalry +10!")
            self.game.generate_dialogue_sequence(
                self.game.huashan_train_win,
                "Huashan Cliff.png",
                [_("Wait! What are you guys doing?"),
                 _("If Master found out about this, he would be furious! Turn back before it's too late."),
                 _("{}.... you... how...").format(self.game.character),
                 _("Please don't tell Master about this... It would be too shameful..."),
                 _("Please, {}, if you promise not to say a word about what you saw, I'll give you the Thousand Swords Manual in return.").format(self.game.character),
                 _("Fine then, since I'm leaving soon anyway, I'll keep my mouth shut.")],
                ["you", "you", "Guo Zhiqiang", "Zhao Huilin", "Guo Zhiqiang", "you"]
            )
            self.game.inv[_("Thousand Swords Manual")] = 1
            messagebox.showinfo("", "Received Thousand Swords Manual x 1!")
        elif choice ==2:
            self.game.chivalry -= 30
            messagebox.showinfo("", "Your chivalry -30!")
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Huilin..."),
                 _("Zhiqiang..."),
                 _("[Content not suitable for minors]"),
                 _("Huilin, I love you so much..."),
                 _("Zhiqiang, I ---"),
                 _("Oh look what we've got here~"),
                 _("{}?! Wait, wh... How...").format(self.game.character),
                 _("You... you saw everything?"),
                 _("You bet I did! That was a nice show..."),
                 _("You pervert! Don't you feel any shame?!"),
                 _("Shame? You guys are the ones that should be ashamed!"),
                 _("If this news gets out to the martial arts community -- no, not even that -- if it even reaches Master's ears..."),
                 _("You guys can imagine what's going to happen to you, right?"),
                 _("Y-you... how despicable..."),
                 _("Call me whatever you want, but you'd better listen to my every command from now on..."),
                 _("The first thing I want is all the sword manuals that you guys have..."),
                 _("I can't do that... it's against the rules of Huashan Sect..."),
                 _("And this little romantic relationship between you guys, is that not against the rules of Huashan Sect?"),
                 _("..........."),
                 _("Hurry up; I don't have time to waste... or do you want me to report everything I saw to Master?"),
                 ],

                ["Guo Zhiqiang", "Zhao Huilin", "Blank", "Guo Zhiqiang", "Zhao Huilin", "you", "Guo Zhiqiang",
                 "Zhao Huilin", "you", "Zhao Huilin", "you", "you", "you", "Guo Zhiqiang", "you", "you",
                 "Guo Zhiqiang", "you", "Guo Zhiqiang", "you"]
            )

            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("There's no need for that."),
                 _("M-master..."),
                 _("D-dad... please, I ... we can explain..."),
                 _("Perfect timing, Master... seems like this son of yours and one of your disciples committed an atrocious act together..."),
                 _("If word got out to the martial arts community, this would bring shame to Huashan Sect and destroy your reputation."),
                 _("Indeed it would... so we need to prevent that from happening..."),
                 _("Exactly... er... wait... what do you mean?"),
                 _("The only people who know about this are the 4 of us. Zhiqiang and Huilin are certainly going to keep quiet, as will I..."),
                 _("But what about you, {}?").format(self.game.character),
                 _("Me? Yeah, of course, I mean... What good will it do for me to spread this?"),
                 _("Hmmm, good, good... but I think just to be safe, we had better make sure you remain... silent..."),
                 _("Wait, what do you mean by that? Hold on! You are quite reputable in the martial arts community. You can't just..."),
                 _("It's quite dishonorable!"),
                 _("Hah... honor? All I care about is my strength and reputation."),
                 _("Let it be known that on this day, while training near the cliffs, {} slipped and fell thousands of feet to his death.").format(self.game.character),
                 _("Quite lamentable, but also a reasonable explanation..."),
                 _("Wait, you... you can't..."),
                 _("Good bye, {}").format(self.game.character),
                 _("AHHHHHHHHHHHHHH!!!")],
                ["Guo Junda", "Zhao Huilin", "Guo Zhiqiang",
                 "you", "you", "Guo Junda", "you", "Guo Junda", "Guo Junda", "you", "Guo Junda", "you", "you",
                 "Guo Junda", "Guo Junda", "Guo Junda", "you", "Guo Junda", "you"]
            )

            if self.game.luck < 80:
                self.game.display_lose_screen()
            else:
                self.game.stopSoundtrack()
                self.game.currentBGM = "jy_mountain_scenery.mp3"
                self.game.startSoundtrackThread()


                self.game.generate_dialogue_sequence(
                    None,
                    "Huashan Cliff Bottom.png",
                    [_("......................"),
                     _("Where... where am I?"),
                     _("Oh, right, I was pushed off of the cliff by Guo Junda and then..."),
                     _("Could it be... that the trees and the water saved me?"),
                     _("Ahahahaha! I am destined to live!"),
                     _("Young man..."),
                     _("WHOA!!! Who- who are you???"),
                     _("Young man, how did you end up here?"),
                     _("I... well, I was pushed off of the cliff by Huashan Sect leader Guo Junda..."),
                     _("HAHAHAHAHAHAHAHA.... AHAHAHAHAHAHAHAHA..."),
                     _("......................"),
                     _("Who would've thought that after 15 years, someone would suffer the same fate as me?"),
                     _("Same fate? So he pushed you off the cliff as well?"),
                     _("What did you do? Discover some secret that would've ruined his reputation?"),
                     _("Ruined his reputation? Possibly... Secret? Not at all..."),
                     _("You see, I'm Gao Yaluo, his wife."),
                     _("Wife?!"),
                     _("Yes, Guo Junda and I were martial arts fanatics from a young age. We trained together and fell in love."),
                     _("We got married, and 3 years later, he earned his position as the Huashan Sect leader."),
                     _("We still studied martial arts and trained together."),
                     _("We even invented our own techniques: Thousand Swords Technique and Wild Wind Blade."),
                     _("I've heard of the former but not the latter..."),
                     _("That's because the Wild Wind Blade was invented by me..."),
                     _("One day, while sparring, we decided to use our respective techniques."),
                     _("I won, and not by a close margin..."),
                     _("I should've known then that Guo Junda, who values his reputation far too much, would do me harm..."),
                     _("Fearing that he would lose face if his wife was stronger than him, he decided to get rid of me..."),
                     _("On the day of our anniversary, he took me to the cliff."),
                     _("Being a young, naive woman, I thought he was going to give me a surprise."),
                     _("Well, I suppose he did... by pushing me off the cliff."),
                     _("By some miracle, I lived, though I became paralyzed in both feet. Luckily, there are enough fruits and water here to keep me alive."),
                     _("For the past 15 years, there's only been one thing on my mind: revenge."),
                     _("Young man, can you promise me that you will kill that bastard for me when you see him again?"),
                     _("Of course, even if not for you, for the sake of the martial arts community..."),
                     _("Good... good..."),
                     _("AHAHAHAHAHAHAHA!!! COME HERE!"),
                     _("Whoaaa, wait!!! What are you doing?!"),
                     _("I'm going to pass all of my internal energy to you, and I will also teach you the Wild Wind Blade."),
                     _("Use this to avenge me!"),
                     _("WAIT... AHHHHH!!!"),
                     _("(My body, it's growing hot! This energy is... too... strong...)"),
                     _("*You feel a surge of energy rush into your abdomen. You find it difficult to breathe. Everything starts to turn blurry.*"),
                     _("Oh man... Ah, my head hurts... I must've passed out..."),
                     _("Madam, thanks for... ......Madam?"),
                     _(".."),
                     _("(Looks like she passed away after giving all her internal energy to me...)"),
                     _("Don't worry, Madam. I will surely avenge you. Rest in peace.")],
                    ["you", "you", "you", "you", "you", "Gao Yaluo", "you", "Gao Yaluo", "you", "Gao Yaluo",
                     "you", "Gao Yaluo", "you", "you", "Gao Yaluo", "Gao Yaluo", "you", "Gao Yaluo", "Gao Yaluo",
                     "Gao Yaluo", "Gao Yaluo", "you", "Gao Yaluo", "Gao Yaluo", "Gao Yaluo", "Gao Yaluo", "Gao Yaluo",
                     "Gao Yaluo", "Gao Yaluo", "Gao Yaluo", "Gao Yaluo", "Gao Yaluo", "Gao Yaluo", "you", "Gao Yaluo",
                     "Gao Yaluo", "you", "Gao Yaluo", "Gao Yaluo", "you", "you", "Blank", "you", "you", "Gao Yaluo",
                     "you", "you"
                     ]
                )

                self.game.staminaMax += 1000
                new_move = special_move(name=_("Wild Wind Blade"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("Your stamina upper limit increased by 1000!\nYou learned a new move: Wild Wind Blade!"))

                self.game.taskProgressDic["join_sect"] = 701
                self.game.generateGameWin()

            return

        self.huashan_training_update(.02)



    def huashan_training_update(self,increment):
        self.game.taskProgressDic["join_sect"] += increment
        self.game.taskProgressDic["join_sect"] = round(self.game.taskProgressDic["join_sect"],2)
        self.game.days_spent_in_training = int(self.game.taskProgressDic["join_sect"]*100-600)
        self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
        if self.game.days_spent_in_training >= 60:
            self.game.days_spent_in_training = 60
            self.game.taskProgressDic["join_sect"] = 6.6
            self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
            self.game.huashan_train_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Well, {}, time has come for us to part ways. I hope you've enjoyed your stay here.").format(self.game.character),
                 _("Remember to always commit acts of chivalry and promote the reputation of the Huashan Sect."),
                 _("Yes, Master, I will! Thank you!"),
                 _("{}, it was nice training with you... By the way, thanks for... you know what...").format(self.game.character),
                 _("Ah, haha, no problem..."),
                 _("Take care, {}!").format(self.game.character),
                 _("Farewell, everyone!")],
                ["Guo Junda", "Guo Junda", "you", "Guo Zhiqiang", "you", "Zhao Huilin", "you"]
            )
            self.game.taskProgressDic["join_sect"] = 700
            self.game.generateGameWin()

        elif self.game.days_spent_in_training >= 58:
            self.game.huashan_F2.place(x=-3000, y=-3000)
            self.game.huashan_F3.place(x=-3000, y=-3000)
            for widget in self.game.huashan_train_win_F1.winfo_children():
                widget.destroy()




class Scene_huashan(Scene_huashan_train):
    def __init__(self, game):
        self.game = game
        if self.game.taskProgressDic["guo_zhiqiang"] in list(range(12, 15)):
            self.game.taskProgressDic["guo_zhiqiang"] += 1

        year = calculate_month_day_year(self.game.gameDate)["Year"]
        if self.game.taskProgressDic["join_sect"] == 701:
            self.game.taskProgressDic["join_sect"] = 702
            time.sleep(.5)

            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Guo Junda! You two-faced villain! Today, you shall pay for what you've done!"),
                 _("{}??? You... ").format(self.game.character),
                 _("You must have incredible luck to have survived that fall..."),
                 _("Guess what... I'm not the only one! Your wife, whom you tried to murder 15 years ago, also survived!"),
                 _("Wh-what are you talking about?! My wife, she... she went missing 15 years ago."),
                 _("Did she fall down the cliff by accident? What does that have to do with me???"),
                 _("Yeah yeah yeah, keep acting..."), #Row 2
                 _("So... you say my wife is still alive? Then where is she?"),
                 _("She is no longer with us, but before she passed away, she taught me her technique... Wild Wind Blade..."),
                 _("And I vowed to use this technique to avenge her!"),
                 _("So that hag is dead, huh? Good good..."),
                 _("D-dad?! How can you say that??? Does that mean... it's true then? You tried to kill mom???"),
                 _("I had no choice, Zhiqiang... One day you will understand..."), #Row 3
                 _("For now, let me take care of this brat who knows way too much to still be alive...")
                 ],
                ["you", "Guo Junda", "Guo Junda", "you", "Guo Junda", "Guo Junda",
                 "you", "Guo Junda", "you", "you", "Guo Junda", "Guo Zhiqiang",
                 "Guo Junda", "Guo Junda"]
            )
            opp = character(_("Guo Junda"), 20,
                            sml=[special_move(_("Basic Sword Technique"), level=10),
                                 special_move(_("Huashan Sword Technique"), level=10),
                                 special_move(_("Thousand Swords Technique"), level=10)],
                            skills=[skill(_("Huashan Agility Technique"), level=10),
                                    skill(_("Huashan Sword Qi Gong"), level=10)],
                            )

            self.game.battleMenu(self.game.you, opp, "battleground_huashan.png",
                                 "jy_heroic.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[], postBattleCmd=self.post_return_to_huashan_battle)


        elif self.game.taskProgressDic["liu_village"] == 19 and self.game.taskProgressDic["join_sect"] == 702:
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Zhiqiang, Huilin, I need your help!"),
                 _("Oh hey, {}. What can we do for you?").format(self.game.character),
                 _("There's a group of masked assassins who were responsible for the Liu Village massacre hiding in the dark forest near Huashan."),
                 _("I need your help to find out where they are hiding..."),
                 _("No problem; we can send some Huashan disciples to search the area."),
                 _("I'm afraid it won't be so simple. These masked assassins are fairly skilled."),
                 _("You mean we should go look for them ourselves?"),  # Row 2
                 _("I think that's the only option we have..."),
                 _("Zhiqiang, what do you think?"),
                 _("We know the area quite well; with {} by our side, we should be able to take on any enemy!").format(self.game.character)
                 ],
                ["you", "Guo Zhiqiang", "you", "you", "Guo Zhiqiang", "you",
                 "Zhao Huilin", "you", "Zhao Huilin", "Guo Zhiqiang"]
            )

            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Anyone? HAHAHAHAHA! What about me?"),
                 _("F-father...?! Why have you returned?"),
                 # Row break
                 _("Hah... If you still wish to call me 'Father', then kill this brat with your hands right now!"),
                 _("Guo Junda, you are not fit to be Zhiqiang's father. You tried to kill his mother!"),
                 _("Once I get rid of you, he'll be begging to call me 'Father'!"),
                 _("I spared your previously... Why have you returned to look for more trouble?"),
                 _("HAHAHAHAHA! You might've won before, but there's no way you can defeat me now!"),
                 _("Not even my wife's Wild Wind Blade is a match for the Devil Crescent Moon Blade! AHAHAHAHAHA!!!!"), #Row 3
                 _("Devil Crescent Moon Blade... Why... How did you get your hands on the manual?!"),
                 _("I've spent countless sleepless nights to come up with a scheme to get the manual."),
                 _("No one besides me deserves it... NO ONE!!!"),  # Row 4
                 _("Then those masked assassins... You were behind it all??"),
                 _("I hired them... They were men from Eagle Sect, including the leader, Jiang Yuanqing..."),
                 _("If you've been paying attention to what's happening in Wulin, you would know that Eagle Sect has been empty for a couple of months."),
                 _("That's because they've been busy following my orders... HAHAHAHAHAHA!"),
                 _("Then where are they now?"),
                 _("Dead... I can't have people knowing about my true ambitions... The only kind of people who can keep secrets are dead people..."), # Row 5
                 _("It was also my way of testing how powerful the Devil Crescent Moon Blade really is."),
                 _("Even Jiang Yuanqing could not last 20 bouts against me."),
                 _("Before I killed him, he begged me to spare him and offered to give me all the Shaolin Kung-Fu manuals that he's been stealing over the years."),
                 _("I happily took the manuals before chopping his head off... And now, I've learned the top Shaolin techniques as well..."),
                 _("I AM INVINCIBLE! HAHAHAHAHAHAHAHAHA!!!!!!"),
                 _("All of Wulin will bow to me!!! AHAHAHAHAHAHAHAHAHA!!!"),  # Row 6
                 _("As for you all... I will not grant you the privilege of bowing to me due to all the trouble you've caused me."),
                 _("There exists only one option... die...")
                 ],
                ["Guo Junda", "Guo Zhiqiang",
                 "Guo Junda", "you", "Guo Junda", "you", "Guo Junda",
                 "Guo Junda", "you", "Guo Junda",
                 "Guo Junda", "you", "Guo Junda", "Guo Junda", "Guo Junda", "you",
                 "Guo Junda", "Guo Junda", "Guo Junda", "Guo Junda", "Guo Junda", "Guo Junda",
                 "Guo Junda", "Guo Junda", "Guo Junda"
                 ]
            )
            opp = character(_("Guo Junda"), 25,
                            sml=[special_move(_("Devil Crescent Moon Blade"), level=10), ],
                            skills=[skill(_("Tendon Changing Technique"), level=10),
                                    skill(_("Basic Agility Technique"), level=10),
                                    skill(_("Shaolin Inner Energy Technique"), level=10),
                                    skill(_("Huashan Agility Technique"), level=10),
                                    skill(_("Huashan Sword Qi Gong"), level=10)],
                            )
            self.game.battleMenu(self.game.you, opp, "battleground_huashan.png", "jy_suspenseful2.mp3",
                                 fromWin=None,
                                 battleType="task", postBattleCmd=self.defeated_evil_guo_junda)


        elif (year > 1 or self.game.taskProgressDic["liu_village"] >= 12) and self.game.taskProgressDic["join_sect"] not in [701,702] and self.game.taskProgressDic["guo_zhiqiang"] == -1:
                self.guo_zhiqiang_conflict()

        elif self.game.taskProgressDic["guo_zhiqiang"] == 15 and (random() <= .75 or self.game.taskProgressDic["liu_village"] == 19):
            self.zhao_huilin_returns()

        elif self.game.taskProgressDic["guo_zhiqiang"] == 17 and self.game.taskProgressDic["liu_village"] == 19:
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Zhiqiang, Huilin, I need your help!"),
                 _("Oh hey, {}. What can we do for you?").format(self.game.character),
                 _("There's a group of masked assassins who were responsible for the Liu Village massacre hiding in the dark forest near Huashan."),
                 _("I need your help to find out where they are hiding..."),
                 _("No problem; we can send some Huashan disciples to search the area."),
                 _("I'm afraid it won't be so simple. These masked assassins are fairly skilled."),
                 _("You mean we should go look for them ourselves?"), #Row 2
                 _("I think that's the only option we have..."),
                 _("Zhiqiang, what do you think?"),
                 _("We owe our lives to {}. I think we --").format(self.game.character)
                ],
                ["you", "Guo Zhiqiang", "you", "you", "Guo Zhiqiang", "you",
                 "Zhao Huilin", "you", "Zhao Huilin", "Guo Zhiqiang"]
            )

            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Zhao Huilin! Today is the day when I shall avenge my previous humiliation!"),
                 _("F-father...?! Why have you returned?"),
                 # Row break
                 _("Hah... If you still wish to call me 'Father', then kill this slut with your own hands right now!"),
                 _("Guo Junda, you are not fit to be Zhiqiang's father. You tried to kill his mother!"),
                 _("Once I get rid of you, he'll be begging to call me 'Father'!"),
                 _("I spared your previously... Why have you returned to look for more trouble?"),
                 _("HAHAHAHAHA! I held back last time... You really think you could defeat me?"),
                 _("Show me what you've got then...")
                 ],
                ["Guo Junda", "Guo Zhiqiang",
                 "Guo Junda", "Zhao Huilin", "Guo Junda", "Zhao Huilin", "Guo Junda", "Zhao Huilin",
                 ]
            )

            you = character(_("Zhao Huilin"), 18,
                            attributeList=[2400, 2400, 3700, 3700, 130, 95, 130, 90, 90],
                            sml=[special_move(_("Basic Sword Technique"), level=10),
                                 special_move(_("Huashan Sword Technique"), level=10),
                                 special_move(_("Wild Wind Blade"), level=10),
                                 special_move(_("Rest"))],
                            skills=[skill(_("Huashan Agility Technique"), level=8)],
                            equipmentList=[Equipment(_("Iron Sword")), Equipment(_("Leather Boots")),
                                           Equipment(_("Leather Body"))]
                            )

            opp = character(_("Guo Junda"), 25,
                            sml=[special_move(_("Devil Crescent Moon Blade"), level=10), ],
                            skills=[skill(_("Tendon Changing Technique"), level=10),
                                    skill(_("Basic Agility Technique"), level=10),
                                    skill(_("Shaolin Inner Energy Technique"), level=10),
                                    skill(_("Huashan Agility Technique"), level=10),
                                    skill(_("Huashan Sword Qi Gong"), level=10)],
                            )

            self.game.battleID = "zhao_huilin_guo_junda_battle"
            self.game.battleMenu(you, opp, "battleground_huashan.png",
                                 postBattleSoundtrack="jy_suspenseful2.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[], postBattleCmd=self.post_zhao_huilin_guo_junda_battle)
            

            
            
        elif self.game.taskProgressDic["guo_zhiqiang"] in [11,21] and self.game.taskProgressDic["liu_village"] == 19:
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Master Guo!"),
                 _("Ah, young man, good to see you again."),
                 _("I need your help with something."),
                 _("Go ahead, I'm listening. If I can be of any assistance, even if it means putting my life on the line, I will not blink an eye!"),
                 _("Perfect! I knew you'd be willing to help! Say, have you heard about the massacre at Liu Village?"),
                 _("Of course, every single person in Wulin must've heard about it by now."),
                 _("It's quite a tragedy... a whole village of innocent people killed... and for what? A martial arts manual?"), #Row 2
                 _("Unbelievable what people will do nowadays..."),
                 _("I'm glad you think that! For the past couple of weeks, I've been investigating who's behind it all."),
                 _("Oh? And what have you found? Do you know the culprit?"),
                 _("Well, there are many masked men who are responsible, but there's a big boss giving them all the orders."),
                 _("I haven't been able to determine who this boss is yet. I thought it was Lu Xifeng at first."),
                 _("But when I confronted him about it, he seemed to not have been involved at all."), #Row 3
                 _("Surprising..."),
                 _("Yeah, I know, right? Anyway, I did discover that these masked assassins like to meet in the dark forest near Huashan."),
                 _("Since I'm not familiar with the area, I could use your help to track them down."),
                 _("I've already killed several of their men; if we team up, we should have no problem taking them down."),
                 _("Say no more; I am with you! Let us put an end to their malicious deeds!")],
                ["you", "Guo Junda", "you", "Guo Junda", "you", "Guo Junda",
                 "Guo Junda", "Guo Junda", "you", "Guo Junda", "you", "you",
                 "you", "Guo Junda", "you", "you", "you", "Guo Junda",
                 ]
            )

            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("This is roughly where I last encountered them..."),
                 _("Ah... I know this forest quite well. There are 2 superb hiding spots in this forest."),
                 _("I bet they are hiding in one of them... Come with me."),
                 _("*You follow Guo Junda through the forest. After many twists and turns, you come to a very dim area with thick leaves and branches.*"),
                 ],
                ["you", "Guo Junda", "Guo Junda", "Blank"],
            )

            #insert tense music here
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("So... You found us at last..."),
                 _("Oh look, we meet again..."),
                 _("Hahahahahaha! Finally, we can get rid of you for good."),
                 _("You should've learned to mind your own business... Now you leave us with no choice."),
                 _("You seem a bit over-confident..."),
                 _("Hah... say what you will... These will be your last words."),
                 ],
                ["Masked Thief", "you", "Masked Thief", "Masked Thief", "you", "Masked Thief"],
            )

            #battle vs masked thief
            opp = character(_("Masked Thief"), 25,
                            sml=[special_move(_("Shaolin Diamond Finger"), level=10),
                                 special_move(_("Guan Yin Palm"), level=10),
                                 special_move(_("Smoke Bomb"), level=10),
                                 special_move(_("Eagle Claws"), level=10),],
                            skills=[skill(_("Tendon Changing Technique"), level=7),
                                    skill(_("Basic Agility Technique"), level=10),
                                    skill(_("Spreading Wings"), level=10)])
            self.game.battleMenu(self.game.you, opp, "battleground_dark_forest.png", "jy_suspenseful2.mp3", fromWin=None,
                            battleType="task", postBattleCmd=self.post_masked_thief_battle)


        else:
            self.game.huashan_win = Toplevel(self.game.mapWin)
            self.game.huashan_win.title(_("Huashan"))
            bg = PhotoImage(file="Huashan.png")
            w = bg.width()
            h = bg.height()

            canvas = Canvas(self.game.huashan_win, width=w, height=h)
            canvas.pack()
            canvas.create_image(0, 0, anchor=NW, image=bg)

            if self.game.taskProgressDic["guo_zhiqiang"] not in list(range(16,20)) and self.game.taskProgressDic["liu_village"] != 1000 and self.game.taskProgressDic["join_sect"] != 702:
                self.game.huashan_F1 = Frame(canvas)
                pic1 = PhotoImage(file="Guo Junda_icon_large.ppm")
                imageButton1 = Button(self.game.huashan_F1, command=partial(self.clickedOnGuoJunda, False))
                imageButton1.grid(row=0, column=0)
                imageButton1.config(image=pic1)
                label = Label(self.game.huashan_F1, text=_("Guo Junda"))
                label.grid(row=1, column=0)
                self.game.huashan_F1.place(x=w // 4 - pic1.width(), y=h // 3 - pic1.height())

            if self.game.taskProgressDic["guo_zhiqiang"] in [-1, 17]:
                self.game.huashan_F2 = Frame(canvas)
                pic2 = PhotoImage(file="Guo Zhiqiang_icon_large.ppm")
                imageButton2 = Button(self.game.huashan_F2, command=partial(self.clickedOnGuoZhiqiang, False))
                imageButton2.grid(row=0, column=0)
                imageButton2.config(image=pic2)
                label = Label(self.game.huashan_F2, text=_("Guo Zhiqiang"))
                label.grid(row=1, column=0)
                self.game.huashan_F2.place(x=w // 4 - pic2.width(), y=h // 3 + pic2.height())

            if self.game.taskProgressDic["guo_zhiqiang"] in [-1, 16, 17]:
                self.game.huashan_F3 = Frame(canvas)
                pic3 = PhotoImage(file="Zhao Huilin_icon_large.ppm")
                imageButton3 = Button(self.game.huashan_F3, command=partial(self.clickedOnZhaoHuilin, False))
                imageButton3.grid(row=0, column=0)
                imageButton3.config(image=pic3)
                label = Label(self.game.huashan_F3, text=_("Zhao Huilin"))
                label.grid(row=1, column=0)
                self.game.huashan_F3.place(x=w * 3 // 4, y=h // 3 - pic3.height())

            self.game.huashan_win_F1 = Frame(self.game.huashan_win)
            self.game.huashan_win_F1.pack()

            menu_frame = self.game.create_menu_frame(master=self.game.huashan_win,
                                                     options=["Map", "Profile", "Inv", "Save", "Settings"])
            menu_frame.pack()

            self.game.huashan_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
            self.game.huashan_win.update_idletasks()
            toplevel_w, toplevel_h = self.game.huashan_win.winfo_width(), self.game.huashan_win.winfo_height()
            self.game.huashan_win.geometry(
                "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, SCREEN_HEIGHT // 2 - toplevel_h * 2 // 4))
            self.game.huashan_win.focus_force()
            self.game.huashan_win.mainloop()  ###


    def zhao_huilin_returns(self, choice=0):
        if choice == 0:
            time.sleep(.5)
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("You sinister villain!"),
                 _("Huilin... you... you're still alive?!"),
                 _("I... I'm so glad you're okay!"),
                 _("Don't even try that with me! I know all the things you've done!"),
                 _("Wh-what do you mean? You are still angry at me for separating you and Zhiqiang, aren't you?"),
                 _("If it was that simple, I would've accepted my fate."),
                 _("Still trying to hide the truth? Does the name 'Gao Yaluo' ring a bell?"),
                 _("Gao... You... how... how did you..."),
                 _("Yep, I'd always thought Zhiqiang's mother died from an illness when he was still young."),
                 _("That is, until I met her at the bottom of the cliff..."),
                 _("Cliff... she... you..."),
                 _("That's right; I didn't die. The vegetation and water broke my fall, and I survived..."),
                 _("Just like Gao Yaluo when you pushed her down from the cliff 15 years ago..."),
                 _("L-lies.... LIES! Huilin... Why... why would you believe such lies?!"),
                 _("I've always thought you were a bit obsessive about protection your reputation."),
                 _("But never did I imagine that you would attempt to kill your wife because she defeated you in a spar."),
                 _("You truly are a narcissistic hypocrite!"),
                 _("ENOUGH!!! ... So... You found out about it, huh? Who else knows? And where is that woman now?"),
                 _("Zhiqiang's mother transferred all her internal energy to me and taught me everything she knew..."),
                 _("Before she died, she made me promise to avenge her, and that's why I'm here today."),
                 _("Hahahaha... is that so? Well, that's awfully convenient."),
                 _("Now that you know so much information, I have to get rid of you."),
                 _("Try me...")
                 ],
                ["Zhao Huilin", "Guo Junda", "Guo Junda", "Zhao Huilin", "Guo Junda", "Zhao Huilin",
                 "Zhao Huilin", "Guo Junda", "Zhao Huilin", "Zhao Huilin", "Guo Junda", "Zhao Huilin",
                 "Zhao Huilin", "Guo Junda", "Zhao Huilin", "Zhao Huilin", "Zhao Huilin", "Guo Junda",
                 "Zhao Huilin", "Zhao Huilin", "Guo Junda", "Guo Junda", "Zhao Huilin"
                 ],
            )

            you = character(_("Zhao Huilin"), 18,
                            attributeList=[2400, 2400, 3700, 3700, 130, 95, 130, 90, 90],
                            sml=[special_move(_("Basic Sword Technique"), level=10),
                                 special_move(_("Huashan Sword Technique"), level=10),
                                 special_move(_("Wild Wind Blade"), level=10),
                                 special_move(_("Rest"))],
                            skills=[skill(_("Huashan Agility Technique"), level=8)],
                            equipmentList=[Equipment(_("Iron Sword")), Equipment(_("Leather Boots")),
                                           Equipment(_("Leather Body"))]
                            )

            opp = character(_("Guo Junda"), 20,
                                sml=[special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Huashan Sword Technique"), level=10),
                                     special_move(_("Thousand Swords Technique"), level=10)],
                                skills=[skill(_("Huashan Agility Technique"), level=10),
                                        skill(_("Huashan Sword Qi Gong"), level=10)],
                                )

            self.game.battleID = "zhao_huilin_revenge"
            cmd = lambda: self.zhao_huilin_returns(1)
            self.game.battleMenu(you, opp, "battleground_huashan.png",
                            "jy_heroic.mp3", fromWin=None,
                            battleType="task", destinationWinList=[], postBattleCmd=cmd)


        elif choice == 1:
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("You... how... impossible..."),
                 _("Your wife's Wild Wind Blade is much better than your Thousand Swords Technique it seems..."),
                 _("This time, I will spare your life to repay all that you've done for me over the years."),
                 _("If you continue in your evil deeds, I will not show mercy again."),
                 _("Is that so... we'll see who gets the last laugh..."),
                 _("*Guo Junda leaves*"),
                 _("Wow, that was impressive! So... what will happen to Huashan Sect now?"),
                 _("I'm not sure... Huashan can't be without a leader..."),
                 _("In that case, I propose you take over Huashan Sect for now, since you are clearly the strongest Huashan disciple at the moment."),
                 _("I... but... I don't think I'm qualified..."),
                 _("I mean, it's either that or the end of Huashan Sect..."),
                 _("Fine; I'll accept the role for now, but can you please look for Zhiqiang and bring him back?"),
                 _("Once he returns, I'll pass the role to him."),
                 _("Sure, I'll go look for him!")
                 ],
                ["Guo Junda", "Zhao Huilin", "Zhao Huilin", "Zhao Huilin", "Guo Junda", "Blank", "you",
                 "Zhao Huilin", "you", "Zhao Huilin", "you", "Zhao Huilin", "Zhao Huilin", "you"]
            )
            self.game.battleID = None
            self.game.taskProgressDic["guo_zhiqiang"] = 16
            self.__init__(self.game)


    def guo_zhiqiang_conflict(self, choice=0):

        if choice == 0:
            time.sleep(.5)
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()
            self.game.taskProgressDic["guo_zhiqiang"] = 0
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("You two... what have you done?!"),
                 _("Do you have any idea how much shame this brings to the Huashan Sect?!"),
                 _("F-father, I... I'm sorry... we..."),
                 _("Master, please, it's all my fault. I ---"),
                 _("No! Father, I accept full responsibility... I'm the one who got Huilin pregnant.. I---"),
                 _("Enough!!! BOTH of you are responsible, and BOTH of you will face the consequences..."),
                 _("But..."),
                 _("No! Do you think this is easy for me? I've raised Huilin since she was 12, as if she's my own daughter."),
                 _("If you two had feelings for each other, I could've arranged for you to get married..."),
                 _("But for you to do this behind my back and commit such a shameful act..."),
                 _("Now you leave me with no choice..."),
                 _("Huilin, from this moment, you are no longer a disciple of Huashan..."),
                 _("Master! No! Please! I'm sorry, I'll do anything to make up for what I've done!"),
                 _("Please don't kick me out!"),
                 _("Father, please! Don't separate us!"),
                 _("She's pregnant with my child... your grandchild! You can't just leave her to live or die on her own!"),
                 _("What choice do I have?! If I don't do this, then how can I uphold Huashan Sect's reputation?"),
                 _("I am being lenient by allowing her to live..."),
                 _("Huilin, I'll give you another 2 hours to pack up your belongings and leave..."),
                 _("As for you, Zhiqiang... from this day forward, you are not allowed to step out of Huashan."),
                 _("If you dare go searching for Huilin, I will ensure you regret your decision...")
                 ],
                ["Guo Junda", "Guo Junda", "Guo Zhiqiang", "Zhao Huilin", "Guo Zhiqiang", "Guo Junda", "Guo Zhiqiang",
                 "Guo Junda", "Guo Junda", "Guo Junda", "Guo Junda", "Guo Junda", "Zhao Huilin", "Zhao Huilin",
                 "Guo Zhiqiang", "Guo Zhiqiang", "Guo Junda", "Guo Junda", "Guo Junda", "Guo Junda", "Guo Junda"
                 ],
                [[_("Help Guo Zhiqiang and Zhao Huilin"), partial(self.guo_zhiqiang_conflict, 1)],
                 [_("Help Guo Junda"), partial(self.guo_zhiqiang_conflict, 2)]]
            )

        elif choice == 1:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.taskProgressDic["guo_zhiqiang"] = 1
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Wait a second... Aren't you a bit too harsh on them?"),
                 _("They're still quite young after all..."),
                 _("Plus, there are many dangerous people in the Martial Arts community."),
                 _("If you force this young lady into the wild on her own while she's pregnant, she won't survive long."),
                 _("Are you insulting Huashan Sect's kung-fu? She's learned enough over the years to protect herself."),
                 _("Thanks for trying to help us, but my father has already made up his mind, I'm afraid..."),
                 _("Not if I can do something about it..."),
                 _("Arrogant youngster...")
                 ],
                ["you", "you", "you", "you", "Guo Junda", "Guo Zhiqiang", "you", "Guo Junda"]
            )

            opp = character(_("Guo Junda"), 20,
                                sml=[special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Huashan Sword Technique"), level=10),
                                     special_move(_("Thousand Swords Technique"), level=10)],
                                skills=[skill(_("Huashan Agility Technique"), level=10),
                                        skill(_("Huashan Sword Qi Gong"), level=10)])

            self.game.battleID = "guo_zhiqiang_conflict_11"
            self.game.battleMenu(self.game.you, opp, "battleground_huashan.png",
                            "jy_shendiaoshuhuan.mp3", fromWin=None,
                            battleType="task", destinationWinList=[], postBattleCmd=self.guo_zhiqiang_conflict)


        elif choice == 2:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.taskProgressDic["guo_zhiqiang"] = 2
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Your father is right."),
                 _("Who... who are you?"),
                 _("What you guys have done is disgraceful, indeed. Huashan Sect's reputation will be tainted."),
                 _("This is none of your business... It's our family matter..."),
                 _("I'm just trying to talk some sense into you..."),
                 _("Stay out of this please..."),
                 _("Huilin, this guy is asking for trouble. Let's teach him a lesson."),
                 ],
                ["you", "Guo Zhiqiang", "you", "Guo Zhiqiang", "you", "Zhao Huilin", "Guo Zhiqiang"]
            )

            opp_list = []
            opp = character(_("Guo Zhiqiang"), 12,
                            sml=[special_move(_("Basic Sword Technique"), level=10),
                                 special_move(_("Huashan Sword Technique"), level=10),
                                 special_move(_("Thousand Swords Technique"), level=7)],
                            skills=[skill(_("Huashan Agility Technique"), level=7)])
            opp_list.append(opp)
            opp = character(_("Zhao Huilin"), 10,
                            sml=[special_move(_("Basic Sword Technique"), level=min([10, self.game.level])),
                                 special_move(_("Huashan Sword Technique"), level=min([8, self.game.level]))],
                            skills=[skill(_("Huashan Agility Technique"), level=6)])
            opp_list.append(opp)

            cmd = lambda: self.guo_zhiqiang_conflict(21)
            self.game.battleMenu(self.game.you, opp_list, "battleground_huashan.png",
                                 "jy_shendiaoshuhuan.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[], postBattleCmd=cmd)


        elif choice == 11:
            self.game.chivalry += 20
            messagebox.showinfo("", _("Your chivalry +20!"))
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("What are you guys doing here? Quick, run while you can!"),
                 _("I can't hold him off forever..."),
                 _("Th-thank you, young hero..."),
                 _("Thank you! I will not forget what you've done for us!"),
                 _("Huilin, let's go!"),
                 _("If you leave, don't ever come back! You are no longer my son!"),
                 _("Sorry Dad... I can't abandon Huilin and... and my child..."),
                 _("I will repay you in another life... Take care, Dad..."),
                 _("Farewell, Master... Thank you for all that you've done for us..."),
                 _("*Guo Zhiqiang and Zhao Huilin leave Huashan*"),
                 _("*Sigh*..."),
                 _("Sorry Master Guo, please forgive me for my insolence."),
                 _("Perhaps this is fate...")
                 ],
                ["you", "you", "Zhao Huilin", "Guo Zhiqiang", "Guo Zhiqiang", "Guo Junda",
                 "Guo Zhiqiang", "Guo Zhiqiang", "Zhao Huilin", "Blank", "Guo Junda",
                 "you", "Guo Junda"]
            )

            self.game.taskProgressDic[_("guo_zhiqiang")] = 11
            self.__init__(self.game)

        elif choice == 12:
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("You are no match for me. Now, please stay out of our family business."),
                 _("Sorry guys... I tried..."),
                 _("Master, thank you for all these years in which you've taken care of me."),
                 _("I'm sorry for what I've done... To protect Huashan's reputation, I'll accept the consequences."),
                 _("Huilin..."),
                 _("Good, you know what you need to do then..."),
                 _("Zhiqiang... The past few years have been the happiest years of my life."),
                 _("After I'm gone, you need to take good care of yourself, ok?"),
                 _("Huilin..."),
                 _("Zhiqiang, I love you so much..."),
                 _("I love you too..."),
                 _("Good bye, Zhiqiang..."),
                 _("*Zhao Huilin jumps off the cliff*"),
                 _("HUILIN!!!!!!!!"),
                 _(".............."),
                 _("HUILIN!!!!! WHY???!!! AHHHHHHHHHHHHHH!!!!!!"),
                 _("Father...... this.... THIS IS ALL YOUR FAULT!!!"),
                 _("I.... I will never forgive you..."),
                 _("One day, you will understand the importance of Huashan Sect's reputation..."),
                 _("That's all you ever care about... Your reputation... Huashan's reputation..."),
                 _("You are not fit to be a father..."),
                 _("I never want to see you again..."),
                 _("*Guo Zhiqiang leaves*")
                 ],
                ["Guo Junda", "you", "Zhao Huilin", "Zhao Huilin", "Guo Zhiqiang", "Guo Junda",
                 "Zhao Huilin", "Zhao Huilin", "Guo Zhiqiang", "Zhao Huilin", "Guo Zhiqiang",
                 "Zhao Huilin", "Blank", "Guo Zhiqiang", "you", "Guo Zhiqiang", "Guo Zhiqiang",
                 "Guo Zhiqiang", "Guo Junda", "Guo Zhiqiang", "Guo Zhiqiang", "Guo Zhiqiang", "Blank"
                 ]
            )


            self.game.taskProgressDic[_("guo_zhiqiang")] = 12
            self.__init__(self.game)


        elif choice == 21:
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("Huilin... looks like this is our fate..."),
                 _("Even this stranger opposes us..."),
                 _("Zhiqiang... I don't care what they think... I have no regrets..."),
                 _("Huilin... I love you... "),
                 _("Zhiqiang, I love you too..."),
                 _("These past few years with you have been the happiest years of my life..."),
                 _("As long as we can be together, that's all that matters..."),
                 _("Yes... together... forever..."),
                 ],
                ["Guo Zhiqiang", "Guo Zhiqiang", "Zhao Huilin", "Guo Zhiqiang", "Zhao Huilin", "Zhao Huilin", 
                 "Guo Zhiqiang", "Zhao Huilin",
                 ]
            )
            self.game.currentEffect = "sfx_sword6.mp3"
            self.game.startSoundEffectThread()
            time.sleep(1)
            self.game.generate_dialogue_sequence(
                None,
                "Huashan Cliff.png",
                [_("*Guo Zhiqiang and Zhao Huilin commit suicide*"),
                 _("..............."),
                 _("*Sigh*... Why did you guys have to make such a foolish decision..."),
                 _("But at least Huashan's reputation has been upheld..."),
                 _("Master Guo! You truly are an impartial, righteous man, not even sparing your own son!"),
                 _("I am very impressed by your integrity; you have earned my utmost respect!"),
                 _("Thank you, young hero... I am comforted that you agree with my judgment..."),
                 _("I am very drawn to your righteous character and believe it was fate that led us to cross paths with each other."),
                 _("As such, I will now teach you the Huashan Sword Technique and hope you will use it to perform more chivalrous deeds."),
                 _("Thank you, Master Guo! I will be sure to do all I can to benefit Huashan's reputation!")
                 ],
                ["Blank", "you", "Guo Junda", "Guo Junda", "you", "you", "Guo Junda", "Guo Junda",
                 "Guo Junda", "you"
                 ]
            )

            new_move = special_move(name=_("Huashan Sword Technique"))
            self.game.learn_move(new_move)
            messagebox.showinfo("", _("Guo Junda taught you a new move: Huashan Sword Technique!"))

            self.game.taskProgressDic[_("guo_zhiqiang")] = 21
            self.__init__(self.game)


    def post_masked_thief_battle(self):
        if self.game.taskProgressDic["feng_xinyi"] == 5:
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Looks like you are still no match for me, despite the Shaolin Kung-fu that you've stolen."),
                 _("What's with the other moves though? Where did you learn those?"),
                 _("Why so many questions? You're a dead man anyway... You think you are safe just because you defeated me?"),
                 _("Oh, is your 'Boss' here as well? Tell him to come out. I'd love to meet him."),
                 _("It seems that this young man is eager to meet you..."),
                 _("Master Guo..."),
                 ],
                ["you", "you", "Masked Thief", "you","Masked Thief", "Masked Thief",
                 ],
            )

        else:
            self.game.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Just who are you, really? Why do you know Shaolin Kung-fu?"),
                 _("I suppose there's no harm in letting you know..."),
                 _("For the past few years, I've been stealing their techniques by sneaking into the Shaolin Scripture Library."),
                 _("I've learned their most advanced techniques, and that, in addition to my Eagle Sect Kung-fu, makes me almost invincible."),
                 _("I'm quite surprised you were able to defeat me, but no matter, soon you will cease to exist."),
                 _("Hahaha... I thought you were the strongest bad guy here. Surely you don't expect your useless goons to take me down."),
                 _("Oh no... my men are far too weak to go against you..."), #Row 2
                 _("But with the help of an old friend, I think we can get rid of you..."),
                 _("Isn't that right... Master Guo?"),
                 ],
                ["you", "Masked Thief", "Masked Thief", "Masked Thief", "Masked Thief", "you",
                 "Masked Thief", "Masked Thief", "Masked Thief",
                 ],
            )


        #TODO: add XLFD music here
        self.game.stopSoundtrack()
        self.game.currentBGM = "xlfd_suspense.mp3"
        self.game.startSoundtrackThread()


        self.game.generate_dialogue_sequence(
            None,
            "Dark Forest.png",
            [_("Hahahaha... I guess I have to reveal myself after all, Master Jiang..."),
             _("Wait, you... you two know each other??"),
             _("Hahahahahaha... Master Guo and I have been friends for years."),
             _("He has always been kind towards Eagle Sect. Since we are a fairly new Sect in the Martial Arts Community, we've had to overcome many challenges..."),
             _("Including financial stability... Master Guo has been more than generous with us."),
             _("And you are...?"),
             _("Jiang Yuanqing, head of Eagle Sect."), #Row 2
             _("Guo Junda must've given you a large sum of money for you to obey his every order..."),
             _("It wasn't just the money... Master Guo promised to lend me the manual after he's done with it."),
             _("It's a good deal for both of us."),
             _("Why him first? Why not you learn it and then give it to him?"),
             _("Because Huashan has always had a reputation for being a chivalrous sect. No one would suspect that Master Guo is involved in the search for the manual."),
             _("It is much safer for him to be in possession of the manual until things quiet down."), #Row 3
             _("Even if I had it, I would not be able to practice the Devil Crescent Moon Blade in peace."),
             _("You are too naive to think that Guo Junda would share that with you..."),
             _("You're lucky if he doesn't kill you once he's done using you."),
             _("Trying to make us doubt each other? Nice try... I will not fall for it..."),
             #_("Hah... Guo Junda even watched his own son and disciple die, all in order to protect Huashan's reputation..."),
             _("After all, Huashan's reputation seems to be all that he cares about..."),
             _("You'd better shut up right now..."), #Row 4
             _("Now that you know about all these wicked deeds he's committing in secret, do you think he'll let you live long?"),
             _("I SAID SHUT UP!!!")
             ],
            ["Guo Junda", "you", "Masked Thief", "Masked Thief", "Masked Thief", "you",
             "Masked Thief", "you", "Masked Thief", "Masked Thief", "you", "Masked Thief",
             "Masked Thief", "Masked Thief", "you", "you", "Masked Thief", "you",
             "Guo Junda", "you", "Guo Junda"
             ],
        )

        opp = character(_("Guo Junda"), 25,
                        sml=[special_move(_("Devil Crescent Moon Blade"), level=10),],
                        skills=[skill(_("Tendon Changing Technique"), level=10),
                                skill(_("Basic Agility Technique"), level=10),
                                skill(_("Shaolin Inner Energy Technique"), level=10),
                                skill(_("Huashan Agility Technique"), level=10),
                                skill(_("Huashan Sword Qi Gong"), level=10)],
                        )
        self.game.battleMenu(self.game.you, opp, "battleground_dark_forest.png", "jy_suspenseful2.mp3", fromWin=None,
                             battleType="task", postBattleCmd=self.defeated_evil_guo_junda)


    def defeated_evil_guo_junda(self, choice=0):
        self.game.taskProgressDic["liu_village"] = 1000
        #Ending if GJD is still head of Huashan
        if self.game.taskProgressDic["guo_zhiqiang"] in  [11,21]:
            if choice == 0:
                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("NO! I CAN'T LOSE! I'VE LEARNED THE MOST POWERFUL TECHNIQUE IN THE WORLD!"),
                     _("YOU... YOU LOST! I WON! I'M UNDEFEATED... I --"),
                     _("*Guo Junda spits out a stream of dark red blood.*"),
                     _("How pitiful... Let me put an end to your misery..."),
                     ],
                    ["Guo Junda", "Guo Junda", "Guo Junda", "you"]
                )
                self.game.currentEffect = "sfx_sword6.mp3"
                self.game.startSoundEffectThread()
                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("M-Master Guo... "),
                     _("As for you..."),
                     _("WAIT! If you let me go, I will repay you greatly...")],
                    ["Masked Thief", "you", "Masked Thief"],
                    [[_("Kill him"), partial(self.defeated_evil_guo_junda, 1)],
                     [_("Spare him"), partial(self.defeated_evil_guo_junda, 2)]]
                )

            elif choice == 1:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.currentEffect = "sfx_sword6.mp3"
                self.game.startSoundEffectThread()
                self.game.chivalry += 30
                messagebox.showinfo("", _("You end Jiang Yuanqing's life. Your chivalry +30!"))
                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("(Hmmm... let's see if we can find the Devil Crescent Moon Blade Manual now...)"),
                     _("*You search Guo Junda's body and find the manual!*")
                     ],
                    ["you", "you"],
                    [[_("Destroy the manual so that no one can practice this evil technique."), partial(self.defeated_evil_guo_junda, 11)],
                     [_("Keep it for yourself"), partial(self.defeated_evil_guo_junda, 12)]]
                )

            elif choice == 2:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("Thank you for your mercy! I will not forget your goodness!"),
                     _("Here, please take this in return...")],
                    ["Masked Thief", "Masked Thief"]
                )
                self.game.chivalry -= 30
                if _("Smoke Bomb") in [m.name for m in self.game.sml] and _("Eagle Claws") not in [m.name for m in self.game.sml]:
                    self.game.add_item(_("Eagle Claws Manual"))
                    self.game.add_item(_("Smoke Bomb"), 100)
                    messagebox.showinfo("", _("Jiang Yuanqing gives you the 'Eagle Claws Manual' and 100 Smoke Bombs. Your chivalry -30!"))
                elif _("Smoke Bomb") in [m.name for m in self.game.sml]:
                    self.game.add_item(_("Smoke Bomb"), 200)
                    messagebox.showinfo("", _("Jiang Yuanqing gives you 200 Smoke Bombs. Your chivalry -30!"))
                else:
                    self.game.add_item(_("Smoke Bomb"), 100)
                    new_move = special_move(name=_("Smoke Bomb"))
                    self.game.learn_move(new_move)
                    messagebox.showinfo("", _("Jiang Yuanqing gives you 100 Smoke Bombs and teaches you how to use them. Your chivalry -30!"))

                self.game.generate_dialogue_sequence(
                    None,
                    "Dark Forest.png",
                    [_("(Hmmm... let's see if we can find the Devil Crescent Moon Blade Manual now...)"),
                     _("*You search Guo Junda's body and find the manual!*")
                     ],
                    ["you", "you"],
                    [[_("Destroy the manual so that no one can practice this evil technique."), partial(self.defeated_evil_guo_junda, 11)],
                     [_("Keep it for yourself"), partial(self.defeated_evil_guo_junda, 12)]]
                )

            elif choice == 11:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.chivalry += 50
                messagebox.showinfo("", _("You shred to pieces and burn up the manual that has caused much conflict and strife. Your chivalry +50!"))
                self.game.mapWin.deiconify()

            elif choice == 12:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.add_item(_("Devil Crescent Moon Blade Manual"))
                self.game.chivalry -= 50
                messagebox.showinfo("", _("You decide to keep the manual for yourself... Your chivalry -50!"))
                self.game.mapWin.deiconify()


        #alternate ending
        elif self.game.taskProgressDic["guo_zhiqiang"] == 17 or self.game.taskProgressDic["join_sect"] == 702:
            if choice == 0:
                self.game.generate_dialogue_sequence(
                    None,
                    "Huashan Cliff.png",
                    [_("NO! I CAN'T LOSE! I'VE LEARNED THE MOST POWERFUL TECHNIQUE IN THE WORLD!"),
                     _("YOU... YOU LOST! I WON! I'M UNDEFEATED... I --"),
                     _("*Guo Junda spits out a stream of dark red blood.*"),
                     _("How pitiful... Let me put an end to your misery..."),
                     ],
                    ["Guo Junda", "Guo Junda", "Guo Junda", "you"]
                )

                self.game.currentEffect = "sfx_sword6.mp3"
                self.game.startSoundEffectThread()
                time.sleep(.5)

                self.game.generate_dialogue_sequence(
                    None,
                    "Huashan Cliff.png",
                    [_("*You stick a sword through Guo Junda's throat*"),
                     _("Finally, it's over..."),
                     _("I can't believe that was my father... All these years, I never saw his true side..."),
                     _("Thank you again, {}, for saving our lives.").format(self.game.character),
                     _("Ah, don't mention it; he would've killed me too if I hadn't beaten him..."),
                     _("*Guo Zhiqiang quickly searches Guo Junda's body and finds a manual.*"),
                     _("Here, {}. I trust you know what to do with this. I will go bury my... father... now.").format(self.game.character),
                     _("*Guo Zhiqiang and Zhao Huilin carry Guo Junda's body away.*"),
                     _("(Hmmm, what should I do with the Devil Crescent Moon Blade Manual?)")
                     ],
                    ["Blank", "you", "Guo Zhiqiang", "Zhao Huilin", "you", "Blank", "Guo Zhiqiang", "Blank", "you"],
                    [[_("Destroy the manual so that no one can practice this evil technique."),
                      partial(self.defeated_evil_guo_junda, 11)],
                     [_("Keep it for yourself"), partial(self.defeated_evil_guo_junda, 12)]]
                )

            elif choice == 11:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.chivalry += 50
                messagebox.showinfo("", _("You shred to pieces and burn up the manual that has caused much conflict and strife. Your chivalry +50!"))
                self.game.mapWin.deiconify()

            elif choice == 12:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.add_item(_("Devil Crescent Moon Blade Manual"))
                self.game.chivalry -= 50
                messagebox.showinfo("", _("You decide to keep the manual for yourself... Your chivalry -50!"))
                self.game.mapWin.deiconify()


    def post_zhao_huilin_guo_junda_battle(self):
        self.game.generate_dialogue_sequence(
            None,
            "Huashan Cliff.png",
            [_("Weak... You barely lasted half a minute... HAHAHAHAHAHA!"),
             _("Where... where did you learn such a powerful technique?"),
             _("How did you get so strong all of a sudden...??"),
             _("Like I thought, not even my wife's Wild Wind Blade is a match for the Devil Crescent Moon Blade! AHAHAHAHAHA!!!!"),
             _("Devil Crescent Moon Blade... Why... How did you get your hands on the manual?!"),
             _("I've spent countless sleepless nights to come up with a scheme to get the manual."),
             _("No one besides me deserves it... NO ONE!!!"), #Row 2
             _("Then those masked assassins... You were behind it all??"),
             _("I hired them... They were men from Eagle Sect, including the leader, Jiang Yuanqing..."),
             _("If you've been paying attention to what's happening in Wulin, you would know that Eagle Sect has been empty for a couple of months."),
             _("That's because they've been busy following my orders... HAHAHAHAHAHA!"),
             _("Then where are they now?"),
             _("Dead... I can't have people knowing about my true ambitions... The only kind of people who can keep secrets are dead people..."), #Row 3
             _("It was also my way of testing how powerful the Devil Crescent Moon Blade really is."),
             _("Even Jiang Yuanqing could not last 20 bouts against me."),
             _("Before I killed him, he begged me to spare him and offered to give me all the Shaolin Kung-Fu manuals that he's been stealing over the years."),
             _("I happily took the manuals before chopping his head off... And now, I've learned the top Shaolin techniques as well..."),
             _("I AM INVINCIBLE! HAHAHAHAHAHAHAHAHA!!!!!!"),
             _("All of Wulin will bow to me!!! AHAHAHAHAHAHAHAHAHA!!!"), #Row 4
             _("As for you all... I will not grant you the privilege of bowing to me due to all the trouble you've caused me."),
             _("There exists only one option... death...")
             ],
            ["Guo Junda", "Zhao Huilin", "Zhao Huilin", "Guo Junda", "you", "Guo Junda",
             "Guo Junda", "you", "Guo Junda", "Guo Junda", "Guo Junda", "you",
             "Guo Junda", "Guo Junda", "Guo Junda", "Guo Junda", "Guo Junda", "Guo Junda",
             "Guo Junda", "Guo Junda", "Guo Junda"
             ]
        )

        opp = character(_("Guo Junda"), 25,
                        sml=[special_move(_("Devil Crescent Moon Blade"), level=10),],
                        skills=[skill(_("Tendon Changing Technique"), level=10),
                                skill(_("Basic Agility Technique"), level=10),
                                skill(_("Shaolin Inner Energy Technique"), level=10),
                                skill(_("Huashan Agility Technique"), level=10),
                                skill(_("Huashan Sword Qi Gong"), level=10)],
                        )
        self.game.battleMenu(self.game.you, opp, "battleground_huashan.png", "jy_suspenseful2.mp3", fromWin=None,
                             battleType="task", postBattleCmd=self.defeated_evil_guo_junda)


    def post_return_to_huashan_battle(self):
        self.game.generate_dialogue_sequence(
            None,
            "Huashan Cliff.png",
            [_("AHHHHH! No... No! HOW COULD I LOSE TO YOU?! I TAUGHT YOU!!!"),
             _("I am ashamed to call you my master... Any last words?"),
             _("Wait! {}! Please... please spare my father...").format(self.game.character),
             _("I know he has made many mistakes in the past, but... he's still my father..."),
             _("Please give him a chance to repent and turn from his evil ways..."),
             _("You are too naive... someone like him won't change."),
             _("I will! I will change! I promise!"), # Row 2
             _("Please, just spare me! I will disappear from the Martial Arts Community!"),
             _("From this day forward, I, Guo Junda, will no longer be a part of Wulin!"),
             _("Please, {}... I'm begging you!").format(self.game.character),
             _("..... I'm not convinced, but for the sake of your son, I will trust you. Just this once."),
             _("Thank you, {}! I will surely turn from my wicked ways!").format(self.game.character),
             _("*Guo Junda runs away, almost tripping and falling over a rock.*"), #Row 3
             _("I sure hope that's the last we see of him..."),
             _("So, what do we do now? Huashan no longer has a leader..."),
             _("Well, I'd say Zhiqiang should take up the position. There's no one more suitable."),
             _("(Pity Huashan sect has become so weak, but oh well, none of my business...)"),
             _("I did what I came to do; wish you guys the best!")
             ],
            ["Guo Junda", "you", "Guo Zhiqiang", "Guo Zhiqiang", "Guo Zhiqiang", "you",
             "Guo Junda", "Guo Junda", "Guo Junda", "Guo Zhiqiang", "you", "Guo Junda",
             "Blank", "you", "Zhao Huilin", "you", "you", "you"]
        )

        self.game.mapWin.deiconify()