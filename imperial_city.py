from tkinter import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
from mini_game_imperial_palace_day import*
from mini_game_imperial_palace_night import*
import time
import tkinter.messagebox as messagebox



_ = gettext


class Scene_imperial_city:

    def __init__(self, game):
        self.game = game
        self.game.imperial_city_win = Toplevel(self.game.mapWin)
        self.game.imperial_city_win.title(_("Imperial City"))

        bg = PhotoImage(file="Imperial City.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.imperial_city_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.game.imperial_cityF1 = Frame(canvas)
        pic1 = PhotoImage(file="Imperial Guard_icon_large.ppm")
        imageButton1 = Button(self.game.imperial_cityF1, command=self.clickedOnImperialGuard)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.game.imperial_cityF1, text=_("Imperial\nGuard"))
        label.grid(row=1, column=0)
        self.game.imperial_cityF1.place(x=w // 4 - pic1.width() // 2, y=h // 4 - pic1.height() // 2)


        self.game.imperial_cityF2 = Frame(canvas)
        pic2 = PhotoImage(file="Imperial Palace_icon_large.ppm")
        imageButton2 = Button(self.game.imperial_cityF2, command=self.goImperialPalace)
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(self.game.imperial_cityF2, text=_("Imperial\nPalace"))
        label.grid(row=1, column=0)
        self.game.imperial_cityF2.place(x=w * 3// 4 - pic2.width() // 2, y=h // 4 - pic2.height() // 2)

        menu_frame = self.game.create_menu_frame(self.game.imperial_city_win)
        menu_frame.pack()
        self.game.imperial_city_win_F1 = Frame(self.game.imperial_city_win)
        self.game.imperial_city_win_F1.pack()

        self.game.imperial_city_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.imperial_city_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.imperial_city_win.winfo_width(), self.game.imperial_city_win.winfo_height()
        self.game.imperial_city_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.imperial_city_win.focus_force()
        self.game.imperial_city_win.mainloop()  ###


    def clickedOnImperialGuard(self):
        if self.game.taskProgressDic["imperial_palace"] <= 0:
            self.game.generate_dialogue_sequence(
                self.game.imperial_city_win,
                "Imperial City.png",
                [_("Can I visit the Imperial Palace please?"),
                 _("No unauthorized personnel allowed.")],
                ["you", "Imperial Guard"]
            )
        elif self.game.taskProgressDic["imperial_palace"] > 0:
            self.game.imperial_city_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Imperial City.png",
                [_("You again?! You little trouble maker... I will not let you get away this time!"),],
                ["Imperial Guard"]
            )
            opp = character(_("Imperial Guard"), 14,
                            sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                                 special_move(_("Shaolin Diamond Finger"), level=5),
                                 special_move(_("Taichi Fist"), level=8),
                                 special_move(_("Shapeless Palm"), level=8),],
                            skills=[skill(_("Shaolin Inner Energy Technique"), level=8),
                                    skill(_("Wudang Agility Technique"), level=8),
                                    skill(_("Pure Yang Qi Skill"), level=8)
                                    ])

            self.game.battleMenu(self.game.you, opp, "battleground_imperial_palace_day.png", "jy_xiaoyaogu.mp3",
                                 fromWin=self.game.mapWin, battleType="normal", destinationWinList=[self.game.mapWin]*3,
                                 )


    def goImperialPalace(self):
        if _("Ninja Suit") not in [eq.name for eq in self.game.equipment]:
            if self.game.taskProgressDic["imperial_palace"] == -1:
                self.game.generate_dialogue_sequence(
                    self.game.imperial_city_win,
                    "Imperial City.png",
                    [_("Hey! Where do you think you're going?"),
                     _("Commoners are not allowed to go into the Imperial Palace like that!"),
                     _("Ah! My apologies, sir... I got lost haha..."),
                     _("(Looks like I'll have to find another way in...)"),
                     _("(Definitely don't wanna get in trouble with the authorities...)"),],
                    ["Imperial Guard", "Imperial Guard", "you", "you", "you"]
                )
                self.game.taskProgressDic["imperial_palace"] = 0

            elif self.game.taskProgressDic["imperial_palace"] >= 0:
                if random() <= .7:
                    self.game.imperial_city_win.withdraw()
                    self.game.generate_dialogue_sequence(
                        None,
                        "Imperial City.png",
                        [_("Hey you! Peasant! What are you trying to do?"),
                         _("Looking for a way to sneak in?! Guards! Get him!")],
                        ["Imperial Guard", "Imperial Guard"]
                    )

                    if self.game.taskProgressDic["imperial_palace"] == 0:
                        self.game.taskProgressDic["imperial_palace"] = 1

                    self.game.mini_game_in_progress = True
                    self.game.mini_game = imperial_palace_day_mini_game(self.game, self)
                    self.game.mini_game.new()

                else:
                    self.game.generate_dialogue_sequence(
                        self.game.imperial_city_win,
                        "Imperial City.png",
                        [_("(I'd better find something that can help me sneak in at night...)"),
                         _("(Definitely don't wanna get in trouble with the authorities...)"),],
                        ["you", "you"]
                    )

        else:
            self.sneak_into_imperial_palace()


    def sneak_into_imperial_palace(self):
        response = messagebox.askquestion("", _("Sneak into the Imperial Palace at night?"))
        if response == "yes":
            self.game.imperial_city_win.quit()
            self.game.imperial_city_win.destroy()
            self.go_imperial_palace_night()


    def go_imperial_palace_night(self):
        if self.game.taskProgressDic["imperial_palace"] == 5:
            self.game.taskProgressDic["imperial_palace"] = 6
            self.game.generate_dialogue_sequence(
                None,
                "Imperial City.png",
                [_("I heard that there's been numerous disturbances recently..."),
                 _("Yes, General Feng, it is my fault. I have failed to secure the palace appropriately."),
                 _("But, don't worry. I have posted more guards."),
                 _("If that intruder dares to come again, I'll be sure to capture him!"),
                 _("That, or just slay him on the spot..."),
                 _("Understood!"),
                 _("(Hmmm interesting... I'd better be extra cautious next time I sneak in...)")],
                ["Feng Pan", "Imperial Guard", "Imperial Guard", "Imperial Guard", "Feng Pan", "Imperial Guard", "you"]
            )

        self.game.imperial_palace_win = Toplevel(self.game.mapWin)
        self.game.imperial_palace_win.title(_("Imperial Palace"))

        bg = PhotoImage(file="battleground_imperial_palace_night.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.imperial_palace_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.game.imperial_palaceF1 = Frame(canvas)
        pic1 = PhotoImage(file="inner_palace_icon_large.ppm")
        imageButton1 = Button(self.game.imperial_palaceF1, command=self.go_inner_palace)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        self.game.imperial_palaceF1.place(x=w // 4 - pic1.width() // 2, y=h // 4 - pic1.height() // 2)

        self.game.imperial_palaceF2 = Frame(canvas)
        pic2 = PhotoImage(file="imperial_palace_night_2_icon_large.ppm")
        imageButton2 = Button(self.game.imperial_palaceF2, command=self.go_general_feng)
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        self.game.imperial_palaceF2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 4 - pic2.height() // 2)

        menu_frame = self.game.create_menu_frame(self.game.imperial_palace_win)
        menu_frame.pack()
        self.game.imperial_palace_win_F1 = Frame(self.game.imperial_palace_win)
        self.game.imperial_palace_win_F1.pack()

        self.game.imperial_palace_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.imperial_palace_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.imperial_palace_win.winfo_width(), self.game.imperial_palace_win.winfo_height()
        self.game.imperial_palace_win.geometry("+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2,
                                                           5))
        self.game.imperial_palace_win.focus_force()
        self.game.imperial_palace_win.mainloop()  ###


    def go_general_feng(self):
        self.game.stopSoundtrack()
        self.game.currentBGM = "jy_shenmishandong.mp3"
        self.game.startSoundtrackThread()
        if self.game.taskProgressDic["feng_xinyi"] in [3,4,5] and self.game.taskProgressDic["feng_pan"] < 0:
            self.game.taskProgressDic["feng_pan"] = 0
            self.game.generate_dialogue_sequence(
                self.game.imperial_palace_win,
                "battleground_imperial_palace_night_2.png",
                [_("Father, I'm back!"),
                 _("Junyi! How did it go? Any news of your sister?"),
                 _("I along with 10 other men spent almost a month looking, but there's no trace of her."),
                 _("That brat... where could she have gone?"),
                 _("Such a disgrace to the family, running away like that... and all because I wouldn't teach her martial arts?!"),
                 _("Why in the world would a young lady like her learn martial arts? She should be learning how to be a real lady!"),
                 _("She will come of age soon and will need to be married..."), #Row 2
                 _("Don't get angry, dad. We'll find her soon... or maybe she'll return on her own."),
                 _("As if I can depend on that... She turned out this way because I've spoiled her..."),
                 _("I'm just reaping what I sowed..."),
                 _("Don't say that, dad.. I don't think you did anything wrong."),
                 _("If only your sister could be half as mature as you... speaking of marriage, I've arranged someone for you, son."),
                 _("Y-you have? Who is it?"), #Row 3
                 _("Ouyang Nana, the niece of Ouyang Xiong from Dragon Sect."),
                 _("What do you think, Junyi? Is she a good fit for you?"),
                 _("She's quite pretty... I think she will be a wonderful spouse."),
                 _("Hahahahaha! Glad to hear that! I knew I made the right choice!"),
                 _("Thanks, dad! Should I continue to look for Xinyi?"),
                 _("No no, you stay home for a few days and rest. I will send some men to find this spoiled sister of yours."), #Row 4
                 _("I will also post a reward for 10000 Gold to the public, so we'll have millions of eyes searching for her."),
                 _("Great idea, dad! Good night!"),
                 _("(Wait a second... Feng.. Xinyi???)"),
                 _("(Man, I gotta go talk to her about this...)")],
                ["Feng Junyi", "Feng Pan", "Feng Junyi", "Feng Pan", "Feng Pan", "Feng Pan",
                 "Feng Pan", "Feng Junyi", "Feng Pan", "Feng Pan", "Feng Junyi", "Feng Pan",
                 "Feng Junyi", "Feng Pan", "Feng Pan", "Feng Junyi", "Feng Pan", "Feng Junyi",
                 "Feng Pan", "Feng Pan", "Feng Junyi", "you", "you"
                 ]
            )

        elif self.game.taskProgressDic["feng_pan"] >= 0 and self.game.luck >= 80 and _("Shapeless Palm Manual") not in self.game.inv:
            self.game.generate_dialogue_sequence(
                self.game.imperial_palace_win,
                "battleground_imperial_palace_night_2.png",
                [_("Junyi, your Shapeless Palm is improving, but it is not yet mature."),
                 _("Once you've mastered it, it will be very difficult for the opponent to anticipate your strikes."),
                 _("Here's the manual; study it hard. I will check on your progress again in a week."),
                 _("Yes dad! I will practice day and night!"),
                 _("Very good... I am very proud of you, my son."),
                 _("*Feng Pan walks away, leaving Feng Junyi to practice alone.*")],
                ["Feng Pan", "Feng Pan", "Feng Pan", "Feng Junyi", "Feng Pan", "Blank"],
                [[_("Take the manual from Feng Junyi."), partial(self.steal_shapeless_palm_manual, 1)],
                 [_("Come back another time."), partial(self.steal_shapeless_palm_manual,0)]]
            )

        else:
            self.game.generate_dialogue_sequence(
                self.game.imperial_palace_win,
                "battleground_imperial_palace_night_2.png",
                [_("(Hmmm, doesn't look like there's anything interesting here...)"),
                 _("(Maybe I should come back at another time.)"), ],
                ["you", "you"]
            )


    def steal_shapeless_palm_manual(self, choice=2):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        if choice == 1:
            self.game.imperial_palace_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "battleground_imperial_palace_night_2.png",
                [_("Hey buddy, gotta borrow something from you..."),
                 _("Who the heck are --"), ],
                ["you", "Feng Junyi"]
            )

            opp = character(_("Feng Junyi"), 14,
                            sml=[special_move(_("Shapeless Palm"), level=8), ],
                            skills=[skill(_("Shaolin Inner Energy Technique"), level=4),
                                    skill(_("Wudang Agility Technique"), level=4),
                                    skill(_("Pure Yang Qi Skill"), level=4),
                                    skill(_("Basic Agility Technique"), level=10)
                                    ],
                            preferences=[1, 2, 2, 1, 1])

            self.game.battleMenu(self.game.you, opp, "battleground_imperial_palace_night_2.png", "jy_shenmishandong.mp3",
                            fromWin=None, battleType="task",
                            destinationWinList=[], postBattleCmd=self.steal_shapeless_palm_manual
                            )

        elif choice == 2:
            self.game.add_item(_("Shapeless Palm Manual"))
            self.game.generate_dialogue_sequence(
                None,
                "battleground_imperial_palace_night_2.png",
                [_("Hahahahahaha! So easy!"),
                 _("*You grab the manual and run away.*")],
                ["you", "Blank"]
            )
            self.game.imperial_palace_win.deiconify()

        elif choice == 0:
            self.game.imperial_palace_win.deiconify()



    def go_inner_palace(self):
        self.game.imperial_palace_win.quit()
        self.game.imperial_palace_win.destroy()

        self.game.generate_dialogue_sequence(
            None,
            "battleground_inner_palace.png",
            [_("(Ahahaha... so this is where the Emperor sits during the day time huh...)"),
             _("(Let me see what I can take from here...)"), ],
            ["you", "you"],
            [[_("Quick Search (Low chance of detection)"), partial(self.imperial_palace_search, 1)],
             [_("Thorough Search (High chance of detection)"), partial(self.imperial_palace_search, 2)],
             [_("Get out of here before someone sees you."), partial(self.imperial_palace_search, 3)]],
        )


    def imperial_palace_search(self, choice):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        if choice == 1:
            self.game.taskProgressDic["imperial_palace"] += 1
            r = int(randrange(700,1000)*self.game.luck/50)
            self.game.inv[_("Gold")] += r
            messagebox.showinfo("", _("Found 'Gold' x {}!").format(r))
            if random() <= .5:
                self.imperial_palace_search_battle()
            else:
                self.game.mapWin.deiconify()

        elif choice == 2:
            self.game.taskProgressDic["imperial_palace"] += 1
            if random()*self.game.luck/50 >= .95:
                r = pickOne([_("Dragon Tear"), _("Sunstone"), _("Golden Chainmail")])
                self.game.add_item(r)
                messagebox.showinfo("", _("Found '{}' x 1!").format(r))

            else:
                r = int(randrange(1000,1500)*self.game.luck/50)
                self.game.inv[_("Gold")] += r
                messagebox.showinfo("", _("Found 'Gold' x {}!").format(r))

            if random() <= .8:
                self.imperial_palace_search_battle()
            else:
                self.game.mapWin.deiconify()

        else:
            self.game.mapWin.deiconify()


    def imperial_palace_search_battle(self):
        self.game.generate_dialogue_sequence(
            None,
            "battleground_inner_palace.png",
            [_("Hey! What are you doing?! Trying to assassinate the Emperor?")],
            ["Imperial Guard"]
        )
        cmd = self.imperial_palace_search_chase
        opp = character(_("Imperial Guard"), 16,
                        sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                             special_move(_("Shaolin Diamond Finger"), level=5),
                             special_move(_("Taichi Fist"), level=8),
                             special_move(_("Shapeless Palm"), level=8), ],
                        skills=[skill(_("Shaolin Inner Energy Technique"), level=8),
                                skill(_("Wudang Agility Technique"), level=8),
                                skill(_("Pure Yang Qi Skill"), level=8)
                                ])

        self.game.battleMenu(self.game.you, opp, "battleground_inner_palace.png", "jy_daojiangu.mp3",
                             fromWin=None, battleType="task",
                             destinationWinList=[], postBattleCmd=cmd
                             )


    def imperial_palace_search_chase(self):
        self.game.generate_dialogue_sequence(
            None,
            "battleground_inner_palace.png",
            [_("Listen up, my men! Don't let him get away!")],
            ["Imperial Guard"]
        )

        self.game.mini_game_in_progress = True
        self.game.mini_game = imperial_palace_night_mini_game(self.game, self)
        self.game.mini_game.new()