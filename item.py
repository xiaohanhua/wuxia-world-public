from random import*
from gettext import gettext
from re import sub, findall

_ = gettext

class Item:
    def __init__(self, name, game, targ=None):
        self.name = name
        self.game = game
        self.targ = targ

        self.dh = 0
        self.ds = 0
        self.datk = 0
        self.dstr = 0
        self.dspd = 0
        self.ddef = 0
        self.ddex = 0
        self.status = None
        self.status_condition = None

        if targ:
            self.battle=True
        else:
            self.battle=False

        if self.name == _("Crab"):
            self.dh = 60
            self.ds = 60
        elif self.name == _("Fish"):
            self.dh = 80
            self.ds = 20
        elif self.name == _("Soup"):
            self.dh = 70
        elif self.name == _("Dumplings"):
            self.ds = 100
        elif self.name == _("Tea Eggs"):
            self.dh = 300
            self.ds = 300
        elif self.name == _("Winterberry"):
            self.dh = 500
        elif self.name == _("Snow Lotus Pill"):
            self.dh = .2
            self.status = _("Normal")
        elif self.name == _("Bezoar Pill"):
            self.status = _("Normal")
            self.status_condition = _("Poison")
        elif self.name == _("Small Recovery Pill"):
            self.dh = .3
        elif self.name == _("Big Recovery Pill"):
            self.dh = .5
        elif self.name == _("Wine"):
            self.datk = -2
            self.dstr = 10
        elif self.name == _("Cherry Blossom Papaya Tremella Soup"):
            self.dh = .25
            self.ds = .4



    def use(self):
        if not self.targ:
            reason = "normal"
            self.game.attack += self.datk
            self.game.strength += self.dstr
            self.game.speed += self.dspd
            self.game.defence += self.ddef
            self.game.dexterity += self.ddex
            if self.dh > 1 or self.ds > 1:
                self.game.restore(self.dh, self.ds, full=False, reason=reason)
            elif self.dh > 0 or self.ds > 0:
                self.game.restore(self.dh*self.game.healthMax, self.ds*self.game.staminaMax, full=False, reason=reason)

            if self.status:
                self.game.status = _("Normal")
                self.game.currentEffect = "sfx_recover.mp3"
                self.game.startSoundEffectThread()

        else:
            reason = "battle"
            self.targ.attack += self.datk
            self.targ.strength += self.dstr
            self.targ.speed += self.dspd
            self.targ.defence += self.ddef
            self.targ.dexterity += self.ddex
            if self.status:
                if self.status_condition == _("Poison"):
                    if _("Poison") in self.targ.status:
                        self.targ.status = sub(_("Poison[\S]*"), "", self.targ.status).strip()
                        if len(self.targ.status.strip()) == 0:
                            self.targ.status = _("Normal")
                else:
                    self.targ.status = _("Normal")

                self.game.currentEffect = "sfx_recover.mp3"
                self.game.startSoundEffectThread()
            elif self.dh > 0 or self.ds > 0:
                self.game.currentEffect = "sfx_recover.mp3"
                self.game.startSoundEffectThread()
            else:
                self.game.currentEffect = "sfx_potion.mp3"
                self.game.startSoundEffectThread()

            if self.dh > 1 or self.ds > 1:
                self.game.restore(self.dh, self.ds, full=False, reason=reason)
            elif self.dh > 0 or self.ds > 0:
                self.game.restore(self.dh*self.targ.healthMax, self.ds*self.targ.staminaMax, full=False, reason=reason)


            if self.name == _("Wine") and _("Drunken Bliss") in [s.name for s in self.targ.skills]:
                self.targ.strength += 10
                print(_("You gain an additional +10 due to effects of 'Drunken Bliss'."))



class Equipment:
    def __init__(self, name):

        self.attributes_dic = {
            _("Golden Gloves"): ["hands_left", 0, 10, 0, 10, 0, 0, .25, "Seal", 0.1],
            _("Dragon Amulet"): ["head", 5, 5, 0, 5, 0, 0, .1, "Critical Hit", 0.1],
            _("Bronze Gauntlets"): ["hands_left", 0, 5, 0, 5, 0, 0, .05, None, 0],
            _("Venomous Viper Gloves"): ["hands_left", 0, 5, 0, 5, 0, 0, .1, "Poison", .25],
            _("Golden Chainmail"): ["body", 0, 0, 0, 50, 0, 0, 0, None, 0],
            _("Steel Platelegs"): ["legs", 0, 0, -5, 30, 0, 0, 0, None, 0],
            _("Steel Platebody"): ["body", 0, 0, -10, 50, 0, 0, 0, None, 0],
            _("Hardened Steel Platebody"): ["body", 0, 0, -10, 70, 0, 0, 0, None, 0],
            _("Light Armor"): ["body", 0, 0, -2, 15, 0, 0, 0, None, 0],
            _("Ninja Suit"): ["body", 0, 0, 0, 0, 20, 0, 0, None, 0],
            _("Leather Body"): ["body", 0, 0, 0, 10, 0, 0, 0, None, 0],
            _("Leather Pants"): ["legs", 0, 0, 0, 7, 0, 0, 0, None, 0],
            _("Linen Shirt"): ["body", 0, 0, 0, 1, 0, 0, 0, None, 0],
            _("Heavy Adamantite Sword"): ["hands_right", 20, 50, -20, 20, 0, 0.5, 0, None, 0],
            _("Black Salamander"): ["hands_right", 20, 20, 10, 10, 0, 0.2, 0, "Critical Hit", .1],
            _("Golden Snake"): ["hands_right", 20, 0, 0, 20, 0, 0, 0, None, 0],
            _("Moonlight Blade"): ["hands_right", 10, 5, 5, 5, 0, 0, 0, "Health", 0.03],
            _("Phoenix Blade"): ["hands_right", 10, 15, 20, 0, 0, 0, 0, None, 0],
            _("Iron Dagger"): ["hands_right", 2, 2, 2, 0, 0, 0, 0, None, 0],
            _("Iron Sword"): ["hands_right", 5, 5, 5, 0, 0, 0, 0, None, 0],
            _("Steel Longsword"): ["hands_right", 10, 10, 0, 0, 0, 0, 0, None, 0],
            _("Seven-inch Serpent Staff"): ["hands_right", 0, 0, 0, 0, 0, 0, 0, "Poison", .5],
            _("Wings of the Dragonfly"): ["hands_right", 10, 10, 40, 10, 40, 0.1, 0, "Critical Hit", .1],
            _("Splinter Staff"): ["hands_right", 5, 5, 5, 40, 5, 0, 0, None, 0],
            _("Lightning Saber"): ["hands_right", 10, 10, 30, 0, 0, .1, 0, "Blind", .5],
            _("Flame Blade"): ["hands_right", 10, 30, 0, 0, 0, .1, 0, "Internal Injury", .3],
            _("Thunder Blade"): ["hands_right", 10, 30, 0, 0, 0, .25, 0, "Internal Injury", .15],
            _("Ice Shard"): ["hands_right", 0, 0, 20, 0, 0, .1, 0, "Disable", .25],
            _("Granite Boots"): ["feet", 0, 25, -10, 25, -10, 0, .25, "Critical Hit", 0.25],
            _("Babao Blade"): ["hands_right", 8, 8, 8, 8, 8, .8, 0, "Critical Hit", 0.8],
            _("Wool Boots"): ["feet", 0, 0, 5, 5, 0, 0, 0, "Health", 0.03],
            _("Leather Boots"): ["feet", 0, 5, 5, 5, 5, 0, 0, None, 0],
            _("Explorer Boots"): ["feet", 5, 5, 5, 5, 5, 0, 0, "Stamina", 0.05],
            _("Explorer Ring"): ["hands_left", 0, 0, 0, 0, 0, 0, 0, "Critical Hit", 0.15],
            _("Silk Fan"): ["hands_left", 0, 0, 0, 5, 0, 0, 0, "Stamina", 0.03],
            _("Buddha Beads"): ["head", 0, 0, 0, 0, 0, 0, 0, "Status", 0.1],
            _("Monk Robe"): ["body", 0, 0, 0, 5, 0, 0, 0, "Status", 0.2],
            _("Blood Diamond Pendant"): ["head", 0, 0, 0, 0, 0, 0, 0, "Health", 0.06],
            _("Amethyst Necklace"): ["head", 20, 0, 0, 0, 0, 0, 0, "Critical Hit", 0.1],
            _("Mask"): ["head", 0, 0, 0, 5, 0, 0, 0, None, 0],
            _("Helmet of Bishamonten"): ["head", 20, 20, 0, 20, 0, 0, 0, "Critical Hit", .2],
        }

        self.selling_price_dic = {
            _("Golden Gloves"): 3300,
            _("Bronze Gauntlets"): 1400,
            _("Golden Chainmail"): 4500,
            _("Steel Platelegs"): 2700,
            _("Steel Platebody"): 3500,
            _("Hardened Steel Platebody"): 5000,
            _("Light Armor"): 1500,
            _("Heavy Adamantite Sword"): 11000,
            _("Black Salamander"): 7500,
            _("Golden Snake"): 4800,
            _("Phoenix Blade"): 5400,
            _("Moonlight Blade"): 3300,
            _("Iron Dagger"): 100,
            _("Iron Sword"): 600,
            _("Steel Longsword"): 2000,
            _("Seven-inch Serpent Staff"): 15000,
            _("Wings of the Dragonfly"): 15000,
            _("Splinter Staff"): 15000,
            _("Lightning Saber"): 15000,
            _("Flame Blade"): 15000,
            _("Thunder Blade"): 15000,
            _("Ice Shard"): 15000,
            _("Ninja Suit"): 1000,
            _("Granite Boots"): 15000,
            _("Babao Blade"): 88888,
            _("Wool Boots"): 1000,
            _("Leather Boots"): 1000,
            _("Leather Body"): 1000,
            _("Leather Pants"): 700,
            _("Linen Shirt"): 5,
            _("Silk Fan"): 200,
            _("Buddha Beads"): 300,
            _("Monk Robe"): 0,
            _("Blood Diamond Pendant"): 6000,
            _("Amethyst Necklace"): 7500,
            _("Dragon Amulet"): 7500,
            _("Mask"): 0,
            _("Helmet of Bishamonten"): 13000,
            _("Venomous Viper Gloves"): 10000,
            _("Explorer Ring"): 0,
            _("Explorer Boots"): 0,

        }

        self.description_dic = {
            _("Golden Chainmail"): _("A light-weight chain mail formed from steel and gold mesh!\n\n"),
            _("Dragon Amulet"): _("One of the treasures of the Dragon Sect, the center of the amulet supposedly\ncontains a piece of molten steel produced from dragon fire.\n\n"),
            _("Light Armor"): _("A light platebody made from a thin layer of tin and bronze that offers basic protection.\n\n"),
            _("Heavy Adamantite Sword"): _("Forged from adamantite rock, this large blade may be\ndifficult to wield but can crush anything in its way.\n\n"),
            _("Black Salamander"): _("A thin, sharp blade forged from black iron.\n\n"),
            _("Golden Snake"): _("A curved sword that gives the wielder a distinct advantage\nin both attack and defence due to its shape.\n\n"),
            _("Phoenix Blade"): _("Forged from high-quality metals, this blade's light weight allows its wielder to attack quickly.\n\n"),
            _("Iron Dagger"): _("Slightly better than being bare-handed.\n\n"),
            _("Iron Sword"): _("A basic sword made of iron.\n\n"),
            _("Steel Longsword"): _("A longsword made from steel.\n\n"),
            _("Seven-inch Serpent Staff"): _("A highly toxic staff crafted in the shape of a serpant that, despite its short length, can deal a lethal blow.\n\n"),
            _("Wings of the Dragonfly"): _("A blade that is both light and easy to handle, allowing its user to perform rapid, consecutive strikes."),
            _("Splinter Staff"): _("A staff with many sharp arms protruding from its body..\n\nIn battle, reflects 25% of damage taken back to the opponent.\n\n"),
            _("Lightning Saber"): _("Quick as lightning.\n\n"),
            _("Flame Blade"): _("A legendary blade that burns to the touch and can partially melt normal metals upon contact.\n\n"),
            _("Thunder Blade"): _("As thunder is to the ears, so is this blade to the heart of anyone who opposes its wielder.\n\n"),
            _("Ice Shard"): _("A sharp dagger that pierces the bone as easily as a knife goes through butter.\n\n"),
            _("Moonlight Blade"): _("A sword with a glow like that of the moon.\n\n"),
            _("Golden Gloves"): _("Great protection for the hands!\n\n"),
            _("Bronze Gauntlets"): _("Gives some extra power to your punch.\n\n"),
            _("Venomous Viper Gloves"): _("Hard leather gloves layered with deadly viper venom.\n\n"),
            _("Ninja Suit"): _("It's much harder to be detected while wearing this.\nUseful for sneaking into places at night.\n\n"),
            _("Explorer Boots"): _("A pair of boots from Scrub as a gift for beta-testing. Beta-only item!\n\n"),
            _("Granite Boots"): _("Boots coated with a thick layer of granite, making it more difficult to move quickly but multiplies the force of each attack.\n\n"),
            _("Babao Blade"): _("A powerful blade imbued with 8 rare gems.\n\n"),
            _("Wool Boots"): _("Keeps my feet warm!\n\n"),
            _("Leather Boots"): _("Crafted from top-quality cow leather.\n\n"),
            _("Leather Body"): _("Crafted from top-quality cow leather.\n\n"),
            _("Leather Pants"): _("Crafted from top-quality cow leather.\n\n"),
            _("Linen Shirt"): _("A plain, linen shirt.\n\n"),
            _("Steel Platelegs"): _("Great protection for the legs!\n\n"),
            _("Steel Platebody"): _("A bit heavy, but shields your organs from devastating blows.\n\n"),
            _("Hardened Steel Platebody"): _("Solid platebody made of high carbon steel.\n\n"),
            _("Explorer Ring"): _("A ring from Scrub as a gift for beta-testing. Beta-only item!\n\n"),
            _("Silk Fan"): _("A delicate fan crafted from mulberry silk.\n\n"),
            _("Buddha Beads"): _("A set of beads made from high-quality opal.\n\n"),
            _("Monk Robe"): _("Worn by the monks of Shaolin.\n\n"),
            _("Blood Diamond Pendant"): _("A valuable pendant crafted using the rare blood diamond.\n\n"),
            _("Amethyst Necklace"): _("A rare, enchanted necklace and one of three treasures of Eagle Sect.\n\n"),
            _("Mask"): _("A creepy-looking mask. Maybe I can scare my opponents to death by wearing it.\n\n"),
            _("Helmet of Bishamonten"): _("Helmet of the Japanese War God.\n\n"),
        }

        self.name = name
        self.name_key = self.name.split("[")[0].strip()
        re_result = findall("\[([\s\S]+)\]", self.name)
        if re_result:
            self.grade = re_result[0]
        else:
            self.grade = "Standard"
        self.slot,self.datk,self.dstr,self.dspd,self.ddef,self.ddex,self.armed_dmg_bonus,self.unarmed_dmg_bonus,self.effect,self.effect_prob = self.attributes_dic[self.name_key]
        self.description = self.name + "\n-------------------------------------\n" + self.description_dic[self.name_key]
        self.selling_price = self.selling_price_dic[self.name_key]
        self.attribute_multiplier = 1
        self.price_multiplier = 1

        self.set_description()


    def set_description(self):

        if self.grade == "Ultra Rare":
            self.attribute_multiplier *= 2
            self.price_multiplier *= 2
        elif self.grade == "Rare":
            self.attribute_multiplier *= 1.5
            self.price_multiplier *= 1.5
        elif self.grade == "Enhanced":
            self.attribute_multiplier *= 1.25
            self.price_multiplier *= 1.25

        self.datk = int(self.datk*self.attribute_multiplier)
        self.dstr = int(self.dstr*self.attribute_multiplier)
        self.dspd = int(self.dspd*self.attribute_multiplier)
        self.ddef = int(self.ddef*self.attribute_multiplier)
        self.ddex = int(self.ddex*self.attribute_multiplier)
        self.selling_price = int(self.selling_price*self.price_multiplier)

        self.description += """Gives the following bonuses in battle:
Attack + {}
Strength + {}
Speed + {}
Defence + {}
Dexterity + {}
        """.format(self.datk, self.dstr, self.dspd, self.ddef, self.ddex).replace("+ -", "-")

        if self.armed_dmg_bonus > 0:
            self.description += """
+{}% damage bonus for armed attacks.
            """.format(self.armed_dmg_bonus*100)

        if self.unarmed_dmg_bonus > 0:
            self.description += """
+{}% damage bonus for unarmed attacks.
            """.format(self.unarmed_dmg_bonus*100)


        if self.slot == _("hands_right"):
            attack_type = _("Armed")
        else:
            attack_type = _("Unarmed")

        if self.effect in [_("Poison"), _("Internal Injury"), _("Seal"), _("Blind")]:
            self.description += """
+{}% chance of inflicting {} to opponent on hit ({} Attacks).
""".format(self.effect_prob*100, self.effect, attack_type)

        elif self.effect == _("Disable"):
            self.description += """
+{}% chance of disabling one of the opponent's moves on hit ({} Attacks).
""".format(self.effect_prob * 100, attack_type)

#        elif self.effect == _("Absorb"):
#            self.description += """
#{}% of damage dealt to opponent is added to own health and stamina ({} Attacks).
#""".format(self.effect_prob * 100, attack_type)

#        elif self.effect == _("Reflect"):
#            self.description += """
#{}% of damage taken from opponent's attack is reflected back to opponent.
#""".format(self.effect_prob * 100, attack_type)

        elif self.effect in [_("Health"), _("Stamina")]:
            self.description += """
Recovers an additional {}% {} when resting.
""".format(self.effect_prob * 100, self.effect)

        elif self.effect == _("Status"):
            self.description += """
Reduces the chance of being inflicted with negative status by {}%.
""".format(self.effect_prob * 100)

        elif self.effect == _("Critical Hit"):
            self.description += """
+{}% chance of Critical Hit.
""".format(self.effect_prob * 100)


        if self.selling_price > 0:
            self.description += """\n\n
Selling price: {}
""".format(self.selling_price)