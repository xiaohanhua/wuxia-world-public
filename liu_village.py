from tkinter import*
from character import*
from item import*
from random import*
from time import sleep
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
import tkinter.messagebox as messagebox


_ = gettext

class Scene_liu_village:

    def __init__(self, game):
        self.game = game
    
        if self.game.taskProgressDic["liu_village"] in [11, 21]:

            self.game.generate_dialogue_sequence(
                None,
                "liu_village.ppm",
                [_("Dang it, why can we still not find it? We've looked everywhere!"),
                 _("(Hmmm... what are these masked men doing here?)"),
                 _("(Could they be related to the village massacre?)"),
                 _("(Maybe I ought to capture one of them and ask...)"),
                 _("(But am I strong enough to take on 3 of them?)"),
                 ],
                ["Masked Assassin", "you", "you", "you", "you"],
                [[_("Fight!"), partial(self.liu_village_fight_masked_assassins, 1)],
                 [_("Keep observing"), partial(self.liu_village_fight_masked_assassins, 2)]]
            )


        else:
            self.game.liuVillageWin = Toplevel(self.game.mapWin)
            self.game.liuVillageWin.title(_("Liu Village"))

            bg = PhotoImage(file="liu_village.ppm")
            w = bg.width()
            h = bg.height()

            canvas = Canvas(self.game.liuVillageWin, width=w, height=h)
            canvas.pack()
            canvas.create_image(0, 0, anchor=NW, image=bg)

            self.game.liuVillageF1 = Frame(canvas)
            pic1 = PhotoImage(file="liu_village_house_icon.png")
            imageButton1 = Button(self.game.liuVillageF1, command=self.go_liu_village_house)
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            self.game.liuVillageF1.place(x=w // 4 - pic1.width() // 2, y=h // 4)


            self.game.liuVillageF2 = Frame(canvas)
            pic2 = PhotoImage(file="liu_village_lake_icon.png")
            imageButton2 = Button(self.game.liuVillageF2, command=self.go_liu_village_lake)
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            self.game.liuVillageF2.place(x=w * 3// 4 - pic2.width() // 2, y=h // 4)


            menu_frame = self.game.create_menu_frame(self.game.liuVillageWin)
            menu_frame.pack()

            self.game.liuVillageWin.protocol("WM_DELETE_WINDOW", self.game.on_exit)
            self.game.liuVillageWin.update_idletasks()
            toplevel_w, toplevel_h = self.game.liuVillageWin.winfo_width(), self.game.liuVillageWin.winfo_height()
            self.game.liuVillageWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
            self.game.liuVillageWin.focus_force()
            self.game.liuVillageWin.mainloop()###

    def liu_village_fight_masked_assassins(self, choice):

        self.game.dialogueWin.destroy()

        if choice == 1:
            self.game.chivalry += 15
            messagebox.showinfo("", "Your chivalry + 15!")
            self.game.generate_dialogue_sequence(
                None,
                "liu_village.ppm",
                [_("Hey! What are you guys looking for? Need some help?")],
                ["you"]
            )

        else:
            self.game.generate_dialogue_sequence(
                None,
                "liu_village.ppm",
                [_("(What can possible be so valuable that they'd kill an entire village and look for it in this cold weather?)"),
                 _("(Man, I'm about to freeze to death just sitting here...)"),
                 _("(If nothing interesting happens soon, I think I'll ---)"),],
                ["you", "you", "you"]
            )
            self.game.currentEffect = "sneeze.mp3"
            self.game.startSoundEffectThread()
            sleep(1)

            self.game.generate_dialogue_sequence(
                None,
                "liu_village.ppm",
                [_("Hey! Who's that?!"),
                 _("Oh hey there! You guys er... enjoying the full moon tonight? ......")],
                ["Masked Assassin", "you"]
            )


        opps = []
        opp = character(_("Masked Assassin"), 8,
                        sml=[special_move(_("Smoke Bomb"), level=5),
                             special_move(_("Basic Sword Technique"), level=10)])
        opps.append(opp)
        opp = character(_("Masked Assassin"), 9,
                        sml=[special_move(_("Smoke Bomb"), level=5),
                             special_move(_("Basic Sword Technique"), level=10)])
        opps.append(opp)
        opp = character(_("Masked Assassin"), 10,
                        sml=[special_move(_("Smoke Bomb"), level=5),
                             special_move(_("Spider Poison Powder")),
                             special_move(_("Basic Sword Technique"), level=10)])
        opps.append(opp)

        self.game.battleMenu(self.game.you, opps, "battleground_liu_village.png",
                        postBattleSoundtrack="jy_suspenseful2.mp3",
                        fromWin=None,
                        battleType="task", destinationWinList=[] * 3,
                        postBattleCmd=self.post_liu_village_fight_masked_assassins
                        )


    def post_liu_village_fight_masked_assassins(self):

        self.game.generate_dialogue_sequence(
            None,
            "liu_village.ppm",
            [_("... This guy's too strong; let's come back later!"),
             _("Hold it right th--- ")],
            ["Masked Assassin", "you"]
        )

        self.game.currentEffect = "sfx_smoke_bomb.mp3"
        self.game.startSoundEffectThread()

        self.game.generate_dialogue_sequence(
            None,
            "liu_village.ppm",
            [_("Ah, dang it! Stupid smoke bombs!"),
             _(".... Wait, one of them dropped something..."),
             _("Hmmmm, what is this....?"),],
            ["you", "you", "you"]
        )

        self.game.inv[_("Smoke Bomb")] = 3
        messagebox.showinfo("", _("Found 'Smoke Bomb' x 3!"))

        if self.game.taskProgressDic["liu_village"] == 11:
            self.game.taskProgressDic["liu_village"] = 12
        elif self.game.taskProgressDic["liu_village"] == 21:
            self.game.taskProgressDic["liu_village"] = 12

        self.__init__(self.game)


    def go_liu_village_house(self, choice=0):

        self.game.liuVillageWin.withdraw()
        if self.game.taskProgressDic["li_guiping_task"] in [6,7] and random() <= .75:
            self.game.taskProgressDic["li_guiping_task"] += 1

        if self.game.taskProgressDic[_("li_guiying")] == -1:
            if choice == 0:
                self.game.generate_dialogue_sequence(
                    None,
                    "liu_village_house.png",
                    [_("You wicked woman! What did you do to this village?"),
                     _("And who the heck are you? Go mind your own business..."),
                     _("Hah! I enjoy getting into other people's business. Got a problem?"),
                     _("I, the Blind Sniper, am known for picking random fights with people."),
                     _("Since I happened to come across you practicing malicious martial arts on innocent people, I'm going to bring justice to these poor victims."),
                     _("Say what you will, but the village massacre had nothing to do with me."),
                     _("I saw those masked men in black come into the village 2 days ago. After killing everyone, they began searching for something..."),
                     _("I was busy practicing and did not want to be interrupted, so I hid myself."),
                     _("Sure, I've had to capture a few villagers to practice on in the past 2 months, but there's no need for me to kill an entire village."),
                     _("In fact, it'd be a waste of training material to kill them all at once..."),
                     _("You villainous witch! Don't think you can escape punishment for your wicked deeds."),
                     _("Today, you will die for all the innocent lives you've taken!"),],
                    ["Blind Sniper", "Li Guiying", "Blind Sniper", "Blind Sniper", "Blind Sniper", "Li Guiying",
                     "Li Guiying", "Li Guiying", "Li Guiying", "Li Guiying", "Blind Sniper", "Blind Sniper"]
                )

                if self.game.taskProgressDic["join_sect"] == 401:
                    self.game.generate_dialogue_sequence(
                        None,
                        "liu_village_house.png",
                        [_("Sister!"),
                         _("Guiping? What are you doing here?"),
                         _("Long story... This is my husband, {}.").format(self.game.character),
                         _("{}, we have to help my sister...").format(self.game.character),
                         _("Of course... I won't let this guy harm either of you..."),
                         _("Enough talk! I'll chop down anyone who stands in my way of killing this witch today!"), ],
                        ["Li Guiping", "Li Guiying", "Li Guiping", "Li Guiping", "you", "Blind Sniper"]
                    )

                    self.game.taskProgressDic[_("li_guiying")] = 110

                    opp = character(_("Blind Sniper"), 15,
                                    sml=[special_move(_("Basic Sword Technique"), level=10),
                                         special_move(_("Taichi Sword"), level=8),
                                         special_move(_("Blind Sniper Blade"), level=10)],
                                    skills=[skill(_("Wudang Agility Technique"), level=9),
                                            skill(_("Pure Yang Qi Skill"), level=9)])

                    cmd = self.go_liu_village_house
                    self.game.battleID = "blind_sniper_battle"
                    self.game.battleMenu(self.game.you, opp, "battleground_liu_village_house.png",
                                    postBattleSoundtrack="jy_suspenseful2.mp3",
                                    fromWin=None,
                                    battleType="task", destinationWinList=[None] * 3,
                                    postBattleCmd=cmd
                                    )

                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "liu_village_house.png",
                        [_("(Interesting... wonder whom I should help in this situation...)")],
                        ["you"],
                        [[_("Help the woman in black."), partial(self.go_liu_village_house, 1)],
                         [_("Help the Blind Sniper."), partial(self.go_liu_village_house, 2)],
                         [_("Watch and observe..."), partial(self.go_liu_village_house, 3)],]
                    )

            elif choice == 1:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.taskProgressDic[_("li_guiying")] = 100
                self.game.chivalry -= 20
                messagebox.showinfo("", _("Your chivalry -20."))

                self.game.generate_dialogue_sequence(
                    None,
                    "liu_village_house.png",
                    [_("Hold on there, pal, what are you doing being so aggressive to a lady?"),
                     _("A lady? You mean a cold-blooded murderer? Now step out of the way before I mistaken you for an accomplice..."),
                     _("I don't think so; you aren't touching a hair on her head until you get past me."),
                     _("Fool... You asked for this...")],
                    ["you", "Blind Sniper", "you", "Blind Sniper"],
                )

                opp = character(_("Blind Sniper"), 15,
                                sml=[special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Taichi Sword"), level=8),
                                     special_move(_("Blind Sniper Blade"), level=10)],
                                skills=[skill(_("Wudang Agility Technique"), level=9),
                                        skill(_("Pure Yang Qi Skill"), level=9)])

                cmd = self.go_liu_village_house
                self.game.battleID = "blind_sniper_battle"
                self.game.battleMenu(self.game.you, opp, "battleground_liu_village_house.png",
                                postBattleSoundtrack="jy_suspenseful2.mp3",
                                fromWin=None,
                                battleType="task", destinationWinList=[None] * 3,
                                postBattleCmd=cmd
                                )


            elif choice == 2:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.taskProgressDic[_("li_guiying")] = 200
                self.game.chivalry += 20
                messagebox.showinfo("", _("Your chivalry +20."))

                self.game.generate_dialogue_sequence(
                    None,
                    "liu_village_house.png",
                    [_("Hold on, Blind Sniper, there's no need for you to waste time and energy on her."),
                     _("I'll take care of her for you!"),
                     _("Another fool looking for death... bring it...")],
                    ["you", "you", "Li Guiying"],
                )

                opp = character(_("Li Guiying"), 14,
                                sml=[special_move(_("Poison Needle"), level=10),
                                     special_move(_("Phantom Claws"), level=9),
                                     special_move(_("Five Poison Palm"), level=7)],
                                skills=[skill(_("Phantom Steps"), level=10)])

                cmd = self.go_liu_village_house
                self.game.battleID = "li_guiying_battle"
                self.game.battleMenu(self.game.you, opp, "battleground_liu_village_house.png",
                                postBattleSoundtrack="jy_suspenseful2.mp3",
                                fromWin=None,
                                battleType="task", destinationWinList=[None] * 3,
                                postBattleCmd=cmd
                                )

            elif choice == 3:
                self.game.taskProgressDic[_("li_guiying")] = 300
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()

                self.game.currentEffect = "sfx_male_voice.mp3"
                self.game.startSoundEffectThread()
                sleep(.5)
                self.game.currentEffect = "sfx_blind_sniper_blade.mp3"
                self.game.startSoundEffectThread()
                sleep(.5)
                self.game.currentEffect = "jy_nv3.mp3"
                self.game.startSoundEffectThread()
                sleep(.5)
                self.game.currentEffect = "sfx_basic_sword_technique.mp3"
                self.game.startSoundEffectThread()

                self.game.generate_dialogue_sequence(
                    None,
                    "liu_village_house.png",
                    [_("AHHH... I don't have time for this..."),
                     _("Hey! Where are you going? You think you can escape from me?"),
                     _("..............."),
                     _("(Hmmm, the lady dropped something while running away...)")],
                    ["Li Guiying", "Blind Sniper", "you", "you"]
                )

                if self.game.luck >= 90:
                    messagebox.showinfo("", _("Found: Five Poison Palm Manual x 1!"))
                    self.game.inv[_("Five Poison Palm Manual")] = 1
                else:
                    messagebox.showinfo("", _("Found: Poison Needle Manual x 1!"))
                    self.game.inv[_("Poison Needle Manual")] = 1
                self.go_liu_village_house()

        elif self.game.taskProgressDic[_("li_guiying")] == 100:
            self.game.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("Not bad... I underestimated you..."),
                 _("If it were not for this random guy you would be dead, witch..."),
                 _("This is not over. I will be back..."),
                 _("Hah! Piece of cake..."),
                 _("Thanks... I owe you one."),
                 _("So how do you plan on repaying me?"),
                 _("..."),
                 _("How about this... if you help me achieve Level 10 of the Five Poison Palm, I will teach you the technique."),
                 _("Ohhhhh! So is that what you've been practicing on the villagers?"),
                 _("Precisely..."),
                 _("Sounds like a deal; what do you need help with?"),
                 _("The Five Poison Palm is so named because to practice it, one must first absorb the poison from 5 different types of Poisonous Moths."),
                 _("Unfortunately, these moths are not easy to catch, so perhaps you can save me some time by catching them for me."),
                 _("No problem! Where can I find these moths?"),
                 _("You can go look in the nearby forest. I'll need 5 of each type, so 25 moths total."),
                 _("Got it!")],
                ["Blind Sniper", "Blind Sniper", "Blind Sniper", "you", "Li Guiying", "you",
                 "Li Guiying", "Li Guiying", "you", "Li Guiying", "you", "Li Guiying",
                 "Li Guiying", "you", "Li Guiying", "you"]
            )
            self.game.taskProgressDic[_("li_guiying")] = 101
            self.go_liu_village_house()


        elif self.game.taskProgressDic[_("li_guiying")] == 110:
            self.game.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("Not bad... I underestimated you..."),
                 _("If it were not for this random guy you would be dead, witch..."),
                 _("This is not over. I will be back..."),
                 _("Hah! Piece of cake..."),
                 _("Thanks, brother-in-law?"),
                 _("Thank you for saving my sister, {}!").format(self.game.character),
                 _("Sister, can you teach {} something so that he can better protect us in the future?").format(self.game.character),
                 _("Sure, how about this... Once I achieve Level 10 of the Five Poison Palm, I will teach him the technique."),
                 _("Ohhhhh! So is that what you've been practicing on the villagers?"),
                 _("Precisely..."),
                 _("Sounds like a deal; what do you need help with?"),
                 _("The Five Poison Palm is so named because to practice it, one must first absorb the poison from 5 different types of Poisonous Moths."),
                 _("Unfortunately, these moths are not easy to catch, so perhaps you can save me some time by catching them for me."),
                 _("No problem! Where can I find these moths?"),
                 _("You can go look in the nearby forest. I'll need 5 of each type, so 25 moths total."),
                 _("Got it!")],
                ["Blind Sniper", "Blind Sniper", "Blind Sniper", "you", "Li Guiying", "Li Guiping",
                 "Li Guiping", "Li Guiying", "you", "Li Guiying", "you", "Li Guiying",
                 "Li Guiying", "you", "Li Guiying", "you"]
            )
            self.game.taskProgressDic[_("li_guiying")] = 111
            self.go_liu_village_house()


        elif self.game.taskProgressDic[_("li_guiying")] == 200:
            self.game.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("Two grown men teaming up against a woman... how shameful..."),
                 _("And you preying on innocent people who don't know any martial arts? What should we call that?"),
                 _("I lost... it is useless to say anything... Do as you wish..."),
                 _("Today, the souls of Liu Village are avenged. Die, you evil witch!"),
                 ],
                ["Li Guiying", "Blind Sniper", "Li Guiying", "Blind Sniper"]
            )
            self.game.currentEffect = "sfx_female_grunt.m4a"
            self.game.startSoundEffectThread()
            self.game.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("AHHHHHHH!!!"),
                 _("You did an honorable deed today. What is your name?"),
                 _("My name is {}. Pleasure to meet you.").format(self.game.character),
                 _("The honor is mine, {}.").format(self.game.character),
                 _("As a token of appreciation, I will teach you something that I came up with on my own: the Blind Sniper Blade!"),
                 _("Thanks!!!"),
                 _("Don't mention it, my friend... Take care!")],
                ["Li Guiying", "Blind Sniper", "you", "Blind Sniper",
                 "Blind Sniper", "you", "Blind Sniper"]
            )

            new_move = special_move(name=_("Blind Sniper Blade"))
            self.game.learn_move(new_move)
            self.game.taskProgressDic[_("li_guiying")] = 201
            messagebox.showinfo("", _("You learned a new move: Blind Sniper Blade!"))
            self.go_liu_village_house()


        elif self.game.taskProgressDic[_("li_guiying")] in [101, 111] and random() <= .33:
            self.game.taskProgressDic[_("li_guiying")] += 1
            if self.game.taskProgressDic["join_sect"] == 600:
                self.game.generate_dialogue_sequence(
                    None,
                    "liu_village_house.png",
                    [_("There he is! He's the one who helped that witch."),
                     _("Scoundrel, you are a disgrace to Wudang!"),
                     _("I see you've found back up, Blind Sniper..."),
                     _("You're going down, villain!"),],
                    ["Blind Sniper", "Li Kefan", "you", "Blind Sniper"]
                )
            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "liu_village_house.png",
                    [_("There he is! He's the one who helped that witch."),
                     _("Scoundrel, you dared to return! I, one of the Warriors of Wudang, will execute justice today!"),
                     _("I see you've found back up, Blind Sniper..."),
                     _("You're going down, villain!"),],
                    ["Blind Sniper", "Li Kefan", "you", "Blind Sniper"]
                )

            opp_list = []
            opp = character(_("Blind Sniper"), 18,
                            sml=[special_move(_("Basic Sword Technique"), level=10),
                                 special_move(_("Taichi Sword"), level=8),
                                 special_move(_("Blind Sniper Blade"), level=10)],
                            skills=[skill(_("Wudang Agility Technique"), level=9),
                                    skill(_("Pure Yang Qi Skill"), level=9)])
            opp_list.append(opp)
            opp = character(_("Li Kefan"), 18,
                            sml=[special_move(_("Taichi Fist"), level=9),
                                 special_move(_("Basic Sword Technique"), level=10),
                                 special_move(_("Taichi Sword"), level=8)],
                            skills=[skill(_("Wudang Agility Technique"), level=8),
                                    skill(_("Pure Yang Qi Skill"), level=8)])
            opp_list.append(opp)
            cmd = self.go_liu_village_house
            self.game.battleMenu(self.game.you, opp_list, "battleground_liu_village_house.png",
                                 postBattleSoundtrack="jy_suspenseful2.mp3",
                                 fromWin=None,battleType="task", destinationWinList=[None] * 3, postBattleCmd=cmd
                                 )


        elif self.game.taskProgressDic[_("li_guiying")] in [102, 112]:
            self.game.taskProgressDic[_("li_guiying")] += 1
            if self.game.taskProgressDic["join_sect"] == 600:
                self.game.generate_dialogue_sequence(
                    None,
                    "liu_village_house.png",
                    [_("One day, your evil acts will catch up to you."),
                     _("If you dare to set foot in Wudang again, I'll cut you to pieces!"),
                     _("Hahahahaha! Losers... Get out of here..."),],
                    ["Blind Sniper", "Li Kefan", "you"]
                )
            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "liu_village_house.png",
                    [_("One day, your evil acts will catch up to you."),
                     _("Don't let me see you again... I will cut you to pieces..."),
                     _("Hahahahaha! Sure, sure, whatever you say... Now, get out of here..."),],
                    ["Blind Sniper", "Li Kefan", "you"]
                )

            self.go_liu_village_house()


        else:
            self.game.liuVillageHouseWin = Toplevel(self.game.liuVillageWin)
            self.game.liuVillageHouseWin.title(_("Liu Village"))

            bg = PhotoImage(file="liu_village_house.png")
            w = bg.width()
            h = bg.height()

            canvas = Canvas(self.game.liuVillageHouseWin, width=w, height=h)
            canvas.pack()
            canvas.create_image(0, 0, anchor=NW, image=bg)

            if self.game.taskProgressDic[_("li_guiying")] in [101,102,103,111,112,113] or (self.game.taskProgressDic["li_guiying"] == 300 and self.game.taskProgressDic["li_guiping_task"] >= 5):
                self.game.liuVillageHouseF1 = Frame(canvas)
                pic1 = PhotoImage(file="Li Guiying_icon_large.ppm")
                imageButton1 = Button(self.game.liuVillageHouseF1, command=self.clickedOnLiGuiying)
                imageButton1.grid(row=0, column=0)
                imageButton1.config(image=pic1)
                label = Label(self.game.liuVillageHouseF1, text=_("Li Guiying"))
                label.grid(row=1, column=0)
                self.game.liuVillageHouseF1.place(x=w // 4 - pic1.width() // 2, y=h // 4 - pic1.height() // 2)

            elif self.game.taskProgressDic[_("li_guiying")] == 201 and self.game.taskProgressDic["blind_sniper"] != 1000001:
                self.game.liuVillageHouseF2 = Frame(canvas)
                pic2 = PhotoImage(file="Blind Sniper_icon_large.ppm")
                imageButton2 = Button(self.game.liuVillageHouseF2, command=self.clickedOnBlindSniper)
                imageButton2.grid(row=0, column=0)
                imageButton2.config(image=pic2)
                label = Label(self.game.liuVillageHouseF2, text=_("Blind Sniper"))
                label.grid(row=1, column=0)
                self.game.liuVillageHouseF2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 4 - pic2.height() // 2)

            self.game.liuVillageHouseWinF1 = Frame(self.game.liuVillageHouseWin)
            self.game.liuVillageHouseWinF1.pack()

            menu_frame = self.game.create_menu_frame(self.game.liuVillageHouseWin)
            menu_frame.pack()

            self.game.liuVillageHouseWin.protocol("WM_DELETE_WINDOW", self.game.on_exit)
            self.game.liuVillageHouseWin.update_idletasks()
            toplevel_w, toplevel_h = self.game.liuVillageHouseWin.winfo_width(), self.game.liuVillageHouseWin.winfo_height()
            self.game.liuVillageHouseWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
            self.game.liuVillageHouseWin.focus_force()
            self.game.liuVillageHouseWin.mainloop()###


    def go_liu_village_lake(self):

        self.game.liuVillageWin.withdraw()
        self.game.stopSoundtrack()
        self.game.currentBGM = "jy_ambient.mp3"
        self.game.startSoundtrackThread()

        self.game.liuVillageLakeWin = Toplevel(self.game.liuVillageWin)
        self.game.liuVillageLakeWin.title(_("Lake"))

        bg = PhotoImage(file="Lake.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.liuVillageLakeWin, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        if True:
            self.game.liuVillageLakeF1 = Frame(canvas)
            pic1 = PhotoImage(file="Cheng Youde_icon_large.ppm")
            imageButton1 = Button(self.game.liuVillageLakeF1, command=self.clickedOnChengYoude)
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            label = Label(self.game.liuVillageLakeF1, text=_("Cheng Youde"))
            label.grid(row=1, column=0)
            self.game.liuVillageLakeF1.place(x=w // 4 - pic1.width() // 2, y=h // 4 - pic1.height() // 2)


        self.game.liuVillageLakeWinF1 = Frame(self.game.liuVillageLakeWin)
        self.game.liuVillageLakeWinF1.pack()

        menu_frame = self.game.create_menu_frame(self.game.liuVillageLakeWin)
        menu_frame.pack()

        self.game.liuVillageLakeWin.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.liuVillageLakeWin.update_idletasks()
        toplevel_w, toplevel_h = self.game.liuVillageLakeWin.winfo_width(), self.game.liuVillageLakeWin.winfo_height()
        self.game.liuVillageLakeWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.liuVillageLakeWin.focus_force()
        self.game.liuVillageLakeWin.mainloop()###


    def clickedOnChengYoude(self):

        if self.game.taskProgressDic["supreme_leader"] == 1 and self.game.chivalry <= 0:
            messagebox.showinfo("", _("Cheng Youde does not seem interested in talking to you..."))

        elif self.game.taskProgressDic["liu_village"] == 12 and (self.game.taskProgressDic["li_guiying"] in [101,102,103,111,112,113,300] or self.game.taskProgressDic["blind_sniper"] == 1000001):
            self.game.generate_dialogue_sequence(
                self.game.liuVillageLakeWin,
                "Lake.png",
                [_("Hey! Got a question for ya..."),
                 _("Hmmm?"),
                 _("I came upon some masked men searching for something; I managed to defeat them and one of them dropped these..."),
                 _("Smoke bombs... what a dishonorable weapon..."),
                 _("From the looks of it, Phantom Sect is probably involved."),
                 _("Lu Xifeng, the Phantom Sect leader, is known for his cruel and dishonorable techniques."),
                 _("So you're saying Phantom Sect is behind all of this?"),
                 _("That's what I suspect, yes..."),
                 _("Any idea what they're looking for? Or why they killed an entire village?"),
                 _("Nearly 4 decades ago, Liu Zhiyuan, the head of the Liu village, defeated Zhao Jianguo, one of the most feared martial artists at the time, in a duel."),
                 _("Zhao Jianguo was slain in the duel, and Liu Zhiyuan obtained the manual for the Devil Crescent Moon Blade, Zhao's specialty."),
                 _("The Devil Crescent Moon Blade is said to be extremely powerful, too powerful, in fact, that whoever practices it seems to lose control over their bodies."),
                 _("To practice the technique, one must commit some unspeakable acts."),
                 _("Furthermore, the more one's heart is bent on doing evil, the more powerful the technique becomes."),
                 _("Wow... Did Liu Zhiyuan practice it?"),
                 _("No, Liu was far too chivalrous to do that. He wouldn't have been able to unlock the technique's full potential anyway."),
                 _("If this devil blade thing really is so powerful, how did Zhao lose to Liu?"),
                 _("A few months before his duel with Liu at Huashan, Zhao met a woman named Song Meixin."),
                 _("He fell in love with her, and since then, he began to change..."),
                 _("Her gentleness and compassion softened his heart, and he turned from his evil ways."),
                 _("Consequently, he was no longer able to use his special technique at its maximum potential."),
                 _("After Zhao's death, Song Meixin gave birth to a son. They lived a quiet life near Huashan and not much is known about them besides that."),
                 _("What became of Liu, then?"),
                 _("Even though Liu never learned the Devil Crescent Moon Blade, he made one mistake..."),
                 _("As a martial arts fanatic, he could not bring himself to destroy such a powerful manual."),
                 _("So he kept it... but this soon attracted many unnecessary troubles..."),
                 _("All the bad guys started going after it?"),
                 _("Yep... but until recently, no one's succeeded. Just last year, Liu Zhiyuan passed away from old age."),
                 _("This created the prime opportunity for power-hungry martial artists."),
                 _("People like Lu Xifeng?"),
                 _("Exactly... Lu is already a tough opponent... I fear if he gets his hands on the manual..."),
                 _("Well, the fact that those masked men came back looking for something means they haven't found it yet, right?"),
                 _("I sure hope so..."),
                 _("Alright, thanks, I'll keep investigating then...")],
                ["you", "Cheng Youde", "you", "Cheng Youde", "Cheng Youde", "Cheng Youde",
                 "you", "Cheng Youde", "you", "Cheng Youde", "Cheng Youde", "Cheng Youde",
                 "Cheng Youde", "Cheng Youde", "you", "Cheng Youde", "you", "Cheng Youde",
                 "Cheng Youde", "Cheng Youde", "Cheng Youde", "Cheng Youde", "you",
                 "Cheng Youde", "Cheng Youde", "Cheng Youde", "you", "Cheng Youde", "Cheng Youde",
                 "you", "Cheng Youde", "you", "Cheng Youde", "you"
                 ]
            )
            self.game.taskProgressDic["liu_village"] = 13

        else:
            self.game.currentEffect = "button-20.mp3"
            self.game.startSoundEffectThread()
            for widget in self.game.liuVillageLakeWinF1.winfo_children():
                widget.destroy()

            Button(self.game.liuVillageLakeWinF1, text=_("Spar"),
                   command=partial(self.interactWithChengYoude, 1)).grid(row=1, column=0)
            Button(self.game.liuVillageLakeWinF1, text=_("Who are you?"),
                   command=partial(self.interactWithChengYoude, 2)).grid(row=2, column=0)
            Button(self.game.liuVillageLakeWinF1, text=_("What are you doing here?"),
                   command=partial(self.interactWithChengYoude, 3)).grid(row=3, column=0)
            Button(self.game.liuVillageLakeWinF1, text=_("Fish with Cheng Youde"),
                   command=partial(self.interactWithChengYoude, 4)).grid(row=4, column=0)


    def interactWithChengYoude(self, choice):
        if choice == 1:
            if self.game.chivalry < 0:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageLakeWin,
                    "Lake.png",
                    [_("There is something more urgent at hand that needs to be addressed..."),
                     _("Young man, I sense there is darkness in your heart... "),
                     _("If not purged, I fear it may lead to your demise..."),
                     _("But there is hope yet... If you are willing, I can perhaps help you get rid of the evil in your heart..."),
                     _("It will come with a cost, though... Your relentless pursuit of power and fame has led you to make some poor decisions."),
                     _("Though you've grown stronger, so have your sinful desires."),
                     _("I can help you to cleanse your heart by removing some of the dark energy that resides in you."),
                     _("This is only a temporary solution, however, as it does not guarantee you will not make poor choices in the future."),
                     _("Are you willing to try it?")],
                    ["Cheng Youde", "Cheng Youde", "Cheng Youde", "Cheng Youde", "Cheng Youde", "Cheng Youde",
                     "Cheng Youde", "Cheng Youde", "Cheng Youde"]
                )
                response = messagebox.askquestion("", _("Allow Cheng Youde to help you cleanse your heart? (Reduces your stamina upper limit by half; resets your chivalry to 0.)"))
                if response == "yes":
                    self.game.chivalry = 0
                    self.game.staminaMax = self.game.staminaMax//2
                    self.game.stamina = self.game.staminaMax
                    messagebox.showinfo("", _("Cheng Youde helped you to clear the malicious thoughts in your head."))


            elif self.game.chivalry >= 50:
                self.game.liuVillageLakeWin.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Lake.png",
                    [_("It is not often that I meet a chivalrous man like yourself."),
                     _("Very well, I humbly accept.")],
                    ["Cheng Youde", "Cheng Youde"]
                )
                year = calculate_month_day_year(self.game.gameDate)["Year"]
                opp = character(_("Cheng Youde"), min([16+year*1, 20]),
                                sml=[special_move(_("Purity Blade"), level=10),
                                     special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Taichi Sword"), level=10)],
                                skills=[skill(_("Wudang Agility Technique"), level=10),
                                        skill(_("Pure Yang Qi Skill"), level=8)])

                self.game.battleMenu(self.game.you, opp, "battleground_lake.png",
                                postBattleSoundtrack="jy_ambient.mp3",
                                fromWin=self.game.liuVillageLakeWin,
                                battleType="training", destinationWinList=[self.game.liuVillageLakeWin] * 3,
                                postBattleCmd = self.chengYoudeBattleReward
                                )


            else:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageLakeWin,
                    "Lake.png",
                    [_("Vanity of vanities! All is vanity!"),
                     _("What does man gain by all the toil at which he toils under the sun?"),
                     _("Alas, friend, it is vain to spar... Why, then, should we do it?")],
                    ["Cheng Youde", "Cheng Youde", "Cheng Youde"]
                )


        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageLakeWin,
                "Lake.png",
                [_("I am but a sojourner... a pilgrim who wanders this land for a short time..."),
                 _("Like a mist that appears for a little time and then vanishes..."),],
                ["Cheng Youde", "Cheng Youde"]
            )

        elif choice == 3:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageLakeWin,
                "Lake.png",
                [_("As you can see, I am fishing."),
                 _("What are you trying to catch? Carp? Cat fish?"),
                 _("People... I am fishing for people..."),
                 _("........"),
                 _("You can join me if you'd like.")],
                ["Cheng Youde", "you", "Cheng Youde", "you", "Cheng Youde"]
            )

        elif choice == 4:

            dialogues_list = [[_("All things are full of weariness; a man cannot utter it;"),
                               _("the eye is not satisfied with seeing, nor the ear filled with hearing."),
                               _("What has been is what will be, and what has been done is what will be done,"),
                               _("and there is nothing new under the sun.")],
                              [_("He who loves money will not be satisfied with money,"),
                               _("nor he who loves wealth with his income; this also is vanity.")],
                              [_("Again I saw that under the sun the race is not to the swift, nor the battle to the strong,"),
                               _("nor bread to the wise, nor riches to the intelligent, nor favor to those with knowledge,"),
                               _("but time and chance happen to them all. For man does not know his time."),
                               _("Like fish that are taken in an evil net, and like birds that are caught in a snare,"),
                               _("so the children of man are snared at an evil time, when it suddenly falls upon them.")],
                              [_("Walk in the ways of your heart and the sight of your eyes."),
                               _("But know that for all these things God will bring you into judgment.")],
                              [_("For everything there is a season, and a time for every matter under heaven:"),
                               _("a time to be born, and a time to die; a time to plant, and a time to pluck up what is planted;"),
                               _("a time to kill, and a time to heal; a time to break down, and a time to build up;"),
                               _("a time to weep, and a time to laugh; a time to mourn, and a time to dance...")]]

            speakers_list = [["Cheng Youde"]*4,
                             ["Cheng Youde"]*2,
                             ["Cheng Youde"]*5,
                             ["Cheng Youde"]*2,
                             ["Cheng Youde"]*4]

            r = randrange(len(dialogues_list))
            self.game.generate_dialogue_sequence(
                self.game.liuVillageLakeWin,
                "Lake.png",
                [_("You sit down to fish with Cheng Youde...")] + dialogues_list[r],
                ["Blank"] + speakers_list[r]
            )

            if (self.game.perception < 85 + 10*self.game.ptc or (self.game.perception < 110 + 10*self.game.ptc and self.game.taskProgressDic["join_sect"] == 800)) and random() <= .3:
                self.game.perception += 1
                messagebox.showinfo("", _("You manage to make sense of some of what he's saying...\nYour perception +1!"))


    def chengYoudeBattleReward(self):

        if _("Purity Blade") not in [m.name for m in self.game.sml] and (self.game.chivalry >= 100 or self.game.luck >= 80):
            self.game.generate_dialogue_sequence(
                self.game.liuVillageLakeWin,
                "Lake.png",
                [_("How refreshing! I haven't had this much fun in a long time!"),
                 _("Friend, not only is your character admirable, but your skill in martial arts is also deserving of praise!"),
                 _("Why, I am humbled by such encouraging words..."),
                 _("The technique you were using is quite unique. I am fairly impressed by it!"),
                 _("Ah, you are talking about the Purity Blade? It is a technique that I invented."),
                 _("Only those with a pure heart are able to learn and execute it with power."),
                 _("Since you are here today, why don't I go ahead and teach you?"),
                 _("Wow, no way! Thank you, friend!")],
                ["Cheng Youde", "Cheng Youde", "you", "you", "Cheng Youde", "Cheng Youde", "Cheng Youde", "you"]
            )

            new_move = special_move(name=_("Purity Blade"))
            messagebox.showinfo("",_("Cheng Youde teaches you a new move: Purity Blade!"))
            self.game.learn_move(new_move)

        else:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageLakeWin,
                "Lake.png",
                [_("Brilliant!"),
                 _("It's always a pleasure to spar with you, {}!").format(self.game.character)],
                ["Cheng Youde", "Cheng Youde"]
            )



    def clickedOnLiGuiying(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if self.game.taskProgressDic["li_guiping_task"] == 5:
            self.game.taskProgressDic["li_guiping_task"] = 6
            self.game.add_item(_("Sack"), -1)
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("Greetings!"),
                 _("Hello... What do you need?"),
                 _("I have a gift for you! From your sister..."),
                 _("*You hand the sack over to Li Guiying*"),
                 _("Perfect! I am very close to fully mastering the Five Poison Palm!"),
                 _("This will speed up my progress even more! Hahahahahaha!!"),
                 _("Congrats!"), #Row 2
                 _("Thank you for your help. Talk to me again in a couple of days."),
                 _("I'll have something else to ask of you then.")],
                ["you", "Li Guiying", "you", "Blank", "Li Guiying", "Li Guiying",
                 "you", "Li Guiying", "Li Guiying"]
            )
            return

        elif self.game.taskProgressDic["li_guiping_task"] in [6,7]:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("Are you ready?"),
                 _("Not yet... Very close though. Give me a bit more time."),
                 _("Will do!")],
                ["you", "Li Guiying", "you"]
            )
            return

        elif self.game.taskProgressDic["li_guiping_task"] == 8:
            self.game.taskProgressDic["li_guiping_task"] = 9
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("Hahahaha! I've fully mastered the Five Poison Palm!"),
                 _("Very nice!!! So, what now?"),
                 _("The reason Master sent me out from Phantom Sect is so I can find a quiet place to concentrate on learning this technique."),
                 _("Now, it's time to make those 'Chivalrous' sects pay..."),
                 _("Oh? What do you mean?"),
                 _("I'm going to every sect that has caused trouble for Phantom Sect to teach them a lesson."),
                 _("I will instill fear into their hearts by showing them even a disciple of Phantom Sect can defeat their elders with ease."),
                 _("Why don't you join me? I will reward you if you do well."), #Row 2
                 _("I'm interested but... there's 1 small condition that I hope you won't mind agreeing to..."),
                 _("What is it?"),
                 _("Well, I don't want to get in trouble with every sect out there, so do you have a mask that I can wear to disguise as a Phantom Sect disciple?"),
                 _("Y'know, one of those that everyone in the sect likes to wear apparently..."),
                 _("Hahaha... not a problem. Here you go."),
                 _("Put this on and let me know when you are ready to go."),],
                ["Li Guiying", "you", "Li Guiying", "Li Guiying", "you", "Li Guiying", "Li Guiying",
                 "Li Guiying", "you", "Li Guiying", "you", "you", "Li Guiying", "Li Guiying"]
            )
            self.game.add_item(_("Mask"))
            messagebox.showinfo("", _("Li Guiying hands you a mask."))
            return

        elif self.game.taskProgressDic["li_guiping_task"] == 9:
            if _("Mask") in [eq.name for eq in self.game.equipment]:
                self.game.liuVillageHouseWin.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "liu_village_house.png",
                    [_("I'm ready!"),
                     _("Very good... First stop, Wudang...")],
                    ["you", "Li Guiying"]
                )
                self.challenge_chivalrous_sects(stage=0)
            else:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("Aren't you going to put on that mask first?")],
                    ["Li Guiying"]
                )


        for widget in self.game.liuVillageHouseWinF1.winfo_children():
            widget.destroy()

        Button(self.game.liuVillageHouseWinF1, text=_("Spar"),
               command=partial(self.interactWithLiGuiying, 1)).grid(row=1, column=0)

        if self.game.taskProgressDic["li_guiying"] in [100, 101, 102, 103, 111, 112, 113]:
            Button(self.game.liuVillageHouseWinF1, text=_("What did you want me to do again?"),
                   command=partial(self.interactWithLiGuiying, 2)).grid(row=2, column=0)
            Button(self.game.liuVillageHouseWinF1, text=_("I've got the moths that you asked for."),
                   command=partial(self.interactWithLiGuiying, 3)).grid(row=3, column=0)


    def challenge_chivalrous_sects(self, stage):
        self.game.battleID = None
        if stage == 0:
            self.game.generate_dialogue_sequence(
                None,
                "Wudang.png",
                [_("Phantom Sect is not welcome here. Please leave."),
                 _("Hahahahaha! You stupid priests are but ants in my eyes..."),
                 _("You! I did not wish to escalate matters since you are a woman, but now you're asking for it!")],
                ["Li Kefan", "Li Guiying", "Li Kefan"]
            )

            cmd = lambda: self.challenge_chivalrous_sects(1)
            response = messagebox.askquestion("", _("Take Li Guiying's place in the fight?"))
            if response == "yes":
                self.game.generate_dialogue_sequence(
                    None,
                    "Wudang.png",
                    [_("Hey! Am I air to you? Why pick on a lady when I'm standing right here?")],
                    ["you"]
                )
                you = self.game.you
            else:
                you = character(_("Li Guiying"), 20,
                                sml=[special_move(_("Poison Needle"), level=10),
                                     special_move(_("Phantom Claws"), level=10),
                                     special_move(_("Five Poison Palm"), level=10),
                                     special_move(_("Rest"))],
                                skills=[skill(_("Phantom Steps"), level=10)]
                                )

            opp = character(_("Li Kefan"), 18,
                            sml=[special_move(_("Taichi Fist"), level=9),
                                 special_move(_("Taichi Sword"), level=8)],
                            skills=[skill(_("Wudang Agility Technique"), level=8),
                                    skill(_("Pure Yang Qi Skill"), level=8)])

            self.game.battleID = "li_guiping_task"
            self.game.battleMenu(you, opp, "Wudang.png", postBattleSoundtrack="jy_suspenseful2.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[], postBattleCmd=cmd)


        elif stage == 1:
            self.game.restore(0,0,full=True)
            self.game.generate_dialogue_sequence(
                None,
                "Wudang.png",
                [_("Hahahahaha! Weak!"),
                 _("Come on, {}, let's go find some opponents that are actually a challenge...").format(self.game.character),],
                ["Li Guiying", "Li Guiying"]
            )

            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Amitabha... What have you to do with Shaolin?"),
                 _("I'm here to represent Phantom Sect. I propose that Shaolin becomes part of Phantom Sect."),
                 _("After all, Shaolin Kung-Fu is much inferior than that of Phantom Sect."),
                 _("Amitabha... As monks, we seek a peaceful and quiet life of meditation."),
                 _("But what you have said greatly insults Shaolin's reputation. I'm afraid I cannot let you go so easily...")],
                ["Elder Xuanjing", "Li Guiying", "Li Guiying", "Elder Xuanjing", "Elder Xuanjing"]
            )

            cmd = lambda: self.challenge_chivalrous_sects(2)
            response = messagebox.askquestion("", _("Take Li Guiying's place in the fight?"))
            if response == "yes":
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Hey! Am I air to you? Why pick on a lady when I'm standing right here?")],
                    ["you"]
                )
                you = self.game.you
            else:
                you = character(_("Li Guiying"), 20,
                                sml=[special_move(_("Poison Needle"), level=10),
                                     special_move(_("Phantom Claws"), level=10),
                                     special_move(_("Five Poison Palm"), level=10),
                                     special_move(_("Rest"))],
                                skills=[skill(_("Phantom Steps"), level=10)]
                                )

            opp = character(_("Elder Xuanjing"), 18,
                            sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                                 special_move(_("Shaolin Diamond Finger"), level=8)],
                            skills=[skill(_("Shaolin Mind Clearing Method"), level=8),
                                    skill(_("Shaolin Inner Energy Technique"), level=10)])

            self.game.battleID = "li_guiping_task"
            self.game.battleMenu(you, opp, "Shaolin.png", postBattleSoundtrack="jy_suspenseful2.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[], postBattleCmd=cmd)
            
        elif stage == 2:
            self.game.restore(0,0,full=True)
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Hahahahaha! Weak!"),
                 _("Shaolin really should just withdraw from Wulin and start planting fruits and vegetables hahahaha!"),
                 _("Time for Shanhu Sect to bow before us...")],
                ["Li Guiying", "Li Guiying", "Li Guiying"]
            )

            self.game.generate_dialogue_sequence(
                None,
                "Shanhu Sect.png",
                [_("Li Guiying... The top disciple of Phantom Sect... "),
                 _("I hope you are not here to cause trouble..."),
                 _("That depends on your response... I have a proposal..."),
                 _("Starting from today, Shanhu Sect becomes a part of Phantom Sect."),
                 _("Hahahahaha! What a joke!"),
                 _("Whether I'm joking or not will be obvious once I defeat you."),
                 _("Of course, as a youngster, I know I am no match for Master Tan, but surely you won't personally fight someone half your age..."),
                 _("Don't worry. I will not trouble you."), #Row 2
                 _("Master Tan, no need for you to do anything. Either of us can handle this."),
                 _("Good! Bring it then...")],
                ["Tan Keyong", "Tan Keyong", "Li Guiying", "Li Guiying", "Yang Kai", "Li Guiying", "Li Guiying",
                 "Tan Keyong", "Zhu Shi Ting", "Li Guiying"]
            )

            cmd = lambda: self.challenge_chivalrous_sects(3)
            response = messagebox.askquestion("", _("Take Li Guiying's place in the fight?"))
            if response == "yes":
                self.game.generate_dialogue_sequence(
                    None,
                    "Shanhu Sect.png",
                    [_("Hey! Am I air to you? Why pick on a lady when I'm standing right here?")],
                    ["you"]
                )
                you = self.game.you
            else:
                you = character(_("Li Guiying"), 20,
                                sml=[special_move(_("Poison Needle"), level=10),
                                     special_move(_("Phantom Claws"), level=10),
                                     special_move(_("Five Poison Palm"), level=10),
                                     special_move(_("Rest"))],
                                skills=[skill(_("Phantom Steps"), level=10)]
                                )


            opp_list = []
            opp_list.append(character(_("Zhu Shi Ting"), 20,
                                sml=[special_move(_("Shanhu Fist"), level=9),
                                     special_move(_("Basic Punching Technique"), level=10),
                                     special_move(_("Soothing Song"))],
                                skills=[skill(_("Divine Protection"), level=8),
                                        skill(_("Leaping Over Mountain Peaks"), level=8)]))
            opp_list.append(character(_("Yang Kai"), 20,
                                sml=[special_move(_("Shanhu Fist"), level=5),
                                     special_move(_("Demon Suppressing Blade"), level=10)],
                                skills=[skill(_("Divine Protection"), level=5),
                                        skill(_("Leaping Over Mountain Peaks"), level=10)]))
            opp = pickOne(opp_list)

            self.game.battleID = "li_guiping_task"
            self.game.battleMenu(you, opp, "Shanhu Sect.png", postBattleSoundtrack="jy_suspenseful2.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[], postBattleCmd=cmd)

        else:
            self.game.taskProgressDic["li_guiping_task"] = 10
            self.game.generate_dialogue_sequence(
                None,
                "Shanhu Sect.png",
                [_("Hahahahaha! Even the great Shanhu Sect is no match for Phantom Sect!"),
                 _("Now, all will fear the mighty Phantom Sect! Master will be very pleased with me!"),
                 _("Thanks for your help, {}. As promised... your reward.").format(self.game.character)],
                ["Li Guiying", "Li Guiying", "Li Guiying",]
            )
            self.game.add_item(_("Venomous Viper Gloves"))
            messagebox.showinfo("", _("Received 'Venomous Viper Gloves'!"))
            self.game.liuVillageHouseWin.deiconify()


    def interactWithLiGuiying(self, choice):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        if choice == 1:
            if _("Five Poison Palm") not in [m.name for m in self.game.sml]:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("I don't have time for that..."),
                     _("Can't you see I'm busy practicing the Five Poison Palm?")],
                    ["Li Guiying", "Li Guiying"]
                )
            else:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("Let's see how much progress you've made...")],
                    ["Li Guiying"]
                )

                opp = character(_("Li Guiying"), 14,
                                sml=[special_move(_("Poison Needle"), level=10),
                                     special_move(_("Phantom Claws"), level=9),
                                     special_move(_("Five Poison Palm"), level=9)],
                                skills=[skill(_("Phantom Steps"), level=10)])

                self.game.battleMenu(self.game.you, opp, "battleground_liu_village_house.png",
                                postBattleSoundtrack="jy_ambient.mp3",
                                fromWin=self.game.liuVillageHouseWin,
                                battleType="training", destinationWinList=[self.game.liuVillageHouseWin] * 3,
                                )

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("I need you to bring me 5 of each of the Poisonous Moths."),
                 _("You should be able to catch them in the nearby forest.")],
                ["Li Guiying", "Li Guiying"]
            )

        elif choice == 3:

            distinct_moths_count = 0
            for item in self.game.inv:
                if _("Moth") in item and self.game.inv[item] >= 5:
                    distinct_moths_count += 1

            if distinct_moths_count < 5:
                messagebox.showinfo("", _("You have not collected enough moths."))

            elif _("Five Poison Palm") in [m.name for m in self.game.sml]:
                for item in self.game.inv:
                    if _("Moth") in item:
                        self.game.inv[item] -= 5
                for move in self.game.sml:
                    if move.name == _("Five Poison Palm"):
                        move.gain_exp(500)
                messagebox.showinfo("", _("You hand the moths over to Li Guiying who shows you how to use the moths. You gain 500 experience in Five Poison Palm."))

            else:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("Perfect! This will help me to master the Five Poison Palm!"),
                     _("As promised, let me teach you this technique..."),
                     _("If you have additional moths, you can also bring them to me."),
                     _("I will teach you how to master this technique more quickly using these moths.")],
                    ["Li Guiying", "Li Guiying", "Li Guiying", "Li Guiying"]
                )

                for item in self.game.inv:
                    if "Moth" in item:
                        self.game.inv[item] -= 5

                new_move = special_move(name=_("Five Poison Palm"))
                messagebox.showinfo("", _("You hand the moths over to Li Guiying, who teaches you a new move: Five Poison Palm!"))
                self.game.learn_move(new_move)


    def clickedOnBlindSniper(self):
        if self.game.taskProgressDic["liu_village"] in [11, 21]:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("I will stick around for a few days to investigate the village massacre."),
                 _("If you are interested, you can look around too.")],
                ["Blind Sniper", "Blind Sniper"]
            )

        elif self.game.taskProgressDic["liu_village"] == 12:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("Hey! Guess what I found!"),
                 _("Hmmm?"),
                 _("I came upon some masked men searching for something; I managed to defeat them and one of them dropped these..."),
                 _("Smoke bombs... what a dishonorable weapon..."),
                 _("From the looks of it, Phantom Sect is probably involved."),
                 _("Lu Xifeng, the Phantom Sect leader, is known for his cruel and dishonorable techniques."),
                 _("So you're saying Phantom Sect is behind all of this?"),
                 _("That's what I suspect, yes..."),
                 _("Any idea what they're looking for? Or why they killed an entire village?"),
                 _("Nearly 4 decades ago, Liu Zhiyuan, the head of the Liu village, defeated Zhao Jianguo, one of the most feared martial artists at the time, in a duel."),
                 _("Zhao Jianguo was slain in the duel, and Liu Zhiyuan obtained the manual for the Devil Crescent Moon Blade, Zhao's specialty."),
                 _("The Devil Crescent Moon Blade is said to be extremely powerful, too powerful, in fact, that whoever practices it seems to lose control over their bodies."),
                 _("To practice the technique, one must commit some unspeakable acts."),
                 _("Furthermore, the more one's heart is bent on doing evil, the more powerful the technique becomes."),
                 _("Wow... Did Liu Zhiyuan practice it?"),
                 _("No, Liu was far too chivalrous to do that. He wouldn't have been able to unlock the technique's full potential anyway."),
                 _("If this devil blade thing really is so powerful, how did Zhao lose to Liu?"),
                 _("A few months before his duel with Liu at Huashan, Zhao met a woman named Song Meixin."),
                 _("He fell in love with her, and since then, he began to change..."),
                 _("Her gentleness and compassion softened his heart, and he turned from his evil ways."),
                 _("Consequently, he was no longer able to use his special technique at its maximum potential."),
                 _("After Zhao's death, Song Meixin gave birth to a son. They lived a quiet life near Huashan and not much is known about them besides that."),
                 _("What became of Liu, then?"),
                 _("Even though Liu never learned the Devil Crescent Moon Blade, he made one mistake..."),
                 _("As a martial arts fanatic, he could not bring himself to destroy such a powerful manual."),
                 _("So he kept it... but this soon attracted many unnecessary troubles..."),
                 _("All the bad guys started going after it?"),
                 _("Yep... but until recently, no one's succeeded. Just last year, Liu Zhiyuan passed away from old age."),
                 _("This created the prime opportunity for power-hungry martial artists."),
                 _("People like Lu Xifeng?"),
                 _("Exactly... Lu is already a tough opponent... I fear if he gets his hands on the manual..."),
                 _("Well, the fact that those masked men came back looking for something means they haven't found it yet, right?"),
                 _("I sure hope so..."),
                 _("In any case, let us continue to be alert. If you find anything, be sure to let me know."),
                 _("Will do!")],
                ["you", "Blind Sniper", "you", "Blind Sniper", "Blind Sniper", "Blind Sniper",
                 "you", "Blind Sniper", "you", "Blind Sniper", "Blind Sniper", "Blind Sniper",
                 "Blind Sniper", "Blind Sniper", "you", "Blind Sniper", "you", "Blind Sniper",
                 "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper", "you",
                 "Blind Sniper", "Blind Sniper", "Blind Sniper", "you", "Blind Sniper", "Blind Sniper",
                 "you", "Blind Sniper", "you", "Blind Sniper", "Blind Sniper", "you"
                 ]
            )
            self.game.taskProgressDic["liu_village"] = 13

        elif self.game.taskProgressDic["liu_village"] == 13:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("I will stick around this area. If you find anything, let me know.")],
                ["Blind Sniper"]
            )

        elif self.game.taskProgressDic["liu_village"] == 14:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("*You tell the Blind Sniper what happened in the Dark Forest...*"),
                 _("Hmmm, check that area periodically and see if you can find our more about their plans."),
                 _("Ok, will do!")],
                ["Blank", "Blind Sniper", "you"]
            )

        elif self.game.taskProgressDic["liu_village"] == 16:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("*You tell the Blind Sniper what happened in the Dark Forest...*"),
                 _("We must head to Phantom Sect at once and stop their plans!"),
                 _("No, it's ok. I'll go alone."),
                 _("But..."),
                 _("I'm not sure I can survive this encounter. If I die, at least you can avenge me."),
                 _("Or if I'm captured, you can ask your acquaintances and friends to help rescue me."),
                 _("You have a point... The path to Phantom Sect is a dangerous one, and my blindness will no doubt become a burden to you."),
                 _("The fate of Wulin rests in your hands, {}. Please be careful.").format(self.game.character),
                 _("I will do everything in my power to stop Lu Xifeng!")],
                ["Blank", "Blind Sniper", "you", "Blind Sniper", "you", "you", "Blind Sniper", "Blind Sniper", "you"]
            )

        elif self.game.taskProgressDic["blind_sniper"] == 0:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("Any news on Wei Yan? How is she? Will you be able to rescue her?"),
                 _("Whoa whoa, calm down, my friend. I've got a plan already! Just hang tight."),
                 _("(The Empress' Palace will be very heavily guarded. I won't be able to get in using normal means.)"),
                 _("(Maybe Scrub can direct me to someone who might be able to help...)")],
                ["Blind Sniper", "you", "you", "you"]
            )

        elif self.game.taskProgressDic["blind_sniper"] == 1000000:

            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_shendiaoshuhuan.mp3"
            self.game.startSoundtrackThread()

            self.game.liuVillageHouseWin.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("(I wonder if I should tell the Blind Sniper the truth... I don't know how he'll take it...)")],
                ["you"],
                [[_("Tell him the truth."), partial(self.interactWithBlindSniper, 1000001)],
                 [_("Tell a lie."), partial(self.interactWithBlindSniper, 1000002)]]
            )
            return


        for widget in self.game.liuVillageHouseWinF1.winfo_children():
            widget.destroy()

        Button(self.game.liuVillageHouseWinF1, text=_("Spar"),
               command=partial(self.interactWithBlindSniper, 1)).grid(row=1, column=0)
        Button(self.game.liuVillageHouseWinF1, text=_("Talk"),
               command=partial(self.interactWithBlindSniper, 2)).grid(row=2, column=0)
        if self.game.taskProgressDic["blind_sniper"] == -1:
            Button(self.game.liuVillageHouseWinF1, text=_("How did you become blind?"),
                   command=partial(self.interactWithBlindSniper, 3)).grid(row=3, column=0)


    def interactWithBlindSniper(self, choice):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        if choice == 1:
            self.game.liuVillageHouseWin.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("Very well, let me assess your skills in combat.")],
                ["Blind Sniper"]
            )

            opp = character(_("Blind Sniper"), 15,
                            sml=[special_move(_("Basic Sword Technique"), level=10),
                                 special_move(_("Taichi Sword"), level=8),
                                 special_move(_("Blind Sniper Blade"), level=10)],
                            skills=[skill(_("Wudang Agility Technique"), level=9),
                                    skill(_("Pure Yang Qi Skill"), level=9)])

            self.game.battleMenu(self.game.you, opp, "battleground_liu_village_house.png",
                                 postBattleSoundtrack="jy_ambient.mp3",
                                 fromWin=self.game.liuVillageHouseWin,
                                 battleType="training", destinationWinList=[self.game.liuVillageHouseWin] * 3,
                                 )

        elif choice == 2:
            if randrange(2) == 0:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("The eyes may be blind, but the heart still sees...")],
                    ["Blind Sniper"]
                )
            else:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("For the sake of sanity, it sometimes helps to turn a blind eye to trivial things."),
                     _("However, matters of great import need be addressed without hesitation.")],
                    ["Blind Sniper", "Blind Sniper"]
                )

        elif choice == 3:
            year_var = calculate_month_day_year(self.game.gameDate)["Year"] + 20
            if self.game.taskProgressDic["blind_sniper"] == -1 and self.game.chivalry >= 10:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("*sigh*... I rarely speak of that nowadays, but you seem trustworthy and chivalrous."),
                     _("I was born into a middle-class family in Hangzhou. My real name is Liang Zhe."),
                     _("I grew up with the daughter of a family friend; her name is Wei Yan."),
                     _("Our families had a good relationship, and we often gathered for celebrations and such."),
                     _("Over time, we fell in love with each other."),
                     _("Then it happened... {} years ago... That year, I was {}, and she was {}.").format(year_var, year_var-3, year_var-5),
                     _("The Emperor at the time was looking for a suitable wife for his son."), #Row 2
                     _("Wei Yan's family was one of the wealthiest in Hangzhou, so naturally she was one of the candidates."),
                     _("Of course, I would have none of it. The prince has thousands of women to choose from. Why should he take mine?"),
                     _("I told Wei Yan that we should run away and change our names and live a low-key life together."),
                     _("She was scared at first, but I was persistent. She finally agreed, 3 days before the Imperial Eunuchs would arrive at her house."),
                     _("But then... then..."),
                     _("Yes?"), #Row 3
                     _("I'm not sure how, but our plan was discovered."),
                     _("On the night that we fled, we had barely gone 4 kilometers before getting ambushed by some masked men."),
                     _("I found it odd, as there was no need for Imperial Soldiers to hide their identities, but they claimed to be Imperial Agents."),
                     _("I had no choice but to fight. It was no use though, since I was too weak at the time."),
                     _("I was blinded in that battle and would've died if it weren't for Master Zhang Tianjian, the current leader of Wudang."),
                     _("He was able to rescue me, but a couple of the Imperial Agents took away Wei Yan."),
                     _("Master Zhang took me back to Wudang, and I stayed there for 3 years to recover."), #Row 4
                     _("During that time, I was fortunate enough to learn some Wudang techniques."),
                     _("After I had fully gotten used to being blind, I thanked Master Zhang and left Wudang to roam the world."),
                     _("I became obsessed with challenging random people to friendly spars."),
                     _("That, along with my physical blindness, earned me the nickname Blind Sniper. I quite like it."),
                     _("Yeah, it does sound nice... So what happened to Wei Yan? Did you ever look for her?"),
                     _("*A long pause and the narrowing of his eyebrows tell you that you've touched a sore spot.*"), #Row 5
                     _("Wei Yan... She... She was chosen to be princess."),
                     _("Not surprising, given her immense beauty..."),
                     _("Then a few years later, the old Emperor died, and his son inherited the throne, so she is the Empress now."),
                     _("Empress?! Whoaaa...."),
                     _("Funny isn't it? If I didn't have a chance back then, I definitely don't stand a chance now.")],
                    ["Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper",
                     "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper",
                     "you", "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper",
                     "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper", "Blind Sniper", "you",
                     "Blank", "Blind Sniper", "Blind Sniper", "Blind Sniper", "you", "Blind Sniper"],
                    [[_("Don't give up yet! I can help you reunite with her!"), partial(self.blind_sniper_choice, 1)],
                     [_("Yep. Better to let go of the past..."), partial(self.blind_sniper_choice, 2)]]
                )

            elif self.game.taskProgressDic["blind_sniper"] == -1:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("Hah, it is a thing of the past. Let us speak of it no more.")],
                    ["Blind Sniper"]
                )

            else:
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("(Maybe I shouldn't keep on bringing up this matter since he has already told me once...)")],
                    ["you"]
                )

        elif choice == 1000001:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.taskProgressDic["blind_sniper"] = 1000001
            self.game.chivalry += 30
            self.game.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("Your chivalry + 30."),
                 _("*You tell the Blind Sniper everything that happened...*"),
                 _("She... she's really changed..."),
                 _("Or maybe that's just how she is, and wealth and power revealed her true character."),
                 _("*Sigh*... I know you're trying to comfort me. Thank you for risking your life to help me."),
                 _("No problem, friend... Hey, if... if there's anything else I can do for you..."),
                 _("I... I need some time alone. Perhaps it's time for me to retire from Wulin and isolate myself from the outside world."),
                 _("Sure, whatever you need to do..."),
                 _("Thanks, friend... take care...")],
                ["Blank", "Blank", "Blind Sniper", "you", "Blind Sniper", "you", "Blind Sniper", "you", "Blind Sniper"]
            )
            self.game.liuVillageHouseWin.quit()
            self.game.liuVillageHouseWin.destroy()
            self.go_liu_village_house()

        elif choice == 1000002:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.taskProgressDic["blind_sniper"] = 1000002
            self.game.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("Hey, bro, I'm really sorry... I tried to get into the Imperial Palace, but the guards outnumbered me 1000 to 1."),
                 _("I barely escaped alive. I'm afraid I've done all I can."),
                 _("Don't worry about it, my friend. You risked your life to help me. For that, I am deeply grateful.")],
                ["you", "you", "Blind Sniper"]
            )

            if _("Wudang Agility Technique") in [s.name for s in self.game.skills]:
                self.game.add_item(_("Hundred Seeds Pill"), 10)
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("I know your valiant efforts deserve much more than this, but here, take these."),
                     _("*The Blind Sniper gives you 'Hundred Seeds Pill' x 10.*"),
                     _("Thanks! Should I try to sneak into the palace again?"),
                     _("No, I cannot allow you to risk your life for me again. *Sigh*... Perhaps this is fate."),
                     _("It is better that I stay out of her life... As long as she is happy, I am content.")],
                    ["Blind Sniper", "Blank", "you", "Blind Sniper", "Blind Sniper"]
                )
            else:
                new_skill = skill(name=_("Wudang Agility Technique"))
                self.game.learn_skill(new_skill)
                self.game.generate_dialogue_sequence(
                    self.game.liuVillageHouseWin,
                    "liu_village_house.png",
                    [_("I know your valiant efforts deserve much more than this, but let me teach you something to help you in case you run into any trouble in the future."),
                     _("*The Blind Sniper teaches you 'Wudang Agility Technique'.*"),
                     _("Thanks! Should I try to sneak into the palace again?"),
                     _("No, I cannot allow you to risk your life for me again. *Sigh*... Perhaps this is fate."),
                     _("It is better that I stay out of her life... As long as she is happy, I am content.")],
                    ["Blind Sniper", "Blank", "you", "Blind Sniper", "Blind Sniper"]
                )


    def blind_sniper_choice(self, choice):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        if choice == 1:
            self.game.taskProgressDic["blind_sniper"] = 0
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("But how? Nevermind the fact that I'm blind."),
                 _("Even if I wasn't handicapped, it'd be near impossible to get into the Imperial Palace and..."),
                 _("Don't worry about that, my friend. I will bring her to you."),
                 _("But even if she sees me... the way I look now... she... she won't--"),
                 _("We'll worry about that later. Just stay put and wait for my good news!"),
                 _("(The Empress' Palace will be very heavily guarded. I won't be able to get in using normal means.)"),
                 _("(Maybe Scrub can direct me to someone who might be able to help...)")],
                ["Blind Sniper", "Blind Sniper", "you", "Blind Sniper", "you", "you", "you"]
            )


        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.liuVillageHouseWin,
                "liu_village_house.png",
                [_("You're right... She's probably quite happy the way she is right now."),
                 _("It makes no sense for me to hold onto these feelings any longer..."),
                 _("*You gained a skill: 'Firm Resolve'.*")],
                ["Blind Sniper", "Blind Sniper", "Blank"]
            )
            self.game.taskProgressDic["blind_sniper"] = -100
            new_skill = skill(name=_("Firm Resolve"), level=10)
            self.game.learn_skill(new_skill)