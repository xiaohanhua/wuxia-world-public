#scenes
from babao_tower import*
from cherry_blossom_island import*
from shanhu_sect import*
from nongyuan_valley import*
from phantom_sect import*
from shaolin import*
from wudang import*
from huashan import*
from snowy_mountain import*
from bamboo_forest import*
from liu_village import*
from imperial_city import*
from dark_forest import*
from dragon_sect import*
from sea_shore import*
from wangling_road import*
from eagle_sect import*
from wooden_house import*
from wanhua_pavillion import*
from tibetan_plains import*
from small_town import*
from yagyu_clan import*
from credits import*

import tkinter.messagebox as messagebox
#from helper_funcs import *
#from datetime import *
#from copy import copy
#from gettext import gettext
from threading import Thread


from mini_game_ant import*
from mini_game_shaolin_sneak import*
from mini_game_escape_from_shaolin import*
from mini_game_assassinate_xuan_sheng import*
from mini_game_potters_field import*
from mini_game_cemetery import*
from mini_game_yagyu_dungeon_level_1 import*


import locale
import pickle
import subprocess
import os
import ctypes
import time
import Pmw
#import sys
from PIL import Image, ImageTk


_ = gettext
os.chdir(RESOURCES_DIR)


class game:

    def __init__(self, win):

        if os.name == "nt":
            if locale.getdefaultlocale() == "cp1252":
                win.option_add("*Label.Font", "TkDefaultFont 11")
                win.option_add("*Button.Font", "TkDefaultFont 11")
            else:
                win.option_add("*Label.Font", "Helvetica 11")
                win.option_add("*Button.Font", "Helvetica 11")
        

        self.partner_gift = None
        self.mini_game_in_progress = False
        self.ptc = 0 #counts number of play-thrus
        self.currentBGM = "jy_intro.mp3"
        self.soundtrackOn = IntVar()
        self.soundtrackOn.set(1)
        self.soundEffectOn = IntVar()
        self.soundEffectOn.set(1)

        try:
            qs = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "quicksave.p"), "rb"))
        except:
            qs = 0
            pickle.dump(qs, open(os.path.join(SAVE_SLOTS_DIR, "quicksave.p"), "wb"))
        self.quickSave = IntVar()
        self.quickSave.set(qs)


        self.startSoundtrackThread()
        self.balloon = Pmw.Balloon()

        self.levelThresholds = [200, 450, 800, 1300, 2000, 3000, 4200, 5600, 7200, 9200,
                                12000, 15500, 20000, 26000, 35000, 48000, 65000, 88000, 120000, 160000,
                                210000, 272000, 348000, 440000, 550000, 680000, 832000, 1008000, 1210000, 9999999999]

        
        
        self.battle_items = [_("Crab"),
                             _("Fish"),
                             _("Soup"),
                             _("Dumplings"),
                             _("Wine"),
                             _("Cherry Blossom Papaya Tremella Soup"),
                             _("Small Recovery Pill"),
                             _("Big Recovery Pill"),
                             _("Bezoar Pill"),
                             _("Snow Lotus Pill"),
                             _("Winterberry"),
                             _("Tea Eggs")
                             ]


        self.animation_frame_delta_dic = {
            _("Scrub Fist"): 180,
            _("Real Scrub Fist"): 180,
            _("Basic Punching Technique"): 180,
            _("Yunhuan Taichi Fist"): 180,
            _("Five Poison Palm"): 85,
            _("Fire Palm"): 100,
            _("Guan Yin Palm"): 70,
            _("Kiss of Death"): 80,
            _("Ice Palm"): 100,
            _("Ice Burst"): 90,
            _("Shaolin Luohan Fist"): 100,
            _("Sumeru Palm"): 90,
            _("Prajna Palm"): 60,
            _("Hun Yuan Shapeshifting Fist"): 110,
            _("Falling Cherry Blossom Finger Technique"): 50,
            _("Fairy Plucking Flowers"): 65,
            _("Lightning Finger Technique"): 60,
            _("Shaolin Diamond Finger"): 40,
            _("Empty Force Fist"): 120,
            _("Taichi Fist"): 70,
            _("Thunder Palm"): 30,
            _("Iron Sand Palm"): 75,
            _("Shapeless Palm"): 70,
            _("Phantom Claws"): 50,
            _("Eagle Claws"): 75,
            _("Heart Shattering Fist"): 70,
            _("Shanhu Fist"): 80,
            _("Dragon Roar"): 90,
            _("Basic Sword Technique"): 40,
            _("Huashan Sword Technique"): 120,
            _("Thousand Swords Technique"): 60,
            _("Cherry Blossom Drifting Snow Blade"): 70,
            _("Taichi Sword"): 90,
            _("Ice Blade"): 110,
            _("Kendo"): 100,
            _("Hachiman Blade"): 80,
            _("Shadow Blade"): 90,
            _("Blind Sniper Blade"): 80,
            _("Babao Blade"): 60,
            _("Wood Combustion Blade"): 80,
            _("Poison Needle"): 20,
            _("Violent Dragon Palm"): 120,
            _("Rainbow Fragrance"): 60,
            _("Spider Poison Powder"): 50,
            _("Centipede Poison Powder"): 50,
            _("Body Cleansing Technique"): 30,
            _("Soothing Song"): 40,
            _("Smoke Bomb"): 60,
            _("Superior Smoke Bomb"): 60,
            _("Whirlwind Staff"): 45,
            _("Wild Wind Blade"): 90,
            _("Serpent Whip"): 100,
            _("Kusarigama"): 100,
            _("Hensojutsu"): 80,
            _("Purity Blade"): 80,
            _("Demon Suppressing Blade"): 110,
            _("Fairy Sword Technique"): 110,
            _("Devil Crescent Moon Blade"): 70,
            _("Luohan Staff"): 70,
            _("Archery"): 25,
            _("Crossbow"): 25,
            _("Spear Attack"): 60,
            _("Dodge"): 80,
            _("Rest"): 40,
            _("Flee"): 100
        }

        self.battleID = None  # determines whether there is a reward at end of battle
        self.saveSlot = None

        self.mainWin = win
        self.mainWin.title(_("Wuxia World"))
        if os.name == "nt":
            self.mainWin.tk.call('wm', 'iconphoto', self.mainWin._w, PhotoImage(file=r'game_main_icon.png'))
        else:
            self.mainWin.tk.call('wm', 'iconphoto', self.mainWin._w, PhotoImage(file=r'game_main_icon.png'))

        bg = PhotoImage(file="wuxia_world.png", master=self.mainWin)
        w = bg.width()
        h = bg.height()

        panel = Label(self.mainWin, image=bg)
        panel.pack(expand="yes")
        panel.image = bg

        main_frame = Frame(self.mainWin)
        main_frame.pack()

        placeholder_block_1 = Label(main_frame, text="\t\t\t\t")
        placeholder_block_1.grid(row=0, column=0)

        button_frame = Frame(main_frame)
        button_frame.grid(row=0,column=1)
        Button(button_frame, text=_("New Game"), command=self.startNewGame).pack()
        Button(button_frame, text=_("Continue"), command=self.continueGame).pack()
        Button(button_frame, text=_("Attribution"), command=self.displayAttribution).pack()


        placeholder_block_2 = Label(main_frame, text="\t\t")
        placeholder_block_2.grid(row=0, column=2)

        discord_frame = Frame(main_frame)
        discord_frame.grid(row=0, column=3)
        self.discord_button_image = PhotoImage(file="Scrub_discord_icon.png")
        imageButton = Button(discord_frame, command=self.joinDiscord)
        imageButton.pack()
        imageButton.config(image=self.discord_button_image)
        Label(discord_frame, text=_("Join Discord to\nreport bugs &\ngive feedback!")).pack()

        

        disclaimer_frame = Frame(self.mainWin, borderwidth=2, relief=SUNKEN)
        disclaimer_frame.pack()
        Label(disclaimer_frame, text=DISCLAIMER, justify=LEFT, font=("Helvetica 9")).pack(anchor=W)

        self.mainWin.update_idletasks()
        toplevel_w, toplevel_h = self.mainWin.winfo_width(), self.mainWin.winfo_height()
        self.mainWin.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.mainWin.focus_force()
        #self.mainWin.mainloop()
        

    ################### HELPER FUNCTIONS ###################
    ################### HELPER FUNCTIONS ###################
    ################### HELPER FUNCTIONS ###################
    ################### HELPER FUNCTIONS ###################
    ################### HELPER FUNCTIONS ###################


    def learn_skill(self, skill_obj):
        current_skills = [s.name for s in self.skills]
        if skill_obj.name not in current_skills:
            self.skills.append(skill_obj)

        
    def learn_move(self, move_obj):
        current_moves = [m.name for m in self.sml]
        if move_obj.name not in current_moves:
            self.sml.insert(len(self.sml) - 3, move_obj)
            if len(self.sml_checked) < 10:
                self.sml_checked.append(move_obj.name)


    def on_exit(self):
        pass


    def terminate_thread(self, thread, delay=0):

        if delay > 0:
            time.sleep(delay)

        if os.name == "nt":
            pass
        else:
            processID = self.process.pid

            try:
                os.kill(processID, 0)
                self.process.kill()
            except:
                print("Terminated gracefully")

            if self.followupProcess != None:
                followupID = self.followupProcess.pid

                try:
                    os.kill(followupID, 0)
                    self.followupProcess.kill()
                except:
                    pass

            if not thread.isAlive():
                return

            exc = ctypes.py_object(SystemExit)
            res = ctypes.pythonapi.PyThreadState_SetAsyncExc(
                ctypes.c_long(thread.ident), exc)
            if res == 0:
                raise ValueError("nonexistent thread id")
            elif res > 1:
                # """if it returns a number greater than one, you're in trouble,
                # and you should call it again with exc=NULL to revert the effect"""
                ctypes.pythonapi.PyThreadState_SetAsyncExc(thread.ident, None)
                raise SystemError("PyThreadState_SetAsyncExc failed")

            self.terminate_thread(self.stopThread)


    def startSoundtrackThread(self, delay=0, loop=False, followup=None, followupDelay=0):
        if not self.soundtrackOn.get():
            return
        # self.loopSoundtracks = True
        if os.name == "nt":
            #freq = mutagen.mp3.MP3(self.currentBGM).info.sample_rate
            pg.mixer.pre_init()
            pg.mixer.init()
            pg.mixer.music.load(self.currentBGM.replace("mp3", "ogg").replace("m4a", "ogg"))
            pg.mixer.music.play(10)
        else:
            self.soundtrackThread = Thread(target=self.playSoundtrack, args=(delay, loop, followup, followupDelay))
            self.soundtrackThread.start()


    def playSoundtrack(self, delay=0, loop=False, followup=None, followupDelay=0):

        if delay > 0:
            time.sleep(delay)

        if os.name == "nt":
            # time.sleep(.5)
            # self.currentBGM = self.currentBGM.replace("mp3", "wav")
            # self.currentBGM = self.currentBGM.replace("m4a", "wav")
            # flags = winsound.SND_FILENAME | winsound.SND_ASYNC | winsound.SND_LOOP
            # winsound.PlaySound(self.currentBGM, flags)
            pass

        else:
            self.process = subprocess.Popen(["afplay", self.currentBGM])
            self.followupProcess = None

            if followup != None:
                if followupDelay > 0:
                    time.sleep(followupDelay)
                self.followupProcess = subprocess.Popen(["afplay", followup])


    def stopSoundtrack(self, delay=0):
        if os.name == "nt":
            try:
                pg.mixer.music.stop()
            except:
                pass
        else:
            self.stopThread = Thread(target=self.terminate_thread, args=(self.soundtrackThread, delay))
            self.stopThread.start()
            self.stopThread.join()


    def startSoundEffectThread(self, delay=0, loop=False):
        if not self.soundEffectOn.get():
            return
        if self.currentEffect == _("sfx_blind_sniper_blade.mp3"):
            delay=.5

        if os.name == "nt":
            if not pg.mixer.get_init():
                pg.mixer.init()
            sound_effect = pg.mixer.Sound(self.currentEffect.replace("mp3", "ogg").replace("m4a", "ogg"))
            sound_effect.play()

        else:
            self.soundEffectThread = Thread(target=self.playSoundEffect, args=(delay, loop))
            self.soundEffectThread.start()
            self.soundEffectThread.join()


    def playSoundEffect(self, delay, loop):

        if delay > 0:
            time.sleep(delay)

        if os.name == "nt":
            pass
            #playsound(self.currentEffect)
        else:
            self.process2 = subprocess.Popen(["afplay", self.currentEffect])


    def stopSoundEffect(self):

        if os.name != "nt":
            self.terminate_thread(self.soundEffectThread)


    def winSwitch(self, fromWin, toWin, destroy=True):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        try:
            if fromWin == self.scrubHouseWin and toWin == self.mainWin:
                response = messagebox.askquestion("", _("You are about to head back to the main menu. All unsaved progress will be lost. Proceed to main menu?"))
                if response == "yes":
                    pass
                else:
                    return
            if fromWin == self.invWin and toWin == self.battleWin:
                pass
                #self.youActionPoints -= self.opp.speed

        except:
            pass

        if destroy:
            fromWin.quit()
            fromWin.destroy()
        else:
            fromWin.withdraw()
        toWin.deiconify()


    ################
    ################
    ##### MAIN #####
    ################
    ################

    def joinDiscord(self):
        if os.name == "nt":
            res = 1
            try:
                res = subprocess.call(
                    "start microsoft-edge:https://discord.gg/wtpNJkpmC2",
                    shell=True, timeout=4)
            except:
                res = subprocess.call(
                    "start chrome.exe https://discord.gg/wtpNJkpmC2",
                    shell=True, timeout=4)
            else:
                if res == 1:
                    messagebox.showinfo("", _(
                        "Default browser not found. Join Discord at: https://discord.gg/wtpNJkpmC2"))
                    print("Discord link: https://discord.gg/wtpNJkpmC2")

        else:
            try:
                subprocess.Popen(["open", "https://discord.gg/wtpNJkpmC2"])
            except:
                messagebox.showinfo("", _(
                    "Default browser not found. Join Discord at: https://discord.gg/wtpNJkpmC2"))
                print("Discord link: https://discord.gg/wtpNJkpmC2")


    def displayAttribution(self):
        self.mainWin.withdraw()
        self.attributionWin = Toplevel(self.mainWin)
        
        Label(self.attributionWin, text=_("See attribution for 查看素材来源:")).pack()
        button_frame = Frame(self.attributionWin, borderwidth=2, relief=SUNKEN)
        button_frame.pack()
        Button(button_frame, text=_("Sprites/Icons 图标 (OpenGameArt - Part I)"), command=partial(self.displayAttributionFor, SPRITE_ATTRIBUTION_OGA_1)).pack(anchor=W)
        Button(button_frame, text=_("Sprites/Icons 图标 (OpenGameArt - Part II)"), command=partial(self.displayAttributionFor, SPRITE_ATTRIBUTION_OGA_2)).pack(anchor=W)
        Button(button_frame, text=_("Sprites/Icons 图标 (Blog.Naver)"), command=partial(self.displayAttributionFor, SPRITE_ATTRIBUTION_BLOGNAVER)).pack(anchor=W)
        Button(button_frame, text=_("Sprites/Icons 图标 (Other)"), command=partial(self.displayAttributionFor, SPRITE_ATTRIBUTION_OTHER)).pack(anchor=W)
        Button(button_frame, text=_("Images/Photos 图片 (Unsplash)"), command=partial(self.displayAttributionFor, IMAGE_ATTRIBUTION_UNSPLASH)).pack(anchor=W)
        Button(button_frame, text=_("Images/Photos 图片 (Other)"), command=partial(self.displayAttributionFor, IMAGE_ATTRIBUTION_OTHER)).pack(anchor=W)
        Button(button_frame, text=_("Animation 动画"), command=partial(self.displayAttributionFor, ANIMATION_ATTRIBUTION)).pack(anchor=W)
        Button(button_frame, text=_("Sound Effects 音效"), command=partial(self.displayAttributionFor, SFX_ATTRIBUTION)).pack(anchor=W)
        Button(button_frame, text=_("Music 音乐"), command=partial(self.displayAttributionFor, MUSIC_ATTRIBUTION)).pack(anchor=W)
        Button(self.attributionWin, text=_("Back"), command=partial(self.winSwitch, self.attributionWin, self.mainWin)).pack()

        self.attributionWin.mainloop()


    def displayAttributionFor(self, attribution_string):
        
        self.attributionWin.withdraw()
        self.attributionTextWin = Toplevel(self.attributionWin)
        attribution_text_frame = Frame(self.attributionTextWin, borderwidth=2, relief=SUNKEN)
        attribution_text_frame.pack()

        Label(attribution_text_frame, text = attribution_string.strip(), justify=LEFT).pack()

        Button(self.attributionTextWin, text=_("Back"),
               command=partial(self.winSwitch, self.attributionTextWin, self.attributionWin)).pack()
        self.attributionTextWin.mainloop()


    def loadGame(self, saveSlot):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        self.saveSlot = saveSlot
        if self.saveSlot == 1 and self.slot1Character == "":
            messagebox.showinfo("", _("This save slot is empty and cannot be loaded."))
            return

        if self.saveSlot == 2 and self.slot2Character == "":
            messagebox.showinfo("", _("This save slot is empty and cannot be loaded."))
            return

        if self.saveSlot == 3 and self.slot3Character == "":
            messagebox.showinfo("", _("This save slot is empty and cannot be loaded."))
            return

        if self.saveSlot == 4 and self.slot4Character == "":
            messagebox.showinfo("", _("This save slot is empty and cannot be loaded."))
            return

        if self.saveSlot == 5 and self.slot5Character == "":
            messagebox.showinfo("", _("This save slot is empty and cannot be loaded."))
            return

        if self.saveSlot == 6 and self.slot6Character == "":
            messagebox.showinfo("", _("This save slot is empty and cannot be loaded."))
            return


        self.character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-{}.p".format(self.saveSlot)), "rb"))
        self.ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-{}.p".format(self.saveSlot)), "rb"))
        self.exp = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "exp-{}.p".format(self.saveSlot)), "rb"))
        self.level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-{}.p".format(self.saveSlot)), "rb"))
        self.upgradePoints = pickle.load(
            open(os.path.join(SAVE_SLOTS_DIR, "upgradePoints-{}.p".format(self.saveSlot)), "rb"))
        self.perception = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "perception-{}.p".format(self.saveSlot)), "rb"))
        self.luck = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "luck-{}.p".format(self.saveSlot)), "rb"))
        self.chivalry = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "chivalry-{}.p".format(self.saveSlot)), "rb"))

        self.status = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "status-{}.p".format(self.saveSlot)), "rb"))
        self.gameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-{}.p".format(self.saveSlot)), "rb"))
        self.taskProgressDic = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "taskProgressDic-{}.p".format(self.saveSlot)), "rb"))

        self.beta_testing_fix()

        self.gameDate = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameDate-{}.p".format(self.saveSlot)), "rb"))
        self.sml = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "sml-{}.p".format(self.saveSlot)), "rb"))
        self.sml_checked = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "sml_checked-{}.p".format(self.saveSlot)), "rb"))
        self.skills = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "skills-{}.p".format(self.saveSlot)), "rb"))
        self.inv = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "inv-{}.p".format(self.saveSlot)), "rb"))
        self.equipment = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "equipment-{}.p".format(self.saveSlot)), "rb"))

        self.you = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "you-{}.p".format(self.saveSlot)), "rb"))

        self.health = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "health-{}.p".format(self.saveSlot)), "rb"))
        self.healthMax = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "healthMax-{}.p".format(self.saveSlot)), "rb"))
        self.stamina = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "stamina-{}.p".format(self.saveSlot)), "rb"))
        self.staminaMax = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "staminaMax-{}.p".format(self.saveSlot)), "rb"))

        self.attack = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "attack-{}.p".format(self.saveSlot)), "rb"))
        self.strength = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "strength-{}.p".format(self.saveSlot)), "rb"))
        self.speed = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "speed-{}.p".format(self.saveSlot)), "rb"))
        self.defence = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "defence-{}.p".format(self.saveSlot)), "rb"))
        self.dexterity = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "dexterity-{}.p".format(self.saveSlot)), "rb"))

        self.continueGameWin.withdraw()
        self.generateGameWin()


    def chooseSaveSlot(self, fromWin):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.quickSave.get():
            self.saveGame()
            return

        fromWin.withdraw()

        self.saveGameWin = Toplevel(fromWin)
        self.saveGameWin.title(_("Save Game"))

        try:
            self.slot1Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-1.p"), "rb"))
            self.slot1Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-1.p"), "rb"))
            self.slot1GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-1.p"), "rb"))
            self.slot1GameDate = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameDate-1.p"), "rb"))
            self.slot1ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-1.p"), "rb"))
        except:
            self.slot1Character = ""

        try:
            self.slot2Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-2.p"), "rb"))
            self.slot2Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-2.p"), "rb"))
            self.slot2GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-2.p"), "rb"))
            self.slot2GameDate = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameDate-2.p"), "rb"))
            self.slot2ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-2.p"), "rb"))
        except:
            self.slot2Character = ""


        try:
            self.slot3Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-3.p"), "rb"))
            self.slot3Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-3.p"), "rb"))
            self.slot3GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-3.p"), "rb"))
            self.slot3GameDate = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameDate-3.p"), "rb"))
            self.slot3ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-3.p"), "rb"))
        except:
            self.slot3Character = ""


        try:
            self.slot4Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-4.p"), "rb"))
            self.slot4Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-4.p"), "rb"))
            self.slot4GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-4.p"), "rb"))
            self.slot4GameDate = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameDate-4.p"), "rb"))
            self.slot4ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-4.p"), "rb"))
        except:
            self.slot4Character = ""

        try:
            self.slot5Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-5.p"), "rb"))
            self.slot5Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-5.p"), "rb"))
            self.slot5GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-5.p"), "rb"))
            self.slot5GameDate = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameDate-5.p"), "rb"))
            self.slot5ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-5.p"), "rb"))
        except:
            self.slot5Character = ""


        try:
            self.slot6Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-6.p"), "rb"))
            self.slot6Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-6.p"), "rb"))
            self.slot6GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-6.p"), "rb"))
            self.slot6GameDate = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameDate-6.p"), "rb"))
            self.slot6ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-6.p"), "rb"))
        except:
            self.slot6Character = ""


        Label(self.saveGameWin, text=_("Pick a save slot to save the game. To enable quick save, change the game settings.")).grid(row=0, column=1)

        F1 = Frame(self.saveGameWin, borderwidth=2, relief=SOLID)
        F1.grid(row=1, column=0)

        F2 = Frame(self.saveGameWin, borderwidth=2, relief=SOLID)
        F2.grid(row=1, column=1)

        F3 = Frame(self.saveGameWin, borderwidth=2, relief=SOLID)
        F3.grid(row=1, column=2)

        Label(self.saveGameWin, text="").grid(row=2, column=1)

        F4 = Frame(self.saveGameWin, borderwidth=2, relief=SOLID)
        F4.grid(row=3, column=0)

        F5 = Frame(self.saveGameWin, borderwidth=2, relief=SOLID)
        F5.grid(row=3, column=1)

        F6 = Frame(self.saveGameWin, borderwidth=2, relief=SOLID)
        F6.grid(row=3, column=2)

        Button(self.saveGameWin, text=_("Back"),
               command=partial(self.winSwitch, self.saveGameWin, fromWin)).grid(row=4, column=1)

        if self.slot1Character == "":
            pic1 = PhotoImage(file="BlankSlot.gif")
            imageButton1 = Button(F1, command=partial(self.saveGame, 1, fromWin))
            imageButton1.pack()
            imageButton1.config(image=pic1)
            Label(F1, text=_("Empty Slot")).pack()

        else:

            pic1 = PhotoImage(file="you.ppm")
            imageButton1 = Button(F1, command=partial(self.saveGame, 1, fromWin))
            imageButton1.pack()
            imageButton1.config(image=pic1)
            Label(F1, text=_("{}\nLevel {}").format(self.slot1Character, self.slot1Level)).pack()

            hours = self.slot1GameClock // 3600
            remainder = self.slot1GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot1ptc)
            self.balloon.bind(imageButton1, _("Total playing time:") + " " + tpt_string + playthru_string)

        if self.slot2Character == "":
            pic2 = PhotoImage(file="BlankSlot.gif")
            imageButton2 = Button(F2, command=partial(self.saveGame, 2, fromWin))
            imageButton2.pack()
            imageButton2.config(image=pic2)
            Label(F2, text=_("Empty Slot")).pack()

        else:

            pic2 = PhotoImage(file="you.ppm")
            imageButton2 = Button(F2, command=partial(self.saveGame, 2, fromWin))
            imageButton2.pack()
            imageButton2.config(image=pic2)
            Label(F2, text=_("{}\nLevel {}").format(self.slot2Character, self.slot2Level)).pack()

            hours = self.slot2GameClock // 3600
            remainder = self.slot2GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot2ptc)
            self.balloon.bind(imageButton2, _("Total playing time:") + " " + tpt_string + playthru_string)

        if self.slot3Character == "":
            pic3 = PhotoImage(file="BlankSlot.gif")
            imageButton3 = Button(F3, command=partial(self.saveGame, 3, fromWin))
            imageButton3.pack()
            imageButton3.config(image=pic3)
            Label(F3, text=_("Empty Slot")).pack()

        else:

            pic3 = PhotoImage(file="you.ppm")
            imageButton3 = Button(F3, command=partial(self.saveGame, 3, fromWin))
            imageButton3.pack()
            imageButton3.config(image=pic3)
            Label(F3, text=_("{}\nLevel {}").format(self.slot3Character, self.slot3Level)).pack()

            hours = self.slot3GameClock // 3600
            remainder = self.slot3GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot3ptc)
            self.balloon.bind(imageButton3, _("Total playing time:") + " " + tpt_string + playthru_string)


        if self.slot4Character == "":
            pic4 = PhotoImage(file="BlankSlot.gif")
            imageButton4 = Button(F4, command=partial(self.saveGame, 4, fromWin))
            imageButton4.pack()
            imageButton4.config(image=pic4)
            Label(F4, text=_("Empty Slot")).pack()

        else:

            pic4 = PhotoImage(file="you.ppm")
            imageButton4 = Button(F4, command=partial(self.saveGame, 4, fromWin))
            imageButton4.pack()
            imageButton4.config(image=pic4)
            Label(F4, text=_("{}\nLevel {}").format(self.slot4Character, self.slot4Level)).pack()

            hours = self.slot4GameClock // 3600
            remainder = self.slot4GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot4ptc)
            self.balloon.bind(imageButton4, _("Total playing time:") + " " + tpt_string + playthru_string)


        if self.slot5Character == "":
            pic5 = PhotoImage(file="BlankSlot.gif")
            imageButton5 = Button(F5, command=partial(self.saveGame, 5, fromWin))
            imageButton5.pack()
            imageButton5.config(image=pic5)
            Label(F5, text=_("Empty Slot")).pack()

        else:

            pic5 = PhotoImage(file="you.ppm")
            imageButton5 = Button(F5, command=partial(self.saveGame, 5, fromWin))
            imageButton5.pack()
            imageButton5.config(image=pic5)
            Label(F5, text=_("{}\nLevel {}").format(self.slot5Character, self.slot5Level)).pack()

            hours = self.slot5GameClock // 3600
            remainder = self.slot5GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot5ptc)
            self.balloon.bind(imageButton5, _("Total playing time:") + " " + tpt_string + playthru_string)


        if self.slot6Character == "":
            pic6 = PhotoImage(file="BlankSlot.gif")
            imageButton6 = Button(F6, command=partial(self.saveGame, 6, fromWin))
            imageButton6.pack()
            imageButton6.config(image=pic6)
            Label(F6, text=_("Empty Slot")).pack()

        else:

            pic6 = PhotoImage(file="you.ppm")
            imageButton6 = Button(F6, command=partial(self.saveGame, 6, fromWin))
            imageButton6.pack()
            imageButton6.config(image=pic6)
            Label(F6, text=_("{}\nLevel {}").format(self.slot6Character, self.slot6Level)).pack()

            hours = self.slot6GameClock // 3600
            remainder = self.slot6GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot6ptc)
            self.balloon.bind(imageButton6, _("Total playing time:") + " " + tpt_string + playthru_string)

        self.saveGameWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.saveGameWin.update_idletasks()
        toplevel_w, toplevel_h = self.saveGameWin.winfo_width(), self.saveGameWin.winfo_height()
        self.saveGameWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.saveGameWin.focus_force()
        self.saveGameWin.mainloop()###


    def saveGame(self, picked_slot=None, fromWin=None, auto_save=False):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        dd = datetime.now() - self.gameInitiatedTimestamp
        self.gameClock += dd.seconds

        if not self.quickSave.get() and not auto_save:
            targ_pickle_path = os.path.join(SAVE_SLOTS_DIR, "character-{}.p".format(picked_slot))
            if os.path.exists(targ_pickle_path):
                response = messagebox.askquestion("", _("You have chosen to save the game on a non-empty save slot. This will erase all previous data from this save slot. Are you sure you wish to continue?"))
                if response == "yes":
                    pass
                else:
                    return


        if not picked_slot:
            picked_slot = self.saveSlot

        pickle.dump(self.ptc, open(os.path.join(SAVE_SLOTS_DIR, "ptc-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.character, open(os.path.join(SAVE_SLOTS_DIR, "character-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.health, open(os.path.join(SAVE_SLOTS_DIR, "health-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.healthMax, open(os.path.join(SAVE_SLOTS_DIR, "healthMax-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.stamina, open(os.path.join(SAVE_SLOTS_DIR, "stamina-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.staminaMax, open(os.path.join(SAVE_SLOTS_DIR, "staminaMax-{}.p".format(picked_slot)), "wb"))

        pickle.dump(self.attack, open(os.path.join(SAVE_SLOTS_DIR, "attack-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.strength, open(os.path.join(SAVE_SLOTS_DIR, "strength-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.speed, open(os.path.join(SAVE_SLOTS_DIR, "speed-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.defence, open(os.path.join(SAVE_SLOTS_DIR, "defence-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.dexterity, open(os.path.join(SAVE_SLOTS_DIR, "dexterity-{}.p".format(picked_slot)), "wb"))

        pickle.dump(self.exp, open(os.path.join(SAVE_SLOTS_DIR, "exp-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.level, open(os.path.join(SAVE_SLOTS_DIR, "level-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.perception, open(os.path.join(SAVE_SLOTS_DIR, "perception-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.luck, open(os.path.join(SAVE_SLOTS_DIR, "luck-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.chivalry, open(os.path.join(SAVE_SLOTS_DIR, "chivalry-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.upgradePoints,
                    open(os.path.join(SAVE_SLOTS_DIR, "upgradePoints-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.status, open(os.path.join(SAVE_SLOTS_DIR, "status-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.gameClock, open(os.path.join(SAVE_SLOTS_DIR, "gameClock-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.gameDate, open(os.path.join(SAVE_SLOTS_DIR, "gameDate-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.taskProgressDic,
                    open(os.path.join(SAVE_SLOTS_DIR, "taskProgressDic-{}.p".format(picked_slot)), "wb"))

        pickle.dump(self.sml, open(os.path.join(SAVE_SLOTS_DIR, "sml-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.sml_checked, open(os.path.join(SAVE_SLOTS_DIR, "sml_checked-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.skills, open(os.path.join(SAVE_SLOTS_DIR, "skills-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.inv, open(os.path.join(SAVE_SLOTS_DIR, "inv-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.equipment, open(os.path.join(SAVE_SLOTS_DIR, "equipment-{}.p".format(picked_slot)), "wb"))
        pickle.dump(self.you, open(os.path.join(SAVE_SLOTS_DIR, "you-{}.p".format(picked_slot)), "wb"))

        if fromWin:
            self.winSwitch(self.saveGameWin, fromWin)
        self.gameInitiatedTimestamp = datetime.now()
        if not auto_save:
            messagebox.showinfo(_("Message"), _("Game saved."))

    ################### GAME ###################
    ################### GAME ###################
    ################### GAME ###################
    ################### GAME ###################
    ################### GAME ###################

    def startNewGame(self):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        self.mainWin.withdraw()
        self.startNewGameWin = Toplevel(self.mainWin)
        self.startNewGameWin.title(_("New Game"))

        try:
            self.slot1Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-1.p"), "rb"))
            self.slot1Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-1.p"), "rb"))
        except:
            self.slot1Character = ""

        try:
            self.slot2Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-2.p"), "rb"))
            self.slot2Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-2.p"), "rb"))
        except:
            self.slot2Character = ""

        try:
            self.slot3Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-3.p"), "rb"))
            self.slot3Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-3.p"), "rb"))
        except:
            self.slot3Character = ""

        try:
            self.slot4Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-4.p"), "rb"))
            self.slot4Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-4.p"), "rb"))
        except:
            self.slot4Character = ""

        try:
            self.slot5Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-5.p"), "rb"))
            self.slot5Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-5.p"), "rb"))
        except:
            self.slot5Character = ""

        try:
            self.slot6Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-6.p"), "rb"))
            self.slot6Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-6.p"), "rb"))
        except:
            self.slot6Character = ""

        Label(self.startNewGameWin, text=_("Pick a save slot to start a new game.")).grid(row=0, column=1)

        F1 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F1.grid(row=1, column=0)

        F2 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F2.grid(row=1, column=1)

        F3 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F3.grid(row=1, column=2)

        Label(self.startNewGameWin, text="").grid(row=2, column=1)

        F4 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F4.grid(row=3, column=0)

        F5 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F5.grid(row=3, column=1)

        F6 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F6.grid(row=3, column=2)


        Button(self.startNewGameWin, text=_("Back"),
               command=partial(self.winSwitch, self.startNewGameWin, self.mainWin)).grid(row=4, column=1)

        if self.slot1Character == "":
            pic1 = PhotoImage(file="BlankSlot.gif")
            imageButton1 = Button(F1, command=partial(self.intro, 1))
            imageButton1.pack()
            imageButton1.config(image=pic1)
            Label(F1, text=_("Empty Slot")).pack()

        else:
            pic1 = PhotoImage(file="you.ppm")
            imageButton1 = Button(F1, command=partial(self.intro, 1))
            imageButton1.pack()
            imageButton1.config(image=pic1)
            Label(F1, text=_("{}\nLevel {}").format(self.slot1Character, self.slot1Level)).pack()

        if self.slot2Character == "":
            pic2 = PhotoImage(file="BlankSlot.gif")
            imageButton2 = Button(F2, command=partial(self.intro, 2))
            imageButton2.pack()
            imageButton2.config(image=pic2)
            Label(F2, text=_("Empty Slot")).pack()

        else:
            pic2 = PhotoImage(file="you.ppm")
            imageButton2 = Button(F2, command=partial(self.intro, 2))
            imageButton2.pack()
            imageButton2.config(image=pic2)
            Label(F2, text=_("{}\nLevel {}").format(self.slot2Character, self.slot2Level)).pack()

        if self.slot3Character == "":
            pic3 = PhotoImage(file="BlankSlot.gif")
            imageButton3 = Button(F3, command=partial(self.intro, 3))
            imageButton3.pack()
            imageButton3.config(image=pic3)
            Label(F3, text=_("Empty Slot")).pack()

        else:
            pic3 = PhotoImage(file="you.ppm")
            imageButton3 = Button(F3, command=partial(self.intro, 3))
            imageButton3.pack()
            imageButton3.config(image=pic3)
            Label(F3, text=_("{}\nLevel {}").format(self.slot3Character, self.slot3Level)).pack()

        if self.slot4Character == "":
            pic4 = PhotoImage(file="BlankSlot.gif")
            imageButton4 = Button(F4, command=partial(self.intro, 4))
            imageButton4.pack()
            imageButton4.config(image=pic4)
            Label(F4, text=_("Empty Slot")).pack()

        else:
            pic4 = PhotoImage(file="you.ppm")
            imageButton4 = Button(F4, command=partial(self.intro, 4))
            imageButton4.pack()
            imageButton4.config(image=pic4)
            Label(F4, text=_("{}\nLevel {}").format(self.slot4Character, self.slot4Level)).pack()

        if self.slot5Character == "":
            pic5 = PhotoImage(file="BlankSlot.gif")
            imageButton5 = Button(F5, command=partial(self.intro, 5))
            imageButton5.pack()
            imageButton5.config(image=pic5)
            Label(F5, text=_("Empty Slot")).pack()

        else:
            pic5 = PhotoImage(file="you.ppm")
            imageButton5 = Button(F5, command=partial(self.intro, 5))
            imageButton5.pack()
            imageButton5.config(image=pic5)
            Label(F5, text=_("{}\nLevel {}").format(self.slot5Character, self.slot5Level)).pack()


        if self.slot6Character == "":
            pic6 = PhotoImage(file="BlankSlot.gif")
            imageButton6 = Button(F6, command=partial(self.intro, 6))
            imageButton6.pack()
            imageButton6.config(image=pic6)
            Label(F6, text=_("Empty Slot")).pack()

        else:
            pic6 = PhotoImage(file="you.ppm")
            imageButton6 = Button(F6, command=partial(self.intro, 6))
            imageButton6.pack()
            imageButton6.config(image=pic6)
            Label(F6, text=_("{}\nLevel {}").format(self.slot6Character, self.slot6Level)).pack()


        self.startNewGameWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.startNewGameWin.update_idletasks()
        toplevel_w, toplevel_h = self.startNewGameWin.winfo_width(), self.startNewGameWin.winfo_height()
        self.startNewGameWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.startNewGameWin.focus_force()
        self.startNewGameWin.mainloop()###


    def continueGame(self):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        self.mainWin.withdraw()

        self.continueGameWin = Toplevel(self.mainWin)
        self.continueGameWin.title(_("Continue Game"))

        try:
            self.slot1Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-1.p"), "rb"))
            self.slot1Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-1.p"), "rb"))
            self.slot1GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-1.p"), "rb"))
            self.slot1ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-1.p"), "rb"))
        except:
            self.slot1Character = ""

        try:
            self.slot2Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-2.p"), "rb"))
            self.slot2Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-2.p"), "rb"))
            self.slot2GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-2.p"), "rb"))
            self.slot2ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-2.p"), "rb"))
        except:
            self.slot2Character = ""


        try:
            self.slot3Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-3.p"), "rb"))
            self.slot3Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-3.p"), "rb"))
            self.slot3GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-3.p"), "rb"))
            self.slot3ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-3.p"), "rb"))
        except:
            self.slot3Character = ""


        try:
            self.slot4Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-4.p"), "rb"))
            self.slot4Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-4.p"), "rb"))
            self.slot4GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-4.p"), "rb"))
            self.slot4ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-4.p"), "rb"))
        except:
            self.slot4Character = ""

        try:
            self.slot5Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-5.p"), "rb"))
            self.slot5Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-5.p"), "rb"))
            self.slot5GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-5.p"), "rb"))
            self.slot5ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-5.p"), "rb"))
        except:
            self.slot5Character = ""


        try:
            self.slot6Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-6.p"), "rb"))
            self.slot6Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-6.p"), "rb"))
            self.slot6GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-6.p"), "rb"))
            self.slot6ptc = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "ptc-6.p"), "rb"))
        except:
            self.slot6Character = ""


        Label(self.continueGameWin, text=_("Pick a save slot to load.")).grid(row=0, column=1)

        F1 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F1.grid(row=1, column=0)

        F2 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F2.grid(row=1, column=1)

        F3 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F3.grid(row=1, column=2)

        Label(self.continueGameWin, text="").grid(row=2, column=1)

        F4 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F4.grid(row=3, column=0)

        F5 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F5.grid(row=3, column=1)

        F6 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F6.grid(row=3, column=2)

        Button(self.continueGameWin, text=_("Back"),
               command=partial(self.winSwitch, self.continueGameWin, self.mainWin, False)).grid(row=4, column=1)

        if self.slot1Character == "":
            pic1 = PhotoImage(file="BlankSlot.gif")
            imageButton1 = Button(F1, command=partial(self.loadGame, 1))
            imageButton1.pack()
            imageButton1.config(image=pic1)
            Label(F1, text=_("Empty Slot")).pack()

        else:

            pic1 = PhotoImage(file="you.ppm")
            imageButton1 = Button(F1, command=partial(self.loadGame, 1))
            imageButton1.pack()
            imageButton1.config(image=pic1)
            Label(F1, text=_("{}\nLevel {}").format(self.slot1Character, self.slot1Level)).pack()

            hours = self.slot1GameClock // 3600
            remainder = self.slot1GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot1ptc)
            self.balloon.bind(imageButton1, _("Total playing time:") + " " + tpt_string + playthru_string)

        if self.slot2Character == "":
            pic2 = PhotoImage(file="BlankSlot.gif")
            imageButton2 = Button(F2, command=partial(self.loadGame, 2))
            imageButton2.pack()
            imageButton2.config(image=pic2)
            Label(F2, text=_("Empty Slot")).pack()

        else:

            pic2 = PhotoImage(file="you.ppm")
            imageButton2 = Button(F2, command=partial(self.loadGame, 2))
            imageButton2.pack()
            imageButton2.config(image=pic2)
            Label(F2, text=_("{}\nLevel {}").format(self.slot2Character, self.slot2Level)).pack()

            hours = self.slot2GameClock // 3600
            remainder = self.slot2GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot2ptc)
            self.balloon.bind(imageButton2, _("Total playing time:") + " " + tpt_string + playthru_string)

        if self.slot3Character == "":
            pic3 = PhotoImage(file="BlankSlot.gif")
            imageButton3 = Button(F3, command=partial(self.loadGame, 3))
            imageButton3.pack()
            imageButton3.config(image=pic3)
            Label(F3, text=_("Empty Slot")).pack()

        else:

            pic3 = PhotoImage(file="you.ppm")
            imageButton3 = Button(F3, command=partial(self.loadGame, 3))
            imageButton3.pack()
            imageButton3.config(image=pic3)
            Label(F3, text=_("{}\nLevel {}").format(self.slot3Character, self.slot3Level)).pack()

            hours = self.slot3GameClock // 3600
            remainder = self.slot3GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot3ptc)
            self.balloon.bind(imageButton3, _("Total playing time:") + " " + tpt_string + playthru_string)


        if self.slot4Character == "":
            pic4 = PhotoImage(file="BlankSlot.gif")
            imageButton4 = Button(F4, command=partial(self.loadGame, 4))
            imageButton4.pack()
            imageButton4.config(image=pic4)
            Label(F4, text=_("Empty Slot")).pack()

        else:

            pic4 = PhotoImage(file="you.ppm")
            imageButton4 = Button(F4, command=partial(self.loadGame, 4))
            imageButton4.pack()
            imageButton4.config(image=pic4)
            Label(F4, text=_("{}\nLevel {}").format(self.slot4Character, self.slot4Level)).pack()

            hours = self.slot4GameClock // 3600
            remainder = self.slot4GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot4ptc)
            self.balloon.bind(imageButton4, _("Total playing time:") + " " + tpt_string + playthru_string)


        if self.slot5Character == "":
            pic5 = PhotoImage(file="BlankSlot.gif")
            imageButton5 = Button(F5, command=partial(self.loadGame, 5))
            imageButton5.pack()
            imageButton5.config(image=pic5)
            Label(F5, text=_("Empty Slot")).pack()

        else:

            pic5 = PhotoImage(file="you.ppm")
            imageButton5 = Button(F5, command=partial(self.loadGame, 5))
            imageButton5.pack()
            imageButton5.config(image=pic5)
            Label(F5, text=_("{}\nLevel {}").format(self.slot5Character, self.slot5Level)).pack()

            hours = self.slot5GameClock // 3600
            remainder = self.slot5GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot5ptc)
            self.balloon.bind(imageButton5, _("Total playing time:") + " " + tpt_string + playthru_string)




        if self.slot6Character == "":
            pic6 = PhotoImage(file="BlankSlot.gif")
            imageButton6 = Button(F6, command=partial(self.loadGame, 6))
            imageButton6.pack()
            imageButton6.config(image=pic6)
            Label(F6, text=_("Empty Slot")).pack()

        else:

            pic6 = PhotoImage(file="you.ppm")
            imageButton6 = Button(F6, command=partial(self.loadGame, 6))
            imageButton6.pack()
            imageButton6.config(image=pic6)
            Label(F6, text=_("{}\nLevel {}").format(self.slot6Character, self.slot6Level)).pack()

            hours = self.slot6GameClock // 3600
            remainder = self.slot6GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} hours {} minutes {} seconds").format(hours, minutes, seconds)
            playthru_string = _("\nPlay-thrus completed: {}").format(self.slot6ptc)
            self.balloon.bind(imageButton6, _("Total playing time:") + " " + tpt_string + playthru_string)


        self.continueGameWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.continueGameWin.update_idletasks()
        toplevel_w, toplevel_h = self.continueGameWin.winfo_width(), self.continueGameWin.winfo_height()
        self.continueGameWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.continueGameWin.focus_force()
        self.continueGameWin.mainloop()###


    def intro(self, saveSlot):

        self.saveSlot = saveSlot
        if self.saveSlot == 1 and self.slot1Character != "":
            response = messagebox.askquestion("",_("You have chosen to start a new game on a non-empty save slot. This will erase all data from this save slot. Are you sure you wish to continue?"))
            if response == "yes":
                pass
            else:
                return

        if self.saveSlot == 2 and self.slot2Character != "":
            response = messagebox.askquestion("",_("You have chosen to start a new game on a non-empty save slot. This will erase all data from this save slot. Are you sure you wish to continue?"))
            if response == "yes":
                pass
            else:
                return

        if self.saveSlot == 3 and self.slot3Character != "":
            response = messagebox.askquestion("",_("You have chosen to start a new game on a non-empty save slot. This will erase all data from this save slot. Are you sure you wish to continue?"))
            if response == "yes":
                pass
            else:
                return

        if self.saveSlot == 4 and self.slot4Character != "":
            response = messagebox.askquestion("",_("You have chosen to start a new game on a non-empty save slot. This will erase all data from this save slot. Are you sure you wish to continue?"))
            if response == "yes":
                pass
            else:
                return

        if self.saveSlot == 5 and self.slot5Character != "":
            response = messagebox.askquestion("",_("You have chosen to start a new game on a non-empty save slot. This will erase all data from this save slot. Are you sure you wish to continue?"))
            if response == "yes":
                pass
            else:
                return

        if self.saveSlot == 6 and self.slot6Character != "":
            response = messagebox.askquestion("",_("You have chosen to start a new game on a non-empty save slot. This will erase all data from this save slot. Are you sure you wish to continue?"))
            if response == "yes":
                pass
            else:
                return

        self.startNewGameWin.withdraw()
        self.intro_quiz()


    def intro_quiz(self, transferred_items=None):

        self.stopSoundtrack()
        self.currentBGM = "jy_calm1.mp3"
        self.startSoundtrackThread()

        self.character = ""
        self.health = 120 + self.ptc*20
        self.healthMax = 120  + self.ptc*20
        self.stamina = 120  + self.ptc*20
        self.staminaMax = 120  + self.ptc*20
        self.attack = randrange(48, 56) + self.ptc*3
        self.strength = randrange(48, 56) + self.ptc*3
        self.speed = randrange(48, 56) + self.ptc*3
        self.defence = randrange(48, 56) + self.ptc*3
        self.dexterity = randrange(48, 56) + self.ptc*3
        self.luck = 50 + self.ptc*10
        self.perception = 50 + + self.ptc*10
        self.chivalry = 0
        self.upgradePoints = 0
        self.inv = {_("Gold"): 1000*(self.ptc+1),
                    _("Iron Dagger"): 1}
        if "Beta" in VERSION:
            self.inv[_("Explorer Ring")] = 1
            self.inv[_("Explorer Boots")] = 1
        if transferred_items:
            for item in transferred_items:
                self.add_item(item, transferred_items[item])
        self.equipment = []

        # Intro quiz
        self.quiz_questions = [[_("Welcome to Wuxia World! Before we begin,\n please answer a few questions..."), []]]
        self.quiz_questions.append([_("What is your name?"), []])
        self.quiz_questions.append([_("Which of the following colors do you like most?"),
                                    [_("Black"), _("Blue"), _("Green"), _("Red"), _("Yellow")]])
        self.quiz_questions.append([_("Which of the following flowers do you like most?"),
                                    [_("Rose"), _("Lily"), _("Violet"), _("Sunflower"), _("I don't like flowers")]])
        self.quiz_questions.append([_("What do you think the creator of this game is\nlike in real life?"),
                                    [_("An introverted homebody"), _("A hero that roams the world"),
                                     _("A young, beautiful lady"), _("A rich, middle-aged man"),
                                     _("A sinner who needs Jesus")]])
        self.quiz_questions.append([_("What are you most likely to do in a fight?"),
                                    [_("Attack without mercy"), _("Defend"),
                                     _("Run away"), _("Depends on the situation"),
                                     _("Seek truce by treating the enemy to a meal")]])
        self.quiz_questions.append([_("Do you enjoy Jin Yong novels?"),
                                    [_("Love them!"), _("They're ok..."),
                                     _("Nope"), _("Who is Jin Yong?"),
                                     _("I only watch the TV shows.")]])
        self.quiz_questions.append([_("If one day, you become a top-tier martial artist,\nit's most likely because"),
                                    [_("You are a quick learner."),
                                     _("You spent much more time training than everyone else."),
                                     _("You have money and connections."),
                                     _("You got lucky and received guidance from a grandmaster."),
                                     _("You sabotaged everyone who got in your way.")]])

        if transferred_items:
            self.quiz_win = Toplevel(self.mainWin)
        else:
            self.quiz_win = Toplevel(self.startNewGameWin)
        self.quiz_win.title("")
        self.quiz_win.geometry("800x400")
        self.question_label = Label(self.quiz_win,
                                    text="")
        self.question_label.pack()
        self.question_number = 0

        self.choices_frame = Frame(self.quiz_win, borderwidth=2, relief=SOLID)
        self.choices_frame.pack()
        self.quiz_choice_var = IntVar()

        self.submit_button = Button(self.quiz_win, text=_("Confirm"), command=self.parse_quiz_response)
        self.submit_button.pack()

        self.display_question()

        self.quiz_win.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.quiz_win.update_idletasks()
        toplevel_w, toplevel_h = self.quiz_win.winfo_width(), self.quiz_win.winfo_height()
        self.quiz_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, SCREEN_HEIGHT//2 - toplevel_h))
        self.quiz_win.focus_force()
        self.quiz_win.mainloop()###
        
        
    def display_question(self):
        # destroy any existing radiobuttons within choices frame
        for widget in self.choices_frame.winfo_children():
            widget.destroy()

        # load question and update question label
        self.quiz_q = self.quiz_questions[self.question_number][0]
        if os.name == "nt":
            self.question_label.config(text=self.quiz_q, font=("TkDefaultFont", 24))
        else:
            self.question_label.config(text=self.quiz_q, font=("TkDefaultFont", 32))

        if self.question_number > 1:
            # loop through choices and add radio button for each choice
            rb_index = 0
            for choice in self.quiz_questions[self.question_number][1]:
                if os.name == "nt":
                    Radiobutton(self.choices_frame, text=choice, font=("TkDefaultFont", 18),
                                variable=self.quiz_choice_var,
                                value=rb_index).grid(row=rb_index, column=0, sticky=W)
                else:
                    Radiobutton(self.choices_frame, text=choice, font=("TkDefaultFont", 24),
                                variable=self.quiz_choice_var, value=rb_index).grid(row=rb_index, column=0, sticky=W)

                rb_index += 1
                # self.quiz_choice_var.set(0)

        elif self.question_number == 1:
            self.user_input = Entry(self.choices_frame, width=12)
            self.user_input.pack()


    def parse_quiz_response(self):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.question_number < len(self.quiz_questions) - 1:
            if self.question_number == 0:
                pass
            elif self.question_number == 1:
                response = self.user_input.get()
                if len(response) > 12 or len(response) < 1:
                    messagebox.showinfo("", _("Please choose a name that is between 1 and 12 characters long."))
                    return
                self.character = response

                #Check for duplicate "Scrub" name
                if self.character.lower() == "scrub":
                    self.character = "Scrubwannabe"

            else:
                response = self.quiz_choice_var.get()
                if self.question_number == 2:
                    if response == 0:
                        self.attack += 1
                        self.strength += 1
                        self.speed += 1
                        self.defence += 1
                        self.dexterity += 1
                    elif response == 1:
                        self.attack += 5
                    elif response == 2:
                        self.defence += 5
                    elif response == 3:
                        self.strength += 5
                    elif response == 4:
                        self.dexterity += 5

                elif self.question_number == 3:
                    if response == 0:
                        self.health += 25
                        self.healthMax += 25
                    elif response == 1:
                        self.health += 15
                        self.healthMax += 15
                        self.stamina += 10
                        self.staminaMax += 10
                    elif response == 2:
                        self.stamina += 25
                        self.staminaMax += 25
                    elif response == 3:
                        self.luck += 25
                        self.attack += 1
                        self.strength += 1
                        self.speed += 1
                        self.defence += 1
                        self.dexterity += 1
                        self.health += 5
                        self.healthMax += 5
                        self.stamina += 5
                        self.staminaMax += 5
                    elif response == 4:
                        self.luck -= 10

                elif self.question_number == 4:
                    if response == 0:
                        self.health += 15
                        self.healthMax += 15
                        self.stamina += 10
                        self.staminaMax += 10
                        self.perception += 10
                    elif response == 1:
                        self.attack += 5
                        self.strength += 5
                    elif response == 2:
                        self.luck += 15
                    elif response == 3:
                        self.inv[_("Gold")] += 2000
                    elif response == 4:
                        self.perception += 30

                elif self.question_number == 5:
                    if response == 0:
                        self.attack += 5
                    elif response == 1:
                        self.defence += 5
                    elif response == 2:
                        self.speed += 5
                    elif response == 3:
                        self.attack += 1
                        self.strength += 1
                        self.speed += 1
                        self.defence += 1
                        self.dexterity += 1
                    elif response == 4:
                        self.chivalry += 50
                        self.inv[_("Gold")] += 100

                elif self.question_number == 6:
                    if response == 0:
                        self.luck += 10
                        self.perception += 5
                    elif response == 1:
                        pass
                    elif response == 2:
                        self.perception -= 5
                        self.inv[_("Gold")] -= 250
                    elif response == 3:
                        self.perception -= 5
                    elif response == 4:
                        self.inv[_("Gold")] += 250

                elif self.question_number == 7:
                    if response == 0:
                        self.perception += 5
                    elif response == 1:
                        self.upgradePoints += 10
                    elif response == 2:
                        self.inv[_("Gold")] += 2000
                    elif response == 3:
                        self.luck += 5
                    elif response == 4:
                        self.chivalry -= 20
                        self.health += 50
                        self.healthMax += 50
                        self.stamina += 50
                        self.staminaMax += 50
                        
            self.question_number += 1
            self.display_question()

        else:
            # Parse last question
            if self.question_number == len(self.quiz_questions) - 1:
                response = self.quiz_choice_var.get()
                if response == 0:
                    self.perception += 5
                elif response == 1:
                    self.upgradePoints += 10
                elif response == 2:
                    self.inv[_("Gold")] += 2000
                elif response == 3:
                    self.luck += 5
                elif response == 4:
                    self.chivalry -= 20
                    self.health += 50
                    self.healthMax += 50
                    self.stamina += 50
                    self.staminaMax += 50
            # End quiz
            self.quiz_win.destroy()
            # Declaring fixed starting attributes and saving all variables

            self.exp = 0
            self.level = 1
            self.status = _("Normal")
            self.gameClock = 0
            self.gameDate = 1

            self.taskProgressDic = {
                "join_sect": -1,
                "xiao_han_jia_wen": -1,
                "liu_village": -1,
                "imperial_palace":-1,
                "feng_xinyi":-1,
                "li_guiying":-1,
                "wu_hua_que":0,
                "wudang_task":0,
                "huashanlunjian":-1,
                "guo_zhiqiang": -1,
                "chivalry": 0,
                "ouyang_nana": -1,
                "ouyang_nana_xu_jun": -1,
                "return_to_phantom_sect": 0,
                "eagle_sect": -1,
                "wife_intimacy": -1,
                "luohanzhen": -1,
                "shinscrub": -1,
                "li_guiping_task": -1,
                "huang_yuwei": -1,
                "huang_yuwei_birthday": -1,
                "nongyuan_valley": -1,
                "babao_tower": -1,
                "potters_field": -1,
                "feng_pan": -1,
                "ma_guobao": -1,
                "small_town": -1,
                "small_town_ghost": -1,
                "yagyu_clan": -1,
                "yagyu_clan_join": -1,
                "yagyu_dungeon": 1,
                "blind_sniper": -1,
                "supreme_leader": -1,
                "huashan_tribute": -1,
                "wudang_tribute": -1,
                "shaolin_tribute": -1,
                "shanhu_sect_tribute": -1,
                "dragon_sect_tribute": -1,
                "phantom_sect_tribute": -1,
                "castrated": 0,
                "wanhua_pavillion": -1,
                "cai_wuqiong": -1,
                "a_xiu": - 1,
                "jiang_yuqi_liu_village_choice": -1,
                "big_beard": -1,
                "scrub_tutorial": -1,
            }

            self.sml = []
            self.sml.append(special_move(name=_("Scrub Fist")))
            self.sml.append(special_move(name=_("Rest")))
            self.sml.append(special_move(name=_("Items")))
            self.sml.append(special_move(name=_("Flee")))
            self.sml_checked = [_("Scrub Fist")]
            self.skills = []


            self.you = character(self.character, self.level, [self.health, self.healthMax,
                                                              self.stamina, self.staminaMax, self.attack,
                                                              self.strength, self.speed, self.defence,
                                                              self.dexterity], self.sml, {}, [], [],
                                 status=self.status)

            pickle.dump(self.you, open(os.path.join(SAVE_SLOTS_DIR, "you.p"), "wb"))

            pickle.dump(self.character, open(os.path.join(SAVE_SLOTS_DIR, "character.p"), "wb"))
            pickle.dump(self.health, open(os.path.join(SAVE_SLOTS_DIR, "health.p"), "wb"))
            pickle.dump(self.healthMax, open(os.path.join(SAVE_SLOTS_DIR, "healthMax.p"), "wb"))
            pickle.dump(self.stamina, open(os.path.join(SAVE_SLOTS_DIR, "stamina.p"), "wb"))
            pickle.dump(self.staminaMax, open(os.path.join(SAVE_SLOTS_DIR, "staminaMax.p"), "wb"))

            pickle.dump(self.attack, open(os.path.join(SAVE_SLOTS_DIR, "attack.p"), "wb"))
            pickle.dump(self.strength, open(os.path.join(SAVE_SLOTS_DIR, "strength.p"), "wb"))
            pickle.dump(self.speed, open(os.path.join(SAVE_SLOTS_DIR, "speed.p"), "wb"))
            pickle.dump(self.defence, open(os.path.join(SAVE_SLOTS_DIR, "defence.p"), "wb"))
            pickle.dump(self.dexterity, open(os.path.join(SAVE_SLOTS_DIR, "dexterity.p"), "wb"))

            pickle.dump(self.luck, open(os.path.join(SAVE_SLOTS_DIR, "luck.p"), "wb"))
            pickle.dump(self.perception, open(os.path.join(SAVE_SLOTS_DIR, "perception.p"), "wb"))
            pickle.dump(self.chivalry, open(os.path.join(SAVE_SLOTS_DIR, "chivalry.p"), "wb"))

            pickle.dump(self.exp, open(os.path.join(SAVE_SLOTS_DIR, "exp.p"), "wb"))
            pickle.dump(self.level, open(os.path.join(SAVE_SLOTS_DIR, "level.p"), "wb"))
            pickle.dump(self.upgradePoints, open(os.path.join(SAVE_SLOTS_DIR, "upgradePoints.p"), "wb"))
            pickle.dump(self.status, open(os.path.join(SAVE_SLOTS_DIR, "status.p"), "wb"))
            pickle.dump(self.gameClock, open(os.path.join(SAVE_SLOTS_DIR, "gameClock.p"), "wb"))
            pickle.dump(self.gameDate, open(os.path.join(SAVE_SLOTS_DIR, "gameDate.p"), "wb"))

            pickle.dump(self.sml, open(os.path.join(SAVE_SLOTS_DIR, "sml.p"), "wb"))
            pickle.dump(self.sml_checked, open(os.path.join(SAVE_SLOTS_DIR, "sml_checked.p"), "wb"))
            pickle.dump(self.skills, open(os.path.join(SAVE_SLOTS_DIR, "skills.p"), "wb"))
            pickle.dump(self.inv, open(os.path.join(SAVE_SLOTS_DIR, "inv.p"), "wb"))

            self.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("Welcome to Wuxia World! My name is Scrub."),
                 _("This is a virtual Wuxia world created by me."),
                 _("Here, you can interact with many renowned martial artists and learn a variety of moves/techniques."),
                 _("Perhaps one day, you can become a martial arts master yourself!"),
                 _("Right now, you are at my house. You can treat it as your 'home base' in this game."),
                 _("As you roam the world and encounter different scenarios, you'll have choices to make, enemies to defeat, and friends (and maybe a wife) to recruit."),
                 _("You have the freedom to make honorable or dishonorable choices, but decisions do come with consequences so think carefully before you act."),
                 _("If you have any questions, just come talk to me, though it will definitely help to read the README file to understand some basics."),
                 _("Otherwise, you can learn while exploring on your own~"),
                 _("Whoaaaa, wait a second! You can't just bring me into a world where everyone knows martial arts without teaching me any moves!"),
                 _("I'll get bullied everywhere I go!"),
                 _("Don't worry! Everyone who's brought here automatically knows the Scrub Fist, a technique that I invented myself."), #Row 2
                 _("...... You're joking, right? Scrub Fist? That sounds awfully weak..."),
                 _("Hey, it's better than nothing. Don't worry; you'll have the opportunity to learn better moves later."),
                 _("Anyway, have fun!")
                 ],
                ["Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "you", "you",
                 "Scrub", "you", "Scrub", "Scrub"],
                bind_text="Left click anywhere in the window to advance the dialogue."
            )
            self.generateGameWin()


    def add_item(self, item_name, quantity=1):
        if item_name in self.inv:
            self.inv[item_name] += quantity
        else:
            self.inv[item_name] = quantity
        
        if self.inv[item_name] < 0:
            self.inv[item_name] = 0


    def check_for_item(self, item_name, quantity=1):
        if item_name in self.inv:
            if self.inv[item_name] >= quantity:
                return True
        return False


    def create_menu_frame(self, master, options=["Map", "Profile", "Inv", "Save", "Settings"]):

        menu_frame = Frame(master=master, borderwidth=2, relief=SUNKEN, padx=5, pady=5)

        if "Map" in options:
            map_icon = PhotoImage(file="map_icon.ppm")
            if master == self.scrubHouseWin:
                map_button = Button(menu_frame, image=map_icon, command=partial(self.map, master))
            else:
                map_button = Button(menu_frame, image=map_icon, command=partial(self.winSwitch, master, self.mapWin))
            map_button.image = map_icon
            map_button.grid(row=0, column=0)

            Label(menu_frame, text=_("Map")).grid(row=1, column=0)

            Label(menu_frame, text="  ").grid(row=0, column=1)

        if "Profile" in options:
            profile_icon = PhotoImage(file="profile_icon.ppm")
            profile_button = Button(menu_frame, image=profile_icon, command=partial(self.checkProfile, master))
            profile_button.image = profile_icon
            profile_button.grid(row=0, column=2)

            Label(menu_frame, text=_("Profile")).grid(row=1, column=2)

            Label(menu_frame, text="  ").grid(row=0, column=3)

        if "Inv" in options:
            inventory_icon = PhotoImage(file="inventory_icon.ppm")
            inventory_button = Button(menu_frame, image=inventory_icon, command=partial(self.checkInv, master))
            inventory_button.image = inventory_icon
            inventory_button.grid(row=0, column=4)

            Label(menu_frame, text=_("Inventory")).grid(row=1, column=4)

            Label(menu_frame, text="  ").grid(row=0, column=5)

        if "Save" in options:
            save_icon = PhotoImage(file="save_icon.ppm")
            save_button = Button(menu_frame, image=save_icon, command=partial(self.chooseSaveSlot, master))
            save_button.image = save_icon
            save_button.grid(row=0, column=6)

            Label(menu_frame, text=_("Save")).grid(row=1, column=6)

            Label(menu_frame, text="  ").grid(row=0, column=7)

        if "Settings" in options:
            settings_icon = PhotoImage(file="settings_icon.ppm")
            settings_button = Button(menu_frame, image=settings_icon, command=partial(self.settings, master))
            settings_button.image = settings_icon
            settings_button.grid(row=0, column=8)

            Label(menu_frame, text=_("Settings")).grid(row=1, column=8)

        return menu_frame


    def generateGameWin(self):

        self.gameInitiatedTimestamp = datetime.now()
        self.current_map_location = "scrub_house"
        self.partner = ""

        if self.taskProgressDic["join_sect"] not in (-1,100):
            if self.taskProgressDic["join_sect"] < 1:
                self.stopSoundtrack()
                self.currentBGM = "jy_pipayu.mp3"
                self.startSoundtrackThread()
                Scene_cherry_blossom_island_train(self)
                return
            elif self.taskProgressDic["join_sect"] < 2:
                self.stopSoundtrack()
                self.currentBGM = "jy_ambient3.mp3"
                self.startSoundtrackThread()
                Scene_shanhu_sect_train(self)
                return
            elif self.taskProgressDic["join_sect"] < 3:
                self.stopSoundtrack()
                self.currentBGM = "jy_huanle.mp3"
                self.startSoundtrackThread()
                Scene_nongyuan_valley_train(self)
                return
            elif self.taskProgressDic["join_sect"] < 4:
                self.stopSoundtrack()
                self.currentBGM = "kindaichi 3.mp3"
                self.startSoundtrackThread()
                Scene_phantom_sect_train(self)
                return
            elif self.taskProgressDic["join_sect"] < 5:
                self.stopSoundtrack()
                self.currentBGM = "jy_shaolin1.mp3"
                self.startSoundtrackThread()
                Scene_shaolin_train(self)
                return
            elif self.taskProgressDic["join_sect"] < 6:
                self.stopSoundtrack()
                self.currentBGM = "jy_wudang.mp3"
                self.startSoundtrackThread()
                Scene_wudang_train(self)
                return
            elif self.taskProgressDic["join_sect"] < 7:
                self.stopSoundtrack()
                self.currentBGM = "jy_shendiaozhuti.mp3"
                self.startSoundtrackThread()
                Scene_huashan_train(self)
                return
            
        if self.taskProgressDic["ouyang_nana_xu_jun"] in range(10,21):
            self.jiangnan_forest(0)


        self.stopSoundtrack()
        self.currentBGM = "jy_calm2.mp3"
        self.startSoundtrackThread()
        self.create_self_character()

        self.scrubHouseWin = Toplevel(self.mainWin)
        self.scrubHouseWin.title(_("Scrub's House"))

        menu = Menu(self.scrubHouseWin)
        self.scrubHouseWin.config(menu=menu)
        sub_menu = Menu(menu)

        menu.add_cascade(label="Admin Options", menu=sub_menu)
        sub_menu.add_command(label="Cheat Code", command=self.cheatCode)
        sub_menu.add_separator()


        bg = PhotoImage(file="scrub_house.ppm")
        w = bg.width()
        h = bg.height()

        self.scrubHouseCanvas = Canvas(self.scrubHouseWin, width=w, height=h)
        self.scrubHouseCanvas .pack()
        self.scrubHouseCanvas .create_image(0, 0, anchor=NW, image=bg)

        self.update_scrub_house_win()

        self.ScrubHouseF3 = Frame(self.scrubHouseWin)
        self.ScrubHouseF3.pack()

        menu_frame = self.create_menu_frame(master=self.scrubHouseWin)
        menu_frame.pack()

        Button(self.scrubHouseWin, text=_("Back to Main Menu"),
               command=partial(self.winSwitch, self.scrubHouseWin, self.mainWin)).pack()

        self.scrubHouseWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.scrubHouseWin.update_idletasks()
        toplevel_w, toplevel_h = self.scrubHouseWin.winfo_width(), self.scrubHouseWin.winfo_height()
        self.scrubHouseWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.scrubHouseWin.focus_force()
        self.scrubHouseWin.mainloop()###


    def update_scrub_house_win(self):

        bg = PhotoImage(file="scrub_house.ppm")
        w = bg.width()
        h = bg.height()
        
        condition_1 = self.taskProgressDic["join_sect"] == 401
        condition_2 = self.taskProgressDic["ouyang_nana"] == 100
        condition_3 = self.taskProgressDic["eagle_sect"] >= 11 and self.taskProgressDic["eagle_sect"] != 1000 and self.taskProgressDic["wife_intimacy"] == -1
        condition_4 = self.taskProgressDic["a_xiu"] >= 0 and self.taskProgressDic["wife_intimacy"] == -1

        self.scrubHousePhotoList = []
        if self.taskProgressDic["wife_intimacy"] >= 0 or condition_3 or condition_4:
            try:
                self.ScrubHouseF1.place(x=-3000, y=-3000)
                self.ScrubHouseF1.destroy()
                self.ScrubHouseF1 = Frame(self.scrubHouseCanvas)
            except:
                self.ScrubHouseF1 = Frame(self.scrubHouseCanvas)

            if self.taskProgressDic["blind_sniper"] not in [0,1]:
                pic1 = PhotoImage(file="Scrub_icon_large.ppm")
                imageButton1 = Button(self.ScrubHouseF1, command=self.clickedOnScrub)
                imageButton1.grid(row=0, column=0)
                imageButton1.config(image=pic1)
                label = Label(self.ScrubHouseF1, text="Scrub")
                label.grid(row=1, column=0)
                self.ScrubHouseF1.place(x=w * 3 // 4 - pic1.width() // 2, y=h//2 - pic1.height()//2)
                self.scrubHousePhotoList.append(pic1)

            if condition_1:
                self.partner = "Li Guiping"
            elif condition_2:
                self.partner = "Ouyang Nana"
            elif condition_3 or self.taskProgressDic["eagle_sect"] == 100:
                self.partner = "Jiang Yuqi"
            elif self.taskProgressDic["a_xiu"] >= 0:
                self.partner = "A-Xiu"
            else:
                self.partner = ""
                
            self.ScrubHouseF2 = Frame(self.scrubHouseCanvas)
            pic2 = PhotoImage(file="{}_icon_large.ppm".format(self.partner))
            imageButton2 = Button(self.ScrubHouseF2, command=self.clickedOnPartner)
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            label = Label(self.ScrubHouseF2, text=self.partner)
            label.grid(row=1, column=0)
            self.ScrubHouseF2.place(x=w // 4 - pic2.width() // 2, y=h//2 - pic2.height()//2)
            self.scrubHousePhotoList.append(pic2)

        else:
            try:
                self.ScrubHouseF1.place(x=-3000, y=-3000)
                self.ScrubHouseF1.destroy()
                self.ScrubHouseF1 = Frame(self.scrubHouseCanvas)
            except:
                self.ScrubHouseF1 = Frame(self.scrubHouseCanvas)
            if self.taskProgressDic["blind_sniper"] not in [0,1]:
                self.ScrubHouseF1 = Frame(self.scrubHouseCanvas)
                pic1 = PhotoImage(file="Scrub_icon_large.ppm")
                imageButton1 = Button(self.ScrubHouseF1, command=self.clickedOnScrub)
                imageButton1.grid(row=0, column=0)
                imageButton1.config(image=pic1)
                label = Label(self.ScrubHouseF1, text="Scrub")
                label.grid(row=1, column=0)
                self.ScrubHouseF1.place(x=w // 2 - pic1.width() // 2, y=h//2 - pic1.height()//2)
                self.scrubHousePhotoList.append(pic1)


    def clickedOnPartner(self):
        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        self.destroy_all(self.ScrubHouseF3)
        Button(self.ScrubHouseF3, text=_("Spar ({})").format(self.partner), command=self.sparWithPartner).grid(row=0,column=0)
        Button(self.ScrubHouseF3, text=_("Interact").format(self.partner), command=self.interactWithPartner).grid(row=1,column=0)
        Button(self.ScrubHouseF3, text=_("Give Gift").format(self.partner), command=self.giveGiftToPartner).grid(row=2,column=0)
        Button(self.ScrubHouseF3, text=_("Check Intimacy").format(self.partner), command=self.checkIntimacyPartner).grid(row=3,column=0)
        self.partner_intimate_activity_button = Button(self.ScrubHouseF3, text=_("Intimate Activity"), command=self.intimateActivityWithPartner)
        self.partner_intimate_activity_button.grid(row=4, column=0)
        
        self.reset_intimacy_button()


    def jiang_yuqi_liu_village_choice(self, choice):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        
        if choice == 1:

            self.stopSoundtrack()
            self.currentBGM = "jy_shendiaoshuhuan.mp3"
            self.startSoundtrackThread()
            self.taskProgressDic["wife_intimacy"] = -1
            self.taskProgressDic["eagle_sect"] = 1000
            self.partner = ""
            self.destroy_all(self.ScrubHouseF3)
            self.ScrubHouseF1.place(x=-3000, y=-3000)
            self.ScrubHouseF2.place(x=-3000, y=-3000)
            
            self.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("Your chivalry + 20."),
                 _("Yuqi... I have to tell you something... It's about your father..."),
                 _("You found him? Or have news of him??"),
                 _("I... yes, but..."),
                 _("*You tell Jiang Yuqi the whole story, beginning from the Liu Village massacre...*"),
                 _("No... This can't be true... No!! Tell me it's not true, {}...").format(self.character),
                 _("I'm sorry, but everything I told you is true."),
                 _("No! It can't be! How can you talk about my father like that??!! You're lying!"),
                 _("He's not that type of person!!! You must've made a mistake!"),
                 _("Yuqi, I understand that you're upset, but those are the facts..."), #Row 2
                 _("No!!! Enough! I've heard enough! I don't believe you! I'm going to find my dad on my own!"),
                 _("I thought you loved me and wanted to help me, but it turns out that I was wrong!"),
                 _("Good bye, {}...").format(self.character),
                 _("*Before you can respond, Jiang Yuqi storms off angrily...*")],
                ["Blank", "you", "Jiang Yuqi", "you", "Blank", "Jiang Yuqi", "you", "Jiang Yuqi", "Jiang Yuqi",
                 "you", "Jiang Yuqi", "Jiang Yuqi", "Jiang Yuqi", "Blank"]
            )

        self.return_to_scrub_house()


    def sparWithPartner(self):
        if self.taskProgressDic["wife_intimacy"] >= 0:
            self.taskProgressDic["wife_intimacy"] += 1
        elif self.taskProgressDic["eagle_sect"] >= 11 and self.taskProgressDic["eagle_sect"] != 1000:
            self.taskProgressDic["eagle_sect"] += 5
        elif self.taskProgressDic["a_xiu"] >= 0:
            self.taskProgressDic["a_xiu"] += 5
            
        if self.partner == "Li Guiping":
            opp = character(_("Li Guiping"), 18 + self.taskProgressDic["wife_intimacy"]//100,
                            sml=[special_move(_("Poison Needle"), level=10),
                                 special_move(_("Phantom Claws"), level=7)],
                            skills=[skill(_("Phantom Steps"), level=8)])
        elif self.partner == "Ouyang Nana":
            opp = character(_("Ouyang Nana"), 14 + self.taskProgressDic["wife_intimacy"]//100,
                            sml=[special_move(_("Basic Sword Technique"), level=10),
                                 special_move(_("Fairy Sword Technique"), level=7)],
                            skills=[skill(_("Burst of Potential"), level=3),
                                    skill(_("Basic Agility Technique"), level=5)], )
        elif self.partner == "Jiang Yuqi":
            opp = character(_("Jiang Yuqi"), 18 + self.taskProgressDic["wife_intimacy"]//100,
                            sml=[special_move(_("Eagle Claws"), level=7),
                                 special_move(_("Fairy Sword Technique"), level=7)],
                            skills=[skill(_("Spreading Wings"), level=5),
                                    skill(_("Basic Agility Technique"), level=10)],
                            preferences=[5, 3, 5, 3, 3])
        elif self.partner == "A-Xiu":
            opp = character(_("A-Xiu"), 12 + self.taskProgressDic["wife_intimacy"]//100,
                            sml=[special_move(_("Fairy Plucking Flowers"), level=10)],
                            skills=[skill(_("Basic Agility Technique"), level=10),
                                    skill(_("Dragonfly Dance"), level=10)],
                            preferences=[3, 1, 3, 1, 2])
        else:
            opp = ""

        self.reset_intimacy_button()
        self.battleMenu(self.you, opp, "battleground_forest.ppm", "jy_calm2.mp3", fromWin=self.scrubHouseWin,
                        battleType="training", destinationWinList=[] * 3, postBattleCmd=self.postPartnerSpar)

        
        
    def postPartnerSpar(self):
        if random() <= self.taskProgressDic["wife_intimacy"]/200:
            self.partner_moves = {
                _("Jiang Yuqi"): [_("Eagle Claws"), _("Fairy Sword Technique")],
                _("Ouyang Nana"): [_("Basic Sword Technique"), _("Fairy Sword Technique")],
                _("Li Guiping"): [_("Poison Needle"), _("Phantom Claws")],
                _("A-Xiu"): [_("Fairy Plucking Flowers")]
            }
            r_move = pickOne(self.partner_moves[self.partner])
            if r_move not in [m.name for m in self.sml]:
                new_move = special_move(r_move)
                self.learn_move(new_move)
                messagebox.showinfo("", _("You learned a new move: {}!").format(r_move))
                
        self.scrubHouseWin.deiconify()
        
        
    def reset_intimacy_button(self):
        if self.taskProgressDic["wife_intimacy"] < 0 or random() >= self.taskProgressDic["wife_intimacy"] / 100 or random() <= .3:
            self.partner_intimate_activity_button.config(state=DISABLED)
        #elif _("Yin Yang Soul Absorption Technique") in [s.name for s in self.skills]:
        elif self.taskProgressDic["castrated"] == 1 and self.taskProgressDic["wife_intimacy"] >= 0:
            self.partner_intimate_activity_button.config(state=DISABLED)
            self.wife_leaves()
        else:
            self.partner_intimate_activity_button.config(state=NORMAL)


    def wife_leaves(self):
        self.stopSoundtrack()
        self.currentBGM = "jy_shendiaoshuhuan.mp3"
        self.startSoundtrackThread()
        self.taskProgressDic["wife_intimacy"] = -1000
        self.chivalry -= 100
        self.destroy_all(self.ScrubHouseF3)
        self.ScrubHouseF1.place(x=-3000, y=-3000)
        self.ScrubHouseF2.place(x=-3000, y=-3000)
        self.update_scrub_house_win()
        self.generate_dialogue_sequence(
            self.scrubHouseWin,
            "scrub_house.ppm",
            [_("{}...").format(self.character),
             _("What do you want? Hurry up, I'm busy..."),
             _("I... I just wanted to see if you could spend some time with me..."),
             _("Recently it seems like you've been really cold towards me, and I don't know if I did something wrong."),
             _("I'm just busy training is all. There are so many people I need to defeat to become the strongest."),
             _("I know you love martial arts but... Aren't there more important things than becoming--"),
             _("Nonsense! Have you lost your mind?! What can possibly be more important than that?"), #Row 2,
             _("I... {}... It just feels like you don't love me as much as you used to...").format(self.character),
             _("And it's been a while since we... we..."),
             _("No!!!"),
             _("I mean, I... I'm just... too tired after training..."),
             _("Then maybe you can take a break for today and --"),
             _("NO!!! I already said no!!! What do you not understand about that?!"), #Row 3
             _("{}... why... is it because I'm no longer attractive to you?").format(self.character),
             _("No that's not it... it's just that... AGH DANG IT! Can you stop bothering me?!"),
             _("Just leave me alone!!! I wish you would just go away and never come back!"),
             _("You... you mean that?"),
             _("Yes... Look, I'm sorry, but we can't... I can't... We will never be as intimate as we used to be."),
             _("And frankly, I don't want to keep wasting your time..."), #Row 4
             _("If you find a better man, go ahead and marry him..."),
             _("... As you wish, {}...").format(self.character),
             _("*{} leaves.*").format(self.partner),
             _("....................")],
            [self.partner, "you", self.partner, self.partner, "you", self.partner,
             "you", self.partner, self.partner, "you", "you", self.partner,
             "you", self.partner, "you", "you", self.partner, "you",
             "you", "you", self.partner, "Blank", "you", 
             ]
        )

        self.partner = ""
        messagebox.showinfo("", _("Your chivalry -100!"))

            
    def giveGiftToPartner(self):

        #if _("Yin Yang Soul Absorption Technique") in [s.name for s in self.skills] and self.taskProgressDic["wife_intimacy"] >= 0:
        if self.taskProgressDic["castrated"] == 1 and self.taskProgressDic["wife_intimacy"] >= 0:
            self.wife_leaves()
            return

        gifts = {_("Leather Boots"): 20, 
                 _("Wool Boots"): 20, 
                 _("Silk Skirt"): 30,
                 _("Silk Fan"): 10, 
                 _("Emerald"): 10,
                 _("Topaz"): 12,
                 _("Dumplings"): 5, 
                 _("Soup"): 5, 
                 _("Crab"): 5,
                 _("Fish"): 3}
        
        if not self.partner_gift:
            self.partner_gift = pickOne(list(gifts.keys()))


        self.generate_dialogue_sequence(
            self.scrubHouseWin,
            "scrub_house.ppm",
            [_("I really want '{}'. Wonder where I can get it...").format(self.partner_gift)],
            [self.partner]
        )
        
        if self.partner_gift in self.inv:
            if self.inv[self.partner_gift] - (self.partner_gift in [e.name for e in self.equipment]) > 0:
                self.scrubHouseWin.withdraw()
                response = messagebox.askquestion("", _("Give {} to {}?").format(self.partner_gift, self.partner))
                if response == "yes":
                    self.add_item(self.partner_gift, -1)
                    if " " in self.partner:
                        partner_name = self.partner.split(" ")[1]
                    else:
                        partner_name = self.partner
                    self.generate_dialogue_sequence(
                        self.scrubHouseWin,
                        "scrub_house.ppm",
                        [_("Here you go, {}! I got this just for you!").format(partner_name),
                         _("Thanks, {}! You're the best!").format(self.character)],
                        ["you", self.partner]
                    )
                    self.reset_intimacy_button()
                        
                    if self.taskProgressDic["wife_intimacy"] >= 0:
                        self.taskProgressDic["wife_intimacy"] += gifts[self.partner_gift]
                    elif self.taskProgressDic["eagle_sect"] >= 11 and self.taskProgressDic["eagle_sect"] != 1000:
                        self.taskProgressDic["eagle_sect"] += gifts[self.partner_gift]*2
                        if self.taskProgressDic["eagle_sect"] >= 100 and self.taskProgressDic["castrated"] != 1:
                            self.taskProgressDic["eagle_sect"] = 100
                            self.taskProgressDic["wife_intimacy"] = 100
                            self.generate_dialogue_sequence(
                                self.scrubHouseWin,
                                "scrub_house.ppm",
                                [_("Hey, {}, Yuqi has been with you for quite some time now, and I can tell you care a lot for her.").format(self.character),
                                 _("Why don't you two get married tonight? I'll be the host!"),
                                 _("Wow, you're a true friend, Scrub!"),
                                 _("Yuqi, what do you think?"),
                                 _("Whatever you think is best, {}. After all, I'm yours from now on... *blush*").format(self.character),
                                 _("Perfect! I'll make the arrangements for you guys right away!"),
                                 _("That night, Scrub officiates your wedding. You and Jiang Yuqi are officially husband and wife!")],
                                ["Scrub", "Scrub", "you", "you", "Jiang Yuqi", "Scrub", "Blank"]
                            )
                            self.staminaMax += 200
                            self.healthMax += 200
                            self.attack += 20
                            self.strength += 20
                            self.speed += 20
                            self.defence += 20
                            self.dexterity += 20
                            
                    elif self.taskProgressDic["a_xiu"] >= 0:
                        self.taskProgressDic["a_xiu"] += gifts[self.partner_gift]*2
                        if self.taskProgressDic["a_xiu"] >= 100 and self.taskProgressDic["castrated"] != 1:
                            self.taskProgressDic["a_xiu"] = 100
                            self.taskProgressDic["wife_intimacy"] = 100
                            self.generate_dialogue_sequence(
                                self.scrubHouseWin,
                                "scrub_house.ppm",
                                [_("Hey, {}, A-Xiu has been with you for quite some time now, and I can tell you care a lot for her.").format(self.character),
                                 _("Why don't you two get married tonight? I'll be the host!"),
                                 _("Wow, you're a true friend, Scrub!"),
                                 _("A-Xiu, what do you think?"),
                                 _("Whatever you think is best, {}. After all, I'm yours from now on... *blush*").format(self.character),
                                 _("Perfect! I'll make the arrangements for you guys right away!"),
                                 _("That night, Scrub officiates your wedding. You and A-Xiu are officially husband and wife!")],
                                ["Scrub", "Scrub", "you", "you", "A-Xiu", "Scrub", "Blank"]
                            )
                            self.staminaMax += 200
                            self.healthMax += 200
                            self.attack += 20
                            self.strength += 20
                            self.speed += 20
                            self.defence += 20
                            self.dexterity += 20

                    self.partner_gift = None
                
                else:
                    self.scrubHouseWin.deiconify()
                    

    def interactWithPartner(self, choice=0):

        #if _("Yin Yang Soul Absorption Technique") in [s.name for s in self.skills] and self.taskProgressDic["wife_intimacy"] >= 0:
        if self.taskProgressDic["castrated"] == 1 and self.taskProgressDic["wife_intimacy"] >= 0:
            self.wife_leaves()
            return

        if choice == 0:
            if self.partner == _("Li Guiping") and random() <= .5:
                self.scrubHouseWin.withdraw()
                self.generate_dialogue_sequence(
                    None,
                    "scrub_house.ppm",
                    [_("Hey, {}, can you please give me a foot massage?").format(self.character)],
                    [self.partner],
                    [[_("Sure!"), partial(self.interactWithPartner, 11)],
                     [_("Maybe later"), partial(self.interactWithPartner, 12)]]
                )
            else:
                r = randrange(4)
                if r == 0:
                    self.generate_dialogue_sequence(
                        self.scrubHouseWin,
                        "scrub_house.ppm",
                        [_("*You glance over at {} and catch her staring at you. She looks away quickly and blushes.*").format(self.partner)],
                        ["Blank"]
                    )
                elif r == 1:
                    self.generate_dialogue_sequence(
                        self.scrubHouseWin,
                        "scrub_house.ppm",
                        [_("I've been practicing pretty hard lately."),
                         _("Whenever you get a chance, let's spar!")],
                        [self.partner, self.partner]
                    )
                elif r == 2:
                    self.generate_dialogue_sequence(
                        self.scrubHouseWin,
                        "scrub_house.ppm",
                        [_("Hey, {}, I was sparring with Scrub the other day, and he used some really strange moves!").format(self.character),
                         _("Really? He's just a scrub... He only uses some basic moves."),
                         _("Hmmm, I don't know... I can't seem to beat  him..."),
                         _("Interesting...")],
                        [self.partner, "you", self.partner, "you"]
                    )
                elif r == 3:
                    self.generate_dialogue_sequence(
                        self.scrubHouseWin,
                        "scrub_house.ppm",
                        [_("There's a famous merchant near the sea who sells a variety of goods for a good price."),
                         _("I purchased some really nice jewelry from him once.")],
                        [self.partner, self.partner]
                    )

        else:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            if choice == 11:
                Scene_phantom_sect_train(self, True)
            elif choice == 12:
                self.scrubHouseWin.deiconify()

    def checkIntimacyPartner(self):
        if self.taskProgressDic["eagle_sect"] >= 11 and self.taskProgressDic["eagle_sect"] < 100:
            if self.taskProgressDic["eagle_sect"] < 50:
                print(_("Your current intimacy level: fair."))
            elif self.taskProgressDic["eagle_sect"] < 100:
                print(_("Your current intimacy level: warm."))
            elif self.taskProgressDic["eagle_sect"] < 150:
                print(_("Your current intimacy level: passionate."))
            elif self.taskProgressDic["eagle_sect"] < 200:
                print(_("Your current intimacy level: inseparable."))
            else:
                print(_("Your current intimacy level: soul mates."))
        elif self.taskProgressDic["a_xiu"] >= 0 and self.taskProgressDic["a_xiu"] < 100:
            if self.taskProgressDic["a_xiu"] < 50:
                print(_("Your current intimacy level: fair."))
            elif self.taskProgressDic["a_xiu"] < 100:
                print(_("Your current intimacy level: warm."))
            elif self.taskProgressDic["a_xiu"] < 150:
                print(_("Your current intimacy level: passionate."))
            elif self.taskProgressDic["a_xiu"] < 200:
                print(_("Your current intimacy level: inseparable."))
            else:
                print(_("Your current intimacy level: soul mates."))
        else:
            if self.taskProgressDic["wife_intimacy"] < 50:
                print(_("Your current intimacy level: fair."))
            elif self.taskProgressDic["wife_intimacy"] < 100:
                print(_("Your current intimacy level: warm."))
            elif self.taskProgressDic["wife_intimacy"] < 150:
                print(_("Your current intimacy level: passionate."))
            elif self.taskProgressDic["wife_intimacy"] < 200:
                print(_("Your current intimacy level: inseparable."))
            else:
                print(_("Your current intimacy level: soul mates."))

    
    def intimateActivityWithPartner(self):
        if self.stamina/self.staminaMax < .5:
            messagebox.showinfo("", _("Not enough stamina."))
            return
        
        if self.taskProgressDic["eagle_sect"] >= 11 and self.taskProgressDic["eagle_sect"] < 100:
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("You... you want to what with me?! *blush*"),
                 _("We're not even married yet! How can you make such a request!"),
                 _("Ah sorry, Yuqi, I guess I was a bit too eager...")],
                ["Jiang Yuqi", "Jiang Yuqi", "you"]
            )
            self.taskProgressDic["eagle_sect"] -= 20
            if self.taskProgressDic["eagle_sect"] < 11:
                self.taskProgressDic["eagle_sect"] = 11
        elif self.taskProgressDic["a_xiu"] >= 0 and self.taskProgressDic["a_xiu"] < 100:
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("You... you want to what?! *blush*"),
                 _("We're not even married yet! How can you make such a request!"),
                 _("Ah sorry, A-Xiu, I guess I was a bit too eager..."),
                 _("Just because I worked at Wanhua Pavillion doesn't mean I'm that type of girl! Hmph...")],
                ["A-Xiu", "A-Xiu", "you", "A-Xiu"]
            )
            self.taskProgressDic["a_xiu"] -= 10
            if self.taskProgressDic["a_xiu"] < 0:
                self.taskProgressDic["a_xiu"] = 0
        else:
            self.taskProgressDic["wife_intimacy"] += 20
            self.stamina = 0
            self.partner_intimate_activity_button.config(state=DISABLED)
            if self.taskProgressDic["wife_intimacy"] <= 200:
                self.healthMax += 50
                self.staminaMax += 50
            messagebox.showinfo("", _("You expend all your stamina engaging in intimate activity with your wife."))
            if random() <= self.taskProgressDic["wife_intimacy"] / 200:
                self.partner_skills = {
                    _("Jiang Yuqi"): [_("Spreading Wings"), _("Basic Agility Technique")],
                    _("Ouyang Nana"): [_("Burst of Potential"), _("Basic Agility Technique")],
                    _("Li Guiping"): [_("Basic Agility Technique"), _("Phantom Steps")],
                    _("A-Xiu"): [_("Basic Agility Technique"), _("Dragonfly Dance")]
                }
                r_skill = pickOne(self.partner_skills[self.partner])
                if r_skill not in [s.name for s in self.skills]:
                    new_skill = skill(r_skill)
                    self.learn_skill(new_skill)
                    messagebox.showinfo("", _("You learned a new skill: {}!").format(r_skill))

            self.scrubHouseWin.deiconify()


    def return_to_scrub_house(self):
        self.update_map_location("scrub_house")

        if self.taskProgressDic["wanhua_pavillion"] == 8:
            self.taskProgressDic["wanhua_pavillion"] = 9
        
        if self.partner == _("Jiang Yuqi") and self.taskProgressDic["liu_village"] >= 1000 and self.taskProgressDic["jiang_yuqi_liu_village_choice"] == -1:
            self.taskProgressDic["jiang_yuqi_liu_village_choice"] = 1
            self.mapWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("Hmmm, should I tell Yuqi the truth about her dad? She might get upset..."),
                 _("Or perhaps it's better to keep it a secret...")],
                ["you", "you"],
                [[_("Tell Yuqi the truth."), partial(self.jiang_yuqi_liu_village_choice, 1)],
                 [_("Don't say anything."), partial(self.jiang_yuqi_liu_village_choice, 2)]]
            )
            return

        self.update_scrub_house_win()
        self.winSwitch(self.mapWin, self.scrubHouseWin)
        if self.taskProgressDic["blind_sniper"] == 0:
            self.taskProgressDic["blind_sniper"] = 1
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("Hey, Scrub! I need --"),
                 _("What the... Where did he go?"),
                 _("Dang it, why does he have to disappear right when I need him..."),
                 _("He can't have gone far. I'd better look for him nearby.")],
                ["you", "you", "you", "you"]
            )


    def clickedOnScrub(self):

        if self.taskProgressDic["potters_field"] == 0:
            self.taskProgressDic["potters_field"] = 1
            self.scrubHouseWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("Yo, Scrub!"),
                 _("Yes...?"),
                 _("Got a question for ya..."),
                 _("Do you know anything about... a potter's field?"),
                 _("Potter's Field... Oh... Er, yes, but how did you hear about it?"),
                 _("Well, I... I killed a guy, and before he died, he muttered something about a potter's field."),
                 _("And to be fair, I didn't actually kill him, or at least it wasn't completely my responsibility..."),
                 _("Oh dear, what terrible things have you been up to...?"), #Row 2
                 _("Hey! I'm not here for you to judge me."),
                 _("Ah, right, right, answers... that's what you want..."),
                 _("Have you ever wondered where people in this game go after they die?"),
                 _("Hmmm, not really... they just, uh, kinda disappear..."),
                 _("Or so it seems... you see, when someone dies in this game, they don't really disappear..."),
                 _("They just end up in a place where you can't see them anymore..."),
                 _("Is that so...?"), #Row 3
                 _("Yes, of course, why would I lie to you?"),
                 _("Anyway, as you've probably guessed, they (or at least their spirit) ends up in the Potter's Field"),
                 _("Which, by the way, is a restricted area in this game."),
                 _("So no, I'm not letting you go there..."),
                 _("Plus, it's dark and spooky in there. You don't wanna go; trust me..."),
                 _("Dark? Spooky? Sounds like my kind of place!"),
                 _("Come on, Scrub! You da best! :)"), #Row 4
                 _("No... absolutely not... no way..."),
                 _("B-but... Ahhhh!!! I really wanna know what's there! You're killing me, dude!"),
                 _("I'd be killing you if I sent you there..."),
                 _("So you're really not going to let me--"),
                 _("No."),
                 _("Wow. Just, wow. I'm speechless, Scrub. I can't believe you would do this to me."),
                 _("You know, I bet you were just a lazy developer who never bothered to finish designing this place."), #Row 5
                 _("That's why you won't let me go... because the place doesn't even exist. HAHAHAHA! What a joke..."),
                 _("I'm not lazy!!! Hey! You take that back!"),
                 _("What a lazy scrub..."),
                 _("What did you call me?!"),
                 _("Lazy. Scrub. Hey, that's your name isn't it? Scrub?"),
                 _("I'm telling you; the place is too dangerous--"),
                 _("Lies. Big fat lies..."), 
                 _("You..."), #Row 6
                 _("*Yawn*"),
                 _("Oh that's it! Fine!!! I'll let you go! Just to prove that I did in fact finish the place..."),
                 _("But don't say I didn't warn you. The place is extremely dark and..."),
                 _("And...?"),
                 _("Well, based on the evil deeds you've done, you might uh... well..."),
                 _("Let's just say that even though some people may be dead, their spirits are not at peace..."),
                 _("Hmmm? What does that mean?"), #Row 7
                 _("You'll find out... Anyway, I will give you a torch before sending you there so you'll actually be able to see..."),
                 _("You ready to go now or do you need some time to prepare?")
                 ],
                ["you", "Scrub", "you", "you", "Scrub", "you", "you",
                 "Scrub", "you", "Scrub", "Scrub", "you", "Scrub", "Scrub",
                 "you", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub",
                 "you", "you", "Scrub", "you", "Scrub", "you", "Scrub", "you",
                 "you", "you", "Scrub", "you", "Scrub", "you", "Scrub", "you",
                 "Scrub", "you", "Scrub", "Scrub", "you", "Scrub", "Scrub",
                 "you", "Scrub", "Scrub"]
            )

            response = messagebox.askquestion("", _("Go to Potter's Field now?"))
            if response == "yes":
                self.generate_dialogue_sequence(
                    None,
                    "scrub_house.ppm",
                    [_("Of course I'm ready! Why do you even ask?"),
                     _("........."),
                     _("Alright. Here, take this torch... Off you go...")],
                    ["you", "Scrub", "Scrub"]
                )
                self.go_potters_field()
            else:
                self.scrubHouseWin.deiconify()
            return

        elif self.taskProgressDic["potters_field"] == 1:
            self.scrubHouseWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("Come on! Are you ready to go yet?")],
                ["Scrub"]
            )
            response = messagebox.askquestion("", _("Go to Potter's Field now?"))
            if response == "yes":
                self.generate_dialogue_sequence(
                    None,
                    "scrub_house.ppm",
                    [_("Of course I'm ready! Why do you even ask?"),
                     _("........."),
                     _("Alright. Here, take this torch... Off you go...")],
                    ["you", "Scrub", "Scrub"]
                )
                self.go_potters_field()

            else:
                self.scrubHouseWin.deiconify()
            return


        elif self.taskProgressDic["blind_sniper"] == 2:
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("Wait, who do I need to talk to again?"),
                 _("Go to Babao Tower and find Nie Tu."),
                 _("He should be able to help you get into the Empress' Chambers...")],
                ["you", "Scrub", "Scrub"]
            )


        self.destroy_all(self.ScrubHouseF3)
        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        if self.taskProgressDic["shinscrub"] > 0:
            Button(self.ScrubHouseF3, text=_("Go to parallel universe"), command=self.transfer_to_parallel_universe).grid(row=2, column=0)
            Button(self.ScrubHouseF3, text=_("Challenge Scrub (Stage {})").format(self.taskProgressDic["shinscrub"]),
                   command=self.sparWithScrub).grid(row=3, column=0)
        else:
            Button(self.ScrubHouseF3, text=_("Spar (Scrub)"), command=self.sparWithScrub).grid(row=2, column=0)
        #Button(self.ScrubHouseF3, text=_("Heal me"), command=self.healFromScrub).grid(row=3, column=0)
        #Button(self.ScrubHouseF3, text=_("Who are you?"), command=self.scrub_who_are_you).grid(row=4, column=0)
        #Button(self.ScrubHouseF3, text=_("Tell me about the combat system..."), command=self.combat_tutorial).grid(row=5,
        #                                                                                                          column=0)
        Button(self.ScrubHouseF3, text=_("Teach me some moves please."), command=self.scrub_teach_moves).grid(row=6,
                                                                                                             column=0)
        Button(self.ScrubHouseF3, text=_("Beginner Guide/README."), command=self.readme).grid(row=7, column=0)

        if self.taskProgressDic["huashanlunjian"] >= 0:
            Button(self.ScrubHouseF3, text=_("Hua Shan Lun Jian"), command=self.huashanlunjian).grid(row=20, column=0)
        if self.taskProgressDic["huashanlunjian"] == 0:
            Button(self.ScrubHouseF3, text=_("What is 'Hua Shan Lun Jian'?"), command=self.huashanlunjian_explanation).grid(row=21, column=0)


    def readme(self):
        if os.name == "nt":
            res = 1
            try:
                res = subprocess.call("start microsoft-edge:https://bitbucket.org/WuxiaScrub/wuxia-world-readme/src/master/README",
                                shell=True, timeout=4)
            except:
                res = subprocess.call("start chrome.exe https://bitbucket.org/WuxiaScrub/wuxia-world-readme/src/master/README",
                                shell=True, timeout=4)
            else:
                if res == 1:
                    messagebox.showinfo("", _("Default browser not found. Access README at: https://bitbucket.org/WuxiaScrub/wuxia-world-readme/src/master/README"))
                    print("README link: https://bitbucket.org/WuxiaScrub/wuxia-world-readme/src/master/README")
        
        else:
            try:
                subprocess.Popen(["open", "https://bitbucket.org/WuxiaScrub/wuxia-world-readme/src/master/README"])
            except:
                messagebox.showinfo("", _("Default browser not found. Access README at: https://bitbucket.org/WuxiaScrub/wuxia-world-readme/src/master/README"))
                print("README link: https://bitbucket.org/WuxiaScrub/wuxia-world-readme/src/master/README")
                
    

    def go_potters_field(self, stage=1):
        if stage == 1:
            self.scrubHouseWin.withdraw()
            self.stopSoundtrack()
            self.currentBGM = "Kindaichi 7.mp3"
            self.startSoundtrackThread()
            self.mini_game_in_progress = True
            self.mini_game = potters_field_mini_game(self, None)
            self.mini_game.new()
        elif stage == 2:
            self.taskProgressDic["potters_field"] = 2
            self.generate_dialogue_sequence(
                None,
                "Potters Field Dialogue BG.png",
                [_("Hmmm, Scrub mentioned that all the dead people go here."),
                 _("I wonder if I can find something interesting from their grave stones...")],
                ["you", "you"]
            )
            previous_level = self.mini_game
            self.mini_game_in_progress = True
            self.mini_game = cemetery_mini_game(self, previous_level)
            self.mini_game.new()

        elif stage == 10: #Won
            self.taskProgressDic["potters_field"] = 10
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("Welcome back!"),
                 _("Wait, what?? Scrub?? How... What the heck just happened?"),
                 _("Why am I back at your house???"),
                 _("Well, you've seen everything you need to see at the Potter's Field."),
                 _("So I decided to wake you up from your dream..."),
                 _("Dream....? You mean, none of that was real??"),
                 _("Hehehe, think of it like a special place in a dimension that you can't see, under normal circumstances at least..."),
                 _("Anyway, I'm surprised you made it out alive..."),
                 _("Hahahaha! Apparently, I'm so strong that ghosts aren't even a match for me anymore."), #Row 2
                 _("*Sigh*... Indeed, you've grown very powerful, at the expense of your honor and chivalry..."),
                 _("Who cares? The weak deserve to be crushed and used as stepping stones!"),
                 _("Oh, by the way, who is 'Popo' and 'Xiao Wantong'? I saw tombstones with their names inscribed."),
                 _("I... I don't know... Has nothing to do with me... They were killed by some dude named Ali, right?"),
                 _("Do you know who this Ali is? I've never seen or heard of these people before..."),
                 _("I... no, haha... how would I know?"),
                 _("You hiding something from me, Scrub?"),
                 _("Hey, would you like to learn a powerful move?"),
                 _("You're not answering my ques--"), #Row 3
                 _("The same one that Ren Zhichu's ghost used?"),
                 _("Oooh, you have my interest now but that still doesn't--"),
                 _("Yes or no?"),
                 _("Yes, of course I want to learn it!"),
                 _("You know what, seeing that you are already overpowered, it can't hurt to teach you this..."),
                 _("From this moment forward, don't mention anything you saw or did at the Potter's Field to anyone."),
                 _("Fine fine fine, got it... Now teach me already."),],
                ["Scrub", "you", "you", "Scrub", "Scrub", "you", "Scrub", "Scrub",
                 "you", "Scrub", "you", "you", "Scrub", "you", "Scrub", "you", "Scrub",
                 "you", "Scrub", "you", "Scrub", "you", "Scrub", "Scrub", "you"]
            )
            self.learn_move(special_move(_("Kiss of Death")))
            messagebox.showinfo("", _("Scrub teaches you a new move: 'Kiss of Death'!"))

        elif stage == 20: #Lost
            try:
                self.taskProgressDic["potters_field"] = 20
                self.generate_dialogue_sequence(
                    self.scrubHouseWin,
                    "scrub_house.ppm",
                    [_("AHHHHHHHHHHHHHHHHH!"),
                     _("Welcome back!"),
                     _("Wait, what?? Scrub?? How... What the heck just happened?"),
                     _("Why am I back at your house???"),
                     _("Well, you've seen everything you need to see at the Potter's Field."),
                     _("So I decided to wake you up from your dream..."),
                     _("Dream....? You mean, none of that was real??"),
                     _("Hehehe, think of it like a special place in a dimension that you can't see, under normal circumstances at least..."),
                     _("I sent you there to teach you a lesson... Now you should know, your evil deeds will eventually catch up to you."),
                     _("............... So I'm not dead?"),  # Row 2
                     _("Right..."),
                     _("Whew..."),
                     _("Oh, by the way, who is 'Popo' and 'Xiao Wantong'? I saw tombstones with their names inscribed."),
                     _("I... I don't know... Has nothing to do with me... They were killed by some dude named Ali, right?"),
                     _("Do you know who this Ali is? I've never seen or heard of these people before..."),
                     _("I... no, haha... how would I know?"),
                     _("You hiding something from me, Scrub?"),
                     _("Hey, would you like to receive some compensation for all that you went through at the Potter's Field?"),
                     _("You're not answering my ques--"),  # Row 3
                     _("Like a large sum of money?"),
                     _("Oooh, you have my interest now but that still doesn't--"),
                     _("Yes or no?"),
                     _("Yes, of course!"),
                     _("I'll give you 30000 gold and you will gladly accept it with gratitude; isn't that right, {}? Yes it is.").format(self.character),
                     _("Also, from this moment forward, don't mention anything you saw or did at the Potter's Field to anyone."),
                     _("Fine fine fine, got it... Now hand over the money already."),],
                    ["you", "Scrub", "you", "you", "Scrub", "Scrub", "you", "Scrub", "Scrub",
                     "you", "Scrub", "you", "you", "Scrub", "you", "Scrub", "you", "Scrub",
                     "you", "Scrub", "you", "Scrub", "you", "Scrub", "Scrub", "you"]
                )
                self.inv[_("Gold")] += 30000
                messagebox.showinfo("", _("Received 'Gold' x 30000 from Scrub!"))
            except Exception as e:
                pass

    def transfer_to_parallel_universe(self):
        
        self.scrubHouseWin.withdraw()

        self.transfer_limit = (self.ptc + 1) * 3
        transferrable_items = []
        for item, quantity in self.inv.items():
            if item not in [_("Gold")] and quantity > 0:
                transferrable_items.append(item)


        self.transfer_item_win = Toplevel(self.scrubHouseWin)
        Label(self.transfer_item_win, 
              text=_("Select the items that you wish to transfer. You can select up to {} items.").format(self.transfer_limit)).pack()
        item_transfer_win_frame = Frame(self.transfer_item_win, relief=SUNKEN, borderwidth=2)
        item_transfer_win_frame.pack()

        if len(transferrable_items) <= 96:
            frame_width = 12
            frame_height = 8
        #elif len(transferrable_items) <= 117:
        else:
            frame_width = 13
            frame_height = 9

        x_counter = 0
        y_counter = 0
        self.selected_transfer_item_checkbutton_vars = []
        self.item_transfer_images = []

        for item in transferrable_items:
            option_frame = Frame(item_transfer_win_frame)
            option_frame.grid(row=y_counter, column=x_counter)
            selected_transfer_item_var = StringVar()
            self.selected_transfer_item_checkbutton_vars.append(selected_transfer_item_var)

            c = Checkbutton(option_frame, text="", variable=selected_transfer_item_var, onvalue=item, offvalue="")
            c.pack(side=LEFT)

            option_image = PhotoImage(file=_("i_{}.ppm").format(item))
            option_label = Label(option_frame, image=option_image)
            option_label.pack(side=RIGHT)
            self.item_transfer_images.append(option_image)

            #Add tooltip to show item description
            if item in ITEM_SELLING_PRICES:
                sell_str = _("\n\nSelling price: ") + str(ITEM_SELLING_PRICES[item])
            else:
                sell_str = ""
            try:
                description_str = item + "\n--------------------------\n" + ITEM_DESCRIPTIONS[item] + sell_str
            except:
                equipment = Equipment(item)
                description_str = equipment.description
            self.balloon.bind(option_label, description_str + _("\n\nAmount owned: {}").format(self.inv[item]))

            x_counter += 1
            if x_counter >= frame_width:
                x_counter = 0
                y_counter += 1

        button_frame = Frame(self.transfer_item_win)
        button_frame.pack()
        Button(button_frame, text=_("Back"), command=partial(self.winSwitch, self.transfer_item_win, self.scrubHouseWin)).grid(row=0,column=0)
        Label(button_frame, text="    ").grid(row=0,column=1)
        Button(button_frame, text=_("Confirm"), command=self.complete_transfer_to_parallel_universe).grid(row=0,column=2)

        

    def complete_transfer_to_parallel_universe(self):
        items = {}
        for var in self.selected_transfer_item_checkbutton_vars:
            item = var.get()
            if len(item) > 0:
                items[item] = self.inv[item]
        if len(items) > self.transfer_limit:
            messagebox.showinfo("", _("You've selected too many items."))
        else:
            self.ptc += 1
            self.transfer_item_win.quit()
            self.transfer_item_win.destroy()
            self.scrubHouseWin.quit()
            self.scrubHouseWin.destroy()
            self.intro_quiz(items)


    def post_shin_scrub_fight(self):
        self.destroy_all(self.ScrubHouseF3)
        if self.taskProgressDic["shinscrub"] == 0:
            self.taskProgressDic["shinscrub"] = 1
            self.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("WOOOOOOOO~~~ I did it!!!"),
                 _("Yup! Congrats! You beat me, the final boss."),
                 _("So... what now?"),
                 _("Well, there's still a lot of plot waiting to be discovered.")],
                ["you", "Scrub", "you", "Scrub"]
            )

            if self.taskProgressDic["wife_intimacy"] < 0:
                self.generate_dialogue_sequence(
                    None,
                    "scrub_house.ppm",
                    [_("Really??"),
                     _("Yeah... for example, you don't even have a wife! You've missed so many opportunities..."),
                     _("Whaaaaat?")],
                    ["you", "Scrub", "you"]
                )

            elif self.taskProgressDic["liu_village"] < 1000:
                self.generate_dialogue_sequence(
                    None,
                    "scrub_house.ppm",
                    [_("Really??"),
                     _("Yeah... for example, you haven't even completed the story line for Liu Village massacre..."),
                     _("You can potentially learn the most powerful move in the game from that!"),
                     _("Whaaaaat?")],
                    ["you", "Scrub", "Scrub", "you"]
                )

            elif self.taskProgressDic["yagyu_clan"] == -1:
                self.generate_dialogue_sequence(
                    None,
                    "scrub_house.ppm",
                    [_("Really??"),
                     _("Yeah... for example, you haven't even completed the story line for Yagyu Clan..."),
                     _("There are several tough opponents you can fight in that story line!"),
                     _("The rewards are quite enticing too!"),
                     _("Whaaaaat?")],
                    ["you", "Scrub", "Scrub", "Scrub", "you"]
                )

            elif self.taskProgressDic["blind_sniper"] == -1:
                self.generate_dialogue_sequence(
                    None,
                    "scrub_house.ppm",
                    [_("Really??"),
                     _("Yeah... for example, you haven't even completed the story line for the Blind Sniper..."),
                     _("There are several other plot lines that can be unlocked from that!"),
                     _("Whaaaaat?")],
                    ["you", "Scrub", "Scrub", "you"]
                )

            self.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("I strongly recommend you play through the game again; this time, make different decisions."),
                 _("Aw man, does that mean I have to start all over again?"),
                 _("I worked really hard to learn all these moves and skills and to get these items!"),
                 _("Well, yes but... not completely."),
                 _("What do you mean?"),
                 _("Each time you beat the game (by defeating me), I can transfer you to a parallel universe."),
                 _("When you arrive in that universe, everything will be 'reset' to the beginning."), #Row 2
                 _("However, there are a few things I will do to allow you to retain some of your progress."),
                 _("Each time you are sent to a parallel universe, you will be allowed to bring a number of items with you."),
                 _("How many items you can bring depends on how many play-thrus you've completed."),
                 _("I will also give your attributes a slight boost so you start out stronger every time."),
                 _("Lastly, the max limit to which you can increase your stats using upgrade points will also increase."),
                 _("Ah I see... It sounds like after each play-thru, I will be able to retain not only my knowledge/experience with the game"), #Row 3
                 _("but also receive some bonuses for my next play-thru that allows me to get stronger than I am now!"),
                 _("Yup, you got it!"),
                 _("This should also make it easier for you to unlock some hidden plot hehehe..."),
                 _("Wow, how thoughtful of you, Scrub! Thanks!")],
                ["Scrub", "you", "you", "Scrub", "you", "Scrub",
                 "Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub",
                 "you", "you", "Scrub", "Scrub", "you"]
            )

            self.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("You're welcome! When you're ready, come talk to me to get transferred to the parallel universe."),
                 _("Alternatively, you can challenge me again to get more rewards before going.")],
                ["Scrub", "Scrub"]
            )
            self.scrubHouseWin.deiconify()
            

        else:
            if self.taskProgressDic["shinscrub"] < 20:
                self.taskProgressDic["shinscrub"] += 1

            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("Very nice! Here's your reward for winning.")],
                ["Scrub"]
            )

            for i in range(1+self.taskProgressDic["shinscrub"]//3):
                reward_options = []
                if random() <= .7:
                    if random() <= .5:
                        existing_move_names = [m.name for m in self.sml]
                        for move_name in self.animation_frame_delta_dic:
                            if move_name not in UNLEARNABLE_MOVES + existing_move_names:
                                reward_options.append(move_name)

                        if reward_options:
                            shin_scrub_reward = pickOne(reward_options)
                            new_move = special_move(name=shin_scrub_reward)
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("Scrub taught you a new move: {}!").format(shin_scrub_reward))
                        else:
                            self.add_item(_("Gold"), 25000)
                            messagebox.showinfo("", _("Received 'Gold' x 25000!"))

                    else:
                        existing_skill_names = [s.name for s in self.skills]
                        for skill_name in SKILL_NAMES:
                            if skill_name not in UNLEARNABLE_SKILLS + existing_skill_names:
                                reward_options.append(skill_name)

                        if reward_options:
                            shin_scrub_reward = pickOne(reward_options)
                            new_skill = skill(name=shin_scrub_reward)
                            self.learn_skill(new_skill)
                            messagebox.showinfo("", _("Scrub taught you a new skill: {}!").format(shin_scrub_reward))
                        else:
                            self.add_item(_("Gold"), 25000)
                            messagebox.showinfo("", _("Received 'Gold' x 25000!"))

                else:
                    reward_options += [_("Marrow Cleansing Manual"), _("Amethyst Necklace"),
                                       _("Venomous Viper Gloves"), _("Seven-inch Serpent Staff"),
                                       _("Lightning Saber"), _("Thunder Blade"), _("Ice Shard")]
                    shin_scrub_reward = pickOne(reward_options)
                    if shin_scrub_reward == _("Gold"):
                        self.add_item(_("Gold"), 25000)
                        messagebox.showinfo("", _("Received 'Gold' x 25000!"))
                    else:
                        self.add_item(shin_scrub_reward)
                        messagebox.showinfo("", _("Received '{}'").format(shin_scrub_reward))




    def sparWithScrub(self):
        if self.taskProgressDic["shinscrub"] == 0:
            opp = character("Scrub", 25+self.ptc,
                            sml=[special_move(_("Real Scrub Fist"), level=10),
                                 special_move(_("Fire Palm"), level=10),
                                 special_move(_("Falling Cherry Blossom Finger Technique"), level=10),
                                 special_move(_("Ice Palm"), level=10),
                                 special_move(_("Five Poison Palm"), level=10),
                                 special_move(_("Empty Force Fist"), level=10)],
                            skills=[skill(_("Divine Protection"), level=min([5+self.ptc,10])),
                                    skill(_("Tendon Changing Technique"), level=min([5+self.ptc,10])),
                                    skill(_("Huashan Agility Technique"), level=10),
                                    skill(_("Pure Yang Qi Skill"), level=10),
                                    skill(_("Phantom Steps"), level=10),
                                    skill(_("Flower and Body Unite"), level=10),
                                    skill(_("Countering Poison With Poison"), level=10),
                                    skill(_("Burst of Potential"), level=10),],
                            preferences=None)
            self.battleID = "shinscrub"
            self.battleMenu(self.you, opp, "battleground_forest.ppm", "jy_calm2.mp3", fromWin=self.scrubHouseWin,
                            battleType="test", destinationWinList=[], postBattleCmd=self.post_shin_scrub_fight)

        elif self.taskProgressDic["shinscrub"] >= 1:
            shin_scrub_sml = [special_move(_("Real Scrub Fist"), level=10),
                                 special_move(_("Fire Palm"), level=10),
                                 special_move(_("Falling Cherry Blossom Finger Technique"), level=10),
                                 special_move(_("Ice Palm"), level=10),
                                 special_move(_("Five Poison Palm"), level=10),
                                 special_move(_("Empty Force Fist"), level=10)]
            shin_scrub_skills = [skill(_("Divine Protection"), level=min([5 + self.ptc, 10])),
                                    skill(_("Tendon Changing Technique"), level=min([5 + self.ptc, 10])),
                                    skill(_("Huashan Agility Technique"), level=10),
                                    skill(_("Pure Yang Qi Skill"), level=10),
                                    skill(_("Phantom Steps"), level=10),
                                    skill(_("Flower and Body Unite"), level=10),
                                    skill(_("Countering Poison With Poison"), level=10),
                                    skill(_("Burst of Potential"), level=10)]
            
            
            additional_moves = [special_move(_("Iron Sand Palm"), level=10),
                                special_move(_("Guan Yin Palm"), level=10),
                                special_move(_("Taichi Fist"), level=10),
                                special_move(_("Taichi Sword"), level=10),
                                special_move(_("Lightning Finger Technique"), level=10),
                                special_move(_("Wild Wind Blade"), level=10),
                                special_move(_("Whirlwind Staff"), level=10),
                                special_move(_("Serpent Whip"), level=10),
                                special_move(_("Wood Combustion Blade"), level=10),
                                special_move(_("Dragon Roar"), level=10),
                                special_move(_("Shadow Blade"), level=10),
                                special_move(_("Ice Blade"), level=10),
                                special_move(_("Blind Sniper Blade"), level=10),
                                special_move(_("Hachiman Blade"), level=10),
                                special_move(_("Eagle Claws"), level=10),
                                special_move(_("Falling Cherry Blossom Finger Technique"), level=10),
                                special_move(_("Cherry Blossom Drifting Snow Blade"), level=10),
                                special_move(_("Demon Suppressing Blade"), level=10),
                                special_move(_("Babao Blade"), level=10),
                                special_move(_("Thousand Swords Technique"), level=10),]

            additional_skills = [skill(_("Dragonfly Dance"), level=10),
                                 skill(_("Huashan Sword Qi Gong"), level=10),
                                 skill(_("Spreading Wings"), level=10),
                                 skill(_("Cherry Blossoms Floating on the Water"), level=10),
                                 skill(_("Splitting Mountains and Earth"), level=10),
                                 skill(_("Bushido"), level=10),
                                 skill(_("Kenjutsu I"), level=10),
                                 skill(_("Ninjutsu I"), level=10),
                                 skill(_("Wudang Agility Technique"), level=10),
                                 skill(_("Shaolin Inner Energy Technique"), level=10),]
            
            shuffle(additional_moves)
            shuffle(additional_skills)
            
            for i in range(min([self.taskProgressDic["shinscrub"], 20])):
                r_move = additional_moves.pop()
                shin_scrub_sml.append(r_move)
                if i%2 == 0:
                    r_skill = additional_skills.pop()
                    shin_scrub_skills.append(r_skill)
            

            opp = character("Scrub", 25 + self.ptc//2 + self.taskProgressDic["shinscrub"]//3,
                            sml=shin_scrub_sml,
                            skills=shin_scrub_skills,
                            preferences=None)
            self.battleID = "shinscrub"
            self.battleMenu(self.you, opp, "battleground_forest.ppm", "jy_calm2.mp3", fromWin=self.scrubHouseWin,
                            battleType="test", destinationWinList=[], postBattleCmd=self.post_shin_scrub_fight)
            

        else:
            opp = character("Scrub", min([self.level, 12]),
                                                sml=[special_move(_("Scrub Fist"), level=min([10, self.level])),
                                                     #special_move(_("Crossbow"), level=10),
                                                     #special_move(_("Kusarigama"), level=10),
                                                     #special_move(_("Hensojutsu"), level=10),
                                                     #special_move(_("Basic Sword Technique"), level=min([10, self.level]))
                                                ],
                                                #skills=[skill(_("Divine Protection"), level=10)],
                                                preferences=None)
    
            self.battleMenu(self.you, opp, "battleground_forest.ppm", "jy_calm2.mp3", fromWin = self.scrubHouseWin,
                                                battleType="training", destinationWinList = [self.scrubHouseWin] * 3)


    def healFromScrub(self):
        response = messagebox.askquestion("", _("Pay 300 gold for Scrub to heal you to full health and stamina?"))
        if response == "yes":
            if self.inv[_("Gold")] >= 300:
                self.inv[_("Gold")] -= 300
                self.restore(0, 0, full=True)
                self.currentEffect = "sfx_recover.mp3"
                self.startSoundEffectThread()
            else:
                messagebox.showinfo("", _("You do not have enough gold."))
        else:
            return


    def scrub_who_are_you(self):

        self.generate_dialogue_sequence(
            self.scrubHouseWin,
            "scrub_house.ppm",
            [_("Who are you?"), _("Didn't I already tell you? My name is Scrub."),
             _("I'm the creator of this game...")],
            ["you", "Scrub", "Scrub"]
        )


    def combat_tutorial(self, choice=0):
        if choice == 0:
            self.scrubHouseWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("What would you like to know more about, specifically?"),
                 _("I can give you a quick summary, but I recommend reading the README file for more details.")],
                ["Scrub", "Scrub"],
                [[_("Stats"), partial(self.combat_tutorial,1)],
                 [_("Moves"), partial(self.combat_tutorial,2)],
                 [_("Skills"), partial(self.combat_tutorial,3)],
                 [_("Battle Status"), partial(self.combat_tutorial,4)],
                 [_("Critical Hit"), partial(self.combat_tutorial,5)],
                 [_("Damage Multiplier"), partial(self.combat_tutorial,6)],
                 [_("Experience"), partial(self.combat_tutorial,7)],
                 [_("Death"), partial(self.combat_tutorial,8)],
                 ]
            )
            
        else:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            
            if choice == 1:
                self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("There are 7 stats that are directly related to combat."),
                 _("Health, stamina, attack, strength, speed, defence, and dexterity"),
                 _("When your health reaches 0, you lose."),
                 _("Stamina is required to execute most attacks. How much stamina an attack uses up depends on the move itself as well as your proficiency for that move."),
                 _("You can increase your health and stamina upper limits by leveling up or by using upgrade points."),
                 _("Attack determines your accuracy, while strength determines the damage multiplier that is applied to each move's base damage."),
                 _("Speed determines how often you get to attack for every attack that your opponent performs. It also comes into play when you are trying to flee from battle."),
                 _("Defence determines how much damage you take from an opponent's attack."),
                 _("Dexterity determines the likelihood of you dodging an attack."),
                 _("These 5 stats can also be increased using upgrade points."),
                 _("During battle, you can click your or the opponent's avatar to view/compare stats, moves, and skills.")
                 ],
                ["Scrub"] * 11
            )

            elif choice == 2:
                self.generate_dialogue_sequence(
                    self.scrubHouseWin,
                    "scrub_house.ppm",
                    [_("In addition to the 'Scrub Fist' I taught you, there are 3 standard moves: rest, items, and flee."),
                     _("Rest: recover 5% health and 20% stamina (based on upper limit). Equipment and/or effects from skills may increase the actual amount recovered."),
                     _("Items: use an item from your inventory. Spends 33% fewer action points."),
                     _("Flee: escape from battle."),
                     _("Special moves are usually obtained by training with masters, completing a quest, or studying a martial arts manual."),
                     _("Most special moves are attacks that require the usage of stamina."),
                     _("You can also level up the proficiency of your attacks but using them repeatedly, up to level 10."),
                     _("Leveling up your special moves increases their damage and unlocks effects!"),
                     _("Read the README file for more details!")],
                    ["Scrub"]*9
                )
            
            elif choice == 3:
                self.generate_dialogue_sequence(
                    self.scrubHouseWin,
                    "scrub_house.ppm",
                    [_("Skills (typically agility techniques or inner energy cultivation techniques) are passive, meaning you do not have to do anything to 'activate' the effects."),
                     _("Skills can be leveled up to level 10 by using upgrade points, 1 point for each level. "),
                     _("Read the README file for more details!")],
                    ["Scrub"]*3
                )
            
            elif choice == 4:
                self.generate_dialogue_sequence(
                    self.scrubHouseWin,
                    "scrub_house.ppm",
                    [_("There are statuses you can acquire in battle that impact your (or your opponent's) performance."),
                     _("Check out the README file for a full list of effects with descriptions or hover over your status during battle for details.")],
                    ["Scrub"]*2
                )
    
            elif choice == 5:
                self.generate_dialogue_sequence(
                    self.scrubHouseWin,
                    "scrub_house.ppm",
                    [_("Some moves have a chance of inflicting critical hit (move inflicts twice as much damage)."),
                     _("Certain weapons/equipment may also increase the probability of critical hits.")],
                    ["Scrub"]*2
                )

            elif choice == 6:
                self.generate_dialogue_sequence(
                    self.scrubHouseWin,
                    "scrub_house.ppm",
                    [_("The damage dealt by each attack is determined by its base damage multiplied by the damage multiplier."),
                     _("Check out the README file for a full list of factors that impact the damage multiplier.")],
                    ["Scrub"]*2
                )

            elif choice == 7:
                self.generate_dialogue_sequence(
                    self.scrubHouseWin,
                    "scrub_house.ppm",
                    [_("After a battle, you will gain experience based on:"),
                     _("Whether you won the battle as well as the level difference between you and your opponent")],
                    ["Scrub"]*2
                )

            elif choice == 8:
                self.generate_dialogue_sequence(
                    self.scrubHouseWin,
                    "scrub_house.ppm",
                    [_("When your health reaches 0, you will lose the battle and die unless you are sparring or if you are in training."),
                     _("If this happens, you will be taken back to the main menu. Any unsaved progress will be lost.")],
                    ["Scrub"] * 2
                )


    def scrub_teach_moves(self):

        if self.taskProgressDic["join_sect"] == -1:
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("I've already taught you Scrub Fist? What? Is that not enough?"),
                 _("Scrub Fist is for scrubs... I want to become a hero!"),
                 _("Come on, just let me train with a Master!"),
                 _("Alright, alright... if you insist... listen up carefully. I'll give you 8 options."),
                 _("Currently, there are 4 martial arts masters with exceptional renown. They are known as the Elite Four."),
                 _("The Elite Four are:\nHuang Xiaodong, the Outstanding One from the East..."),
                 _("Tan Keyong, the Disciple from the West;\nSu Ling, Doctor from the South;\nand Lu Xifeng, Queer of the North."),
                 _("Besides these, you can also choose from the 3 famous Sects: Shaolin, Wudang, or Huashan."),
                 _("Wait...  you said 8 options. That's only 7..."),
                 _("Yup, there's one last option, which is none of the above..."),
                 _("If you choose this option, you will not be able to join any sects, which means you will not have a Master."),
                 _("However, if you decide to go rogue, I'll give you a gift before I send you on your way."),
                 _("Take a close look at your options again and choose wisely!"),
                 ],
                ["Scrub", "you", "you", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "you", "Scrub", "Scrub", "Scrub",
                 "Scrub"]
            )

            self.join_sect()

        else:
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("I have nothing more to teach you."),
                 _("If you want to learn some new moves, go roam around. Maybe you'll have some lucky encounters!")],
                ["Scrub", "Scrub"]
            )


    def join_sect(self, choice=None):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        self.gameDate += 60
        
        try:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
        except:
            pass

        if choice == None:
            self.scrubHouseWin.withdraw()
            self.join_sect_win = Toplevel(self.scrubHouseWin)

            self.sect_options_frame = Frame(self.join_sect_win)
            self.sect_options_frame.pack()

            options = [
                [_("Huang Xiaodong"), _("Huang Xiaodong (Outstanding One of the East)\n--------------------------------------------------\nHuang Xiaodong is the owner of Cherry Blossom\nIsland and is highly skilled in acupuncture\ntechniques. Being an introvert, he seldom\nmakes contact with the outside world, choosing\ninstead to invest time studying mathematics,\nscience, and the arts, while simulataneously\nimproving his Kung Fu techniques.\n\nHe lives on the island with his wife and two\ndaughers. Visitors who are uninvited have a\ndifficult time getting into the island due to\nthe Cherry Blossom Maze that he designed on\nthe outskirts of the island.")],
                [_("Tan Keyong"), _("Tan Keyong (Disciple of the West)\n--------------------------------------------------\nTan Keyong is the 4th generation leader of\nthe Shan Hu Sect, known for its Shan Hu Fist\nalong with its powerful Inner Energy Skills. He\nis well-respected in the Martial Arts community\nand often praised for his firm morals.\n20 years ago, by chance, Tan met the Martial\nArts Grandmaster Ye Hehua. Ye liked the young\nman's character and accepted him as his first\nofficial disciple. Within a few years of\ntraining with Ye, Tan became one of the\nstrongest fighters in the Martial Arts world,\nearning him a spot in the Elite Four.")],
                [_("Su Ling"), _("Su Ling (Doctor of the South)\n--------------------------------------------------\nHaving studied medicine and Kung Fu from an\nearly age, Su Ling is not only knowledgeable\nin curing diseases and prescribing medicine\nbut is also well-versed in martial arts, though\nher techniques are mostly defensive in nature.\nShe lives with her husband, Li Jinyi, in\nNongyuan Valley.\n\nLi Jinyi is also a renowned healer in the area.\nHowever, unlike his wife who prefers using\nconventional methods, Li enjoys experimenting\nwith poisons to counteract other poisons.\nHis experimentations have earned him quite the\nreputation but also an immunity to poison.")],
                [_("Lu Xifeng"), _("Lu Xifeng (Queer of the North)\n--------------------------------------------------\nThe true origins of Lu Xifeng are a mystery, but what is known is that he resides in Phantom\nValley and leads the Phantom Sect. With a name that instills fear in all who hear it, Lu earned\nhis reputation through his acts of cruelty and extreme narcisism, having once massacred an\nentire village because one of the villagers made a degrading comment about his voice.\n\nLu is seen with a mask over his face wherever he goes. No one has seen his true appearance,\nnot even members of his own sect.\n\nLu Xifeng claims to be the top martial artist in the world, even commenting at times that he\nhas surpassed the Grandmaster Ye Hehua. His title 'Queer of the North' came not only from\nhis strange temperament, but also because his\nfighting style focuses on bizarre techniques that catch the opponent off-guard.")],
                [_("Shaolin"), _("Shaolin Sect\n--------------------------------------------------\nOne of the best-known orthodox sects in the\nMartial Arts world, Shaolin is the birthplace\nof many fighting techniques and styles.\nShaolin monks undergo strenuous daily training\nand adhere to a strict diet, keeping them in\ntop physical shape. Their dedication to Buddhist\nteachings is reflected in their daily meditation.")],
                [_("Wudang"), _("Wudang Sect\n--------------------------------------------------\nOriginally founded by Zhang Sanfeng in the Yuan\ndynasty, Wudang has grown to become a respected\nsect within the Martial Arts world, with most of\nits martial arts techniques evolving from the\nconcept of 'Taichi' with a focus on using soft\nand gentle techniques to overcome brute force.")],
                [_("Huashan"), _("Huashan Sect\n--------------------------------------------------\nA well established sect in the Martial Arts\nworld; Huashan Sect 's martial arts mainly\ncomprise of sword techniques, such as the Huashan\nSword Technique and Thousand Swords Technique,\n and inner energy cultivation skills.")],
                [_("Rogue"), _("I don't need a master! I can learn everything\non my own! Rogue is the way to go!")]
            ]
            option_images = []

            for option_i in range(len(options)):
                x = option_i % 4
                y = option_i // 4 + 1
                name = options[option_i][0]
                description = options[option_i][1]

                F = Frame(self.sect_options_frame)
                F.grid(row=y, column=x)
                option_image = PhotoImage(file=name + "_icon_full.ppm")
                option_images.append(option_image)
                option_image_button = Button(F, command=partial(self.join_sect, option_i))
                option_image_button.pack()
                option_image_button.config(image=option_image)
                Label(F, text=name).pack()
                self.balloon.bind(F, description)

            Button(self.join_sect_win, text="Back",
                   command=partial(self.winSwitch, self.join_sect_win, self.scrubHouseWin)).pack()
            self.join_sect_win.protocol("WM_DELETE_WINDOW", self.on_exit)
            self.join_sect_win.update_idletasks()
            toplevel_w, toplevel_h = self.join_sect_win.winfo_width(), self.join_sect_win.winfo_height()
            self.join_sect_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
            self.join_sect_win.focus_force()
            self.join_sect_win.mainloop()###
        else:
            self.join_sect_win.quit()
            self.join_sect_win.destroy()

            if choice == 0:

                self.stopSoundtrack()
                self.currentBGM = "jy_pipayu.mp3"
                self.startSoundtrackThread()
                self.generate_dialogue_sequence(
                    None,
                    "cherry_blossom_island.png",
                    [_("Welcome to Cherry Blossom Island. Since you are a special guest, please make yourself at home."),
                     _("I will teach you the Falling Cherry Blossom Finger Technique. Practice diligently."),
                     _("At the end of 60 days, I will personally check your progress to see how you are doing."),
                     _("Depending on your performance, I may teach you additional techniques."),
                     _("If you have any questions in the mean time, you can ask my daughters, who will be able to help you."),
                     _("Understood! Thank you, Master!")],
                    ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong",
                     "Huang Xiaodong", "you"]
                )

                new_move = special_move(name=_("Falling Cherry Blossom Finger Technique"))
                self.learn_move(new_move)
                print(_("You learned a new move: Falling Cherry Blossom Finger Technique!"))
                self.taskProgressDic["join_sect"] = 0.00
                Scene_cherry_blossom_island_train(self)

            elif choice == 1:
                self.taskProgressDic["join_sect"] = 1.00
                self.stopSoundtrack()
                self.currentBGM = "jy_ambient3.mp3"
                self.startSoundtrackThread()
                self.generate_dialogue_sequence(
                    None,
                    "Shanhu Sect.png",
                    [_("Welcome to Shanhu Sect!"),
                     _("There are many opportunities to learn powerful techniques during your 60 days here."),
                     _("I hope you will use your time effectively."),
                     _("Yes, Master Tan! Thank you for having me!"),
                     _("My friends Zhu Shi Ting and Yang Kai will be around too."),
                     _("They are elders of the Shanhu Sect, and there is much you can learn by spending time with them."),
                     _("I'll make note of that; thank you!")],
                    ["Tan Keyong", "Tan Keyong", "Tan Keyong", "you", "Tan Keyong", "Tan Keyong", "you"]
                )

                Scene_shanhu_sect_train(self)

            elif choice == 2:

                self.stopSoundtrack()
                self.currentBGM = "jy_huanle.mp3"
                self.startSoundtrackThread()
                self.generate_dialogue_sequence(
                    None,
                    "Nongyuan Valley.png",
                    [_("Nice to meet you! I am Doctor Su Ling, and this is my husband, Li Jinyi."),
                     _("Hey there, young lad! Good to see you!"),
                     _("You definitely came to the right place! We're going to teach you a lot of cool stuff in the next 60 days!"),
                     _("Hi, welcome! I'm Xu Ting, an apprentice of Doctor Su!"),
                     _("Pleased to meet you all!"),
                     _("I place myself in your care. Thanks in advance!")],
                    ["Su Ling", "Li Jinyi", "Li Jinyi", "Xu Ting", "you", "you"]
                )

                self.taskProgressDic["join_sect"] = 2.00
                Scene_nongyuan_valley_train(self)


            elif choice == 3:
                if self.chivalry <= -20:
                    self.taskProgressDic["join_sect"] = 3.00
                    self.stopSoundtrack()
                    self.currentBGM = "Kindaichi 3.mp3"
                    self.startSoundtrackThread()
                    self.taskProgressDic["li_guiping_count"] = 0
                    self.taskProgressDic["ren_zhichu_count"] = 0
                    self.chivalry -= 50
                    messagebox.showinfo("", _("Your chivalry -50."))

                    self.generate_dialogue_sequence(
                        None,
                        "Phantom Sect.png",
                        [_("Hmmm, young man, you have made a wise choice in joining the strongest sect."),
                         _("I see great potential in you, and you will no doubt learn the most powerful techniques here."),
                         _("Remember, you must be willing to do anything... anything to get what you want."),
                         _("Yes, Master, I understand!"),
                         _("I will have Li Guiping, my top disciple, train you for the next 60 days."),
                         _("If you are obedient and do as I say, I'll teach you all the essentials."),
                         _("Ren Zhichu, a less talented disciple, will also help you along. Isn't that right?"),
                         _("Y-yes, Master... H-hi there, {}, pleased to meet you.").format(self.character),
                         _("Very good, now just remember not to wander around without my permission..."),
                         _("Violators will be punished... severely..."),
                         _("Yes, Master, I will be careful!"),
                         _("(Man this whole place gives me the creeps... What's with everyone covering their faces?)"),
                         _("(And this Lu Xifeng guy's voice sounds... weird... gentle but creepy and intimidating... I wonder if it was even a good idea to join this sect...)")
                         ],
                        ["Lu Xifeng", "Lu Xifeng", "Lu Xifeng", "you", "Lu Xifeng", "Li Guiping",
                         "Lu Xifeng", "Ren Zhichu", "Lu Xifeng", "Lu Xifeng", "you", "you", "you"]
                    )

                    Scene_phantom_sect_train(self)

                elif self.chivalry < 0:
                    self.generate_dialogue_sequence(
                        self.scrubHouseWin,
                        "scrub_house.ppm",
                        [_("I see some potential in you, but you are not quite ready just yet..."),
                         _("You need to demonstrate a willingness to do whatever it takes to get what you want, even if it means crushing others beneath your feet."),
                         _("What he means is that you are not evil enough to join his Sect... imagine that..."),
                         _("(Hmmm, looks like I need to commit some more evil before I try to join this Sect...)")],
                        ["Lu Xifeng", "Lu Xifeng", "Scrub", "you"]
                    )
                    self.taskProgressDic["join_sect"] = -1
                    self.gameDate -= 60
                else:
                    self.generate_dialogue_sequence(
                        self.scrubHouseWin,
                        "scrub_house.ppm",
                        [_("Ha, another weak, worthless scum who is unworthy of joining my Sect."),
                         _("Depart from me before I extinguish your existence with a sweep of my arm!"),
                         _("You are far too chivalrous to join his Sect..."),
                         _("(Hmmm, looks like I need to commit some evil before I try to join this Sect...)")],
                        ["Lu Xifeng", "Lu Xifeng", "Scrub", "you"]
                    )
                    self.taskProgressDic["join_sect"] = -1
                    self.gameDate -= 60


            elif choice == 4:
                self.taskProgressDic["join_sect"] = 4.00
                self.stopSoundtrack()
                self.currentBGM = "jy_shaolin1.mp3"
                self.startSoundtrackThread()

                self.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Welcome to the Shaolin Temple! We're glad to have you here."),
                     _("I am the Abbot here. I will put 2 elders, Xuanjing and Xuansheng, in charge of you."),
                     _("They will teach you martial arts as well as meditation techniques."),
                     _("Pleased to meet you; I am Elder Xuanjing."),
                     _("Nice to meet you; my name is Elder Xuansheng."),
                     _("It is my honor, Abbot, Elder Xuanjing, and Elder Xuansheng. Please take care of me."),
                     _("Xu Qing, who recently joined the temple 3 months ago will also be here. He can teach you the ropes and train with you."),
                     _("Hello there! Let us learn from each other."),
                     _("I will leave you to train with them now. See you in 60 days.")
                     ],
                    ["Abbot", "Abbot", "Abbot", "Elder Xuanjing", "Elder Xuansheng", "you", "Abbot", "Xu Qing", "Abbot"]
                )

                Scene_shaolin_train(self)


            elif choice == 5:
                self.taskProgressDic["join_sect"] = 5.00

                self.stopSoundtrack()
                self.currentBGM = "jy_wudang.mp3"
                self.startSoundtrackThread()

                self.generate_dialogue_sequence(
                    None,
                    "Wudang.png",
                    [_("Welcome to Wudang!"),
                     _("My name is Zhang Tianjian, the current leader of the Wudang Sect."),
                     _("It is my great honor to join your sect!"),
                     _("For the next 60 days, you will be training with Zhou Enrui and Li Kefan, 2 of the 3 Wudang Warriors."),
                     _("The 3rd one, Zhong Yue, is not currently at Wudang since he's on a mission."),
                     _("Welcome, {}, I expect you to train very hard and make great progress.").format(self.character),
                     _("Since Wudang is a chivalrous Sect, we send our members out to fight evil and help the poor and oppressed."),
                     _("We will have tasks for you periodically; if you are successful in completing them, you will be rewarded."),
                     _("Awesome! Looking forward to it!"),
                     _("Xu Jun, one of the junior members, will also be around to train with you."),
                     _("Hi there, {}!").format(self.character),
                     _("Alright, I think that's quite enough for introductions. Let's get down to business!")
                     ],
                    ["Zhang Tianjian", "Zhang Tianjian", "you", "Zhang Tianjian", "Zhang Tianjian", "Zhou Enrui",
                     "Li Kefan", "Li Kefan", "you", "Zhang Tianjian", "Xu Jun", "Li Kefan"]
                )

                Scene_wudang_train(self)


            elif choice == 6:
                self.taskProgressDic["join_sect"] = 6.00

                self.stopSoundtrack()
                self.currentBGM = "jy_shendiaozhuti.mp3"
                self.startSoundtrackThread()

                self.generate_dialogue_sequence(
                    None,
                    "Huashan.png",
                    [_("Welcome to Huashan! I am Guo Junda, the leader of the Huashan Sect."),
                     _("We are masters of the sword, and you will be exposed to quite a few sword techniques while you are with us."),
                     _("Since we only have 60 days together, I hope you will work extremely hard and make good use of your time."),
                     _("Yes, Master Guo! Thank you for taking care of me!"),
                     _("My son, Guo Zhiqiang, along with Zhao Huilin, one of my brightest students, will also train with you."),
                     _("We will show you the power of the Huashan Sword!"),
                     _("Nice to have you join us."),
                     _("It is my honor!"),
                     ],
                    ["Guo Junda", "Guo Junda", "Guo Junda", "you", "Guo Junda", "Guo Zhiqiang",
                     "Zhao Huilin", "you"]
                )

                Scene_huashan_train(self)

            elif choice == 7:
                self.generate_dialogue_sequence(self.scrubHouseWin,
                                                "scrub_house.ppm",
                                                [_("I have chosen to go rogue!"),
                                                 _("Hmmm, quite an unexpected choice... but hey, whatever makes you happy!"),
                                                 _("As promised, I will leave you with a little gift...")
                                                 ],
                                                ["you", "Scrub", "Scrub"])

                self.perception += 30
                self.luck += 30
                self.inv["Gold"] += 3000
                self.taskProgressDic["join_sect"] = 800
                self.gameDate -= 60
                messagebox.showinfo("", _("Scrub increases your perception and luck by 30 and gives you 3000 gold!"))


    def force_go_rogue(self):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        self.generate_dialogue_sequence(None,
                                        "scrub_house.ppm",
                                        [_("I have chosen to go rogue!"),
                                         _("Hmmm, quite an unexpected choice... but hey, whatever makes you happy!"),
                                         _("Since being rogue is not an easy path to take, I will leave you with a little gift...")
                                         ],
                                        ["you", "Scrub", "Scrub"])

        self.perception += 30
        self.luck += 30
        self.inv["Gold"] += 3000
        self.taskProgressDic["join_sect"] = 800
        messagebox.showinfo("", _("Scrub increases your perception and luck by 30 and gives you 3000 gold!"))
        self.mapWin.deiconify()


    def huashanlunjian(self):

        if calculate_month_day_year(self.gameDate)["Month"] != 9:
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("Sorry, Hua Shan Lun Jian is only held once a year in September. You'll have to come back later."),],
                ["Scrub"]
            )
            return

        level = self.taskProgressDic["huashanlunjian"]
        base_level = 24
        opp_pool = []
        opp_pool.append(
            character(_("Xiao Yong"), base_level + level,
                          sml=[special_move(_("Shanhu Fist"), level=10),
                               special_move(_("Demon Suppressing Blade"), level=8),
                               special_move(_("Ice Blade"), level=10),
                               special_move(_("Ice Palm"), level=10),
                               special_move(_("Soothing Song"))],
                          skills=[skill(_("Divine Protection"), level=8),
                                  skill(_("Leaping Over Mountain Peaks"), level=10)])
        )
        opp_pool.append(
            character(_("Su Ling"), base_level + level,
                      sml=[special_move(_("Basic Sword Technique"), level=10),
                           special_move(_("Basic Punching Technique"), level=10),
                           special_move(_("Body Cleansing Technique"), level=10)],
                      skills=[skill(_("Recuperation"), level=10)],
                      preferences=[1, 1, 3, 3, 3])
        )
        opp_pool.append(
            character(_("Huang Xiaodong"), base_level + level,
                      sml=[special_move(_("Falling Cherry Blossom Finger Technique"), level=10),
                           special_move(_("Cherry Blossom Drifting Snow Blade"), level=10)],
                      skills=[skill(_("Cherry Blossoms Floating on the Water"), level=10),
                              skill(_("Flower and Body Unite"), level=10)])
        )
        opp_pool.append(
            character(_("Abbot"), base_level + level,
                      sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                           special_move(_("Shaolin Diamond Finger"), level=10),
                           special_move(_("Guan Yin Palm"), level=10),
                           special_move(_("Empty Force Fist"), level=10), ],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                              skill(_("Tendon Changing Technique"), level=10),
                              skill(_("Shaolin Inner Energy Technique"), level=10)])
        )
        opp_pool.append(
            character(_("Tan Keyong"), base_level + level,
                          sml=[special_move(_("Shanhu Fist"), level=10),
                               special_move(_("Demon Suppressing Blade"), level=10)],
                          skills=[skill(_("Divine Protection"), level=10),
                                  skill(_("Leaping Over Mountain Peaks"), level=10)])
        )
        opp_pool.append(
            character(_("Ouyang Xiong"), base_level + level,
                          sml=[special_move(_("Violent Dragon Palm"), level=10),
                               special_move(_("Dragon Roar"), level=10)],
                          skills=[skill(_("Burst of Potential"), level=10),
                                  skill(_("Basic Agility Technique"), level=10)],
                          preferences=[5, 5, 3, 3, 2])
        )
        opp_pool.append(
            character(_("Zhang Tianjian"), base_level + level,
                          sml=[special_move(_("Taichi Fist"), level=10),
                               special_move(_("Taichi Sword"), level=10)],
                          skills=[skill(_("Wudang Agility Technique"), level=10),
                                  skill(_("Pure Yang Qi Skill"), level=10),
                                  skill(_("Taichi 18 Forms"), level=10),
                                  ])
        )
        opp_pool.append(
            character(_("Lu Xifeng"), base_level + level,
                      sml=[special_move(_("Phantom Claws"), level=10),
                           special_move(_("Heart Shattering Fist"), level=10),
                           special_move(_("Five Poison Palm"), level=10),
                           special_move(_("Shadow Blade"), level=10),
                           special_move(_("Centipede Poison Powder"))],
                      skills=[skill(_("Phantom Steps"), level=10),
                              skill(_("Yin Yang Soul Absorption Technique"), level=10),
                              skill(_("Countering Poison With Poison"), level=10)])
        )
        
        opp_list = []
        shuffle(opp_pool)
        for i in range(4):
            opp = opp_pool.pop()
            opp_list.append(opp)
            
        self.battleMenu(self.you, opp_list, "Huashan Cliff.png", "jy_calm2.mp3", fromWin = self.scrubHouseWin,
                        battleType="huashanlunjian", destinationWinList = [self.scrubHouseWin] * 3,
                        postBattleCmd=self.huashanlunjian_level_up)



    def huashanlunjian_explanation(self):
        self.generate_dialogue_sequence(
            self.scrubHouseWin,
            "scrub_house.ppm",
            [_("Ahh.... Hua Shan Lun Jian, the annual 'Duel at Huashan', is an event that takes place on the peak of Huashan in September."),
             _("The strongest fighters in the Martial Arts Community compete for the title of Number One in the World."),
             _("If you choose to participate, you will be up against 4 top-tier fighters."),
             _("If you win, I will give you a reward!"),
             _("Wait, since it's an annual event, do I have to wait a whole year after winning before I can go again?"),
             _("Yep, but each time, the rewards are very generous; plus, you can just run around the map to pass time if you are really that desperate..."),
             _("By the way, each time you win, your opponents will get a bit stronger."),
             _("And your opponents are chosen randomly, so the difficulty will vary."),
             _("Awesome, looking forward to it. Thanks Scrub!"),
             _("Haha, you're welcome. Good luck!")],
            ["Scrub", "Scrub", "Scrub", "Scrub", "you", "Scrub", "Scrub", "Scrub", "you", "Scrub"]
        )


    def huashanlunjian_level_up(self):
        self.taskProgressDic["huashanlunjian"] += 1
        self.gameDate += 31
        self.generate_dialogue_sequence(
            self.scrubHouseWin,
            "scrub_house.ppm",
            [_("Very nice! Here's your reward for winning.")],
            ["Scrub"]
        )

        for i in range(3):
            reward_options = []
            if random() <= .65:
                if random() <= .5:
                    existing_move_names = [m.name for m in self.sml]
                    for move_name in self.animation_frame_delta_dic:
                        if move_name not in UNLEARNABLE_MOVES + existing_move_names:
                            reward_options.append(move_name)

                    if reward_options:
                        huashanlunjian_reward = pickOne(reward_options)
                        new_move = special_move(name=huashanlunjian_reward)
                        self.learn_move(new_move)
                        messagebox.showinfo("", _("Scrub taught you a new move: {}!").format(huashanlunjian_reward))
                    else:
                        self.add_item(_("Gold"), 25000)
                        messagebox.showinfo("", _("Received 'Gold' x 25000!"))

                else:
                    existing_skill_names = [s.name for s in self.skills]
                    for skill_name in SKILL_NAMES:
                        if skill_name not in UNLEARNABLE_SKILLS + existing_skill_names:
                            reward_options.append(skill_name)

                    if reward_options:
                        huashanlunjian_reward = pickOne(reward_options)
                        new_skill = skill(name=huashanlunjian_reward)
                        self.learn_skill(new_skill)
                        messagebox.showinfo("", _("Scrub taught you a new skill: {}!").format(huashanlunjian_reward))
                    else:
                        self.add_item(_("Gold"), 25000)
                        messagebox.showinfo("", _("Received 'Gold' x 25000!"))


            else:
                reward_options += [_("Marrow Cleansing Manual"), _("Amethyst Necklace"),
                                   _("Venomous Viper Gloves"), _("Seven-inch Serpent Staff"),
                                   _("Lightning Saber"), _("Thunder Blade"), _("Ice Shard")]
                huashanlunjian_reward = pickOne(reward_options)
                if huashanlunjian_reward == _("Gold"):
                    self.add_item(_("Gold"), 25000)
                    messagebox.showinfo("", _("Received 'Gold' x 25000!"))
                else:
                    self.add_item(huashanlunjian_reward)
                    messagebox.showinfo("", _("Received '{}'").format(huashanlunjian_reward))



        if self.taskProgressDic["huashanlunjian"] >= 1 and self.taskProgressDic["shinscrub"] == -1:
            self.taskProgressDic["shinscrub"] = 0
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("So, how does it feel to be Ranked #2 in the world?"),
                 _("Wait, what? #2? You mean #1? Didn't you see me defeat all those pros?"),
                 _("Yes, that was quite impressive, however... You might not be able to win so easily against me."),
                 _("HAHAHAHAHA!!! You?? Dude, you're not even in my league..."),
                 _("I could probably knock you out with 1 move."),
                 _("Hehehe... Do you really think I would make myself THAT weak in my own game?"),
                 _("Plus, up until now, every fight that you took part in, you had the freedom to use items."),
                 _("And your opponents weren't even close to the maximum level: 30."),
                 _("Ok, you kinda have a point..."), #Row 2
                 _("If you want a true challenge, come fight me. You won't be allowed to use items though."),
                 _("Oh yeah? So you are the final boss, eh? Then prepare for a humiliating defeat..."),
                 _("Got what it takes to back up your words? Come challenge me when you're ready, hehehehe...")],
                ["Scrub", "you", "Scrub", "you", "you", "Scrub", "Scrub", "Scrub", 
                 "you", "Scrub", "you", "Scrub"]
            )


    def map(self, fromWin):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.taskProgressDic["scrub_tutorial"] == -1:
            self.taskProgressDic["scrub_tutorial"] = 1
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("Wait! Where do you think you're going?!"),
                 _("Um... to explore...?"),
                 _("Do you even know how to fight yet? Come here and spar with me real quick, so I can see if you're in good shape..."),
                 _("... What if you kill me?"),
                 _("Haha, don't worry; I'll go easy on you."),
                 _("Plus, spars are safe. When you spar someone, you don't die even if you lose."),
                 _("So... how can I die?"),
                 _("Well, say you did something dumb like walking into a dark forest when you are low level and you encounter some robbers..."),
                 _("When you start out, there's a lot that you don't know, so frequent saves are recommended."),
                 _("Hmmm, got it, thanks!"),
                 _("You ready for a warm up spar session then?"),
                 _("Sure, let's go!")],
                ["Scrub", "you", "Scrub", "you", "Scrub", "Scrub", "you", "Scrub", "Scrub", "you", "Scrub", "you"]
            )
            self.sparWithScrub()
            return

        self.stopSoundtrack()
        self.currentBGM = pickOne(["jy_huha.mp3", "jy_heroic.mp3", "jy_xiaoyaogu.mp3", "jy_xiaoyaogu3.mp3"])
        self.startSoundtrackThread()

        if fromWin.title() == _("Scrub's House"):
            fromWin.withdraw()

        else:
            fromWin.quit()
            fromWin.destroy()

        try:
            if self.mapWin.winfo_exists():
                self.mapWin.deiconify()
            else:
                raise
        except:

            self.mapWin = Toplevel(self.mainWin)

            self.map_image = PhotoImage(file="map.ppm")
            self.map_canvas = Canvas(master=self.mapWin, width=self.map_image.width() - 1, height=self.map_image.height() - 1,
                                     bg='white')
            self.map_canvas.create_image(0, 0, anchor=NW, image=self.map_image)

            self.map_location_coordinates = {"scrub_house": (290, 320),
                                             "eagle_sect": (170, 390),
                                             "babao_tower": (370, 190),
                                             "shanhu_sect": (30, 145),
                                             "small_town": (125, 85),
                                             "snowy_mountain": (20, 30),
                                             "nongyuan_valley": (280, 460),
                                             "wangling_road": (940, 20),
                                             "sea_shore": (1030, 75),
                                             "imperial_city": (825, 10),
                                             "phantom_sect": (340, 5),
                                             "yuelai_inn": (720, 240),
                                             "liu_village": (800, 250),
                                             "bamboo_forest": (750, 440),
                                             "forest": (900, 200),
                                             "yagyu_clan": (925, 310),
                                             "cherry_blossom_island": (1030, 425),
                                             "dark_forest": (545, 155),
                                             "huashan": (510, 15),
                                             "shaolin": (730, 20),
                                             "wudang": (640, 170),
                                             "dragon_sect": (570, 325),
                                             "tibetan_plains": (150, 240),
                                             "wanhua_pavillion": (410, 365)}


            self.current_coords = self.map_location_coordinates[self.current_map_location]

            # Scrub's House
            scrub_house_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.scrub_house_image = PhotoImage(file="scrub_house_icon.ppm")
            self.scrub_house_image_button = Button(scrub_house_frame,
                                                   command=self.return_to_scrub_house)
            self.scrub_house_image_button.pack()
            self.scrub_house_image_button.config(image=self.scrub_house_image)
            Label(scrub_house_frame, text=_("Scrub's House"), wraplength=60, bg="#d3d3d3").pack()
            scrub_house_frame.place(x=290, y=320)

            # Eagle Sect
            eagle_sect_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.eagle_sect_image = PhotoImage(file="eagle_sect_icon.ppm")
            self.eagle_sect_image_button = Button(eagle_sect_frame,
                                                      command=partial(self.travel, "eagle_sect"))
            self.eagle_sect_image_button.pack()
            self.eagle_sect_image_button.config(image=self.eagle_sect_image)
            Label(eagle_sect_frame, text=_("Eagle Sect"), wraplength=60, bg="#d3d3d3").pack()
            eagle_sect_frame.place(x=170, y=390)

            # Babao Tower
            babao_tower_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.babao_tower_image = PhotoImage(file="babao_tower_icon.ppm")
            self.babao_tower_image_button = Button(babao_tower_frame,
                                                      command=partial(self.travel, "babao_tower"))
            self.babao_tower_image_button.pack()
            self.babao_tower_image_button.config(image=self.babao_tower_image)
            Label(babao_tower_frame, text=_("Babao Tower"), wraplength=60, bg="#d3d3d3").pack()
            babao_tower_frame.place(x=370, y=190)

            # Shanhu Sect
            shanhu_sect_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.shanhu_sect_image = PhotoImage(file="shanhu_sect_icon.ppm")
            self.shanhu_sect_image_button = Button(shanhu_sect_frame,
                                                      command=partial(self.travel, "shanhu_sect"))
            self.shanhu_sect_image_button.pack()
            self.shanhu_sect_image_button.config(image=self.shanhu_sect_image)
            Label(shanhu_sect_frame, text=_("Shanhu Sect"), wraplength=60, bg="#d3d3d3").pack()
            shanhu_sect_frame.place(x=30, y=145)

            # Small Town
            small_town_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.small_town_image = PhotoImage(file="small_town_icon.ppm")
            self.small_town_image_button = Button(small_town_frame,
                                                   command=partial(self.travel, "small_town"))
            self.small_town_image_button.pack()
            self.small_town_image_button.config(image=self.small_town_image)
            Label(small_town_frame, text=_("Small Town"), wraplength=60, bg="#d3d3d3").pack()
            small_town_frame.place(x=125, y=85)

            # Snowy Mountain
            snowy_mountain_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.snowy_mountain_image = PhotoImage(file="snowy_mountain_icon.ppm")
            self.snowy_mountain_image_button = Button(snowy_mountain_frame,
                                                      command=partial(self.travel, "snowy_mountain"))
            self.snowy_mountain_image_button.pack()
            self.snowy_mountain_image_button.config(image=self.snowy_mountain_image)
            Label(snowy_mountain_frame, text=_("Snowy Mountain"), wraplength=60, bg="#d3d3d3").pack()
            snowy_mountain_frame.place(x=20, y=30)

            # Nongyuan Valley
            nongyuan_valley_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.nongyuan_valley_image = PhotoImage(file="nongyuan_valley_icon.ppm")
            self.nongyuan_valley_image_button = Button(nongyuan_valley_frame,
                                                      command=partial(self.travel, "nongyuan_valley"))
            self.nongyuan_valley_image_button.pack()
            self.nongyuan_valley_image_button.config(image=self.nongyuan_valley_image)
            Label(nongyuan_valley_frame, text=_("Nongyuan Valley"), wraplength=60, bg="#d3d3d3", font=("TkDefaultFont", 10)).pack()
            nongyuan_valley_frame.place(x=280, y=460)

            # Wangling Road
            wangling_road_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.wangling_road_image = PhotoImage(file="wangling_road_icon.ppm")
            self.wangling_road_image_button = Button(wangling_road_frame,
                                                       command=partial(self.travel, "wangling_road"))
            self.wangling_road_image_button.pack()
            self.wangling_road_image_button.config(image=self.wangling_road_image)
            Label(wangling_road_frame, text=_("Wangling Road"), wraplength=60, bg="#d3d3d3",
                  font=("TkDefaultFont", 10)).pack()
            wangling_road_frame.place(x=940, y=20)

            # Sea Shore
            sea_shore_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.sea_shore_image = PhotoImage(file="sea_shore_icon.ppm")
            self.sea_shore_image_button = Button(sea_shore_frame,
                                                     command=partial(self.travel, "sea_shore"))
            self.sea_shore_image_button.pack()
            self.sea_shore_image_button.config(image=self.sea_shore_image)
            Label(sea_shore_frame, text=_("Sea Shore"), wraplength=60, bg="#d3d3d3").pack()
            sea_shore_frame.place(x=1030, y=75)

            # Imperial City
            imperial_city_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.imperial_city_image = PhotoImage(file="imperial_city_icon.ppm")
            self.imperial_city_image_button = Button(imperial_city_frame,
                                               command=partial(self.travel, "imperial_city"))
            self.imperial_city_image_button.pack()
            self.imperial_city_image_button.config(image=self.imperial_city_image)
            Label(imperial_city_frame, text=_("Imperial City"), wraplength=60, bg="#d3d3d3").pack()
            imperial_city_frame.place(x=825, y=10)

            # Phantom Sect
            phantom_sect_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.phantom_sect_image = PhotoImage(file="phantom_sect_icon.ppm")
            self.phantom_sect_image_button = Button(phantom_sect_frame,
                                               command=partial(self.travel, "phantom_sect"))
            self.phantom_sect_image_button.pack()
            self.phantom_sect_image_button.config(image=self.phantom_sect_image)
            Label(phantom_sect_frame, text=_("Phantom Sect"), wraplength=60, bg="#d3d3d3").pack()
            phantom_sect_frame.place(x=340, y=5)

            # Inn
            inn_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.inn_image = PhotoImage(file="inn_icon.ppm")
            self.inn_image_button = Button(inn_frame, command=partial(self.travel, "yuelai_inn"))
            self.inn_image_button.pack()
            self.inn_image_button.config(image=self.inn_image)
            Label(inn_frame, text=_("Yuelai Inn"), wraplength=60, bg="#d3d3d3").pack()
            inn_frame.place(x=720, y=240)

            # Village
            village_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.village_image = PhotoImage(file="liu_village_icon.ppm")
            self.village_image_button = Button(village_frame,
                                               command=partial(self.travel, "liu_village"))
            self.village_image_button.pack()
            self.village_image_button.config(image=self.village_image)
            Label(village_frame, text=_("Liu Village"), wraplength=60, bg="#d3d3d3").pack()
            village_frame.place(x=800, y=250)

            # Bamboo Forest
            bamboo_forest_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.bamboo_forest_image = PhotoImage(file="bamboo_forest_icon.ppm")
            self.bamboo_forest_image_button = Button(bamboo_forest_frame,
                                               command=partial(self.travel, "bamboo_forest"))
            self.bamboo_forest_image_button.pack()
            self.bamboo_forest_image_button.config(image=self.bamboo_forest_image)
            Label(bamboo_forest_frame, text=_("Bamboo Forest"), wraplength=60, bg="#d3d3d3").pack()
            bamboo_forest_frame.place(x=750, y=440)

            # Forest
            forest_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.forest_image = PhotoImage(file="forest_icon.ppm")
            self.forest_image_button = Button(forest_frame,
                                              command=partial(self.travel, "forest"))
            self.forest_image_button.pack()
            self.forest_image_button.config(image=self.forest_image)
            Label(forest_frame, text=_("Forest"), wraplength=60, bg="#d3d3d3").pack()
            forest_frame.place(x=900, y=200)

            # Cherry Blossom Island
            cherry_blossom_island_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.cherry_blossom_island_image = PhotoImage(file="cherry_blossom_island_icon.ppm")
            self.cherry_blossom_island_image_button = Button(cherry_blossom_island_frame,
                                                             command=partial(self.travel, "cherry_blossom_island"))
            self.cherry_blossom_island_image_button.pack()
            self.cherry_blossom_island_image_button.config(image=self.cherry_blossom_island_image)
            Label(cherry_blossom_island_frame, text=_("Cherry Blossom Island"), wraplength=60, bg="#d3d3d3").pack()
            cherry_blossom_island_frame.place(x=1030, y=425)

            # Dark Forest
            dark_forest_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.dark_forest_image = PhotoImage(file="dark_forest_icon.ppm")
            self.dark_forest_image_button = Button(dark_forest_frame,
                                                   command=partial(self.travel, "dark_forest"))
            self.dark_forest_image_button.pack()
            self.dark_forest_image_button.config(image=self.dark_forest_image)
            Label(dark_forest_frame, text=_("Dark Forest"), wraplength=60, bg="#d3d3d3").pack()
            dark_forest_frame.place(x=545, y=155)

            # Huashan
            huashan_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.huashan_image = PhotoImage(file="huashan_icon.ppm")
            self.huashan_image_button = Button(huashan_frame,
                                                   command=partial(self.travel, "huashan"))
            self.huashan_image_button.pack()
            self.huashan_image_button.config(image=self.huashan_image)
            Label(huashan_frame, text=_("Huashan Sect"), wraplength=60, bg="#d3d3d3").pack()
            huashan_frame.place(x=510, y=15)

            # Shaolin
            shaolin_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.shaolin_image = PhotoImage(file="shaolin_icon.ppm")
            self.shaolin_image_button = Button(shaolin_frame,
                                               command=partial(self.travel, "shaolin"))
            self.shaolin_image_button.pack()
            self.shaolin_image_button.config(image=self.shaolin_image)
            Label(shaolin_frame, text=_("Shaolin Sect"), wraplength=60, bg="#d3d3d3").pack()
            shaolin_frame.place(x=730, y=20)

            # Wudang
            wudang_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.wudang_image = PhotoImage(file="wudang_icon.ppm")
            self.wudang_image_button = Button(wudang_frame,
                                               command=partial(self.travel, "wudang"))
            self.wudang_image_button.pack()
            self.wudang_image_button.config(image=self.wudang_image)
            Label(wudang_frame, text=_("Wudang Sect"), wraplength=60, bg="#d3d3d3").pack()
            wudang_frame.place(x=640, y=170)

            # Dragon Sect
            dragon_sect_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.dragon_sect_image = PhotoImage(file="dragon_sect_icon.ppm")
            self.dragon_sect_image_button = Button(dragon_sect_frame,
                                              command=partial(self.travel, "dragon_sect"))
            self.dragon_sect_image_button.pack()
            self.dragon_sect_image_button.config(image=self.dragon_sect_image)
            Label(dragon_sect_frame, text=_("Dragon Sect"), wraplength=60, bg="#d3d3d3").pack()
            dragon_sect_frame.place(x=570, y=325)

            # Yagyu Clan
            yagyu_clan_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.yagyu_clan_image = PhotoImage(file="yagyu_clan_icon.ppm")
            self.yagyu_clan_image_button = Button(yagyu_clan_frame,
                                                   command=partial(self.travel, "yagyu_clan"))
            self.yagyu_clan_image_button.pack()
            self.yagyu_clan_image_button.config(image=self.yagyu_clan_image)
            Label(yagyu_clan_frame, text=_("Yagyu Clan"), wraplength=60, bg="#d3d3d3").pack()
            yagyu_clan_frame.place(x=925, y=310)

            # Tibetan Plains
            tibetan_plains_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.tibetan_plains_image = PhotoImage(file="tibetan_plains_icon.png")
            self.tibetan_plains_image_button = Button(tibetan_plains_frame,
                                                  command=partial(self.travel, "tibetan_plains"))
            self.tibetan_plains_image_button.pack()
            self.tibetan_plains_image_button.config(image=self.tibetan_plains_image)
            Label(tibetan_plains_frame, text=_("Tibetan Plains"), wraplength=60, bg="#d3d3d3").pack()
            tibetan_plains_frame.place(x=150, y=240)

            # Wanhua Pavillion
            wanhua_pavillion_frame = Frame(self.map_canvas, bg="#d3d3d3")
            self.wanhua_pavillion_image = PhotoImage(file="wanhua_pavillion_icon.png")
            self.wanhua_pavillion_image_button = Button(wanhua_pavillion_frame,
                                                      command=partial(self.travel, "wanhua_pavillion"))
            self.wanhua_pavillion_image_button.pack()
            self.wanhua_pavillion_image_button.config(image=self.wanhua_pavillion_image)
            Label(wanhua_pavillion_frame, text=_("Wanhua Pavillion"), wraplength=60, bg="#d3d3d3").pack()
            wanhua_pavillion_frame.place(x=410, y=365)



            # Personal Icon
            self.map_location_icon_image = PhotoImage(file="you_map_location_icon.png")
            self.map_location_icon = Button(self.map_canvas)
            self.map_location_icon.pack()
            self.map_location_icon.config(image=self.map_location_icon_image)
            self.map_location_icon.place(x=self.current_coords[0], y=self.current_coords[1])

            self.map_location_image_buttons = {
                "scrub_house": self.scrub_house_image_button,
                "eagle_sect": self.eagle_sect_image_button,
                "babao_tower": self.babao_tower_image_button,
                "shanhu_sect": self.shanhu_sect_image_button,
                "small_town": self.small_town_image_button,
                "snowy_mountain": self.snowy_mountain_image_button,
                "nongyuan_valley": self.nongyuan_valley_image_button,
                "wangling_road": self.wangling_road_image_button,
                "sea_shore": self.sea_shore_image_button,
                "imperial_city": self.imperial_city_image_button,
                "phantom_sect": self.phantom_sect_image_button,
                "yuelai_inn": self.inn_image_button,
                "liu_village": self.village_image_button,
                "bamboo_forest": self.bamboo_forest_image_button,
                "forest": self.forest_image_button,
                "cherry_blossom_island": self.cherry_blossom_island_image_button,
                "dark_forest": self.dark_forest_image_button,
                "huashan": self.huashan_image_button,
                "shaolin": self.shaolin_image_button,
                "wudang": self.wudang_image_button,
                "dragon_sect": self.dragon_sect_image_button,
                "yagyu_clan": self.yagyu_clan_image_button,
                "tibetan_plains": self.tibetan_plains_image_button,
                "wanhua_pavillion": self.wanhua_pavillion_image_button,
            }
            self.update_travel_time_tooltips()

            self.game_date_label = Label(self.mapWin, text="")
            self.game_date_label.pack()
            self.update_date_label()
            self.map_canvas.pack()
            Label(self.mapWin, text="Imagery © 2020 TerraMetrics, Map Data © 2020").pack()
            self.mapWin.protocol("WM_DELETE_WINDOW", self.on_exit)
            self.mapWin.update_idletasks()
            toplevel_w, toplevel_h = self.mapWin.winfo_width(), self.mapWin.winfo_height()
            self.mapWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
            self.mapWin.focus_force()
            self.mapWin.mainloop()###


    def chivalry_task(self, destination):
        self.mapWin.withdraw()
        time.sleep(1)
        self.stopSoundtrack()
        self.generate_dialogue_sequence(
            None,
            "Shanhu Sect.png",
            [_("{}, you're in big trouble...").format(self.character),
             _("Hmmm? What do you mean?"),
             _("You've been committing too many unchivalrous acts and have become a nuisance to the Martial Arts Community..."),
             _("Hah! They're just jealous because I'm too strong..."),
             _("Well, they've formed a group to take you down... Good luck with that..."),
             _("I'm not afraid of them! Bring it! I'll show those scrubs who's boss...")],
            ["Scrub", "you", "Scrub", "you", "Scrub", "you"]
        )
        opp_pool = []
        opp = character(_("Cheng Youde"), 15 - self.taskProgressDic["chivalry"]//10,
                        sml=[special_move(_("Purity Blade"), level=10),
                             special_move(_("Basic Sword Technique"), level=10),
                             special_move(_("Taichi Sword"), level=10)],
                        skills=[skill(_("Wudang Agility Technique"), level=10),
                                skill(_("Pure Yang Qi Skill"), level=8)])
        opp_pool.append(opp)
        
        opp = character(_("Blind Sniper"), 15 - self.taskProgressDic["chivalry"]//10,
                        sml=[special_move(_("Basic Sword Technique"), level=10),
                             special_move(_("Taichi Sword"), level=8),
                             special_move(_("Blind Sniper Blade"), level=10)],
                        skills=[skill(_("Wudang Agility Technique"), level=9),
                                skill(_("Pure Yang Qi Skill"), level=9)])
        opp_pool.append(opp)

        if self.taskProgressDic["li_guiping_task"] < 3:
            opp = character(_("Elder Xuansheng"), 15 - self.taskProgressDic["chivalry"] // 10,
                            sml=[special_move(_("Shaolin Luohan Fist"), level=8),
                                 special_move(_("Shaolin Diamond Finger"), level=6)],
                            skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                    skill(_("Shaolin Inner Energy Technique"), level=10),
                                    skill(_("Shaolin Agility Technique"), level=10),])
            opp_pool.append(opp)

        opp = character(_("Elder Xuanjing"), 15 - self.taskProgressDic["chivalry"]//10,
                        sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                             special_move(_("Shaolin Diamond Finger"), level=8)],
                        skills=[skill(_("Shaolin Mind Clearing Method"), level=6),
                                skill(_("Shaolin Inner Energy Technique"), level=10),
                                skill(_("Shaolin Agility Technique"), level=10),])
        opp_pool.append(opp)

        opp = character(_("Zhou Enrui"), 15 - self.taskProgressDic["chivalry"]//10,
                        sml=[special_move(_("Taichi Fist"), level=10),
                             special_move(_("Basic Sword Technique"), level=10),
                             special_move(_("Taichi Sword"), level=9)],
                        skills=[skill(_("Wudang Agility Technique"), level=10),
                                skill(_("Pure Yang Qi Skill"), level=9)])
        opp_pool.append(opp)

        opp = character(_("Li Kefan"), 15 - self.taskProgressDic["chivalry"]//10,
                        sml=[special_move(_("Taichi Fist"), level=9),
                             special_move(_("Basic Sword Technique"), level=10),
                             special_move(_("Taichi Sword"), level=8)],
                        skills=[skill(_("Wudang Agility Technique"), level=8),
                                skill(_("Pure Yang Qi Skill"), level=8)])
        opp_pool.append(opp)
        
        opp = character(_("Zhong Yue"), 15 - self.taskProgressDic["chivalry"]//10,
                        sml=[special_move(_("Taichi Fist"), level=8),
                             special_move(_("Basic Sword Technique"), level=10),
                             special_move(_("Taichi Sword"), level=8)],
                        skills=[skill(_("Wudang Agility Technique"), level=8),
                                skill(_("Pure Yang Qi Skill"), level=7)])
        opp_pool.append(opp)

        opp = character("Jia Wen", 15 - self.taskProgressDic["chivalry"]//10,
                        sml=[special_move(_("Shanhu Fist"), level=6),
                             special_move(_("Basic Sword Technique"), level=10),
                             special_move(_("Fairy Sword Technique"), level=10),
                             special_move(_("Falling Cherry Blossom Finger Technique"), level=5), ],
                        skills=[skill(_("Divine Protection"), level=4),
                                skill(_("Leaping Over Mountain Peaks"), level=7),
                                skill(_("Cherry Blossoms Floating on the Water"), level=7)],
                        preferences=None)
        opp_pool.append(opp)

        opp = character(_("Zhu Shi Ting"), 15 - self.taskProgressDic["chivalry"]//10,
                        sml=[special_move(_("Shanhu Fist"), level=9)],
                        skills=[skill(_("Divine Protection"), level=8),
                                skill(_("Leaping Over Mountain Peaks"), level=8)])
        opp_pool.append(opp)

        opp = character(_("Yang Kai"), 15 - self.taskProgressDic["chivalry"]//10,
                        sml=[special_move(_("Shanhu Fist"), level=5),
                             special_move(_("Basic Sword Technique"), level=10),
                             special_move(_("Demon Suppressing Blade"), level=10)],
                        skills=[skill(_("Divine Protection"), level=5),
                                skill(_("Leaping Over Mountain Peaks"), level=10)])
        opp_pool.append(opp)

        opp_list = []
        shuffle(opp_pool)
        num_opps = min([4 - self.taskProgressDic["chivalry"]//20, 8])
        for i in range(num_opps):
            opp = opp_pool.pop()
            opp_list.append(opp)

        cmd = lambda: self.travel(destination, True)
        self.battleMenu(self.you, opp_list, "battleground_shanhu_sect.png", "jy_xiaoyaogu.mp3", fromWin=self.mapWin,
                        battleType="test", destinationWinList=[], postBattleCmd=cmd
        )


    def travel(self, destination, direct=False):

        r = random()

        
        if self.taskProgressDic["wanhua_pavillion"] == 9 and destination != "wanhua_pavillion":
            self.stopSoundtrack()
            self.currentBGM = "jy_suspenseful1.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("*As you travel through a forest, you hear the rustling of some leaves behind you.*"),
                 _("Who's there? Come out."),
                 _("Die, you scumbag!"),
                 _("*You duck out of the way as 3 needles fly over your head.*"),
                 _("What the heck?? Xian-er?! What did I do to you??"),
                 _("All men like you must die... I feel sorry for your wife who has to put up with someone like you!"),
                 _("You... you..."),
                 _("Hahahaha... Ever heard of the Three-Tailed Fox?"),
                 _("Three-Tailed Fox?? But I... we... that night... You didn't have any missing limbs!"),
                 _("That was only a rumor that we spread..."),
                 _("'We'...???"),
                 _("The Three-Tailed Fox does not have only 3 limbs. The Three-Tailed Fox isn't even a single person..."), #Row 2 
                 _("There are 3 of us, and together, we are the Three-Tailed Fox!"), 
                 _("I see... it's you three! You, A-Xiu, and A-Qi!"),
                 _("Hahahaha... You figured it out, but too late, I'm afraid..."),
                 _("Why? What do you have against me... or other married men, for that matter?"),
                 _("We don't have a problem with married men... only those who cheat on their wives!!!"),
                 _("My fiance slept with my cousin the day before our marriage. When I caught them, they tried to kill me!"),
                 _("A-Xiu's mother was killed by an adulterer who tricked her into falling for him..."),
                 _("A-Qi's husband divorced her for a rich woman... Our wounds, our pain... brought us together..."),
                 _("We had a common goal, and that is to kill all unfaithful men in the world!"), #Row 3
                 _("Well, I can see what role you play on the team. What about the other 2?"),
                 _("Since you are about to die, I suppose it won't hurt to let you know the details."),
                 _("A-Qi doesn't know any martial arts, but she is experienced in running a business and managing finances, which helps keep Wanhua Pavillion running."),
                 _("A-Xiu's the daughter of the Dexterous Fairy, so she's naturally a talented thief."),
                 _("Her skills are very useful in tracking customers who visit Wanhua Pavillion. I'm sure you've noticed her scent?"),
                 _("Yeah, she smells really nice... so what?"),
                 _("When she comes in contact with a customer, she will leave her scent (the Rainbow Fragrance) on them."),
                 _("After they leave, she can track them down with a special type of bee that she's raised."),
                 _("This way, we will know where they live and also verify that they are indeed married."), # Row 4
                 _("Once A-Xiu confirms the details, I strike, just like I'm doing now... Any more questions before I send you to hell?"),
                 _("Yeah... Do you think A-Xiu would marry me in another life?"),
                 _("Idiot...")],
                ["Blank", "you", "Xian-er", "Blank", "you", "Xian-er", "you", "Xian-er", "you", "Xian-er", "you",
                 "Xian-er", "Xian-er", "you", "Xian-er", "you", "Xian-er", "Xian-er", "Xian-er", "Xian-er",
                 "Xian-er", "you", "Xian-er", "Xian-er", "Xian-er", "Xian-er", "you", "Xian-er", "Xian-er",
                 "Xian-er", "Xian-er", "you", "Xian-er"],
            )

            opp = character(_("Xian-er"), 18,
                            sml=[special_move(_("Poison Needle"), level=10),
                                 special_move(_("Rainbow Fragrance"), level=10),],
                            skills=[skill(_("Seductive Dance"), level=10),
                                    skill(_("Basic Agility Technique"), level=10),
                                    skill(_("Dragonfly Dance"), level=10)],
                            preferences=[3, 1, 3, 1, 1])
            self.battleMenu(self.you, opp, "Dark Forest.png", "jy_shendiaoshuhuan.mp3", fromWin=None,
                            battleType = "task", destinationWinList = [None]*3, postBattleCmd = self.postXianErFight)
            return

        
        if self.taskProgressDic["yagyu_clan"] == -10 and destination != "shaolin":
            self.stopSoundtrack()
            self.currentBGM = "jy_suspenseful1.mp3"
            self.startSoundtrackThread()

            self.taskProgressDic["yagyu_clan"] = -11
            self.mapWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("*As you travel through a forest, you notice the rustling of some leaves behind you.*"),
                 _("Who's there? You've been following me for quite a while now... Come out."),
                 _("..."),
                 _("Who are you? What do you want?"),
                 _("All you need to know is that you must die.")
                 ],
                ["Blank", "you", "Hattori Ninja", "you", "Hattori Ninja"],
            )

            opp = character(_("Hattori Ninja"), 16,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Superior Smoke Bomb"), level=10),
                                 special_move(_("Spider Poison Powder"), level=10),
                                 special_move(_("Centipede Poison Powder"), level=10)],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Ninjutsu I"), level=10),
                                    skill(_("Ninjutsu II"), level=5)],
                            preferences=[2, 1, 3, 1, 2])
            self.battleID = "hattori_ninjas"
            cmd = lambda: self.travel(destination)
            self.battleMenu(self.you, opp, "Dark Forest.png", "jy_suspenseful1.mp3", fromWin=None,
                            battleType = "task", destinationWinList = [None]*3, postBattleCmd = cmd)
            return

        elif self.taskProgressDic["yagyu_clan"] == -11:
            self.taskProgressDic["yagyu_clan"] = -12
            self.mapWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("I don't go down that easily..."),
                 _("You have yet to see the true skill of Hattori ninjas...")],
                ["you", "Hattori Ninja"],
            )

            self.currentEffect = "sfx_shapeless_palm.wav"
            self.startSoundEffectThread()
            time.sleep(.5)
            self.currentEffect = "sfx_smoke_bomb.mp3"
            self.startSoundEffectThread()
            time.sleep(1)

            self.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("You had a chance to live, but you chose to be nosy..."),
                 _("Now, you pay.")],
                ["Shadow Clones", "Shadow Clones"],
            )

            opp = character(_("Shadow Clones"), 16,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Superior Smoke Bomb"), level=10),
                                 special_move(_("Spider Poison Powder"), level=10),],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Ninjutsu I"), level=10),
                                    skill(_("Ninjutsu II"), level=5)],
                            preferences=[2, 1, 3, 1, 2],
                            multiple=5)
            self.battleID = "hattori_ninjas"
            cmd = lambda: self.travel(destination)
            self.battleMenu(self.you, opp, "Dark Forest.png", "jy_suspenseful1.mp3", fromWin=None,
                            battleType = "task", destinationWinList = [None]*3, postBattleCmd = cmd)
            return

        elif self.taskProgressDic["yagyu_clan"] == -12:
            self.battleID = None
            self.taskProgressDic["yagyu_clan"] = -13
            self.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("We will meet again...")],
                ["Shadow Clones"],
            )
            self.currentEffect = "sfx_smoke_bomb.mp3"
            self.startSoundEffectThread()
            time.sleep(1)

            self.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Yagyu Tadashi wants me dead, it seems..."),
                 _("More motivation for me to mess with his plans...")],
                ["you", "you"],
            )
            self.mapWin.deiconify()
            return

        elif self.chivalry <= -100 and r <= max([.4 + self.taskProgressDic["chivalry"]/100, .2]) and not direct and self.taskProgressDic["chivalry"] >= -50 and self.taskProgressDic["supreme_leader"] != 1:
            self.taskProgressDic["chivalry"] -= 10
            self.chivalry_task(destination)

        elif self.taskProgressDic["join_sect"] == -1 and (self.level >= 10 or self.gameDate >= 365):
            self.mapWin.withdraw()
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("Hey, I see you've been leveling up quite a bit."),
                 _("Would you like me to introduce you to a master with whom you can train?")],
                ["Scrub", "Scrub"],
                [[_("Sure, what options do you have for me?"), self.join_sect],
                 [_("It's ok, I don't need a master."), self.force_go_rogue]]
            )
            return

        elif self.taskProgressDic["huashanlunjian"] == -1 and calculate_month_day_year(self.gameDate)["Month"] == 9 and self.level>=20:
            self.mapWin.withdraw()
            self.taskProgressDic["huashanlunjian"] = 0
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("Hey, {}. The Annual Duel at Huashan (Hua Shan Lun Jian) is coming up.").format(self.character),
                 _("Would you like to join?"),
                 _("What's that...?"),
                 _("Come talk to me for more information.")],
                ["Scrub", "Scrub", "you", "Scrub"],
            )
            return

        elif self.taskProgressDic["yagyu_clan"] > 30 and self.taskProgressDic["yagyu_clan"] < 10000:
            self.mapWin.withdraw()
            if self.gameDate - self.taskProgressDic["yagyu_clan"] >= 30:
                self.generate_dialogue_sequence(
                    None,
                    "forest.ppm",
                    [_("Hmmm, I'd better head back to the Yagyu Clan. The tournament is starting soon.")],
                    ["you"],
                    [[_("Head back to Yagyu Clan."), partial(self.yagyu_clan_tournament, 1)],
                     [_("Forget it. I don't want to help them anymore."), partial(self.yagyu_clan_tournament, 0)]]
                )

                self.update_map_location(destination)
                self.update_date_label()
                return

        elif self.taskProgressDic["supreme_leader"] == 1 and self.chivalry <= -200:
            self.update_map_location(destination)
            self.update_date_label()
            self.mapWin.withdraw()
            time.sleep(.5)
            self.stopSoundtrack()
            self.generate_dialogue_sequence(
                None,
                "Shanhu Sect.png",
                [_("{}, I am extremely disappointed...").format(self.character),
                 _("Hmmm? What do you mean?"),
                 _("You've committed too many unchivalrous acts and have become a disgrace to the Martial Arts Community..."),
                 _("You are no longer fit to be the Supreme Leader of Wulin."),
                 _("Shut up, you Scrub. How dare you talk to me like that?"),
                 _("*Sigh*... There's no more hope for you..."),
                 _("What? You jealous of how powerful I am? I'm practically invincible now, hahahahaha!"),
                 _("You've become very strong, yes, but at what cost? People don't even want to talk to you anymore because of the shameful things you've done."),
                 _("You don't get it, Scrub. You'll never be as powerful as I am, so you won't understand."),
                 _("Now, get lost. I'm sick and tired of your babbling..."),
                 _("Oh, don't worry; I'll leave... once I take care of you, that is."),
                 _("Are you out of your mind? I would rip you into pieces within seconds."),
                 _("I'll give you 2 options. One: go immediately to a parallel universe and restart your life there from scratch."), #Row 2
                 _("Or two: I'll dig a hole and bury you myself since no one else will bother to do it."),],
                ["Scrub", "you", "Scrub", "Scrub", "you", "Scrub", "you", "Scrub", "you", "you", "Scrub", "you",
                 "Scrub", "Scrub"],
                [[_("Fine; take me to the parallel universe."), partial(self.supreme_leader_scrub_challenge, 1)],
                 [_("How about I bury you instead?"), partial(self.supreme_leader_scrub_challenge, 2)]]
            )
            return

        elif self.taskProgressDic["supreme_leader"] == 1 and destination in SECT_ATTRIBUTES:
            self.update_map_location(destination)
            self.update_date_label()
            self.mapWin.withdraw()
            self.visit_sect_as_supreme_leader(destination)
            return

        elif self.taskProgressDic["blind_sniper"] > 9 and self.taskProgressDic["blind_sniper"] < 1000000:
            if self.gameDate - self.taskProgressDic["blind_sniper"] >= 10:
                self.mapWin.withdraw()
                self.taskProgressDic["blind_sniper"] = 1000000
                self.generate_dialogue_sequence(
                    None,
                    "battleground_imperial_palace_night.png",
                    [_("(Ok, it's time to rescue the Empress...)"),
                     _("*You sneak through the same tunnel that you dug previously...*"),
                     _("Alright, are you ready to g--")],
                    ["you", "Blank", "you"]
                )

                self.stopSoundtrack()
                self.currentBGM = "xlfd_suspense.mp3"
                self.startSoundtrackThread()

                self.generate_dialogue_sequence(
                    None,
                    "battleground_imperial_palace_night.png",
                    [_("You insolent peasant! How dare you trespass the Empress's quarters at night?"),
                     _(".........."),
                     _("Guards, arrest this assassin, or kill him if he resists."),
                     _("You evil woman! How could you betray me?? Why are you doing this?!"),
                     _("Since you are a dead man, I might as well tell you..."),
                     _("Back when I was a young, naive little girl, I thought that I would be happy forever as long as I married the man I loved."),
                     _("I couldn't understand why my parents opposed my relationship with Liang Zhe and even argued with them at times."),
                     _("The day before we decided to elope, my father must've detected something different about my behavior."),
                     _("Just to be safe, he hired some men to follow me. Those men were the assassins we met in the forest."),
                     _("So it wasn't the Imperial Agents... It was your father's men..."), #Row 2
                     _("That's right. When I found out, I was initially furious with my father, but there was nothing I could do."),
                     _("It wasn't until I married the current Emperor that I realized how right my parents were."),
                     _("There is nothing more gratifying than having endless riches and power! To think I almost gave it up for 'love'... Hahahaha, how laughable..."),
                     _("I can't go back to the despicable life of a peasant, but you seemed so persistent on 'helping' me."),
                     _("You could've just refused. Why do you have to set me up like this?"),
                     _("I can't have a peasant like you spreading stories about my past... I need to silence you, just in case."),
                     _("It's a pity... If you had been a coward and not returned, you would be alive."),
                     _("Don't worry though; I'll order that you be buried properly."),
                     _("Good bye, friend of Liang Zhe..."), #Row 3
                     _("Guards! Get him!"),
                     _("(Crap... I'd better defeat these goons quickly and get out of here before more guards arrive...)")],
                    ["Imperial Eunuch", "you", "Empress", "you", "Empress", "Empress", "Empress", "Empress", "Empress",
                     "you", "Empress", "Empress", "Empress", "Empress", "you", "Empress", "Empress", "Empress",
                     "Empress", "Imperial Eunuch", "you"]
                )

                opp_list = [character(_("Archers"), 10,
                                attributeList=[20000, 20000, 1000, 1000, 200, 200, 300, 20, 10],
                                sml=[special_move(_("Archery"), level=10)]),
                            character(_("Spearmen"), 10,
                                attributeList=[25000, 25000, 1000, 1000, 190, 170, 200, 100, 10],
                                sml=[special_move(_("Spear Attack"), level=10)]),
                            character(_("Imperial Eunuch"), 20,
                                      sml=[special_move(_("Hun Yuan Shapeshifting Fist"), level=10)],
                                      skills=[skill(_("Hun Yuan Yin Qi"), level=10)])
                ]

                
                self.battleMenu(self.you, opp_list, "battleground_imperial_palace_night.png",
                                          postBattleSoundtrack="jy_daojiangu.mp3",
                                          fromWin=None,
                                          battleType="task", destinationWinList=[self.mapWin] * 3)
                
                


        self.update_map_location(destination)
        self.update_date_label()
        
        if destination == "snowy_mountain":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_mountain_scenery.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_snowy_mountain(self)

        elif destination == "bamboo_forest":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_shendiaoshuhuan.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_bamboo_forest(self)

        elif destination == "forest":
            self.mapWin.withdraw()
            if self.taskProgressDic["wudang_task"] == 1:
                self.wu_hua_que_forest_encounter()
            else:
                threshold = .2
                if self.taskProgressDic["join_sect"] == -1:
                    threshold = .4

                if random() <= threshold:
                    self.robber_encounter()
                else:
                    self.go_forest()

        elif destination == "cherry_blossom_island":

            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_shendiaogeqi.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_cherry_blossom_island(self)

        elif destination == "yuelai_inn":

            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_relaxing2.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            self.go_inn()

        elif destination == "liu_village":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_suspenseful2.mp3"
            self.startSoundtrackThread()

            if self.taskProgressDic["liu_village"] == -1:
                self.generate_dialogue_sequence(
                    self.mapWin,
                    "liu_village.ppm",
                    [_("(Hmmm, an abandoned village... looks like some fights took place here. Wonder what happened...)")],
                    ["you"]
                )
                self.taskProgressDic["liu_village"] = 0
            self.mapWin.withdraw()
            Scene_liu_village(self)
        
        elif destination == "nongyuan_valley":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_huanle.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_nongyuan_valley(self)


        elif destination == "shanhu_sect":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_ambient3.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_shanhu_sect(self)


        elif destination == "wudang":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()

            if self.taskProgressDic["wudang_task"] == -2000:
                messagebox.showinfo("", _("You decide it's probably better not to go back only to have the Wudang priests bother you..."))
                return
            elif self.taskProgressDic["li_guiying"] in [103, 113]:
                messagebox.showinfo("", _("You decide it's probably better not to go to Wudang after the incident at Liu Village..."))
                return
            else:
                self.stopSoundtrack()
                self.currentBGM = "jy_wudang.mp3"
                self.startSoundtrackThread()
                self.mapWin.withdraw()
                Scene_wudang(self)


        elif destination == "huashan":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_shendiaozhuti.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_huashan(self)


        elif destination == "shaolin":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            if self.taskProgressDic["li_guiping_task"] == 2 and _("Ninja Suit") in [eq.name for eq in self.equipment]:
                response = messagebox.askquestion("", "Sneak into Shaolin at night to assassinate Elder Xuansheng?")
                if response == "yes":
                    self.assassinate_xuan_sheng()
                    return

            elif self.taskProgressDic["feng_xinyi"] >= 4 and self.taskProgressDic["feng_pan"] in [11, 70] and _("Ninja Suit") in [eq.name for eq in self.equipment]:
                response = messagebox.askquestion("", "Sneak into the Shaolin Scripture Library at night?")
                if response == "yes":
                    self.mapWin.withdraw()
                    self.stopSoundtrack()
                    self.currentBGM = "jy_shanguxingjin2.mp3"
                    self.startSoundtrackThread()
                    if self.taskProgressDic["yagyu_clan"] in [-4,-10,-13,-14,-15,-16,-17,10,11] or self.taskProgressDic["yagyu_clan"] >= 30:
                        self.generate_dialogue_sequence(
                            self.mapWin,
                            "battleground_shaolin_night.png",
                            [_( "(Hmmm, this place is much more heavily guarded than usual.)"),
                             _("(There's no way I can sneak in right now...)")],
                            ["you", "you"],
                        )
                    else:
                        self.mini_game_in_progress = True
                        self.mini_game = shaolin_sneak_mini_game(self, None)
                        self.mini_game.new()
                    return

            self.stopSoundtrack()
            self.currentBGM = "jy_shaolin1.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_shaolin(self)
            

        elif destination == "imperial_city":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_daojiangu.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_imperial_city(self)


        elif destination == "dragon_sect":
            if self.taskProgressDic["ouyang_nana"] in [200,300]:
                messagebox.showinfo("", _("You decide you'd better not go back there after what happened..."))
            elif self.taskProgressDic["ouyang_nana"] < 100:
                self.currentEffect = "button-20.mp3"
                self.startSoundEffectThread()
                Scene_dragon_sect(self)
            else:
                self.currentEffect = "button-20.mp3"
                self.startSoundEffectThread()
                Scene_dragon_sect(self, "Normal")

        elif destination == "phantom_sect":
            Scene_phantom_sect(self)
            
        elif destination == "babao_tower":
            if self.taskProgressDic["blind_sniper"] == -1000:
                messagebox.showinfo("", _("You decide you'd better not go back there after what you did to Nie Tu..."))
            else:
                self.currentEffect = "button-20.mp3"
                self.startSoundEffectThread()
                self.stopSoundtrack()
                self.mapWin.withdraw()
                self.currentBGM = "linhai_huanqin.mp3"
                self.startSoundtrackThread()
                Scene_babao_tower(self)

        elif destination == "sea_shore":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "sea_bg.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_sea_shore(self)

        elif destination == "wangling_road":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_cheerful3.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_wangling_road(self)
            
        elif destination == "eagle_sect":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_shanguxingjin.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_eagle_sect(self)
            
        elif destination == "dark_forest":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "Kindaichi 4.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_dark_forest(self)
            #self.currentBGM = "jy_shanguxingjin2.mp3"
            #self.startSoundtrackThread()
            #self.mini_game_in_progress = True
            #self.mini_game = dark_forest_mini_game(self, None)
            #self.mini_game.new()

        elif destination == "wanhua_pavillion":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_pipayu.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_wanhua_pavillion(self)

        elif destination == "tibetan_plains":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_ambient2.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_tibetan_plains(self)

        elif destination == "small_town":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            if self.taskProgressDic["small_town"] in [20, 200]:
                self.currentBGM = "txdy_gaojinjiu.mp3"
            elif self.taskProgressDic["small_town_ghost"] not in [2]:
                self.currentBGM = "jy_tianwaicun.mp3"
            else:
                self.currentBGM = "bqt_investigate.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_small_town(self)

        elif destination == "yagyu_clan":
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            if self.level < 15:
                self.generate_dialogue_sequence(
                    self.mapWin,
                    "Yagyu Clan BG.png",
                    [_("The Yagyu Clan does not welcome the weak.")],
                    ["Ashikaga Yoshiro"]
                )
            else:
                self.stopSoundtrack()
                if self.taskProgressDic["yagyu_clan"] <= -3:
                    self.currentBGM = "jy_suspenseful1.mp3"
                else:   
                    self.currentBGM = "Sakura_Variation.mp3"
                self.startSoundtrackThread()
                self.mapWin.withdraw()
                Scene_yagyu_clan(self)
            
        else:
            messagebox.showinfo("", _("Area under development."))
            return

    
    def update_map_location(self, destination):
        self.gameDate += int(calculate_distance(self.current_coords,
                               self.map_location_coordinates[destination]) // 50)
        self.current_map_location = destination
        self.current_coords = self.map_location_coordinates[self.current_map_location]
        self.map_location_icon.place(x=self.current_coords[0], y=self.current_coords[1])
        self.update_travel_time_tooltips()


    def update_travel_time_tooltips(self):
        for location_str, image_button in self.map_location_image_buttons.items():
            string = _("Travel Time: {} days").format(int(calculate_distance(self.current_coords,
                                                                    self.map_location_coordinates[location_str]) // 50))
            self.balloon.bind(image_button, string)


    def update_date_label(self):
        game_date_dic = calculate_month_day_year(self.gameDate)
        self.game_date_label.config(text=_("Year: {}    Month: {}    Day: {}").format(game_date_dic["Year"],
                                                                                      game_date_dic["Month"],
                                                                                      game_date_dic["Day"],))


    def postXianErFight(self, choice=0):
        if choice == 0:
            self.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("I lost; do whatever you want, you scumbag..."),
                 _("Hmmmm, you know there's a bounty on your head right? I can hand you over to the officials and become rich!"),
                 _("I'm not afraid to die... It's just a pity I can't rid the world of heartless men...")],
                ["Xian-er", "you", "Xian-er"],
                [[_("Let her go."), partial(self.postXianErFight, 1)],
                 [_("Turn her in and claim 10000 Gold."), partial(self.postXianErFight, 2)]]
            )

        elif choice == 1:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.taskProgressDic["wanhua_pavillion"] = 10
            self.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Then perhaps you should continue on... "),
                 _("Wh-what...?"),
                 _("I thought about what you said; you are doing a good deed."),
                 _("Though to the world, you are just a cold-hearted, envious murderer, you are actually serving justice to those who are unfaithful to their spouse."),
                 _("I cannot in good conscience hand you over to the government..."),
                 _("You... you are not as terrible as I thought..."),
                 _("Your words were a wake-up call for me. I should not sleep around with random women..."),
                 _("I promise to change from now on."),
                 _("I'm glad to hear that."),
                 _("I have nothing to give you, but perhaps this is the only way I can repay you for your mercy."),
                 _("*Xian-er gives you some 'Rainbow Fragrance' and teaches you how to use it.*"),
                 _("Perhaps this will be useful for you when you need to defend yourself."),
                 _("If you run out of the powder, you can purchase more from A-Xiu. Good bye...")],
                ["you", "Xian-er", "you", "you", "you", "Xian-er", "you", "you", "Xian-er", "Xian-er", "Blank",
                 "Xian-er", "Xian-er"],
            )
            self.add_item(_("Rainbow Fragrance"), 20)
            new_move = special_move(name=_("Rainbow Fragrance"), level=10)
            self.learn_move(new_move)
            self.mapWin.deiconify()

        elif choice == 2:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.add_item(_("Gold"), 10000)
            self.taskProgressDic["wanhua_pavillion"] = 20
            self.generate_dialogue_sequence(
                None,
                "Dark Forest.png",
                [_("Come on, let's go... I have to turn you in now..."),
                 _("Before I go, I have one last request..."),
                 _("What is it?"),
                 _("I'm the one who murdered all those men. A-Xiu and A-Qi's hands are clean."),
                 _("Please do not harm them..."),
                 _("Very well, I promise you that."),
                 _("Thank you..."),
                 _("*Xian-er looks up at the night sky for a few seconds and closes her eyes.*"),
                 _("*Suddenly, a stream of blood bursts from her mouth, and soon she stops breathing.*"),
                 _("*It appears that she committed suicide by biting her tongue.*"),
                 _("*You bring her body to the government officials and receive 10000 Gold.*"),
                 ],
                ["you", "Xian-er", "you", "Xian-er", "Xian-er", "you", "Xian-er", "Blank", "Blank", "Blank", "Blank"],
            )

            self.mapWin.deiconify()



    def visit_sect_as_supreme_leader(self, destination):

        rep = SECT_ATTRIBUTES[destination]["rep"]
        if self.taskProgressDic["guo_zhiqiang"] in [16, 17] and destination == "huashan":
            rep = "Zhao Huilin"
        elif self.taskProgressDic["liu_village"] >= 1000 and destination == "huashan":
            self.generate_dialogue_sequence(
                self.mapWin,
                "Huashan.png",
                [_("(After what happened with Guo Junda, Huashan Sect is barely existent...)"),
                 _("(There's not much to be gained from interacting with them...)")],
                ["you", "you"],
            )
            return

        self.generate_dialogue_sequence(
            None,
            SECT_ATTRIBUTES[destination]["bg"],
            [_("Greetings, Supreme Leader, what can we do for you?")],
            [rep],
            [[_("Collect the annual tribute."), partial(self.collect_tribute, destination, rep, "tribute")],
             [_("Demand sect treasures."), partial(self.collect_tribute, destination, rep, "treasures")],
             [_("Leave."), partial(self.collect_tribute, destination, rep, "leave")]]
        )


    def collect_tribute(self, destination, rep, purpose):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()

        if purpose == "tribute":
            if self.taskProgressDic[destination+"_tribute"] == -100:
                messagebox.showinfo("", _("You can no longer collect any tribute from this sect."))
            elif self.gameDate - self.taskProgressDic[destination+"_tribute"] >= 365 or self.taskProgressDic[destination+"_tribute"] == -1:
                self.taskProgressDic[destination + "_tribute"] = self.gameDate
                amount = SECT_ATTRIBUTES[destination]["tribute"]
                self.add_item(_("Gold"), amount)
                self.generate_dialogue_sequence(
                    None,
                    SECT_ATTRIBUTES[destination]["bg"],
                    [_("Please accept our humble tribute, Supreme Leader."),
                     _("*Received Gold x {}!*").format(amount)],
                    [rep, "Blank"],
                )
            else:
                ddays = int(365 - (self.gameDate - self.taskProgressDic[destination+"_tribute"]))
                messagebox.showinfo("", _("You must wait another {} days before collecting tribute from this sect.").format(ddays))


        elif purpose == "treasures":
            if self.taskProgressDic[destination+"_tribute"] == -100:
                messagebox.showinfo("", _("You can no longer collect any tribute from this sect."))
            else:
                treasures = SECT_ATTRIBUTES[destination]["treasures"]
                r_str = "Received: "
                for treasure in treasures:
                    r_str += treasure + ", "
                    self.add_item(treasure)
                r_str = r_str[:-2]

                d_chivalry = SECT_ATTRIBUTES[destination]["chivalry"]
                if d_chivalry > 0:
                    self.chivalry -= d_chivalry
                    r_str += ". Your chivalry - {}!".format(d_chivalry)

                self.taskProgressDic[destination + "_tribute"] = -100
                self.generate_dialogue_sequence(
                    None,
                    SECT_ATTRIBUTES[destination]["bg"],
                    [_("Hand over your most prized treasures and manuals!"),
                     _("What?? That is outrageous!"),
                     _("Do it now, or I, as Supreme Leader, will kill all of your members and disband your sect."),
                     _("If you obey my command, I will no longer require any tribute from you, and you can continue leading your sect in peace."),
                     _("I should've known you couldn't be trusted. Your dishonorable acts will be made known to everyone in Wulin."),
                     r_str],
                    ["you", rep, "you", "you", rep, "Blank"],
                )

        self.mapWin.deiconify()


    def supreme_leader_scrub_challenge(self, choice):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        if choice == 1:
            self.generate_dialogue_sequence(
                None,
                "Shanhu Sect.png",
                [_("Ok, I hope you can learn from your mistakes and be a better person this time...")],
                ["Scrub"],
            )
            self.transfer_to_parallel_universe()
        elif choice == 2:
            self.generate_dialogue_sequence(
                None,
                "Shanhu Sect.png",
                [_("Stubborn...")],
                ["Scrub"],
            )

            opp = character("Scrub", 30+self.level,
                            sml=[special_move(_("Real Scrub Fist"), level=10),
                                 special_move(_("Fire Palm"), level=10),
                                 special_move(_("Falling Cherry Blossom Finger Technique"), level=10),
                                 special_move(_("Ice Palm"), level=10),
                                 special_move(_("Five Poison Palm"), level=10),
                                 special_move(_("Empty Force Fist"), level=10)],
                            skills=[skill(_("Divine Protection"), level=5),
                                    skill(_("Tendon Changing Technique"), level=5),
                                    skill(_("Huashan Agility Technique"), level=10),
                                    skill(_("Pure Yang Qi Skill"), level=10),
                                    skill(_("Phantom Steps"), level=10),
                                    skill(_("Flower and Body Unite"), level=10),
                                    skill(_("Countering Poison With Poison"), level=10),
                                    skill(_("Burst of Potential"), level=10), ],
                            preferences=None)

            self.battleMenu(self.you, opp, "Shanhu Sect.png", "jy_calm2.mp3", fromWin=None,
                            battleType="task", destinationWinList=[])



    def yagyu_clan_tournament(self, choice):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        if choice == 1:
            self.taskProgressDic["yagyu_clan"] = 10000
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_suspenseful1.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            Scene_yagyu_clan(self)
        else:
            self.taskProgressDic["yagyu_clan"] = 999999999
            messagebox.showinfo("", _("Your relationship with the Yagyu Clan has broken down such that you are no longer allies."))
            self.mapWin.deiconify()



    def assassinate_xuan_sheng(self, stage=0):

        if self.taskProgressDic["yagyu_clan"] in [-4,-10,-13,-14,-15,-16,-17,10,11] or self.taskProgressDic["yagyu_clan"] >= 30:
            self.generate_dialogue_sequence(
                self.mapWin,
                "battleground_shaolin_night.png",
                [_("(Hmmm, this place is much more heavily guarded than usual.)"),
                 _("(There's no way I can sneak in right now...)")],
                ["you", "you"],
            )


        elif stage == 0:
            self.mapWin.withdraw()
            self.stopSoundtrack()
            self.currentBGM = "jy_shanguxingjin2.mp3"
            self.startSoundtrackThread()
            self.mini_game_in_progress = True
            self.mini_game = assassinate_xuan_sheng_mini_game(self, None)
            self.mini_game.new()
            
        elif stage == 1:
            self.mini_game_in_progress = False
            pg.display.quit()
            self.generate_dialogue_sequence(
                None,
                "battleground_shaolin_night.png",
                [_("Amitabha... Sir, if you are looking for the Scripture Library, I'm afraid you are terribly lost."),
                 _("...")],
                ["Elder Xuansheng", "you"],
            )

            opp = character(_("Elder Xuansheng"), 17,
                      sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                           special_move(_("Shaolin Diamond Finger"), level=7)],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                              skill(_("Shaolin Inner Energy Technique"), level=10),
                              skill(_("Shaolin Agility Technique"), level=10),])

            cmd = lambda: self.assassinate_xuan_sheng(2)
            self.battleMenu(self.you, opp, "battleground_shaolin_night.png", "luohanzhen.mp3",
                            fromWin=None, battleType = "task", destinationWinList = [], postBattleCmd=cmd)
            
        elif stage == 2:
            self.taskProgressDic["li_guiping_task"] = 3
            self.generate_dialogue_sequence(
                None,
                "battleground_shaolin_night.png",
                [_("Ugh-ahhhhhhh!!!"),
                 _("Hah! Mission accomplished! Now to get out of here..."),
                 _("What was that noise?!"),
                 _("Elder Xuansheng, are you--"),
                 _("Elder Xuansheng!!!"),
                 _("Who's that dark figure over there??!! He's got to be the murderer! Get him!"),
                 _("(Uh oh... better run...)")],
                ["Elder Xuansheng", "you", "Luohan Monk", "Luohan Monk", "Luohan Monk", "Luohan Monk", "you"],
            )

            self.mini_game_in_progress = True
            self.mini_game = escape_from_shaolin_mini_game(self, None)
            self.mini_game.new()
        
    
    def post_xuan_sheng_assassination(self):
        self.mini_game_in_progress = True
        pg.display.quit()
        self.mapWin.deiconify()


    def resume_mini_game(self, soundtrack, drop_coords=None):
        self.mini_game.running = True
        if soundtrack:
            self.stopSoundtrack()
            self.currentBGM = soundtrack
            self.startSoundtrackThread()
        if drop_coords:
            self.mini_game.generate_drop(*drop_coords)
        self.mini_game.run()


    def go_inn(self):

        if self.taskProgressDic["feng_pan"] == 20:
            time.sleep(.5)
            self.stopSoundtrack()
            self.currentBGM = "xlfd_suspense.mp3"
            self.startSoundtrackThread()
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("Xinyi! Come home with me at once!"),
                 _("D-dad?! How did you find me?"),
                 _("Did you really think you'd be invisible with that silly disguise?"),
                 _("I had some of the best detectives in the Imperial City searching for you."),
                 _("And with a 10000 Gold reward, many citizens provided useful information."),
                 _("Now quit fooling around and come home."),
                 _("No dad, I'm not going home... not unless you promise to give me more freedom."), #Row 2
                 _("More freedom?! Oh look at all this trouble you've caused, and you still want more freedom?"),
                 _("I just want to learn martial arts. Why can't--"),
                 _("Enough! I'm not going to ask you again. Are you coming with me willingly or do I have to use force?"),
                 _("Hey! Leave her alone!"),
                 _("Who the heck are you?"),
                 _("{}! Run! Don't get involved in this!").format(self.character), #Row 3
                 _("Oh no, I'm not going to abandon you."),
                 _("Peasant, do you have any idea who you are talking to right now?"),
                 _("My father is General Feng Pan, military leader of 3 provinces, creator of the Shapeless Palm technique."),
                 _("We're in the middle of some family matter right now and can't be bothered teaching peasants like you a lesson."),
                 _("So get out of here before my father changes his mind."),
                 _("Shapeless Palm eh? How about I make your face shapeless too?"), #Row 4
                 _("Insolent peasant! You're looking for death!")
                 ],
                ["Feng Pan", "Customer", "Feng Pan", "Feng Pan", "Feng Pan", "Feng Pan",
                 "Customer", "Feng Pan", "Customer", "Feng Pan", "you", "Feng Pan",
                 "Customer", "you", "Feng Junyi", "Feng Junyi", "Feng Junyi", "Feng Junyi",
                 "you", "Feng Junyi"]
            )

            self.taskProgressDic["feng_pan"] = 21
            opp = character(_("Feng Junyi"), 14,
                            sml=[special_move(_("Shapeless Palm"), level=8), ],
                            skills=[skill(_("Shaolin Inner Energy Technique"), level=4),
                                    skill(_("Wudang Agility Technique"), level=4),
                                    skill(_("Pure Yang Qi Skill"), level=4),
                                    skill(_("Basic Agility Technique"), level=10)
                                    ],
                            preferences=[1,2,2,1,1])

            self.battleMenu(self.you, opp, "battleground_inn.png", "jy_daojiangu.mp3",
                                 fromWin=None, battleType="task",
                                 destinationWinList=[], postBattleCmd=self.go_inn
                                 )
            return
        
        elif self.taskProgressDic["feng_pan"] == 21:
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("Hah... Is that all you've got?"),
                 _("Step aside, Junyi... Let me teach this youngster a lesson...")
                 ],
                ["you", "Feng Pan"]
            )

            self.taskProgressDic["feng_pan"] = 22
            self.battleID = "feng_pan"
            opp = character(_("Feng Pan"), 20,
                            sml=[special_move(_("Shapeless Palm"), level=10),
                                 special_move(_("Shaolin Diamond Finger"), level=8),],
                            skills=[skill(_("Shaolin Inner Energy Technique"), level=8),
                                    skill(_("Wudang Agility Technique"), level=8),
                                    skill(_("Pure Yang Qi Skill"), level=8),
                                    skill(_("Basic Agility Technique"), level=10)
                                    ],
                            equipmentList=[Equipment(_("Leather Boots")),
                                           Equipment(_("Golden Gloves")),
                                           Equipment(_("Golden Chainmail"))],
                            preferences=[1,2,2,1,1])

            self.battleMenu(self.you, opp, "battleground_inn.png", "jy_daojiangu.mp3",
                                 fromWin=None, battleType="task",
                                 destinationWinList=[], postBattleCmd=self.go_inn
                                 )
            return

        elif self.taskProgressDic["feng_pan"] == 22:
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("Argh! I didn't want to do this, but you're making me..."),
                 _("Archers! Finish him!")
                 ],
                ["Feng Pan", "Feng Pan"]
            )

            self.taskProgressDic["feng_pan"] = 23
            self.battleID = "feng_pan"
            opp = character(_("Archers"), 15,
                            attributeList=[20000, 20000, 500, 500, 150, 250, 300, 20, 10],
                            sml=[special_move(_("Archery"), level=10)]
                            )

            self.battleMenu(self.you, opp, "battleground_inn.png", "jy_daojiangu.mp3",
                                 fromWin=None, battleType="task",
                                 destinationWinList=[], postBattleCmd=self.go_inn
                                 )
            return
        
        elif self.taskProgressDic["feng_pan"] == 23:
            self.taskProgressDic["feng_pan"] = 24
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("(Although I can escape alone without problem, it'll be difficult to bring Xinyi with me.)"),
                 _("(What should I do?)"),
                 _("Keep firing! Don't stop! Let's see how long he can last!"),
                 _("Hold it!"),
                 _("Who are you? Get out of the way!"),
                 _("Greetings. I'm Yang Kai, one of the elders of Shanhu Sect."),
                 _("I happened to pass by this place and couldn't help but do my best to prevent unnecessary bloodshed."),
                 _("Then you're aware, I assume, of my position in the government..."),
                 _("Yes, of course, General Feng."),  # Row 2
                 _("The government and people of Wulin have always stayed out of each other's business... I hope things remain that way."),
                 _("I'm afraid I can't let these 2 youngsters die."),
                 _("Very well, my target is this little brat, not you. If you decide to jump in to protect him and get hurt in the process"),
                 _("Then it is but an unfortunate accident..."),
                 _("Archers! Fire!"),
                 _("*The archers release a volley of arrows.*"),  # Row 3
                 _("*In one swift movement, Yang Kai kicks some nearby tables and chairs into the air to block the arrows.*"),
                 _("*Grabbing Feng Xinyi by the arm, he rushes out of the inn, signaling you to follow.*"),
                 _("*After running for some time, the 3 of you come to a small house in the woods.*"),
                 ],
                ["you", "you", "Feng Pan", "Yang Kai", "Feng Pan", "Yang Kai", "Yang Kai", "Feng Pan",
                 "Yang Kai", "Feng Pan", "Yang Kai", "Feng Pan", "Feng Pan", "Feng Pan",
                 "Blank", "Blank", "Blank", "Blank"
                 ]
            )
            self.go_inn()
            return

        elif self.taskProgressDic["feng_pan"] in [33, 43]:
            self.taskProgressDic["feng_pan"] += 1
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("*Injured and exhausted, you lay on the ground, waiting for the final blow.*"),
                 _("*Suddenly, a figure jumps out in front of you.*"),
                 _("Who are you? Get out of the way!"),
                 _("Greetings. I'm Yang Kai, one of the elders of Shanhu Sect."),
                 _("I happened to pass by this place and couldn't help but do my best to prevent unnecessary bloodshed."),
                 _("Then you're aware, I assume, of my position in the government..."),
                 _("Yes, of course, General Feng."), #Row 2
                 _("The government and people of Wulin have always stayed out of each other's business... I hope things remain that way."),
                 _("I'm afraid I can't let these 2 youngsters die."),
                 _("Very well, my target is this little brat, not you. If you decide to jump in to protect him and get hurt in the process"),
                 _("Then it is but an unfortunate accident..."),
                 _("Archers! Fire!"),
                 _("*The archers release a volley of arrows.*"), #Row 3
                 _("*In one swift movement, Yang Kai kicks some nearby tables and chairs into the air to block the arrows and turns his body around.*"),
                 _("*Then immediately, he grabs you and Feng Xinyi by the arm and rushes out of the inn.*"),
                 _("*After running for some time, the 3 of you come to a small house in the woods.*"),
                 ],
                ["Blank", "Blank", "Feng Pan", "Yang Kai", "Yang Kai", "Feng Pan",
                 "Yang Kai", "Feng Pan", "Yang Kai", "Feng Pan", "Feng Pan", "Feng Pan",
                 "Blank", "Blank", "Blank", "Blank"]
            )
            self.go_inn()
            return

        elif self.taskProgressDic["feng_pan"] in [24, 34, 44]:
            time.sleep(.5)
            self.stopSoundtrack()
            self.currentBGM = "txdy_romantic.mp3"
            self.startSoundtrackThread()
            self.generate_dialogue_sequence(
                None,
                "Wooden House.png",
                [_("Everyone okay? Anyone hurt?"),
                 _("I... I'm okay... a small scratch on my arm but nothing urgent."),
                 _("Thank you for saving us, Elder Yang."),
                 _("Don't mention it, and no need to be so formal. You can call me by my name."),
                 _("How about... Brother Yang? Can I call you that?"),
                 _("Sure thing, and your name?")
                 ],
                ["Yang Kai", "Customer", "Customer", "Yang Kai", "Customer", "Yang Kai"]
            )

            if self.taskProgressDic["feng_pan"] == 44 or self.taskProgressDic["join_sect"] == 200:
                self.generate_dialogue_sequence(
                    None,
                    "Wooden House.png",
                    [_("I'm Feng Xinyi, and this is --"),
                     _("{}, good to see you again.").format(self.character),
                     _("Likewise, Elder Yang!"),
                     _("You... you two know each other??"),
                     _("Of course! I'm a disciple of Shanhu Sect hehe..."),
                     _("Anyway, let's take care of your wounds, Xinyi. You too, {}.").format(self.character),
                     _("Sounds good, but why were you at the inn? It's quite far from Shanhu Sect."),
                     _("I was sent to help investigate some matters involving Wudang."),
                     _("Well, I'm glad you appeared to save the day! Anyway, is this place safe?"), #Row 2
                     _("It should be; let's rest here for a few days and wait for things to quiet down before we leave."),
                     _("Perfect!")
                     ],
                    ["Customer", "Yang Kai", "you", "Customer", "you", "Yang Kai", "you", "Yang Kai",
                     "you", "Yang Kai", "you"]
                )

            else:
                self.generate_dialogue_sequence(
                    None,
                    "Wooden House.png",
                    [_("I'm Feng Xinyi, and this is my friend {}.").format(self.character),
                     _("Nice to meet you both."),
                     _("Likewise, Elder Yang!"),
                     _("Anyway, let's take care of your wounds, Xinyi. You too, {}.").format(self.character),
                     _("Sounds good, but why were you at the inn? It's quite far from Shanhu Sect."),
                     _("I was sent to help investigate some matters involving Wudang."),
                     _("Well, I'm glad you appeared to save the day! Anyway, is this place safe?"),  # Row 2
                     _("It should be; let's rest here for a few days and wait for things to quiet down before we leave."),
                     _("Perfect!")
                     ],
                    ["Customer", "Yang Kai", "you", "Yang Kai", "you", "Yang Kai",
                     "you", "Yang Kai", "you"]
                )

            Scene_wooden_house(self)
            return


        elif self.taskProgressDic["ouyang_nana_xu_jun"] == -1 and self.taskProgressDic["ouyang_nana"] in [200, 300]:
            time.sleep(.5)
            self.stopSoundtrack()
            self.currentBGM = "hand_in_hand.mp3"
            self.startSoundtrackThread()

            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("You're cheating, you're cheating, you're cheating!"),
                 _("What are you talking about, Miss? I explained the rules to you beforehand."),
                 _("I don't care! Hmph! There's no way I can lose 5 times in a row!"),
                 _("Ahhhh, maybe it's just bad luck. Perhaps you should come back another day..."),
                 _("Come back another day?? Yeah right... Who knows where you'll be by then, you scammer!"),
                 _("Give me back my money now!"), #Row 2
                 _("Come on, Miss, you're putting me in a tough situation. I won fair and square!"),
                 _("The spiders must be very hungry today. Maybe if you--"),
                 _("Let me inspect the ants then! I bet they're all crippled..."),
                 _("Or maybe you fed them poison so that they've become very slow..."),
                 _("Whatever it is, I'm sure you're cheating somehow!"), #Row 3
                 _("(Wait, that voice... No way... It can't be...)"),
                 _("You can't just accuse me like that without proof! You're ruining my reputation!"),
                 _("Hmph! I can do a lot more than just ruin your reputation if you don't admit that you cheated!"),
                 _("(There's no mistake... that voice... it's gotta be..."),
                 _("*You approach the figure and call out her name.*"),
                 _("Nana... Ouyang Nana...")],
                ["Ouyang Nana Silhouette", "Gambler", "Ouyang Nana Silhouette", "Gambler", "Ouyang Nana Silhouette",
                 "Ouyang Nana Silhouette", "Gambler", "Gambler", "Ouyang Nana Silhouette", "Ouyang Nana Silhouette",
                 "Ouyang Nana Silhouette", "you", "Gambler", "Ouyang Nana Silhouette", "you", "Blank", "you"]
            )

            if self.taskProgressDic["ouyang_nana"] == 200:
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("Give me my money back, you--"),
                     _("Nana!"),
                     _("..."),
                     _("Nana, I'm so sorry... I... I didn't know you would..."),
                     _("What? You didn't know I would kill myself after having my heart broken by someone I trusted?"),
                     _("I... I was wrong. I am so ashamed of myself... I... but how... why are you here?"),
                     _("Y-you... you didn't..."),
                     _("Yeah, I didn't actually kill myself. What... Disappointed, are you?"),
                     _("No! Of course not! I... I'm very happy to see you."), #Row 2
                     _("Oh, really? Sadly, I can't say the same about you..."),
                     _("Please, Nana, please forgive me. I'll do anything if you forgive me!"),
                     _("Anything?"),
                     _("Anything!"),
                     _("Hmmm... I'm in a bad mood right now because that gambler over there scammed me."),
                     _("Beat him 5 times in a row. Then we'll talk.")],
                    ["Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana", "you", "you", "Ouyang Nana",
                     "you", "Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana"],
                    [[_("Got it! I'll get revenge for you!"), partial(self.talk_to_ouyang_nana_post_dragon_sect, 1)],
                     [_("That's going to take forever. Can I do something else instead?"), partial(self.talk_to_ouyang_nana_post_dragon_sect, 2)]]
                )

            elif self.taskProgressDic["ouyang_nana"] == 300:
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("Give me my money back, you--"),
                     _("Nana!"),
                     _("..."),
                     _("Oh look, it's my very reliable 'friend'..."),
                     _("Nana, I'm sorry... I was a coward... I promised to help you but..."),
                     _("*Sigh* ... Well, I suppose I didn't need your help. I managed to escape again on my own... Hmph..."),
                     _("I knew your uncle couldn't keep you locked up for long! You're a clever cookie!"),
                     _("Are you done? Flattery isn't going to win my trust..."),
                     _("Ah, wait wait wait... Come on, Nana, what do I have to do for you to forgive me?"), #Row 3
                     _("What are you willing to do?"),
                     _("Anything! I'll do anything if you stop being mad at me!"),
                     _("Anything...? Hmmm... We'll start with an easy one."),
                     _("You see that gambler over there? He scammed me with his stupid spider game."),
                     _("Go beat him 5 times in a row, and then we'll talk.")],
                    ["Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana",
                     "you", "Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana", "Ouyang Nana"],
                    [[_("Got it! I'll get revenge for you!"), partial(self.talk_to_ouyang_nana_post_dragon_sect, 1)],
                     [_("That's going to take forever. Can I do something else instead?"), partial(self.talk_to_ouyang_nana_post_dragon_sect, 2)]]
                )


        self.innWin = Toplevel(self.mapWin)
        self.innWin.title(_("Yuelai Inn"))

        self.bg = PhotoImage(file="Inn.png")
        w = self.bg.width()
        h = self.bg.height()

        canvas = Canvas(self.innWin, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=self.bg)

        self.innF1 = Frame(canvas)
        self.pic1 = PhotoImage(file="Waiter_icon_large.ppm")
        imageButton1 = Button(self.innF1, command=self.order_food)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=self.pic1)
        label = Label(self.innF1, text=_("Waiter"))
        label.grid(row=1, column=0)
        self.innF1.place(x=w // 4 - self.pic1.width() // 2, y=h // 4 - self.pic1.height()//2)

        if self.taskProgressDic["feng_xinyi"] < 100 and self.taskProgressDic["feng_pan"] not in [11, 70] and self.taskProgressDic["supreme_leader"] != 1:
            self.innF2 = Frame(canvas)
            if self.taskProgressDic["feng_xinyi"] >= 1:
                self.pic2 = PhotoImage(file="Feng Xinyi_icon_large.ppm")
            else:
                self.pic2 = PhotoImage(file="Customer_icon_large.ppm")
            imageButton2 = Button(self.innF2, command=self.clickedOnCustomer)
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=self.pic2)
            if self.taskProgressDic["feng_xinyi"] >= 1:
                label = Label(self.innF2, text=_("Feng Xinyi"))
            else:
                label = Label(self.innF2, text=_("Customer"))
            label.grid(row=1, column=0)
            self.innF2.place(x=w * 3 // 4 - self.pic2.width() // 2, y=h // 4 - self.pic2.height()//2)

        if self.taskProgressDic["liu_village"] == 0 and self.taskProgressDic["join_sect"] != -1 :
            self.innF3 = Frame(canvas)
            self.pic3 = PhotoImage(file="Kid_icon_large.ppm")
            imageButton3 = Button(self.innF3, command=self.liu_village_talk_to_kid)
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=self.pic3)
            label = Label(self.innF3, text=_("Kid"))
            label.grid(row=1, column=0)
            self.innF3.place(x=w * 3 // 4 - self.pic3.width() // 2, y=h // 2 + self.pic3.height()//4)
            
        elif (self.taskProgressDic["ouyang_nana"] <= 0 and (self.taskProgressDic["wudang_task"] >= 2 or self.taskProgressDic["wudang_task"] == -1) and self.taskProgressDic["join_sect"] not in [-1] and self.taskProgressDic["wife_intimacy"] == -1):
            time.sleep(.5)
            self.stopSoundtrack()
            self.currentBGM = "hand_in_hand.mp3"
            self.startSoundtrackThread()

            self.innF3 = Frame(canvas)
            self.pic3 = PhotoImage(file="Ouyang Nana_icon_large.ppm")
            imageButton3 = Button(self.innF3, command=self.talk_to_ouyang_nana)
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=self.pic3)
            label = Label(self.innF3, text=_("Ouyang Nana"))
            label.grid(row=1, column=0)
            self.innF3.place(x=w * 3 // 4 - self.pic3.width() // 2, y=h // 2 + self.pic3.height() // 4)

        elif self.taskProgressDic["ouyang_nana_xu_jun"] >= 0 and self.taskProgressDic["ouyang_nana_xu_jun"] <= 5:
            self.innF3 = Frame(canvas)
            self.pic3 = PhotoImage(file="Ouyang Nana_icon_large.ppm")
            imageButton3 = Button(self.innF3, command=partial(self.talk_to_ouyang_nana_post_dragon_sect,0))
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=self.pic3)
            label = Label(self.innF3, text=_("Ouyang Nana"))
            label.grid(row=1, column=0)
            self.innF3.place(x=w * 3 // 4 - self.pic3.width() // 2, y=h // 2 + self.pic3.height() // 4)
            
        elif (self.taskProgressDic["guo_zhiqiang"] == 16):
            self.innF3 = Frame(canvas)
            self.pic3 = PhotoImage(file="Guo Zhiqiang_icon_large.ppm")
            imageButton3 = Button(self.innF3, command=self.talk_to_guo_zhiqiang)
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=self.pic3)
            label = Label(self.innF3, text=_("Guo Zhiqiang"))
            label.grid(row=1, column=0)
            self.innF3.place(x=w * 3 // 4 - self.pic3.width() // 2, y=h // 2 + self.pic3.height() // 4)

        self.innF4 = Frame(canvas)
        self.pic4 = PhotoImage(file="Gambler_icon_large.ppm")
        imageButton4 = Button(self.innF4, command=self.gamble)
        imageButton4.grid(row=0, column=0)
        imageButton4.config(image=self.pic4)
        label = Label(self.innF4, text=_("Gambler"))
        label.grid(row=1, column=0)
        self.innF4.place(x=w // 4 - self.pic4.width() // 2, y=h // 2 + self.pic4.height()//4)

        self.innWinF1 = Frame(self.innWin)
        self.innWinF1.pack()

        menu_frame = self.create_menu_frame(self.innWin)
        menu_frame.pack()

        self.innWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.innWin.update_idletasks()
        toplevel_w, toplevel_h = self.innWin.winfo_width(), self.innWin.winfo_height()
        self.innWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.innWin.focus_force()
        self.innWin.mainloop()###



    def liu_village_talk_to_kid(self, stage=0, choice=None):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        if stage == 0:
            self.innWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("Wahhhhh... Wahhhhh..."),
                 _("Hey, what's going on? Why are you crying?"),
                 _("My... my family... dead... gone... men in black..."),
                 _("(Hmmm, could it be that this kid is from that abandoned village I went to earlier?)"),
                 _("Hey kid, are you from Liu Village?"),
                 _("Y-yes... but h-how... did you... know?"),
                 _("I passed by the village not long ago and saw the destruction with my own eyes..."),
                 _("Please, uncle, please avenge my family! They were brutally murdered!"),
                 ],
                ["Kid", "you", "Kid", "you", "you", "Kid", "you", "Kid"],
                [[_("No worries; I will investigate this matter to the end!"), partial(self.liu_village_talk_to_kid, 1, 1)],
                 [_("Listen up, kiddo, I'm a busy guy, but if you give me some money, I'll consider it."), partial(self.liu_village_talk_to_kid, 1, 2)]
                 ]
            )

        elif stage == 1:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.innF3.place(x=-3000,y=-3000)
            self.taskProgressDic["liu_village"] = 11

            if choice == 1:
                self.chivalry += 30
                messagebox.showinfo("", _("Your chivalry +30."))
                self.generate_dialogue_sequence(
                    self.innWin,
                    "Inn.png",
                    [_("Thank you, uncle! I don't know how I can ever repay you!"),
                     _("Don't worry about that. Just stay safe, ok?"),
                     _("Yes, uncle! Thank you so much!")],
                    ["Kid", "you", "Kid"]
                )


            elif choice == 2:

                self.taskProgressDic["liu_village"] = 21
                self.chivalry -= 30
                messagebox.showinfo("", _("Your chivalry -30."))

                self.generate_dialogue_sequence(
                    self.innWin,
                    "Inn.png",
                    [_("How shameful! A grown man picking on a kid who has just lost his family?"),
                     _("And who are you, you nosy priest?"),
                     _("Someone who's about to teach you a lesson!")],
                    ["Zhong Yue", "you", "Zhong Yue"]
                )

                opp = character(_("Zhong Yue"), 12,
                                sml=[special_move(_("Taichi Fist"), level=8),
                                     special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Taichi Sword"), level=8)],
                                skills=[skill(_("Wudang Agility Technique"), level=8),
                                        skill(_("Pure Yang Qi Skill"), level=7)])

                if self.taskProgressDic["join_sect"] == 600:
                    dialogues = [_("Ha, weak! Go practice a few more decades before trying to be a hero, you nosy priest!"),
                                   _("You... how do you know Wudang Kungfu?"),
                                   _("I am a Wudang disciple. What, scared?"),
                                   _("You are a disgrace to Wudang! From this day forward, you are no longer a Wudang disciple!"),
                                   _("Yeah blablabla... Doesn't matter to me hahahaha...")]
                    speakers = ["you", "Zhong Yue", "you", "Zhong Yue", "you"]
                    self.taskProgressDic["wudang_task"] = -1000
                else:
                    dialogues = [_("Ha, weak! Go practice a few more decades before trying to be a hero, you nosy priest!"),
                                       _("It is such a shame that someone with your abilities has such poor character..."),
                                       _("Young man, I suggest you turn back before it's too late..."),
                                       _("Yeah blablabla... sore loser... get out of my sight!")]
                    speakers = ["you", "Zhong Yue", "Zhong Yue", "you"]

                cmd = lambda: self.generate_dialogue_sequence(self.innWin, "Inn.png", dialogues, speakers)

                self.battleMenu(self.you, opp, "battleground_inn.png",
                                postBattleSoundtrack="jy_suspenseful2.mp3",
                                fromWin=self.innWin,
                                battleType="normal", destinationWinList=[self.innWin] * 3,
                                postBattleCmd = cmd
                                )


    def talk_to_ouyang_nana_post_dragon_sect(self, choice):
        if choice == 0:
            if self.taskProgressDic["ouyang_nana_xu_jun"] < 5:
                self.generate_dialogue_sequence(
                    self.innWin,
                    "Inn.png",
                    [_("Come on, you still haven't beaten him 5 times in a row yet?"),
                     _("Soon, soon! I'm about to kick his sorry butt!"),],
                    ["Ouyang Nana", "you"]
                )
            elif self.taskProgressDic["ouyang_nana_xu_jun"] == 5:
                self.innWin.withdraw()
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("There! I won so much money from him that his wife is going to smack the crap out of him for sure!"),
                     _("Very good... Now my mood has improved quite a bit."),
                     _("So... you're not mad at me anymore?"),
                     _("For the time being, I guess... I'll still need your help, though."),
                     _("Sure, what can I do for you?"),],
                    ["you", "Ouyang Nana", "you", "Ouyang Nana", "you"]
                )
                
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("I want to go to Wudang and confess my feelings to Xu Jun."),
                     _("What?? You still haven't given up?"),
                     _("I've given it a lot of thought recently..."),
                     _("If I can marry a reliable man like him, it'll be the best thing that's ever happened to me."),
                     _("You don't even know him that well. How do you know he's reliable?"),
                     _("What if he's a wimp? Or a pervert? Why take such a big risk?"),
                     _("Hey! Don't you dare say that about him!"),
                     _("Even if he has some unknown flaws, he'll still be heck a lot more reliable than you! Hmph!"), #Row 2
                     _("Dah... Ok, you know what, I actually can't argue with that..."),
                     _("Anyway, I need you to come with me to convince Xu Jun's masters that I'm a good girl."),
                     _("You ready with some compliments?")],
                    ["Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana", "you", "you", "Ouyang Nana",
                     "Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana"]
                )

                self.ouyang_nana_wudang_dialogue(choice=0)


        else:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()

        if choice == 1:
            self.taskProgressDic["ouyang_nana_xu_jun"] = 0
        elif choice == 2:
            if self.taskProgressDic["ouyang_nana"] == 300 or self.taskProgressDic["castrated"] == 1:
                self.taskProgressDic["ouyang_nana_xu_jun"] = -10
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("I knew it! You're a liar!"),
                     _("You won't even do something so simple. How can you expect me to trust you?!"),
                     _("You'll never see me again! Hmph!!!")],
                    ["Ouyang Nana", "Ouyang Nana", "Ouyang Nana"]
                )
            elif self.taskProgressDic["ouyang_nana"] == 200:
                self.taskProgressDic["ouyang_nana_xu_jun"] = -20
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("...... Fine.... I'm hungry. Treat me to a meal then."),
                     _("Ah, that's easy. Waiter! 2 jugs of wine and some appetizers please!"),
                     _("Coming right up, sir!"),
                     _("So, Nana, what do you want to eat? No need to worry about the cost."),
                     _("Aren't you worried that I'm going to get revenge on you by bankrupting you?"),
                     _("Haha, you're not that type of person... Plus, not to brag, but I'm quite rich hehehe..."),
                     _("Hmmm, fine, tell the waiter to bring all their specialty dishes then..."),
                     _("Hey! Waiter!"),
                     _("Ah yes, sir! Here's your wine... and your peanuts, beef jerky, and fried tofu for appetizers..."),
                     _("What more can I get for you?"), #Row 2
                     _("What are your top 10 dishes?"),
                     _("Well, we have the Squirrel Fish, Spicy Stir-Fried Kidneys, Stewed Pork Tongue..."),
                     _("Ok, all of those sound good so far. I'll take all 10 please..."),
                     _("Sure thing, dear customer! I'll let the chef know right away!"),
                     _("If you need anything else, just holler!"),
                     _("Mmmm, I'm starving. This fried tofu is really good!"),
                     _("Their wine is quite nice too..."),
                     _("Hmmm, yeah, not bad, not bad... Thanks for the meal, cheers!"),
                     _("Cheers! To friendship!"), #Row 3
                     _("To friendship!"),
                     _("Ahhhh, so satisfying... Now let me try some peanuts..."),
                     _("*Ahem*... Don't you think you owe me a formal apology?"),
                     _("Ah, oh... right... Here, I'll drink 3 cups of wine as an apology to you, Nana."),
                     _("*Gulp gulp gulp... You quickly drink 3 more cups of wine...*"),
                     _("That's more like it... You know, {}, after you disappointed me, I decided I would never trust anyone again...").format(self.character),
                     _("But when I saw you just now and you apologized, my heart softened again..."),
                     _("That is, until you refused to do even 1 simple request for me."), #Row 4
                     _("Wait, what? Surely you're not still mad about th-- Ah! What the..."),
                     _("*You start to feel a bit light-headed.*"),
                     _("Oh gosh... how embarrassing... I didn't think I'd... get drunk... this fast..."),
                     _("It's not that... I just added something into your cup while you were ordering the food; that's all..."),
                     _("What? Nana, you..."),
                     _("*Your vision starts to blur and the world begins to rotate.*"),
                     _("You can't blame me for doing this. People like you need to be taught a lesson...")],
                    ["Ouyang Nana", "you", "Waiter", "you", "Ouyang Nana", "you", "Ouyang Nana", "you", "Waiter",
                     "Waiter", "you", "Waiter", "you", "Waiter", "Waiter", "Ouyang Nana", "you", "Ouyang Nana",
                     "you", "Ouyang Nana", "you", "Ouyang Nana", "you", "you", "Ouyang Nana", "Ouyang Nana",
                     "Ouyang Nana", "you", "Blank", "you", "Ouyang Nana", "you", "Blank", "Ouyang Nana"]
                )

                self.generate_dialogue_sequence(
                    None,
                    "BlackDialogueBG.png",
                    [_("No... I... I..."),
                     _("*You begin to lose consciousness...*")],
                    ["you", "Blank"]
                )

                self.generate_dialogue_sequence(
                    None,
                    "BlackDialogueBG.png",
                    [_("*An unknown length of time later...*"),
                     _("How's the next group?"),
                     _("They're all waiting to undergo the procedure, Your Highness."),
                     _("I have ordered them to be bathed for 8 hours in preparation."),
                     _("Afterwards, they will be taken to Doctor Wan for 'cleansing'..."),
                     _("Good, I hope this time there are some competent ones."),
                     _("Rest assured, Your Highness! I've carefully inspected each of them, and all the sellers are highly trustworthy."), #Row 2
                     _("After I'm done training them, there won't be anyone more fit to serve you than these young men!"),
                     _("Excellent, report to me once everything is ready."),
                     _("Yes, Your Highness! I will go check on Doctor Wan now to see if everything is in order."),
                     _("*The sound of their footsteps slowly fades into the distance...*")],
                    ["Blank", "Empress", "Imperial Eunuch", "Imperial Eunuch", "Imperial Eunuch", "Empress",
                     "Imperial Eunuch", "Imperial Eunuch", "Empress", "Imperial Eunuch", "Blank"],
                    [[_("Stay and serve the woman."), partial(self.talk_to_ouyang_nana_post_dragon_sect, 21)],
                     [_("Escape while you have a chance."), partial(self.talk_to_ouyang_nana_post_dragon_sect, 22)]]
                )

        elif choice == 21:
            self.generate_dialogue_sequence(
                None,
                "BlackDialogueBG.png",
                [_("(Hehehe, let me continue to pretend like I'm asleep, so I can serve this beautiful woman...)"),
                 _("(I wonder what she'll have me do, ehehehehe~)"),
                 _("*A few minutes later...*"),
                 _("Alright, wake up! Come on, wake up, all of you!"),
                 _("*You open your eyes...*")],
                ["you", "you", "Blank", "Imperial Eunuch", "Blank"],
            )

            self.generate_dialogue_sequence(
                None,
                "Empress Palace BG.png",
                [_("Wait... where am I?"),
                 _("You're in the palace of the Empress; now, come with me so we can prepare you to serve Her Highness."),
                 _("Who are you?"),
                 _("Insolence! You are to address me as Head Eunuch Liu."),
                 _("Ah, sorry... Head Eunuch Liu, where are you taking us?"),
                 _("Do you really not know or are you just pretending to be ignorant?"),
                 _("Of course we are going to get you 'cleansed'... Can't have you entertaining naughty thoughts while serving the Empress...")],
                ["you", "Imperial Eunuch", "you", "Imperial Eunuch", "you", "Imperial Eunuch", "Imperial Eunuch"],
                [[_("No! I don't want to be a eunuch!"), partial(self.talk_to_ouyang_nana_post_dragon_sect, 31)],
                 [_("Being free of sexual desires may help me become a better martial artist. Let's do it!"), partial(self.talk_to_ouyang_nana_post_dragon_sect, 32)]]
            )

        elif choice == 22:
            self.generate_dialogue_sequence(
                None,
                "BlackDialogueBG.png",
                [_("*You quickly sneak out of there...*")],
                ["Blank"]
            )
            self.mapWin.deiconify()

        elif choice == 31:
            self.generate_dialogue_sequence(
                None,
                "Empress Palace BG.png",
                [_("Insolence! Don't you know that it's a great honor to serve the Empress?"),
                 _("Or would you rather be put to death?"),
                 _("There's no way I'm gonna let you castrate me! I'd rather die!"),
                 _("Pity... You could've been a good servant with your pretty little face..."),
                 _("Let me make an example out of you to show the others what happens when they disobey me...")],
                ["Imperial Eunuch", "Imperial Eunuch", "you", "Imperial Eunuch", "Imperial Eunuch"]
            )

            opp = character(_("Imperial Eunuch"), 18,
                            sml=[special_move(_("Hun Yuan Shapeshifting Fist"), level=10)],
                            skills=[skill(_("Hun Yuan Yin Qi"), level=10)])
            
            self.battleMenu(self.you, opp, "Empress Palace BG.png",
                            postBattleSoundtrack="jy_heroic.mp3",
                            fromWin=self.mapWin,
                            battleType="normal", destinationWinList=[self.mapWin] * 3)

        elif choice == 32:
            self.gameDate += 7
            self.speed += 20
            self.dexterity += 20
            self.taskProgressDic["castrated"] = 1
            self.generate_dialogue_sequence(
                None,
                "Empress Palace BG.png",
                [_("(Let me receive free surgery and post-surgery care... Afterwards, I'll get out of here.)"),
                 _("Oh, Head Eunuch Liu, I can't wait to start serving the Empress!"),
                 _("I will follow you orders and learn from you!"),
                 _("Very good! I like your attitude..."),
                 _("After you've gone through 'cleansing', each of you will receive a new name."),
                 _("Since I like you so much, I'll go ahead and give you a name now..."),
                 _("Henceforth, you will be known as 'Little Egg'."),
                 _("Little Egg! I love that name! Thank you, Head Eunuch Liu!"), #Row 2
                 _("Great, now follow me, all of you..."),
                 _("*Head Eunuch Liu takes you, along with several other young men, to Doctor Wan's operation room.*"),
                 _("*Soon you are drugged and fall into deep sleep. After waking up, you are no longer a complete man.*"),
                 _("(Hmmm, my body feels a lot lighter after having lost some of that weight...)"),
                 _("(And I bet I can practice some powerful techniques now... Just need to know where to find the manuals...)"),
                 _("*Your speed and dexterity increased by 20.*")],
                ["you", "you", "you", "Imperial Eunuch", "Imperial Eunuch", "Imperial Eunuch", "Imperial Eunuch",
                 "you", "Imperial Eunuch", "Blank", "Blank", "you", "you", "Blank"]
            )
            
            self.mapWin.deiconify()


    def ouyang_nana_wudang_dialogue(self, choice):
        if choice == 0:
            if self.taskProgressDic["wudang_task"] in [-1000, -2000]:
                self.taskProgressDic["ouyang_nana_xu_jun"] = -5
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("Erm... Well..."),
                     _("What? ... Can't think of anything good to say about me?"),
                     _("No, it's not that... You see, there's a problem..."),
                     _("I'm not exactly on good terms with Wudang. If I go there, I'll just create more trouble."),
                     _("You're better off without me..."),
                     _("Wow... I should've known... I can't depend on you for anything."),
                     _("Fine then, I don't need your help anymore. I don't want to see you ever again!"),
                     _("Wait --"),
                     _("*Ouyang Nana storms off angrily.*"),
                     _("*Sigh*... Why did I get into trouble with those Wudang priests...")],
                    ["you", "Ouyang Nana", "you", "you", "you", "Ouyang Nana", "Ouyang Nana", "you", "Blank", "you"]
                )
                self.mapWin.deiconify()


            elif self.chivalry <= 0 and self.taskProgressDic["join_sect"] != 600:
                self.taskProgressDic["ouyang_nana_xu_jun"] = -5
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("Of course! With my persuasive words, those priests will be eager to let you have their precious disciple!"),
                     _("Hehehehe... Good! Let's go then!")],
                    ["you", "Ouyang Nana"]
                )
            
                self.generate_dialogue_sequence(
                    None,
                    "Wudang.png",
                    [_("Welcome to Wudang. What can we do for you?"),
                     _("Hi, Elder Li! Thank you for taking the time out of your busy day to speak to us. My name is {}.").format(self.character),
                     _("I am here today to bring you most joyful news! But before that, please allow me to introduce my good friend: Ouyang Nana."),
                     _("My respects, Elder Li and Elder Zhou."),
                     _("Pleasure to meet you, {} and Nana.").format(self.character),
                     _("So, what is this good news that you speak of?"),
                     _("It has been made known to me that you have a young disciple named Xu Jun."),
                     _("Yes, Xu Jun is one of our disciples. I hope he has not caused you any trouble."),
                     _("Oh no, of course not! In fact, I heard that he is a bright, young man!"), #Row 2
                     _("If I may ask, is he married?"),
                     _("No, he is not. Why do you ask?"),
                     _("Very glad to hear that! This is a great opportunity then. My friend Nana would be a perfect match for him!"),
                     _("In terms of physical appearance, she is just as beautiful as he is handsome."),
                     _("In terms of status, she is the niece of the renowned Dragon Sect leader Ouyang Xiong."),
                     _("In terms of skill, her fundamentals are solid and she's got great potential!"),
                     _("In terms of character, she is gentle, lady-like, outgoing... Need I say more?"),
                     _("Hold on a second, {}. What you say sounds good, but I'm afraid your words carry little weight.").format(self.character),
                     _("We are unfamiliar with your character, and your integrity is unproven."),
                     _("We cannot make any rash decisions from your words alone."), #Row 3
                     _("Wait, you've gotta give her a chance. She came all the way --"),
                     _("I appreciate your intentions, but we, as Xu Jun's teachers, can find a suitable spouse for him."),
                     _("You need not worry about such matters."),
                     _("Now, if you would like to come in and have some tea and snacks, we welcome you."),
                     _("However, please speak no more on the topic of marriage."),
                     _("We'll pass on that, thank you, but can you at least let Xu Jun come out for a second?"),
                     _("Nana has been longing to see him."),
                     _("Sorry, Xu Jun is busy training at the moment. He will not be available any time soon."), #Row 4
                     _("You stubborn priests! Why are you so heartless?!"),
                     _("Hah, it seems I was right to question your character."),
                     _("Sorry, Elder Li and Elder Zhou! My friend is a bit careless with his words. Please for--"),
                     _("Enough. There's no need to explain. Our disciple cannot afford to marry the precious niece of that arrogant Ouyang Xiong."),
                     _("Now if you'll excuse us, we have matters to attend to..."),
                     _("No! Please wait..."),
                     _("..."), #Row 5
                     _("Crap, why was I so quick to speak... Nana, I'm sorry... I ruined everything..."),
                     _("I can't believe you did that, {}! This is all your fault! I never want to see you again!").format(self.character),
                     _("*Ouyang Nana runs off into the distance as you bang your head against the wall, regretting your poor word choice and impatience.*")],
                    ["Li Kefan", "you", "you", "Ouyang Nana", "Zhou Enrui", "Li Kefan", "you", "Zhou Enrui",
                     "you", "you", "Zhou Enrui", "you", "you", "you", "you", "you", "Li Kefan", "Li Kefan",
                     "Li Kefan", "you", "Li Kefan", "Li Kefan", "Li Kefan", "Li Kefan", "you", "you",
                     "Zhou Enrui", "you", "Li Kefan", "Ouyang Nana", "Li Kefan", "Li Kefan", "Ouyang Nana",
                     "Ouyang Nana", "you", "Ouyang Nana", "Blank"]
                )

                self.mapWin.deiconify()

            elif self.taskProgressDic["join_sect"] == 600:
                self.generate_dialogue_sequence(
                    None,
                    "Wudang.png",
                    [_("Welcome back, {}! It is good to see you again!").format(self.character),
                     _("Elder Li! Elder Zhou! How have you guys been?"),
                     _("No complaints; not much has changed except some more gray hair and wrinkles, possibly."),
                     _("I am here today to bring you most joyful news! But before that, please allow me to introduce my good friend: Ouyang Nana."),
                     _("My respects, Elder Li and Elder Zhou."),
                     _("Pleasure to meet you, {} and Nana.").format(self.character),
                     _("So, what is this good news that you speak of?"),
                     _("Well, I have long admired Martial Brother Xu Jun, especially his spirit of perseverance and his compassionate heart."),
                     _("It is a pity that he does not yet have a prudent wife by his side."),
                     _("However, that won't be a problem for long because my friend Nana is a perfect match for him!"), #Row 2
                     _("In terms of physical appearance, she is just as beautiful as he is handsome."),
                     _("In terms of status, she is the niece of the renowned Dragon Sect leader Ouyang Xiong."),
                     _("In terms of skill, her fundamentals are solid and she's got great potential!"),
                     _("In terms of character, she is gentle, lady-like, outgoing... Need I say more?"),
                     _("That's very kind of you to think about Xu Jun. Given our relationship, we trust your judgment."),
                     _("However, what does her uncle say of this?"),
                     _("My uncle... he doesn't approve of this, but don't worry!"),
                     _("If you would be so kind to give us your blessings, I will roam the world with Xu Jun and not cause trouble for Wudang!"),
                     _("It's just... it's just that you would be losing a disciple in the process..."), #Row 3
                     _("I know this is a difficult choice to make, and I will respect your decision, no matter what it is."),
                     _("But I'm so in love with Xu Jun that I had to try, even if the chance of success is low!"),
                     _("Hahahaha! What a clever girl! I'm starting to like her already!"),
                     _("As this is not an easy decision to make, I must first give you a test."),
                     _("If you pass, then I will speak to Xu Jun and see how he feels about this."),
                     _("If he, too, feels the same way about you, then I see no reason to object and will wish you two the best."),
                     _("Thank you, Elder Zhou and Elder Li! What is this test? I will do anything within my abilities!"), #Row 4
                     _("Tomorrow, Brother Li and I are visiting some nearby towns and villages to provide aide to the poor and needy."),
                     _("We had originally planned to exterminate the Goblin Duo of Jiangnan afterwards."),
                     _("But if you are willing to do that for us, it would save us some time."),
                     _("Goblin Duo of Jiangnan? Who are they, and how do we find them?"),
                     _("You may not have heard of them, as they are more of a local pest than a real threat."),
                     _("The Goblin Duo of Jiangnan are 2 cousins who often cause trouble near the Jiangnan district."),
                     _("Though their combat skills are nothing extraordinary, they far surpass that of commoners."), #Row 5
                     _("Recently, they've been active on the roads between Guigang and Liuzhou, robbing merchants and citizens who are unfortunate enough to encounter them."),
                     _("See if you can lure them out and capture them."),
                     _("I must warn you, this could be quite a dangerous task."),
                     _("I'm ready. Even if it weren't for Xu Jun, I would do it to prevent more innocent people from getting hurt."),
                     _("Very well; I will give you instructions on where to go. I look forward to hearing news of your success."),
                     _("*After receiving some instructions, you and Ouyang Nana head towards the road between Guigang and Liuzhou.*")
                     ],
                    ["Li Kefan", "you", "Zhou Enrui", "you", "Ouyang Nana", "Zhou Enrui", "Li Kefan", "you", "you",
                     "you", "you", "you", "you", "you", "you", "Li Kefan", "Li Kefan", "Ouyang Nana", "Ouyang Nana",
                     "Ouyang Nana", "Ouyang Nana", "Ouyang Nana", "Zhou Enrui", "Zhou Enrui", "Zhou Enrui", "Zhou Enrui",
                     "Ouyang Nana", "Zhou Enrui", "Zhou Enrui", "Zhou Enrui", "you", "Zhou Enrui", "Zhou Enrui",
                     "Zhou Enrui", "Zhou Enrui", "Zhou Enrui", "Zhou Enrui", "Ouyang Nana", "Zhou Enrui", "Blank"
                     ]
                )
                self.jiangnan_forest(0)

            else:
                self.generate_dialogue_sequence(
                    None,
                    "Wudang.png",
                    [_("Greetings Elder Li and Elder Zhou! My name is {}, and I'm here to bring you great news!").format(self.character),
                     _("Hello there, please, do tell."),
                     _("Ah, but first, please allow me to introduce my good friend: Ouyang Nana."),
                     _("My respects, Elder Li and Elder Zhou."),
                     _("Pleasure to meet you, {} and Nana.").format(self.character),
                     _("So, what is this good news that you speak of?"),
                     _("It has come to my attention that you have a brilliant student named Xu Jun."),
                     _("As far as I know, he is currently single, quite a pity, I know..."),
                     _("However, that won't be a problem for long because my friend Nana is a perfect match for him!"),# Row 2
                     _("In terms of physical appearance, she is just as beautiful as he is handsome."),
                     _("In terms of status, she is the niece of the renowned Dragon Sect leader Ouyang Xiong."),
                     _("In terms of skill, her fundamentals are solid and she's got great potential!"),
                     _("In terms of character, she is gentle, lady-like, outgoing... Need I say more?"),
                     _("We have been looking for a suitable spouse for Xu Jun, and you seem to be a person of good character..."),
                     _("However, what does her uncle say of this?"),
                     _("My uncle... he doesn't approve of this, but don't worry!"),
                     _("If you would be so kind to give us your blessings, I will roam the world with Xu Jun and not cause trouble for Wudang!"),
                     _("It's just... it's just that you would be losing a disciple in the process..."),  # Row 3
                     _("I know this is a difficult choice to make, and I will respect your decision, no matter what it is."),
                     _("But I'm so in love with Xu Jun that I had to try, even if the chance of success is low!"),
                     _("Hahahaha! What a clever girl! I'm starting to like her already!"),
                     _("As this is not an easy decision to make, I must first give you a test."),
                     _("If you pass, then I will speak to Xu Jun and see how he feels about this."),
                     _("If he, too, feels the same way about you, then I see no reason to object and will wish you two the best."),
                     _("Thank you, Elder Zhou and Elder Li! What is this test? I will do anything within my abilities!"),# Row 4
                     _("Tomorrow, Brother Li and I are visiting some nearby towns and villages to provide aide to the poor and needy."),
                     _("We had originally planned to exterminate the Goblin Duo of Jiangnan afterwards."),
                     _("But if you are willing to do that for us, it would save us some time."),
                     _("Goblin Duo of Jiangnan? Who are they, and how do we find them?"),
                     _("You may not have heard of them, as they are more of a local pest than a real threat."),
                     _("The Goblin Duo of Jiangnan are 2 cousins who often cause trouble near the Jiangnan district."),
                     _("Though their combat skills are nothing extraordinary, they far surpass that of commoners."),# Row 5
                     _("Recently, they've been active on the roads between Guigang and Liuzhou, robbing merchants and citizens who are unfortunate enough to encounter them."),
                     _("See if you can lure them out and capture them."),
                     _("I must warn you, this could be quite a dangerous task."),
                     _("I'm ready. Even if it weren't for Xu Jun, I would do it to prevent more innocent people from getting hurt."),
                     _("Very well; I will give you instructions on where to go. I look forward to hearing news of your success."),
                     _("*After receiving some instructions, you and Ouyang Nana head towards the road between Guigang and Liuzhou.*")
                     ],
                    [ "you", "Li Kefan", "you", "Ouyang Nana", "Zhou Enrui", "Li Kefan", "you", "you",
                     "you", "you", "you", "you", "you", "Li Kefan", "Li Kefan", "Ouyang Nana", "Ouyang Nana",
                     "Ouyang Nana", "Ouyang Nana", "Ouyang Nana", "Zhou Enrui", "Zhou Enrui", "Zhou Enrui", "Zhou Enrui",
                     "Ouyang Nana", "Zhou Enrui", "Zhou Enrui", "Zhou Enrui", "you", "Zhou Enrui", "Zhou Enrui",
                     "Zhou Enrui", "Zhou Enrui", "Zhou Enrui", "Zhou Enrui", "Ouyang Nana", "Zhou Enrui", "Blank"
                     ]
                )

                self.jiangnan_forest(0)


    def jiangnan_forest(self, stage=0):
        if stage == 0:
            if self.taskProgressDic["ouyang_nana_xu_jun"] <= 10:
                self.taskProgressDic["ouyang_nana_xu_jun"] = 10
            self.stopSoundtrack()
            self.currentBGM = "jy_shanguxingjin.mp3"
            self.startSoundtrackThread()
        
            self.jiangnan_forest_win = Toplevel(self.mainWin)
            self.jiangnan_forest_win.title(_("Forest"))
        
            bg = PhotoImage(file="JiangnanForest.png")
            w = bg.width()
            h = bg.height()
        
            canvas = Canvas(self.jiangnan_forest_win, width=w, height=h)
            canvas.pack()
            canvas.create_image(0, 0, anchor=NW, image=bg)
        
            self.jiangnan_forest_winF1 = Frame(canvas)
            pic1 = PhotoImage(file="Ouyang Nana_icon_large.ppm")
            imageButton1 = Button(self.jiangnan_forest_winF1, command=self.interact_ouyang_nana_jiangnan_forest)
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            label = Label(self.jiangnan_forest_winF1, text=_("Ouyang Nana"))
            label.grid(row=1, column=0)
            self.jiangnan_forest_winF1.place(x=w // 4 - pic1.width() // 2, y=h // 4)

            menu_frame = self.create_menu_frame(self.jiangnan_forest_win,
                                                options=["Profile", "Inv", "Save", "Settings"])
            menu_frame.pack()
        
            self.jiangnan_forest_win.protocol("WM_DELETE_WINDOW", self.on_exit)
            self.jiangnan_forest_win.update_idletasks()
            toplevel_w, toplevel_h = self.jiangnan_forest_win.winfo_width(), self.jiangnan_forest_win.winfo_height()
            self.jiangnan_forest_win.geometry("+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, SCREEN_HEIGHT // 2 - toplevel_h * 3 // 5))
            self.jiangnan_forest_win.focus_force()
            self.jiangnan_forest_win.mainloop()  ###

    
        elif stage == 1: #triggered Goblins; been observing you; lure you into trap --> ouyang nana has to fight alone
            self.generate_dialogue_sequence(
                self.jiangnan_forest_win,
                "JiangnanForest.png",
                [_("Listen carefully to our words; we own this place: dirt, trees, and birds."),
                 _("Those who wish to travel this road; must pay a toll with jewelry or gold."),
                 _("If money and jewelry you have none; then leave us your girl so we can have fun."),
                 _("Refuse to comply with our humble request; and our crossbows will happily send you to the West."),
                 _("Hahahahahaha! So you appear at last! I've been waiting for you, Goblin Duo of Jiangnan..."),
                 _("What is this that we are hearing? You anticipated our appearing?"),#Row 2
                 _("That's right. Now surrender before I teach you the meaning of pain."), 
                 _("This youngster looks quite hard to beat; perhaps it's time for us to retreat."),
                 _("*The Goblin Duo turn around and run into the woods. Without a moment's hesitation, you chase after them, with Ouyang Nana following closely behind.*"),
                 _("*Just as you close the gap between you and the Goblin Duo to 6 feet, something tightens around your right ankle.*"),
                 _("*The next thing you know, you are hanging upside down with a rope tied to your ankle.*"),
                 _("Cheap! Let me down and fight me honorably!"),
                 _("{}! Don't worry, I'll defeat them and rescue you!").format(self.character), #Row 3
                 _("This young lady looks dandily fine! I can't wait to make her mine! ... I mean, ours..."),
                 ],
                ["Jiangnan Goblins", "Jiangnan Goblins", "Jiangnan Goblins", "Jiangnan Goblins", "you",
                 "Jiangnan Goblins", "you", "Jiangnan Goblins", "Blank", "Blank", "Blank", "you",
                 "Ouyang Nana", "Jiangnan Goblins"],
            )

            you = character(_("Ouyang Nana"), 12 + (self.taskProgressDic["ouyang_nana_xu_jun"]-10)//3,
                                sml=[special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Fairy Sword Technique"), level=7),
                                     special_move(_("Rest"))],
                                skills=[skill(_("Burst of Potential"), level=5),
                                        skill(_("Basic Agility Technique"), level=10)],
                                equipmentList=[Equipment(_("Iron Sword")),]
                                )
            
            opp = character(_("Jiangnan Goblins"), 10,
                            sml=[special_move(_("Crossbow"), level=10)],
                            skills=[skill(_("Basic Agility Technique"), level=10)],
                            equipmentList=[Equipment(_("Leather Body")),
                                           Equipment(_("Leather Pants"))],
                            multiple=2
                            )

            cmd = lambda: self.jiangnan_forest(11)
            self.battleID = "ouyang_nana_jiangnan_goblins"
            self.battleMenu(you, opp, "JiangnanForest.png", "jy_shanguxingjin.mp3",
                            fromWin=self.jiangnan_forest_win, battleType="task",
                            destinationWinList=[], postBattleCmd=cmd)
        
        elif stage == 2: #triggered Goblins; think you are a Scrub; you fight them
            self.generate_dialogue_sequence(
                self.jiangnan_forest_win,
                "JiangnanForest.png",
                [_("Ayyyyy, we've gotta take a break. I can't do this anymore..."),
                 _("The gold is too heavy. I need to rest for a while to catch my breath..."),
                 _("You're right... I didn't think 20kg of gold would be this much burden."),
                 _("Good thing you're helping me carry the other 60kg."),
                 _("I can't believe your grandfather left you such a large inheritance."),
                 _("After I help you transport this, you'd better give me 10kg instead of the original 5kg that you promised."),
                 _("This was much harder than I thought it would be."),
                 _("Listen carefully to our words; we own this place: dirt, trees, and birds."),
                 _("Those who wish to travel this road; must pay a toll with jewelry or gold."),
                 _("If money and jewelry you have none; then leave us your girl so we can have fun."),
                 _("Refuse to comply with our humble request; and our crossbows will happily send you to the West."),
                 _("Hahahahahaha! So you appear at last! I've been waiting for you, Goblin Duo of Jiangnan..."),
                 _("What is this that we are hearing? You anticipated our appearing?"),
                 _("That's right. Now surrender before I teach you the meaning of pain."),
                 _("Our iron bolts shall pierce your heart; and our sharpened swords will tear you apart!")],
                ["you", "you", "Ouyang Nana", "Ouyang Nana", "you", "you", "you", "Jiangnan Goblins","Jiangnan Goblins",
                 "Jiangnan Goblins", "Jiangnan Goblins", "you", "Jiangnan Goblins", "you", "Jiangnan Goblins"],
            )
            
            opp = character(_("Jiangnan Goblins"), 10,
                            sml=[special_move(_("Crossbow"), level=10)],
                            skills=[skill(_("Basic Agility Technique"), level=10)],
                            equipmentList=[Equipment(_("Leather Body")),
                                           Equipment(_("Leather Pants"))],
                            multiple=2)
            
            cmd = lambda: self.jiangnan_forest(21)
            self.battleMenu(self.you, opp, "JiangnanForest.png", "jy_shanguxingjin.mp3",
                            fromWin=self.jiangnan_forest_win, battleType="task",
                            destinationWinList=[], postBattleCmd = cmd)

        elif stage == 11: #Nana defeated Goblins --> Nana & Xu Jun get married
            self.stopSoundtrack()
            self.currentBGM = "hand_in_hand.mp3"
            self.startSoundtrackThread()
            self.battleID = None
            self.chivalry += 20
            self.generate_dialogue_sequence(
                None,
                "JiangnanForest.png",
                [_("Yay, I did it!!!"),
                 _("Wow, Nana, I'm quite impressed! You sure saved my butt!"),
                 _("Speaking of that, would ya let me down please?"),
                 _("*Ouyang Nana cuts the rope from which you are hanging and releases you.*"),
                 _("Ahhh, thanks, that's much better. Now let's take these two buffoons back to Wudang and get you married off.")],
                ["Ouyang Nana", "you", "you", "Blank", "you"],
            )

            self.generate_dialogue_sequence(
                None,
                "Wudang.png",
                [_("Elder Li, it seems that there is hope for the next generation of young warriors."),
                 _("Indeed, Elder Zhou, these two youngsters have done a brilliant job!"),
                 _("Congratulations, you two, you've done a great deed for the Martial Arts Community!"),
                 _("It is our duty to protect the weak and poor!"),
                 _("Your chivalry + 20."),
                 _("So..."),
                 _("Ah, yes! Now I know that you are truly a brave and chivalrous young lady!"),
                 _("We couldn't be happier to have you as our student's future wife!"),
                 _("In fact, the only person happier than us is probably... Xu Jun, you can come out now."), #Row 2
                 _("Nana!"),
                 _("Xu Jun... *Blush* ... I..."),
                 _("It's good to see you again... I... uh..."),
                 _("*Ahem* Elder Zhou, let's leave the young ones to talk alone."),
                 _("Ah, that's right. {}, why don't you come with us? We have some interesting things to show you.").format(self.character),
                 _("Ohhh yes, yes..."),
                 _("*With a smile spread across your face, you glance over at Ouyang Nana and Xu Jun one more time before following Elder Li and Elder Zhou.*")],
                ["Zhou Enrui", "Li Kefan", "Li Kefan", "you", "Blank", "Ouyang Nana", "Li Kefan", "Zhou Enrui",
                 "Zhou Enrui", "Xu Jun", "Ouyang Nana", "Xu Jun", "Li Kefan", "Zhou Enrui", "you", "Blank"],
            )

            self.generate_dialogue_sequence(
                None,
                "Wudang.png",
                [_("*Two days later...*"),
                 _("First, kowtow to heaven and earth!"),
                 _("Second, kowtow to the parents!"),
                 _("Now, husband and wife pay respect to each other."),
                 _("Excellent, Xu Jun, Ouyang Nana, you two are now officially husband and wife!"),
                 _("Xu Jun, you may now take the bride into the marriage chamber."),
                 _("Congratulations you two!"),
                 _("What a joyous day it is! I have not felt this happy in a long time."),
                 _("Xu Jun, you must take good care of Nana. Nana, if he is mean to you, then come to me."), #Row 2
                 _("Grandmaster will discipline him!"),
                 _("Haha, thank you Grandmaster Zhang!~"),
                 _("Grandmaster Zhang, Elder Li and Elder Zhou, you all have been so kind to me and raised me as if I were your child."),
                 _("I am sorry to have to leave Wudang, but please do understand. We just want to avoid creating conflict between Wudang and Dragon Sect."),
                 _("Ah, silly child, Grandmaster is delighted that you have found such a lovely wife of good character! What a blessing from the Heavens!"),
                 _("Just be sure to visit us when you get a chance and be careful as you roam the world."), #Row 3
                 _("Yes, Grandmaster, we will definitely visit you often!"),
                 _("If you guys run into any trouble, just let me know! I'll be the first to rush to your aide!"),
                 _("Hehe, thank you, {}. I really appreciate your help and encouragement that made all of this possible.").format(self.character),
                 _("Well, all I can say is that you guys are a cute couple, and I'm glad I could be a part of this adventure!"),
                 _("*As laughter fills the cheerful atmosphere, you and the others fill your stomachs with delicious food and wine.*"),
                 _("*The next day, you bid everyone farewell to begin your next adventure.*")],
                ["Blank", "Li Kefan", "Li Kefan", "Li Kefan", "Li Kefan", "Li Kefan", "Zhou Enrui", "Zhang Tianjian",
                 "Zhang Tianjian", "Zhang Tianjian", "Ouyang Nana", "Xu Jun", "Xu Jun", "Zhang Tianjian",
                 "Zhang Tianjian", "Ouyang Nana", "you", "Ouyang Nana", "you", "Blank", "Blank"],
            )

            self.taskProgressDic["ouyang_nana_xu_jun"] = 30
            self.generateGameWin()

            
        elif stage == 21: #defeated Goblins --> Nana & Xu Jun get married --> head back to claim reward
            self.stopSoundtrack()
            self.currentBGM = "hand_in_hand.mp3"
            self.startSoundtrackThread()
            self.battleID = None
            self.chivalry += 20
            self.generate_dialogue_sequence(
                None,
                "JiangnanForest.png",
                [_("Yay!!! Nicely done, {}!").format(self.character),
                 _("Hehehehe... Now let's take these two buffoons back to Wudang and get you married off."),],
                ["Ouyang Nana", "you"],
            )

        self.generate_dialogue_sequence(
            None,
            "Wudang.png",
            [_("Elder Li, it seems that there is hope for the next generation of young warriors."),
             _("Indeed, Elder Zhou, these two youngsters have done a brilliant job!"),
             _("Congratulations, you two, you've done a great deed for the Martial Arts Community!"),
             _("It is our duty to protect the weak and poor!"),
             _("Your chivalry + 20."),
             _("So..."),
             _("Ah, yes! Now I know that you are truly a brave and chivalrous young lady!"),
             _("We couldn't be happier to have you as our student's future wife!"),
             _("In fact, the only person happier than us is probably... Xu Jun, you can come out now."),  # Row 2
             _("Nana!"),
             _("Xu Jun... *Blush* ... I..."),
             _("It's good to see you again... I... uh..."),
             _("*Ahem* Elder Zhou, let's leave the young ones to talk alone."),
             _("Ah, that's right. {}, why don't you come with us? We have some interesting things to show you.").format(self.character),
             _("Ohhh yes, yes..."),
             _("*With a smile spread across your face, you glance over at Ouyang Nana and Xu Jun one more time before following Elder Li and Elder Zhou.*")],
            ["Zhou Enrui", "Li Kefan", "Li Kefan", "you", "Blank", "Ouyang Nana", "Li Kefan", "Zhou Enrui",
             "Zhou Enrui", "Xu Jun", "Ouyang Nana", "Xu Jun", "Li Kefan", "Zhou Enrui", "you", "Blank"],
        )

        self.generate_dialogue_sequence(
            None,
            "Wudang.png",
            [_("*Two days later...*"),
             _("First, kowtow to heaven and earth!"),
             _("Second, kowtow to the parents!"),
             _("Now, husband and wife pay respect to each other."),
             _("Excellent, Xu Jun, Ouyang Nana, you two are now officially husband and wife!"),
             _("Xu Jun, you may now take the bride into the marriage chamber."),
             _("Congratulations you two!"),
             _("What a joyous day it is! I have not felt this happy in a long time."),
             _("Xu Jun, you must take good care of Nana. Nana, if he is mean to you, then come to me."),  # Row 2
             _("Grandmaster will discipline him!"),
             _("Haha, thank you Grandmaster Zhang!~"),
             _("Grandmaster Zhang, Elder Li and Elder Zhou, you all have been so kind to me and raised me as if I were your child."),
             _("I am sorry to have to leave Wudang, but please do understand. We just want to avoid creating conflict between Wudang and Dragon Sect."),
             _("Ah, silly child, Grandmaster is delighted that you have found such a lovely wife of good character! What a blessing from the Heavens!"),
             _("Just be sure to visit us when you get a chance and be careful as you roam the world."),  # Row 3
             _("Yes, Grandmaster, we will definitely visit you often!"),
             _("If you guys run into any trouble, just let me know! I'll be the first to rush to your aide!"),
             _("Hehe, thank you, {}. I really appreciate your help and encouragement that made all of this possible.").format(self.character),
             _("Well, all I can say is that you guys are a cute couple, and I'm glad I could be a part of this adventure!"),
             _("*As laughter fills the cheerful atmosphere, you and the others fill your stomachs with delicious food and wine.*"),
             _("*The next day, after receiving some 'Hundred Seeds Pills' as a parting gift, you bid everyone farewell to begin your next adventure.*")],
            ["Blank", "Li Kefan", "Li Kefan", "Li Kefan", "Li Kefan", "Li Kefan", "Zhou Enrui", "Zhang Tianjian",
             "Zhang Tianjian", "Zhang Tianjian", "Ouyang Nana", "Xu Jun", "Xu Jun", "Zhang Tianjian",
             "Zhang Tianjian", "Ouyang Nana", "you", "Ouyang Nana", "you", "Blank", "Blank"],
        )

        self.add_item(_("Hundred Seeds Pill"), 10)
        self.taskProgressDic["ouyang_nana_xu_jun"] = 30
        self.generateGameWin()





    def interact_ouyang_nana_jiangnan_forest(self, choice=0):
        if choice == 0:
            self.jiangnan_forest_win.withdraw()
            self.generate_dialogue_sequence(
                None,
                "JiangnanForest.png",
                [_("Alright, {}, what should we do next?").format(self.character)],
                ["Ouyang Nana"],
                [[_("Let's spar!"), partial(self.interact_ouyang_nana_jiangnan_forest, 1)],
                 [_("I want to jump from tree to tree to practice my agility."), partial(self.interact_ouyang_nana_jiangnan_forest, 2)],
                 [_("Let's draw attention to ourselves."), partial(self.interact_ouyang_nana_jiangnan_forest, 3)],
                 [_("Let's get out of here. This is too dangerous."), partial(self.interact_ouyang_nana_jiangnan_forest, 4)],]
            )

        else:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            
            if self.taskProgressDic["ouyang_nana_xu_jun"] >= 20:
                self.jiangnan_forest(1)
            
            self.gameDate += 1
            self.taskProgressDic["ouyang_nana_xu_jun"] += 1
            if choice == 1:
                opp = character(_("Ouyang Nana"), 12 + (self.taskProgressDic["ouyang_nana_xu_jun"]-10)//4,
                                sml=[special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Fairy Sword Technique"), level=7)],
                                skills=[skill(_("Burst of Potential"), level=3),
                                        skill(_("Basic Agility Technique"), level=5)],
                                equipmentList=[Equipment(_("Iron Sword")),]
                                )

                self.battleMenu(self.you, opp, "JiangnanForest.png", "jy_shanguxingjin.mp3",
                                fromWin = self.jiangnan_forest_win, battleType = "training", 
                                destinationWinList = [self.jiangnan_forest_win]*3
                )
                
            elif choice == 2:
                self.dexterity += 2
                self.generate_dialogue_sequence(
                    self.jiangnan_forest_win,
                    "JiangnanForest.png",
                    [_("Okkkk, you go ahead and do that. I'm going to train for a bit..."),
                     _("*You spend a few hours jumping from tree to tree. Your dexterity + 2.*")],
                    ["Ouyang Nana", "Blank"],
                )
                
            elif choice == 3:
                if self.taskProgressDic["ouyang_nana_xu_jun"] >= 12:
                    self.jiangnan_forest_win.withdraw()
                    self.generate_dialogue_sequence(
                        None,
                        "JiangnanForest.png",
                        [_("Ayyyyy, we've gotta take a break. I can't do this anymore..."),
                         _("The gold is too heavy. I need to rest for a while to catch my breath..."),
                         _("You're right... I didn't think 20kg of gold would be this much burden."),
                         _("Good thing you're helping me carry the other 60kg."),
                         _("I can't believe your grandfather left you such a large inheritance."),
                         _("After I help you transport this, you'd better give me 10kg instead of the original 5kg that you promised."),
                         _("This was much harder than I thought it would be."),],
                        ["you", "you", "Ouyang Nana", "Ouyang Nana", "you", "you", "you"],
                    )
                    self.jiangnan_forest(1)
                else:
                    self.jiangnan_forest(2)
            
            else:
                self.taskProgressDic["ouyang_nana_xu_jun"] = -5
                self.chivalry -= 30
                self.generate_dialogue_sequence(
                    self.jiangnan_forest_win,
                    "JiangnanForest.png",
                    [_("Your chivalry -30."),
                     _("What? I can't believe you would say that! You're such a wimp..."),
                     _("Say what you will, but I'm not going to risk my life for this. Bye!"),
                     _("Fine then! I never want to see you again! I'll take care of them on my own!"),
                     _("*You quickly exit the forest and find your way back to Scrub's house.*")],
                    ["Blank", "Ouyang Nana", "you", "Ouyang Nana", "Blank"],
                )
                
                self.jiangnan_forest_win.quit()
                self.jiangnan_forest_win.destroy()
                self.generateGameWin()



    def talk_to_ouyang_nana(self, choice=0):

        if choice == 0:
            self.innWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("*You notice the depressing look on Ouyang Nana's face.*")],
                ["Blank"],
                [[_("Order some wine and have a drink with her (cost 30 Gold)."), partial(self.talk_to_ouyang_nana, 1)],
                 [_("Ignore her."), partial(self.talk_to_ouyang_nana, -1)]]
            )

        elif choice == -1:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.innWin.deiconify()

        elif choice == 1:
            self.update_map_location("dragon_sect")
            self.update_date_label()
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            if self.inv[_("Gold")] < 30:
                messagebox.showinfo("", _("You don't have enough gold."))
                self.innWin.deiconify()
            elif self.taskProgressDic["ouyang_nana"] != -1:
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("A jug of wine please!"),
                     _("Coming right up, sir!"),
                     _("{}....?").format(self.character),
                     _("I'm glad you still recognize me! You look troubled... what's bothering you?"),
                     _("*sigh*... Family issues..."),
                     _("Tell me about it; maybe I can help you..."),
                     _("It's my uncle... he... he won't approve of me marrying Xu Jun from Wudang Sect..."),
                     _("So I ran away from home.")],
                    ["you", "Waiter", "Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana"]
                )
            else:
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("A jug of wine please!"),
                     _("Coming right up, sir!"),
                     _("And you are...?"),
                     _("Hey there, young lady, my name is {}. Nice to meet you. You look troubled... what's bothering you?").format(self.character),
                     _("I'm Ouyang Nana; nice to meet you... I... nevermind... don't worry about it.."),
                     _("Come on, tell me; maybe I can help you..."),
                     _("*Sigh*... It's my uncle... he... he won't approve of me marrying Xu Jun from Wudang Sect..."),
                     _("So I ran away from home.")],
                     ["you", "Waiter", "Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana"]
                )

            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                 [_("You live with your uncle?"),
                  _("Yeah... he's my dad's older brother. My mom passed away giving birth to me, and my dad died 6 years later."),
                  _("I'm sorry to hear that..."),
                  _("Well, my uncle's taken very good care of me, and his daughter, my older cousin, is also like a sister to me."),
                  _("If your uncle is so good to you, why does he oppose your love interest?"),
                  _("As far as I know, Wudang is quite a reputable sect, and Wudang disciples are all chivalrous and quite skilled!"),
                  _("Well, that's the problem... You see, my uncle is the leader of the Dragon Sect."),
                  _("He's a bit arrogant and thinks very little of other sects..."),
                  _("I think part of it has to do with the fact that he never joined any sects when he was young."), # row 2
                  _("He learned and practiced martial arts on his own, through sparring with numerous opponents."),
                  _("After 40 years of self-taught martial arts, he started his own Sect and invented his own technique: the Violent Dragon Palm."),
                  _("Sounds pretty powerful! Er, anyway... how did you even meet Xu Jun and fall in love with him?"),
                  _("We met at the annual Wulin Conference two years ago... A lot of Sects and Clans were there."),
                  _("He was one of my opponents during the casual sparring contests."),
                  _("Oh? Who won?"),
                  _("He did, but only barely... I remember him countering my attack with his Taichi Fist."),
                  _("His inner energy is more powerful than mine, and I was pushed back several feet."), # row 3
                  _("That's when he... he caught me in his arms to prevent me from getting hurt..."),
                  _("I think I fell for him in that very moment..."),
                  _("Wow, quite a story..."),
                  _("*sigh*... I only get to see him once or twice a year, when there's a big event."),
                  _("I think we made eye contact several times. When I stare into his eyes, I... I just can't control myself..."),
                  _("I want to take in every detail about him and engrave his image in my mind.")
                 ],
                 ["you", "Ouyang Nana", "you", "Ouyang Nana", "you", "you", "Ouyang Nana", "Ouyang Nana",
                 "Ouyang Nana", "Ouyang Nana", "Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana", "you", "Ouyang Nana",
                 "Ouyang Nana", "Ouyang Nana", "Ouyang Nana", "you", "Ouyang Nana", "Ouyang Nana", "Ouyang Nana",
                 ],
                [[_("I know that feeling, Nana..."), partial(self.talk_to_ouyang_nana, 11)],
                 [_("Wow, sounds like you are obsessed with this dude."), partial(self.talk_to_ouyang_nana, 12)]]
            )


        elif choice == 11:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.taskProgressDic["ouyang_nana"] += 10
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("You do??? That means you've been in love before, too!"),
                 _("Tell me about your story!"),
                 _("Er, how about another time? We've gotta take care of your problem first."),
                 _("Oh, right... what do you think we should do?")],
                ["Ouyang Nana", "Ouyang Nana", "you", "Ouyang Nana"],
                [[_("Let's go talk to your uncle"), partial(self.talk_to_ouyang_nana, 21)],
                 [_("I'll take you to Wudang to meet Xu Jun"), partial(self.talk_to_ouyang_nana, 22)]]
            )

        elif choice == 12:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("*sigh*... You don't understand..."),
                 _("I guess I've never really fallen that deeply for anyone before... Anyway, let's take care of your problem first."),
                 _("Oh, right... what do you think we should do?")],
                ["Ouyang Nana", "you", "Ouyang Nana"],
                [[_("Let's go talk to your uncle"), partial(self.talk_to_ouyang_nana, 21)],
                 [_("I'll take you to Wudang to meet Xu Jun"), partial(self.talk_to_ouyang_nana, 22)]]
            )

        elif choice == 21:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("I'll convince your uncle to change his mind!"),
                 _("R-really? You'll do that? But... he... he's kinda stubborn..."),
                 _("No worries, you can count on me! Let's go!"),
                 _("Alright! Follow me!")
                ],
                ["you", "Ouyang Nana", "you", "Ouyang Nana"]
            )
            self.ouyang_nana_task(1)

        elif choice == 22:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("Let's go up to Wudang and look for Xu Jun. Once we find him, you confess your feelings, ok?"),
                 _("I... but... I don't even know if he likes me..."),
                 _("Come on! Didn't you escape from your uncle for this very reason?"),
                 _("If you don't try now, you might not get another chance!"),
                 _("You're right... This is the only opportunity I have... I can't let it go to waste..."),
                 _("That's the spirit! Follow me!")
                 ],
                ["you", "Ouyang Nana", "you", "you", "Ouyang Nana", "you"]
            )
            self.ouyang_nana_task(2)


    def ouyang_nana_task(self, choice):
        if choice == 1:
            Scene_dragon_sect(self, "Dragon Sect")
        elif choice == 2:
            self.taskProgressDic["ouyang_nana"] += 5
            Scene_dragon_sect(self, "Wudang")


    def talk_to_guo_zhiqiang(self):
        self.generate_dialogue_sequence(
            self.innWin,
            "Inn.png",
            [_("Wine... I want... more wine... *hiccup*"),
             _("Hey, get it together man! I have news for you."),
             _("Oh hey... it's you, {}...").format(self.character),
             _("I want to get drunk... Feel free to join me; otherwise, please leave me alone..."),
             _("I know you're sad about Huilin, but guess what... She's alive and well!"),
             _("W-what?? What did you say??"),
             _("She's still alive; she didn't die after falling off the cliff."), #row 2
             _("*You tell Guo Zhiqiang the details of what happened.*"),
             _("You... you're serious?! Huilin... she's waiting for me at Huashan???"),
             _("Yup, better hurry~"),
             _("Thank you, {}!!! Thank you so much!!!").format(self.character),
             _("I hope this will repay even a fraction of what I owe you!")
             ],
            ["Guo Zhiqiang", "you", "Guo Zhiqiang", "Guo Zhiqiang", "you", "Guo Zhiqiang",
             "you", "Blank", "Guo Zhiqiang", "you", "Guo Zhiqiang", "Guo Zhiqiang"]
        )

        self.taskProgressDic["guo_zhiqiang"] = 17
        self.innF3.place(x=-3000, y=-3000)
        existing_moves = [m.name for m in self.sml]
        if _("Thousand Swords Technique") not in existing_moves:
            new_move = special_move(name=_("Thousand Swords Technique"))
            self.learn_move(new_move)
            messagebox.showinfo("", _("Before departing for Huashan, Guo Zhiqiang teaches you a new move: Thousand Swords Technique!"))
        elif _("Huashan Sword Technique") not in existing_moves:
            new_move = special_move(name=_("Huashan Sword Technique"))
            self.learn_move(new_move)
            messagebox.showinfo("", _("Before departing for Huashan, Guo Zhiqiang teaches you a new move: Huashan Sword Technique!"))
        else:
            self.add_item(_("Gold"), 3000)
            messagebox.showinfo("", _("Guo Zhiqiang gives you 3000 gold before leaving for Huashan."))



    def clickedOnCustomer(self, choice=0):
        if self.taskProgressDic["feng_xinyi"] == -1 and choice == 0:
            self.generate_dialogue_sequence(
                self.innWin,
                "Inn.png",
                [_("Hey there! I'm starving! Can you treat me to some dumplings?")],
                ["Customer"],
                [[_("Sure, here you go."),
                 partial(self.clickedOnCustomer, 1)],
                [_("Do I look like I'm made of gold? Go buy your own..."),
                 partial(self.clickedOnCustomer, 2)]]
            )
            return

        elif self.taskProgressDic["feng_xinyi"] == -1 and choice==1:
            try:
                if self.inv[_("Dumplings")] > 0:
                    self.inv[_("Dumplings")] -= 1
                    print(_("You give one of your dumplings away to this customer."))
                    if random() <= .3:
                        self.taskProgressDic["feng_xinyi"] = 0
                else:
                    messagebox.showinfo("", _("Not enough dumplings in inventory."))

            except:
                messagebox.showinfo("", _("Not enough dumplings in inventory."))


        elif self.taskProgressDic["feng_xinyi"] == 0 and choice == 0:
            if _("Basic Sword Technique") in [m.name for m in self.sml]:
                self.taskProgressDic["feng_xinyi"] = 1
                self.innF2.place(x=-3000,y=-3000)
                self.generate_dialogue_sequence(
                    self.innWin,
                    "Inn.png",
                    [_("Wow, you are a really kind person!"),
                     _("I haven't met anyone as generous as you since I've started traveling..."),
                     _("Say, I want to be friends with you and give you a small gift.")],
                    ["Customer", "Customer", "Customer"]
                )
                if self.luck >= 80:
                    self.inv["Gold"] += 2000
                    messagebox.showinfo("", _("Received Gold x 2000!"))
                else:
                    self.inv["Gold"] += 1000
                    messagebox.showinfo("", _("Received Gold x 1000!"))

            else:
                self.generate_dialogue_sequence(
                    self.innWin,
                    "Inn.png",
                    [_("Wow, you are a really kind person!"),
                     _("I haven't met anyone as generous as you since I've started traveling..."),
                     _("Say, I want to be friends with you and give you a small gift."),
                     _("Do you prefer money or learning something?")],
                    ["Customer", "Customer", "Customer", "Customer"],
                    [[_("Teach me something please."),
                      partial(self.clickedOnCustomer, 1)],
                     [_("I'll take the gold please."),
                      partial(self.clickedOnCustomer, 2)]]
                )
            return


        elif self.taskProgressDic["feng_xinyi"] == 0 and choice == 1:
            self.taskProgressDic["feng_xinyi"] = 1
            self.innF2.place(x=-3000, y=-3000)
            new_move = special_move(name=_("Basic Sword Technique"))
            self.learn_move(new_move)
            messagebox.showinfo("", _("Customer taught you a new move: Basic Sword Technique!"))


        elif self.taskProgressDic["feng_xinyi"] == 0 and choice == 2:
            self.taskProgressDic["feng_xinyi"] = 1
            self.innF2.place(x=-3000, y=-3000)
            if self.luck >= 80:
                self.inv["Gold"] += 2000
                messagebox.showinfo("", _("Received Gold x 2000!"))
            else:
                self.inv["Gold"] += 1000
                messagebox.showinfo("", _("Received Gold x 1000!"))


        elif self.taskProgressDic["feng_xinyi"] == 1:
            self.generate_dialogue_sequence(
                self.innWin,
                "Inn.png",
                [_("Hey there!"),
                 _("Hi... you... look familiar for some reason..."),
                 _("Ay, you block head. Of course I do. I'm the person whom you treated earlier, remember?"),
                 _("Ohhhhh! The dumpling guy? So you are a girl?"),
                 _("Duh... I disguised myself as a male. My real name is Feng Xinyi."),
                 _("You've got a few tricks up your sleeve, I see..."),
                 _("Hah, you haven't seen anything yet! Wait till I show you some moves..."),
                 _("Quick spar? I suppose that can't hurt..."),],
                ["Feng Xinyi", "you", "Feng Xinyi", "you", "Feng Xinyi","you", "Feng Xinyi", "you"]
            )


            self.taskProgressDic["feng_xinyi"] = 2
            self.spar_with_feng_xinyi()
            return


        elif self.taskProgressDic["feng_xinyi"] == 2:
            if self.taskProgressDic["join_sect"] == 500:
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("Wait, where did you learn Shaolin Kungfu?"),
                     _("As far as I know, Shaolin doesn't accept female disciples..."),
                     _("I sneak in, of course..."),
                     _("Sneak in???"),
                     _("Yup, during the day it's very difficult, but at night, when I'm disguised in a ninja outfit, it's much safer."),
                     _("Plus, I came upon a lightly guarded area."),
                     _("I've even located the Scripture Library where the martial arts manuals are stored, along with a bunch of other boring books unfortunately..."),
                     _("If you ever want to go with me, just let me know!"),
                     _("Hold on a second! I'm a Shaolin Disciple!"),
                     _("I can't let you steal our Kung-Fu and get away with it..."),
                     _("Is that so? What are you going to do to stop me?")],
                    ["you", "you", "Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi", "you",
                     "you", "Feng Xinyi"]
                )
                
                opp = character(_("Feng Xinyi"), 9,
                                sml=[special_move(_("Shaolin Luohan Fist"), level=6),
                                     special_move(_("Basic Sword Technique"), level=9),
                                     special_move(_("Shaolin Diamond Finger"), level=4),
                                     special_move(_("Basic Punching Technique"), level=10)],
                                skills=[skill(_("Shaolin Mind Clearing Method"), level=4),
                                        skill(_("Shaolin Inner Energy Technique"), level=3)])
        
                self.battleMenu(self.you, opp, "battleground_inn.png",
                                postBattleSoundtrack="jy_relaxing2.mp3",
                                fromWin=self.innWin,
                                battleType="task", destinationWinList=[] * 3,
                                postBattleCmd=self.feng_xinyi_busted
                                )
            
                return

            else:
                self.generate_dialogue_sequence(
                    self.innWin,
                    "Inn.png",
                    [_("Wait, where did you learn Shaolin Kungfu?"),
                     _("As far as I know, Shaolin doesn't accept female disciples..."),
                     _("I sneak in, of course..."),
                     _("Sneak in???"),
                     _("Yup, during the day it's very difficult, but at night, when I'm disguised in a ninja outfit, it's much safer."),
                     _("Plus, I came upon a lightly guarded area."),
                     _("I've even located the Scripture Library where the martial arts manuals are stored, along with a bunch of other boring books unfortunately..."),
                     _("If you ever want to go with me, just let me know!"),
                     _("Ah... let me think about it haha..."),
                     _("(Hmmm, it sounds like a good opportunity to learn some rare techniques, but if I get caught...)"),
                     _("(The consequences could be very severe...)")],
                    ["you", "you", "Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi", "you",
                     "you", "you"]
                )
                self.taskProgressDic["feng_xinyi"] = 3
                return


        elif self.taskProgressDic["feng_pan"] == 0:
            self.innWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("Hey... Quick question..."),
                 _("Mhmm?"),
                 _("Are you the daughter of General Feng Pan?"),
                 _(".........."),
                 _("Why do you want to know?"),
                 _("I came across General Feng while messing around in the imperial palace..."),
                 _(".......... He... he's too controlling... Home feels like a prison."),
                 _("I really couldn't take it anymore, so I snuck out..."),
                 _("I disguised myself as a male initially to reduce my chances of getting caught...")],
                ["you", "Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi"],
                [[_("Your dad's offering 10000 Gold to whoever brings you back, so you're coming with me."),
                  partial(self.feng_xinyi_choice, 10)],
                 [_("I heard he's looking for you, but don't worry. I'll make sure no one harms you."),
                  partial(self.feng_xinyi_choice, 20)]]
            )

        elif self.taskProgressDic["feng_xinyi"] >= 3:

            for widget in self.innWinF1.winfo_children():
                widget.destroy()

            Button(self.innWinF1, text=_("Spar"),
                   command=self.spar_with_feng_xinyi).grid(row=1, column=0)
            Button(self.innWinF1, text=_("Sneak into Shaolin"),
                   command=self.sneak_into_shaolin).grid(row=2, column=0)

        try:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
        except:
            pass

        self.innWin.deiconify()


    def feng_xinyi_choice(self, choice):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        if choice == 10:
            self.taskProgressDic["feng_pan"] = 10
            self.chivalry -= 30
            messagebox.showinfo("", _("Your chivalry -30!"))
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("Wow, I can't believe I treated you as my friend this whole time..."),
                 _("Now you're going to backstab me over money???"),
                 _("I gotta make a living, y'know..."),
                 _("I'm not going anywhere with you. From this day forward, I have nothing to do with you."),
                 _("I'm afraid you don't get to decide where to go."),
                 _("I say you're going home, and that's that.")],
                ["Feng Xinyi", "Feng Xinyi", "you", "Feng Xinyi", "you", "you"]
            )
            
            opp = character(_("Feng Xinyi"), 9,
                            sml=[special_move(_("Shaolin Luohan Fist"), level=6),
                                 special_move(_("Basic Sword Technique"), level=9),
                                 special_move(_("Shaolin Diamond Finger"), level=4),
                                 special_move(_("Basic Punching Technique"), level=10)],
                            skills=[skill(_("Shaolin Mind Clearing Method"), level=4),
                                    skill(_("Shaolin Inner Energy Technique"), level=3)])

            cmd = lambda: self.feng_xinyi_choice(11)
            self.battleMenu(self.you, opp, "battleground_inn.png",
                            postBattleSoundtrack="jy_shenmishandong.mp3",
                            fromWin=self.innWin,
                            battleType="task", destinationWinList=[] * 3,
                            postBattleCmd=cmd
                            )

        elif choice == 11:
            self.taskProgressDic["feng_pan"] = 11
            self.innWin.quit()
            self.innWin.destroy()
            self.generate_dialogue_sequence(
                self.mapWin,
                "battleground_imperial_palace_night_2.png",
                [_("General Feng! I've found your daughter!"),
                 _("Xinyi! Where have you been?! Your brother and I have looked everywhere for you!"),
                 _("I... Dad, why can't I learn martial arts? Why do you have to control every aspect of my life?"),
                 _("It's for your own good, my dear daughter... One day you will understand..."),
                 _("But I'm 18! I'm old enough to make my own decisions!"),
                 _("Ahem... Don't mean to interrupt your conversation, but I believe there was a reward, General Feng..."),
                 _("Oh, right, how could I forget? I'll have the servants wrap up the 10000 Gold for you right away."),
                 _("I hate you, {}...").format(self.character),
                 _("Enjoy your time with your family! One day you'll thank me for it hahahaha...")],
                ["you", "Feng Pan", "Feng Xinyi", "Feng Pan", "Feng Xinyi", "you", "Feng Pan", "Feng Xinyi", "you",
                 ]
            )
            self.add_item(_("Gold"), 10000)
            messagebox.showinfo("", _("Received 'Gold' x 10000!"))
            
            
        elif choice == 20:
            self.taskProgressDic["feng_pan"] = 20
            self.chivalry += 30
            messagebox.showinfo("", _("Your chivalry +30!"))
            self.generate_dialogue_sequence(
                self.innWin,
                "Inn.png",
                [_("Thanks, {}, I knew I could count on you!").format(self.character),
                 _("I will be very cautious, but I don't think you want to mess with my father."),
                 _("I don't want you to get into trouble..."),
                 _("Come on, Xinyi, I would do this for any friend of mine."),
                 _("Thank you, {}. I'm so glad I have a friend like you.").format(self.character),
                 _("I'm going to change back into my disguise now that I know they're looking for me."),
                 _("Hehehe... Ok, I've got some other business to take care of, but I'll check up on your periodically."),
                 _("Be careful, and see ya later!")],
                ["Feng Xinyi", "Feng Xinyi", "Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi", "you", "you"]
            )
        

    def spar_with_feng_xinyi(self):

        opp = character(_("Feng Xinyi"), 7,
                        sml=[special_move(_("Shaolin Luohan Fist"), level=4),
                             special_move(_("Basic Sword Technique"), level=8),
                             special_move(_("Shaolin Diamond Finger"), level=4),
                             special_move(_("Basic Punching Technique"), level=10)],
                        skills=[skill(_("Shaolin Mind Clearing Method"), level=4),
                                skill(_("Shaolin Inner Energy Technique"), level=3)])

        self.battleMenu(self.you, opp, "battleground_inn.png",
                        postBattleSoundtrack="jy_relaxing2.mp3",
                        fromWin=self.innWin,
                        battleType="training", destinationWinList=[] * 3,
                        postBattleCmd=self.clickedOnCustomer
                        )


    def sneak_into_shaolin(self):

        self.innWin.withdraw()
        if self.taskProgressDic["feng_xinyi"] == 3:
            self.taskProgressDic["feng_xinyi"] = 4
        elif self.taskProgressDic["feng_xinyi"] == 4 and self.level >= 15 and self.taskProgressDic[_("liu_village")] < 15 and self.taskProgressDic["feng_pan"] not in [11, 70]:
            self.stopSoundtrack()
            self.currentBGM = "jy_suspenseful1.mp3"
            self.startSoundtrackThread()
            self.taskProgressDic["feng_xinyi"] = 5
            self.generate_dialogue_sequence(
                None,
                "battleground_shaolin_night.png",
                [_("Alright, just go up from here; it's very lightly guarded."),
                 _("Wait! Something's happening over there..."),
                 _("Sir, what has brought you to Shaolin so late at night?"),
                 _("Haha... I am suffering from insomnia and wanted to borrow a few books from the Scripture Library to read..."),
                 _("Is there a problem, Abbot?"),
                 _("Amitabha... We have many scripture books available, if you'd like to read them. However, the one you are holding..."),
                 _("That's the manual for one the 72 Arts of Shaolin's. Please return it, and we would be more than happy to lend you another book."), #Row 2
                 _("But Abbot, I think this is just what I need. In the past 3 years, I've been studying the Tendon Changing Manual."),
                 _("And I gotta say, it has helped me tremendously..."),
                 _("Amitabha... Sir, the Tendon Changing Manual is Shaolin's prized possession..."),
                 _("Now that you've stolen it, I'm afraid I cannot let you go so easily..."),
                 _("A chance to go against the Shaolin Abbot? What an honor...")
                 ],
                ["Feng Xinyi", "you", "Abbot", "Masked Thief", "Masked Thief", "Abbot",
                 "Abbot", "Masked Thief", "Masked Thief", "Abbot", "Abbot", "Masked Thief"
                 ],
                [[_("Help Abbot"), partial(self.thief_encounter_at_shaolin, 1)],
                 [_("Defeat both of them and get the manual"), partial(self.thief_encounter_at_shaolin, 2)],
                 [_("Stay out of this"), partial(self.thief_encounter_at_shaolin, 3)]]
            )
            return

        if _("Ninja Suit") not in self.inv:
            self.generate_dialogue_sequence(
                self.innWin,
                "Inn.png",
                [_("Here, take this; it will come in handy when you need to be stealthy.")],
                ["Feng Xinyi"]
            )
            self.inv[_("Ninja Suit")] = 1
            messagebox.showinfo("", _("Received 'Ninja Suit' x 1!"))
            return

        #Check to make sure equipping ninja suit
        if _("Ninja Suit") in [eq.name for eq in self.equipment]:
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("I'll show you the entrance. Afterwards, you're on your own. Good luck!")],
                ["Feng Xinyi"]
            )
            self.stopSoundtrack()
            self.currentBGM = "jy_shanguxingjin2.mp3"
            self.startSoundtrackThread()

            if self.taskProgressDic["yagyu_clan"] in [-4,-10,-13,-14,-15,-16,-17,10,11] or self.taskProgressDic["yagyu_clan"] >= 30:
                self.generate_dialogue_sequence(
                    self.innWin,
                    "battleground_shaolin_night.png",
                    [_("(Hmmm, this place is much more heavily guarded than usual.)"),
                     _("(There's no way I can sneak in right now...)")],
                    ["you", "you"],
                )
            else:
                self.mini_game_in_progress = True
                self.mini_game = shaolin_sneak_mini_game(self, None)
                self.mini_game.new()
        
        else:
            self.generate_dialogue_sequence(
                self.innWin,
                "Inn.png",
                [_("You gotta put on the Ninja Suit first!")],
                ["Feng Xinyi"]
            )


    def thief_encounter_at_shaolin(self, choice):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()

        if choice == 1:
            self.chivalry += 25
            messagebox.showinfo("", _("Your chivalry +25!"))
            self.generate_dialogue_sequence(
                None,
                "battleground_shaolin_night.png",
                [_("Stop right there, thief! Give the manual back to Abbot before I make you regret it."),
                 _("Hah! A thief who cries 'thief'! If I'm not mistaken, you came to Shaolin for the same purpose as me..."),
                 _("Shut it!")
                 ],
                ["you", "Masked Thief", "you"]
            )

            opp = character(_("Masked Thief"), 22,
                            sml=[special_move(_("Shaolin Diamond Finger"), level=10),
                                 special_move(_("Guan Yin Palm"), level=10),
                                 special_move(_("Smoke Bomb"), level=10),],
                            skills=[skill(_("Tendon Changing Technique"), level=5),
                                    skill(_("Basic Agility Technique"), level=10)])
            cmd = lambda: self.thief_encounter_reward(1)
            self.battleMenu(self.you, opp, "battleground_shaolin_night.png", "jy_xiaoyaogu.mp3", fromWin=None,
                            battleType="task", postBattleCmd = cmd)

        elif choice == 2:
            self.generate_dialogue_sequence(
                None,
                "battleground_shaolin_night.png",
                [_("I can resolve your conflict. Just give the book to me, and you won't have to fight over it anymore..."),
                 _("........"),
                 _("Amitabha...")
                 ],
                ["you", "Masked Thief", "Abbot"]
            )

            opp_list = []
            opp = character(_("Masked Thief"), 22,
                            sml=[special_move(_("Shaolin Diamond Finger"), level=10),
                                 special_move(_("Guan Yin Palm"), level=10),
                                 special_move(_("Smoke Bomb"), level=10),],
                            skills=[skill(_("Tendon Changing Technique"), level=5),
                                    skill(_("Basic Agility Technique"), level=10)])

            opp_list.append(opp)
            opp = character(_("Abbot"), 25,
                            sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                                 special_move(_("Shaolin Diamond Finger"), level=10),
                                 special_move(_("Guan Yin Palm"), level=10),
                                 special_move(_("Empty Force Fist"), level=8), ],
                            skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                    skill(_("Tendon Changing Technique"), level=5),
                                    skill(_("Shaolin Inner Energy Technique"), level=10),
                                    skill(_("Shaolin Agility Technique"), level=10),])
            opp_list.append(opp)

            cmd = lambda: self.thief_encounter_reward(2)
            self.battleMenu(self.you, opp_list, "battleground_shaolin_night.png", "jy_xiaoyaogu.mp3", fromWin=None,
                            battleType="task", postBattleCmd=cmd)

        elif choice == 3:
            self.generate_dialogue_sequence(
                None,
                "battleground_shaolin_night.png",
                [_("Hmmm... better stay out of this..."),
                 ],
                ["you"]
            )
            self.innWin.deiconify()


    def thief_encounter_reward(self, choice):
        if choice == 1:
            self.generate_dialogue_sequence(
                None,
                "battleground_shaolin_night.png",
                [_("There... Give me that book!"),
                 _("Here's your manual back, Abbot."),
                 _("Amitabha... Thank you, young man. As for this thief, please spare his life."),
                 _("I do not wish to see any blood shed on Shaolin ground."),
                 _("................"),
                 _("*The Masked Thief escapes.*")
                 ],
                ["you", "you", "Abbot", "Abbot", "Masked Thief", "Blank"]
            )

        elif choice == 2:
            self.generate_dialogue_sequence(
                None,
                "battleground_shaolin_night.png",
                [_("Give me that! Ahahahaha!!!"),
                 ],
                ["you"]
            )

            self.add_item(_("Empty Force Fist Manual"))
            messagebox.showinfo("", _("Obtained 'Empty Force Fist Manual'!"))

        self.innWin.deiconify()


    def detected_at_shaolin(self):
        self.mini_game_in_progress = False
        pg.display.quit()
        self.generate_dialogue_sequence(
            None,
            "battleground_shaolin_night.png",
            [_("Hey! What are you doing here?! You're not a Shaolin Disciple!"),
             _("Intruder!!! There's an intruder!!!"),
             ],
            ["Xu Qing","Xu Qing"]
        )

        if random() <= .25:
            self.generate_dialogue_sequence(
                None,
                "battleground_shaolin_night.png",
                [_("Looks like we have an unwelcome guest...")
                 ],
                ["Elder Xuanjing"]
            )
            opp_list = []
            opp_list.append(
                character(_("Elder Xuanjing"), 18,
                      sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                           special_move(_("Shaolin Diamond Finger"), level=8)],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=6),
                              skill(_("Shaolin Inner Energy Technique"), level=10),
                              skill(_("Shaolin Agility Technique"), level=10),])
            )

            if self.taskProgressDic["li_guiping_task"] < 3:
                opp_list.append(
                    character(_("Elder Xuansheng"), 16,
                          sml=[special_move(_("Shaolin Luohan Fist"), level=8),
                               special_move(_("Shaolin Diamond Finger"), level=6)],
                          skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                  skill(_("Shaolin Inner Energy Technique"), level=10),
                                  skill(_("Shaolin Agility Technique"), level=10),])
                )

            self.battleMenu(self.you, opp_list, "battleground_shaolin_night.png", "jy_xiaoyaogu.mp3", fromWin=self.mapWin,
                            battleType="normal", destinationWinList=[self.mapWin] * 3)


        else:
            self.generate_dialogue_sequence(
                None,
                "battleground_shaolin_night.png",
                [_("(I'd better get out of here before back-up arrives...)")
                 ],
                ["you"]
            )

            self.mapWin.deiconify()
        
    
    def scripture_library(self):
        self.generate_dialogue_sequence(
            None,
            "Scripture Library.png",
            [_("(I bet there are some super rare martial arts manuals in here...)"),
             _("(Where should I begin...?)"),
             ],
            ["you", "you"],
            [[_("Search a couple of book shelves (low chance of being detected)"), partial(self.search_scripture_library, "low")],
             [_("Search half of the book shelves (medium chance of being detected)"), partial(self.search_scripture_library, "medium")],
             [_("Search all of the book shelves (high chance of being detected)"), partial(self.search_scripture_library, "high")]]
        )


    def search_scripture_library(self, choice):
        #low: can get Luohan Fist
        #medium: can get Shaolin Inner Energy & Shaolin Mind Clearing
        #high: can get Shaolin Diamond Finger, Empty Force Fist, Tendon Changing Technique
        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        r = random()*self.luck/50
        r_multiplier = 1
        if choice == "low":
            r_multiplier *= .75
        elif choice == "high":
            r_multiplier *= 1.25
        r *= r_multiplier


        if r >= 1 and _("Tendon Changing Technique") not in [s.name for s in self.skills] and _("Tendon Changing Manual") not in self.inv:
            messagebox.showinfo("", _("Found 'Tendon Changing Manual'!"))
            if random() <= 1*r_multiplier:
                self.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Young man, please put that back. Otherwise, don't blame me for not showing mercy.")],
                    ["Abbot"],
                    [[_("Return what you found."), partial(self.scripture_library_battle)],
                     [_("Keep it!"), partial(self.scripture_library_battle, _("Tendon Changing Manual"))]]
                )
            else:
                self.add_item(_("Tendon Changing Manual"))
                self.mapWin.deiconify()
                
        elif r >= .85 and _("Shaolin Diamond Finger") not in [m.name for m in self.sml] and _("Shaolin Diamond Finger Manual") not in self.inv:
            messagebox.showinfo("", _("Found 'Shaolin Diamond Finger Manual'!"))
            if random() <= .85*r_multiplier:
                self.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Young man, please put that back. Otherwise, don't blame me for not showing mercy.")],
                    ["Abbot"],
                    [[_("Return what you found."), partial(self.scripture_library_battle)],
                     [_("Keep it!"), partial(self.scripture_library_battle, _("Shaolin Diamond Finger Manual"))]]
                )
            else:
                self.add_item(_("Shaolin Diamond Finger Manual"))
                self.mapWin.deiconify()

        elif r >= .8 and _("Guan Yin Palm") not in [m.name for m in self.sml] and _("Guan Yin Palm Manual") not in self.inv:
            messagebox.showinfo("", _("Found 'Guan Yin Palm Manual'!"))
            if random() <= .8*r_multiplier:
                self.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Young man, please put that back. Otherwise, don't blame me for not showing mercy.")],
                    ["Abbot"],
                    [[_("Return what you found."), partial(self.scripture_library_battle)],
                     [_("Keep it!"), partial(self.scripture_library_battle, _("Guan Yin Palm Manual"))]]
                )
            else:
                self.add_item(_("Guan Yin Palm Manual"))
                self.mapWin.deiconify()


        elif r >= .65 and _("Shaolin Inner Energy Technique") not in [s.name for s in self.skills] and _("Shaolin Inner Energy Manual") not in self.inv:
            messagebox.showinfo("", _("Found 'Shaolin Inner Energy Manual'!"))
            if random() <= .65*r_multiplier:
                self.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Young man, please put that back. Otherwise, don't blame me for not showing mercy.")],
                    ["Abbot"],
                    [[_("Return what you found."), partial(self.scripture_library_battle)],
                     [_("Keep it!"), partial(self.scripture_library_battle, _("Shaolin Inner Energy Manual"))]]
                )
            else:
                self.add_item(_("Shaolin Inner Energy Manual"))
                self.mapWin.deiconify()


        elif r >= .6 and _("Shaolin Mind Clearing Technique") not in [s.name for s in self.skills] and _("Shaolin Mind Clearing Manual") not in self.inv:
            messagebox.showinfo("", _("Found 'Shaolin Mind Clearing Manual'!"))
            if random() <= .6*r_multiplier:
                self.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Young man, please put that back. Otherwise, don't blame me for not showing mercy.")],
                    ["Abbot"],
                    [[_("Return what you found."), partial(self.scripture_library_battle)],
                     [_("Keep it!"), partial(self.scripture_library_battle, _("Shaolin Mind Clearing Manual"))]]
                )
            else:
                self.add_item(_("Shaolin Mind Clearing Manual"))
                self.mapWin.deiconify()


        elif r >= .5 and _("Shaolin Luohan Fist") not in [m.name for m in self.sml] and _("Shaolin Luohan Fist Manual") not in self.inv:
            messagebox.showinfo("", _("Found 'Shaolin Luohan Fist Manual'!"))
            if random() <= .5*r_multiplier:
                self.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Young man, please put that back. Otherwise, don't blame me for not showing mercy.")],
                    ["Abbot"],
                    [[_("Return what you found."), partial(self.scripture_library_battle)],
                     [_("Keep it!"), partial(self.scripture_library_battle, _("Shaolin Luohan Fist Manual"))]]
                )
            else:
                self.add_item(_("Shaolin Luohan Fist Manual"))
                self.mapWin.deiconify()


        elif r >= .5:
            a = randrange(1, 3)
            messagebox.showinfo("", _("Found 'Big Recovery Pill' x {}!").format(a))
            if random() <= .5*r_multiplier:
                self.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Young man, please put that back. Otherwise, don't blame me for not showing mercy.")],
                    ["Abbot"],
                    [[_("Return what you found."), partial(self.scripture_library_battle)],
                     [_("Keep it!"), partial(self.scripture_library_battle, _("Big Recovery Pill"), a)]]
                )
            else:
                self.add_item(_("Big Recovery Pill"), a)
                self.mapWin.deiconify()


        else:
            a = randrange(2, 5)
            messagebox.showinfo("", _("Found 'Small Recovery Pill' x {}!").format(a))
            if random() <= .3*r_multiplier:
                self.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Young man, please put that back. Otherwise, don't blame me for not showing mercy.")],
                    ["Abbot"],
                    [[_("Return what you found."), partial(self.scripture_library_battle)],
                     [_("Keep it!"), partial(self.scripture_library_battle, _("Small Recovery Pill"), a)]]
                )
            else:
                self.add_item(_("Small Recovery Pill"), a)
                self.mapWin.deiconify()


    def scripture_library_battle(self, choice=None, amount=1):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        if not choice:
            self.mapWin.deiconify()
        else:
            self.add_item(choice, amount)
            opp = character(_("Abbot"), 25,
                      sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                           special_move(_("Shaolin Diamond Finger"), level=10),
                           special_move(_("Guan Yin Palm"), level=10),
                           special_move(_("Empty Force Fist"), level=8), ],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                              skill(_("Tendon Changing Technique"), level=5),
                              skill(_("Shaolin Inner Energy Technique"), level=10),
                              skill(_("Shaolin Agility Technique"), level=10),])

            if self.taskProgressDic["feng_pan"] in [11, 70]:
                fw = self.mapWin
            else:
                fw = self.innWin
            self.battleMenu(self.you, opp, "battleground_scripture_library.png", "jy_xiaoyaogu.mp3", fromWin=fw,
                            battleType="normal", destinationWinList=[self.mapWin] * 3)


    def feng_xinyi_busted(self, choice=0):
        
        if choice == 0:
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("Alright, alright... you win... I'll do whatever you say..."),
                 _("Since you didn't have any ill intentions, I won't be too harsh on you."),
                 _("If you promise to forget all Shaolin Kung-Fu that you've learned and never to go back to Shaolin, I'll let you go."),
                 _("B-but... I spent a lot of time learning that!"),
                 _("How about I give you some money? 1000 --- no, 2000 gold?")],
                ["Feng Xinyi", "you", "you", "Feng Xinyi", "Feng Xinyi"],
                [[_("Decline the bribe"), partial(self.feng_xinyi_busted, 1)],
                 [_("Accept the bribe"), partial(self.feng_xinyi_busted, 2)]]
            )

        else:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.taskProgressDic["feng_xinyi"] = 100
            if choice == 1:
                self.chivalry += 15
                messagebox.showinfo("", _("Your chivalry +15!"))
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("I'm not gonna say it again..."),
                     _("Ok ok ok! I promise! Geez..."),
                     _("Good, now leave before I change my mind."),],
                    ["you", "Feng Xinyi", "you"]
                )

            elif choice == 2:
                self.inv[_("Gold")] += 2000
                self.chivalry -= 15
                messagebox.showinfo("", _("Received Gold x 2000!\nYour chivalry -15!"))
                self.generate_dialogue_sequence(
                    None,
                    "Inn.png",
                    [_("Fine fine... now leave, quick..."),
                     _("Woohoo~~ bye~"), ],
                    ["you", "Feng Xinyi"]
                )

            self.add_item(_("Ninja Suit"))
            messagebox.showinfo("", _("As she leaves, Feng Xinyi gives you her Ninja Suit."))

            self.innF2.place(x=-3000, y=-3000)
            self.innWin.deiconify()


    def gamble(self, choice=0):

        if choice == 0:
            self.innWin.withdraw()
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("Hello there, my friend! Would you like to play a little game to win some money?")],
                ["Gambler"],
                [[_("What game?"), partial(self.gamble, 1)],
                 [_("Let's go!"), partial(self.gamble, 2)],
                 [_("No thanks."), partial(self.gamble, -1)]
                ]
            )

        elif choice == 1:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.generate_dialogue_sequence(
                self.innWin,
                "Inn.png",
                [_("First you bet some amount of money: 10, 25, 50, or 100."),
                 _("Then I'll drop an ant into a den of spiders. The higher your bet, the more spiders there will be."),
                 _("If the ant survives for at least 15 seconds, you win the amount that you wagered for every 15 seconds that it survives."),
                 _("Otherwise, you lose the amount that you wagered. Just let me know whenever you're up for it~")],
                ["Gambler", "Gambler", "Gambler", "Gambler"],
            )

        elif choice == 2:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.generate_dialogue_sequence(
                None,
                "Inn.png",
                [_("Please place your bets...")],
                ["Gambler"],
                [[_("Bet 10 gold."), partial(self.initiate_gambling_mini_game, 10)],
                 [_("Bet 50 gold."), partial(self.initiate_gambling_mini_game, 50)],
                 [_("Bet 100 gold."), partial(self.initiate_gambling_mini_game, 100)],
                 [_("Bet 500 gold."), partial(self.initiate_gambling_mini_game, 500)],]
            )

        else:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.innWin.deiconify()


    def initiate_gambling_mini_game(self, amount):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        self.gamble_amount = amount
        if self.inv[_("Gold")] < self.gamble_amount:
            messagebox.showinfo("", _("You don't have enough money."))
            self.innWin.deiconify()
        else:            
            self.mini_game_in_progress = True
            self.mini_game = ant_mini_game(self)
            self.mini_game.new()


    def gambling_mini_game_reward(self, multiplier):
        self.mini_game_in_progress = False
        pg.display.quit()
        if multiplier == 0:
            self.inv[_("Gold")] -= self.gamble_amount
            messagebox.showinfo("", _("Lost {} gold!").format(self.gamble_amount))
            if self.taskProgressDic["ouyang_nana_xu_jun"] in range(1,5):
                self.taskProgressDic["ouyang_nana_xu_jun"] -= 1
        else:
            self.inv[_("Gold")] += int(self.gamble_amount*multiplier)
            messagebox.showinfo("", _("Won {} gold!").format(int(self.gamble_amount*multiplier)))
            if self.taskProgressDic["ouyang_nana_xu_jun"] in range(0,5):
                self.taskProgressDic["ouyang_nana_xu_jun"] += 1
        if multiplier > 3:
            self.luck += 10
            messagebox.showinfo("", _("Your luck + 10!"))
        elif multiplier == 3:
            self.luck += 5
            messagebox.showinfo("", _("Your luck + 5!"))

        self.innWin.deiconify()


    def order_food(self):

        self.generate_dialogue_sequence(
            self.innWin,
            "Inn.png",
            [_("Greetings, customer! Come see what delicious food we have!")],
            ["Waiter"]
        )

        self.inn_food_prices = {
            _("Crab"): 30,
            _("Soup"): 25,
            _("Fish"): 18,
            _("Dumplings"): 20,
            _("Wine"): 30
        }
        self.selected_shop_item = None
        self.innWin.withdraw()
        self.innMenuWin = Toplevel(self.innWin)

        F1 = Frame(self.innMenuWin, borderwidth=2, relief=SUNKEN, padx=5, pady=5)
        F1.pack()

        self.shop_item_buttons = []
        self.shop_item_icons = []
        self.item_price_labels = []
        self.selected_shop_item = None
        self.selected_shop_item_index = None


        item_x = 0
        item_y = 0
        item_index = 0
        for item_name in self.inn_food_prices:
            item_icon = PhotoImage(file=_("i_{}.ppm").format(item_name))
            item_button = Button(F1, image=item_icon, command=partial(self.selectShopItem, item_name, item_index))
            item_button.image = item_icon
            item_button.grid(row=item_y, column=item_x)

            item_description = item_name + "\n------------------------------\n" + ITEM_DESCRIPTIONS[item_name]
            self.balloon.bind(item_button, item_description)
            self.shop_item_icons.append(item_icon)
            self.shop_item_buttons.append(item_button)

            price_label = Label(text="Price:" + str(self.inn_food_prices[item_name]), master=F1)
            price_label.grid(row=item_y + 1, column=item_x)
            self.item_price_labels.append(price_label)

            item_x += 1
            item_index += 1


        F2 = Frame(self.innMenuWin, padx=5, pady=5)
        F2.pack()

        Button(F2, text="Back", command=partial(self.winSwitch, self.innMenuWin, self.innWin)).grid(row=0, column=0)
        Label(F2, text = "  ").grid(row=0, column=1)
        Button(F2, text="Buy", command=partial(self.buy_selected_shop_item, self.inn_food_prices)).grid(row=0, column=2)
        Label(F2, text="  ").grid(row=0, column=3)

        self.shop_gold_amount_label = Label(F2, text=_("Your gold: ")+ str(self.inv[_("Gold")]))
        self.shop_gold_amount_label.grid(row=0,column=4)


        self.innMenuWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.innMenuWin.update_idletasks()
        toplevel_w, toplevel_h = self.innMenuWin.winfo_width(), self.innMenuWin.winfo_height()
        self.innMenuWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.innMenuWin.focus_force()
        self.innMenuWin.mainloop()###


    def selectShopItem(self, item_name, item_index):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.selected_shop_item:
            self.shop_item_icons[self.selected_shop_item_index] = PhotoImage(file=_("i_{}.ppm").format(self.selected_shop_item))
            self.shop_item_buttons[self.selected_shop_item_index].config(image=self.shop_item_icons[self.selected_shop_item_index])

        self.selected_shop_item = item_name
        self.selected_shop_item_index = item_index

        self.shop_item_icons[self.selected_shop_item_index] = PhotoImage(file=_("i_{}_selected.ppm").format(self.selected_shop_item))
        self.shop_item_buttons[self.selected_shop_item_index].config(image=self.shop_item_icons[self.selected_shop_item_index])


    def buy_selected_shop_item(self, price_dic):

        if not self.selected_shop_item:
            messagebox.showinfo("", _("No item selected."))
            return

        if self.inv[_("Gold")]< price_dic[self.selected_shop_item]:
            messagebox.showinfo("", _("Not enough gold."))
            return

        else:
            self.currentEffect = "button-19.mp3"
            self.startSoundEffectThread()
            self.add_item(self.selected_shop_item)
            self.inv[_("Gold")] -= price_dic[self.selected_shop_item]
            self.shop_gold_amount_label.config(text=_("Your gold: ")+ str(self.inv[_("Gold")]))
            print(_("Purchased {}.").format(self.selected_shop_item))


    def robber_encounter(self):

        self.stopSoundtrack()
        self.currentBGM = "jy_suspenseful1.mp3"
        self.startSoundtrackThread()

        self.generate_dialogue_sequence(
            None,
            "forest.ppm",
            [_("Heeeeeelp! Someone help me!!!"),
             _("Robbers! Someone please save me!"),
             _("Calm down bro... You're lucky you met me today."),
             _("Where are the robbers? I'll take care of them for you."),
             _("Hah! So this guy's looking for trouble, huh? Then I'll make you my victim as well!")],
            ["Merchant", "Merchant", "you", "you", "Masked Robber"]
        )

        enemy_level = pickOne([1,2,2,2,3,3,4])
        post_soundtrack = pickOne(["jy_huha.mp3", "jy_heroic.mp3", "jy_xiaoyaogu.mp3", "jy_xiaoyaogu3.mp3"])
        cmd = lambda: self.generate_dialogue_sequence(None,
                                                      "forest.ppm",
                                                      [_("Argh! You got lucky this time... Just you wait..."),
                                                       _("Thank you for saving me, young hero!")],
                                                      ["Masked Robber", "Merchant"],
                                                      [[_("You're welcome! Be careful from now on."),
                                                        partial(self.robber_encounter_post_battle, True)],
                                                       [_("Who said I was helping you? I just want all the money for myself. Now hand it over!"),
                                                           partial(self.robber_encounter_post_battle, False)]
                                                       ])

        self.battleMenu(self.you, character(_("Masked Robber"), enemy_level,
                                            sml=[special_move(_("Basic Sword Technique"), level=enemy_level + 1)]),
                        "battleground_forest.ppm", postBattleSoundtrack=post_soundtrack, fromWin=self.mapWin,
                        postBattleCmd=cmd)


    def robber_encounter_post_battle(self, return_money):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        if return_money:
            messagebox.showinfo("", _("Your chivalry +10."))
            self.chivalry += 10

        else:
            self.generate_dialogue_sequence(
                None,
                "forest.ppm",
                [_("You... you..."),
                 _("*Sigh* People nowadays... Fine, take it then...")],
                ["Merchant", "Merchant"]
            )
            random_gold = randrange(200, 500)
            self.inv[_("Gold")] += random_gold
            self.chivalry -= 10
            messagebox.showinfo("", _("You receive {} gold. Your chivalry -10.").format(random_gold))

        self.go_forest()


    def wu_hua_que_forest_encounter(self, stage=0):
        if stage == 0:
            self.stopSoundtrack()
            self.currentBGM = "jy_suspenseful1.mp3"
            self.startSoundtrackThread()
            self.taskProgressDic["ouyang_nana"] = 0
            self.generate_dialogue_sequence(
                None,
                "forest.ppm",
                [_("Yoho~ Where are you going, pretty lady?~"),
                 _("Get away from me, you pervert!"),
                 _("Oh wow, you look even prettier when you are angry~"),
                 _("Come here, and let me calm you down ahahahahaha!"),
                 _("Hey you! I'm feeling a little angry too; you wanna help me calm down?")],
                ["Wu Hua Que", "Ouyang Nana", "Wu Hua Que", "Wu Hua Que", "you"]
            )

            opp = character(_("Wu Hua Que"), 12,
                            sml=[special_move(_("Centipede Poison Powder")),
                                 special_move(_("Basic Punching Technique"), level=10)],
                            skills=[skill(_("Dragonfly Dance"), level=10)],
                            preferences=[1, 1, 4, 1, 4]
                            )
            cmd = lambda: self.wu_hua_que_forest_encounter(1)
            self.battleMenu(self.you, opp, "battleground_forest.ppm",
                                 postBattleSoundtrack="jy_suspenseful1.mp3",
                                 fromWin=None,
                                 battleType="task", destinationWinList=[] * 3,
                                 postBattleCmd=cmd)

        elif stage == 1:
            self.generate_dialogue_sequence(
                None,
                "forest.ppm",
                [_("Wait! Spare me! I was wrong!"),
                 _("I won't do it again! Please, if you spare me I will repay you!")],
                ["Wu Hua Que", "Wu Hua Que"],
                [[_("Get rid of this evil once and for all."), partial(self.wu_hua_que_forest_encounter, 2)],
                 [_("Spare him and see what he has to offer."), partial(self.wu_hua_que_forest_encounter, -1)]]
            )
            
        elif stage == 2:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            self.generate_dialogue_sequence(
                None,
                "forest.ppm",
                [_("Enough! Nothing good can come out of that mouth of yours..."),
                 _("Let me shut it up once and for all."),
                 _("AHHHHHHHHHH!!!"),
                 _("There! Mission accomplished!"),
                 _("Thank you for saving me, young hero!"),
                 _("Glad you're ok, Miss. I'm {}; pleasure to meet you.").format(self.character),
                 _("My name is Ouyang Nana... You can call me Nana..."),
                 _("Ah, then... take care, Nana."),
                 _("You too...")],
                ["you", "you", "Wu Hua Que", "you", "Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana"]
            )
            self.taskProgressDic["wudang_task"] = 2
            self.chivalry += 30
            messagebox.showinfo("", _("Your chivalry + 30!"))
            self.go_forest()
        
        elif stage == -1:
            self.dialogueWin.quit()
            self.dialogueWin.destroy()
            if _("Dragonfly Dance Manual") in self.inv:
                self.generate_dialogue_sequence(
                    None,
                    "forest.ppm",
                    [_("Thank you, young hero!"),
                     _("In return, please take this money. Good bye!")],
                    ["Wu Hua Que", "Wu Hua Que"]
                )
                self.chivalry -= 30
                self.add_item(_("Gold"), 2000)
                messagebox.showinfo("", _("Your chivalry -30!\nYou received Gold x 2000!"))
                self.taskProgressDic['wudang_task'] = 3
                self.generate_dialogue_sequence(
                    None,
                    "forest.ppm",
                    [_("Thank you for saving me, young hero!"),
                     _("Glad you're ok, Miss. I'm {}; pleasure to meet you.").format(self.character),
                     _("My name is Ouyang Nana... You can call me Nana..."),
                     _("Ah, then... take care, Nana."),
                     _("You too...")],
                    ["Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana"]
                )

            else:
                self.generate_dialogue_sequence(
                    None,
                    "forest.ppm",
                    [_("Thank you, young hero!"),
                     _("In return, please take this. Good bye!")],
                    ["Wu Hua Que", "Wu Hua Que"]
                )
                self.chivalry -= 30
                self.add_item(_("Dragonfly Dance Manual"))
                messagebox.showinfo("", _("Your chivalry -30!\nYou received 'Dragonfly Dance Manual' x 1!"))
                self.taskProgressDic['wudang_task'] = 3
                self.generate_dialogue_sequence(
                    None,
                    "forest.ppm",
                    [_("Thank you for saving me, young hero!"),
                     _("Glad you're ok, Miss. I'm {}; pleasure to meet you.").format(self.character),
                     _("My name is Ouyang Nana... You can call me Nana..."),
                     _("Ah, then... take care, Nana."),
                     _("You too...")],
                    ["Ouyang Nana", "you", "Ouyang Nana", "you", "Ouyang Nana"]
                )

            self.go_forest()


    def go_forest(self):

        self.mapWin.withdraw()
        self.forestWin = Toplevel(self.mapWin)
        self.forestWin.title(_("Forest"))

        bg = PhotoImage(file="forest.ppm")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.forestWin, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.forestF1 = Frame(canvas)
        pic1 = PhotoImage(file="Woodsman_icon_large.ppm")
        imageButton1 = Button(self.forestF1, command=self.chop_wood)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.forestF1, text=_("Woodsman"))
        label.grid(row=1, column=0)
        self.forestF1.place(x=w // 4 - pic1.width() // 2, y=h // 4)

        self.forestF2 = Frame(canvas)
        pic2 = PhotoImage(file="forest_explore_icon_large.ppm")
        imageButton2 = Button(self.forestF2, command=partial(self.hunt, self.forestWin))
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(self.forestF2, text=_("Explore"))
        label.grid(row=1, column=0)
        self.forestF2.place(x=w *3// 4 - pic2.width() // 2, y=h // 4)


        menu_frame = self.create_menu_frame(self.forestWin)
        menu_frame.pack()

        self.forestWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.forestWin.update_idletasks()
        toplevel_w, toplevel_h = self.forestWin.winfo_width(), self.forestWin.winfo_height()
        self.forestWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.forestWin.focus_force()
        self.forestWin.mainloop()###


    def chop_wood(self, action="Talk"):

        if action == "Talk":
            self.generate_dialogue_sequence(
                self.forestWin,
                "forest.ppm",
                [_("Hey there, young lad! You look quite strong. Want to chop some wood for me?"),
                 _("I'll pay you some gold in return.")],
                ["Woodsman", "Woodsman"],
                options=[[_("Yes (uses up 50 stamina)"), partial(self.chop_wood, action="Chop")],
                         [_("Not right now"), partial(self.chop_wood, action="Nothing")]]
            )
            return

        elif action == "Chop":
            if self.stamina >= 50:
                self.stamina -= 50
                random_gold = int(randrange(30, 50)*self.strength/50)
                self.inv[_("Gold")] += random_gold
                if self.strength < 75:
                    self.strength += 1
                    messagebox.showinfo("",
                                        _("You receive {} gold.\nYour strength increased by 1.").format(random_gold))
                else:
                    self.staminaMax += 2
                    self.healthMax += 2
                    messagebox.showinfo("", _(
                        "You receive {} gold.\nYour health and stamina upper limits increased by 2.").format(
                        random_gold))

            else:
                messagebox.showinfo("", _("Not enough stamina."))

        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        self.forestWin.deiconify()


    def hunt(self, fromWin):

        if self.stamina < 50:
            messagebox.showinfo("", _("Not enough stamina (Requires 50)."))
            return

        self.stamina -= 50
        self.huntingMultiplier = (self.speed + self.dexterity)/100
        self.huntingFromWin = fromWin
        self.hunting_ended = False
        self.hunting_results = {_("Venomous Spider"):0,
                                _("Snake Heart"):0,
                                _("Venomous Centipede"):0,
                                _("Bear Bile"):0,
                                _("Poisonous Moth1"): 0,
                                _("Poisonous Moth2"): 0,
                                _("Poisonous Moth3"): 0,
                                _("Poisonous Moth4"): 0,
                                _("Poisonous Moth5"): 0,
                                }
        self.huntingFromWin.withdraw()
        self.stopSoundtrack()
        self.currentBGM = "jy_worklist.mp3"
        self.startSoundtrackThread()


        self.off_screen_x, self.off_screen_y = -1000, -1000
        self.huntingWin = Toplevel(self.huntingFromWin)
        bg_img = PhotoImage(file="forest.ppm")

        self.hunting_canvas_width = bg_img.width()
        self.hunting_canvas_height = bg_img.height()

        if os.name == "nt":
            self.hunting_canvas = Canvas(self.huntingWin, width=self.hunting_canvas_width,
                                         height=self.hunting_canvas_height, bg='grey', cursor="hand2")
        else:
            self.hunting_canvas = Canvas(self.huntingWin, width=self.hunting_canvas_width,
                                         height=self.hunting_canvas_height, bg='grey', cursor="hand")

        self.hunting_canvas.pack(side=LEFT)

        self.hunting_instructions_frame = Frame(self.huntingWin, borderwidth = 2, relief = SUNKEN)
        self.hunting_instructions_frame.pack(side=RIGHT, fill=Y)

        Label(self.hunting_instructions_frame, text = _("Click on the animal/insect icons\nas they pop up to capture them.\nTime limit: 60 seconds")).pack()
        #Button(self.hunting_instructions_frame, text="Back", command=partial(self.winSwitch, self.huntingWin, self.forestWin)).pack()



        self.bg_img = self.hunting_canvas.create_image(self.hunting_canvas_width - bg_img.width(),
                                                       self.hunting_canvas_height - bg_img.height(),
                                                       anchor=NW, image=bg_img)

        spider_photo = PhotoImage(file="Venomous Spider_icon_small.ppm")
        snake_photo = PhotoImage(file="Snake_icon_small.ppm")
        centipede_photo = PhotoImage(file="Venomous Centipede_icon_small.ppm")
        bear_photo = PhotoImage(file="Bear_icon_small.ppm")

        spider_count = 6
        snake_count = 4
        centipede_count = 6
        bear_count = 3
        moth_count = 5

        for i in range(spider_count):
            spider_img = self.hunting_canvas.create_image(self.off_screen_x, self.off_screen_y, anchor=NW,
                                                          image=spider_photo)
            self.hunting_canvas.tag_bind(spider_img, "<Button-1>",
                                         partial(self.hunting_clicked_on, _("Venomous Spider"), spider_img))
            t = Thread(target=self.hunting_move_item, args=(spider_img,))
            t.start()

        for i in range(snake_count):
            snake_img = self.hunting_canvas.create_image(self.off_screen_x, self.off_screen_y, anchor=NW,
                                                         image=snake_photo)
            self.hunting_canvas.tag_bind(snake_img, "<Button-1>",
                                         partial(self.hunting_clicked_on, _("Snake Heart"), snake_img))
            t = Thread(target=self.hunting_move_item, args=(snake_img, (10, 21)))
            t.start()

        for i in range(centipede_count):
            centipede_img = self.hunting_canvas.create_image(self.off_screen_x, self.off_screen_y, anchor=NW,
                                                             image=centipede_photo)
            self.hunting_canvas.tag_bind(centipede_img, "<Button-1>",
                                         partial(self.hunting_clicked_on, _("Venomous Centipede"), centipede_img))
            t = Thread(target=self.hunting_move_item, args=(centipede_img,))
            t.start()

        for i in range(bear_count):
            bear_img = self.hunting_canvas.create_image(self.off_screen_x, self.off_screen_y, anchor=NW,
                                                        image=bear_photo)
            self.hunting_canvas.tag_bind(bear_img, "<Button-1>", partial(self.hunting_clicked_on, _("Bear Bile"), bear_img))
            t = Thread(target=self.hunting_move_item, args=(bear_img, (20, 26)))
            t.start()


        moth_photo_list = []
        for i in range(moth_count):
            moth_photo = PhotoImage(file="Poisonous Moth{}_icon_small.ppm".format(i+1))
            moth_photo_list.append(moth_photo)
            moth_img = self.hunting_canvas.create_image(self.off_screen_x, self.off_screen_y, anchor=NW,
                                                        image=moth_photo)
            self.hunting_canvas.tag_bind(moth_img, "<Button-1>", partial(self.hunting_clicked_on, _("Poisonous Moth") + str(i+1), moth_img))
            t = Thread(target=self.hunting_move_item, args=(moth_img, (7, 11)))
            t.start()


        self.huntingWin.after(65000, self.hunting_quit)
        self.huntingWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.huntingWin.update_idletasks()
        toplevel_w, toplevel_h = self.huntingWin.winfo_width(), self.huntingWin.winfo_height()
        self.huntingWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.huntingWin.focus_force()
        self.huntingWin.mainloop()###


    def hunting_quit(self):
        #print(self.hunting_results)
        for hunting_item in self.hunting_results:
            hunting_item_count = self.hunting_results[hunting_item]
            kept_amount = int(self.huntingMultiplier*hunting_item_count/4) + randrange(2)
            if kept_amount > 0:
                if hunting_item in self.inv:
                    self.inv[hunting_item] += kept_amount
                else:
                    self.inv[hunting_item] = kept_amount

        self.hunting_ended = True
        self.huntingWin.quit()
        self.huntingWin.destroy()
        self.huntingFromWin.deiconify()


    def hunting_clicked_on(self, object_name, hunting_canvas_tag, event):

        self.hunting_reset_object(hunting_canvas_tag)
        if _("Spider") in object_name:
            self.currentEffect = pickOne(["insect1.mp3","insect2.mp3"])
            self.startSoundEffectThread()
        elif _("Snake") in object_name:
            self.currentEffect = "snakehiss.mp3"
            self.startSoundEffectThread()
        elif _("Centipede") in object_name:
            self.currentEffect = pickOne(["insect1.mp3","insect2.mp3"])
            self.startSoundEffectThread()
        elif _("Bear") in object_name:
            self.currentEffect = "beargrowl.mp3"
            self.startSoundEffectThread()
        elif _("Moth") in object_name:
            self.currentEffect = pickOne(["insect1.mp3","insect2.mp3"])
            self.startSoundEffectThread()

        self.hunting_results[object_name] += 1


    def hunting_reset_object(self, hunting_canvas_tag):
        try:
            x1, y1, x2, y2 = self.hunting_canvas.bbox(hunting_canvas_tag)
            if x1 != self.off_screen_x and y1 != self.off_screen_y:
                self.hunting_canvas.move(hunting_canvas_tag, -(x1 - self.off_screen_x), -(y1 - self.off_screen_y))
        except:
            pass

    def hunting_move_item(self, hunting_canvas_tag, freq=(5, 11)):

        try:
            time.sleep(randrange(freq[0], freq[1]))

            on_screen_x = randrange(50, self.hunting_canvas_width - 50)
            on_screen_y = randrange(50, self.hunting_canvas_height - 50)

            self.hunting_canvas.move(hunting_canvas_tag, on_screen_x - self.off_screen_x,
                                     on_screen_y - self.off_screen_y)

            time.sleep(.5 + random()*self.huntingMultiplier)
            self.hunting_reset_object(hunting_canvas_tag)
            if not self.hunting_ended:
                self.hunting_move_item(hunting_canvas_tag, freq=freq)

        except:
            pass


    #############################################
    #############################################
    #############MENU FUNCTIONS##################
    #############################################
    #############################################
    def inventory_scroll(self, direction, purpose):
        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        self.selected_item = None
        if direction > 0:
            self.current_item_page = min([self.total_item_pages, self.current_item_page+1])
        else:
            self.current_item_page = max([1, self.current_item_page-1])
        self.populate_inventory(purpose)


    def populate_inventory(self, purpose):
        self.destroy_all(self.invWin_F1)
        self.item_buttons = []
        self.item_icons = []
        self.item_quantity_labels = []
        self.selected_item = None
        self.selected_item_index = None
        item_x = 0
        item_y = 0
        self.starting_item_index = (self.current_item_page - 1) * self.inv_width * self.inv_height
        
        for i in range(self.starting_item_index, self.starting_item_index+self.inv_width * self.inv_height):
            if i < len(self.inv):
                item_name = list(self.inv.keys())[i]
                item_icon = PhotoImage(file=_("i_{}.ppm").format(item_name))
                item_button = Button(self.invWin_F1, image=item_icon,
                                     command=partial(self.selectItem, item_name, i-self.starting_item_index, purpose))
                item_button.image = item_icon
                item_button.grid(row=item_y, column=item_x)

                self.item_icons.append(item_icon)
                self.item_buttons.append(item_button)

                # Add label to show quantity of item owned
                quantity_label = Label(text="x" + str(self.inv[item_name]), master=self.invWin_F1)
                quantity_label.grid(row=item_y + 1, column=item_x)
                self.item_quantity_labels.append(quantity_label)

                # Add balloon to show item description
                if item_name in ITEM_SELLING_PRICES:
                    sell_str = _("\n\nSelling price: ") + str(ITEM_SELLING_PRICES[item_name])
                else:
                    sell_str = ""
                try:
                    description_str = item_name + "\n--------------------------\n" + ITEM_DESCRIPTIONS[
                        item_name] + sell_str
                except:
                    equipment = Equipment(item_name)
                    description_str = equipment.description
                self.balloon.bind(item_button, description_str)

            else:
                item_icon = PhotoImage(file="Blank.ppm")
                item_button = Button(self.invWin_F1, image=item_icon)
                item_button.image = item_icon
                item_button.grid(row=item_y, column=item_x)
                self.item_buttons.append(item_button)

                quantity_label = Label(text=" ", master=self.invWin_F1)
                quantity_label.grid(row=item_y + 1, column=item_x)

            item_x += 1
            if item_x == self.inv_width:
                item_x = 0
                item_y += 2


        Label(self.invWin_F1, text=" ").grid(row=self.inv_height * 2 + 1, column=self.inv_width // 2)
        self.item_page_label = Label(self.invWin_F1,
                                     text="{}/{}".format(self.current_item_page, self.total_item_pages),
                                     relief=SUNKEN)
        self.item_page_label.grid(row=self.inv_height * 2 + 2, column=self.inv_width // 2)
        Button(self.invWin_F1, text="<<<", command=partial(self.inventory_scroll, -1, purpose)).grid(row=self.inv_height * 2 + 2,
                                                                                       column=self.inv_width // 2 - 1)
        Button(self.invWin_F1, text=">>>", command=partial(self.inventory_scroll, 1, purpose)).grid(row=self.inv_height * 2 + 2,
                                                                                      column=self.inv_width // 2 + 1)


    def checkInv(self, fromWin, purpose="normal"):

        if purpose == "battle":
            fromWin.withdraw()

        try:
            if self.invWin.winfo_exists():
                self.invWin.deiconify()

            else:
                raise

        except:
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()

            self.invWin = Toplevel(fromWin)
            self.invWin_F0 = Frame(self.invWin)
            self.invWin_F0.pack()
            self.invWin_F1 = Frame(self.invWin_F0)
            self.invWin_F1.pack(side=LEFT)
            self.invWin_F2 = Frame(self.invWin_F0, borderwidth=3, relief=SOLID)
            self.invWin_F2.pack(side=RIGHT, fill=Y)
            
            self.inv_width = 9
            self.inv_height = 5
            self.current_item_page = 1
            self.total_item_pages = len(self.inv) // (self.inv_width * self.inv_height) + (len(self.inv) % (self.inv_width * self.inv_height) > 0)

            self.populate_inventory(purpose=purpose)            

            #Top frame on the right side
            self.invWin_F2_0 = Frame(self.invWin_F2, padx=20, pady=5)
            self.invWin_F2_0.pack(side=TOP)
            
            #Add equipment slots in self.invWin_F2_0
            Label(self.invWin_F2_0, text="  ").grid(row=0, column=1)
            Label(self.invWin_F2_0, text="  ").grid(row=0, column=3)
            
            self.eq_head_image = PhotoImage(file="eq_head.png")
            self.eq_head_button = Button(self.invWin_F2_0)
            self.eq_head_button.grid(row=1, column=2)
            self.eq_head_button.config(image=self.eq_head_image)

            self.eq_body_image = PhotoImage(file="eq_body.png")
            self.eq_body_button = Button(self.invWin_F2_0)
            self.eq_body_button.grid(row=2, column=2)
            self.eq_body_button.config(image=self.eq_body_image)

            self.eq_hands_right_image = PhotoImage(file="eq_hands_right.png")
            self.eq_hands_right_button = Button(self.invWin_F2_0)
            self.eq_hands_right_button.grid(row=2, column=0)
            self.eq_hands_right_button.config(image=self.eq_hands_right_image)
            
            self.eq_hands_left_image = PhotoImage(file="eq_hands_left.png")
            self.eq_hands_left_button = Button(self.invWin_F2_0)
            self.eq_hands_left_button.grid(row=2, column=4)
            self.eq_hands_left_button.config(image=self.eq_hands_left_image)

            self.eq_legs_image = PhotoImage(file="eq_legs.png")
            self.eq_legs_button = Button(self.invWin_F2_0)
            self.eq_legs_button.grid(row=3, column=2)
            self.eq_legs_button.config(image=self.eq_legs_image)

            self.eq_feet_image = PhotoImage(file="eq_feet.png")
            self.eq_feet_button = Button(self.invWin_F2_0)
            self.eq_feet_button.grid(row=4, column=2)
            self.eq_feet_button.config(image=self.eq_feet_image)

            #update slots with currently equipped items
            self.update_equipment_slots()

            #Bottom frame on the right side
            self.invWin_F2_1 = Frame(self.invWin_F2)
            self.invWin_F2_1.pack(side=BOTTOM)

            if purpose == "normal":
                self.inv_use_button = Button(self.invWin_F2_1, text=_("Use"), font=("TkDefaultFont", 18), command=self.useSelectedItem)
                self.inv_use_button.grid(row=0, column=0)
                Label(self.invWin_F2_1, text="  ", font=("TkDefaultFont", 18)).grid(row=0, column=1)
                self.inv_equip_button = Button(self.invWin_F2_1, text=_("Equip"), font=("TkDefaultFont", 18), command=self.equipSelectedItem)
                self.inv_equip_button.grid(row=0, column=2)
                Label(self.invWin_F2_1, text="  ", font=("TkDefaultFont", 18)).grid(row=0, column=3)
                self.inv_sell_button = Button(self.invWin_F2_1, text=_("Sell"), font=("TkDefaultFont", 18), command=self.sellSelectedItem)
                self.inv_sell_button.grid(row=0, column=4)


            elif purpose == "battle":
                Button(self.invWin_F2_1, text=_("Use"), font=("TkDefaultFont", 18),
                       command=partial(self.useSelectedItem, "battle")).grid(row=0,column=0)
                Label(self.invWin_F2_1, text="  ", font=("TkDefaultFont", 18)).grid(row=0, column=1)
                Button(self.invWin_F2_1, text=_("Back"), font=("TkDefaultFont", 18),
                       command=partial(self.winSwitch, self.invWin, self.battleWin, False)).grid(row=0, column=2)
                self.invWin.protocol("WM_DELETE_WINDOW", self.on_exit)

            self.invWin.update_idletasks()
            toplevel_w, toplevel_h = self.invWin.winfo_width(), self.invWin.winfo_height()
            self.invWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
            self.invWin.focus_force()
            self.invWin.mainloop()###


    def selectItem(self, item_name, item_index, purpose):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.selected_item:
            self.item_icons[self.selected_item_index] = PhotoImage(file=_("i_{}.ppm").format(self.selected_item))
            self.item_buttons[self.selected_item_index].config(image=self.item_icons[self.selected_item_index])

        self.selected_item = item_name
        self.selected_item_index = item_index

        self.item_icons[self.selected_item_index] = PhotoImage(file=_("i_{}_selected.ppm").format(self.selected_item))
        self.item_buttons[self.selected_item_index].config(image=self.item_icons[self.selected_item_index])

        if purpose == "normal":
            #update equip button accordingly
            if self.selected_item in [eq.name for eq in self.equipment]:
                self.inv_equip_button.config(text="Remove")
                self.inv_equip_button.config(state=NORMAL)
            elif self.selected_item not in ITEM_DESCRIPTIONS: #meaning it is an equipment
                self.inv_equip_button.config(text="Equip")
                self.inv_equip_button.config(state=NORMAL)
            else:
                self.inv_equip_button.config(state=DISABLED)

            #update sell button accordingly
            if self.selected_item in ITEM_SELLING_PRICES:
                self.inv_sell_button.config(state=NORMAL)
            else:
                try:
                    equipment = Equipment(self.selected_item)
                    if equipment.selling_price > 0 and self.selected_item not in [eq.name for eq in self.equipment]:
                        self.inv_sell_button.config(state=NORMAL)
                    else:
                        self.inv_sell_button.config(state=DISABLED)
                except:
                    self.inv_sell_button.config(state=DISABLED)


    def useSelectedItem(self, purpose="normal", quantity=1):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.selected_item:
            item_used = True
            if self.inv[self.selected_item] > 0:
                self.add_item(self.selected_item, -quantity)
                if purpose == "battle":
                    item_targ = self.you
                else:
                    item_targ = None
                
                item_obj = Item(self.selected_item, self, item_targ)
                
                #Battle items
                if self.selected_item in self.battle_items:
                    item_obj.use()
                #None battle items
                elif purpose != "battle":
                    if self.selected_item == _("Snake Heart"):
                        self.healthMax += 10
                        print(_("Your health upper limit increased by 10."))
                    elif self.selected_item == _("Bear Bile"):
                        self.staminaMax += 15
                        print(_("Your stamina upper limit increased by 15."))
                    elif self.selected_item == _("Caterpillar Fungus"):
                        self.healthMax += 100
                        self.staminaMax += 100
                        print(_("Your health and stamina upper limits increased by 100."))
                    elif self.selected_item == _("Thousand Year Ginseng Extract"):
                        self.healthMax += 500
                        self.staminaMax += 500
                        print(_("Your health and stamina upper limits increased by 500."))

                    elif self.selected_item == _("Yagyu Family Tree"):
                        im = Image.open("Yagyu Family Tree.png")
                        im.show()
                        item_used = False

                    elif self.selected_item == _("Shaolin Diamond Finger Manual"):
                        if self.perception < 85 or self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_move = special_move(name=_("Shaolin Diamond Finger"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Shaolin Diamond Finger!"))
                            
                    elif self.selected_item == _("Shapeless Palm Manual"):
                        if self.perception < 70 or self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_move = special_move(name=_("Shapeless Palm"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Shapeless Palm!"))
                            
                    elif self.selected_item == _("Prajna Palm Manual"):
                        if self.perception < 85 or self.upgradePoints < 5:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 5
                            new_move = special_move(name=_("Prajna Palm"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Prajna Palm!"))

                    elif self.selected_item == _("Sumeru Palm Manual"):
                        if self.perception < 95 or self.upgradePoints < 5:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 5
                            new_move = special_move(name=_("Sumeru Palm"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Sumeru Palm!"))

                    elif self.selected_item == _("Taichi Fist Manual"):
                        if self.perception < 85 or self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_move = special_move(name=_("Taichi Fist"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Taichi Fist!"))

                    elif self.selected_item == _("Taichi Sword Manual"):
                        if self.perception < 85 or self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_move = special_move(name=_("Taichi Sword"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Taichi Sword!"))
                            
                    elif self.selected_item == _("Eagle Claws Manual"):
                        if self.perception < 75 or self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_move = special_move(name=_("Eagle Claws"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Eagle Claws!"))

                    elif self.selected_item == _("Guan Yin Palm Manual"):
                        if self.perception < 85 or self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_move = special_move(name=_("Guan Yin Palm"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Guan Yin Palm!"))

                    elif self.selected_item == _("Devil Crescent Moon Blade Manual"):
                        if self.upgradePoints < 10 or self.chivalry > -150:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 10
                            new_move = special_move(name=_("Devil Crescent Moon Blade"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Devil Crescent Moon Blade!"))

                    elif self.selected_item == _("Poison Needle Manual"):
                        if self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_move = special_move(name=_("Poison Needle"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Poison Needle!"))

                    elif self.selected_item == _("Empty Force Fist Manual"):
                        if self.perception < 85 or self.upgradePoints < 5:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 5
                            new_move = special_move(name=_("Empty Force Fist"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Empty Force Fist!"))

                    elif self.selected_item == _("Five Poison Palm Manual"):
                        if self.perception < 85 or self.upgradePoints < 5:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 5
                            new_move = special_move(name=_("Five Poison Palm"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Five Poison Palm!"))

                    elif self.selected_item == _("Thousand Swords Manual"):
                        if self.perception < 60 or self.upgradePoints < 2:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 2
                            new_move = special_move(name=_("Thousand Swords Technique"))
                            messagebox.showinfo("", _("You learned a new move: Thousand Swords Technique!"))
                            self.learn_move(new_move)

                    elif self.selected_item == _("Demon Suppressing Blade Manual"):
                        if self.perception < 80 or self.upgradePoints < 4:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 4
                            new_move = special_move(name=_("Demon Suppressing Blade"))
                            messagebox.showinfo("", _("You learned a new move: Demon Suppressing Blade!"))
                            self.learn_move(new_move)

                    elif self.selected_item == _("Dragon Roar Manual"):
                        if self.perception < 85 or self.upgradePoints < 5:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 5
                            new_move = special_move(name=_("Dragon Roar"))
                            messagebox.showinfo("", _("You learned a new move: Dragon Roar!"))
                            self.learn_move(new_move)

                    elif self.selected_item == _("Shadow Blade Manual"):
                        if self.perception < 80 or self.upgradePoints < 5:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 5
                            new_move = special_move(name=_("Shadow Blade"))
                            messagebox.showinfo("", _("You learned a new move: Shadow Blade!"))
                            self.learn_move(new_move)

                    elif self.selected_item == _("Shanhu Fist Manual"):
                        if self.perception < 90 or self.upgradePoints < 6:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 6
                            new_move = special_move(name=_("Shanhu Fist"))
                            messagebox.showinfo("", _("You learned a new move: Shanhu Fist!"))
                            self.learn_move(new_move)

                    elif self.selected_item == _("Violent Dragon Palm Manual"):
                        if self.perception < 90 or self.upgradePoints < 6:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 6
                            new_move = special_move(name=_("Violent Dragon Palm"))
                            messagebox.showinfo("", _("You learned a new move: Violent Dragon Palm!"))
                            self.learn_move(new_move)

                    elif self.selected_item == _("Phantom Steps Manual"):
                        if self.perception < 75 or self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_skill = skill(name=_("Phantom Steps"))
                            messagebox.showinfo("", _("You learned a new skill: Phantom Steps!"))
                            self.learn_skill(new_skill)

                    elif self.selected_item == _("Divine Protection Manual"):
                        if self.perception < 100 or self.upgradePoints < 8:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_skill = skill(name=_("Divine Protection"))
                            messagebox.showinfo("", _("You learned a new skill: Divine Protection!"))
                            self.learn_skill(new_skill)

                    elif self.selected_item == _("Taichi 18 Forms Manual"):
                        if self.perception < 90 or self.upgradePoints < 8:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_skill = skill(name=_("Taichi 18 Forms"))
                            messagebox.showinfo("", _("You learned a new skill: Taichi 18 Forms!"))
                            self.learn_skill(new_skill)

                    elif self.selected_item == _("Dragonfly Dance Manual"):
                        if self.perception < 70 or self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_skill = skill(name=_("Dragonfly Dance"))
                            messagebox.showinfo("", _("You learned a new skill: Dragonfly Dance!"))
                            self.learn_skill(new_skill)
                            
                    elif self.selected_item == _("Huashan Agility Technique Manual"):
                        if self.perception < 75 or self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_skill = skill(name=_("Huashan Agility Technique"))
                            messagebox.showinfo("", _("You learned a new skill: Huashan Agility Technique!"))
                            self.learn_skill(new_skill)

                    elif self.selected_item == _("Shaolin Inner Energy Manual"):
                        if self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_skill = skill(name=_("Shaolin Inner Energy Technique"))
                            messagebox.showinfo("", _("You learned a new skill: Shaolin Inner Energy Technique!"))
                            self.learn_skill(new_skill)

                    elif self.selected_item == _("Shaolin Mind Clearing Manual"):
                        if self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_skill = skill(name=_("Shaolin Mind Clearing Method"))
                            messagebox.showinfo("", _("You learned a new skill: Shaolin Mind Clearing Method!"))
                            self.learn_skill(new_skill)

                    elif self.selected_item == _("Tendon Changing Manual"):
                        if self.perception < 90 or self.upgradePoints < 8:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 8
                            new_skill = skill(name=_("Tendon Changing Technique"))
                            messagebox.showinfo("", _("You learned a new skill: Tendon Changing Technique!"))
                            self.learn_skill(new_skill)

                    elif self.selected_item == _("Marrow Cleansing Manual"):
                        if self.perception < 100 or self.upgradePoints < 8:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 8
                            new_skill = skill(name=_("Marrow Cleansing Technique"))
                            messagebox.showinfo("", _("You learned a new skill: Marrow Cleansing Technique!"))
                            self.learn_skill(new_skill)

                    elif self.selected_item == _("Shaolin Luohan Fist Manual"):
                        if self.upgradePoints < 3:
                            messagebox.showinfo("", _("Requirements not met."))
                            item_used = False
                        else:
                            self.upgradePoints -= 3
                            new_move = special_move(name=_("Shaolin Luohan Fist"))
                            self.learn_move(new_move)
                            messagebox.showinfo("", _("You learned a new move: Shaolin Luohan Fist Technique!"))

                    elif self.selected_item == _("Hundred Seeds Pill"):
                        r = randrange(6)
                        if r == 0:
                            self.perception += 3
                            messagebox.showinfo("", "Your perception increased by 3!")
                        elif r == 1:
                            self.attack += 3
                            messagebox.showinfo("", "Your attack increased by 3!")
                        elif r == 2:
                            self.strength += 3
                            messagebox.showinfo("", "Your strength increased by 3!")
                        elif r == 3:
                            self.speed += 3
                            messagebox.showinfo("", "Your speed increased by 3!")
                        elif r == 4:
                            self.defence += 3
                            messagebox.showinfo("", "Your defence increased by 3!")
                        elif r == 5:
                            self.dexterity += 3
                            messagebox.showinfo("", "Your dexterity increased by 3!")

                    elif self.selected_item == _("Sack"):
                        if self.taskProgressDic["li_guiping_task"] == 5:
                            response = messagebox.askquestion("", "Open sack to see what's inside?")
                            if response == "yes":
                                self.taskProgressDic["li_guiping_task"] = 50
                                for i in range(1,6):
                                    self.add_item(_("Poisonous Moth{}").format(i), randrange(15,21))
                                messagebox.showinfo("", _("You open the sack and find some poisonous moths. You decide to keep some of them for your own use."))
                            else:
                                item_used = False
                        else:
                            item_used = False
                            
                    elif self.selected_item == _("Yin Yang Soul Absorption Manual"):
                        if self.taskProgressDic["castrated"] == 1:
                            new_skill = skill(name=_("Yin Yang Soul Absorption Technique"))
                            messagebox.showinfo("", _("You learned a new skill: Yin Yang Soul Absorption Technique!"))
                            self.learn_skill(new_skill)
                        else:
                            response = messagebox.askquestion("", "Undergo self-castration to learn this technique?")
                            if response == "yes":
                                self.stopSoundtrack()
                                self.generate_dialogue_sequence(
                                    None,
                                    "scrub_house.ppm",
                                    [_("I can't believe you are willing to go this far..."),
                                     _("You don't understand, Scrub. I'll do anything to become the strongest..."),
                                     _("You're weak because you don't have the same type of mindset as me."),
                                     _("Ok, whatever you say... I'll leave you to it then. You can rest here for a bit once you're done..."),
                                     _("Bye..."),
                                     _("..."),
                                     _("Ok, now that he's gone, I can begin..."),
                                     _("I just need to endure a little bit of pain... It'll be over quick..."),
                                     _("Here...I...go..."),
                                     ],
                                    ["Scrub","you","you","Scrub","Scrub","you","you","you","you"]
                                )

                                self.currentEffect = "thunder_sound_FX-Grant_Evans.mp3"
                                self.startSoundEffectThread()
                                self.currentBGM = "Kindaichi 10.mp3"
                                self.startSoundtrackThread()
                                self.generate_dialogue_sequence(
                                    None,
                                    "scrub_house.ppm",
                                    [_("AHHHHHHHHHHHHHHH!!!!!!!!!!"),
                                     _("AGGHHHHHHHHH!!! AHHHHHHHHHHHHHHHHHHHH!! ...."),
                                     _("Ha..... ha..... ah..... I... I did it... I can finally...learn..."),
                                     _("This terrifyingly powerful technique....."),
                                     _("No one... will be able to stop me..."),
                                     _("NO ONE!!! AHAHAHAHAHAHAHAHA!!!"),
                                     _("ALL WHO STAND IN MY WAY SHALL DIEEEEEEEE!!!!"),
                                     ],
                                    ["you", "you", "you", "you", "you", "you", "you"]
                                )
                                if self.taskProgressDic["wife_intimacy"] >= 50:
                                    self.taskProgressDic["wife_intimacy"] = 10
                                self.taskProgressDic["castrated"] = 1
                                new_skill = skill(name=_("Yin Yang Soul Absorption Technique"))
                                messagebox.showinfo("", _("You learned a new skill: Yin Yang Soul Absorption Technique!"))
                                self.learn_skill(new_skill)

                            else:
                                item_used = False

                    else:
                        messagebox.showinfo("", _("Cannot use item."))
                        item_used = False

                else:
                    messagebox.showinfo("", _("Cannot use item."))
                    item_used = False

                try:
                    self.updateProfileLabels()
                except:
                    pass

                if item_used:
                    self.item_quantity_labels[self.selected_item_index].config(
                        text="x" + str(self.inv[self.selected_item]))

                    if self.selected_item in [_("Sack")]: #do this for items that generate more items when used
                        self.populate_inventory(purpose="normal")

                    if purpose == "battle":
                        self.youActionPoints += self.you.speed // 3
                        self.youActionPoints -= self.opp.speed
                        self.youHaveMoved = True
                        self.updateLabels()
                        self.invWin.withdraw()
                        self.battleWin.deiconify()
                else:
                    self.add_item(self.selected_item, quantity)
                    
            else:
                messagebox.showinfo("", _("You do not have any more of the selected item."))
        else:
            messagebox.showinfo("", _("No item selected."))


    def equipSelectedItem(self):

        if self.selected_item:
            if self.inv[self.selected_item] > 0:
                self.currentEffect = "button-19.mp3"
                self.startSoundEffectThread()
                equipped_items = [eq.name for eq in self.equipment]
                if self.selected_item in equipped_items:
                    self.inv_equip_button.config(text="Equip")
                    self.inv_sell_button.config(state=NORMAL)
                    for eq in self.equipment:
                        if eq.name == self.selected_item:
                            self.remove_equipment(eq)
                else:
                    selected_equipment = Equipment(self.selected_item)
                    for eq in self.equipment:
                        if eq.slot == selected_equipment.slot:
                            self.remove_equipment(eq)
                    self.equipment.append(selected_equipment)
                    self.inv_equip_button.config(text="Remove")
                    self.inv_sell_button.config(state=DISABLED)

                self.update_equipment_slots()
            else:
                messagebox.showinfo("", _("You do not have any more of the selected item."))
        else:
            messagebox.showinfo("", _("No item selected."))


    def remove_equipment(self, eq):
        self.equipment_slot_image_button_dic = {
            "head": [self.eq_head_image, self.eq_head_button],
            "body": [self.eq_body_image, self.eq_body_button],
            "hands_left": [self.eq_hands_left_image, self.eq_hands_left_button],
            "hands_right": [self.eq_hands_right_image, self.eq_hands_right_button],
            "legs": [self.eq_legs_image, self.eq_legs_button],
            "feet": [self.eq_feet_image, self.eq_feet_button]
        }
        self.equipment_slot_images = []
        equipment_image, equipment_button = self.equipment_slot_image_button_dic[eq.slot]
        equipment_image = PhotoImage(file=_("eq_{}.png").format(eq.slot))
        equipment_button.config(image=equipment_image)
        self.equipment_slot_images.append(equipment_image)
        self.equipment.remove(eq)

        
    def update_equipment_slots(self):
        self.equipment_slot_image_button_dic = {
            "head": [self.eq_head_image, self.eq_head_button],
            "body": [self.eq_body_image, self.eq_body_button],
            "hands_left": [self.eq_hands_left_image, self.eq_hands_left_button],
            "hands_right": [self.eq_hands_right_image, self.eq_hands_right_button],
            "legs": [self.eq_legs_image, self.eq_legs_button],
            "feet": [self.eq_feet_image, self.eq_feet_button]
        }
        
        self.equipment_images = []
        for eq in self.equipment:
            equipment_image, equipment_button = self.equipment_slot_image_button_dic[eq.slot]
            equipment_image = PhotoImage(file=_("i_{}.ppm").format(eq.name))
            equipment_button.config(image=equipment_image)
            self.equipment_images.append(equipment_image)
            self.balloon.bind(equipment_button, eq.description)


    def sellSelectedItem(self, quantity=1):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.selected_item:
            if self.inv[self.selected_item] > 0:
                if self.selected_item in ITEM_SELLING_PRICES and self.selected_item not in [eq.name for eq in self.equipment]:
                    sell_price = ITEM_SELLING_PRICES[self.selected_item]
                    self.add_item(self.selected_item, -quantity)
                    self.add_item(_("Gold"), sell_price)
                    self.item_quantity_labels[self.selected_item_index].config(text="x" + str(self.inv[self.selected_item]))
                    if self.current_item_page == 1:
                        self.item_quantity_labels[0].config(text="x" + str(self.inv[_("Gold")]))
                    print(_("Sold {} for {} gold.").format(self.selected_item, sell_price))
                else:
                    try:
                        equipment = Equipment(self.selected_item)
                        sell_price = equipment.selling_price
                        if sell_price <= 0:
                            raise Exception("Non-tradeable equipment")
                        self.add_item(self.selected_item, -quantity)
                        self.add_item(_("Gold"), sell_price)
                        self.item_quantity_labels[self.selected_item_index].config(text="x" + str(self.inv[self.selected_item]))
                        self.item_quantity_labels[0].config(text="x" + str(self.inv[_("Gold")]))
                        print(_("Sold {} for {} gold.").format(self.selected_item, sell_price))
                    except:
                        messagebox.showinfo("", _("Cannot sell item."))
            else:
                messagebox.showinfo("", _("You do not have any more of the selected item."))
        else:
            messagebox.showinfo("", _("No item selected."))


    def settings(self, fromWin):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        fromWin.withdraw()

        self.settingsWin = Toplevel(fromWin)
        self.settingsWin.title(_("Settings"))
        F1 = Frame(self.settingsWin, borderwidth=2, relief=SOLID)
        F1.pack()
        F2 = Frame(self.settingsWin)
        F2.pack()

        Checkbutton(master=F1, text=_("Music"), variable=self.soundtrackOn, onvalue=1, offvalue=0).pack(anchor=W)
        Checkbutton(master=F1, text=_("Sound Effects"), variable=self.soundEffectOn, onvalue=1, offvalue=0).pack(
            anchor=W)
        Checkbutton(master=F1, text=_("Quick Save"), variable=self.quickSave, onvalue=1, offvalue=0).pack(
            anchor=W)

        B1 = Button(F2, text=_("Back"), command=partial(self.winSwitch, self.settingsWin, fromWin))
        B1.grid(row=0, column=0)
        B2 = Button(F2, text=_("Confirm"), command=self.confirm_settings)
        B2.grid(row=0, column=1)

        self.settingsWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.settingsWin.update_idletasks()
        toplevel_w, toplevel_h = self.settingsWin.winfo_width(), self.settingsWin.winfo_height()
        self.settingsWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.settingsWin.focus_force()
        self.settingsWin.mainloop()###
        
    def confirm_settings(self):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if not self.soundtrackOn.get():
            self.stopSoundtrack()

        pickle.dump(self.quickSave.get(), open(os.path.join(SAVE_SLOTS_DIR, "quicksave.p"), "wb"))
        messagebox.showinfo("", _("Settings saved."))

    def useUpgradePoints(self, reason=None, quantity=1):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        self.stat_limit = 200 + 50*self.ptc

        if self.upgradePoints < 1:
            messagebox.showinfo("", _("No upgrade points left."))
            return

        if reason == "Attack" and self.attack < self.stat_limit:
            self.attack += 1
        elif reason == "Strength" and self.strength < self.stat_limit:
            self.strength += 1
        elif reason == "Speed" and self.speed < self.stat_limit:
            self.speed += 1
        elif reason == "Defence" and self.defence < self.stat_limit:
            self.defence += 1
        elif reason == "Dexterity" and self.dexterity < self.stat_limit:
            self.dexterity += 1
        elif reason == "Health":
            self.healthMax += 10
        elif reason == "Stamina":
            self.staminaMax += 10
        elif not reason:
            pass
        else:
            messagebox.showinfo("", _("Cannot increase stat beyond {}.").format(self.stat_limit))
            return

        self.upgradePoints -= quantity
        self.updateProfileLabels()


    def cheatCode(self):

        try:
            admin = pickle.load(open("admin.p", "rb"))
            if admin != "19318212":
                print(_("Option only available to admins..."))
                return
        except:
            print(_("Option only available to admins..."))
            return

        userInput = input("Enter cheat code and press enter:")

        if "coinconfig " in userInput:
            self.inv[_("Gold")] = int(userInput.split("coinconfig ")[1])

        elif "upgradeconfig " in userInput:
            self.upgradePoints = int(userInput.split("upgradeconfig ")[1])

        elif "perceptionconfig " in userInput:
            self.perception = int(userInput.split("perceptionconfig ")[1])

        elif "luckconfig " in userInput:
            self.luck = int(userInput.split("luckconfig ")[1])

        elif "chivalryconfig " in userInput:
            self.chivalry = int(userInput.split("chivalryconfig ")[1])

        elif "gainexp " in userInput:
            self.gainExp(int(userInput.split("gainexp ")[1]))
            
        elif "spawnitem " in userInput:
            if " x " in userInput:
                amount = int(userInput.split(" x ")[1])
            else:
                amount = 1
            self.add_item(userInput.split("spawnitem ")[1].split(" x ")[0], amount)

        elif userInput == "gimmefood":
            for food_item in [_("Soup"), _("Dumplings"), _("Crab"), _("Fish"), _("Wine")]:
                self.add_item(food_item, 5)

        elif "learn " in userInput:
            if "level " in userInput:
                move_level = int(userInput.split("level ")[1])
            else:
                move_level = 1
            move_name = userInput.split("learn ")[1].split("level ")[0].strip()
            new_move = special_move(name=move_name, level=move_level)
            self.learn_move(new_move)
            print(_("Learned"), new_move.name)
            
        elif "learnskill " in userInput:
            skill_name = userInput.split("learnskill ")[1].strip()
            new_skill = skill(name=skill_name)
            self.learn_skill(new_skill)
            print(_("Learned"), new_skill.name)

        elif userInput == "unlockAll":
            existing_move_names = [m.name for m in self.sml]
            for move_name in self.animation_frame_delta_dic:
                if move_name not in existing_move_names + UNLEARNABLE_MOVES:
                    try:
                        new_move = special_move(name=move_name)
                        self.learn_move(new_move)
                        print(_("Learned"), new_move.name)
                    except:
                        pass
                    
        elif userInput == "unlockAllSkills":
            existing_skill_names = [s.name for s in self.skills]
            for skill_name in SKILL_NAMES:
                if skill_name not in existing_skill_names:
                    try:
                        new_skill = skill(name=skill_name)
                        self.learn_skill(new_skill)
                        print(_("Learned"), new_skill.name)
                    except:
                        pass

        elif "learnskill " in userInput:
            skill_name = userInput.split("learnskill ")[1].strip()
            new_skill = skill(name=skill_name)
            self.learn_skill(new_skill)
            print(_("Learned"), new_skill.name)

        elif userInput == "forgetmove":
            self.sml.pop(len(self.sml) - 4)
            
        elif userInput == "forgetskill":
            self.skills.pop(len(self.skills) - 1)
            
        elif userInput == "clearskills":
            self.skills = []

        elif "setstatus " in userInput:
            self.status = userInput.split("setstatus ")[1]

        elif "setstats " in userInput:
            self.attack = int(userInput.split("setstats ")[1])
            self.strength = int(userInput.split("setstats ")[1])
            self.speed = int(userInput.split("setstats ")[1])
            self.defence = int(userInput.split("setstats ")[1])
            self.dexterity = int(userInput.split("setstats ")[1])
            
        elif userInput == "maxstats":
            self.attack = 200
            self.strength = 200
            self.speed = 200
            self.defence = 200
            self.dexterity = 200

        elif userInput == "minstats":
            self.attack = 50
            self.strength = 50
            self.speed = 50
            self.defence = 50
            self.dexterity = 50


    def beta_testing_fix(self):
        defaultDic = {
            "join_sect": -1,
            "xiao_han_jia_wen": -1,
            "liu_village": -1,
            "imperial_palace": -1,
            "feng_xinyi": -1,
            "li_guiying": -1,
            "wu_hua_que": 0,
            "wudang_task": 0,
            "huashanlunjian": -1,
            "guo_zhiqiang": -1,
            "chivalry": 0,
            "ouyang_nana": -1,
            "ouyang_nana_xu_jun": -1,
            "return_to_phantom_sect": 0,
            "eagle_sect": -1,
            "wife_intimacy": -1,
            "luohanzhen": -1,
            "shinscrub": -1,
            "li_guiping_task": -1,
            "huang_yuwei": -1,
            "huang_yuwei_birthday": -1,
            "nongyuan_valley": -1,
            "babao_tower": -1,
            "potters_field": -1,
            "feng_pan": -1,
            "ma_guobao": -1,
            "small_town": -1,
            "small_town_ghost": -1,
            "yagyu_clan": -1,
            "yagyu_clan_join": -1,
            "yagyu_dungeon": 1,
            "blind_sniper": -1,
            "supreme_leader": -1,
            "huashan_tribute": -1,
            "wudang_tribute": -1,
            "shaolin_tribute": -1,
            "shanhu_sect_tribute": -1,
            "dragon_sect_tribute": -1,
            "phantom_sect_tribute": -1,
            "castrated": 0,
            "wanhua_pavillion": -1,
            "cai_wuqiong": -1,
            "a_xiu": - 1,
            "jiang_yuqi_liu_village_choice": -1,
            "big_beard": -1,
            "scrub_tutorial": -1,
        }

        # Scrub Tutorial Fix
        if self.level > 1:
            defaultDic["scrub_tutorial"] = 1


        for task in defaultDic:
            if task not in self.taskProgressDic:
                self.taskProgressDic[task] = defaultDic[task]

        if self.taskProgressDic["liu_village"] == 1001 and self.taskProgressDic["potters_field"] < 0:
            self.taskProgressDic["potters_field"] = 0


        #Equipment fix
        try:
            pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "equipment-{}.p".format(self.saveSlot)), "rb"))

        except:
            self.equipment = []
            pickle.dump(self.equipment, open(os.path.join(SAVE_SLOTS_DIR, "equipment-{}.p".format(self.saveSlot)), "wb"))

        #Move fix
        self.sml = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "sml-{}.p".format(self.saveSlot)), "rb"))
        for move in self.sml:
            if move.name == _("Taichi Fist") and len(move.animation_file_list) == 27:
                current_level = move.level
                current_exp = move.exp
                self.sml.remove(move)
                new_move = special_move(_("Taichi Fist"), level=current_level)
                new_move.exp = current_exp
                self.sml.insert(len(self.sml) - 3, new_move)
                print("Fixed Taichi Fist animation list.")

        pickle.dump(self.sml, open(os.path.join(SAVE_SLOTS_DIR, "sml-{}.p".format(self.saveSlot)), "wb"))

        #checked moves fix
        try:
            self.sml_checked = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "sml_checked-{}.p".format(self.saveSlot)), "rb"))
        except:
            self.sml_checked = []
            for i in range(min([10, len(self.sml)])):
                move = self.sml[i]
                if move.name not in [_("Rest"),_("Items"), _("Flee")]:
                    self.sml_checked.append(move.name)
            pickle.dump(self.sml_checked, open(os.path.join(SAVE_SLOTS_DIR, "sml_checked-{}.p".format(self.saveSlot)), "wb"))
        
        #Castration fix
        self.skills = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "skills-{}.p".format(self.saveSlot)), "rb"))
        if _("Yin Yang Soul Absorption Technique") in [s.name for s in self.skills]:
            self.taskProgressDic["castrated"] = 1

        #Game Date fix
        for i in range(1,7):
            try:
                pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameDate-{}.p".format(i)), "rb"))
            except:
                gameDate = randrange(100,300)
                pickle.dump(gameDate, open(os.path.join(SAVE_SLOTS_DIR, "gameDate-{}.p".format(i)), "wb"))
                if self.saveSlot == i:
                    self.gameDate = gameDate


    def updateProfileLabels(self):
        self.profile_L0.config(text=_("Name: {}").format(self.character))
        self.profile_L1.config(text=_("Level: {}").format(int(self.level)))
        self.profile_L2.config(text=_("Experience: {}/{}").format(int(self.exp), self.levelUpThreshold))
        self.profile_L3.config(text=_("Upgrade Points: {}").format(int(self.upgradePoints)))
        self.profile_L4.config(text=_("Status: {}").format(self.status))

        self.profile_L6.config(text=_("Health: {}/{}").format(int(self.health), int(self.healthMax)))
        self.profile_L7.config(text=_("Stamina: {}/{}").format(int(self.stamina), int(self.staminaMax)))
        self.profile_L8.config(text=_("Attack: {}").format(int(self.attack)))
        self.profile_L9.config(text=_("Strength: {}").format(int(self.strength)))
        self.profile_L10.config(text=_("Speed: {}").format(int(self.speed)))
        self.profile_L11.config(text=_("Defence: {}").format(int(self.defence)))
        self.profile_L12.config(text=_("Dexterity: {}").format(int(self.dexterity)))
        self.profile_L13.config(text=_("Perception: {}").format(int(self.perception)))
        self.profile_L14.config(text=_("Luck: {}").format(int(self.luck)))
        self.profile_L15.config(text=_("Chivalry: {}").format(int(self.chivalry)))


    def checkProfile(self, fromWin):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        try:
            if self.profileWin.winfo_exists():
                return
            else:
                raise

        except:

            self.profileWin = Toplevel(fromWin)

            self.stat_frame = Frame(self.profileWin)
            self.stat_frame.pack(side=LEFT)

            self.skill_move_frame = Frame(self.profileWin, borderwidth = 3, relief=SOLID)
            self.skill_move_frame.pack(side=RIGHT, expand=Y, fill=Y)


            F0 = Frame(self.stat_frame)
            F0.pack(side=TOP)

            F0_0 = Frame(F0)
            F0_0.grid(row=0, column=0)

            self.photo1 = PhotoImage(file="you.ppm", master=self.profileWin)
            pic1 = Label(F0_0, image=self.photo1)
            pic1.pack(side=LEFT)

            F0_1 = Frame(F0)
            F0_1.grid(row=0, column=1)
            F1 = Frame(F0_1)
            F1.pack()
            self.levelUpThreshold = self.levelThresholds[self.level - 1]

            self.profile_L0 = Label(F1, text=_("Name: {}").format(self.character))
            self.profile_L1 = Label(F1, text=_("Level: {}").format(int(self.level)))
            self.profile_L2 = Label(F1, text=_("Experience: {}/{}").format(int(self.exp), self.levelUpThreshold))
            self.profile_L3 = Label(F1, text=_("Upgrade Points: {}").format(int(self.upgradePoints)))
            self.profile_L4 = Label(F1, text=_("Status: {}").format(self.status))

            self.profile_L0.grid(row=0, column=0, sticky=W)
            self.profile_L1.grid(row=1, column=0, sticky=W)
            self.profile_L2.grid(row=2, column=0, sticky=W)
            self.profile_L3.grid(row=3, column=0, sticky=W)
            self.profile_L4.grid(row=4, column=0, sticky=W)

            F0_2 = Frame(F0)
            F0_2.grid(row=0, column=2)
            # Label(F0_2, text="\t").pack(side=TOP)
            # Label(F0_2, text="\t").pack(side=TOP)
            # inventory_icon = PhotoImage(file="inventory_icon.ppm")
            # imageButton1 = Button(F0_2, command=partial(self.checkInv,self.profileWin))
            # imageButton1.pack(side=BOTTOM)
            # imageButton1.config(image=inventory_icon)

            F2 = Frame(self.stat_frame, borderwidth=3, relief=SOLID)
            F2.pack()
            #self.balloon.bind(F2, _("Click on the green + buttons to use upgrade points.\nEach upgrade point increases health/stamina\nupper limit by 10 or each attribute by 1."))

            self.profile_L6 = Label(F2, text=_("Health: {}/{}").format(int(self.health), int(self.healthMax)))
            self.profile_L7 = Label(F2, text=_("Stamina: {}/{}").format(int(self.stamina), int(self.staminaMax)))
            self.profile_L8 = Label(F2, text=_("Attack: {}").format(int(self.attack)))
            self.profile_L9 = Label(F2, text=_("Strength: {}").format(int(self.strength)))
            self.profile_L10 = Label(F2, text=_("Speed: {}").format(int(self.speed)))
            self.profile_L11 = Label(F2, text=_("Defence: {}").format(int(self.defence)))
            self.profile_L12 = Label(F2, text=_("Dexterity: {}").format(int(self.dexterity)))

            self.profile_L6.grid(row=0, column=0, sticky=W)
            self.profile_L7.grid(row=1, column=0, sticky=W)
            self.profile_L8.grid(row=2, column=0, sticky=W)
            self.profile_L9.grid(row=3, column=0, sticky=W)
            self.profile_L10.grid(row=4, column=0, sticky=W)
            self.profile_L11.grid(row=5, column=0, sticky=W)
            self.profile_L12.grid(row=6, column=0, sticky=W)

            self.balloon.bind(self.profile_L6, _("When this reaches 0, you lose the battle.\n\nClick the green + button to increase by 10.\nUses 1 upgrade point."))
            self.balloon.bind(self.profile_L7, _("Stamina is needed to perform attacks. The higher your stamina upper\nlimit, the more defence and damage bonus you receive in battle.\n\nClick the green + button to increase by 10.\nUses 1 upgrade point."))
            self.balloon.bind(self.profile_L8, _("Determines accuracy of your attacks.\n\nClick the green + button to increase by 1.\nUses 1 upgrade point."))
            self.balloon.bind(self.profile_L9, _("Determines your damage multiplier.\n\nClick the green + button to increase by 1.\nUses 1 upgrade point."))
            self.balloon.bind(self.profile_L10, _("Determines how often you can execute a move for each time an opponent does a move.\nAffects max jump height in mini-games.\n\nClick the green + button to increase by 1.\nUses 1 upgrade point."))
            self.balloon.bind(self.profile_L11, _("Determines how much damage you take from opponent's attacks.\n\nClick the green + button to increase by 1.\nUses 1 upgrade point."))
            self.balloon.bind(self.profile_L12, _("Determines chance of you dodging an attack.\nAffects max jump height in mini-games.\n\nClick the green + button to increase by 1.\nUses 1 upgrade point."))

            add_icon = PhotoImage(file="add_button_small.ppm")
            Button(F2, command=partial(self.useUpgradePoints, "Health"), image=add_icon).grid(row=0, column=1, sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Stamina"), image=add_icon).grid(row=1, column=1,
                                                                                               sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Attack"), image=add_icon).grid(row=2, column=1, sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Strength"), image=add_icon).grid(row=3, column=1,
                                                                                                sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Speed"), image=add_icon).grid(row=4, column=1, sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Defence"), image=add_icon).grid(row=5, column=1,
                                                                                               sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Dexterity"), image=add_icon).grid(row=6, column=1,
                                                                                                 sticky=W)

            Label(F2, text="  ").grid(row=0, column=2)

            self.profile_L13 = Label(F2, text=_("Perception: {}").format(int(self.perception)))
            self.profile_L14 = Label(F2, text=_("Luck: {}").format(int(self.luck)))
            self.profile_L15 = Label(F2, text=_("Chivalry: {}").format(int(self.chivalry)))
            self.profile_L13.grid(row=0, column=3, sticky=W)
            self.profile_L14.grid(row=1, column=3, sticky=W)
            self.profile_L15.grid(row=2, column=3, sticky=W)
            self.balloon.bind(self.profile_L13, _("Determines how many upgrade points you earn\nwhen you level up as well as exp you gain\neach time you perform an attack."))
            self.balloon.bind(self.profile_L14, _("Having higher luck increases your chances of unlocking hidden plot\nas well as the rewards you receive from completing quests."))
            self.balloon.bind(self.profile_L15, _("Your chivalry is affected by the decisions that you make\nand influences plot development."))

            # B1 = Button(self.profileWin, text=_("Back"), command=partial(self.winSwitch, self.profileWin, fromWin))
            # B1.pack(side=BOTTOM)

            self.skill_move_image_list = []
            self.move_image_labels = []
            Label(self.skill_move_frame, text=_("Attacks/Moves (click to select/unselect up to 10 to use in battle)")).pack()
            moves_frame = Frame(self.skill_move_frame, borderwidth = 1, relief=SUNKEN)
            moves_frame.pack()

            if len(self.sml + self.skills) >= 72:
                frame_width = 11
            elif len(self.sml + self.skills) >= 60:
                frame_width = 10
            elif len(self.sml + self.skills) >= 48:
                frame_width = 9
            else:
                frame_width = 8

            frame_x = 0
            frame_y = 0

            for i in range(len(self.sml)-3):
                move = self.sml[i]
                if move.name in self.sml_checked:
                    checkmark_image = Image.open("green_checkmark.png")
                    move_im = Image.open(move.file_name)
                    move_im.paste(checkmark_image, (0,0), checkmark_image)
                    move_image = ImageTk.PhotoImage(move_im)
                else:
                    move_image = PhotoImage(file=move.file_name)
                move_image_label = Button(moves_frame, image=move_image, command=partial(self.check_uncheck_move, i))
                move_image_label.grid(row=frame_y, column=frame_x)
                move_image_label.config(image=move_image)
                self.skill_move_image_list.append(move_image)
                self.move_image_labels.append(move_image_label)
                self.balloon.bind(move_image_label, move.description)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1




            Label(self.skill_move_frame, text=_("Inner Energy/Agility Techniques")).pack()
            skills_frame = Frame(self.skill_move_frame)
            skills_frame.pack()

            if len(self.sml + self.skills) >= 72:
                frame_width = 11
            elif len(self.sml + self.skills) >= 60:
                frame_width = 10
            elif len(self.sml + self.skills) >= 48:
                frame_width = 9
            else:
                frame_width = 8
            frame_x = 0
            frame_y = 0

            self.profile_skill_labels = []
            for i in range(len(self.skills)):
                s_frame = Frame(skills_frame, borderwidth = 1, relief=SUNKEN)
                s_frame.grid(row=frame_y, column=frame_x)

                skill = self.skills[i]
                skill_image = PhotoImage(file=skill.file_name)
                skill_image_label = Button(s_frame, image=skill_image)
                skill_image_label.pack()
                skill_image_label.config(image=skill_image)
                self.skill_move_image_list.append(skill_image)
                upgrade_skill_button = Button(s_frame, text="Upgrade", command=partial(self.upgradeSkill, skill))
                upgrade_skill_button.pack()
                self.balloon.bind(skill_image_label, skill.description)
                self.balloon.bind(upgrade_skill_button, _("Spend 1 upgrade point to upgrade this skill."))
                self.profile_skill_labels.append(skill_image_label)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1

            self.profileWin.update_idletasks()
            toplevel_w, toplevel_h = self.profileWin.winfo_width(), self.profileWin.winfo_height()
            self.profileWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
            self.profileWin.focus_force()
            self.profileWin.mainloop()###


    def upgradeSkill(self,skill):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if skill.level >= 10:
            messagebox.showinfo("", _("Cannot upgrade skill beyond level 10."))
        elif self.upgradePoints < 1:
            messagebox.showinfo("", _("Not enough upgrade points."))
        else:
            self.useUpgradePoints()
            skill.upgrade_skill(1)
            messagebox.showinfo("", _("Upgraded '{}' to level {}.".format(skill.name, skill.level)))
            self.balloon.bind(self.profile_skill_labels[self.skills.index(skill)], skill.description)

            if skill.name == _("Shaolin Inner Energy Technique"):
                self.staminaMax += skill.level*15
            elif skill.name == _("Tendon Changing Technique"):
                self.staminaMax += skill.level*20
                self.healthMax += skill.level*20
            elif skill.name == _("Marrow Cleansing Technique"):
                self.staminaMax += 150
                self.healthMax += 50
            elif skill.name == _("Yin Yang Soul Absorption Technique"):
                self.staminaMax += 200
                self.healthMax += 200
            elif skill.name == _("Divine Protection"):
                self.staminaMax += 150
                self.healthMax += 150
            elif skill.name == _("Bushido"):
                self.staminaMax += 100
                self.healthMax += 100
            elif skill.name == _("Ninjutsu III"):
                self.staminaMax += 100
                self.healthMax += 100
            elif skill.name == _("Taichi 18 Forms"):
                self.staminaMax += 200
            elif skill.name == _("Leaping Over Mountain Peaks"):
                self.speed += 1
                self.dexterity += 1
            elif skill.name == _("Hun Yuan Yin Qi"):
                self.staminaMax += 300
            

            try:
                self.updateProfileLabels()
            except:
                pass

    
    def check_uncheck_move(self, move_index):
        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        
        selected_move = self.sml[move_index]
        if selected_move.name in self.sml_checked: #remove
            if len(self.sml_checked) == 1:
                messagebox.showinfo("", _("Must have at least 1 move selected for battle."))
            else:
                self.sml_checked.remove(selected_move.name)
                move_image = PhotoImage(file=selected_move.file_name)
                self.move_image_labels[move_index].config(image=move_image)
                self.skill_move_image_list.append(move_image)

        else: #add
            if len(self.sml_checked) < 10:
                self.sml_checked.append(selected_move.name)
                checkmark_image = Image.open("green_checkmark.png")
                move_im = Image.open(selected_move.file_name)
                move_im.paste(checkmark_image, (0, 0), checkmark_image)
                move_image = ImageTk.PhotoImage(move_im)
                self.move_image_labels[move_index].config(image=move_image)
                self.skill_move_image_list.append(move_image)

            else:
                messagebox.showinfo("", _("Cannot select more than 10 moves to use in battle. Please deselect another move first."))



    def create_self_character(self):
        move_list = self.sml.copy()
        for move in self.sml:
            if move.name not in self.sml_checked + [_("Rest"), _("Items"), _("Flee")]:
                move_list.remove(move)
                
        self.you = character(self.character, self.level, [self.health, self.healthMax,
                                                          self.stamina, self.staminaMax, self.attack,
                                                          self.strength, self.speed, self.defence,
                                                          self.dexterity], move_list, self.skills, [], self.equipment,
                             status=self.status)


    def generate_dialogue_sequence(self, fromWin, bg, dialogue_list, speaker_list, options=None,
                                   bind_text=None):

        self.dialogue_speaker = speaker_list[0]
        self.dialogue_list = dialogue_list
        self.speaker_list = speaker_list
        self.dialogue_length = len(self.dialogue_list)
        self.dialogue_count = 1  # we are displaying 0th index at beginning
        self.dialogueFromWin = fromWin
        self.dialogue_options = options
        if self.dialogueFromWin:
            try:
                self.dialogueFromWin.withdraw()
            except:
                pass

        self.dialogueWin = Toplevel(self.mainWin)

        dialogue_bg = PhotoImage(file=bg)
        canvas = Canvas(self.dialogueWin, width=dialogue_bg.width() - 1, height=dialogue_bg.height() - 1, bg='white')
        canvas.create_image(0, 0, anchor=NW, image=dialogue_bg)
        canvas.pack()
        self.dialogue_display_frame = Frame(self.dialogueWin)
        self.dialogue_display_frame.pack()
        self.dialogue_options_frame = Frame(self.dialogueWin)
        self.dialogue_options_frame.pack()
        Button(self.dialogue_options_frame, text=_("Skip Dialogue"), command=self.skip_dialogue).pack()
    
        try:
            self.speaker_avatar_image = PhotoImage(file="{}_icon_large.ppm".format(self.dialogue_speaker))
        except:
            self.speaker_avatar_image = PhotoImage(file="{}_icon.ppm".format(self.dialogue_speaker))
        self.speaker_avatar_label = Label(self.dialogue_display_frame, image=self.speaker_avatar_image)
        self.speaker_avatar_label.grid(row=0, column=0)
        Label(self.dialogue_display_frame, text="  ").grid(row=0, column=1)
        if os.name == "nt":
            self.dialogue_label = Label(self.dialogue_display_frame, text=self.dialogue_list[0], wraplength=270,
                                        width=30, height=6, relief=SUNKEN, borderwidth=2)
        else:
            self.dialogue_label = Label(self.dialogue_display_frame, text=self.dialogue_list[0], wraplength=350,
                                        width=30, height=4, relief=SUNKEN, borderwidth=2, font=("TkDefaultFont", 18))
        self.dialogue_label.grid(row=0, column=2)

        self.dialogueWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.dialogueWin.bind("<Button-1>", self.advance_dialogue)
        if bind_text:
            self.balloon.bind(self.dialogueWin, bind_text)
        self.dialogueWin.update_idletasks()
        toplevel_w, toplevel_h = self.dialogueWin.winfo_width(), self.dialogueWin.winfo_height()
        self.dialogueWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.dialogueWin.focus_force()
        self.dialogueWin.mainloop()###
        

    def advance_dialogue(self, event):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        try:
            self.speaker_avatar_image = PhotoImage(file="{}_icon_large.ppm".format(self.dialogue_speaker))
        except:
            self.speaker_avatar_image = PhotoImage(file="{}_icon.ppm".format(self.dialogue_speaker))

        if self.dialogue_count < self.dialogue_length:
            self.dialogue_speaker = self.speaker_list[self.dialogue_count]
            try:
                self.speaker_avatar_image = PhotoImage(file="{}_icon_large.ppm".format(self.dialogue_speaker))
            except:
                self.speaker_avatar_image = PhotoImage(file="{}_icon.ppm".format(self.dialogue_speaker))
            self.speaker_avatar_label.config(image=self.speaker_avatar_image)
            self.dialogue_label.config(text=self.dialogue_list[self.dialogue_count])

        elif self.dialogue_count == self.dialogue_length:
            if self.dialogue_options:
                self.destroy_all(self.dialogue_options_frame)
                self.dialogue_display_frame.pack_forget()
                for option in self.dialogue_options:
                    Button(self.dialogue_options_frame, text=option[0], command=option[1]).pack()
            else:
                self.dialogueWin.quit()
                self.dialogueWin.destroy()
                if self.dialogueFromWin:
                    self.dialogueFromWin.deiconify()

        self.dialogue_count += 1


    def skip_dialogue(self):
        self.dialogue_count = self.dialogue_length
        self.advance_dialogue(None)



        
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    ##     BATTLE    BATTLE    BATTLE    BATTLE    BATTLE    BATTLE    BATTLE    BATTLE  ##
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################

    def battleMenu(self, you, opp_list, bg_file_name, postBattleSoundtrack, fromWin, battleType="normal",
                   destinationWinList=None, postBattleCmd=None):

        time.sleep(1)

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        self.postBattleSoundtrack = postBattleSoundtrack
        self.battleType = battleType
        self.playAsOther = False
        self.autoBattle = False
        
        if self.battleType in ["training"]:
            self.currentBGM = pickOne(["fangshiyu.mp3", "huangfeihong.mp3", "chushitaiji.mp3", "jy_battle2.mp3", "jy_battle5.mp3"])
            followup = pickOne(["fangshiyu.mp3", "huangfeihong.mp3", "chushitaiji.mp3", "jy_battle2.mp3", "jy_battle5.mp3"])
            if self.currentBGM == "fangshiyu.mp3":
                followupDelay = 152
            elif self.currentBGM == "huangfeihong.mp3":
                followupDelay = 177
            elif self.currentBGM == "chushitaiji.mp3":
                followupDelay = 98
            elif self.currentBGM == "jy_battle2.mp3":
                followupDelay = 114
            elif self.currentBGM == "jy_battle5.mp3":
                followupDelay = 114

        elif self.battleType in ["normal"]:
            self.currentBGM = pickOne(["jy_battle2.mp3", "jy_battle5.mp3"])
            followup = pickOne(["jy_battle2.mp3", "jy_battle5.mp3"])
            if self.currentBGM == "jy_battle2.mp3":
                followupDelay = 114
            elif self.currentBGM == "jy_battle5.mp3":
                followupDelay = 114
                
        elif self.battleType in ["test"]:
            self.currentBGM = pickOne(["xianjianqiyuan.mp3"])
            followup = pickOne(["xianjianqiyuan.mp3"])
            if self.currentBGM == "xianjianqiyuan.mp3":
                followupDelay = 156

        elif self.battleType in ["task"]:
            self.currentBGM = pickOne(["jy_battle1.mp3", "jy_battle3.mp3", "jy_battle4.mp3"])
            followup = pickOne(["jy_battle1.mp3", "jy_battle3.mp3", "jy_battle4.mp3"])
            if self.currentBGM == "jy_battle1.mp3":
                followupDelay = 87
            elif self.currentBGM == "jy_battle3.mp3":
                followupDelay = 75
            elif self.currentBGM == "jy_battle4.mp3":
                followupDelay = 108

        elif self.battleType in ["huashanlunjian"]:
            self.currentBGM = "jy_huashanlunjian.mp3"
            followup = "jy_huashanlunjian.mp3"
            followupDelay = 178

        elif self.battleType in ["potters_field"]:
            self.currentBGM = "Kindaichi 5.mp3"
            followup = "Kindaichi 5.mp3"
            followupDelay = 116

        elif self.battleType in ["luohanzhen"]:
            self.currentBGM = None
            followup = "luohanzhen.mp3"
            followupDelay = 116

        if self.currentBGM and self.battleID not in ["hattori_ninjas", "ashikaga_yamamoto", "yagyu_hattori_battle"]:
            self.stopSoundtrack()
            self.startSoundtrackThread(followup=followup, followupDelay=followupDelay)

        self.battleFromWin = fromWin
        if self.battleFromWin:
            self.battleFromWin.withdraw()
        if not destinationWinList:
            self.destinationWinList = [None, None, None]
        else:
            self.destinationWinList = destinationWinList
        self.postBattleCmd = postBattleCmd

        if type(opp_list) != list:
            self.opp_list = [opp_list]
        else:
            self.opp_list = opp_list
        self.opp_list.reverse()
        self.opp = self.opp_list.pop()
        #self.opp = copy(opp)

        self.battleWin = Toplevel(self.battleFromWin)
        self.battle_map_image = PhotoImage(file=bg_file_name)
        self.canvas_width = self.battle_map_image.width()
        self.canvas_height = self.battle_map_image.height()
        self.canvas = Canvas(self.battleWin, width=self.canvas_width - 1, height=self.canvas_height - 1, bg='white')
        self.canvas.create_image(0, 0, anchor=NW, image=self.battle_map_image)

        # update exp gain rate
        for move in self.sml:
            if move.name == _("Yunhuan Taichi Fist"):
                move.exp_rate = int(30 * (self.perception / 50))
            else:
                move.exp_rate = int(10 * (self.perception / 50))

        self.animation_target = 1
        self.animation_x = -300
        self.animation_y = -300
        self.animation_in_progress = False

        self.you_animation_index = 0
        self.opp_animation_index = 0
        self.you_animation_list = []
        self.you_animation_img_list = []
        self.opp_animation_list = []
        self.opp_animation_img_list = []
        self.animation_buttons = []
        self.animation_buttons_img_list = []


        self.move_image_button_frame = Frame(self.battleWin)
        self.move_image_button_frame.pack(side=BOTTOM)


        
        #if len(self.sml)+3 >= 28:
        #    button_frame_width = (len(self.sml)+3)//4+1
        #else:
        #    button_frame_width = 7
        button_frame_width = 7
        button_frame_x = 0
        button_frame_y = 0

        if self.battleID in ["zhao_huilin_revenge", "zhao_huilin_guo_junda_battle", "li_guiping_task",
                             "shaolin_yagyu_spar", "shaolin_yagyu_tournament", "ouyang_nana_jiangnan_goblins"]:
            self.playAsOther = True
        if self.playAsOther:
            self.you = you
        else:
            self.create_self_character()

        for i in range(len(self.you.sml)):
            move = self.you.sml[i]
            move_image = PhotoImage(file=move.file_name)
            move_image_button = Button(self.move_image_button_frame, command=partial(self.move_buffer_zone, move))
            move_image_button.grid(row=button_frame_y, column=button_frame_x)
            move_image_button.config(image=move_image)
            self.animation_buttons_img_list.append(move_image)
            self.animation_buttons.append(move_image_button)
            self.balloon.bind(move_image_button, move.description)
            button_frame_x += 1
            if button_frame_x >= button_frame_width:
                button_frame_x = 0
                button_frame_y += 1


        self.statusTooltip = _("Your status is normal.")
        self.oppStatusTooltip = _("Opponent's status is normal.")
        
        if not self.playAsOther:
            self.set_battle_scene(True, False)
        else:
            self.youAcupunctureCount = 0
            self.youBlindCount = 0
            self.youWeaponDisabledCount = 0
            self.youDeathCount = 0
            self.youConsecutiveHits = 0
            self.set_battle_scene(False, False)
            

        if not self.playAsOther:
            Label(self.move_image_button_frame, text="").grid(row=100, column=0)

            self.auto_battle_image = PhotoImage(file="sprite_computer.png")
            self.auto_battle_image_button = Button(self.move_image_button_frame, command=self.toggle_auto_battle)
            self.auto_battle_image_button.grid(row=101, column=3)
            self.auto_battle_image_button.config(image=self.auto_battle_image)

            self.auto_battle_label = Label(self.move_image_button_frame, text=_("Manual"))
            self.auto_battle_label.grid(row=102, column=3)

            self.auto_battle_tooltip = _(
                "Click to toggle Automatic Move Selection 'On'. The AI will pick your move for you.\nYou can turn this off at any time during the battle to return to manual move selection.")
            self.balloon.bind(self.auto_battle_image_button, self.auto_battle_tooltip)

        self.battleWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.battleWin.update_idletasks()
        toplevel_w, toplevel_h = self.battleWin.winfo_width(), self.battleWin.winfo_height()
        self.battleWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.battleWin.focus_force()
        self.battleWin.mainloop()###


    def toggle_auto_battle(self):
        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        if self.autoBattle:
            self.auto_battle_image = PhotoImage(file="sprite_computer.png")
            self.auto_battle_image_button.config(image=self.auto_battle_image)
            self.auto_battle_label.config(text=_("Manual"))
            self.auto_battle_tooltip = _("Click to toggle Automatic Move Selection 'On'. The AI will pick your move for you.\nYou can turn this off at any time during the battle to return to manual move selection.")
            self.balloon.bind(self.auto_battle_image_button, self.auto_battle_tooltip)
            self.autoBattle = False
        else:
            self.auto_battle_image = PhotoImage(file="sprite_computer2.png")
            self.auto_battle_image_button.config(image=self.auto_battle_image)
            self.auto_battle_label.config(text=_("Auto"))
            self.auto_battle_tooltip = _("Click to toggle Automatic Move Selection 'Off' and resume manual move selection.")
            self.balloon.bind(self.auto_battle_image_button, self.auto_battle_tooltip)
            self.autoBattle = True
    
            self.allOff(self.animation_buttons)
            self.battleWin.after(1500, self.auto_select_player_move)

            
            

    def set_battle_scene(self, create_self=False, ignore_self_bonus=True):
        #create_self flag is used to prevent re-creating self.you object during multi-opponent battles
        #ignore_self_bonus flag is used to prevent re-applying skill/equipment bonus during multi-opponent battles
        if create_self:
            self.create_self_character()
            self.youAcupunctureCount = 0
            self.youBlindCount = 0
            self.youWeaponDisabledCount = 0
            self.youDeathCount = 0
            self.youConsecutiveHits = 0

        self.turnEnded = False
        self.battleEnded = False

        self.youHaveMoved = False
        self.oppHasMoved = False
        self.youActionPoints = self.you.speed
        self.oppActionPoints = self.opp.speed

        self.oppVengeance = False
        self.vengeance = False
        self.oppAcupunctureCount = 0
        self.oppBlindCount = 0
        self.oppDeathCount = 0
        self.oppConsecutiveHits = 0


        ############################
        self.you_armed_equipment_bonus = 0
        self.you_unarmed_equipment_bonus = 0
        self.you_poison_damage_multiplier = 1
        #self.you_poison_bonus = 0
        #self.you_internal_injury_bonus = 0
        #self.you_seal_bonus = 0
        #self.you_blind_bonus = 0
        self.you_critical_hit_bonus = 0
        self.you_status_bonus = 0
        if self.you.health_recovery <= 0.05:
            self.you.health_recovery = 0.05
        if self.you.stamina_recovery <= .2:
            self.you.stamina_recovery = 0.2
        self.opp_armed_equipment_bonus = 0
        self.opp_unarmed_equipment_bonus = 0
        self.opp_poison_damage_multiplier = 1
        #self.opp_poison_bonus = 0
        #self.opp_internal_injury_bonus = 0
        #self.opp_seal_bonus = 0
        #self.opp_blind_bonus = 0
        self.opp_critical_hit_bonus = 0
        self.opp_status_bonus = 0
        # apply any bonuses from skills
        if not ignore_self_bonus:
            self.apply_skill_bonus(self.you)
        self.apply_skill_bonus(self.opp)

        # apply any bonuses from equipment
        self.hasWeaponEquipped = False
        self.apply_equipment_bonus(self.you, ignore_stats=ignore_self_bonus)
        self.apply_equipment_bonus(self.opp)

        ###################

        self.accuracy = (self.you.attack - self.opp.dexterity + 80) / 100
        self.damage_scale = (self.you.strength ** .85 / self.opp.defence ** .85) * 1.075 ** (self.you.staminaMax / 500) * 1.05 ** (-self.opp.staminaMax / 500)

        self.oppaccuracy = (self.opp.attack - self.you.dexterity + 80) / 100
        self.oppdamage_scale = (self.opp.strength ** .85 / self.you.defence ** .85) * 1.075 ** (self.opp.staminaMax / 500) * 1.05 ** (-self.you.staminaMax / 500)

        ###################

        if self.accuracy <= .3:
            self.accuracy = .3

        if self.damage_scale <= .2:
            self.damage_scale = .2

        if self.oppaccuracy <= .3:
            self.oppaccuracy = .3

        if self.oppdamage_scale <= .2:
            self.oppdamage_scale = .2

        if self.youBlindCount > 0:
            self.accuracy *= .5

        if self.oppBlindCount > 0:
            self.oppaccuracy *= .5


        #print(self.accuracy, self.damage_scale)
        #print(self.oppaccuracy, self.oppdamage_scale)

        self.callAfterStatChange()

        if self.battleID == "yagyu_hattori_battle":
            self.you.status = _("Bleeding")
            if self.you.health < self.you.healthMax:
                self.you.health = int(self.you.health*.75)
            else:
                self.you.health = int(self.you.healthMax*.75)

            if self.you.stamina < self.you.staminaMax:
                self.you.stamina = int(self.you.stamina*.75)
            else:
                self.you.stamina = int(self.you.staminaMax*.75)


        try:
            self.destroy_all(self.canvas)
        except:
            pass

        self.you_x, self.you_y = 50, 50
        if self.you.name == self.character:
            self.you_img = PhotoImage(file="you_icon.ppm")
        else:
            self.you_img = PhotoImage(file="{}_icon.ppm".format(self.you.name))
        self.you_canvas_img = self.canvas.create_image(self.you_x, self.you_y, anchor=NW, image=self.you_img)
        self.you_avatar_frame_img = PhotoImage(file="avatar_frame.png")
        self.you_avatar_frame = self.canvas.create_image(self.you_x, self.you_y, anchor=NW, image=self.you_avatar_frame_img)

        self.you_hp_mp_frame_x, self.you_hp_mp_frame_y = self.you_x + self.you_img.width(), self.you_y
        you_frame_img = Image.open("bar_hp_mp.png").convert("RGBA")
        self.you_frame_img = ImageTk.PhotoImage(you_frame_img)
        self.you_frame_height = self.you_frame_img.height()
        self.you_canvas_hp_mp_frame = self.canvas.create_image(self.you_hp_mp_frame_x, self.you_hp_mp_frame_y,
                                                               anchor=NW, image=self.you_frame_img)

        you_hp_img = Image.open("bar_hp.png").convert("RGBA")
        self.you_hp_img = ImageTk.PhotoImage(you_hp_img)
        self.you_canvas_hp_img = self.canvas.create_image(self.you_hp_mp_frame_x, self.you_hp_mp_frame_y,
                                                          anchor=NW, image=self.you_hp_img)

        you_mp_img = Image.open("bar_mp.png").convert("RGBA")
        self.you_mp_img = ImageTk.PhotoImage(you_mp_img)
        self.you_canvas_mp_img = self.canvas.create_image(self.you_hp_mp_frame_x,
                                                          self.you_hp_mp_frame_y + self.you_frame_height // 2,
                                                          anchor=NW, image=self.you_mp_img)


        self.opp_hp_mp_frame_x = self.canvas_width - self.you_hp_mp_frame_x - you_frame_img.width
        self.opp_hp_mp_frame_y = self.you_hp_mp_frame_y

        self.opp_x, self.opp_y = self.battle_map_image.width() - self.you_x - self.you_img.width(), self.you_y
        self.opp_img = PhotoImage(file=_("{}_icon.ppm").format(self.opp.name))
        self.opp_canvas_img = self.canvas.create_image(self.opp_x, self.opp_y, anchor=NW, image=self.opp_img)
        self.opp_avatar_frame_img = PhotoImage(file="avatar_frame.png")
        self.opp_avatar_frame = self.canvas.create_image(self.opp_x, self.opp_y, anchor=NW, image=self.opp_avatar_frame_img)

        opp_frame_img = Image.open("bar_hp_mp.png").convert("RGBA")
        self.opp_frame_img = ImageTk.PhotoImage(opp_frame_img)
        self.opp_frame_height = self.opp_frame_img.height()
        self.opp_canvas_hp_mp_frame = self.canvas.create_image(self.opp_hp_mp_frame_x, self.opp_hp_mp_frame_y,
                                                               anchor=NW, image=self.opp_frame_img)

        opp_hp_img = Image.open("bar_hp.png").convert("RGBA")
        self.opp_hp_img = ImageTk.PhotoImage(opp_hp_img)
        self.opp_canvas_hp_img = self.canvas.create_image(self.opp_hp_mp_frame_x, self.opp_hp_mp_frame_y,
                                                          anchor=NW, image=self.opp_hp_img)

        opp_mp_img = Image.open("bar_mp.png").convert("RGBA")
        self.opp_mp_img = ImageTk.PhotoImage(opp_mp_img)
        self.opp_canvas_mp_img = self.canvas.create_image(self.opp_hp_mp_frame_x,
                                                          self.opp_hp_mp_frame_y + self.opp_frame_height // 2,
                                                          anchor=NW, image=self.opp_mp_img)


        if not self.playAsOther:
            self.canvas.tag_bind(self.you_avatar_frame,"<Button-1>",self.battleCheckProfile)
            self.canvas.tag_bind(self.opp_avatar_frame,"<Button-1>",self.oppBattleCheckProfile)
            self.balloon.bind(self.canvas, "Click on your/opponent's picture to view detailed battle-related stats.")

        F1 = Frame(self.canvas, borderwidth=2, relief=SUNKEN, width=30)
        F1.pack()
        F2 = Frame(self.canvas, borderwidth=2, relief=SUNKEN, width=30)
        F2.pack()
        self.nameLabel1 = Label(F1, text=_("{} (Lv. {})").format(self.you.name, self.you.level))
        self.nameLabel1.grid(row=0, column=0, sticky=W)
        self.hptext = _("Health: {}/{}").format(int(self.you.health), int(self.you.healthMax))
        self.staminatext = _("Stamina: {}/{}").format(int(self.you.stamina), int(self.you.staminaMax))
        self.statustext = _("Status: {}").format(self.you.status)

        self.hplabel = Label(F1, text=self.hptext)
        self.hplabel.grid(row=2, column=0, sticky=W)
        self.staminalabel = Label(F1, text=self.staminatext)
        self.staminalabel.grid(row=3, column=0, sticky=W)
        self.statuslabel = Label(F1, text=self.statustext)
        self.statuslabel.grid(row=4, column=0, sticky=W)
        self.balloon.bind(self.statuslabel, self.statusTooltip)

        #############################################################################
        if self.opp.name == _("Mysterious"):
            self.nameLabel2 = Label(F2, text=_("{} (Lv. {})").format(_("Ren Zhichu"), self.opp.level))
        else:
            if len(self.opp.name) >= 15:
                name_text = self.opp.name.replace(" & ", "&\n").replace(" ", "\n")
            else:
                name_text = self.opp.name
            self.nameLabel2 = Label(F2, text=_("{} (Lv. {})").format(name_text, self.opp.level))
        self.nameLabel2.grid(row=0, column=0, sticky=W)
        self.opphptext = _("Health: {}/{}").format(int(self.opp.health), int(self.opp.healthMax))
        self.oppstaminatext = _("Stamina: {}/{}").format(int(self.opp.stamina), int(self.opp.staminaMax))
        self.oppstatustext = _("Status: {}").format(self.opp.status)

        self.opphplabel = Label(F2, text=self.opphptext)
        self.opphplabel.grid(row=2, column=0, sticky=W)
        self.oppstaminalabel = Label(F2, text=self.oppstaminatext)
        self.oppstaminalabel.grid(row=3, column=0, sticky=W)
        self.oppstatuslabel = Label(F2, text=self.oppstatustext)
        self.oppstatuslabel.grid(row=4, column=0, sticky=W)
        self.balloon.bind(self.oppstatuslabel, self.oppStatusTooltip)

        F1.place(x=self.you_x, y=self.you_y + 80)
        F2.place(x=self.opp_x - self.you_x - self.opp_img.width()//2, y=self.opp_y + 80)

        self.canvas.pack(side=TOP)
        self.update_bars()
        self.allOn(self.animation_buttons)



    def apply_equipment_bonus(self, targ_obj, ignore_stats=False):
        for eq in targ_obj.equipmentList:
            if not ignore_stats:
                targ_obj.attack += eq.datk
                targ_obj.strength += eq.dstr
                targ_obj.speed += eq.dspd
                targ_obj.defence += eq.ddef
                targ_obj.dexterity += eq.ddex
            if targ_obj == self.you:
                self.you_armed_equipment_bonus += eq.armed_dmg_bonus
                self.you_unarmed_equipment_bonus += eq.unarmed_dmg_bonus
                #apply damage bonus from skills
                skills = targ_obj.skills
                for skill in skills:
                    if skill.name == _("Kenjutsu I"):
                        self.you_armed_equipment_bonus += round(skill.level*.02,3)
                    elif skill.name == _("Kenjutsu II"):
                        self.you_armed_equipment_bonus += round(skill.level*.025,3)
                if eq.effect:
                    #if eq.effect == _("Poison"):
                    #    self.you_poison_bonus += eq.effect_prob
                    #elif eq.effect == _("Internal Injury"):
                    #    self.you_internal_injury_bonus += eq.effect_prob
                    #elif eq.effect == _("Seal"):
                    #    self.you_seal_bonus += eq.effect_prob
                    #elif eq.effect == _("Blind"):
                    #    self.you_blind_bonus += eq.effect_prob
                    if eq.effect == _("Critical Hit"):
                        self.you_critical_hit_bonus += eq.effect_prob
                    elif eq.effect == _("Status"):
                        self.you_status_bonus += eq.effect_prob
                    elif eq.effect == _("Health"):
                        self.you.health_recovery += eq.effect_prob
                    elif eq.effect == _("Stamina"):
                        self.you.stamina_recovery += eq.effect_prob

            else:
                self.opp_armed_equipment_bonus += eq.armed_dmg_bonus
                self.opp_unarmed_equipment_bonus += eq.unarmed_dmg_bonus
                #apply damage bonus from skills
                skills = targ_obj.skills
                for skill in skills:
                    if skill.name == _("Kenjutsu I"):
                        self.opp_armed_equipment_bonus += round(skill.level*.02,3)
                    elif skill.name == _("Kenjutsu II"):
                        self.opp_armed_equipment_bonus += round(skill.level*.025,3)
                if eq.effect:
                    #if eq.effect == _("Poison"):
                    #    self.opp_poison_bonus += eq.effect_prob
                    #elif eq.effect == _("Internal Injury"):
                    #    self.opp_internal_injury_bonus += eq.effect_prob
                    #elif eq.effect == _("Seal"):
                    #    self.opp_seal_bonus += eq.effect_prob
                    #elif eq.effect == _("Blind"):
                    #    self.opp_blind_bonus += eq.effect_prob
                    if eq.effect == _("Critical Hit"):
                        self.opp_critical_hit_bonus += eq.effect_prob
                    elif eq.effect == _("Status"):
                        self.opp_status_bonus += eq.effect_prob
                    elif eq.effect == _("Health"):
                        self.opp.health_recovery += eq.effect_prob
                    elif eq.effect == _("Stamina"):
                        self.opp.stamina_recovery += eq.effect_prob

            if eq.slot == "hands_right":
                self.hasWeaponEquipped = True


    def apply_skill_bonus(self, targ_obj):
        skills = targ_obj.skills
        for skill in skills:
            skill_level = skill.level
            if skill.name == _("Cherry Blossoms Floating on the Water"):
                targ_obj.speed += 2 * skill_level
                targ_obj.dexterity += skill_level
            elif skill.name == _("Pure Yang Qi Skill"):
                targ_obj.defence += sum([i//3 + 1 for i in range(1, skill_level+1)])
            elif skill.name == _("Huashan Agility Technique"):
                targ_obj.speed += sum([i//5 + 1 for i in range(1, skill_level+1)])
                targ_obj.dexterity += skill_level
            elif skill.name == _("Wudang Agility Technique"):
                targ_obj.speed += skill_level
                targ_obj.dexterity += skill_level*2
            elif skill.name in  [_("Phantom Steps"), _("Shaolin Agility Technique")]:
                targ_obj.speed += int(1.5*skill_level)
                targ_obj.dexterity += int(1.5*skill_level)
            elif skill.name == _("Dragonfly Dance"):
                targ_obj.dexterity += skill_level*2
            elif skill.name == _("Shaolin Inner Energy Technique"):
                targ_obj.defence += skill_level*3
            elif skill.name == _("Divine Protection"):
                targ_obj.defence += skill_level*5
            elif skill.name == _("Recuperation"):
                targ_obj.health_recovery += skill.level/100
            elif skill.name == _("Burst of Potential"):
                targ_obj.attack += skill_level*3
                targ_obj.strength += skill_level*3
            elif skill.name == _("Basic Agility Technique"):
                targ_obj.dexterity += int(skill_level*1.5)
            elif skill.name == _("Yin Yang Soul Absorption Technique"):
                targ_obj.attack += skill_level*10
                targ_obj.strength += skill_level*10
                targ_obj.speed += skill_level*10
            elif skill.name == _("Firm Resolve"):
                if targ_obj.name != self.character and not (self.playAsOther and targ_obj.name == self.you.name):
                    self.opp_status_bonus += .15
                else:
                    self.you_status_bonus += .15
            elif skill.name == _("Shaolin Mind Clearing Method"):
                if targ_obj.name != self.character and not (self.playAsOther and targ_obj.name == self.you.name):
                    self.opp_status_bonus += skill.level*.02
                else:
                    self.you_status_bonus += skill.level * .02
            elif skill.name == _("Flower and Body Unite"):
                targ_obj.attack += skill_level*2
                targ_obj.strength += skill_level*2
                targ_obj.speed += skill_level*2
                targ_obj.defence += skill_level * 2
                targ_obj.dexterity += skill_level * 2
            elif skill.name == _("Marrow Cleansing Technique"):
                if targ_obj.name != self.character and not (self.playAsOther and targ_obj.name == self.you.name):
                    self.opp_status_bonus += .5
                else:
                    self.you_status_bonus += .5
            elif skill.name == _("Huashan Sword Qi Gong"):
                armed_count = len([m.name for m in targ_obj.sml if m.type == _("Armed")])
                targ_obj.strength += armed_count * skill_level
            elif skill.name == _("Splitting Mountains and Earth") and not (self.playAsOther and targ_obj.name == self.you.name):
                targ_obj.strength += 3*skill_level
                if targ_obj.name != self.character:
                    self.opp_critical_hit_bonus += .03*skill_level
                else:
                    self.you_critical_hit_bonus += .03*skill_level
            elif skill.name == _("Toad Form"):
                if targ_obj.name != self.character:
                    self.opp_poison_damage_multiplier += .03*skill_level
                else:
                    self.you_poison_damage_multiplier += .03*skill_level
            elif skill.name == _("Kenjutsu I"):
                targ_obj.attack += skill_level*2
            elif skill.name == _("Kenjutsu II"):
                targ_obj.strength += skill_level*3
            elif skill.name == _("Bushido"):
                targ_obj.strength += skill_level*4
            elif skill.name == _("Ninjutsu I"):
                targ_obj.attack += skill_level
                targ_obj.speed += skill_level
                targ_obj.dexterity += skill_level
            elif skill.name == _("Ninjutsu II"):
                targ_obj.attack += skill_level*3
                targ_obj.speed += skill_level*3
                targ_obj.dexterity += skill_level*3
            elif skill.name == _("Ninjutsu III"):
                if targ_obj.name != self.character:
                    self.opp_critical_hit_bonus += .05*skill_level
                else:
                    self.you_critical_hit_bonus += .05*skill_level
            elif skill.name == _("Hun Yuan Yin Qi"):
                targ_obj.defence += 50

            if targ_obj == self.opp:
                if skill.name == _("Yin Yang Soul Absorption Technique"):
                    targ_obj.health += skill_level * 200
                    targ_obj.healthMax += skill_level * 200
                    targ_obj.stamina += skill_level * 200
                    targ_obj.staminaMax += skill_level * 200
                elif skill.name == _("Marrow Cleansing Technique"):
                    targ_obj.health += skill_level * 50
                    targ_obj.healthMax += skill_level * 50
                    targ_obj.stamina += skill_level * 150
                    targ_obj.staminaMax += skill_level * 150
                elif skill.name == _("Tendon Changing Technique"):
                    self.opp.healthMax += sum([i*20 for i in range(1,skill.level+1)])
                    self.opp.staminaMax += sum([i*20 for i in range(1,skill.level+1)])
                    self.opp.health = self.opp.healthMax
                    self.opp.stamina = self.opp.staminaMax
                elif skill.name == _("Shaolin Inner Energy Technique"):
                    self.opp.staminaMax += sum([i*15 for i in range(1,skill.level+1)])
                    self.opp.stamina = self.opp.staminaMax
                elif skill.name == _("Taichi 18 Forms"):
                    self.opp.staminaMax += skill_level*200
                    self.opp.stamina = self.opp.staminaMax
                elif skill.name == _("Leaping Over Mountain Peaks"):
                    self.opp.dexterity += 3
                    self.opp.speed += 3
                    self.opp.dexterity += skill_level - 1
                    self.opp.speed += skill_level - 1
                elif skill.name == _("Divine Protection"):
                    targ_obj.health += skill_level * 150
                    targ_obj.healthMax += skill_level * 150
                    targ_obj.stamina += skill_level * 150
                    targ_obj.staminaMax += skill_level * 150
                elif skill.name == _("Bushido"):
                    targ_obj.health += skill_level * 100
                    targ_obj.healthMax += skill_level * 100
                    targ_obj.stamina += skill_level * 100
                    targ_obj.staminaMax += skill_level * 100
                elif skill.name == _("Ninjutsu III"):
                    targ_obj.health += skill_level * 100
                    targ_obj.healthMax += skill_level * 100
                    targ_obj.stamina += skill_level * 100
                    targ_obj.staminaMax += skill_level * 100
                elif skill.name == _("Hun Yuan Yin Qi"):
                    targ_obj.stamina += skill_level * 300
                    targ_obj.staminaMax += skill_level * 300


    def update_move_button_tooltips(self, move):
        self.balloon.bind(self.animation_buttons[self.you.sml.index(move)], move.description)


    def load_animation(self, move_name, animation_file_list, target=1):

        self.animation_target = target
        if animation_file_list:

            if self.animation_target:
                self.animation_x, self.animation_y = self.opp_x, self.opp_y
            else:
                self.animation_x, self.animation_y = self.you_x, self.you_y

            for img_file in animation_file_list[::-1]:
                img = PhotoImage(file=img_file)
                canvas_img = self.canvas.create_image(self.animation_x, self.animation_y, anchor=NW, image=img)
                if self.animation_target:
                    self.you_animation_img_list.append(img)
                    self.you_animation_list.append(canvas_img)
                else:
                    self.opp_animation_img_list.append(img)
                    self.opp_animation_list.append(canvas_img)

            frame_delta = self.animation_frame_delta_dic[move_name]
            self.animate(frame_delta)

    def animation_clean_up(self):

        if self.animation_target:
            self.you_animation_list = []
            self.you_animation_img_list = []
            self.you_animation_index = 0
        else:
            self.opp_animation_list = []
            self.opp_animation_img_list = []
            self.opp_animation_index = 0

    def animate(self, frame_delta):
        self.animation_in_progress = True
        if self.animation_target:
            try:
                self.canvas.delete(self.you_animation_list[::-1][self.you_animation_index])
            except:
                pass
            self.you_animation_index += 1
            if self.you_animation_index < len(self.you_animation_list):
                self.canvas.after(frame_delta, self.animate, frame_delta)
            else:
                self.animation_clean_up()
        else:
            try:
                self.canvas.delete(self.opp_animation_list[::-1][self.opp_animation_index])
            except:
                pass
            self.opp_animation_index += 1
            if self.opp_animation_index < len(self.opp_animation_list):
                self.canvas.after(frame_delta, self.animate, frame_delta)
            else:
                self.animation_clean_up()


    def updateLabels(self):

        try:
            if self.you.healthMax <= 0:
                self.you.healthMax = 0

            if self.opp.healthMax <= 0:
                self.opp.healthMax = 0

            if self.you.health <= 0:
                self.you.health = 0

            if self.opp.health <= 0:
                self.opp.health = 0

            if self.you.health >= self.you.healthMax:
                self.you.health = self.you.healthMax

            if self.opp.health >= self.opp.healthMax:
                self.opp.health = self.opp.healthMax

            self.hptext = _("Health: {}/{}").format(int(self.you.health), int(self.you.healthMax))
            self.opphptext = _("Health: {}/{}").format(int(self.opp.health), int(self.opp.healthMax))
            self.staminatext = _("Stamina: {}/{}").format(int(self.you.stamina), int(self.you.staminaMax))
            self.oppstaminatext = _("Stamina: {}/{}").format(int(self.opp.stamina), int(self.opp.staminaMax))
            self.statustext = _("Status: {}").format(self.you.status)
            self.oppstatustext = _("Status: {}").format(self.opp.status)

            self.hplabel.config(text=self.hptext)
            self.opphplabel.config(text=self.opphptext)

            self.staminalabel.config(text=self.staminatext)
            self.oppstaminalabel.config(text=self.oppstaminatext)

            self.statuslabel.config(text=self.statustext)
            self.oppstatuslabel.config(text=self.oppstatustext)

            self.update_bars()

        except:
            pass


    def update_bars(self):

        self.canvas.delete(self.you_canvas_hp_img)
        you_hp_img = Image.open("bar_hp.png").convert("RGBA")
        self.you_hp_img = you_hp_img.resize(
            (max([1, int(you_hp_img.width * self.you.health / self.you.healthMax)]), you_hp_img.height), Image.ANTIALIAS)
        self.you_hp_photo_img = ImageTk.PhotoImage(self.you_hp_img)
        self.you_canvas_hp_img = self.canvas.create_image(self.you_hp_mp_frame_x, self.you_hp_mp_frame_y,
                                                          anchor=NW, image=self.you_hp_photo_img)

        self.canvas.delete(self.you_canvas_mp_img)
        you_mp_img = Image.open("bar_mp.png").convert("RGBA")
        self.you_mp_img = you_mp_img.resize(
            (max([1, int(you_mp_img.width * self.you.stamina / self.you.staminaMax)]), you_mp_img.height), Image.ANTIALIAS)
        self.you_mp_photo_img = ImageTk.PhotoImage(self.you_mp_img)
        self.you_canvas_mp_img = self.canvas.create_image(self.you_hp_mp_frame_x,
                                                          self.you_hp_mp_frame_y + self.you_frame_height // 2,
                                                          anchor=NW, image=self.you_mp_photo_img)
        
        
        self.canvas.delete(self.opp_canvas_hp_img)
        opp_hp_img = Image.open("bar_hp.png").convert("RGBA")
        self.opp_hp_img = opp_hp_img.resize(
            (max([1, int(opp_hp_img.width * self.opp.health / self.opp.healthMax)]), opp_hp_img.height), Image.ANTIALIAS)
        self.opp_hp_photo_img = ImageTk.PhotoImage(self.opp_hp_img)
        self.opp_canvas_hp_img = self.canvas.create_image(self.opp_hp_mp_frame_x, self.opp_hp_mp_frame_y,
                                                          anchor=NW, image=self.opp_hp_photo_img)

        self.canvas.delete(self.opp_canvas_mp_img)
        opp_mp_img = Image.open("bar_mp.png").convert("RGBA")
        self.opp_mp_img = opp_mp_img.resize(
            (max([1, int(opp_mp_img.width * self.opp.stamina / self.opp.staminaMax)]), opp_mp_img.height), Image.ANTIALIAS)
        self.opp_mp_photo_img = ImageTk.PhotoImage(self.opp_mp_img)
        self.opp_canvas_mp_img = self.canvas.create_image(self.opp_hp_mp_frame_x,
                                                          self.opp_hp_mp_frame_y + self.opp_frame_height // 2,
                                                          anchor=NW, image=self.opp_mp_photo_img)

        
    def restore(self, h, s, full=False, targ=0, reason="normal"):

        self.currentEffect = "sfx_recover.mp3"
        self.startSoundEffectThread()

        if reason == "normal":
            if targ == 0:
                if full or (h + self.health >= self.healthMax and s + self.stamina >= self.staminaMax and h > 0 and s > 0):
                    self.health = self.healthMax
                    self.stamina = self.staminaMax
                    print("{} recovers to full health and stamina.\n".format(self.character))


                elif h + self.health >= self.healthMax and s <= 0:
                    self.health = self.healthMax
                    print("{} recovers to full health.\n".format(self.character))


                elif s + self.stamina >= self.staminaMax and h <= 0:
                    self.stamina = self.staminaMax
                    print("{} recovers to full stamina.\n".format(self.character))


                else:

                    if h + self.health >= self.healthMax:
                        h = int(self.healthMax) - int(self.health)

                    if s + self.stamina >= self.staminaMax:
                        s = int(self.staminaMax) - int(self.stamina)

                    self.health += h
                    self.stamina += s

                    if h > 0 and s > 0:
                        print("{} recovers {} health and {} stamina.\n".format(self.character, int(h), int(s)))

                    elif h > 0:
                        print("{} recovers {} health.\n".format(self.character, int(h)))

                    elif s > 0:
                        print("{} recovers {} stamina.\n".format(self.character, int(s)))

        elif reason == "battle":
            if targ == 0:
                targ_obj = self.you
            else:
                targ_obj = self.opp

            if full or (h + targ_obj.health >= targ_obj.healthMax and s + targ_obj.stamina >= targ_obj.staminaMax and h > 0 and s > 0):
                targ_obj.health = targ_obj.healthMax
                targ_obj.stamina = targ_obj.staminaMax
                print("{} recovers to full health and stamina.\n".format(self.character))


            elif h + targ_obj.health >= targ_obj.healthMax and s <= 0:
                targ_obj.health = targ_obj.healthMax
                print("{} recovers to full health.\n".format(self.character))


            elif s + targ_obj.stamina >= targ_obj.staminaMax and h <= 0:
                targ_obj.stamina = targ_obj.staminaMax
                print("{} recovers to full stamina.\n".format(self.character))


            else:

                if h + targ_obj.health >= targ_obj.healthMax:
                    h = int(targ_obj.healthMax) - int(targ_obj.health)

                if s + targ_obj.stamina >= targ_obj.staminaMax:
                    s = int(targ_obj.staminaMax) - int(targ_obj.stamina)

                targ_obj.health += h
                targ_obj.stamina += s

                if targ == 0:
                    if h > 0 and s > 0:
                        print("{} recovers {} health and {} stamina.\n".format(self.character, int(h), int(s)))

                    elif h > 0:
                        print("{} recovers {} health.\n".format(self.character, int(h)))

                    elif s > 0:
                        print("{} recovers {} stamina.\n".format(self.character, int(s)))




    def setStatus(self, status, targ=None):

        if targ:
            targ.status = status
            if status == _("Normal"):
                print("{}'s status has returned to 'Normal'.".format(self.character))

        else:
            self.status = status



    def battleCheckProfile(self, event):
        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        try:
            if self.battleProfileWin.winfo_exists():
                return
            else:
                raise

        except:

            self.battleProfileWin = Toplevel(self.battleWin)

            self.battle_stat_frame = Frame(self.battleProfileWin)
            self.battle_stat_frame.pack(side=LEFT)

            self.battle_skill_move_frame = Frame(self.battleProfileWin, borderwidth=3, relief=SOLID)
            self.battle_skill_move_frame.pack(side=RIGHT, expand=Y, fill=Y)

            F0 = Frame(self.battle_stat_frame)
            F0.pack(side=TOP)

            F0_0 = Frame(F0)
            F0_0.grid(row=0, column=0)

            photo1 = PhotoImage(file="you.ppm", master=self.battleProfileWin)
            pic1 = Label(F0_0, image=photo1)
            pic1.pack(side=LEFT)

            F0_1 = Frame(F0)
            F0_1.grid(row=0, column=1)
            F1 = Frame(F0_1)
            F1.pack()
            self.levelUpThreshold = self.levelThresholds[self.level - 1]

            self.profile_L0 = Label(F1, text=_("Name: {}").format(self.character))
            self.profile_L1 = Label(F1, text=_("Level: {}").format(int(self.level)))
            self.profile_L2 = Label(F1, text=_("Experience: {}/{}").format(int(self.exp), self.levelUpThreshold))
            self.profile_L3 = Label(F1, text=_("Upgrade Points: {}").format(int(self.upgradePoints)))
            self.profile_L4 = Label(F1, text=_("Status: {}").format(self.you.status))

            self.profile_L0.grid(row=0, column=0, sticky=W)
            self.profile_L1.grid(row=1, column=0, sticky=W)
            self.profile_L2.grid(row=2, column=0, sticky=W)
            self.profile_L3.grid(row=3, column=0, sticky=W)
            self.profile_L4.grid(row=4, column=0, sticky=W)

            F0_2 = Frame(F0)
            F0_2.grid(row=0, column=2)
            # Label(F0_2, text="\t").pack(side=TOP)
            # Label(F0_2, text="\t").pack(side=TOP)
            # inventory_icon = PhotoImage(file="inventory_icon.ppm")
            # imageButton1 = Button(F0_2, command=partial(self.checkInv,self.profileWin))
            # imageButton1.pack(side=BOTTOM)
            # imageButton1.config(image=inventory_icon)

            F2 = Frame(self.battle_stat_frame, borderwidth=3, relief=SOLID)
            F2.pack()
            self.profile_L6 = Label(F2, text=_("Health: {}/{}").format(int(self.you.health), int(self.you.healthMax)))
            self.profile_L7 = Label(F2, text=_("Stamina: {}/{}").format(int(self.you.stamina), int(self.you.staminaMax)))
            self.profile_L8 = Label(F2, text=_("Attack: {}").format(int(self.you.attack)))
            self.profile_L9 = Label(F2, text=_("Strength: {}").format(int(self.you.strength)))
            self.profile_L10 = Label(F2, text=_("Speed: {}").format(int(self.you.speed)))
            self.profile_L11 = Label(F2, text=_("Defence: {}").format(int(self.you.defence)))
            self.profile_L12 = Label(F2, text=_("Dexterity: {}").format(int(self.you.dexterity)))

            self.profile_L6.grid(row=0, column=0, sticky=W)
            self.profile_L7.grid(row=1, column=0, sticky=W)
            self.profile_L8.grid(row=2, column=0, sticky=W)
            self.profile_L9.grid(row=3, column=0, sticky=W)
            self.profile_L10.grid(row=4, column=0, sticky=W)
            self.profile_L11.grid(row=5, column=0, sticky=W)
            self.profile_L12.grid(row=6, column=0, sticky=W)


            self.profile_L13 = Label(F2, text=_("Perception: {}").format(int(self.perception)))
            self.profile_L14 = Label(F2, text=_("Luck: {}").format(int(self.luck)))
            self.profile_L15 = Label(F2, text=_("Chivalry: {}").format(int(self.chivalry)))
            self.profile_L13.grid(row=0, column=3, sticky=W)
            self.profile_L14.grid(row=1, column=3, sticky=W)
            self.profile_L15.grid(row=2, column=3, sticky=W)

            # B1 = Button(self.profileWin, text=_("Back"), command=partial(self.winSwitch, self.profileWin, fromWin))
            # B1.pack(side=BOTTOM)

            skill_move_image_list = []
            Label(self.battle_skill_move_frame, text="Attacks/Moves").pack()
            moves_frame = Frame(self.battle_skill_move_frame, borderwidth=1, relief=SUNKEN)
            moves_frame.pack()

            frame_width = 8
            frame_x = 0
            frame_y = 0
            for i in range(len(self.sml) - 3):
                move = self.sml[i]
                move_image = PhotoImage(file=move.file_name)
                move_image_label = Button(moves_frame, image=move_image)
                move_image_label.grid(row=frame_y, column=frame_x)
                move_image_label.config(image=move_image)
                skill_move_image_list.append(move_image)
                self.balloon.bind(move_image_label, move.description)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1

            Label(self.battle_skill_move_frame, text="Inner Energy/Agility Techniques").pack()
            skills_frame = Frame(self.battle_skill_move_frame)
            skills_frame.pack()

            frame_width = 8
            frame_x = 0
            frame_y = 0

            for i in range(len(self.skills)):
                skill = self.skills[i]
                skill_image = PhotoImage(file=skill.file_name)
                skill_image_label = Button(skills_frame, image=skill_image)
                skill_image_label.grid(row=frame_y, column=frame_x)
                skill_image_label.config(image=skill_image)
                skill_move_image_list.append(skill_image)
                self.balloon.bind(skill_image_label, skill.description)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1

            self.battleProfileWin.update_idletasks()
            toplevel_w, toplevel_h = self.battleProfileWin.winfo_width(), self.battleProfileWin.winfo_height()
            self.battleProfileWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
            self.battleProfileWin.focus_force()
            self.battleProfileWin.mainloop()###

    def oppBattleCheckProfile(self,event):
        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        try:
            if self.oppBattleProfileWin.winfo_exists():
                return
            else:
                raise

        except:

            self.oppBattleProfileWin = Toplevel(self.battleWin)

            opp_battle_stat_frame = Frame(self.oppBattleProfileWin)
            opp_battle_stat_frame.pack(side=LEFT)

            opp_battle_skill_move_frame = Frame(self.oppBattleProfileWin, borderwidth=3, relief=SOLID)
            opp_battle_skill_move_frame.pack(side=RIGHT, expand=Y, fill=Y)

            F0 = Frame(opp_battle_stat_frame)
            F0.pack(side=TOP)

            F0_0 = Frame(F0)
            F0_0.grid(row=0, column=0)

            photo1 = PhotoImage(file=_("{}_icon.ppm").format(self.opp.name), master=self.oppBattleProfileWin)
            pic1 = Label(F0_0, image=photo1)
            pic1.pack(side=LEFT)

            F0_1 = Frame(F0)
            F0_1.grid(row=0, column=1)
            F1 = Frame(F0_1)
            F1.pack()

            self.profile_L0 = Label(F1, text=_("Name: {}").format(self.opp.name))
            self.profile_L1 = Label(F1, text=_("Level: {}").format(int(self.opp.level)))
            self.profile_L2 = Label(F1, text=_("Status: {}").format(self.opp.status))

            self.profile_L0.grid(row=0, column=0, sticky=W)
            self.profile_L1.grid(row=1, column=0, sticky=W)
            self.profile_L2.grid(row=2, column=0, sticky=W)

            F0_2 = Frame(F0)
            F0_2.grid(row=0, column=2)

            F2 = Frame(opp_battle_stat_frame, borderwidth=3, relief=SOLID)
            F2.pack()
            self.profile_L6 = Label(F2, text=_("Health: {}/{}").format(int(self.opp.health), int(self.opp.healthMax)))
            self.profile_L7 = Label(F2,
                                    text=_("Stamina: {}/{}").format(int(self.opp.stamina), int(self.opp.staminaMax)))
            self.profile_L8 = Label(F2, text=_("Attack: {}").format(int(self.opp.attack)))
            self.profile_L9 = Label(F2, text=_("Strength: {}").format(int(self.opp.strength)))
            self.profile_L10 = Label(F2, text=_("Speed: {}").format(int(self.opp.speed)))
            self.profile_L11 = Label(F2, text=_("Defence: {}").format(int(self.opp.defence)))
            self.profile_L12 = Label(F2, text=_("Dexterity: {}").format(int(self.opp.dexterity)))

            self.profile_L6.grid(row=0, column=0, sticky=W)
            self.profile_L7.grid(row=1, column=0, sticky=W)
            self.profile_L8.grid(row=2, column=0, sticky=W)
            self.profile_L9.grid(row=3, column=0, sticky=W)
            self.profile_L10.grid(row=4, column=0, sticky=W)
            self.profile_L11.grid(row=5, column=0, sticky=W)
            self.profile_L12.grid(row=6, column=0, sticky=W)

            skill_move_image_list = []
            Label(opp_battle_skill_move_frame, text="Attacks/Moves").pack()
            moves_frame = Frame(opp_battle_skill_move_frame, borderwidth=1, relief=SUNKEN)
            moves_frame.pack()

            frame_width = 8
            frame_x = 0
            frame_y = 0
            for i in range(len(self.opp.sml)):
                move = self.opp.sml[i]
                move_image = PhotoImage(file=move.file_name)
                move_image_label = Button(moves_frame, image=move_image)
                move_image_label.grid(row=frame_y, column=frame_x)
                move_image_label.config(image=move_image)
                skill_move_image_list.append(move_image)
                self.balloon.bind(move_image_label, move.description)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1

            Label(opp_battle_skill_move_frame, text="Inner Energy/Agility Techniques").pack()
            skills_frame = Frame(opp_battle_skill_move_frame)
            skills_frame.pack()

            frame_width = 8
            frame_x = 0
            frame_y = 0

            for i in range(len(self.opp.skills)):
                skill = self.opp.skills[i]
                skill_image = PhotoImage(file=skill.file_name)
                skill_image_label = Button(skills_frame, image=skill_image)
                skill_image_label.grid(row=frame_y, column=frame_x)
                skill_image_label.config(image=skill_image)
                skill_move_image_list.append(skill_image)
                self.balloon.bind(skill_image_label, skill.description)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1

            self.oppBattleProfileWin.update_idletasks()
            toplevel_w, toplevel_h = self.oppBattleProfileWin.winfo_width(), self.oppBattleProfileWin.winfo_height()
            self.oppBattleProfileWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
            self.oppBattleProfileWin.focus_force()
            self.oppBattleProfileWin.mainloop()###

    def endTurn(self):

        self.youHaveMoved = False
        self.oppHasMoved = False
        self.turnEnded = False
        self.animation_in_progress = False

        if _("Tendon Changing Technique") in [s.name for s in self.you.skills]:
            if self.you.status != _("Normal") and random() <= .2:
                self.currentEffect = "sfx_restore_status.mp3"
                self.startSoundEffectThread()
                print(_("Your status is restored to 'Normal' due to effects of the Tendon Changing Technique."))
                self.you.status = _("Normal")
                self.youAcupunctureCount = 0
                self.youBlindCount = 0
                self.youDeathCount = 0
                self.updateLabels()

        if _("Tendon Changing Technique") in [s.name for s in self.opp.skills]:
            if self.opp.status != _("Normal") and random() <= .2:
                self.currentEffect = "sfx_restore_status.mp3"
                self.startSoundEffectThread()
                print(_("Your opponent's status is restored to 'Normal' due to effects of the Tendon Changing Technique."))
                self.opp.status = _("Normal")
                self.oppAcupunctureCount = 0
                self.oppBlindCount = 0
                self.oppDeathCount = 0
                self.updateLabels()

        if _("Death") in self.you.status:
            if random() <= self.youDeathCount/100:
                self.you.health = 0
                print(_("Your health dropped to 0 due to the effects of 'Kiss of Death'!"))

        if _("Death") in self.opp.status:
            if random() <= self.oppDeathCount/100:
                self.opp.health = 0
                print(_("The opponent's health dropped to 0 due to the effects of 'Kiss of Death'!"))

        if not self.battleEnded:
            t = Thread(target=self.allOn, args=(self.animation_buttons,))
            self.battleWin.after(1000, t.start)
            self.battleWin.after(500, self.checkForEffects)
            
            if self.you.status != _("Normal"):
                self.statusTooltip = _("")
                for status, description in BATTLE_STATUS_DESCRIPTIONS.items():
                    if status in self.statustext:
                        self.statusTooltip += description
                self.balloon.bind(self.statuslabel, self.statusTooltip.strip())

            if self.opp.status != _("Normal"):
                self.oppStatusTooltip = _("")
                for status, description in BATTLE_STATUS_DESCRIPTIONS.items():
                    if status in self.oppstatustext:
                        self.oppStatusTooltip += description
                self.balloon.bind(self.oppstatuslabel, self.oppStatusTooltip.strip())

            if self.autoBattle:
                self.battleWin.after(1500, self.auto_select_player_move)

        print("\n")


    def checkForDeath(self):

        # print(self.you.health)
        # print(self.opp.health)
        if self.opp.health <= 0 and not self.battleEnded:
            self.youHaveMoved = True
            self.oppHasMoved = True
            self.battleEnded = True
            if self.opp_list:
                self.battleExperience = int(max([100*(1.275**self.opp.level) - 50*1.2**self.level, 300])) + 100*self.opp.level
                self.gainExp(self.battleExperience)
                self.opp = self.opp_list.pop()
                self.battleWin.after(1000, self.set_battle_scene)
            else:
                t = Thread(target=self.battleVictory, args=(self.destinationWinList[0],))
                self.battleWin.after(1000, t.start)

        elif self.you.health <= 0 and not self.battleEnded:
            self.youHaveMoved = True
            self.oppHasMoved = True
            self.battleEnded = True
            time.sleep(1)
            self.battleLoss(self.destinationWinList[1])

        else:
            return None


    def gainExp(self, amount):
        try:
            if self.playAsOther:
                self.playAsOther = False
                return
        except:
            pass
        self.exp += int(amount)
        print(_("{} gains {} experience.\n\n").format(self.character, amount))
        while self.exp >= self.levelThresholds[self.level - 1]:
            self.level += 1
            self.upgradePoints += int(random() * self.perception / 8) + self.level
            self.staminaMax += self.level * 10
            self.healthMax += self.level * 10
            print(_("{} leveled up to level {}!").format(self.character, self.level))
            print(_("Health and stamina upper limits increased by {}!").format(self.level * 10))


    def battleVictory(self, destinationWin=None):
        self.health = self.you.health
        self.stamina = self.you.stamina
        self.status = _("Normal")
        #self.status = self.you.status
        self.create_self_character()

        self.battleExperience = int(max([100*(1.275**self.opp.level) - 50*1.2**self.level, 300])) + 100*self.opp.level
        self.gainExp(self.battleExperience)

        time.sleep(1)

        if self.postBattleSoundtrack:
            self.stopSoundtrack()
            self.currentBGM = self.postBattleSoundtrack
            self.startSoundtrackThread()


        # self.battleWin.destroy()
        self.battleWin.withdraw()
        try:
            self.invWin.destroy()
        except Exception as e:
            pass


        if self.battleID == "HuangXiaodongTrainingTest":
            self.battleWin.after(100, self.cheater_ending, self.battleID)
            self.battleID = None
            return
        elif self.battleID == "ShaolinTrainingTest":
            self.battleWin.after(100, self.cheater_ending, self.battleID)
            self.battleID = None
            return
        elif self.battleID == "LiGuipingPunishment":
            self.battleWin.after(100, self.postBattleCmd, True)
            self.battleID = None
            return
        elif self.battleID == "guo_zhiqiang_conflict_11":
            self.currentBGM = "jy_shendiaoshuhuan.mp3"
            self.startSoundtrackThread()
            cmd = lambda: self.postBattleCmd(11)
            self.battleWin.after(100, cmd)
            self.battleID = None
            return
        elif self.battleID == "yagyu_clan_initial_challenge":
            self.currentBGM = "Sakura_Variation.mp3"
            self.startSoundtrackThread()
            cmd = lambda: self.postBattleCmd(1)
            self.battleWin.after(100, cmd)
            self.battleID = None
            return
        elif self.battleID == "yagyu_tadashi_spar":
            self.currentBGM = "Sakura_Variation.mp3"
            self.startSoundtrackThread()
            cmd = lambda: self.postBattleCmd(2)
            self.battleWin.after(100, cmd)
            self.battleID = None
            return
        elif self.battleID == "shaolin_yagyu_tournament":
            self.currentBGM = "jy_suspenseful1.mp3"
            self.startSoundtrackThread()
            self.battleID = None
            self.playAsOther = False
            cmd = lambda: self.postBattleCmd(1)
            self.battleWin.after(100, cmd)
            return
        elif self.battleID == "shaolin_yagyu_tournament_self":
            self.currentBGM = "jy_suspenseful1.mp3"
            self.startSoundtrackThread()
            self.battleID = None
            cmd = lambda: self.postBattleCmd(1)
            self.battleWin.after(100, cmd)
            return
        elif self.battleID == "babao_tower_tournament":
            self.battleID = None
            self.taskProgressDic["blind_sniper"] += 1
            cmd = lambda: self.postBattleCmd(1)
            self.battleWin.after(100, cmd)
            return
        elif self.battleID == "potters_field":
            self.health = self.healthMax
            self.stamina = self.staminaMax
            self.status = 'Normal'
            self.battleWin.after(100, self.go_potters_field, 10)
            self.battleID = None
            return


        if destinationWin:
            destinationWin.deiconify()
        if self.postBattleCmd:
            self.battleWin.after(100, self.postBattleCmd)


    def battleLoss(self, destinationWin=None):

        print(_("You have been defeated.\n"))
        # self.battleWin.destroy()
        try:
            self.battleWin.withdraw()
            self.invWin.destroy()
        except Exception as e:
            pass
        self.faint(destinationWin)


    def faint(self, destinationWin):

        self.stopSoundtrack()

        if self.battleID == "HuangXiaodongTrainingTest":
            self.battleWin.after(100, self.postBattleCmd)
            self.battleID = None
            return

        elif self.battleID == "ShaolinTrainingTest":
            self.battleWin.after(100, self.postBattleCmd)
            self.battleID = None
            return

        elif self.battleID == "LiGuipingPunishment":
            self.health = 10
            self.stamina = self.you.stamina
            self.status = 'Normal'
            self.battleID = None
            self.battleWin.after(100, self.postBattleCmd, False)
            return

        elif self.battleID == "feng_pan_inn_battle":
            self.health = 10
            self.stamina = self.you.stamina
            self.status = 'Normal'
            self.battleID = None
            if self.taskProgressDic["join_sect"] == 200:
                self.taskProgressDic["feng_pan"] = 43
            else:
                self.taskProgressDic["feng_pan"] = 33
            self.battleWin.after(100, self.go_inn)
            return

        elif self.battleID == "li_guiying_battle" and self.opp.healthMax//2 >= self.opp.health and self.luck >= 80:

            self.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("(I can't believe this is how I'm gonna go...)"),
                 _("Hold it!")],
                ["you", "Blind Sniper"]
            )

            self.currentEffect = "sfx_male_voice.mp3"
            self.startSoundEffectThread()
            self.currentEffect = "sfx_basic_sword_technique.mp3"
            self.startSoundEffectThread()
            self.currentEffect = "sfx_blind_sniper_blade.mp3"
            self.startSoundEffectThread()

            self.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("AHHHHHHH!!!"),
                 _("If it wasn't for the fact that she killed so many innocent people, I would've let her live."),
                 _("After all, it almost seems as if I'm taking unfair advantage of the situation..."),
                 _("Thanks for saving me!"),
                 _("Don't mention it; I applaud you for your bravery, young man."),
                 _("Here's some gold for you to buy some medicine and heal your wounds.")],
                ["Li Guiying", "Blind Sniper", "Blind Sniper", "you", "Blind Sniper", "Blind Sniper"]
            )

            self.taskProgressDic[_("li_guiying")] = 201
            if self.luck >= 90:
                self.inv["Gold"] += 3000
                messagebox.showinfo("", "Received 3000 Gold!")
            else:
                self.inv["Gold"] += 2000
                messagebox.showinfo("", "Received 2000 Gold!")

            self.currentBGM = "jy_ambient.mp3"
            self.startSoundtrackThread()

            self.health = 10
            self.stamina = self.you.stamina
            self.status = 'Normal'
            self.battleID = None
            self.battleWin.after(100, self.postBattleCmd)
            return

        elif self.battleID == "blind_sniper_battle" and self.opp.healthMax//2 >= self.opp.health and self.taskProgressDic[_("li_guiying")] != 110 and self.luck >= 80:

            self.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("I warned you to stay out of this. Now you die..."),
                 _("Not so quick!")],
                ["Blind Sniper", "Li Guiying"]
            )

            self.currentEffect = "jy_nv3.mp3"
            self.startSoundEffectThread()
            self.currentEffect = "sfx_five_poison_palm.mp3"
            self.startSoundEffectThread()

            self.generate_dialogue_sequence(
                None,
                "liu_village_house.png",
                [_("You just finished a long battle; do you still think you can take me down easily?"),
                 _("Consider yourselves lucky this time... I will be back..."),
                 _("Whew... that was a close one... thanks."),
                 _("Now neither of us owes each other anything..."),
                 _("Aw come on, the only reason I'm in such a state is because I tried to help you in the first place."),
                 _("How about this... if you help me achieve Level 10 of the Five Poison Palm, I will teach you the technique."),
                 _("Ohhhhh! So is that what you've been practicing on the villagers?"),
                 _("Precisely..."),
                 _("Sounds like a deal; what do you need help with?"),
                 _("The Five Poison Palm is so named because to practice it, one must first absorb the poison from 5 different types of Poisonous Moths."),
                 _("Unfortunately, these moths are not easy to catch, so perhaps you can save me some time by catching them for me."),
                 _("No problem! Where can I find these moths?"),
                 _("You can go look in the nearby forest. I'll need 5 of each type, so 25 moths total."),
                 _("Got it!")],
                ["Li Guiying", "Blind Sniper", "you", "Li Guiying", "you",
                 "Li Guiying", "you", "Li Guiying", "you", "Li Guiying",
                 "Li Guiying", "you", "Li Guiying", "you"]
            )

            self.taskProgressDic[_("li_guiying")] = 101
            self.currentBGM = "jy_ambient.mp3"
            self.startSoundtrackThread()

            self.health = 10
            self.stamina = self.you.stamina
            self.status = 'Normal'
            self.battleID = None
            self.battleWin.after(100, self.postBattleCmd)
            return


        elif self.battleID == "guo_zhiqiang_conflict_11":

            self.currentBGM = "jy_shendiaoshuhuan.mp3"
            self.startSoundtrackThread()

            self.health = 10
            self.stamina = self.you.stamina
            self.status = 'Normal'
            self.battleID = None
            cmd = lambda: self.postBattleCmd(12)
            self.battleWin.after(100, cmd)
            return

        elif self.battleID == "zhao_huilin_guo_junda_battle":
            self.health = self.healthMax
            self.stamina = self.staminaMax
            self.status = 'Normal'
            self.battleID = None
            self.currentBGM = self.postBattleSoundtrack
            self.startSoundtrackThread()
            self.battleWin.after(100, self.postBattleCmd)
            return

        elif self.battleID == "shaolin_yagyu_spar":
            self.health = self.healthMax
            self.stamina = self.staminaMax
            self.status = 'Normal'
            self.battleID = None
            self.playAsOther = False
            self.currentBGM = self.postBattleSoundtrack
            self.startSoundtrackThread()
            self.battleWin.after(100, self.postBattleCmd)
            return

        elif self.battleID == "shaolin_yagyu_tournament":
            self.currentBGM = "jy_suspenseful1.mp3"
            self.startSoundtrackThread()
            self.battleID = None
            self.playAsOther = False
            cmd = lambda: self.postBattleCmd(-1)
            self.battleWin.after(100, cmd)
            return

        elif self.battleID == "shaolin_yagyu_tournament_self":
            self.currentBGM = "jy_suspenseful1.mp3"
            self.startSoundtrackThread()
            self.battleID = None
            cmd = lambda: self.postBattleCmd(-1)
            self.battleWin.after(100, cmd)
            return

        elif self.battleID == "huang_yuwei_birthday_challenge_battle":
            if self.taskProgressDic["huang_yuwei_birthday"] == 10:
                self.taskProgressDic["huang_yuwei_birthday"] = -20
            elif self.taskProgressDic["huang_yuwei_birthday"] == 1:
                self.taskProgressDic["huang_yuwei_birthday"] = -10

            self.health = self.healthMax
            self.stamina = self.staminaMax
            self.status = 'Normal'
            self.battleID = None
            self.currentBGM = self.postBattleSoundtrack
            self.startSoundtrackThread()
            self.battleWin.after(100, self.postBattleCmd)
            return

        elif self.battleID == "yagyu_clan_initial_challenge":
            self.health = 10
            self.status = 'Normal'
            self.battleID = None
            self.currentBGM = self.postBattleSoundtrack
            self.startSoundtrackThread()
            cmd = lambda: self.postBattleCmd(-1)
            self.battleWin.after(100, cmd)
            return

        elif self.battleID == "yagyu_tadashi_spar":
            self.health = 10
            self.status = 'Normal'
            self.battleID = None
            self.currentBGM = self.postBattleSoundtrack
            self.startSoundtrackThread()
            cmd = lambda: self.postBattleCmd(-2)
            self.battleWin.after(100, cmd)
            return

        elif self.battleID == "babao_tower_tournament":
            self.health = 10
            self.status = 'Normal'
            self.battleID = None
            if self.taskProgressDic["blind_sniper"] == 6:
                self.taskProgressDic["blind_sniper"] += 1
            else:
                self.taskProgressDic["blind_sniper"] = 3
            cmd = lambda: self.postBattleCmd(1)
            self.battleWin.after(100, cmd)
            return

        elif self.battleID == "potters_field":
            self.health = self.healthMax
            self.stamina = self.staminaMax
            self.status = 'Normal'
            self.currentBGM = "jy_calm2.mp3"
            self.startSoundtrackThread()
            self.battleWin.after(100, self.go_potters_field, 20)
            self.battleID = None
            return

        self.battleID = None

        if self.battleType == "training":
            self.battleExperience = int(.25 * max([int(min([self.opp.level, 10])** 2.75), 100])) + 50
            self.gainExp(self.battleExperience)
            if self.battleID != "LiGuipingPunishment":
                self.restore(0, 0, True)
                print(_("You wake up from the battle, finding yourself fully healed."))
            self.status = _("Normal")
            self.currentBGM = self.postBattleSoundtrack
            self.startSoundtrackThread()
            try:
                if destinationWin:
                    destinationWin.deiconify()
                if self.postBattleCmd:
                    self.battleWin.after(100, self.postBattleCmd)
            except:
                pass

        elif self.inv[_("Gold")] >= 300 and self.opp.name == "Scrub" and self.chivalry < 0:
            self.inv[_("Gold")] -= 300
            self.battleExperience = int(.25 * max([int(self.opp.level ** 2.75), 100])) + 50
            self.gainExp(self.battleExperience)
            self.restore(0, 0, True)
            self.status = _("Normal")
            print(_("You wake up at Scrub's House after passing out for an unknown length\nof time. Your gold pouch feels lighter than before..."))

            try:
                self.scrubHouseWin.deiconify()
            except:
                pass

            self.currentBGM = "jy_calm2.mp3"
            self.startSoundtrackThread()

        else:
            self.display_lose_screen()


    def display_lose_screen(self):

        try:
            if self.mini_game_in_progress:
                self.mini_game.running = False
                self.mini_game.playing = False
                self.mini_game_in_progress = False
                pg.display.quit()
                if os.name == "nt":
                    self.mainWin.tk.call('wm', 'iconphoto', self.mainWin._w, PhotoImage(file=r'game_main_icon.png'))
                else:
                    self.mainWin.tk.call('wm', 'iconphoto', self.mainWin._w, PhotoImage(file=r'game_main_icon.png'))
                if os.name != "nt":
                    self.stopSoundtrack()
            else:
                self.stopSoundtrack()
        except Exception as e:
            pass


        self.currentBGM = "jy_gameover.mp3"
        self.startSoundtrackThread()
        try:
            self.battleWin.withdraw()
        except:
            pass

        self.lose_screen_win = Toplevel(self.mainWin)

        bg = PhotoImage(file="GameOverEn.png", master=self.lose_screen_win)
        panel = Label(self.lose_screen_win, image=bg)
        panel.pack(expand="yes")
        panel.image = bg

        Button(self.lose_screen_win, text=_("Back to Main Menu"), command=self.restartGame).pack()

        self.lose_screen_win.update_idletasks()
        toplevel_w, toplevel_h = self.lose_screen_win.winfo_width(), self.lose_screen_win.winfo_height()
        self.lose_screen_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.lose_screen_win.focus_force()
        self.lose_screen_win.mainloop()###


    def restartGame(self):

        self.stopSoundtrack()
        self.currentEffect = "button-20.mp3"
        self.currentBGM = "jy_intro.mp3"
        self.startSoundEffectThread()
        self.startSoundtrackThread()

        self.lose_screen_win.quit()
        self.lose_screen_win.destroy()
        #self.destroy_all(self.mainWin)
        self.mainWin.deiconify()


    def destroy_all(self, targ):
        for widget in targ.winfo_children():
            widget.destroy()



    def cheater_ending(self, battleID):

        if battleID == "HuangXiaodongTrainingTest":
            self.generate_dialogue_sequence(
                None,
                "cherry_blossom_island.png",
                [_("I... I won!"),
                 _("Unbelievable! I actually won!"),
                 _("But why do I feel so...empty?"),
                 _("..."),
                 _("I just started this journey, yet somehow, I've already beaten one of the Elite Four..."),
                 _("If that's the case, what is there to look forward to?"),
                 _("*Sigh* ... There is no point... no challenge... no motivation..."),
                 _("I suppose I'd better just jump off a cliff and end it here."),],
                ["you"]*8
            )

        elif battleID == "ShaolinTrainingTest":
            self.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("I... I won!"),
                 _("Unbelievable! I actually won!"),
                 _("But why do I feel so...empty?"),
                 _("..."),
                 _("I just started this journey, yet somehow, I've already beaten the Abbot of Shaolin..."),
                 _("If that's the case, what is there to look forward to?"),
                 _("*Sigh* ... There is no point... no challenge... no motivation..."),
                 _("I suppose I'd better just jump off a cliff and end it here."),],
                ["you"]*8
            )


        self.display_lose_screen()



    def checkForEffects(self):

        if self.you.status != _("Normal") or self.opp.status != _("Normal"):
            if _("Poison") in self.you.status:
                print(_("You are hurt by poison!\n"))
                if _("Poison++") in self.you.status:
                    self.you.health -= int(self.you.healthMax * .1 *self.opp_poison_damage_multiplier)
                elif _("Poison+") in self.you.status:
                    self.you.health -= int(self.you.healthMax * .075*self.opp_poison_damage_multiplier)
                elif _("Poison") in self.you.status:
                    self.you.health -= int(self.you.healthMax * .05*self.opp_poison_damage_multiplier)

            if _("Poison") in self.opp.status:
                print(self.you_poison_damage_multiplier)
                print(_("{} is hurt by poison!\n".format(self.opp.name)))
                if _("Poison++") in self.opp.status:
                    self.opp.health -= int(self.opp.healthMax * .1*self.you_poison_damage_multiplier)
                elif _("Poison+") in self.opp.status:
                    self.opp.health -= int(self.opp.healthMax * .075*self.you_poison_damage_multiplier)
                elif _("Poison") in self.opp.status:
                    self.opp.health -= int(self.opp.healthMax * .05*self.you_poison_damage_multiplier)

            if _("Internal Injury") in self.you.status:
                print(_("You lost some stamina due to internal injury!\n"))
                if _("Internal Injury++") in self.you.status:
                    self.you.stamina -= int(self.you.staminaMax * .1)
                elif _("Internal Injury+") in self.you.status:
                    self.you.stamina -= int(self.you.staminaMax * .075)
                elif _("Internal Injury") in self.you.status:
                    self.you.stamina -= int(self.you.staminaMax * .05)

            if _("Internal Injury") in self.opp.status:
                print(_("{} loses some stamina due to internal injury!\n".format(self.opp.name)))
                if _("Internal Injury++") in self.opp.status:
                    self.opp.stamina -= int(self.opp.staminaMax * .1)
                elif _("Internal Injury+") in self.opp.status:
                    self.opp.stamina -= int(self.opp.staminaMax * .075)
                elif _("Internal Injury") in self.opp.status:
                    self.opp.stamina -= int(self.opp.staminaMax * .05)

            if _("Bleeding") in self.you.status:
                print(_("You're bleeding profusely and lost some health and stamina!\n"))
                self.you.health -= self.opp.strength
                self.you.stamina -= self.opp.strength

            if _("Bleeding") in self.opp.status:
                print(_("Your opponent is bleeding profusely and lost some health and stamina!\n"))
                self.opp.health -= self.you.strength
                self.opp.stamina -= self.you.strength

            if self.you.health <= 0:
                self.you.health = 0
            if self.you.stamina <= 0:
                self.you.stamina = 0
            if self.opp.health <= 0:
                self.opp.health = 0
            if self.opp.stamina <= 0:
                self.opp.stamina = 0

            self.updateLabels()
            self.checkForDeath()
            return True
        else:
            return False


    def pickpocket(self, target):
        if target == "self":
            r = randrange(50,301)
            self.add_item(_("Gold"), -r)
            print(_("Some of your gold is stolen...\n"))
            
        else:
            if target in PICKPOCKET_ITEMS:
                item_dic = PICKPOCKET_ITEMS[target]
                for key in item_dic:
                    x,y,z = item_dic[key]
                    if random() <= z:
                        r = randrange(x,y)
                        self.add_item(key, r)
                        print(_("You managed to steal {} x {} from {}.").format(key, r, target))
                        break

            elif target == _("A-Xiu") and self.taskProgressDic["cai_wuqiong"] == 10 and random() <= .05*self.luck/50:
                self.add_item(_("A-Xiu's Shoes"), 1)
                self.taskProgressDic["cai_wuqiong"] = 11
                print("Stolen")
                
            else:
                r = randrange(20,51)
                self.add_item(_("Gold"), r)
                print(_("You managed to steal Gold x {} from {}.").format(r, target))



    def youGotHit(self, damage, move, mute=False, deflected=False):

        if deflected:
            damage = int(damage)
            self.you.health = int(self.you.health - damage)
            if self.you.health <= 0:
                self.you.health = 0

        else:

            if move.name in [_("Violent Dragon Palm")]:
                self.oppConsecutiveHits += 1
            else:
                self.oppConsecutiveHits = 0

            if mute:
                pass
            else:
                self.currentEffect = move.sfx_file_name
                self.startSoundEffectThread()

            damage = int(damage)

            self.you.health = int(self.you.health - damage)
            if self.you.health <= 0:
                self.you.health = 0

            self.load_animation(move.name, move.animation_file_list, target=0)

            # Apply move effects
            effects = []
            r = random()
            r = r * (1 + self.you_status_bonus)

            if move.name == _("Five Poison Palm") and _("Countering Poison With Poison") not in [s.name for s in
                                                                                                 self.you.skills]:
                # r -= self.opp_poison_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2) or (
                        move.level >= 3 and r <= .1):
                    effects.append(_("Poison"))
                    print(_("You've been poisoned!"))

            elif move.name == _("Falling Cherry Blossom Finger Technique") and move.level >= 3:
                # r -= self.opp_seal_bonus
                if (move.level >= 10 and r <= .4) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2) or (
                        move.level >= 3 and r <= .1):
                    self.youAcupunctureCount = 3
                    effects.append(_("Seal (3)"))
                    print(_("Your acupuncture points have been sealed!"))

            elif move.name == _("Shaolin Diamond Finger") and move.level >= 3:
                # r -= self.opp_seal_bonus
                if (move.level >= 10 and r <= 1) or (move.level >= 7 and r <= .6) or (move.level >= 5 and r <= .4) or (
                        move.level >= 3 and r <= .2):
                    self.youAcupunctureCount = 1
                    effects.append(_("Seal (1)"))
                    print(_("Your acupuncture points have been sealed!"))

            elif move.name == _("Lightning Finger Technique") and move.level >= 3:
                # r -= self.opp_seal_bonus
                if (move.level >= 10 and r <= .4) or (move.level >= 7 and r <= .25) or (
                        move.level >= 5 and r <= .15) or (move.level >= 3 and r <= .05):
                    self.youAcupunctureCount = 1
                    effects.append(_("Seal (1)"))
                    self.youActionPoints -= self.you.speed
                    print(_("Your acupuncture points have been sealed!"))
                    print(_("You were stunned by the attack!"))

            elif move.name == _("Fairy Plucking Flowers"):
                # r -= self.opp_seal_bonus
                if (move.level >= 10 and r <= 1) or (move.level >= 8 and r <= .8) or (
                        move.level >= 6 and r <= .6) or (move.level >= 4 and r <= .4) or (move.level >= 2 and r <= .2):
                    self.youAcupunctureCount = 1
                    effects.append(_("Seal (1)"))
                    print(_("Your acupuncture points have been sealed!"))
                    self.pickpocket("self")

            elif move.name == _("Cherry Blossom Drifting Snow Blade") and move.level >= 3:
                # r -= self.opp_blind_bonus
                if (move.level >= 10 and r <= .4) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2) or (
                        move.level >= 3 and r <= .1):
                    self.youBlindCount = 3
                    effects.append(_("Blind (3)"))
                    print(_("You have been temporarily blinded by the attack!"))


            elif move.name == _("Babao Blade") and r <= .75 and move.level >= 3:
                # r -= self.opp_blind_bonus
                r_effect = randrange(1, move.level - 1)
                if r_effect == 1:
                    self.youBlindCount = 1
                    effects.append(_("Blind (1)"))
                    print(_("You have been temporarily blinded by the attack!"))
                elif r_effect == 2:
                    self.youAcupunctureCount = 1
                    effects.append(_("Seal (1)"))
                    print(_("Your acupuncture points have been sealed!"))
                elif r_effect == 3:
                    effects.append(_("Internal Injury"))
                    print(_("You're suffering from internal injury as a result of the opponent's attack!"))
                elif r_effect == 4:
                    self.youActionPoints -= self.you.speed
                    print(_("You were stunned by the attack!"))
                elif r_effect == 5:
                    self.you.defence -= 20
                    print(_("Your defense decreased by 20!"))
                elif r_effect == 6:
                    self.you.speed -= 20
                    print(_("Your speed decreased by 20!"))
                elif r_effect == 7:
                    self.youBlindCount = 2
                    effects.append(_("Blind (2)"))
                    print(_("You have been temporarily blinded by the attack!"))
                else:
                    self.youAcupunctureCount = 2
                    effects.append(_("Seal (2)"))
                    print(_("Your acupuncture points have been sealed!"))


            elif move.name == _("Shadow Blade"):
                if move.level >= 10:
                    self.you.defence -= 10
                    self.you.dexterity -= 10
                    print(_("Your defence and dexterity decreased by 10!"))

                elif move.level >= 8:
                    self.you.defence -= 8
                    self.you.dexterity -= 8
                    print(_("Your defence and dexterity decreased by 8!"))

                elif move.level >= 6:
                    self.you.defence -= 6
                    self.you.dexterity -= 6
                    print(_("Your defence and dexterity decreased by 6!"))

                elif move.level >= 4:
                    self.you.defence -= 4
                    self.you.dexterity -= 4
                    print(_("Your defence and dexterity decreased by 4!"))

                elif move.level >= 2:
                    self.you.defence -= 2
                    self.you.dexterity -= 2
                    print(_("Your defence and dexterity decreased by 2!"))

            elif move.name == _("Eagle Claws"):
                if move.level >= 10:
                    self.you.attack -= 3
                    self.you.strength -= 3
                    self.you.speed -= 3
                    self.you.defence -= 3
                    self.you.dexterity -= 3
                    print(_("All of your stats decreased by 3!"))

                elif move.level >= 5:
                    self.you.attack -= 2
                    self.you.strength -= 2
                    self.you.speed -= 2
                    self.you.defence -= 2
                    self.you.dexterity -= 2
                    print(_("All of your stats decreased by 2!"))

            elif move.name == _("Serpent Whip") and move.level >= 3:
                if move.level >= 10:
                    sd = 12
                elif move.level >= 6:
                    sd = 8
                else:
                    sd = 4
                r = randrange(5)
                if r == 0:
                    self.you.attack -= sd
                    print(_("Your attack decreased by {}!").format(sd))
                elif r == 1:
                    self.you.strength -= sd
                    print(_("Your strength decreased by {}!").format(sd))
                elif r == 2:
                    self.you.speed -= sd
                    print(_("Your speed decreased by {}!").format(sd))
                elif r == 3:
                    self.you.defence -= sd
                    print(_("Your defence decreased by {}!").format(sd))
                else:
                    self.you.dexterity -= sd
                    print(_("Your dexterity decreased by {}!").format(sd))

            elif move.name == _("Smoke Bomb") and move.level >= 3:
                # r -= self.opp_blind_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .35) or (
                        move.level >= 5 and r <= .25) or (move.level >= 3 and r <= .15):
                    self.youBlindCount = 2
                    effects.append(_("Blind (2)"))
                    print(_("You have been temporarily blinded by the attack!"))

            elif move.name == _("Superior Smoke Bomb") and move.level >= 3:
                # r -= self.opp_blind_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .35) or (
                        move.level >= 5 and r <= .25) or (move.level >= 3 and r <= .15):
                    self.youBlindCount = 3
                    effects.append(_("Blind (3)"))
                    print(_("You have been temporarily blinded by the attack!"))

            elif move.name == _("Devil Crescent Moon Blade"):
                self.youBlindCount = 1
                self.youAcupunctureCount = 1
                effects.append(_("Seal (1)"))
                effects.append(_("Blind (1)"))
                print(_("Your acupuncture points have been sealed!"))
                print(_("You have been temporarily blinded by the attack!"))
                if move.level >= 10:
                    self.you.defence -= 10
                    self.you.dexterity -= 10
                    print(_("Your defence and dexterity decreased by 10!"))
                elif move.level >= 7:
                    self.you.defence -= 6
                    self.you.dexterity -= 6
                    print(_("Your defence and dexterity decreased by 6!"))
                elif move.level >= 4:
                    self.you.defence -= 4
                    self.you.dexterity -= 4
                    print(_("Your defence and dexterity decreased by 4!"))

                self.you.healthMax = max([0, self.you.healthMax - damage // 2])
                self.you.staminaMax = max([0, self.you.staminaMax - damage // 2])
                self.you.stamina = min([self.you.stamina, self.you.staminaMax])
                print(_("Your health upper limit was reduced to {} and your stamina upper limit reduced to {}!").format(
                    self.you.healthMax, self.you.staminaMax))


            elif move.name == _("Wild Wind Blade") and move.level >= 2:
                self.youBlindCount = 1
                effects.append(_("Blind (1)"))
                print(_("You have been temporarily blinded by the attack!"))

            elif move.name == _("Kendo"):
                if move.level >= 10:
                    self.opp.attack += 8
                    self.opp.strength += 8
                    print(_("Opponent's attack and strength increased by 8!"))
                elif move.level >= 5:
                    self.opp.attack += 4
                    self.opp.strength += 4
                    print(_("Opponent's attack and strength increased by 4!"))

            elif move.name == _("Hachiman Blade"):
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2) or (
                        move.level >= 3 and r <= .1):
                    effects.append(_("Bleeding"))
                    print(_("You're bleeding!"))

            elif move.name == _("Shaolin Luohan Fist"):
                if move.level >= 10:
                    self.gainStamina(self.opp, self.opp.staminaMax * .2)

                elif move.level >= 6:
                    self.gainStamina(self.opp, self.opp.staminaMax * .1)

                elif move.level >= 3:
                    self.gainStamina(self.opp, self.opp.staminaMax * .05)


            elif move.name == _("Sumeru Palm") and self.opp.stamina > 0:
                extra_stamina_used = min([self.opp.stamina, randrange(move.stamina//2, move.stamina+1)])
                extra_damage = int(extra_stamina_used*move.damage/move.stamina)
                self.opp.stamina -= extra_stamina_used
                self.you.health = int(self.you.health - extra_damage)
                print(_("Opponent spends an additional {} stamina to deal {} extra damage.").format(extra_stamina_used,
                                                                                              extra_damage))

                if self.you.health <= 0:
                    self.you.health = 0


            elif move.name == _("Hun Yuan Shapeshifting Fist"):
                hyyq_level = 0
                for s in self.opp.skills:
                    if s.name == _("Hun Yuan Yin Qi"):
                        hyyq_level = s.level
                        
                if hyyq_level > 0:
                    extra_damage = int((1+hyyq_level/10) * move.damage)
                    self.you.health = int(self.you.health - extra_damage)
                    if self.you.health <= 0:
                        self.you.health = 0


            elif move.name in [_("Taichi Fist"), _("Taichi Sword")]:
                if move.level >= 10:
                    self.opp.defence += 10
                    print(_("Opponent's defence increased by 10!"))

                elif move.level >= 7:
                    self.opp.defence += 6
                    print(_("Opponent's defence increased by 6!"))

                elif move.level >= 5:
                    self.opp.defence += 4
                    print(_("Opponent's defence increased by 4!"))

                elif move.level >= 3:
                    self.opp.defence += 2
                    print(_("Opponent's defence increased by 2!"))


            elif move.name in [_("Guan Yin Palm")]:
                if move.level >= 10:
                    self.opp.strength += 10
                    print(_("Opponent's strength increased by 10!"))

                elif move.level >= 8:
                    self.opp.strength += 8
                    print(_("Opponent's strength increased by 8!"))

                elif move.level >= 6:
                    self.opp.strength += 6
                    print(_("Opponent's strength increased by 6!"))

                elif move.level >= 4:
                    self.opp.strength += 4
                    print(_("Opponent's strength increased by 4!"))

                elif move.level >= 2:
                    self.opp.strength += 2
                    print(_("Opponent's strength increased by 2!"))


            elif move.name == _("Iron Sand Palm"):
                if move.level >= 10:
                    self.you.defence -= 10
                    print(_("Your defence decreased by 10!"))
                elif move.level >= 8:
                    self.you.defence -= 8
                    print(_("Your defence decreased by 8!"))
                elif move.level >= 6:
                    self.you.defence -= 6
                    print(_("Your defence decreased by 6!"))
                elif move.level >= 4:
                    self.you.defence -= 8
                    print(_("Your defence decreased by 4!"))
                elif move.level >= 2:
                    self.you.defence -= 4
                    print(_("Your defence decreased by 2!"))



            elif move.name in [_("Thousand Swords Technique")]:
                if move.level >= 10:
                    self.opp.attack += 10
                    print(_("Opponent's attack increased by 10!"))

                elif move.level >= 7:
                    self.opp.attack += 6
                    print(_("Opponent's attack increased by 6!"))

                elif move.level >= 5:
                    self.opp.attack += 4
                    print(_("Opponent's attack increased by 4!"))

                elif move.level >= 3:
                    self.opp.attack += 2
                    print(_("Opponent's attack increased by 2!"))


            elif move.name == _("Wood Combustion Blade"):
                if move.level >= 10:
                    self.opp.strength += 16
                    print(_("Opponent's strength increased by 16!"))
                elif move.level >= 5:
                    self.opp.strength += 8
                    print(_("Opponent's strength increased by 8!"))


            elif move.name in [_("Demon Suppressing Blade")]:
                if move.level >= 10:
                    self.gainStamina(self.you, -2 * damage)

                elif move.level >= 7:
                    self.gainStamina(self.you, -damage)

                elif move.level >= 5:
                    self.gainStamina(self.you, -damage // 2)

                elif move.level >= 3:
                    self.gainStamina(self.you, -damage // 4)


            elif move.name in [_("Whirlwind Staff")]:
                # r -= self.you_blind_bonus
                if move.level >= 10:
                    self.opp.attack += 6
                    self.opp.strength += 6
                    print(_("Opponent's attack and strength increased by 6!"))
                elif move.level >= 6:
                    self.opp.attack += 4
                    self.opp.strength += 4
                    print(_("Opponent's attack and strength increased by 4!"))
                elif move.level >= 2:
                    self.opp.attack += 2
                    self.opp.strength += 2
                    print(_("Opponent's attack and strength increased by 2!"))

                if (move.level >= 10 and r <= 1) or (move.level >= 8 and r <= .8) or (move.level >= 6 and r <= .6) or (
                        move.level >= 4 and r <= .4) or (move.level >= 2 and r <= .2):
                    self.youBlindCount = 1
                    effects.append(_("Blind (1)"))
                    print(_("You have been temporarily blinded by the attack!"))

            elif move.name == _("Scrub Fist"):
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2):
                    self.youActionPoints -= self.you.speed
                    print(_("You were stunned by the attack!"))

            elif move.name == _("Real Scrub Fist"):
                if (move.level >= 10 and r <= .6) or (move.level >= 7 and r <= .4) or (move.level >= 5 and r <= .3):
                    self.youActionPoints -= self.you.speed
                    print(_("You were stunned by the attack!"))

            elif move.name == _("Luohan Staff"):
                if (move.level >= 10 and r <= .6) or (move.level >= 7 and r <= .4) or (
                        move.level >= 5 and r <= .25) or (move.level >= 3 and r <= .1):
                    self.youActionPoints -= self.you.speed
                    print(_("You were stunned by the attack!"))

            elif move.name == _("Dragon Roar"):
                # r -= self.opp_internal_injury_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2) or (
                        move.level >= 3 and r <= .1):
                    effects.append(_("Internal Injury"))
                    print(_("You're suffering from internal injury as a result of the opponent's attack!"))
                self.you.defence -= 6
                print(_("Your defence decreased by 6."))

            elif move.name == _("Shanhu Fist"):
                # r -= self.opp_internal_injury_bonus
                if (move.level >= 10 and r <= .4) or (move.level >= 8 and r <= .3) or (
                        move.level >= 6 and r <= .25) or (move.level >= 4 and r <= .2) or (
                        move.level >= 2 and r <= .15):
                    self.youActionPoints -= self.you.speed
                    effects.append(_("Internal Injury"))
                    print(_("You're suffering from internal injury as a result of the opponent's attack!"))
                    print(_("You were stunned by the attack!"))

            elif move.name == _("Fire Palm"):
                # r -= self.opp_internal_injury_bonus
                if (move.level >= 10 and r <= .8) or (move.level >= 7 and r <= .6) or (move.level >= 5 and r <= .4) or (
                        move.level >= 3 and r <= .2):
                    effects.append(_("Internal Injury"))
                    print(_("You're suffering from internal injury as a result of the opponent's attack!"))

            elif move.name == _("Heart Shattering Fist"):
                if (move.level >= 10 and r <= .5) or (move.level >= 8 and r <= .4) or (move.level >= 6 and r <= .3) or (
                        move.level >= 4 and r <= .2) or (move.level >= 2 and r <= .1):
                    self.youActionPoints -= self.you.speed
                    print(_("You were stunned by the attack!"))

            elif move.name in [_("Ice Palm"), _("Ice Blade")]:
                # r -= self.you_internal_injury_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 8 and r <= .4) or (move.level >= 6 and r <= .3) or (
                        move.level >= 4 and r <= .2) or (move.level >= 2 and r <= .1):
                    effects.append(_("Internal Injury"))
                    print(_("You're suffering from internal injury as a result of the opponent's attack!"))
                if move.level >= 2:
                    self.you.speed -= move.level//2*2
                    print(_("Your speed was reduced by {}!").format(move.level))

            elif move.name == _("Ice Burst"):
                decrement = 0
                for m in self.opp.sml:
                    if m.name == _("Ice Palm"):
                        decrement = m.level * 2
                self.you.speed -= decrement
                if decrement > 0:
                    print(_("Your speed was reduced by {}!").format(decrement))

            elif move.name == _("Spider Poison Powder"):
                # r -= self.opp_poison_bonus
                if r <= .75 and _("Countering Poison With Poison") not in [s.name for s in self.you.skills]:
                    effects.append(_("Poison"))
                    print(_("You've been poisoned!"))

            elif move.name == _("Centipede Poison Powder"):
                self.gainStamina(self.you, -self.you.staminaMax // 2)
                
            elif move.name == _("Rainbow Fragrance"):
                self.gainStamina(self.you, -self.you.staminaMax // 5)
                self.you.attack -= 2
                self.you.strength -= 2
                self.you.speed -= 2
                self.you.defence -= 2
                self.you.dexterity -= 2
                self.youBlindCount = 1
                effects.append(_("Blind (1)"))
                print(_("You have been temporarily blinded by the attack! Your stamina is reduced by 20% and your stats are lowered by 2!"))

            elif move.name == _("Kiss of Death"):
                if (move.level >= 10) or (move.level >= 5 and r <= .5):
                    effects.append(_("Death"))
                    self.youDeathCount += 10

            elif move.name == _("Poison Needle") and _("Countering Poison With Poison") not in [s.name for s in
                                                                                                self.you.skills]:
                # r -= self.opp_poison_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 8 and r <= .4) or (move.level >= 6 and r <= .3) or (
                        move.level >= 4 and r <= .2) or (move.level >= 2 and r <= .1):
                    effects.append(_("Poison"))
                    print(_("You've been poisoned!"))

            elif move.name in [_("Phantom Claws")]:
                if move.level >= 10:
                    self.gainHealth(self.opp, int(damage * .2))
                    self.gainStamina(self.opp, int(damage * .2))

                elif move.level >= 7:
                    self.gainHealth(self.opp, int(damage * .15))
                    self.gainStamina(self.opp, int(damage * .15))

                elif move.level >= 5:
                    self.gainHealth(self.opp, int(damage * .1))
                    self.gainStamina(self.opp, int(damage * .1))

                elif move.level >= 3:
                    self.gainHealth(self.opp, int(damage * .05))
                    self.gainStamina(self.opp, int(damage * .05))

            elif move.name == _("Kusarigama"):
                if random() <= .5 and self.youWeaponDisabledCount == 0:
                    if move.level >= 10:
                        self.youWeaponDisabledCount = 5
                        print(_("Your weapon has been knocked out of your hands! You are unable to use armed attacks for 5 turns."))

                    elif move.level >= 7:
                        self.youWeaponDisabledCount = 3
                        print(_("Your weapon has been knocked out of your hands! You are unable to use armed attacks for 3 turns."))

                    elif move.level >= 5:
                        self.youWeaponDisabledCount = 2
                        print(_("Your weapon has been knocked out of your hands! You are unable to use armed attacks for 2 turns."))

                    elif move.level >= 3:
                        self.youWeaponDisabledCount = 1
                        print(_("Your weapon has been knocked out of your hands! You are unable to use armed attacks for 1 turn."))

            # Check for absorption or deflection or other reflective effects
            if _("Yin Yang Soul Absorption Technique") in [s.name for s in self.opp.skills]:
                self.gainHealth(self.opp, int(damage * .5))
                self.gainStamina(self.opp, int(damage * .5))

            if _("Divine Protection") in [s.name for s in self.you.skills]:
                if self.you.health > 0:
                    self.oppGotHit(damage // 4, None, False, True)
                    print(_("You deflected {} damage back to the opponent!").format(damage // 4))
            if _("Splinter Staff") in [eq.name for eq in self.equipment]:
                if self.you.health > 0:
                    self.oppGotHit(damage // 4, None, False, True)
                    print(_("Your Splinter Staff deflected {} damage back to the opponent!").format(damage // 4))


            # Apply effects
            if effects:
                self.apply_effects(effects, 0)
            # Check if weapons have effect
            self.check_weapon_effects(move, 0)

        self.callAfterStatChange()
        self.updateLabels()
        self.checkForDeath()


    def oppGotHit(self, damage, move, mute=False, deflected=False):

        if deflected:
            damage = int(damage)
            self.opp.health = int(self.opp.health - damage)
            if self.opp.health <= 0:
                self.opp.health = 0

        else:
            if move.name in [_("Violent Dragon Palm")]:
                self.youConsecutiveHits += 1
            else:
                self.youConsecutiveHits = 0

            if mute:
                pass
            else:
                self.currentEffect = move.sfx_file_name
                self.startSoundEffectThread()

            damage = int(damage)
            self.opp.health = int(self.opp.health - damage)
            if self.opp.health <= 0:
                self.opp.health = 0

            self.load_animation(move.name, move.animation_file_list, target=1)

            # Apply move effects
            effects = []
            r = random()
            r = r * (1 + self.opp_status_bonus)

            if move.name == _("Five Poison Palm") and _("Countering Poison With Poison") not in [s.name for s in
                                                                                                 self.opp.skills]:
                # r -= self.you_poison_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2) or (
                        move.level >= 3 and r <= .1):
                    effects.append(_("Poison"))
                    print(_("Opponent has been poisoned!"))

            elif move.name == _("Falling Cherry Blossom Finger Technique") and move.level >= 3:
                # r -= self.you_seal_bonus
                if (move.level >= 10 and r <= .4) or (move.level >= 7 and r <= .3) or (
                        move.level >= 5 and r <= .2) or (move.level >= 3 and r <= .1):
                    self.oppAcupunctureCount = 3
                    effects.append(_("Seal (3)"))
                    print(_("Opponent's acupuncture points have been sealed!"))

            elif move.name == _("Shaolin Diamond Finger") and move.level >= 3:
                # r -= self.you_seal_bonus
                if (move.level >= 10 and r <= 1) or (move.level >= 7 and r <= .6) or (move.level >= 5 and r <= .4) or (
                        move.level >= 3 and r <= .2):
                    self.oppAcupunctureCount = 1
                    effects.append(_("Seal (1)"))
                    print(_("Opponent's acupuncture points have been sealed!"))

            elif move.name == _("Lightning Finger Technique") and move.level >= 3:
                # r -= self.opp_seal_bonus
                if (move.level >= 10 and r <= .4) or (move.level >= 7 and r <= .25) or (
                        move.level >= 5 and r <= .15) or (move.level >= 3 and r <= .05):
                    self.oppAcupunctureCount = 1
                    effects.append(_("Seal (1)"))
                    self.oppActionPoints -= self.opp.speed
                    print(_("Opponent's acupuncture points have been sealed!"))
                    print(_("Opponent was stunned by the attack!"))
                    
            elif move.name == _("Fairy Plucking Flowers"):
                if (move.level >= 10 and r <= 1) or (move.level >= 8 and r <= .8) or (
                        move.level >= 6 and r <= .6) or (move.level >= 4 and r <= .4) or (move.level >= 2 and r <= .2):
                    self.oppAcupunctureCount = 1
                    effects.append(_("Seal (1)"))
                    print(_("The opponent's acupuncture points have been sealed!"))
                    self.pickpocket(self.opp.name)

            elif move.name == _("Cherry Blossom Drifting Snow Blade") and move.level >= 3:
                # r -= self.you_blind_bonus
                if (move.level >= 10 and r <= .4) or (move.level >= 7 and r <= .3) or (
                        move.level >= 5 and r <= .2) or (move.level >= 3 and r <= .1):
                    self.oppBlindCount = 3
                    effects.append(_("Blind (3)"))
                    print(_("Your opponent has been temporarily blinded by the attack!"))


            elif move.name == _("Babao Blade") and r <= .75 and move.level >= 3:
                # r -= self.opp_blind_bonus
                r_effect = randrange(1, move.level - 1)
                if r_effect == 1:
                    self.oppBlindCount = 1
                    effects.append(_("Blind (1)"))
                    print(_("The opponent has been temporarily blinded by the attack!"))
                elif r_effect == 2:
                    self.oppAcupunctureCount = 1
                    effects.append(_("Seal (1)"))
                    print(_("Your opponent's acupuncture points have been sealed!"))
                elif r_effect == 3:
                    effects.append(_("Internal Injury"))
                    print(_("The opponent is suffering from internal injury as a result of the your attack!"))
                elif r_effect == 4:
                    self.oppActionPoints -= self.opp.speed
                    print(_("The opponent was stunned by the attack!"))
                elif r_effect == 5:
                    self.opp.defence -= 20
                    print(_("Your opponent's defense decreased by 20!"))
                elif r_effect == 6:
                    self.opp.speed -= 20
                    print(_("Your opponent's speed decreased by 20!"))
                elif r_effect == 7:
                    self.oppBlindCount = 2
                    effects.append(_("Blind (2)"))
                    print(_("Your opponent has been temporarily blinded by the attack!"))
                else:
                    self.oppAcupunctureCount = 2
                    effects.append(_("Seal (2)"))
                    print(_("Your opponent's acupuncture points have been sealed!"))


            elif move.name == _("Shadow Blade"):
                if move.level >= 10:
                    self.opp.defence -= 10
                    self.opp.dexterity -= 10
                    print(_("The opponent's defence and dexterity decreased by 10!"))

                elif move.level >= 8:
                    self.opp.defence -= 8
                    self.opp.dexterity -= 8
                    print(_("The opponent's defence and dexterity decreased by 8!"))

                elif move.level >= 6:
                    self.opp.defence -= 6
                    self.opp.dexterity -= 6
                    print(_("The opponent's defence and dexterity decreased by 6!"))

                elif move.level >= 4:
                    self.opp.defence -= 4
                    self.opp.dexterity -= 4
                    print(_("The opponent's defence and dexterity decreased by 4!"))

                elif move.level >= 2:
                    self.opp.defence -= 2
                    self.opp.dexterity -= 2
                    print(_("The opponent's defence and dexterity decreased by 2!"))

            elif move.name == _("Eagle Claws"):
                if move.level >= 10:
                    self.opp.attack -= 3
                    self.opp.strength -= 3
                    self.opp.speed -= 3
                    self.opp.defence -= 3
                    self.opp.dexterity -= 3
                    print(_("All of your opponent's stats decreased by 3!"))

                elif move.level >= 5:
                    self.opp.attack -= 2
                    self.opp.strength -= 2
                    self.opp.speed -= 2
                    self.opp.defence -= 2
                    self.opp.dexterity -= 2
                    print(_("All of your opponent's stats decreased by 2!"))

            elif move.name == _("Serpent Whip") and move.level >= 3:
                if move.level >= 10:
                    sd = 12
                elif move.level >= 6:
                    sd = 8
                else:
                    sd = 4
                r = randrange(5)
                if r == 0:
                    self.opp.attack -= sd
                    print(_("Your opponent's attack decreased by {}!").format(sd))
                elif r == 1:
                    self.opp.strength -= sd
                    print(_("Your opponent's strength decreased by {}!").format(sd))
                elif r == 2:
                    self.opp.speed -= sd
                    print(_("Your opponent's speed decreased by {}!").format(sd))
                elif r == 3:
                    self.opp.defence -= sd
                    print(_("Your opponent's defence decreased by {}!").format(sd))
                else:
                    self.opp.dexterity -= sd
                    print(_("Your opponent's dexterity decreased by {}!").format(sd))

            elif move.name == _("Smoke Bomb") and move.level >= 3:
                # r -= self.you_blind_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .35) or (
                        move.level >= 5 and r <= .25) or (move.level >= 3 and r <= .15):
                    self.oppBlindCount = 2
                    effects.append(_("Blind (2)"))
                    print(_("The opponent has been temporarily blinded by the attack!"))

            elif move.name == _("Superior Smoke Bomb") and move.level >= 3:
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .35) or (
                        move.level >= 5 and r <= .25) or (move.level >= 3 and r <= .15):
                    self.oppBlindCount = 3
                    effects.append(_("Blind (3)"))
                    print(_("The opponent has been temporarily blinded by the attack!"))

            elif move.name == _("Devil Crescent Moon Blade"):
                self.oppBlindCount = 1
                self.oppAcupunctureCount = 1
                effects.append(_("Seal (1)"))
                effects.append(_("Blind (1)"))
                print(_("Your opponent's acupuncture points have been sealed!"))
                print(_("Your opponent has been temporarily blinded by the attack!"))
                if move.level >= 10:
                    self.opp.defence -= 10
                    self.opp.dexterity -= 10
                    print(_("Your opponent's defence and dexterity decreased by 10!"))
                elif move.level >= 7:
                    self.opp.defence -= 6
                    self.opp.dexterity -= 6
                    print(_("Your opponent's defence and dexterity decreased by 6!"))
                elif move.level >= 4:
                    self.opp.defence -= 4
                    self.opp.dexterity -= 4
                    print(_("Your opponent's defence and dexterity decreased by 4!"))

                self.opp.healthMax = max([0, self.opp.healthMax - damage // 2])
                self.opp.staminaMax = max([0, self.opp.staminaMax - damage // 2])
                self.opp.stamina = min([self.opp.stamina, self.opp.staminaMax])
                print(_(
                    "Your opponent's health upper limit was reduced to {} and stamina upper limit reduced to {}!").format(
                    self.opp.healthMax, self.opp.staminaMax))


            elif move.name == _("Wild Wind Blade") and move.level >= 2:
                self.oppBlindCount = 1
                effects.append(_("Blind (1)"))
                print(_("Your opponent has been temporarily blinded by the attack!"))

            elif move.name == _("Kendo"):
                if move.level >= 10:
                    self.you.attack += 8
                    self.you.strength += 8
                    print(_("Your attack and strength increased by 8!"))
                elif move.level >= 5:
                    self.you.attack += 4
                    self.you.strength += 4
                    print(_("Your attack and strength increased by 4!"))

            elif move.name == _("Hachiman Blade"):
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2) or (
                        move.level >= 3 and r <= .1):
                    effects.append(_("Bleeding"))
                    print(_("Your opponent is bleeding!"))

            elif move.name == _("Shaolin Luohan Fist"):
                if move.level >= 10:
                    self.gainStamina(self.you, self.you.staminaMax * .2)

                elif move.level >= 6:
                    self.gainStamina(self.you, self.you.staminaMax * .1)

                elif move.level >= 3:
                    self.gainStamina(self.you, self.you.staminaMax * .05)
                    
            elif move.name == _("Sumeru Palm") and self.you.stamina > 0:
                extra_stamina_used = min([self.you.stamina, randrange(move.stamina//2, move.stamina+1)])
                extra_damage = int(extra_stamina_used*move.damage/move.stamina)
                self.you.stamina -= extra_stamina_used
                self.opp.health = int(self.opp.health - extra_damage)
                print(_("You spend an additional {} stamina to deal {} extra damage.").format(extra_stamina_used,
                                                                                              extra_damage))
                if self.opp.health <= 0:
                    self.opp.health = 0


            elif move.name == _("Hun Yuan Shapeshifting Fist"):
                hyyq_level = 0
                for s in self.you.skills:
                    if s.name == _("Hun Yuan Yin Qi"):
                        hyyq_level = s.level

                if hyyq_level > 0:
                    extra_damage = int((1 + hyyq_level / 10) * move.damage)
                    self.opp.health = int(self.opp.health - extra_damage)
                    if self.opp.health <= 0:
                        self.opp.health = 0


            elif move.name in [_("Taichi Fist"), _("Taichi Sword")]:
                if move.level >= 10:
                    self.you.defence += 10
                    print(_("Your defence increased by 10!"))

                elif move.level >= 7:
                    self.you.defence += 6
                    print(_("Your defence increased by 6!"))

                elif move.level >= 5:
                    self.you.defence += 4
                    print(_("Your defence increased by 4!"))

                elif move.level >= 3:
                    self.you.defence += 2
                    print(_("Your defence increased by 2!"))

            elif move.name in [_("Guan Yin Palm")]:
                if move.level >= 10:
                    self.you.strength += 10
                    print(_("Your strength increased by 10!"))

                elif move.level >= 8:
                    self.you.strength += 8
                    print(_("Your strength increased by 8!"))

                elif move.level >= 6:
                    self.you.strength += 6
                    print(_("Your strength increased by 6!"))

                elif move.level >= 4:
                    self.you.strength += 4
                    print(_("Your strength increased by 4!"))

                elif move.level >= 2:
                    self.you.strength += 2
                    print(_("Your strength increased by 2!"))


            elif move.name == _("Iron Sand Palm"):
                if move.level >= 10:
                    self.opp.defence -= 10
                    print(_("Opponent's defence decreased by 10!"))
                elif move.level >= 8:
                    self.opp.defence -= 8
                    print(_("Opponent's defence decreased by 8!"))
                elif move.level >= 6:
                    self.opp.defence -= 6
                    print(_("Opponent's defence decreased by 6!"))
                elif move.level >= 4:
                    self.opp.defence -= 8
                    print(_("Opponent's defence decreased by 4!"))
                elif move.level >= 2:
                    self.opp.defence -= 4
                    print(_("Opponent's defence decreased by 2!"))


            elif move.name in [_("Thousand Swords Technique")]:
                if move.level >= 10:
                    self.you.attack += 10
                    print(_("Your attack increased by 10!"))

                elif move.level >= 7:
                    self.you.attack += 6
                    print(_("Your attack increased by 6!"))

                elif move.level >= 5:
                    self.you.attack += 4
                    print(_("Your attack increased by 4!"))

                elif move.level >= 3:
                    self.you.attack += 2
                    print(_("Your attack increased by 2!"))


            elif move.name == _("Wood Combustion Blade"):
                if move.level >= 10:
                    self.you.strength += 16
                    print(_("Your strength increased by 16!"))
                elif move.level >= 5:
                    self.you.strength += 8
                    print(_("Your strength increased by 8!"))


            elif move.name in [_("Demon Suppressing Blade")]:
                if move.level >= 10:
                    self.gainStamina(self.opp, -2 * damage)

                elif move.level >= 7:
                    self.gainStamina(self.opp, -damage)

                elif move.level >= 5:
                    self.gainStamina(self.opp, -damage // 2)

                elif move.level >= 3:
                    self.gainStamina(self.opp, -damage // 4)


            elif move.name in [_("Whirlwind Staff")]:
                # r -= self.you_blind_bonus
                if move.level >= 10:
                    self.you.attack += 6
                    self.you.strength += 6
                    print(_("Your attack and strength increased by 6!"))
                elif move.level >= 6:
                    self.you.attack += 4
                    self.you.strength += 4
                    print(_("Your attack and strength increased by 4!"))
                elif move.level >= 2:
                    self.you.attack += 2
                    self.you.strength += 2
                    print(_("Your attack and strength increased by 2!"))

                if (move.level >= 10 and r <= 1) or (move.level >= 8 and r <= .8) or (move.level >= 6 and r <= .6) or (
                        move.level >= 4 and r <= .4) or (move.level >= 2 and r <= .2):
                    self.oppBlindCount = 1
                    effects.append(_("Blind (1)"))
                    print(_("The opponent has been temporarily blinded by the attack!"))

            elif move.name == _("Scrub Fist"):
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2):
                    self.oppActionPoints -= self.opp.speed
                    print(_("Opponent was stunned by the attack!"))

            elif move.name == _("Real Scrub Fist"):
                if (move.level >= 10 and r <= .6) or (move.level >= 7 and r <= .4) or (move.level >= 5 and r <= .3):
                    self.oppActionPoints -= self.opp.speed
                    print(_("Opponent was stunned by the attack!"))

            elif move.name == _("Luohan Staff"):
                if (move.level >= 10 and r <= .6) or (move.level >= 7 and r <= .4) or (
                        move.level >= 5 and r <= .25) or (move.level >= 3 and r <= .1):
                    self.oppActionPoints -= self.opp.speed
                    print(_("Opponent was stunned by the attack!"))

            elif move.name == _("Shanhu Fist"):
                # r -= self.you_internal_injury_bonus
                if (move.level >= 10 and r <= .4) or (move.level >= 8 and r <= .3) or (
                        move.level >= 6 and r <= .25) or (move.level >= 4 and r <= .2) or (
                        move.level >= 2 and r <= .15):
                    self.oppActionPoints -= self.opp.speed
                    effects.append(_("Internal Injury"))
                    print(_("The opponent is suffering from internal injury as a result of your attack!"))
                    print(_("Opponent was stunned by the attack!"))

            elif move.name == _("Dragon Roar"):
                # r -= self.you_internal_injury_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2) or (
                        move.level >= 3 and r <= .1):
                    effects.append(_("Internal Injury"))
                    print(_("The opponent is suffering from internal injury as a result of your attack!"))
                self.opp.defence -= 6
                print(_("The opponent's defence decreased by 6."))

            elif move.name == _("Fire Palm"):
                # r -= self.you_internal_injury_bonus
                if (move.level >= 10 and r <= .8) or (move.level >= 7 and r <= .6) or (move.level >= 5 and r <= .4) or (
                        move.level >= 3 and r <= .2):
                    effects.append(_("Internal Injury"))
                    print(_("The opponent is suffering from internal injury as a result of your attack!"))

            elif move.name == _("Heart Shattering Fist"):
                if (move.level >= 10 and r <= .5) or (move.level >= 8 and r <= .4) or (move.level >= 6 and r <= .3) or (
                        move.level >= 4 and r <= .2) or (move.level >= 2 and r <= .1):
                    self.oppActionPoints -= self.opp.speed
                    print(_("Opponent was stunned by the attack!"))

            elif move.name in [_("Ice Palm"), _("Ice Blade")]:
                # r -= self.you_internal_injury_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 8 and r <= .4) or (move.level >= 6 and r <= .3) or (
                        move.level >= 4 and r <= .2) or (move.level >= 2 and r <= .1):
                    effects.append(_("Internal Injury"))
                    print(_("The opponent is suffering from internal injury as a result of your attack!"))
                if move.level >= 2:
                    self.opp.speed -= move.level//2*2
                    print(_("The opponent's speed was reduced by {}!").format(move.level))

            elif move.name == _("Ice Burst"):
                decrement = 0
                for m in self.sml:
                    if m.name == _("Ice Palm"):
                        decrement = m.level * 2
                self.opp.speed -= decrement
                if decrement > 0:
                    print(_("Opponent's speed was reduced by {}!").format(decrement))

            elif move.name == _("Spider Poison Powder"):
                # r -= self.you_poison_bonus
                if r <= .75 and _("Countering Poison With Poison") not in [s.name for s in self.opp.skills]:
                    effects.append(_("Poison"))
                    print(_("The opponent has been poisoned!"))

            elif move.name == _("Centipede Poison Powder"):
                self.gainStamina(self.opp, -self.opp.staminaMax // 2)

            elif move.name == _("Rainbow Fragrance"):
                self.gainStamina(self.opp, -self.opp.staminaMax // 5)
                self.opp.attack -= 2
                self.opp.strength -= 2
                self.opp.speed -= 2
                self.opp.defence -= 2
                self.opp.dexterity -= 2
                self.oppBlindCount = 1
                effects.append(_("Blind (1)"))
                print(_("Opponent has been temporarily blinded by the attack! Opponent's stamina is reduced by 20% and stats are lowered by 2!"))

            elif move.name == _("Kiss of Death"):
                if (move.level >= 10) or (move.level >= 5 and r <= .5):
                    effects.append(_("Death"))
                    self.oppDeathCount += 10

            elif move.name == _("Poison Needle") and _("Countering Poison With Poison") not in [s.name for s in
                                                                                                self.opp.skills]:
                # r -= self.you_poison_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 8 and r <= .4) or (move.level >= 6 and r <= .3) or (
                        move.level >= 4 and r <= .2) or (move.level >= 2 and r <= .1):
                    effects.append(_("Poison"))
                    print(_("The opponent has been poisoned!"))

            elif move.name in [_("Phantom Claws")]:
                if move.level >= 10:
                    self.gainHealth(self.you, int(damage * .2))
                    self.gainStamina(self.you, int(damage * .2))

                elif move.level >= 7:
                    self.gainHealth(self.you, int(damage * .15))
                    self.gainStamina(self.you, int(damage * .15))

                elif move.level >= 5:
                    self.gainHealth(self.you, int(damage * .1))
                    self.gainStamina(self.you, int(damage * .1))

                elif move.level >= 3:
                    self.gainHealth(self.you, int(damage * .05))
                    self.gainStamina(self.you, int(damage * .05))

            # Check for absorption or deflection
            if _("Yin Yang Soul Absorption Technique") in [s.name for s in self.skills]:
                self.gainHealth(self.you, int(damage * .5))
                self.gainStamina(self.you, int(damage * .5))

            if _("Divine Protection") in [s.name for s in self.opp.skills]:
                if self.opp.health > 0:
                    self.youGotHit(damage // 4, None, False, True)
                    print(_("Opponent deflected {} damage back to you!").format(damage // 4))

            if _("Splinter Staff") in [eq.name for eq in self.opp.equipmentList]:
                if self.opp.health > 0:
                    self.youGotHit(damage // 4, None, False, True)
                    print(_("Opponent's Splinter Staff deflected {} damage back to you!").format(damage // 4))

            if _("Seductive Dance") in [s.name for s in self.opp.skills]:
                self.you.attack -= 3
                self.you.strength -= 3
                self.you.speed -= 3
                self.you.defence -= 3
                self.you.dexterity -= 3
                self.youUseStamina(int(self.you.staminaMax*.1))
                print(_("All your stats are reduced by 3 from the effects of 'Seductive Dance'. You lose 10% stamina."))

            # Apply effects
            if effects:
                self.apply_effects(effects, 1)
            # Check if weapons have effect
            self.check_weapon_effects(move, 1)

        self.callAfterStatChange()
        self.updateLabels()
        self.checkForDeath()


    def apply_effects(self, effects, targ):
        if targ == 0:
            if self.you.status == _("Normal"):
                self.you.status = ""
            for effect in effects:
                if effect == _("Poison") and _("Poison") in self.you.status:
                    if _("Poison++") not in self.you.status:
                        self.you.status = self.you.status.replace(_("Poison"), _("Poison+"))
                elif effect == _("Internal Injury") and _("Internal Injury") in self.you.status:
                    if _("Internal Injury++") not in self.you.status:
                        self.you.status = self.you.status.replace(_("Internal Injury"), _("Internal Injury+"))
                elif _("Seal") in effect and _("Seal") in self.you.status and self.youAcupunctureCount > 0:
                    self.you.status = re.sub(_("Seal \(\d\)"), effect, self.you.status)
                elif _("Blind") in effect and _("Blind") in self.you.status and self.youBlindCount > 0:
                    self.you.status = re.sub(_("Blind \(\d\)"), effect, self.you.status)
                elif effect == _("Death"):
                    if effect in self.you.status:
                        self.you.status = re.sub(_("Death \(\d+\%\)"), "Death ({}%)".format(self.youDeathCount), self.you.status)
                    else:
                        self.you.status += "\n" + " " + "Death (10%)"
                elif effect not in self.you.status:
                    if self.you.status == "":
                        self.you.status += "\n" + " " + effect
                    else:
                        if effect == _("Internal Injury"):
                            self.you.status = effect + "\n" + self.you.status
                        else:
                            first_line = self.you.status.split("\n")[0]
                            self.you.status += "\n" + " " * (18 - len(first_line)) + effect
            self.you.status = self.you.status.strip()  # remove extra spaces

        else:
            if self.opp.status == _("Normal"):
                self.opp.status = ""
            print(effects)
            for effect in effects:
                if effect == _("Poison") and _("Poison") in self.opp.status:
                    if _("Poison++") not in self.opp.status:
                        self.opp.status = self.opp.status.replace(_("Poison"), _("Poison+"))
                elif effect == _("Internal Injury") and _("Internal Injury") in self.opp.status:
                    if _("Internal Injury++") not in self.opp.status:
                        self.opp.status = self.opp.status.replace(_("Internal Injury"), _("Internal Injury+"))
                elif _("Seal") in effect and _("Seal") in self.opp.status and self.oppAcupunctureCount > 0:
                    self.opp.status = re.sub(_("Seal \(\d\)"), effect, self.opp.status)
                elif _("Blind") in effect and _("Blind") in self.opp.status and self.oppBlindCount > 0:
                    self.opp.status = re.sub(_("Blind \(\d\)"), effect, self.opp.status)
                elif effect == _("Death"):
                    if effect in self.opp.status:
                        self.opp.status = re.sub(_("Death \(\d+\%\)"), "Death (" + str(self.oppDeathCount) + "%)", self.opp.status)
                    else:
                        self.opp.status += "\n" + " " + "Death (10%)"
                elif effect not in self.opp.status:
                    if self.opp.status == "":
                        self.opp.status += "\n" + " " + effect
                    else:
                        if effect == _("Internal Injury"):
                            self.opp.status = effect + "\n" + self.opp.status
                        else:
                            first_line = self.opp.status.split("\n")[0]
                            self.opp.status += "\n" + " " * (18 - len(first_line)) + effect

            self.opp.status = self.opp.status.strip()  # remove extra spaces


    def check_weapon_effects(self, move, targ):
        effects = []
        if targ == 0:
            for eq in self.opp.equipmentList:
                if eq.slot == "hands_right":
                    attack_type = _("Armed")
                elif eq.slot == "hands_left":
                    attack_type = _("Unarmed")
                else:
                    attack_type = move.type

                r = random()
                r = r * (1 + self.you_status_bonus)
                if eq.effect and attack_type == move.type and eq.effect not in effects:
                    if eq.effect in [_("Seal"), ("Blind")]:
                        if eq.effect not in self.you.status:
                            if r <= eq.effect_prob:
                                effects.append(eq.effect + " (1)")
                                if eq.effect == _("Seal"):
                                    self.youAcupunctureCount = 1
                                elif eq.effect == _("Blind"):
                                    self.youBlindCount = 1

                    elif eq.effect in [_("Poison"), _("Internal Injury")]:
                        if eq.effect == _("Poison") and _("Countering Poison With Poison") in [s.name for s in self.you.skills]:
                            pass
                        else:
                            if r <= eq.effect_prob:
                                effects.append(eq.effect)

            if effects:
                self.apply_effects(effects, 0)

        else:
            for eq in self.you.equipmentList:
                if eq.slot == "hands_right":
                    attack_type = _("Armed")
                elif eq.slot == "hands_left":
                    attack_type = _("Unarmed")
                else:
                    attack_type = move.type

                r = random()
                r = r * (1 + self.opp_status_bonus)
                if eq.effect and attack_type == move.type and eq.effect not in effects:
                    if eq.effect in [_("Seal"), ("Blind")]:
                        if eq.effect not in self.opp.status:
                            if r <= eq.effect_prob:
                                effects.append(eq.effect + " (1)")
                                if eq.effect == _("Seal"):
                                    self.oppAcupunctureCount = 1
                                elif eq.effect == _("Blind"):
                                    self.oppBlindCount = 1

                    elif eq.effect in [_("Poison"), _("Internal Injury")]:
                        if eq.effect == _("Poison") and _("Countering Poison With Poison") in [s.name for s in self.opp.skills]:
                            pass
                        else:
                            if r <= eq.effect_prob:
                                effects.append(eq.effect)

                    elif eq.effect == _("Disable"):
                        if random() <= eq.effect_prob:
                            self.disable_random_move(self.opp)

            if effects:
                self.apply_effects(effects, 1)


    def disable_random_move(self, targ_obj):
        move_pool = [move for move in targ_obj.sml if move.name != _("Rest")]
        if move_pool:
            random_move = pickOne(move_pool)
            targ_obj.sml.remove(random_move)
            print(_("{}'s move: '{}' was disabled!").format(targ_obj.name, random_move.name))


    def gainHealth(self, person, gain):

        if person == self.you:

            gain = int(gain)
            if self.you.health + gain >= self.you.healthMax:
                gain = int(self.you.healthMax - self.you.health)
            self.you.health += gain
            self.updateLabels()
            print("{} recovers {} health.\n".format(self.you.name, gain))

        else:

            gain = int(gain)
            if self.opp.health + gain >= self.opp.healthMax:
                gain = int(self.opp.healthMax - self.opp.health)
            self.opp.health += gain
            self.updateLabels()
            print("{} recovers {} health.\n".format(self.opp.name, gain))


    def youUseStamina(self, loss):

        self.you.stamina -= loss
        if self.you.stamina <= 0:
            self.you.stamina = 0
        self.updateLabels()


    def oppUseStamina(self, loss):

        self.opp.stamina -= loss
        if self.opp.stamina <= 0:
            self.opp.stamina = 0
        self.updateLabels()


    def gainStamina(self, person, gain):

        if person == self.you:

            gain = int(gain)
            if gain >= 0:
                if self.you.stamina + gain >= self.you.staminaMax:
                    gain = int(self.you.staminaMax - self.you.stamina)
                print("{} recovers {} stamina.\n".format(self.you.name, gain))

            else:
                if self.you.stamina + gain < 0:
                    gain = int(0-self.you.stamina)
                print("{} loses {} stamina.\n".format(self.you.name, -gain))

            self.you.stamina += gain

        else:
            gain = int(gain)
            if gain >= 0:
                if self.opp.stamina + gain >= self.opp.staminaMax:
                    gain = int(self.opp.staminaMax - self.opp.stamina)
                print("{} recovers {} stamina.\n".format(self.opp.name, gain))

            else:
                if self.opp.stamina + gain < 0:
                    gain = int(0 - self.opp.stamina)
                print("{} loses {} stamina.\n".format(self.opp.name, -gain))

            self.opp.stamina += gain

        self.updateLabels()


    def updateStats(self, person, datk=0, dstr=0, dspd=0, ddef=0, ddex=0):

        person.attack += datk
        person.strength += dstr
        person.speed += dspd
        person.defence += ddef
        person.dexterity += ddex

        if person.attack <= 0:
            person.attack = 0

        if person.strength <= 0:
            person.strength = 0

        if person.speed <= 0:
            person.speed = 0

        if person.defence <= 0:
            person.defence = 0

        if person.dexterity <= 0:
            person.dexterity = 0

        self.callAfterStatChange()

        self.updateLabels()


    def callAfterStatChange(self):

        if self.you.attack <= 10:
            self.you.attack = 10
        if self.you.strength <= 10:
            self.you.strength = 10
        if self.you.speed <= 10:
            self.you.speed = 10
        if self.you.defence <= 10:
            self.you.defence = 10
        if self.you.dexterity <= 10:
            self.you.dexterity = 10
        if self.opp.attack <= 10:
            self.opp.attack = 10
        if self.opp.strength <= 10:
            self.opp.strength = 10
        if self.opp.speed <= 10:
            self.opp.speed = 10
        if self.opp.defence <= 10:
            self.opp.defence = 10
        if self.opp.dexterity <= 10:
            self.opp.dexterity = 10

        self.accuracy = (self.you.attack - self.opp.dexterity + 80) / 100
        self.you_stamina_damage_bonus = 1.075**(self.you.staminaMax/500)*1.05**(-self.opp.staminaMax/500)
        self.damage_scale = (self.you.strength**.85/self.opp.defence**.85)*self.you_stamina_damage_bonus
        if _("Taichi 18 Forms") in [s.name for s in self.skills]:
            self.damage_scale *= 1+self.you.defence/200

        self.oppaccuracy = (self.opp.attack - self.you.dexterity + 80) / 100
        self.opp_stamina_damage_bonus = 1.075**(self.opp.staminaMax/500)*1.05**(-self.you.staminaMax/500)
        self.oppdamage_scale = (self.opp.strength**.85/self.you.defence**.85)*self.opp_stamina_damage_bonus
        if _("Taichi 18 Forms") in [s.name for s in self.opp.skills]:
            self.oppdamage_scale *= 1+self.opp.defence/200

        if self.accuracy <= .3:
            self.accuracy = .3

        if self.damage_scale <= .2:
            self.damage_scale = .2

        if self.oppaccuracy <= .3:
            self.oppaccuracy = .3

        if self.oppdamage_scale <= .2:
            self.oppdamage_scale = .2

        if self.youBlindCount > 0:
            self.accuracy *= .5

        if self.oppBlindCount > 0:
            self.oppaccuracy *= .5


    def move_buffer_zone(self, move):

        if self.you.stamina >= move.stamina:

            self.allOff(self.animation_buttons)

            # print("You Action Pts", self.youActionPoints)
            # print("Opp Action Pts", self.oppActionPoints)
            # print("\n")

            if self.youActionPoints < self.oppActionPoints:
                self.youActionPoints += self.you.speed
                self.oppChooseMove()
            else:
                self.oppActionPoints += self.opp.speed
                self.oppHasMoved = True
                self.you_move(move)
                if self.youAcupunctureCount > 0 and move.stamina == 0 and move.name not in [_("Items")]:
                    self.youAcupunctureCount -= 1
                    self.you.status = self.you.status.replace(_("Seal ({})").format(self.youAcupunctureCount + 1),
                                                              _("Seal ({})").format(self.youAcupunctureCount))
                    self.you.status = self.you.status.replace(_("Seal (0)"), "")
                    self.you.status = self.you.status.strip()

                    if self.you.status == "":
                        self.you.status = _("Normal")

                    self.updateLabels()

                if self.youBlindCount > 0 and move.name not in [_("Items")]:
                    self.youBlindCount -= 1
                    self.you.status = self.you.status.replace(_("Blind ({})").format(self.youBlindCount + 1),
                                                              _("Blind ({})").format(self.youBlindCount))
                    self.you.status = self.you.status.replace(_("Blind (0)"), "")
                    self.you.status = self.you.status.strip()

                    if self.you.status == "":
                        self.you.status = _("Normal")

                    self.updateLabels()
                    
                if self.youWeaponDisabledCount > 0 and move.name not in [_("Items")]:
                    self.youWeaponDisabledCount -= 1

            if self.youHaveMoved and self.oppHasMoved:
                self.endTurn()
            else:
                # print("Initializing next move thread")
                if not self.battleEnded:
                    t = Thread(target=self.move_buffer_zone, args=(move,))
                    self.battleWin.after(1800, t.start)

        else:
            if not self.autoBattle:
                messagebox.showinfo("", _("Not enough stamina to use this move."))
            self.endTurn()


    def you_move(self, move):
        if self.battleEnded:
            return

        self.youUseStamina(move.stamina)
        self.updateLabels()
        move.gain_exp(move.exp_rate)
        self.update_move_button_tooltips(move)

        if move.name == _("Flee"):
            self.flee(targ=0)
        elif move.name == _("Rest"):
            self.rest(self.you)
        elif move.name == _("Items"):
            self.battleWin.after(100, self.checkInv, self.battleWin, "battle")
            self.youActionPoints += self.opp.speed
        elif move.name == _("Body Cleansing Technique"):
            self.body_cleansing_technique(targ=0)
            print(_("You use 'Body Cleansing Technique' to restore your status back to 'Normal'."))
        elif move.name == _("Soothing Song"):
            self.soothing_song(targ=0)
        else:
            if move.name in [_("Spider Poison Powder"), _("Centipede Poison Powder"), _("Smoke Bomb"), _("Rainbow Fragrance")]:
                self.inv[move.name] -= 1
                item_index = list(self.inv.keys()).index(move.name)
                try:
                    self.item_quantity_labels[item_index].config(text="x" + str(self.inv[move.name]))
                except:
                    pass
            self.execute_attack(move, 1)


        self.youHaveMoved = True


    def execute_attack(self, move, targ):
        accuracy_multiplier = 1.0

        if move.name == _("Basic Sword Technique"):
            if move.level >= 10:
                accuracy_multiplier *= 1.4
            elif move.level >= 6:
                accuracy_multiplier *= 1.2
            elif move.level >= 3:
                accuracy_multiplier *= 1.1

        elif move.name == _("Huashan Sword Technique"):
            if move.level >= 10:
                accuracy_multiplier *= 1.5
            elif move.level >= 8:
                accuracy_multiplier *= 1.4
            elif move.level >= 6:
                accuracy_multiplier *= 1.3
            elif move.level >= 4:
                accuracy_multiplier *= 1.2
            elif move.level >= 2:
                accuracy_multiplier *= 1.1

        elif move.name == _("Thousand Swords Technique"):
            if move.level >= 10:
                accuracy_multiplier *= 2
            elif move.level >= 7:
                accuracy_multiplier *= 1.7
            elif move.level >= 5:
                accuracy_multiplier *= 1.5
            elif move.level >= 3:
                accuracy_multiplier *= 1.3

        elif move.name == _("Sumeru Palm"):
            if move.level >= 10:
                accuracy_multiplier *= 1.5
            elif move.level >= 5:
                accuracy_multiplier *= 1.3
            elif move.level >= 3:
                accuracy_multiplier *= 1.2

        elif move.name == _("Prajna Palm"):
            if move.level >= 10:
                accuracy_multiplier *= 2
            elif move.level >= 7:
                accuracy_multiplier *= 1.7
            elif move.level >= 5:
                accuracy_multiplier *= 1.5
            elif move.level >= 3:
                accuracy_multiplier *= 1.3

        elif move.name in [_("Dragon Roar"), _("Kendo")]:
            accuracy_multiplier *= 100

        self.type_bonus = 1
        self.opp_type_bonus = 1
        self.type_bonus_count = 0
        r = random()

        if targ == 1:
            move_damage = int(move.damage * self.damage_scale)
            
            if self.playAsOther:
                for m in self.you.sml:
                    if move.type == m.type:
                        self.type_bonus_count += m.level
                self.type_bonus += .06*self.type_bonus_count
            else:
                for m in self.sml:
                    if move.type == m.type:
                        self.type_bonus_count += m.level
                self.type_bonus += .03*self.type_bonus_count

            if move.type == "Armed":
                self.type_bonus += self.you_armed_equipment_bonus
            elif move.type == "Unarmed":
                self.type_bonus += self.you_unarmed_equipment_bonus
            move_accuracy = self.accuracy * 100 * (1+ (self.type_bonus-1)/3)
            move_damage = int(move_damage*self.type_bonus)


            #Moves with critical hit probability
            if move.name == _("Blind Sniper Blade"):
                r -= self.you_critical_hit_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 8 and r <= .4) or (move.level >= 6 and r <= .3) or (
                        move.level >= 4 and r <= .2) or (move.level >= 2 and r <= .1):
                    move_damage *= 2

            elif move.name == _("Empty Force Fist"):
                r -= self.you_critical_hit_bonus
                if (move.level >= 10 and r <= .75) or (move.level >= 7 and r <= .5) or (move.level >= 5 and r <= .3) or (
                        move.level >= 3 and r <= .15):
                    move_damage *= 2

            elif move.name == _("Thunder Palm") and _("Seal") in self.opp.status:
                if move.level >= 10:
                    move_damage *= 3.5
                elif move.level >= 7:
                    move_damage *= 2.5
                elif move.level >= 5:
                    move_damage *= 2
                elif move.level >= 3:
                    move_damage *= 1.5
                move_damage = int(move_damage)

            elif r <= self.you_critical_hit_bonus:
                move_damage *= 2

            r = random()
            #Other effects
            if move.name in [_("Poison Needle")]:
                self.youActionPoints += self.you.speed//4

            elif move.name in [_("Wild Wind Blade")]:
                if move.level >= 10:
                    self.youActionPoints += int(self.you.speed*.4)
                elif move.level >= 8:
                    self.youActionPoints += int(self.you.speed * .3)
                elif move.level >= 6:
                    self.youActionPoints += int(self.you.speed * .25)
                elif move.level >= 4:
                    self.youActionPoints += int(self.you.speed * .2)
                elif move.level >= 2:
                    self.youActionPoints += int(self.you.speed * .15)

            elif move.name in [_("Sumeru Palm")]: #75% extra action point cost
                self.youActionPoints -= int(self.you.speed*.75)

            elif move.name == _("Purity Blade"):
                if self.chivalry < 0:
                    move_damage = 0
                else:
                    move_damage = int(move_damage*(1 + self.chivalry/100))
                if (move.level >= 10 and r <= 1) or (move.level >= 7 and r <= .6) or (move.level >= 5 and r <= .4) or (
                        move.level >= 3 and r <= .2):
                    self.body_cleansing_technique(targ=0, animation=False)
                    print(_("Your status is restored to 'Normal' due to effects of Purity Blade."))

            elif move.name == _("Devil Crescent Moon Blade"):
                if self.chivalry >= 0:
                    move_damage = 0
                else:
                    move_damage = int(move_damage*(1 - self.chivalry/100))

            elif move.name == _("Violent Dragon Palm"):
                #print(self.youConsecutiveHits)
                move_damage = int(move_damage*((1+move.level/50)**self.youConsecutiveHits))

            elif move.name == _("Fairy Sword Technique"):
                self.gainHealth(self.you, .03*self.you.healthMax)
                ddex = 0
                if move.level >= 10:
                    ddex = 5
                elif move.level >= 8:
                    ddex = 4
                elif move.level >= 6:
                    ddex = 3
                elif move.level >= 4:
                    ddex = 2
                elif move.level >= 2:
                    ddex = 1

                self.you.dexterity += ddex
                print(_("You recover some health and your dexterity increases by {}.").format(ddex))

            elif move.name == _("Prajna Palm"):
                if move.level >= 10:
                    dstamina = .1
                elif move.level >= 7:
                    dstamina = .07
                elif move.level >= 5:
                    dstamina = .05
                elif move.level >= 3:
                    dstamina = .03
                else:
                    dstamina = 0

                if dstamina > 0:
                    self.gainStamina(self.you, int(dstamina*self.you.staminaMax))
                    print(_("You recover {} stamina.").format(int(dstamina*self.you.staminaMax)))
                

            if move.name == _("Yunhuan Taichi Fist"):
                print(_("You try to use Yunhuan Taichi Fist but end up hurting yourself instead!"))
                self.youGotHit(move_damage, move)
            elif move_accuracy * accuracy_multiplier >= randrange(1, 101):
                print(_("You used {}, dealing {} damage to the opponent!").format(move.name, move_damage))
                self.oppGotHit(move_damage, move)
            else:
                self.currentEffect = pickOne(["sfx_attack_missed.mp3", "sfx_attack_missed2.mp3"])
                self.startSoundEffectThread()
                print(_("You used {}, but the opponent dodged it!").format(move.name))
                self.load_animation("Dodge",
                                    ["a_dodge_{}1.gif".format(self.opp.name), "a_dodge_{}2.gif".format(self.opp.name),
                                     "a_dodge_{}3.gif".format(self.opp.name), "a_dodge_{}4.gif".format(self.opp.name),
                                     "a_dodge_{}5.gif".format(self.opp.name), "a_dodge_{}6.gif".format(self.opp.name),
                                     "a_dodge_{}7.gif".format(self.opp.name),
                                     "a_dodge_{}1.gif".format(self.opp.name), ],
                                    target=1)
                self.youConsecutiveHits = 0

        else:
            move_damage = int(move.damage * self.oppdamage_scale)
            for m in self.opp.sml:
                if move.type == m.type:
                    self.type_bonus_count += m.level
            self.opp_type_bonus += .06*self.type_bonus_count

            if move.type == "Armed":
                self.opp_type_bonus += self.opp_armed_equipment_bonus
            elif move.type == "Unarmed":
                self.opp_type_bonus += self.opp_unarmed_equipment_bonus

            move_accuracy = self.oppaccuracy * 100 * (1+ (self.opp_type_bonus-1)/3)
            move_damage = int(move_damage * self.opp_type_bonus)

            if move.name == _("Blind Sniper Blade"):
                r -= self.opp_critical_hit_bonus
                if (move.level >= 10 and r <= .5) or (move.level >= 8 and r <= .4) or (move.level >= 6 and r <= .3) or (
                        move.level >= 4 and r <= .2) or (move.level >= 2 and r <= .1):
                    move_damage *= 2

            elif move.name == _("Empty Force Fist"):
                r -= self.opp_critical_hit_bonus
                if (move.level >= 10 and r <= .75) or (move.level >= 7 and r <= .5) or (move.level >= 5 and r <= .3) or (
                        move.level >= 3 and r <= .15):
                    move_damage *= 2

            elif move.name == _("Thunder Palm") and _("Seal") in self.you.status:
                if move.level >= 10:
                    move_damage *= 3.5
                elif move.level >= 7:
                    move_damage *= 2.5
                elif move.level >= 5:
                    move_damage *= 2
                elif move.level >= 3:
                    move_damage *= 1.5
                move_damage = int(move_damage)

            elif r <= self.opp_critical_hit_bonus:
                move_damage *= 2

            r = random()

            if move.name in [_("Poison Needle"), _("Archery")]:
                self.oppActionPoints += self.opp.speed//4
                
            elif move.name in [_("Crossbow")]:
                self.oppActionPoints += self.opp.speed//2

            elif move.name in [_("Sumeru Palm")]: #75% extra action point cost
                self.oppActionPoints -= int(self.opp.speed*.75)

            elif move.name in [_("Wild Wind Blade")]:
                if move.level >= 10:
                    self.oppActionPoints += int(self.opp.speed*.4)
                elif move.level >= 8:
                    self.oppActionPoints += int(self.opp.speed * .3)
                elif move.level >= 6:
                    self.oppActionPoints += int(self.opp.speed * .25)
                elif move.level >= 4:
                    self.oppActionPoints += int(self.opp.speed * .2)
                elif move.level >= 2:
                    self.oppActionPoints += int(self.opp.speed * .15)

            elif move.name == _("Purity Blade"):
                move_damage *= 2
                if (move.level >= 10 and r <= 1) or (move.level >= 7 and r <= .6) or (move.level >= 5 and r <= .4) or (
                        move.level >= 3 and r <= .2):
                    self.body_cleansing_technique(targ=1, animation=False)
                    print(_("Opponent's status is restored to 'Normal' due to effects of Purity Blade."))

            elif move.name == _("Devil Crescent Moon Blade"):
                move_damage *= 3

            elif move.name == _("Violent Dragon Palm"):
                move_damage = int(move_damage*((1+move.level/50)**self.oppConsecutiveHits))

            elif move.name == _("Fairy Sword Technique"):
                self.gainHealth(self.opp, .03*self.opp.healthMax)
                ddex = 0
                if move.level >= 10:
                    ddex = 5
                elif move.level >= 8:
                    ddex = 4
                elif move.level >= 6:
                    ddex = 3
                elif move.level >= 4:
                    ddex = 2
                elif move.level >= 2:
                    ddex = 1

                self.opp.dexterity += ddex
                print(_("The opponent recovers some health and their dexterity increases by {}.").format(ddex))

            elif move.name == _("Prajna Palm"):
                if move.level >= 10:
                    dstamina = .1
                elif move.level >= 7:
                    dstamina = .07
                elif move.level >= 5:
                    dstamina = .05
                elif move.level >= 3:
                    dstamina = .03
                else:
                    dstamina = 0

                if dstamina > 0:
                    self.gainStamina(self.opp, int(dstamina * self.you.staminaMax))
                    print(_("Opponent recovers {} stamina.").format(int(dstamina * self.you.staminaMax)))


            if move_accuracy * accuracy_multiplier >= randrange(1, 101):
                print(_("{} used {} and deals {} damage to you!").format(self.opp.name, move.name, move_damage))
                self.youGotHit(move_damage, move)
            else:
                self.currentEffect = pickOne(["sfx_attack_missed.mp3", "sfx_attack_missed2.mp3"])
                self.startSoundEffectThread()
                print(_("{} used {}, but you dodged it!").format(self.opp.name, move.name))
                if self.you.name == self.character:
                    self.load_animation("Dodge",
                                        ["a_dodge_you7.gif", "a_dodge_you6.gif", "a_dodge_you5.gif",
                                         "a_dodge_you4.gif", "a_dodge_you3.gif", "a_dodge_you2.gif", "a_dodge_you1.gif"],
                                        target=0)
                else:
                    self.load_animation("Dodge",
                                        ["a_dodge_{}{}.gif".format(self.you.name, i) for i in range(7,0,-1)],
                                        target=0)
                self.oppConsecutiveHits = 0


    def body_cleansing_technique(self, targ, animation=True):

        self.currentEffect = "sfx_restore_status.mp3"
        self.startSoundEffectThread()

        if targ == 0:
            self.you.status = 'Normal'
            self.youBlindCount = 0
            self.youDeathCount = 0
            self.youAcupunctureCount = 0
            if animation:
                self.load_animation("Body Cleansing Technique", ["a_body_cleansing_technique{}.png".format(i) for i in range(1, 31)], target=0)

        else:
            self.opp.status = 'Normal'
            self.oppBlindCount = 0
            self.oppDeathCount = 0
            self.oppAcupunctureCount = 0
            if animation:
                self.load_animation("Body Cleansing Technique", ["a_body_cleansing_technique{}.png".format(i) for i in range(1, 31)], target=1)

        self.updateLabels()


    def soothing_song(self, targ, animation=True):

        if targ == 0:
            self.restore(self.you.healthMax//10, self.you.staminaMax//10, full=False, targ=targ, reason="battle")
            if animation:
                self.load_animation("Rest", ["a_rest{}.gif".format(i) for i in range(1,20)], target=0)
            if random() <= .4:
                self.you.status = 'Normal'
                self.youBlindCount = 0
                self.youDeathCount = 0
                self.youAcupunctureCount = 0
                self.currentEffect = "sfx_restore_status.mp3"
                self.startSoundEffectThread()

        else:
            self.restore(self.opp.healthMax//10, self.opp.staminaMax//10, full=False, targ=targ, reason="battle")
            if animation:
                self.load_animation("Rest", ["a_rest{}.gif".format(i) for i in range(1, 20)], target=1)
            if random() <= .4:
                self.opp.status = 'Normal'
                self.oppBlindCount = 0
                self.oppDeathCount = 0
                self.oppAcupunctureCount = 0
                self.currentEffect = "sfx_restore_status.mp3"
                self.startSoundEffectThread()

        self.updateLabels()
        

    def rest(self, targ_obj):

        self.currentEffect = "sfx_recover.mp3"
        self.startSoundEffectThread()

        if targ_obj == self.you:
            self.load_animation("Rest", ["a_rest{}.gif".format(i) for i in range(1,20)], target=0)
        else:
            self.load_animation("Rest", ["a_rest{}.gif".format(i) for i in range(1, 20)], target=1)
        
        rHealth = int(targ_obj.healthMax * (targ_obj.health_recovery))
        rStamina = int(targ_obj.staminaMax * (targ_obj.stamina_recovery))
        self.gainHealth(targ_obj, rHealth)
        self.gainStamina(targ_obj, rStamina)
        self.updateLabels()


    def flee(self, targ=0, useSmokeBomb=False):

        if targ:
            self.health = self.you.health
            self.stamina = self.you.stamina
            self.you.status = _("Normal")
            self.status = self.you.status
            self.battleID = None
            self.battleEnded = True
            print(_("The opponent fled from battle..."))
            self.battleFromWin.deiconify()
            self.battleWin.quit()
            self.battleWin.destroy()
            self.stopSoundtrack()

            self.currentBGM = self.postBattleSoundtrack
            self.startSoundtrackThread()

            return None

        else:
            if self.battleType not in ("training", "normal", "huashanlunjian"):
                print(_("Can't escape from this battle!\n"))
                return None

            if self.oppHasMoved:

                successRate = .3 + .5 * (self.you.level - self.opp.level) / min(
                    [self.you.level, self.opp.level]) + .5 * (
                                      self.you.speed - self.opp.speed) / min([self.you.speed, self.opp.speed])

                if successRate >= 1:
                    successRate = 1
                if successRate <= .1:
                    successRate = .1
                # print(successRate)

                if useSmokeBomb:
                    successRate = 1

                if random() <= successRate:
                    self.health = self.you.health
                    self.stamina = self.you.stamina
                    self.you.status = _("Normal")
                    self.status = self.you.status
                    self.battleID = None
                    self.battleEnded = True
                    print(_("{} successfully fled from battle.\n").format(self.character))
                    self.battleFromWin.deiconify()
                    self.battleWin.destroy()
                    self.stopSoundtrack()

                    self.currentBGM = self.postBattleSoundtrack
                    self.startSoundtrackThread()

                    return None


                else:
                    print(_("You fail to escape!\n"))

                self.youHaveMoved = True

                if not self.oppHasMoved:
                    self.oppChooseMove()

            else:
                self.bufferedMove = self.flee
                self.oppChooseMove()


    def hensojutsu(self, animations):

        self.currentEffect = "sfx_hensojutsu.mp3"
        self.startSoundEffectThread()

        print(_("The opponent used 'Hensojutsu' and copied one of your stats."))
        self.load_animation("Hensojutsu", animations, target=1)
        self.updateLabels()
        

    def oppChooseMove(self, priority=None):

        if self.opp.health <= 0:
            return None

        move = priority

        try:
            minStaminaRequired = min([move.stamina for move in self.opp.sml])
        except:
            # print("No special moves")
            minStaminaRequired = 999999999

        if self.oppAcupunctureCount > 0:
            minStaminaRequired = 999999999


        if priority == None:

            itemCount = len(self.opp.itemList)
            conditional_moves = [_("Body Cleansing Technique"), _("Soothing Song")]

            if itemCount == 0 or self.opp.healthMax - self.opp.health < 50:
                if self.opp.stamina <= self.opp.staminaMax / 2 and random() <= .5:
                    self.rest(self.opp)
                    self.oppHasMoved = True
                elif self.opp.name == _("Su Ling") and self.opp.health <= self.opp.healthMax / 2 and random() <= .5:
                    self.rest(self.opp)
                    self.oppHasMoved = True
                elif self.opp.stamina >= minStaminaRequired:
                    move = pickOne([m for m in self.opp.sml
                                    if (m.stamina <= self.opp.stamina) and not (m.name in conditional_moves
                                                                                and self.opp.status == _("Normal"))])
                    if move.name == _("Body Cleansing Technique"):
                        self.body_cleansing_technique(targ=1)
                        print(_("Opponent uses 'Body Cleansing Technique' to restore their status back to 'Normal'."))
                    elif move.name == _("Soothing Song"):
                        self.soothing_song(targ=1)
                    elif move.name == _("Flee"):
                        self.flee(targ=1)
                    elif move.name == _("Hensojutsu"):
                        if self.you.health - self.opp.health >= 1000 and self.opp.healthMax - self.opp.health >= 1000:
                            self.gainHealth(self.opp, self.you.health - self.opp.health)
                        elif self.you.attack - self.opp.attack >= 40:
                            self.opp.attack = self.you.attack
                        elif self.you.strength - self.opp.strength >= 40:
                            self.opp.strength = self.you.strength
                        elif self.you.speed - self.opp.speed >= 40:
                            self.opp.speed = self.you.speed
                        elif self.you.defence - self.opp.defence >= 40:
                            self.opp.defence = self.you.defence
                        elif self.you.dexterity - self.opp.dexterity >= 40:
                            self.opp.dexterity = self.you.dexterity
                        else:
                            self.oppChooseMove()
                            return
                        self.hensojutsu(move.animation_file_list)
                    else:
                        self.opp_move(move)
                else:
                    self.rest(self.opp)
                    self.oppHasMoved = True


            elif itemCount > 0:
                if self.opp.stamina >= minStaminaRequired:
                    r = randrange(len(self.opp.sml) + 1)
                    if r == len(self.opp.sml):
                        self.opp_use_item()
                        self.oppHasMoved = True

                    else:
                        if self.opp.stamina <= self.opp.staminaMax / 2 and random() <= .5:
                            self.rest(self.opp)
                            self.oppHasMoved = True
                        else:
                            move = pickOne([m for m in self.opp.sml
                                            if (m.stamina <= self.opp.stamina) and
                                            not (m.name == _("Body Cleansing Technique")
                                                 and self.opp.status == _("Normal"))])

                            if move.name == _("Body Cleansing Technique"):
                                self.body_cleansing_technique(targ=1)
                                print(_("Opponent uses 'Body Cleansing Technique' to restore their status back to 'Normal'."))
                            elif move.name == _("Soothing Song"):
                                self.soothing_song(targ=1)
                            elif move.name == _("Flee"):
                                self.flee(targ=1)
                            elif move.name == _("Hensojutsu"):
                                if self.you.health - self.opp.health >= 1000 and self.opp.healthMax - self.opp.health >= 1000:
                                    self.gainHealth(self.opp, self.you.health - self.opp.health)
                                elif self.you.attack - self.opp.attack >= 40:
                                    self.opp.attack = self.you.attack
                                elif self.you.strength - self.opp.strength >= 40:
                                    self.opp.strength = self.you.strength
                                elif self.you.speed - self.opp.speed >= 40:
                                    self.opp.speed = self.you.speed
                                elif self.you.defence - self.opp.defence >= 40:
                                    self.opp.defence = self.you.defence
                                elif self.you.dexterity - self.opp.dexterity >= 40:
                                    self.opp.dexterity = self.you.dexterity
                                else:
                                    self.oppChooseMove()
                                    return
                                self.hensojutsu(move.animation_file_list)
                            else:
                                self.opp_move(move)

                else:
                    self.rest(self.opp)
                    self.oppHasMoved = True

        if self.oppAcupunctureCount > 0:
            self.oppAcupunctureCount -= 1
            self.opp.status = self.opp.status.replace(_("Seal ({})").format(self.oppAcupunctureCount + 1),
                                                      _("Seal ({})").format(self.oppAcupunctureCount))
            self.opp.status = self.opp.status.replace(_("Seal (0)"), "")
            self.opp.status = self.opp.status.strip()

            if self.opp.status == "":
                self.opp.status = _("Normal")

            self.updateLabels()

        if self.oppBlindCount > 0:
            self.oppBlindCount -= 1
            self.opp.status = self.opp.status.replace(_("Blind ({})").format(self.oppBlindCount + 1),
                                                      _("Blind ({})").format(self.oppBlindCount))
            self.opp.status = self.opp.status.replace(_("Blind (0)"), "")
            self.opp.status = self.opp.status.strip()

            if self.opp.status == "":
                self.opp.status = _("Normal")

            self.updateLabels()


    def opp_move(self, move):
        try:
            self.oppUseStamina(move.stamina)
            self.updateLabels()
            self.execute_attack(move, targ=0)
            self.oppHasMoved = True
        except:
            print("Opp move aborted...")

    def opp_use_item(self):
        pass

    def allOff(self, button_list):
        try:
            for button in button_list:
                button.config(state=DISABLED)
        except Exception as e:
            pass

    def allOn(self, button_list):
        if self.autoBattle:
            return
        try:
            self.allOff(self.animation_buttons)
            if self.youAcupunctureCount > 0:
                if self.playAsOther:
                    for button in button_list[-1:]:
                        button.config(state=NORMAL)
                else:
                    for button in button_list[-3:]:
                        button.config(state=NORMAL)
                for button_i in range(len(button_list)):
                    button = button_list[button_i]
                    if self.youWeaponDisabledCount == 0:
                        if self.you.sml[button_i].stamina == 0 and self.you.sml[button_i].type != _("Hidden Weapon"):
                            button.config(state=NORMAL)
                    else:
                        if self.you.sml[button_i].stamina == 0 and self.you.sml[button_i].type != _("Hidden Weapon") and self.you.sml[button_i].type != _("Armed"):
                            button.config(state=NORMAL)
            else:
                for button_i in range(len(button_list)):
                    button = button_list[button_i]
                    move = self.you.sml[button_i]

                    if self.youWeaponDisabledCount == 0:
                        if move.stamina <= self.you.stamina:
                            button.config(state=NORMAL)
                    else:
                        if move.stamina <= self.you.stamina and move.type != _("Armed"):
                            button.config(state=NORMAL)

                    if move.name == _("Spider Poison Powder"):
                        if not self.check_for_item(_("Spider Poison Powder")):
                            button.config(state=DISABLED)

                    if move.name == _("Centipede Poison Powder"):
                        if not self.check_for_item(_("Centipede Poison Powder")):
                            button.config(state=DISABLED)
                            
                    if move.name == _("Rainbow Fragrance"):
                        if not self.check_for_item(_("Rainbow Fragrance")):
                            button.config(state=DISABLED)

                    if move.name in [_("Smoke Bomb"), _("Superior Smoke Bomb")]:
                        if _("Smoke Bomb") not in self.inv:
                            button.config(state=DISABLED)
                        elif self.inv[_("Smoke Bomb")] < 1:
                            button.config(state=DISABLED)
                            
                    if move.type == _("Armed") and not self.hasWeaponEquipped:
                        button.config(state=DISABLED)

            if self.battleType not in ("training", "normal", "huashanlunjian"):
                for button_i in range(len(button_list)):
                    button = button_list[button_i]
                    move = self.you.sml[button_i]
                    if move.name == _("Flee"):
                        button.config(state=DISABLED)

            if self.battleID == "shinscrub":
                for button_i in range(len(button_list)):
                    button = button_list[button_i]
                    move = self.you.sml[button_i]
                    if move.name == _("Items"):
                        button.config(state=DISABLED)

        except Exception as e:
            pass


    def auto_select_player_move(self):

        ineligible_moves = [_("Spider Poison Powder"), _("Centipede Poison Powder"), _("Rainbow Fragrance"),
                            _("Items"), _("Flee"), _("Smoke Bomb"), _("Superior Smoke Bomb"), _("Rest")]
        eligible_moves = [move for move in self.you.sml if move.name not in ineligible_moves and move.stamina <= self.you.stamina]
        
        rest_move = [move for move in self.you.sml if move.name == _("Rest")][0]

        if len(eligible_moves) == 0 or self.youAcupunctureCount > 0 or self.you.stamina/self.you.staminaMax <= .1:
            self.move_buffer_zone(rest_move)
        elif eligible_moves:
            random_move = pickOne(eligible_moves)
            self.move_buffer_zone(random_move)
        else:
            self.move_buffer_zone(rest_move)

            


#sys.setrecursionlimit(1200)
tkWin = Tk()
g = game(tkWin)
tkWin.mainloop()