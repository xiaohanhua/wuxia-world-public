from tk_tooltip import *
import tkinter.messagebox as messagebox
from random import *
from datetime import *
from copy import copy
from functools import partial
from threading import Thread
from gettext import gettext
from maze_generator import *
import pickle
import subprocess
import os
import ctypes
import time
import Pmw

_ = gettext
RESOURCES_DIR = "Resources"
SAVE_SLOTS_DIR = "SaveSlots"
os.chdir(RESOURCES_DIR)

if os.name == "nt":
    from playsound import playsound
    from pygame import mixer
    import mutagen.mp3


def pickOne(aList):
    l = len(aList)
    i = randrange(l)
    return aList[i]


def isPrime(number):
    count = 0
    if number <= 1:
        return False
    for i in range(2, number):
        test = number % i
        if test == 0:
            count += 1
    if count < 1:
        return True
    else:
        return False


class game:

    def __init__(self, win):

        if os.name == "nt":
            win.option_add("*Label.Font", "TkDefaultFont 11")
            win.option_add("*Button.Font", "TkDefaultFont 11")

        self.can_view_map = True
        self.currentBGM = "jy_intro.mp3"
        self.soundtrackOn = IntVar()
        self.soundtrackOn.set(1)
        self.soundEffectOn = IntVar()
        self.soundEffectOn.set(1)
        self.startSoundtrackThread()
        self.balloon = Pmw.Balloon()

        self.levelThresholds = [200, 400, 700, 1300, 2000, 3000, 4200, 5600, 7200, 9200,
                                12000, 15500, 20000, 26000, 35000, 48000, 65000, 88000, 120000, 160000,
                                210000, 272000, 350000, 450000, 600000, 999999999]

        ITEM_DESCRIPTIONS = {
            _("金钱"): _("可以用来买东西"),
            _("螃蟹"): _("美味的螃蟹！\n\n恢复50气血, 50内力"),
            _("鱼"): _("但愿我不会被卡住喉咙...\n\n恢复30气血, 30内力"),
            _("汤"): _("一碗热腾腾的汤！\n\n恢复70气血"),
            _("包子"): _("猪肉包, 我的最爱！\n\n恢复100内力"),
            _("樱花阵阵图"): _("有了这个图,我再也不会被困于樱花阵内了!"),
            _("樱花木瓜银耳汤"): _("特殊原料制作的鲜汤\n\n恢复15%气血, 20%内力"),
            _("毒蜈蚣"): _("即使是此生物的微量毒液也具有致命性."),
            _("毒蜘蛛"): _("即使是此生物的微量毒液也具有致命性."),
            _("蛇心"): _("新鲜的蛇心\n\n气血上限+10"),
            _("熊胆"): _("新鲜的熊胆\n\n内力上限+15"),
            _("毒蛾1"): _("五大毒蛾之一, 光是看上一眼就令人觉得恐怖."),
            _("毒蛾2"): _("五大毒蛾之一, 光是看上一眼就令人觉得恐怖."),
            _("毒蛾3"): _("五大毒蛾之一, 光是看上一眼就令人觉得恐怖."),
            _("毒蛾4"): _("五大毒蛾之一, 光是看上一眼就令人觉得恐怖."),
            _("毒蛾5"): _("五大毒蛾之一, 光是看上一眼就令人觉得恐怖."),
            _("金丝手套"): _("护手的好道具!\n\n使用拳法，掌法，指法时+25%伤害加成"),

        }


        self.battleID = None  # determines whether there is a reward at end of battle
        self.saveSlot = None

        self.mainWin = win
        self.mainWin.title(_("Scrub世界"))
        if os.name == "nt":
            self.mainWin.tk.call('wm', 'iconphoto', self.mainWin._w, PhotoImage(file=r'game_main_icon.ico'))
        else:
            self.mainWin.tk.call('wm', 'iconphoto', self.mainWin._w, PhotoImage(file=r'game_main_icon.png'))

        bg = PhotoImage(file="wuxia_world.png", master=self.mainWin)
        w = bg.width()
        h = bg.height()

        panel = Label(self.mainWin, image=bg)
        panel.pack(expand="yes")
        panel.image = bg

        Button(self.mainWin, text=_("开始游戏"), command=self.startNewGame).pack()
        Button(self.mainWin, text=_("读档"), command=self.continueGame).pack()

        self.mainWin.mainloop()

    ################### HELPER FUNCTIONS ###################
    ################### HELPER FUNCTIONS ###################
    ################### HELPER FUNCTIONS ###################
    ################### HELPER FUNCTIONS ###################
    ################### HELPER FUNCTIONS ###################

    def on_exit(self):
        pass

    def terminate_thread(self, thread, delay=0):

        if delay > 0:
            time.sleep(delay)

        if os.name == "nt":
            mixer.music.stop()

        else:
            processID = self.process.pid

            try:
                os.kill(processID, 0)
                self.process.kill()
            except:
                print("Terminated gracefully")

            if self.followupProcess != None:
                followupID = self.followupProcess.pid

                try:
                    os.kill(followupID, 0)
                    self.followupProcess.kill()
                except:
                    pass

            if not thread.isAlive():
                return

            exc = ctypes.py_object(SystemExit)
            res = ctypes.pythonapi.PyThreadState_SetAsyncExc(
                ctypes.c_long(thread.ident), exc)
            if res == 0:
                raise ValueError("nonexistent thread id")
            elif res > 1:
                # """if it returns a number greater than one, you're in trouble,
                # and you should call it again with exc=NULL to revert the effect"""
                ctypes.pythonapi.PyThreadState_SetAsyncExc(thread.ident, None)
                raise SystemError("PyThreadState_SetAsyncExc failed")

            self.terminate_thread(self.stopThread)

    def startSoundtrackThread(self, delay=0, loop=False, followup=None, followupDelay=0):
        if not self.soundtrackOn.get():
            return
        # self.loopSoundtracks = True
        self.soundtrackThread = Thread(target=self.playSoundtrack, args=(delay, loop, followup, followupDelay))
        self.soundtrackThread.start()

    def playSoundtrack(self, delay=0, loop=False, followup=None, followupDelay=0):

        if delay > 0:
            time.sleep(delay)

        if os.name == "nt":
            # time.sleep(.5)
            # self.currentBGM = self.currentBGM.replace("mp3", "wav")
            # self.currentBGM = self.currentBGM.replace("m4a", "wav")
            # flags = winsound.SND_FILENAME | winsound.SND_ASYNC | winsound.SND_LOOP
            # winsound.PlaySound(self.currentBGM, flags)
            freq = mutagen.mp3.MP3(self.currentBGM).info.sample_rate
            mixer.init()
            mixer.quit()
            mixer.init(frequency=freq)
            mixer.music.load(self.currentBGM)
            mixer.music.play(5)

        else:

            self.process = subprocess.Popen(["afplay", self.currentBGM])
            self.followupProcess = None

            if followup != None:
                if followupDelay > 0:
                    time.sleep(followupDelay)
                self.followupProcess = subprocess.Popen(["afplay", followup])

    def stopSoundtrack(self, delay=0):

        self.stopThread = Thread(target=self.terminate_thread, args=(self.soundtrackThread, delay))
        self.stopThread.start()

    def startSoundEffectThread(self, delay=0, loop=False):
        if not self.soundEffectOn.get():
            return

        self.soundEffectThread = Thread(target=self.playSoundEffect, args=(delay, loop))
        self.soundEffectThread.start()

    def playSoundEffect(self, delay, loop):

        if delay > 0:
            time.sleep(delay)

        if os.name == "nt":
            playsound(self.currentEffect)
        else:
            self.process2 = subprocess.Popen(["afplay", self.currentEffect])

    def stopSoundEffect(self):

        self.terminate_thread(self.soundEffectThread)

    def winSwitch(self, fromWin, toWin, destroy=True):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        try:
            if fromWin == self.scrubHouseWin and toWin == self.mainWin:
                response = messagebox.askquestion("", _("You are about to head back to the main menu. All unsaved progress will be lost. Proceed to main menu?"))
                if response == "yes":
                    pass
                else:
                    return
        except:
            pass


        try:
            if fromWin == "":
                self.stopSoundtrack()
                self.currentBGM = "scapeSoft.mp3"
                self.startSoundtrackThread()

        except:
            pass

        if destroy:
            fromWin.destroy()
        else:
            fromWin.withdraw()
        toWin.deiconify()

    def loadGame(self, saveSlot):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        self.saveSlot = saveSlot
        if self.saveSlot == 1 and self.slot1Character == "":
            messagebox.showinfo("", _("此档案是空的."))
            return

        if self.saveSlot == 2 and self.slot2Character == "":
            messagebox.showinfo("", _("此档案是空的."))
            return

        if self.saveSlot == 3 and self.slot3Character == "":
            messagebox.showinfo("", _("此档案是空的."))
            return

        if self.saveSlot == 4 and self.slot4Character == "":
            messagebox.showinfo("", _("此档案是空的."))
            return

        if self.saveSlot == 5 and self.slot5Character == "":
            messagebox.showinfo("", _("此档案是空的."))
            return

        if self.saveSlot == 6 and self.slot6Character == "":
            messagebox.showinfo("", _("此档案是空的."))
            return

        self.character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-{}.p".format(self.saveSlot)), "rb"))

        self.exp = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "exp-{}.p".format(self.saveSlot)), "rb"))
        self.level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-{}.p".format(self.saveSlot)), "rb"))
        self.upgradePoints = pickle.load(
            open(os.path.join(SAVE_SLOTS_DIR, "upgradePoints-{}.p".format(self.saveSlot)), "rb"))
        self.perception = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "perception-{}.p".format(self.saveSlot)), "rb"))
        self.luck = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "luck-{}.p".format(self.saveSlot)), "rb"))
        self.chivalry = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "chivalry-{}.p".format(self.saveSlot)), "rb"))

        self.status = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "status-{}.p".format(self.saveSlot)), "rb"))
        self.gameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-{}.p".format(self.saveSlot)), "rb"))
        self.taskProgressDic = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "taskProgressDic-{}.p".format(self.saveSlot)), "rb"))

        self.sml = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "sml-{}.p".format(self.saveSlot)), "rb"))
        self.skills = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "skills-{}.p".format(self.saveSlot)), "rb"))
        self.inv = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "inv-{}.p".format(self.saveSlot)), "rb"))

        self.you = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "you-{}.p".format(self.saveSlot)), "rb"))

        self.health = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "health-{}.p".format(self.saveSlot)), "rb"))
        self.healthMax = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "healthMax-{}.p".format(self.saveSlot)), "rb"))
        self.stamina = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "stamina-{}.p".format(self.saveSlot)), "rb"))
        self.staminaMax = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "staminaMax-{}.p".format(self.saveSlot)), "rb"))

        self.attack = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "attack-{}.p".format(self.saveSlot)), "rb"))
        self.strength = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "strength-{}.p".format(self.saveSlot)), "rb"))
        self.speed = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "speed-{}.p".format(self.saveSlot)), "rb"))
        self.defence = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "defence-{}.p".format(self.saveSlot)), "rb"))
        self.dexterity = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "dexterity-{}.p".format(self.saveSlot)), "rb"))

        self.continueGameWin.withdraw()
        self.generateGameWin()

    def saveGame(self):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        dd = datetime.now() - self.gameInitiatedTimestamp
        self.gameClock += dd.seconds

        pickle.dump(self.character, open(os.path.join(SAVE_SLOTS_DIR, "character-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.health, open(os.path.join(SAVE_SLOTS_DIR, "health-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.healthMax, open(os.path.join(SAVE_SLOTS_DIR, "healthMax-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.stamina, open(os.path.join(SAVE_SLOTS_DIR, "stamina-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.staminaMax, open(os.path.join(SAVE_SLOTS_DIR, "staminaMax-{}.p".format(self.saveSlot)), "wb"))

        pickle.dump(self.attack, open(os.path.join(SAVE_SLOTS_DIR, "attack-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.strength, open(os.path.join(SAVE_SLOTS_DIR, "strength-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.speed, open(os.path.join(SAVE_SLOTS_DIR, "speed-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.defence, open(os.path.join(SAVE_SLOTS_DIR, "defence-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.dexterity, open(os.path.join(SAVE_SLOTS_DIR, "dexterity-{}.p".format(self.saveSlot)), "wb"))

        pickle.dump(self.exp, open(os.path.join(SAVE_SLOTS_DIR, "exp-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.level, open(os.path.join(SAVE_SLOTS_DIR, "level-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.perception, open(os.path.join(SAVE_SLOTS_DIR, "perception-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.luck, open(os.path.join(SAVE_SLOTS_DIR, "luck-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.chivalry, open(os.path.join(SAVE_SLOTS_DIR, "chivalry-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.upgradePoints,
                    open(os.path.join(SAVE_SLOTS_DIR, "upgradePoints-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.status, open(os.path.join(SAVE_SLOTS_DIR, "status-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.gameClock, open(os.path.join(SAVE_SLOTS_DIR, "gameClock-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.taskProgressDic,
                    open(os.path.join(SAVE_SLOTS_DIR, "taskProgressDic-{}.p".format(self.saveSlot)), "wb"))

        pickle.dump(self.sml, open(os.path.join(SAVE_SLOTS_DIR, "sml-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.skills, open(os.path.join(SAVE_SLOTS_DIR, "skills-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.inv, open(os.path.join(SAVE_SLOTS_DIR, "inv-{}.p".format(self.saveSlot)), "wb"))
        pickle.dump(self.you, open(os.path.join(SAVE_SLOTS_DIR, "you-{}.p".format(self.saveSlot)), "wb"))

        messagebox.showinfo(_("系统信息"), _("已存档"))

    ################### GAME ###################
    ################### GAME ###################
    ################### GAME ###################
    ################### GAME ###################
    ################### GAME ###################

    def startNewGame(self):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        self.mainWin.withdraw()
        self.startNewGameWin = Toplevel(self.mainWin)
        self.startNewGameWin.title(_("开始游戏"))

        try:
            self.slot1Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-1.p"), "rb"))
            self.slot1Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-1.p"), "rb"))
        except:
            self.slot1Character = ""

        try:
            self.slot2Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-2.p"), "rb"))
            self.slot2Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-2.p"), "rb"))
        except:
            self.slot2Character = ""

        try:
            self.slot3Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-3.p"), "rb"))
            self.slot3Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-3.p"), "rb"))
        except:
            self.slot3Character = ""

        try:
            self.slot4Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-4.p"), "rb"))
            self.slot4Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-4.p"), "rb"))
        except:
            self.slot4Character = ""

        try:
            self.slot5Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-5.p"), "rb"))
            self.slot5Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-5.p"), "rb"))
        except:
            self.slot5Character = ""

        try:
            self.slot6Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-6.p"), "rb"))
            self.slot6Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-6.p"), "rb"))
        except:
            self.slot6Character = ""

        Label(self.startNewGameWin, text=_("请选择档案开始新游戏.")).grid(row=0, column=1)

        F1 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F1.grid(row=1, column=0)

        F2 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F2.grid(row=1, column=1)

        F3 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F3.grid(row=1, column=2)

        Label(self.startNewGameWin, text="").grid(row=2, column=1)

        F4 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F4.grid(row=3, column=0)

        F5 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F5.grid(row=3, column=1)

        F6 = Frame(self.startNewGameWin, borderwidth=2, relief=SOLID)
        F6.grid(row=3, column=2)


        Button(self.startNewGameWin, text=_("返回"),
               command=partial(self.winSwitch, self.startNewGameWin, self.mainWin)).grid(row=4, column=1)

        if self.slot1Character == "":
            pic1 = PhotoImage(file="BlankSlot.gif")
            imageButton1 = Button(F1, command=partial(self.intro, 1))
            imageButton1.pack()
            imageButton1.config(image=pic1)
            Label(F1, text=_("空档")).pack()

        else:
            pic1 = PhotoImage(file="you.ppm")
            imageButton1 = Button(F1, command=partial(self.intro, 1))
            imageButton1.pack()
            imageButton1.config(image=pic1)
            Label(F1, text=_("{}\n等级 {}").format(self.slot1Character, self.slot1Level)).pack()

        if self.slot2Character == "":
            pic2 = PhotoImage(file="BlankSlot.gif")
            imageButton2 = Button(F2, command=partial(self.intro, 2))
            imageButton2.pack()
            imageButton2.config(image=pic2)
            Label(F2, text=_("空档")).pack()

        else:
            pic2 = PhotoImage(file="you.ppm")
            imageButton2 = Button(F2, command=partial(self.intro, 2))
            imageButton2.pack()
            imageButton2.config(image=pic2)
            Label(F2, text=_("{}\n等级 {}").format(self.slot2Character, self.slot2Level)).pack()

        if self.slot3Character == "":
            pic3 = PhotoImage(file="BlankSlot.gif")
            imageButton3 = Button(F3, command=partial(self.intro, 3))
            imageButton3.pack()
            imageButton3.config(image=pic3)
            Label(F3, text=_("空档")).pack()

        else:
            pic3 = PhotoImage(file="you.ppm")
            imageButton3 = Button(F3, command=partial(self.intro, 3))
            imageButton3.pack()
            imageButton3.config(image=pic3)
            Label(F3, text=_("{}\n等级 {}").format(self.slot3Character, self.slot3Level)).pack()

        if self.slot4Character == "":
            pic4 = PhotoImage(file="BlankSlot.gif")
            imageButton4 = Button(F4, command=partial(self.intro, 4))
            imageButton4.pack()
            imageButton4.config(image=pic4)
            Label(F4, text=_("空档")).pack()

        else:
            pic4 = PhotoImage(file="you.ppm")
            imageButton4 = Button(F4, command=partial(self.intro, 4))
            imageButton4.pack()
            imageButton4.config(image=pic4)
            Label(F4, text=_("{}\n等级 {}").format(self.slot4Character, self.slot4Level)).pack()

        if self.slot5Character == "":
            pic5 = PhotoImage(file="BlankSlot.gif")
            imageButton5 = Button(F5, command=partial(self.intro, 5))
            imageButton5.pack()
            imageButton5.config(image=pic5)
            Label(F5, text=_("空档")).pack()

        else:
            pic5 = PhotoImage(file="you.ppm")
            imageButton5 = Button(F5, command=partial(self.intro, 5))
            imageButton5.pack()
            imageButton5.config(image=pic5)
            Label(F5, text=_("{}\n等级 {}").format(self.slot5Character, self.slot5Level)).pack()


        if self.slot6Character == "":
            pic6 = PhotoImage(file="BlankSlot.gif")
            imageButton6 = Button(F6, command=partial(self.intro, 6))
            imageButton6.pack()
            imageButton6.config(image=pic6)
            Label(F6, text=_("空档")).pack()

        else:
            pic6 = PhotoImage(file="you.ppm")
            imageButton6 = Button(F6, command=partial(self.intro, 6))
            imageButton6.pack()
            imageButton6.config(image=pic6)
            Label(F6, text=_("{}\n等级 {}").format(self.slot6Character, self.slot6Level)).pack()


        self.startNewGameWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.startNewGameWin.mainloop()


    def continueGame(self):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        self.mainWin.withdraw()

        self.continueGameWin = Toplevel(self.mainWin)
        self.continueGameWin.title(_("读档"))

        try:
            self.slot1Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-1.p"), "rb"))
            self.slot1Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-1.p"), "rb"))
            self.slot1GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-1.p"), "rb"))
        except:
            self.slot1Character = ""

        try:
            self.slot2Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-2.p"), "rb"))
            self.slot2Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-2.p"), "rb"))
            self.slot2GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-2.p"), "rb"))
        except:
            self.slot2Character = ""


        try:
            self.slot3Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-3.p"), "rb"))
            self.slot3Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-3.p"), "rb"))
            self.slot3GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-3.p"), "rb"))
        except:
            self.slot3Character = ""


        try:
            self.slot4Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-4.p"), "rb"))
            self.slot4Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-4.p"), "rb"))
            self.slot4GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-4.p"), "rb"))
        except:
            self.slot4Character = ""

        try:
            self.slot5Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-5.p"), "rb"))
            self.slot5Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-5.p"), "rb"))
            self.slot5GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-5.p"), "rb"))
        except:
            self.slot5Character = ""


        try:
            self.slot6Character = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "character-6.p"), "rb"))
            self.slot6Level = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "level-6.p"), "rb"))
            self.slot6GameClock = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "gameClock-6.p"), "rb"))
        except:
            self.slot6Character = ""


        Label(self.continueGameWin, text=_("请选择一个档案.")).grid(row=0, column=1)

        F1 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F1.grid(row=1, column=0)

        F2 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F2.grid(row=1, column=1)

        F3 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F3.grid(row=1, column=2)

        Label(self.continueGameWin, text="").grid(row=2, column=1)

        F4 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F4.grid(row=3, column=0)

        F5 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F5.grid(row=3, column=1)

        F6 = Frame(self.continueGameWin, borderwidth=2, relief=SOLID)
        F6.grid(row=3, column=2)

        Button(self.continueGameWin, text=_("返回"),
               command=partial(self.winSwitch, self.continueGameWin, self.mainWin, False)).grid(row=4, column=1)

        if self.slot1Character == "":
            pic1 = PhotoImage(file="BlankSlot.gif")
            imageButton1 = Button(F1, command=partial(self.loadGame, 1))
            imageButton1.pack()
            imageButton1.config(image=pic1)
            Label(F1, text=_("空档")).pack()

        else:

            pic1 = PhotoImage(file="you.ppm")
            imageButton1 = Button(F1, command=partial(self.loadGame, 1))
            imageButton1.pack()
            imageButton1.config(image=pic1)
            Label(F1, text=_("{}\n等级 {}").format(self.slot1Character, self.slot1Level)).pack()

            hours = self.slot1GameClock // 3600
            remainder = self.slot1GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} 小时 {} 分钟 {} 秒").format(hours, minutes, seconds)
            self.balloon.bind(imageButton1, _("总共玩了:") + " " + tpt_string)

        if self.slot2Character == "":
            pic2 = PhotoImage(file="BlankSlot.gif")
            imageButton2 = Button(F2, command=partial(self.loadGame, 2))
            imageButton2.pack()
            imageButton2.config(image=pic2)
            Label(F2, text=_("空档")).pack()

        else:

            pic2 = PhotoImage(file="you.ppm")
            imageButton2 = Button(F2, command=partial(self.loadGame, 2))
            imageButton2.pack()
            imageButton2.config(image=pic2)
            Label(F2, text=_("{}\n等级 {}").format(self.slot2Character, self.slot2Level)).pack()

            hours = self.slot2GameClock // 3600
            remainder = self.slot2GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} 小时 {} 分钟 {} 秒").format(hours, minutes, seconds)
            self.balloon.bind(imageButton2, _("总共玩了:") + " " + tpt_string)

        if self.slot3Character == "":
            pic3 = PhotoImage(file="BlankSlot.gif")
            imageButton3 = Button(F3, command=partial(self.loadGame, 3))
            imageButton3.pack()
            imageButton3.config(image=pic3)
            Label(F3, text=_("空档")).pack()

        else:

            pic3 = PhotoImage(file="you.ppm")
            imageButton3 = Button(F3, command=partial(self.loadGame, 3))
            imageButton3.pack()
            imageButton3.config(image=pic3)
            Label(F3, text=_("{}\n等级 {}").format(self.slot3Character, self.slot3Level)).pack()

            hours = self.slot3GameClock // 3600
            remainder = self.slot3GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} 小时 {} 分钟 {} 秒").format(hours, minutes, seconds)
            self.balloon.bind(imageButton3, _("总共玩了:") + " " + tpt_string)


        if self.slot4Character == "":
            pic4 = PhotoImage(file="BlankSlot.gif")
            imageButton4 = Button(F4, command=partial(self.loadGame, 4))
            imageButton4.pack()
            imageButton4.config(image=pic4)
            Label(F4, text=_("空档")).pack()

        else:

            pic4 = PhotoImage(file="you.ppm")
            imageButton4 = Button(F4, command=partial(self.loadGame, 4))
            imageButton4.pack()
            imageButton4.config(image=pic4)
            Label(F4, text=_("{}\n等级 {}").format(self.slot4Character, self.slot4Level)).pack()

            hours = self.slot4GameClock // 3600
            remainder = self.slot4GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} 小时 {} 分钟 {} 秒").format(hours, minutes, seconds)
            self.balloon.bind(imageButton4, _("总共玩了:") + " " + tpt_string)


        if self.slot5Character == "":
            pic5 = PhotoImage(file="BlankSlot.gif")
            imageButton5 = Button(F5, command=partial(self.loadGame, 5))
            imageButton5.pack()
            imageButton5.config(image=pic5)
            Label(F5, text=_("空档")).pack()

        else:

            pic5 = PhotoImage(file="you.ppm")
            imageButton5 = Button(F5, command=partial(self.loadGame, 5))
            imageButton5.pack()
            imageButton5.config(image=pic5)
            Label(F5, text=_("{}\n等级 {}").format(self.slot5Character, self.slot5Level)).pack()

            hours = self.slot5GameClock // 3600
            remainder = self.slot5GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} 小时 {} 分钟 {} 秒").format(hours, minutes, seconds)
            self.balloon.bind(imageButton5, _("总共玩了:") + " " + tpt_string)




        if self.slot6Character == "":
            pic6 = PhotoImage(file="BlankSlot.gif")
            imageButton6 = Button(F6, command=partial(self.loadGame, 6))
            imageButton6.pack()
            imageButton6.config(image=pic6)
            Label(F6, text=_("空档")).pack()

        else:

            pic6 = PhotoImage(file="you.ppm")
            imageButton6 = Button(F6, command=partial(self.loadGame, 6))
            imageButton6.pack()
            imageButton6.config(image=pic6)
            Label(F6, text=_("{}\n等级 {}").format(self.slot6Character, self.slot6Level)).pack()

            hours = self.slot6GameClock // 3600
            remainder = self.slot6GameClock % 3600
            minutes = remainder // 60
            seconds = remainder % 60
            tpt_string = _("{} 小时 {} 分钟 {} 秒").format(hours, minutes, seconds)
            self.balloon.bind(imageButton6, _("总共玩了:") + " " + tpt_string)


        self.continueGameWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.continueGameWin.mainloop()


    def intro(self, saveSlot):

        self.saveSlot = saveSlot
        if self.saveSlot == 1 and self.slot1Character != "":
            response = messagebox.askquestion("",_("你选择的档案并非空的. 这将清空该档案的所有数据. 确定要继续吗?"))
            if response == "yes":
                pass
            else:
                return

        if self.saveSlot == 2 and self.slot2Character != "":
            response = messagebox.askquestion("",_("你选择的档案并非空的. 这将清空该档案的所有数据. 确定要继续吗?"))
            if response == "yes":
                pass
            else:
                return

        if self.saveSlot == 3 and self.slot3Character != "":
            response = messagebox.askquestion("",_("你选择的档案并非空的. 这将清空该档案的所有数据. 确定要继续吗?"))
            if response == "yes":
                pass
            else:
                return


        if self.saveSlot == 4 and self.slot4Character != "":
            response = messagebox.askquestion("",_("你选择的档案并非空的. 这将清空该档案的所有数据. 确定要继续吗?"))
            if response == "yes":
                pass
            else:
                return


        if self.saveSlot == 5 and self.slot5Character != "":
            response = messagebox.askquestion("",_("你选择的档案并非空的. 这将清空该档案的所有数据. 确定要继续吗?"))
            if response == "yes":
                pass
            else:
                return



        if self.saveSlot == 6 and self.slot6Character != "":
            response = messagebox.askquestion("",_("你选择的档案并非空的. 这将清空该档案的所有数据. 确定要继续吗?"))
            if response == "yes":
                pass
            else:
                return


        self.startNewGameWin.withdraw()
        self.intro_quiz()

    def intro_quiz(self):

        self.stopSoundtrack()
        self.currentBGM = "jy_calm1.mp3"
        self.startSoundtrackThread()

        self.character = ""
        self.health = 120
        self.healthMax = 120
        self.stamina = 120
        self.staminaMax = 120
        self.attack = randrange(48, 56)
        self.strength = randrange(48, 56)
        self.speed = randrange(48, 56)
        self.defence = randrange(48, 56)
        self.dexterity = randrange(48, 56)
        self.luck = 50
        self.perception = 50
        self.chivalry = 0
        self.inv = {_("金钱"): 500}

        # Intro quiz
        self.quiz_questions = [[_("欢迎来到Scrub世界! 游戏开始前请回答几个问题."), []]]
        self.quiz_questions.append([_("你的名字是?"), []])
        self.quiz_questions.append([_("以下几个颜色中你最喜欢哪个？"),
                                    [_("黑"), _("蓝"), _("绿"), _("红"), _("黄")]])
        self.quiz_questions.append([_("以下的几种花你最喜欢哪个?"),
                                    [_("玫瑰"), _("百合"), _("紫罗兰"), _("向日葵"), _("我不喜欢花")]])
        self.quiz_questions.append([_("你觉得这个游戏的作者在现实生活中是什么样的？"),
                                    [_("内向的宅男"), _("行走江湖的大侠"),
                                     _("年轻貌美的姑娘"), _("有钱的中年男子"),
                                     _("需要耶稣的罪人")]])
        self.quiz_questions.append([_("你打架时最可能做的是？"),
                                    [_("毫不留情地攻击"), _("防守"),
                                     _("逃跑"), _("看情况再说"),
                                     _("请敌人吃顿饭与他和好")]])
        self.quiz_questions.append([_("你喜欢金庸小说吗？"),
                                    [_("超喜欢！"), _("还行吧"),
                                     _("不喜欢"), _("金庸是谁？"),
                                     _("我只看电视剧")]])

        self.quiz_win = Toplevel(self.startNewGameWin)
        self.quiz_win.title("")
        self.question_label = Label(self.quiz_win,
                                    text="")
        self.question_label.pack()
        self.question_number = 0

        self.choices_frame = Frame(self.quiz_win, borderwidth=2, relief=SOLID)
        self.choices_frame.pack()
        self.quiz_choice_var = IntVar()

        self.submit_button = Button(self.quiz_win, text=_("确认"), command=self.parse_quiz_response)
        self.submit_button.pack()

        self.display_question()

        self.quiz_win.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.quiz_win.mainloop()

    def display_question(self):
        # destroy any existing radiobuttons within choices frame
        for widget in self.choices_frame.winfo_children():
            widget.destroy()

        # load question and update question label
        self.quiz_q = self.quiz_questions[self.question_number][0]
        if os.name == "nt":
            self.question_label.config(text=self.quiz_q, font=("TkDefaultFont", 24))
        else:
            self.question_label.config(text=self.quiz_q, font=("TkDefaultFont", 32))

        if self.question_number > 1:
            # loop through choices and add radio button for each choice
            rb_index = 0
            for choice in self.quiz_questions[self.question_number][1]:
                if os.name == "nt":
                    Radiobutton(self.choices_frame, text=choice, font=("TkDefaultFont", 18),
                                variable=self.quiz_choice_var,
                                value=rb_index).grid(row=rb_index, column=0, sticky=W)
                else:
                    Radiobutton(self.choices_frame, text=choice, font=("TkDefaultFont", 24),
                                variable=self.quiz_choice_var, value=rb_index).grid(row=rb_index, column=0, sticky=W)

                rb_index += 1
                # self.quiz_choice_var.set(0)

        elif self.question_number == 1:
            self.user_input = Entry(self.choices_frame, width=12)
            self.user_input.pack()

    def parse_quiz_response(self):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.question_number < len(self.quiz_questions) - 1:
            if self.question_number == 0:
                pass
            elif self.question_number == 1:
                response = self.user_input.get()
                if len(response) > 12 or len(response) < 1:
                    messagebox.showinfo("", _("Please choose a name that is between 1 and 12 characters long."))
                    return
                self.character = response
            else:
                response = self.quiz_choice_var.get()
                if self.question_number == 2:
                    if response == 0:
                        self.attack += 1
                        self.strength += 1
                        self.speed += 1
                        self.defence += 1
                        self.dexterity += 1
                    elif response == 1:
                        self.attack += 5
                    elif response == 2:
                        self.defence += 5
                    elif response == 3:
                        self.strength += 5
                    elif response == 4:
                        self.dexterity += 5

                elif self.question_number == 3:
                    if response == 0:
                        self.health += 25
                        self.healthMax += 25
                    elif response == 1:
                        self.health += 15
                        self.healthMax += 15
                        self.stamina += 10
                        self.staminaMax += 10
                    elif response == 2:
                        self.stamina += 25
                        self.staminaMax += 25
                    elif response == 3:
                        self.luck += 25
                        self.attack += 1
                        self.strength += 1
                        self.speed += 1
                        self.defence += 1
                        self.dexterity += 1
                        self.health += 5
                        self.healthMax += 5
                        self.stamina += 5
                        self.staminaMax += 5
                    elif response == 4:
                        self.luck -= 10

                elif self.question_number == 4:
                    if response == 0:
                        self.health += 15
                        self.healthMax += 15
                        self.stamina += 10
                        self.staminaMax += 10
                        self.perception += 10
                    elif response == 1:
                        self.attack += 5
                        self.strength += 5
                    elif response == 2:
                        self.luck += 15
                    elif response == 3:
                        self.inv[_("金钱")] += 500
                    elif response == 4:
                        self.perception += 30

                elif self.question_number == 5:
                    if response == 0:
                        self.attack += 5
                    elif response == 1:
                        self.defence += 5
                    elif response == 2:
                        self.speed += 5
                    elif response == 3:
                        self.attack += 1
                        self.strength += 1
                        self.speed += 1
                        self.defence += 1
                        self.dexterity += 1
                    elif response == 4:
                        self.chivalry += 50
                        self.inv[_("金钱")] += 100

                elif self.question_number == 6:
                    if response == 0:
                        self.luck += 10
                        self.perception += 5
                    elif response == 1:
                        pass
                    elif response == 2:
                        self.perception -= 5
                        self.inv[_("金钱")] -= 250
                    elif response == 3:
                        self.perception -= 5
                    elif response == 4:
                        self.inv[_("金钱")] += 250

            self.question_number += 1
            self.display_question()
        else:
            # End quiz
            self.quiz_win.destroy()
            self.startNewGameWin.withdraw()
            # Declaring fixed starting attributes and saving all variables

            self.exp = 0
            self.level = 1
            self.upgradePoints = 0
            self.status = "Normal"
            self.gameClock = 0

            self.taskProgressDic = {
                "join_sect": -1,
                "xiao_han_jia_wen": -1,
                "liu_village": -1
            }

            self.sml = []
            self.sml.append(special_move(name=_("菜鸟拳")))
            self.sml.append(special_move(name=_("休息")))
            self.sml.append(special_move(name=_("物品")))
            self.sml.append(special_move(name=_("逃跑")))
            self.skills = []


            self.you = character(self.character, self.level, [self.health, self.healthMax,
                                                              self.stamina, self.staminaMax, self.attack,
                                                              self.strength, self.speed, self.defence,
                                                              self.dexterity], self.sml, {}, [],
                                 status=self.status)

            pickle.dump(self.you, open(os.path.join(SAVE_SLOTS_DIR, "you.p"), "wb"))

            pickle.dump(self.character, open(os.path.join(SAVE_SLOTS_DIR, "character.p"), "wb"))
            pickle.dump(self.health, open(os.path.join(SAVE_SLOTS_DIR, "health.p"), "wb"))
            pickle.dump(self.healthMax, open(os.path.join(SAVE_SLOTS_DIR, "healthMax.p"), "wb"))
            pickle.dump(self.stamina, open(os.path.join(SAVE_SLOTS_DIR, "stamina.p"), "wb"))
            pickle.dump(self.staminaMax, open(os.path.join(SAVE_SLOTS_DIR, "staminaMax.p"), "wb"))

            pickle.dump(self.attack, open(os.path.join(SAVE_SLOTS_DIR, "attack.p"), "wb"))
            pickle.dump(self.strength, open(os.path.join(SAVE_SLOTS_DIR, "strength.p"), "wb"))
            pickle.dump(self.speed, open(os.path.join(SAVE_SLOTS_DIR, "speed.p"), "wb"))
            pickle.dump(self.defence, open(os.path.join(SAVE_SLOTS_DIR, "defence.p"), "wb"))
            pickle.dump(self.dexterity, open(os.path.join(SAVE_SLOTS_DIR, "dexterity.p"), "wb"))

            pickle.dump(self.luck, open(os.path.join(SAVE_SLOTS_DIR, "luck.p"), "wb"))
            pickle.dump(self.perception, open(os.path.join(SAVE_SLOTS_DIR, "perception.p"), "wb"))
            pickle.dump(self.chivalry, open(os.path.join(SAVE_SLOTS_DIR, "chivalry.p"), "wb"))

            pickle.dump(self.exp, open(os.path.join(SAVE_SLOTS_DIR, "exp.p"), "wb"))
            pickle.dump(self.level, open(os.path.join(SAVE_SLOTS_DIR, "level.p"), "wb"))
            pickle.dump(self.upgradePoints, open(os.path.join(SAVE_SLOTS_DIR, "upgradePoints.p"), "wb"))
            pickle.dump(self.status, open(os.path.join(SAVE_SLOTS_DIR, "status.p"), "wb"))
            pickle.dump(self.gameClock, open(os.path.join(SAVE_SLOTS_DIR, "gameClock.p"), "wb"))

            pickle.dump(self.sml, open(os.path.join(SAVE_SLOTS_DIR, "sml.p"), "wb"))
            pickle.dump(self.skills, open(os.path.join(SAVE_SLOTS_DIR, "skills.p"), "wb"))
            pickle.dump(self.inv, open(os.path.join(SAVE_SLOTS_DIR, "inv.p"), "wb"))

            self.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("欢迎来到Scrub世界！我的名字是Scrub."),
                 _("这是我创造的虚拟武侠世界."),
                 _("你现在在我家. 你可以把这儿当成你在游戏中的根据地."),
                 _("你有问题的话就来找我, 当然, 你最好先读一下说明书(README)."),
                 _("或者你也可以自己慢慢地探索！好好玩哈~"),
                 _("喂，等一下! 你把我带到武侠世界总得教我两招吧？"),
                 _("要不然我岂不是去哪儿都要被人欺负?"),
                 _("别担心,每一个到这里来的人都自动学会《菜鸟拳》，我独创的拳法哦!"),
                 _("我去,你逗我吗? 菜鸟拳? 听起来就弱爆了..."),
                 _("总比没有强吧...我先走了,你好好玩哈～")
                 ],
                ["Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "you", "you", "Scrub", "you", "Scrub"],
                bind_text="Left click anywhere in the window to advance the dialogue."
            )
            self.generateGameWin()

    def create_menu_frame(self, master, options=["Map", "Profile", "Inv", "Save", "Settings"]):

        menu_frame = Frame(master=master, borderwidth=2, relief=SUNKEN, padx=5, pady=5)

        if "Map" in options:
            map_icon = PhotoImage(file="map_icon.ppm")
            if master == self.scrubHouseWin:
                map_button = Button(menu_frame, image=map_icon, command=partial(self.map, master))
            else:
                map_button = Button(menu_frame, image=map_icon, command=partial(self.winSwitch, master, self.mapWin))
            map_button.image = map_icon
            map_button.grid(row=0, column=0)

            Label(menu_frame, text=_("大地图")).grid(row=1, column=0)

            Label(menu_frame, text="  ").grid(row=0, column=1)

        if "Profile" in options:
            profile_icon = PhotoImage(file="profile_icon.ppm")
            profile_button = Button(menu_frame, image=profile_icon, command=partial(self.checkProfile, master))
            profile_button.image = profile_icon
            profile_button.grid(row=0, column=2)

            Label(menu_frame, text=_("人物")).grid(row=1, column=2)

            Label(menu_frame, text="  ").grid(row=0, column=3)

        if "Inv" in options:
            inventory_icon = PhotoImage(file="inventory_icon.ppm")
            inventory_button = Button(menu_frame, image=inventory_icon, command=partial(self.checkInv, master))
            inventory_button.image = inventory_icon
            inventory_button.grid(row=0, column=4)

            Label(menu_frame, text=_("包裹")).grid(row=1, column=4)

            Label(menu_frame, text="  ").grid(row=0, column=5)

        if "Save" in options:
            save_icon = PhotoImage(file="save_icon.ppm")
            save_button = Button(menu_frame, image=save_icon, command=self.saveGame)
            save_button.image = save_icon
            save_button.grid(row=0, column=6)

            Label(menu_frame, text=_("存档")).grid(row=1, column=6)

            Label(menu_frame, text="  ").grid(row=0, column=7)

        if "Settings" in options:
            settings_icon = PhotoImage(file="settings_icon.ppm")
            settings_button = Button(menu_frame, image=settings_icon, command=partial(self.settings, master))
            settings_button.image = settings_icon
            settings_button.grid(row=0, column=8)

            Label(menu_frame, text=_("设置")).grid(row=1, column=8)

        return menu_frame


    def generateGameWin(self):

        self.gameInitiatedTimestamp = datetime.now()

        if self.taskProgressDic["join_sect"] not in (-1,100):
            if self.taskProgressDic["join_sect"] < 1:
                self.stopSoundtrack()
                self.currentBGM = "jy_pipayu.mp3"
                self.startSoundtrackThread()
                self.cherry_blossom_island_main()
                return

        self.stopSoundtrack()
        self.currentBGM = "jy_calm2.mp3"
        self.startSoundtrackThread()
        self.create_self_character()

        self.scrubHouseWin = Toplevel(self.mainWin)
        self.scrubHouseWin.title(_("Scrub家"))

        menu = Menu(self.scrubHouseWin)
        self.scrubHouseWin.config(menu=menu)
        sub_menu = Menu(menu)

        menu.add_cascade(label="Admin Options", menu=sub_menu)
        sub_menu.add_command(label="Cheat Code", command=self.cheatCode)
        sub_menu.add_separator()

        # def callback(event):
        #    print("clicked at", event.x, event.y)

        # self.scrubHouseWin.bind("<Button>", callback)
        # self.scrubHouseWin.mainloop()

        bg = PhotoImage(file="scrub_house.ppm")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.scrubHouseWin, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.ScrubHouseF1 = Frame(canvas)
        pic1 = PhotoImage(file="Scrub_icon.ppm")
        imageButton1 = Button(self.ScrubHouseF1, command=self.clickedOnScrub)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.ScrubHouseF1, text="Scrub")
        label.grid(row=1, column=0)
        self.ScrubHouseF1.place(x=w // 2 - pic1.width() // 2, y=h // 2)

        self.ScrubHouseF2 = Frame(self.scrubHouseWin)
        self.ScrubHouseF2.pack()

        menu_frame = self.create_menu_frame(master=self.scrubHouseWin)
        menu_frame.pack()

        Button(self.scrubHouseWin, text=_("返回"),
               command=partial(self.winSwitch, self.scrubHouseWin, self.mainWin)).pack()

        self.scrubHouseWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.scrubHouseWin.mainloop()

    def clickedOnScrub(self):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        Button(self.ScrubHouseF2, text=_("切磋"), command=partial(self.battleMenu, self.you,
                                                                  character("Scrub", max([self.level-1, 1]), sml=[
                                                                      special_move(_("菜鸟拳"), level=self.level),
                                                                      special_move(
                                                                          _("基本剑法"),
                                                                          level=self.level)]),
                                                                  "battleground_forest.ppm", "jy_calm2.mp3",
                                                                  fromWin=self.scrubHouseWin,
                                                                  destinationWinList=[self.scrubHouseWin] * 3)
               ).grid(row=2, column=0)
        Button(self.ScrubHouseF2, text=_("医治我吧"), command=self.healFromScrub).grid(row=3, column=0)
        Button(self.ScrubHouseF2, text=_("你是谁？"), command=self.scrub_who_are_you).grid(row=4, column=0)
        Button(self.ScrubHouseF2, text=_("给我解释一下战斗系统..."), command=self.combat_tutorial).grid(row=5,
                                                                                                                  column=0)
        Button(self.ScrubHouseF2, text=_("教我两招吧"), command=self.scrub_teach_moves).grid(row=6,
                                                                                                             column=0)

    def healFromScrub(self):
        response = messagebox.askquestion("",
                                          _("使用300金钱让Scrub帮你恢复满血和内力?"))
        if response == "yes":
            if self.inv[_("金钱")] >= 300:
                self.inv[_("金钱")] -= 300
                self.restore(0, 0, full=True)
                self.currentEffect = "sfx_recover.mp3"
                self.startSoundEffectThread()
            else:
                messagebox.showinfo("", _("你的金钱不够."))
        else:
            return

    def scrub_who_are_you(self):

        self.generate_dialogue_sequence(
            self.scrubHouseWin,
            "scrub_house.ppm",
            [_("你是谁？"), _("我不是已经告诉你了吗? 我叫Scrub"),
             _("是这个游戏的创造者")],
            ["you", "Scrub", "Scrub"]
        )

    def combat_tutorial(self):

        self.generate_dialogue_sequence(
            self.scrubHouseWin,
            "scrub_house.ppm",
            [_("总共有7个属性跟战斗有关"),
             _("分别为气血,内力,攻击,力量,速度,防御,和敏捷"),
             _("当你的气血到0时,你将输掉战斗"),
             _("大部分的招式都需要内力. 消耗的数量在于你使用的招式和该招式的等级."),
             _("当你重复使用一个招式, 你会获得与该招式相关的经验并且可以将它升级."),
             _("升级后的招式会制造更多伤害且可能带有特殊效果."),
             _("但同时也会消耗更多的内力"),
             _("你可以通过你的人物升级或修为点来提升你的气血/内力上限."),
             _("'攻击'决定你的准确度;力量决定你的伤害加成."),
             _("速度决定你的攻击频率与你的逃跑成功率."),
             _("防御决定你受到的伤害点."),
             _("敏捷决定你躲闪敌方攻击的成功率."),
             _("你可以使用修为点来提升这五个属性."),
             _("每当你的人物升级, 你会根据你的等级和'悟性'获得修为点."),
             _("我知道这个信息量有点大. 如果你想再读一遍就随时找我，或者你也可以自己试一下！")
             ],
            ["Scrub"] * 15
        )

    def scrub_teach_moves(self):

        if self.taskProgressDic["join_sect"] == -1:
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("怎么, 我教你的《菜鸟拳》不够用？"),
                 _("《菜鸟拳》是给菜鸟用的... 我想当大侠!"),
                 _("你就让我跟高手学习一下嘛"),
                 _("好吧好吧... 那我给你8个选项. 听好了哟"),
                 _("当今武林有四大高手, 称为四绝"),
                 _("他们分别为'东杰'黄笑冬..."),
                 _("'西徒'谭克勇, '南医'苏灵,和'北怪'陆西峰."),
                 _("除了这些以外，你也可以加入三大帮派: 少林,武当,华山"),
                 _("等等，你不是说8个选项吗？这才7个..."),
                 _("对呀,还有一个选择就是不加入任何帮派"),
                 _("这也就意味着你不能拜师了"),
                 _("但如果你选择不加入帮派的话, 我会送给你一个礼物."),
                 _("仔细看一下你的选项再做决定哦!"),
                 ],
                ["Scrub", "you", "you", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "you", "Scrub", "Scrub", "Scrub",
                 "Scrub"]
            )

            self.join_sect()

        else:
            self.generate_dialogue_sequence(
                self.scrubHouseWin,
                "scrub_house.ppm",
                [_("我没有什么可以教你的了"),
                 _("你如果想学一些新的招式就自己出去转悠吧. 或许能遇到什么高手呢")],
                ["Scrub", "Scrub"]
            )

    def join_sect(self, choice=None):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if choice == None:
            self.scrubHouseWin.withdraw()
            self.join_sect_win = Toplevel(self.scrubHouseWin)

            self.sect_options_frame = Frame(self.join_sect_win)
            self.sect_options_frame.pack()

            options = [
                [_("黄笑冬"), _("黄笑冬 (东杰)\n---------------------\n黄笑冬,樱花岛岛主,拥有一流的点穴技术.\n他性格内向, 很少与外界有来往. 他花大\n量时间研究天文地理,琴棋书画,同时也不断\n提高自己的武学境界. 他与太太和两个女儿\n在岛上生活. 不速之客来到岛上通常会被他\n在岛的外围所设计的樱花阵困住.")],
                [_("谭克勇"), _("谭克勇 (西徒)\n---------------------\n谭克勇乃《山湖派》的第四任掌门人.他行侠\n仗义，善待于人，在整个武林无人不敬他三分.\n山湖派的《山湖拳法》与内功心法更是威震武\n林. 20年前,谭克勇偶遇武林至尊叶和华.\n叶因欣赏这年轻人的为人便收他为其第一位正\n式门徒. 几年后, 谭已成为武林中一流的\n高手, 并被他人列为四绝之一.")],
                [_("苏灵"), _("")],
                [_("陆西峰"), _("")],
                [_("少林派"), _("")],
                [_("武当派"), _("")],
                [_("华山派"), _("")],
                [_("Rogue"), _("我才不要拜师呢！我自己学什么不行? 无帮无派才是正道!")]
            ]
            option_images = []

            for option_i in range(len(options)):
                x = option_i % 4
                y = option_i // 4 + 1
                name = options[option_i][0]
                description = options[option_i][1]

                F = Frame(self.sect_options_frame)
                F.grid(row=y, column=x)
                option_image = PhotoImage(file=name + "_icon_full.ppm")
                option_images.append(option_image)
                option_image_button = Button(F, command=partial(self.join_sect, option_i))
                option_image_button.pack()
                option_image_button.config(image=option_image)
                Label(F, text=name).pack()
                self.balloon.bind(F, description)

            Button(self.join_sect_win, text="Back",
                   command=partial(self.winSwitch, self.join_sect_win, self.scrubHouseWin)).pack()
            self.join_sect_win.protocol("WM_DELETE_WINDOW", self.on_exit)
            self.join_sect_win.mainloop()

        else:
            self.join_sect_win.quit()
            self.join_sect_win.destroy()

            if choice == 0:

                self.stopSoundtrack()
                self.currentBGM = "jy_pipayu.mp3"
                self.startSoundtrackThread()
                self.generate_dialogue_sequence(
                    None,
                    "cherry_blossom_island.png",
                    [_("欢迎来到樱花岛. 既然是贵客就不必客气了."),
                     _("我先教你《落樱指法》.你要好好练习."),
                     _("60天后, 我将亲自检查你的进展."),
                     _("若你的表现令我满意, 或许我会传给你更多武功."),
                     _("在这期间你若有问题就找我的两个女儿吧"),
                     _("是！多谢师父！")],
                    ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong",
                     "Huang Xiaodong", "you"]
                )

                new_move = special_move(name=_("落樱指法"))
                print(_("你学会了《落樱指法》！"))
                self.learn_move(new_move)
                self.taskProgressDic["join_sect"] = 0.00
                self.cherry_blossom_island_main()

            elif choice == 1:
                self.taskProgressDic["join_sect"] = 1.00

            elif choice == 2:
                self.taskProgressDic["join_sect"] = 2.00

            elif choice == 3:
                if self.chivalry <= -50:
                    print("Join Phantom Sect")
                elif self.chivalry < 0:
                    self.generate_dialogue_sequence(
                        self.scrubHouseWin,
                        "scrub_house.ppm",
                        [_("I see some potential in you, but you are not quite ready just yet..."),
                         _("You need to demonstrate a willingness to do whatever it takes to get what you want, even if it means crushing others beneath your feet."),
                         _("What he means is that you are not evil enough to join his Sect... imagine that..."),
                         _("(Hmmm, looks like I need to commit some more evil before I try to join this Sect...)")],
                        ["Lu Xifeng", "Lu Xifeng", "Scrub", "you"]
                    )
                    self.taskProgressDic["join_sect"] = -1
                else:
                    self.generate_dialogue_sequence(
                        self.scrubHouseWin,
                        "scrub_house.ppm",
                        [_("Ha, another weak, worthless scum who is unworthy of joining my Sect."),
                         _("Depart from me before I extinguish your existence with a sweep of my arm!"),
                         _("You are far too chivalrous to join his Sect..."),
                         _("(Hmmm, looks like I need to commit some evil before I try to join this Sect...)")],
                        ["Lu Xifeng", "Lu Xifeng", "Scrub", "you"]
                    )
                    self.taskProgressDic["join_sect"] = -1


            elif choice == 4:
                self.taskProgressDic["join_sect"] = 4.00

            elif choice == 5:
                self.taskProgressDic["join_sect"] = 5.00

            elif choice == 6:
                self.taskProgressDic["join_sect"] = 6.00

            elif choice == 7:
                self.generate_dialogue_sequence(self.scrubHouseWin,
                                                "scrub_house.ppm",
                                                [_("我决定不加入任何帮派!"),
                                                 _("虽然有点意外，但只要你高兴就好"),
                                                 _("诺,说到要给你一个礼物的...")
                                                 ],
                                                ["you", "Scrub", "Scrub"])

                self.perception += 30
                self.luck += 30
                self.taskProgressDic["join_sect"] = 7.00
                messagebox.showinfo("", _("Scrub把你的悟性和福缘各提升了30!"))



    def cherry_blossom_island_main(self):

        self.days_spent_in_training = int(self.taskProgressDic["join_sect"]*100)
        try:
            if self.taskProgressDic["join_sect"] == 0.00:
                self.cherry_blossom_island_train_win = Toplevel(self.scrubHouseWin)
            else:
                self.cherry_blossom_island_train_win = Toplevel(self.continueGameWin)
        except:
            pass

        self.cherry_blossom_island_train_win.title(_("樱花岛"))

        bg = PhotoImage(file="cherry_blossom_island.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.cherry_blossom_island_train_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Huang Yuwei_icon_large.ppm")
        imageButton1 = Button(F1, command=partial(self.clickedOnHuangYuwei, True))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("黄羽薇"))
        label.grid(row=1, column=0)
        F1.place(x=w // 5, y=h // 2)

        F2 = Frame(canvas)
        pic2 = PhotoImage(file="Huang Yufei_icon_large.ppm")
        imageButton2 = Button(F2, command=partial(self.clickedOnHuangYufei, True))
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(F2, text=_("黄羽菲"))
        label.grid(row=1, column=0)
        F2.place(x=w * 3 // 5, y=h // 2)

        self.days_spent_in_training_label = Label(self.cherry_blossom_island_train_win,
                                                  text=_("训练日数: {}").format(self.days_spent_in_training))
        self.days_spent_in_training_label.pack()
        self.cherry_blossom_island_train_F1 = Frame(self.cherry_blossom_island_train_win)
        self.cherry_blossom_island_train_F1.pack()

        menu_frame = self.create_menu_frame(master=self.cherry_blossom_island_train_win,
                                            options=["Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.cherry_blossom_island_train_win.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.cherry_blossom_island_train_win.mainloop()


    def clickedOnHuangYuwei(self, training=False):
        if training:
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            for widget in self.cherry_blossom_island_train_F1.winfo_children():
                widget.destroy()

            Button(self.cherry_blossom_island_train_F1, text=_("切磋"),
                   command=partial(self.cherry_blossom_island_spar, _("黄羽薇"))
                   ).grid(row=2, column=0)
            Button(self.cherry_blossom_island_train_F1, text=_("你是谁？"),
                   command=self.huang_yuwei_who_are_you).grid(row=3, column=0)
            Button(self.cherry_blossom_island_train_F1, text=_("教我一点东西呗"),
                   command=self.huang_yuwei_teach).grid(row=4, column=0)


    def huang_yuwei_who_are_you(self):
        self.generate_dialogue_sequence(
            self.cherry_blossom_island_train_win,
            "cherry_blossom_island.png",
            [_("你好,我是黄羽薇,岛主的大女儿!"),
             _("如果你对天文地理有兴趣就来问我"),
             _("如果你想提高你的武功,那还是去找我妹妹吧"),
             _("她在习武上花的功夫比我要多多了. 她的轻功也很厉害!")],
            ["Huang Yuwei", "Huang Yuwei", "Huang Yuwei", "Huang Yuwei"]
        )


    def huang_yuwei_teach(self):
        self.days_spent_in_training = int(self.taskProgressDic["join_sect"] * 100)
        self.days_spent_in_training_label.config(text=_("训练日数: {}").format(self.days_spent_in_training))
        r = randrange(1,3)
        self.perception += r
        messagebox.showinfo("", _("黄羽薇花了两天时间与你研究天文地理,琴棋书画...\n你的悟性+{}.").format(r))
        self.cherry_blossom_island_training_update(.02)


    def cherry_blossom_island_spar(self, targ):

        if targ == _("黄羽薇"):
            opp = character(_("黄羽薇"), self.level,
                  sml=[special_move(_("落樱指法"), level=min([self.level + 1, 5])),
                       special_move(_("基本剑法"), level=min([self.level + 1, 5]))],
                  skills=[skill(_("樱花水上漂"), level=5)])
        elif targ == _("黄羽菲"):
            opp = character(_("黄羽菲"), self.level, sml=[special_move(_("落樱指法"),
                                        level=min([self.level + 1, 7])), special_move(_("基本剑法"),
                                        level=min([self.level + 1, 7]))],
                            skills=[skill(_("樱花水上漂"), level=8)])
        else:
            opp = None

        self.battleMenu(self.you, opp,"battleground_cherry_blossom_island.png",
                        "jy_pipayu.mp3",fromWin = self.cherry_blossom_island_train_win,
                        battleType = "training", destinationWinList = [self.cherry_blossom_island_train_win] * 3)

        if self.taskProgressDic["join_sect"] < 100:
            self.cherry_blossom_island_training_update(.01)


    def clickedOnHuangYufei(self, training=False):

        if training:
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            for widget in self.cherry_blossom_island_train_F1.winfo_children():
                widget.destroy()

            Button(self.cherry_blossom_island_train_F1, text=_("切磋"),
                   command=partial(self.cherry_blossom_island_spar, targ=_("黄羽菲"))).grid(row=2, column=0)
            Button(self.cherry_blossom_island_train_F1, text=_("你是谁？"),
                   command=self.huang_yufei_who_are_you).grid(row=3, column=0)
            Button(self.cherry_blossom_island_train_F1, text=_("教我一点东西呗"),
                   command=self.huang_yufei_teach).grid(row=4, column=0)


    def huang_yufei_who_are_you(self):
        self.generate_dialogue_sequence(
            self.cherry_blossom_island_train_win,
            "cherry_blossom_island.png",
            [_("嘿！我是黄羽薇,岛主的小女儿!"),
             _("我从小就喜欢练武，而姐姐比较喜欢读书."),
             _("如果你想学一些有趣的东西就去请教她吧."),
             _("如果你想找人陪你练武, 师妹我随时奉陪！")],
            ["Huang Yufei", "Huang Yufei", "Huang Yufei", "Huang Yufei"]
        )


    def huang_yufei_teach(self):

        self.cherry_blossom_island_train_win.withdraw()
        self.generate_dialogue_sequence(
            None,
            "cherry_blossom_island.png",
            [_("你想让我教你? 好啊，不过你得先陪我玩儿！"),
             _("你来抓我呀～ 我在樱花阵中间的亭子等你~")],
            ["Huang Yufei", "Huang Yufei"],
        )
        self.go_cherry_blossom_island(training=True)
        self.cherry_blossom_island_training_update(.01)


    def cherry_blossom_island_training_update(self, increment=.01):
        self.taskProgressDic["join_sect"] += increment
        self.days_spent_in_training = int(self.taskProgressDic["join_sect"] * 100)
        self.days_spent_in_training_label.config(text=_("训练日数: {}").format(self.days_spent_in_training))
        if self.days_spent_in_training >= 60:
            self.days_spent_in_training = 60
            self.taskProgressDic["join_sect"] = .6
            self.days_spent_in_training_label.config(text=_("训练日数: {}").format(self.days_spent_in_training))
            self.cherry_blossom_island_train_win.withdraw()

            self.generate_dialogue_sequence(
                None,
                "cherry_blossom_island.png",
                [_("60天已到，希望你有好好的利用这段时间."),
                 _("你离开之前, 我先看看你这几天的进展如何..."),
                 _("别担心,我会手下留情的.")],
                ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong"]
            )

            self.restore(0,0,full=True)


            opp = character(_("黄笑冬"), 20,
                  sml=[special_move(_("落樱指法"), level=10),
                       special_move(_("基本剑法"), level=10)],
                  skills=[skill(_("樱花水上漂"), level=10)])

            self.battleID = "HuangXiaodongTrainingTest"
            self.battleMenu(self.you, opp,"battleground_cherry_blossom_island.png",
                            postBattleSoundtrack="jy_pipayu.mp3",
                            fromWin = self.cherry_blossom_island_train_win,
                            battleType = "test", destinationWinList = [self.cherry_blossom_island_train_win] * 3
                            )


    def cherry_blossom_island_training_reward(self):
        self.taskProgressDic["join_sect"] = 100
        total_levels = 0
        for move in self.sml:
            if "Cherry Blossom" in move.name:
                total_levels += move.level
        for skill in self.skills:
            if "Cherry Blossom" in skill.name:
                total_levels += skill.level

        self.restore(0, 0, full=True)
        self.inv[_('Cherry Blossom Island Map')] = 1
        print(total_levels)
        if self.luck*total_levels/10 >= 95:
            self.generate_dialogue_sequence(
                self.cherry_blossom_island_train_win,
                "cherry_blossom_island.png",
                [_("Hmmmm, I am very pleased with the progress you've made."),
                 _("As your reward, I will teach you the 'Cherry Blossom Drifting Snow Blade'."),
                 _("Finally, here's a map of the island; with this map, you will have no trouble getting through the maze if you should ever come back to visit."),
                 _("Thank you, Master, for your kindness and hospitality!"),
                 _("You're welcome. I hope you can use what you've learned for good, and not evil."),
                 _("Don't worry; I won't disappoint you! Take care, Master!"),
                 _("Farewell, Yuwei and Yufei!"),
                 _("Goodbye, {}!".format(self.character)),
                 _("Come back and visit us when you get a chance!")],
                ["Huang Xiaodong","Huang Xiaodong","Huang Xiaodong","you","Huang Xiaodong","you",
                 "you","Huang Yuwei","Huang Yufei"]
            )

            new_skill = skill(name=_("Cherry Blossom Drifting Snow Blade"))
            self.learn_skill(new_skill)
            print("Learned", new_skill.name)


        elif self.luck*total_levels/10 >= random()*100:
            self.generate_dialogue_sequence(
                self.cherry_blossom_island_train_win,
                "cherry_blossom_island.png",
                [_("Hmmmm, I am very pleased with the progress you've made."),
                 _("As your reward, here are some Golden Gloves and also some Cherry Blossom Papaya Tremella Soup that my wife and daughters made."),
                 _("Finally, here's a map of the island; with this map, you will have no trouble getting through the maze if you should ever come back to visit."),
                 _("Thank you, Master, for your kindness and hospitality!"),
                 _("You're welcome. I hope you can use what you've learned for good, and not evil."),
                 _("Don't worry; I won't disappoint you! Take care, Master!"),
                 _("Farewell, Yuwei and Yufei!"),
                 _("Goodbye, {}!".format(self.character)),
                 _("Come back and visit us when you get a chance!")],
                ["Huang Xiaodong","Huang Xiaodong","Huang Xiaodong","you","Huang Xiaodong","you",
                 "you","Huang Yuwei","Huang Yufei"]
            )
            self.inv["Cherry Blossom Papaya Tremella Soup"] = 5
            self.inv["Golden Gloves"] = 1


        elif self.luck*total_levels/10 >= random()*80:
            self.generate_dialogue_sequence(
                self.cherry_blossom_island_train_win,
                "cherry_blossom_island.png",
                [_("Hmmmm, I am very pleased with the progress you've made."),
                 _("As your reward, here's some Cherry Blossom Papaya Tremella Soup that my wife and daughters made."),
                 _("Finally, here's a map of the island; with this map, you will have no trouble getting through the maze if you should ever come back to visit."),
                 _("Thank you, Master, for your kindness and hospitality!"),
                 _("You're welcome. I hope you can use what you've learned for good, and not evil."),
                 _("Don't worry; I won't disappoint you! Take care, Master!"),
                 _("Farewell, Yuwei and Yufei!"),
                 _("Goodbye, {}!".format(self.character)),
                 _("Come back and visit us when you get a chance!")],
                ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "you", "Huang Xiaodong", "you",
                 "you", "Huang Yuwei", "Huang Yufei"]
            )
            self.inv["Cherry Blossom Papaya Tremella Soup"] = 5


        else:
            self.generate_dialogue_sequence(
                self.cherry_blossom_island_train_win,
                "cherry_blossom_island.png",
                [_("Hmmmm, you didn't meet my expectations..."),
                 _("However, I will not let you go empty handed."),
                 _("Here's a map of the island; with this map, you will have no trouble getting through the maze if you should ever come back to visit."),
                 _("Thank you, Master, for your kindness and hospitality!"),
                 _("You're welcome. I hope you can use what you've learned for good, and not evil."),
                 _("Don't worry; I won't disappoint you! Take care, Master!"),
                 _("Farewell, Yuwei and Yufei!"),
                 _("Goodbye, {}!".format(self.character)),
                 _("Come back and visit us when you get a chance!")],
                ["Huang Xiaodong", "Huang Xiaodong", "Huang Xiaodong", "you", "Huang Xiaodong", "you",
                 "you", "Huang Yuwei", "Huang Yufei"]
            )

        self.cherry_blossom_island_train_win.quit()
        self.cherry_blossom_island_train_win.destroy()
        self.generateGameWin()


    def map(self, fromWin):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        self.stopSoundtrack()
        self.currentBGM = pickOne(["jy_huha.mp3", "jy_heroic.mp3", "jy_xiaoyaogu.mp3", "jy_xiaoyaogu3.mp3"])
        self.startSoundtrackThread()

        fromWin.withdraw()

        self.mapWin = Toplevel(fromWin)

        map_image = PhotoImage(file="map.ppm")
        self.map_canvas = Canvas(master=self.mapWin, width=map_image.width() - 1, height=map_image.height() - 1,
                                 bg='white')
        self.map_canvas.create_image(0, 0, anchor=NW, image=map_image)

        # Scrub's House
        scrub_house_frame = Frame(self.map_canvas, bg="#d3d3d3")
        scrub_house_image = PhotoImage(file="scrub_house_icon.ppm")
        self.scrub_house_image_button = Button(scrub_house_frame,
                                               command=partial(self.winSwitch, self.mapWin, self.scrubHouseWin))
        self.scrub_house_image_button.pack()
        self.scrub_house_image_button.config(image=scrub_house_image)
        Label(scrub_house_frame, text=_("Scrub家"), wraplength=60, bg="#d3d3d3").pack()
        scrub_house_frame.place(x=300, y=220)

        # Snowy Mountain
        snowy_mountain_frame = Frame(self.map_canvas, bg="#d3d3d3")
        snowy_mountain_image = PhotoImage(file="snowy_mountain_icon.ppm")
        self.snowy_mountain_image_button = Button(snowy_mountain_frame,
                                                  command=partial(self.travel, "snowy_mountain"))
        self.snowy_mountain_image_button.pack()
        self.snowy_mountain_image_button.config(image=snowy_mountain_image)
        Label(snowy_mountain_frame, text=_("雪山"), wraplength=60, bg="#d3d3d3").pack()
        snowy_mountain_frame.place(x=20, y=30)


        # Inn
        inn_frame = Frame(self.map_canvas, bg="#d3d3d3")
        inn_image = PhotoImage(file="inn_icon.ppm")
        self.inn_image_button = Button(inn_frame,
                                           command=partial(self.travel, "Inn"))
        self.inn_image_button.pack()
        self.inn_image_button.config(image=inn_image)
        Label(inn_frame, text=_("悦来客栈"), wraplength=60, bg="#d3d3d3").pack()
        inn_frame.place(x=720, y=240)


        # Village
        village_frame = Frame(self.map_canvas, bg="#d3d3d3")
        village_image = PhotoImage(file="liu_village_icon.ppm")
        self.village_image_button = Button(village_frame,
                                           command=partial(self.travel, "liu_village"))
        self.village_image_button.pack()
        self.village_image_button.config(image=village_image)
        Label(village_frame, text=_("刘家村"), wraplength=60, bg="#d3d3d3").pack()
        village_frame.place(x=800, y=250)

        # Forest
        forest_frame = Frame(self.map_canvas, bg="#d3d3d3")
        forest_image = PhotoImage(file="forest_icon.ppm")
        self.forest_image_button = Button(forest_frame,
                                          command=partial(self.travel, "forest"))
        self.forest_image_button.pack()
        self.forest_image_button.config(image=forest_image)
        Label(forest_frame, text=_("森林"), wraplength=60, bg="#d3d3d3").pack()
        forest_frame.place(x=900, y=200)

        # Cherry Blossom Island
        cherry_blossom_island_frame = Frame(self.map_canvas, bg="#d3d3d3")
        cherry_blossom_island_image = PhotoImage(file="cherry_blossom_island_icon.ppm")
        self.cherry_blossom_island_image_button = Button(cherry_blossom_island_frame,
                                                         command=partial(self.travel, "cherry_blossom_island"))
        self.cherry_blossom_island_image_button.pack()
        self.cherry_blossom_island_image_button.config(image=cherry_blossom_island_image)
        Label(cherry_blossom_island_frame, text=_("樱花岛"), wraplength=60, bg="#d3d3d3").pack()
        cherry_blossom_island_frame.place(x=1000, y=475)

        # Dark Forest
        dark_forest_frame = Frame(self.map_canvas, bg="#d3d3d3")
        dark_forest_image = PhotoImage(file="dark_forest_icon.ppm")
        self.dark_forest_image_button = Button(dark_forest_frame,
                                               command=partial(self.travel, "dark_forest"))
        self.dark_forest_image_button.pack()
        self.dark_forest_image_button.config(image=dark_forest_image)
        Label(dark_forest_frame, text=_("黑暗森林"), wraplength=60, bg="#d3d3d3").pack()
        dark_forest_frame.place(x=600, y=460)

        self.map_canvas.pack()
        self.mapWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.mapWin.mainloop()

    def travel(self, destination):

        if destination == "snowy_mountain":
            pass

        elif destination == "forest":
            self.mapWin.withdraw()
            if random() <= .2:
                self.robber_encounter()
            else:
                self.go_forest()

        elif destination == "cherry_blossom_island":

            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_shendiaogeqi.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            self.go_cherry_blossom_island()

        elif destination == "Inn":

            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()
            self.stopSoundtrack()
            self.currentBGM = "jy_relaxing2.mp3"
            self.startSoundtrackThread()
            self.mapWin.withdraw()
            self.go_inn()



    def go_inn(self):

        self.innWin = Toplevel(self.mapWin)
        self.innWin.title(_("悦来客栈"))

        # def callback(event):
        #    print("clicked at", event.x, event.y)

        # self.scrubHouseWin.bind("<Button>", callback)
        # self.scrubHouseWin.mainloop()

        bg = PhotoImage(file="Inn.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.innWin, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.innF1 = Frame(canvas)
        pic1 = PhotoImage(file="Waiter_icon_large.ppm")
        imageButton1 = Button(self.innF1, command=self.order_food)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.innF1, text=_("店小二"))
        label.grid(row=1, column=0)
        self.innF1.place(x=w // 4 - pic1.width() // 2, y=h // 4)

        self.innF2 = Frame(canvas)
        pic2 = PhotoImage(file="Customer_icon_large.ppm")
        imageButton2 = Button(self.innF2)
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(self.innF2, text=_("客官"))
        label.grid(row=1, column=0)
        self.innF2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 4)

        menu_frame = self.create_menu_frame(self.innWin)
        menu_frame.pack()

        self.innWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.innWin.mainloop()


    def order_food(self):

        self.generate_dialogue_sequence(
            self.innWin,
            "Inn.png",
            [_("Greetings, customer! Come see what delicious food we have!")],
            ["Waiter"]
        )

        self.inn_food_prices = {
            _("螃蟹"): 30,
            _("汤"): 25,
            _("鱼"): 18,
            _("包子"): 20
        }
        self.selected_shop_item = None
        self.innWin.withdraw()
        self.innMenuWin = Toplevel(self.innWin)

        F1 = Frame(self.innMenuWin, borderwidth=2, relief=SUNKEN, padx=5, pady=5)
        F1.pack()

        self.shop_item_buttons = []
        self.shop_item_icons = []
        self.item_price_labels = []
        self.selected_shop_item = None
        self.selected_shop_item_index = None


        item_x = 0
        item_y = 0
        item_index = 0
        for item_name in self.inn_food_prices:
            item_icon = PhotoImage(file=_("i_{}.ppm").format(item_name))
            item_button = Button(F1, image=item_icon, command=partial(self.selectShopItem, item_name, item_index))
            item_button.image = item_icon
            item_button.grid(row=item_y, column=item_x)

            item_description = ITEM_DESCRIPTIONS[item_name]
            self.balloon.bind(item_button, item_description)
            self.shop_item_icons.append(item_icon)
            self.shop_item_buttons.append(item_button)

            price_label = Label(text="Price:" + str(self.inn_food_prices[item_name]), master=F1)
            price_label.grid(row=item_y + 1, column=item_x)
            self.item_price_labels.append(price_label)

            item_x += 1
            item_index += 1


        F2 = Frame(self.innMenuWin, padx=5, pady=5)
        F2.pack()

        Button(F2, text="Back", command=partial(self.winSwitch, self.innMenuWin, self.innWin)).grid(row=0, column=0)
        Label(F2, text = "  ").grid(row=0, column=1)
        Button(F2, text="Buy", command=self.buy_selected_shop_item).grid(row=0, column=2)
        Label(F2, text="  ").grid(row=0, column=3)

        self.shop_gold_amount_label = Label(F2, text=_("Your gold: ")+ str(self.inv[_("金钱")]))
        self.shop_gold_amount_label.grid(row=0,column=4)


        self.innMenuWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.innMenuWin.mainloop()


    def selectShopItem(self, item_name, item_index):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.selected_shop_item:
            self.shop_item_icons[self.selected_shop_item_index] = PhotoImage(file=_("i_{}.ppm").format(self.selected_shop_item))
            self.shop_item_buttons[self.selected_shop_item_index].config(image=self.shop_item_icons[self.selected_shop_item_index])

        self.selected_shop_item = item_name
        self.selected_shop_item_index = item_index

        self.shop_item_icons[self.selected_shop_item_index] = PhotoImage(file=_("i_{}_selected.ppm").format(self.selected_shop_item))
        self.shop_item_buttons[self.selected_shop_item_index].config(image=self.shop_item_icons[self.selected_shop_item_index])


    def buy_selected_shop_item(self):

        if not self.selected_shop_item:
            messagebox.showinfo("", _("未选择任何物品"))
            return

        if self.inv[_("金钱")]< self.inn_food_prices[self.selected_shop_item]:
            messagebox.showinfo("", _("Not enough gold."))
            return

        else:
            if self.selected_shop_item in self.inv:
                self.inv[self.selected_shop_item] += 1
            else:
                self.inv[self.selected_shop_item] = 1
            self.inv[_("金钱")] -= self.inn_food_prices[self.selected_shop_item]
            self.shop_gold_amount_label.config(text=_("Your gold: ")+ str(self.inv[_("金钱")]))
            messagebox.showinfo("", _("Purchased {}.").format(self.selected_shop_item))


    def robber_encounter(self):

        self.stopSoundtrack()
        self.currentBGM = "jy_suspenseful1.mp3"
        self.startSoundtrackThread()

        self.generate_dialogue_sequence(
            None,
            "forest.ppm",
            [_("Heeeeeelp! Someone help me!!!"),
             _("Robbers! Someone please save me!"),
             _("Calm down bro... You're lucky you met me today."),
             _("Where are the robbers? I'll take care of them for you."),
             _("Hah! So this guy's looking for trouble, huh? Then I'll make you my victim as well!")],
            ["Merchant", "Merchant", "you", "you", "Masked Robber"]
        )

        enemy_level = pickOne([1,2,2,2,3,3,4])
        post_soundtrack = pickOne(["jy_huha.mp3", "jy_heroic.mp3", "jy_xiaoyaogu.mp3", "jy_xiaoyaogu3.mp3"])
        cmd = lambda: self.generate_dialogue_sequence(None,
                                                      "forest.ppm",
                                                      [_("Argh! You got lucky this time... Just you wait..."),
                                                       _("Thank you for saving me, young hero!")],
                                                      ["Masked Robber", "Merchant"],
                                                      [["You're welcome! Be careful from now on.",
                                                        partial(self.robber_encounter_post_battle, True)],
                                                       [
                                                           "Who said I was helping you? I just want all the money for myself. Now hand it over!",
                                                           partial(self.robber_encounter_post_battle, False)]
                                                       ])

        self.battleMenu(self.you, character("Masked Robber", enemy_level,
                                            sml=[special_move(_("基本剑法"), level=enemy_level + 1)]),
                        "battleground_forest.ppm", postBattleSoundtrack=post_soundtrack, fromWin=self.mapWin,
                        postBattleCmd=cmd)

    def robber_encounter_post_battle(self, return_money):
        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        if return_money:
            messagebox.showinfo("", _("Your chivalry +10."))
            self.chivalry += 10

        else:
            self.generate_dialogue_sequence(
                None,
                "forest.ppm",
                [_("You... you..."),
                 _("*Sigh* People nowadays... Fine, take it then...")],
                ["Merchant", "Merchant"]
            )
            random_gold = randrange(200, 500)
            self.inv[_("金钱")] += random_gold
            self.chivalry -= 10
            messagebox.showinfo("", _("You receive {} gold. Your chivalry -10.").format(random_gold))

        self.go_forest()

    def go_forest(self):

        self.mapWin.withdraw()
        self.forestWin = Toplevel(self.mapWin)
        self.forestWin.title(_("森林"))

        # def callback(event):
        #    print("clicked at", event.x, event.y)

        # self.scrubHouseWin.bind("<Button>", callback)
        # self.scrubHouseWin.mainloop()

        bg = PhotoImage(file="forest.ppm")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.forestWin, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.forestF1 = Frame(canvas)
        pic1 = PhotoImage(file="Woodsman_icon.ppm")
        imageButton1 = Button(self.forestF1, command=self.chop_wood)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.forestF1, text=_("樵夫"))
        label.grid(row=1, column=0)
        self.forestF1.place(x=w // 4 - pic1.width() // 2, y=h // 4)

        self.forestF2 = Frame(canvas)
        pic2 = PhotoImage(file="forest_explore_icon.ppm")
        imageButton2 = Button(self.forestF2, command=self.hunt)
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(self.forestF2, text=_("探索"))
        label.grid(row=1, column=0)
        self.forestF2.place(x=w *3// 4 - pic2.width() // 2, y=h // 4)


        menu_frame = self.create_menu_frame(self.forestWin)
        menu_frame.pack()

        self.forestWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.forestWin.mainloop()


    def chop_wood(self, action="Talk"):

        if action == "Talk":
            self.generate_dialogue_sequence(
                self.forestWin,
                "forest.ppm",
                [_("Hey there, young lad! You look quite strong. Want to chop some wood for me?"),
                 _("I'll pay you some gold in return.")],
                ["Woodsman", "Woodsman"],
                options=[[_("Yes (uses up 50 stamina)"), partial(self.chop_wood, action="Chop")],
                         [_("Not right now"), partial(self.chop_wood, action="Nothing")]]
            )
            return

        elif action == "Chop":
            if self.stamina >= 50:
                self.stamina -= 50
                random_gold = randrange(30, 50)
                self.inv[_("金钱")] += random_gold
                if self.strength < 75:
                    self.strength += 1
                    messagebox.showinfo("",
                                        _("You receive {} gold.\nYour strength increased by 1.").format(random_gold))
                else:
                    self.staminaMax += 2
                    self.healthMax += 2
                    messagebox.showinfo("", _(
                        "You receive {} gold.\nYour health and stamina upper limits increased by 2.").format(
                        random_gold))

            else:
                messagebox.showinfo("", _("Not enough stamina."))

        self.dialogueWin.quit()
        self.dialogueWin.destroy()
        self.forestWin.deiconify()


    def hunt(self):

        if self.stamina < 50:
            messagebox.showinfo("", _("Not enough stamina (Requires 50)."))
            return

        self.stamina -= 50
        self.hunting_ended = False
        self.hunting_results = {_("毒蜘蛛"):0,
                                _("蛇心"):0,
                                _("毒蜈蚣"):0,
                                _("熊胆"):0,
                                _("毒蛾1"): 0,
                                _("毒蛾2"): 0,
                                _("毒蛾3"): 0,
                                _("毒蛾4"): 0,
                                _("毒蛾5"): 0,
                                }
        self.forestWin.withdraw()
        self.stopSoundtrack()
        self.currentBGM = "jy_worklist.mp3"
        self.startSoundtrackThread()


        self.off_screen_x, self.off_screen_y = -1000, -1000
        self.huntingWin = Toplevel(self.forestWin)
        bg_img = PhotoImage(file="forest.ppm")

        self.hunting_canvas_width = bg_img.width()
        self.hunting_canvas_height = bg_img.height()

        if os.name == "nt":
            self.hunting_canvas = Canvas(self.huntingWin, width=self.hunting_canvas_width,
                                         height=self.hunting_canvas_height, bg='grey', cursor="hand2")
        else:
            self.hunting_canvas = Canvas(self.huntingWin, width=self.hunting_canvas_width,
                                         height=self.hunting_canvas_height, bg='grey', cursor="hand")

        self.hunting_canvas.pack(side=LEFT)

        self.hunting_instructions_frame = Frame(self.huntingWin, borderwidth = 2, relief = SUNKEN)
        self.hunting_instructions_frame.pack(side=RIGHT, fill=Y)

        Label(self.hunting_instructions_frame, text = _("Click on the animal/insect icons\nas they pop up to capture them.\nTime limit: 60 seconds")).pack()
        #Button(self.hunting_instructions_frame, text="Back", command=partial(self.winSwitch, self.huntingWin, self.forestWin)).pack()



        self.bg_img = self.hunting_canvas.create_image(self.hunting_canvas_width - bg_img.width(),
                                                       self.hunting_canvas_height - bg_img.height(),
                                                       anchor=NW, image=bg_img)

        spider_photo = PhotoImage(file="Venomous Spider_icon_small.ppm")
        snake_photo = PhotoImage(file="Snake_icon_small.ppm")
        centipede_photo = PhotoImage(file="Venomous Centipede_icon_small.ppm")
        bear_photo = PhotoImage(file="Bear_icon_small.ppm")

        spider_count = 6
        snake_count = 4
        centipede_count = 6
        bear_count = 3
        moth_count = 5

        for i in range(spider_count):
            spider_img = self.hunting_canvas.create_image(self.off_screen_x, self.off_screen_y, anchor=NW,
                                                          image=spider_photo)
            self.hunting_canvas.tag_bind(spider_img, "<Button-1>",
                                         partial(self.hunting_clicked_on, _("毒蜘蛛"), spider_img))
            t = Thread(target=self.hunting_move_item, args=(spider_img,))
            t.start()

        for i in range(snake_count):
            snake_img = self.hunting_canvas.create_image(self.off_screen_x, self.off_screen_y, anchor=NW,
                                                         image=snake_photo)
            self.hunting_canvas.tag_bind(snake_img, "<Button-1>",
                                         partial(self.hunting_clicked_on, _("蛇心"), snake_img))
            t = Thread(target=self.hunting_move_item, args=(snake_img, (10, 21)))
            t.start()

        for i in range(centipede_count):
            centipede_img = self.hunting_canvas.create_image(self.off_screen_x, self.off_screen_y, anchor=NW,
                                                             image=centipede_photo)
            self.hunting_canvas.tag_bind(centipede_img, "<Button-1>",
                                         partial(self.hunting_clicked_on, _("毒蜈蚣"), centipede_img))
            t = Thread(target=self.hunting_move_item, args=(centipede_img,))
            t.start()

        for i in range(bear_count):
            bear_img = self.hunting_canvas.create_image(self.off_screen_x, self.off_screen_y, anchor=NW,
                                                        image=bear_photo)
            self.hunting_canvas.tag_bind(bear_img, "<Button-1>", partial(self.hunting_clicked_on, _("熊胆"), bear_img))
            t = Thread(target=self.hunting_move_item, args=(bear_img, (20, 26)))
            t.start()


        moth_photo_list = []
        for i in range(moth_count):
            moth_photo = PhotoImage(file="Poisonous Moth{}_icon_small.ppm".format(i+1))
            moth_photo_list.append(moth_photo)
            moth_img = self.hunting_canvas.create_image(self.off_screen_x, self.off_screen_y, anchor=NW,
                                                        image=moth_photo)
            self.hunting_canvas.tag_bind(moth_img, "<Button-1>", partial(self.hunting_clicked_on, _("毒蛾") + str(i+1), moth_img))
            t = Thread(target=self.hunting_move_item, args=(moth_img, (7, 11)))
            t.start()


        self.huntingWin.after(65000, self.hunting_quit)
        self.huntingWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.huntingWin.mainloop()



    def hunting_quit(self):
        #print(self.hunting_results)
        for hunting_item in self.hunting_results:
            hunting_item_count = self.hunting_results[hunting_item]
            kept_amount = hunting_item_count//5 + randrange(2)
            if kept_amount > 0:
                if hunting_item in self.inv:
                    self.inv[hunting_item] += kept_amount
                else:
                    self.inv[hunting_item] = kept_amount

        self.hunting_ended = True
        self.huntingWin.quit()
        self.huntingWin.destroy()
        self.forestWin.deiconify()


    def hunting_clicked_on(self, object_name, hunting_canvas_tag, event):

        self.hunting_reset_object(hunting_canvas_tag)
        if _("蜘蛛") in object_name:
            self.currentEffect = pickOne(["insect1.mp3","insect2.mp3"])
            self.startSoundEffectThread()
        elif _("蛇") in object_name:
            self.currentEffect = "snakehiss.mp3"
            self.startSoundEffectThread()
        elif _("蜈蚣") in object_name:
            self.currentEffect = pickOne(["insect1.mp3","insect2.mp3"])
            self.startSoundEffectThread()
        elif _("熊") in object_name:
            self.currentEffect = "beargrowl.mp3"
            self.startSoundEffectThread()
        elif _("蛾") in object_name:
            self.currentEffect = pickOne(["insect1.mp3","insect2.mp3"])
            self.startSoundEffectThread()

        self.hunting_results[object_name] += 1


    def hunting_reset_object(self, hunting_canvas_tag):
        try:
            x1, y1, x2, y2 = self.hunting_canvas.bbox(hunting_canvas_tag)
            if x1 != self.off_screen_x and y1 != self.off_screen_y:
                self.hunting_canvas.move(hunting_canvas_tag, -(x1 - self.off_screen_x), -(y1 - self.off_screen_y))
        except:
            pass

    def hunting_move_item(self, hunting_canvas_tag, freq=(5, 11)):

        try:
            time.sleep(randrange(freq[0], freq[1]))

            on_screen_x = randrange(50, self.hunting_canvas_width - 50)
            on_screen_y = randrange(50, self.hunting_canvas_height - 50)

            self.hunting_canvas.move(hunting_canvas_tag, on_screen_x - self.off_screen_x,
                                     on_screen_y - self.off_screen_y)

            time.sleep(.5 + random())
            self.hunting_reset_object(hunting_canvas_tag)
            if not self.hunting_ended:
                self.hunting_move_item(hunting_canvas_tag, freq=freq)

        except:
            pass


    def go_cherry_blossom_island(self, training=False):

        print("Loading map... Please wait.")
        self.maze_movement_speed = 10
        self.maze_training = training
        self.maze_time_start = datetime.now()

        self.maze_canvas_width = 800
        self.maze_canvas_height = 600

        if training:
            self.mazeWin = Toplevel(self.cherry_blossom_island_train_win)
        else:
            self.mazeWin = Toplevel(self.mapWin)

        self.maze_canvas = Canvas(self.mazeWin, width=self.maze_canvas_width, height=self.maze_canvas_height,
                                  bg='black')
        self.maze_canvas.pack(side=LEFT)

        self.maze_instructions_frame = Frame(self.mazeWin, borderwidth = 2, relief = SUNKEN)
        self.maze_instructions_frame.pack(fill=Y)

        Label(self.maze_instructions_frame, text = _("Use the arrow keys to move.\nLook for the pavillion in the\nmiddle of the maze.")).pack()
        if self.maze_training:
            Button(self.maze_instructions_frame, text="Back", command=partial(self.winSwitch, self.mazeWin, self.cherry_blossom_island_train_win)).pack()
        else:
            Button(self.maze_instructions_frame, text="Back", command=partial(self.winSwitch, self.mazeWin, self.mapWin)).pack()



        bg_img = PhotoImage(file="maze_bg_stone_big2.png")
        self.bg_img = self.maze_canvas.create_image(self.maze_canvas_width - bg_img.width(),
                                                    self.maze_canvas_height - bg_img.height(),
                                                    anchor=NW, image=bg_img)

        self.walls = []
        wall_img = PhotoImage(file="cherry_blossom_piece.ppm")
        w = wall_img.width()
        h = wall_img.height()

        starting_x = self.maze_canvas_width - bg_img.width() - w // 2
        starting_y = self.maze_canvas_height - bg_img.height() - w // 2
        potential_targ_coords = []
        maze = generate_maze(bg_img.width() // w, bg_img.height() // h, .9, .9)

        for x in range(maze.shape[1]):
            for y in range(maze.shape[0]):
                if maze[y][x]:
                    wall = self.maze_canvas.create_image(starting_x + x * w, starting_y + y * h, anchor=NW,
                                                         image=wall_img)
                    self.walls.append(wall)
                else:
                    # neighbors = [maze[y][x-1], maze[y][x+1], maze[y-1][x], maze[y+1][x]]
                    if abs(
                            y - bg_img.height() // h // 2) <= 3 and x - bg_img.width() // w // 2 <= -5:  # and sum(neighbors) <= 2:
                        potential_targ_coords.append([x, y])

        targ_coords = choice(potential_targ_coords)
        targ_img = PhotoImage(file="cherry_blossom_island_hut.ppm")
        self.maze_targ = self.maze_canvas.create_image(starting_x + targ_coords[0] * w,
                                                       starting_y + targ_coords[1] * h, anchor=NW, image=targ_img)

        you_img = PhotoImage(file="you_icon_small.ppm")
        self.you_x, self.you_y = self.maze_canvas_width - you_img.width() - w // 2 - 5, self.maze_canvas_height - you_img.height() - h // 2 - 5
        self.you_img = self.maze_canvas.create_image(self.you_x, self.you_y, anchor=NW, image=you_img)

        self.mazeWin.bind('<Key>', self.move)
        self.mazeWin.mainloop()

    def reached_targ(self):
        x1, y1, x2, y2 = self.maze_canvas.bbox(self.you_img)
        tx1, ty1, tx2, ty2 = self.maze_canvas.bbox(self.maze_targ)
        return ((x1 >= tx1 and x1 <= tx2) or (x2 >= tx1 and x2 <= tx2)) and (
                    (y1 >= ty1 and y1 <= ty2) or (y2 >= ty1 and y2 <= ty2))

    def collision_detection(self, dx, dy):
        x1, y1, x2, y2 = self.maze_canvas.bbox(self.you_img)
        for wall in self.walls:
            wx1, wy1, wx2, wy2 = self.maze_canvas.bbox(wall)

            if dx > 0:
                if x2 + dx >= wx1 and x2 + dx <= wx2 and ((y1 >= wy1 and y1 <= wy2) or (y2 >= wy1 and y2 <= wy2)):
                    return True
            if dx < 0:
                if x1 + dx <= wx2 and x1 + dx >= wx1 and ((y1 >= wy1 and y1 <= wy2) or (y2 >= wy1 and y2 <= wy2)):
                    return True

            if dy > 0:
                if y2 + dy >= wy1 and y2 + dy <= wy2 and ((x1 >= wx1 and x1 <= wx2) or (x2 <= wx2 and x2 >= wx1)):
                    return True
            if dy < 0:
                if y1 + dy <= wy2 and y1 + dy >= wy1 and ((x1 >= wx1 and x1 <= wx2) or (x2 <= wx2 and x2 >= wx1)):
                    return True

    def bg_boundary_detection(self, dx, dy):

        x1, y1, x2, y2 = self.maze_canvas.bbox(self.you_img)
        bx1, by1, bx2, by2 = self.maze_canvas.bbox(self.bg_img)

        current_x = (x1 + x2) // 2
        current_y = (y1 + y2) // 2

        if dx > 0:
            if current_x + self.maze_canvas_width // 2 + (
                    x2 - x1) // 2 + dx > bx2 or current_x - self.maze_canvas_width // 2 - (
                    x2 - x1) // 2 + 25 < bx1:
                return "x"
        if dx < 0:
            if current_x - self.maze_canvas_width // 2 - (
                    x2 - x1) // 2 + dx + 25 < bx1 or current_x + self.maze_canvas_width // 2 + (x2 - x1) // 2 > bx2:
                return "x"

        if dy > 0:
            if current_y + (y2 - y1) // 2 + self.maze_canvas_height // 2 + dy > by2 or current_y - (
                    y2 - y1) // 2 - self.maze_canvas_height // 2 + 25 < by1:
                return "y"
        if dy < 0:
            if current_y - (y2 - y1) // 2 - self.maze_canvas_height // 2 + dy + 25 < by1 or current_y + (
                    y2 - y1) // 2 + self.maze_canvas_height // 2 > by2:
                return "y"

    def obj_boundary_detection(self, obj, dx, dy):
        x1, y1, x2, y2 = self.maze_canvas.bbox(obj)
        current_x = (x1 + x2) // 2
        current_y = (y1 + y2) // 2
        obj_w = x2 - x1
        obj_h = y2 - y1
        targ_x = current_x + dx
        targ_y = current_y + dy
        if targ_x + obj_w // 2 > self.maze_canvas_width or targ_x - obj_w // 2 < 0:
            return "x"
        elif targ_y + obj_h // 2 > self.maze_canvas_height or targ_y - obj_h // 2 < 0:
            return "y"
        else:
            return

    def move(self, event):

        if event.keysym == "Left":
            if not self.bg_boundary_detection(-self.maze_movement_speed, 0):
                if not self.collision_detection(-self.maze_movement_speed, 0):
                    self.maze_canvas.move(self.bg_img, self.maze_movement_speed, 0)
                    for item in [self.maze_targ] + self.walls:
                        self.maze_canvas.move(item, self.maze_movement_speed, 0)

            elif not self.obj_boundary_detection(self.you_img, -self.maze_movement_speed,
                                                 0) and not self.collision_detection(
                    -self.maze_movement_speed, 0):
                self.maze_canvas.move(self.you_img, -self.maze_movement_speed, 0)

        elif event.keysym == "Right":
            if not self.bg_boundary_detection(self.maze_movement_speed, 0):
                if not self.collision_detection(self.maze_movement_speed, 0):
                    self.maze_canvas.move(self.bg_img, -self.maze_movement_speed, 0)
                    for item in [self.maze_targ] + self.walls:
                        self.maze_canvas.move(item, -self.maze_movement_speed, 0)

            elif not self.obj_boundary_detection(self.you_img, self.maze_movement_speed,
                                                 0) and not self.collision_detection(
                    self.maze_movement_speed, 0):
                self.maze_canvas.move(self.you_img, self.maze_movement_speed, 0)

        elif event.keysym == "Down":
            if not self.bg_boundary_detection(0, self.maze_movement_speed):
                if not self.collision_detection(0, self.maze_movement_speed):
                    self.maze_canvas.move(self.bg_img, 0, -self.maze_movement_speed)
                    for item in [self.maze_targ] + self.walls:
                        self.maze_canvas.move(item, 0, -self.maze_movement_speed)

            elif not self.obj_boundary_detection(self.you_img, 0,
                                                 self.maze_movement_speed) and not self.collision_detection(0,
                                                                                                            self.maze_movement_speed):
                self.maze_canvas.move(self.you_img, 0, self.maze_movement_speed)

        elif event.keysym == "Up":
            if not self.bg_boundary_detection(0, -self.maze_movement_speed):
                if not self.collision_detection(0, -self.maze_movement_speed):
                    self.maze_canvas.move(self.bg_img, 0, self.maze_movement_speed)
                    for item in [self.maze_targ] + self.walls:
                        self.maze_canvas.move(item, 0, self.maze_movement_speed)

            elif not self.obj_boundary_detection(self.you_img, 0,
                                                 -self.maze_movement_speed) and not self.collision_detection(0,
                                                                                                             -self.maze_movement_speed):
                self.maze_canvas.move(self.you_img, 0, -self.maze_movement_speed)

        if self.reached_targ():

            dd = datetime.now() - self.maze_time_start
            print("Spent ", dd.seconds)

            if self.maze_training:
                self.mazeWin.quit()
                self.mazeWin.destroy()

                if dd.seconds <= 45:
                    if self.luck*1.5 >= random()*100 and _("樱花水上漂") not in [skill.name for skill in self.skills]:
                        self.learn_skill(skill(_("樱花水上漂")))
                        messagebox.showinfo("", _("Huang Yufei taught you an agility technique: 'Cherry Blossoms Floating on the Water'"))
                    else:
                        self.speed += 2
                        self.dexterity += 2
                        messagebox.showinfo("", _("After running through the maze, you feel lighter and swifter. Your speed +2, dexterity +2."))


                elif dd.seconds <= 60:
                    if self.luck >= random()*100 and _("樱花水上漂") not in [skill.name for skill in self.skills]:
                        self.learn_skill(skill(_("樱花水上漂")))
                        messagebox.showinfo("", _("Huang Yufei taught you an agility technique: 'Cherry Blossoms Floating on the Water'"))
                    else:
                        self.speed += 1
                        self.dexterity += 1
                        messagebox.showinfo("", _("After running through the maze, you feel lighter and swifter. Your speed +1, dexterity +1."))

                elif dd.seconds <= 90:
                    self.speed += 1
                    self.dexterity += 1
                    messagebox.showinfo("", _("After running through the maze, you feel lighter and swifter. Your speed +1, dexterity +1."))

                self.cherry_blossom_island_train_win.deiconify()

            else:
                self.mazeWin.quit()
                self.mazeWin.destroy()
                self.mapWin.deiconify()

    #############################################
    #############################################
    #############MENU FUNCTIONS##################
    #############################################
    #############################################

    def checkInv(self, fromWin, purpose="normal"):

        if purpose == "battle":
            return

        try:
            if self.invWin.winfo_exists():
                return
            else:
                raise

        except:
            self.currentEffect = "button-20.mp3"
            self.startSoundEffectThread()

            self.invWin = Toplevel(fromWin)
            F0 = Frame(self.invWin)
            F0.pack()
            F1 = Frame(F0)
            F1.pack(side=LEFT)
            F2 = Frame(F0, borderwidth=3, relief=SOLID)
            F2.pack(side=RIGHT, fill=Y)

            width = 9
            height = 5
            item_x = 0
            item_y = 0

            self.item_buttons = []
            self.item_icons = []
            self.item_quantity_labels = []
            self.selected_item = None
            self.selected_item_index = None

            for i in range(width * height):
                if i < len(self.inv):
                    item_name = list(self.inv.keys())[i]
                    item_icon = PhotoImage(file=_("i_{}.ppm").format(item_name))
                    item_button = Button(F1, image=item_icon, command=partial(self.selectItem, item_name, i))
                    item_button.image = item_icon
                    item_button.grid(row=item_y, column=item_x)

                    self.item_icons.append(item_icon)
                    self.item_buttons.append(item_button)

                    quantity_label = Label(text="x" + str(self.inv[item_name]), master=F1)
                    quantity_label.grid(row=item_y + 1, column=item_x)
                    self.item_quantity_labels.append(quantity_label)

                else:
                    item_icon = PhotoImage(file="Blank.ppm")
                    item_button = Button(F1, image=item_icon)
                    item_button.image = item_icon
                    item_button.grid(row=item_y, column=item_x)
                    self.item_buttons.append(item_button)

                    quantity_label = Label(text=" ", master=F1)
                    quantity_label.grid(row=item_y + 1, column=item_x)

                item_x += 1
                if item_x == width:
                    item_x = 0
                    item_y += 2

            F2_0 = Frame(F2)
            F2_0.pack(side=TOP)
            self.item_description_label = Label(F2_0,
                                                text=_("点击物品观看基本信息"),
                                                wraplength=300, font=("TkDefaultFont", 24), width=20)
            self.item_description_label.pack()

            F2_1 = Frame(F2)
            F2_1.pack(side=BOTTOM)
            Button(F2_1, text=_("使用"), font=("TkDefaultFont", 24), command=self.useSelectedItem).grid(row=0, column=0)
            Label(F2_1, text="  ", font=("TkDefaultFont", 24)).grid(row=0, column=1)
            Button(F2_1, text=_("卖"), font=("TkDefaultFont", 24), command=self.sellSelectedItem).grid(row=0,
                                                                                                         column=2)

            # Button(self.invWin, text=_("返回"), command=partial(self.winSwitch, self.invWin, fromWin)).pack()

            self.invWin.mainloop()

    def selectItem(self, item_name, item_index):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.selected_item:
            self.item_icons[self.selected_item_index] = PhotoImage(file=_("i_{}.ppm").format(self.selected_item))
            self.item_buttons[self.selected_item_index].config(image=self.item_icons[self.selected_item_index])

        self.selected_item = item_name
        self.selected_item_index = item_index

        self.item_icons[self.selected_item_index] = PhotoImage(file=_("i_{}_selected.ppm").format(self.selected_item))
        self.item_buttons[self.selected_item_index].config(image=self.item_icons[self.selected_item_index])
        self.item_description_label.config(text=item_name + "\n--------------------------\n"
                                                + ITEM_DESCRIPTIONS[self.selected_item])

    def useSelectedItem(self, quantity=1):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.selected_item:
            if self.inv[self.selected_item] > 0:
                self.inv[self.selected_item] -= quantity
                self.item_quantity_labels[self.selected_item_index].config(text="x" + str(self.inv[self.selected_item]))

                if self.selected_item == _("螃蟹"):
                    self.restore(h=50, s=50, full=False)
                elif self.selected_item == _("鱼"):
                    self.restore(h=30, s=30, full=False)
                elif self.selected_item == _("汤"):
                    self.restore(h=70, s=0, full=False)
                elif self.selected_item == _("包子"):
                    self.restore(h=0, s=100, full=False)
                elif self.selected_item == _("樱花木瓜银耳汤"):
                    self.restore(h=int(self.healthMax*.15), s=int(self.staminaMax*.20), full=False)
                elif self.selected_item == _("蛇心"):
                    self.healthMax += 10
                elif self.selected_item == _("熊胆"):
                    self.staminaMax += 15
                else:
                    messagebox.showinfo("", _("Cannot use item."))

                try:
                    self.updateProfileLabels()
                except:
                    pass

            else:
                messagebox.showinfo("", _("这个物品已经用光了"))
        else:
            messagebox.showinfo("", _("未选择任何物品"))


    def sellSelectedItem(self, quantity=1):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.selected_item:
            if self.inv[self.selected_item] > 0:
                print("Sell item", self.selected_item)
                self.inv[self.selected_item] -= quantity
                self.item_quantity_labels[self.selected_item_index].config(text="x" + str(self.inv[self.selected_item]))
            else:
                messagebox.showinfo("", _("这个物品已经用光了"))
        else:
            messagebox.showinfo("", _("未选择任何物品"))

    def settings(self, fromWin):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        fromWin.withdraw()

        self.settingsWin = Toplevel(fromWin)
        self.settingsWin.title(_("设置"))
        F1 = Frame(self.settingsWin, borderwidth=2, relief=SOLID)
        F1.pack()
        F2 = Frame(self.settingsWin)
        F2.pack()

        Checkbutton(master=F1, text=_("音乐"), variable=self.soundtrackOn, onvalue=1, offvalue=0).pack(anchor=W)
        Checkbutton(master=F1, text=_("音效"), variable=self.soundEffectOn, onvalue=1, offvalue=0).pack(
            anchor=W)

        B1 = Button(F2, text=_("返回"), command=partial(self.winSwitch, self.settingsWin, fromWin))
        B1.grid(row=0, column=0)
        B2 = Button(F2, text=_("确认"), command=self.confirm_settings)
        B2.grid(row=0, column=1)

        self.settingsWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.settingsWin.mainloop()

    def confirm_settings(self):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if not self.soundtrackOn.get():
            self.stopSoundtrack()

        messagebox.showinfo("", _("已保存设置"))

    def useUpgradePoints(self, reason, quantity=1, update_profile_labels=True):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if self.upgradePoints < 1:
            messagebox.showinfo("", _("No upgrade points left."))
            return

        if reason == "Attack":
            self.attack += 1
        elif reason == "Strength":
            self.strength += 1
        elif reason == "Speed":
            self.speed += 1
        elif reason == "Defence":
            self.defence += 1
        elif reason == "Dexterity":
            self.dexterity += 1
        elif reason == "Health":
            self.healthMax += 20
        elif reason == "Stamina":
            self.staminaMax += 20

        self.upgradePoints -= quantity
        self.updateProfileLabels()

    def cheatCode(self):
        userInput = input("Enter cheat code and press enter:")

        if "coinconfig " in userInput:
            self.inv[_("金钱")] = int(userInput.split("coinconfig ")[1])

        elif "upgradeconfig " in userInput:
            self.upgradePoints = int(userInput.split("upgradeconfig ")[1])

        elif "chivalryconfig " in userInput:
            self.chivalry = int(userInput.split("chivalryconfig ")[1])

        elif userInput == "gimmefood":
            try:
                self.inv[_('Soup')] = 5
                self.inv[_('Dumplings')] = 5
                self.inv[_('Crab')] = 5
                self.inv[_('Fish')] = 5
            except:
                print("Invalid input.")

        elif "learn " in userInput:
            if "level " in userInput:
                move_level = int(userInput.split("level ")[1])
            else:
                move_level = 1
            move_name = userInput.split("learn ")[1].split("level ")[0].strip()
            new_move = special_move(name=move_name, level=move_level)
            self.learn_move(new_move)
            print("Learned", new_move.name)

        elif userInput == "forgetmove":
            self.sml.pop(len(self.sml) - 4)

        elif "setstatus " in userInput:
            self.status = userInput.split("setstatus ")[1]

    def updateProfileLabels(self):
        self.profile_L0.config(text=_("姓名: {}").format(self.character))
        self.profile_L1.config(text=_("等级: {}").format(int(self.level)))
        self.profile_L2.config(text=_("经验: {}").format(int(self.exp), self.levelUpThreshold))
        self.profile_L3.config(text=_("修为点: {}").format(int(self.upgradePoints)))
        self.profile_L4.config(text=_("状态: {}").format(self.status))

        self.profile_L6.config(text=_("气血: {}/{}").format(int(self.health), int(self.healthMax)))
        self.profile_L7.config(text=_("内力: {}/{}").format(int(self.stamina), int(self.staminaMax)))
        self.profile_L8.config(text=_("攻击: {}").format(int(self.attack)))
        self.profile_L9.config(text=_("力量: {}").format(int(self.strength)))
        self.profile_L10.config(text=_("速度: {}").format(int(self.speed)))
        self.profile_L11.config(text=_("防御: {}").format(int(self.defence)))
        self.profile_L12.config(text=_("敏捷: {}").format(int(self.dexterity)))

        self.profile_L13.config(text=_("悟性: {}").format(int(self.perception)))
        self.profile_L14.config(text=_("福缘: {}").format(int(self.luck)))
        self.profile_L15.config(text=_("侠义值: {}").format(int(self.chivalry)))

    def checkProfile(self, fromWin):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        try:
            if self.profileWin.winfo_exists():
                return
            else:
                raise

        except:

            self.profileWin = Toplevel(fromWin)

            self.stat_frame = Frame(self.profileWin)
            self.stat_frame.pack(side=LEFT)

            self.skill_move_frame = Frame(self.profileWin, borderwidth = 3, relief=SOLID)
            self.skill_move_frame.pack(side=RIGHT, expand=Y, fill=Y)


            F0 = Frame(self.stat_frame)
            F0.pack(side=TOP)

            F0_0 = Frame(F0)
            F0_0.grid(row=0, column=0)

            self.photo1 = PhotoImage(file="you.ppm", master=self.profileWin)
            pic1 = Label(F0_0, image=self.photo1)
            pic1.pack(side=LEFT)

            F0_1 = Frame(F0)
            F0_1.grid(row=0, column=1)
            F1 = Frame(F0_1)
            F1.pack()
            self.levelUpThreshold = self.levelThresholds[self.level - 1]

            self.profile_L0 = Label(F1, text=_("姓名: {}").format(self.character))
            self.profile_L1 = Label(F1, text=_("等级: {}").format(int(self.level)))
            self.profile_L2 = Label(F1, text=_("经验: {}").format(int(self.exp), self.levelUpThreshold))
            self.profile_L3 = Label(F1, text=_("修为点: {}").format(int(self.upgradePoints)))
            self.profile_L4 = Label(F1, text=_("状态: {}").format(self.status))

            self.profile_L0.grid(row=0, column=0, sticky=W)
            self.profile_L1.grid(row=1, column=0, sticky=W)
            self.profile_L2.grid(row=2, column=0, sticky=W)
            self.profile_L3.grid(row=3, column=0, sticky=W)
            self.profile_L4.grid(row=4, column=0, sticky=W)

            F0_2 = Frame(F0)
            F0_2.grid(row=0, column=2)
            # Label(F0_2, text="\t").pack(side=TOP)
            # Label(F0_2, text="\t").pack(side=TOP)
            # inventory_icon = PhotoImage(file="inventory_icon.ppm")
            # imageButton1 = Button(F0_2, command=partial(self.checkInv,self.profileWin))
            # imageButton1.pack(side=BOTTOM)
            # imageButton1.config(image=inventory_icon)

            F2 = Frame(self.stat_frame, borderwidth=3, relief=SOLID)
            F2.pack()
            self.balloon.bind(F2, _("点击绿色+按钮使用修为点.\n每个修为点可以使气血或内\n力上限提高20分或其他属\n性提高1分."))

            self.profile_L6 = Label(F2, text=_("气血: {}/{}").format(int(self.health), int(self.healthMax)))
            self.profile_L7 = Label(F2, text=_("内力: {}/{}").format(int(self.stamina), int(self.staminaMax)))
            self.profile_L8 = Label(F2, text=_("攻击: {}").format(int(self.attack)))
            self.profile_L9 = Label(F2, text=_("力量: {}").format(int(self.strength)))
            self.profile_L10 = Label(F2, text=_("速度: {}").format(int(self.speed)))
            self.profile_L11 = Label(F2, text=_("防御: {}").format(int(self.defence)))
            self.profile_L12 = Label(F2, text=_("敏捷: {}").format(int(self.dexterity)))

            self.profile_L6.grid(row=0, column=0, sticky=W)
            self.profile_L7.grid(row=1, column=0, sticky=W)
            self.profile_L8.grid(row=2, column=0, sticky=W)
            self.profile_L9.grid(row=3, column=0, sticky=W)
            self.profile_L10.grid(row=4, column=0, sticky=W)
            self.profile_L11.grid(row=5, column=0, sticky=W)
            self.profile_L12.grid(row=6, column=0, sticky=W)

            add_icon = PhotoImage(file="add_button_small.ppm")
            Button(F2, command=partial(self.useUpgradePoints, "Health"), image=add_icon).grid(row=0, column=1, sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Stamina"), image=add_icon).grid(row=1, column=1,
                                                                                               sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Attack"), image=add_icon).grid(row=2, column=1, sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Strength"), image=add_icon).grid(row=3, column=1,
                                                                                                sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Speed"), image=add_icon).grid(row=4, column=1, sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Defence"), image=add_icon).grid(row=5, column=1,
                                                                                               sticky=W)
            Button(F2, command=partial(self.useUpgradePoints, "Dexterity"), image=add_icon).grid(row=6, column=1,
                                                                                                 sticky=W)

            Label(F2, text="  ").grid(row=0, column=2)

            self.profile_L13 = Label(F2, text=_("悟性: {}").format(int(self.perception)))
            self.profile_L14 = Label(F2, text=_("福缘: {}").format(int(self.luck)))
            self.profile_L15 = Label(F2, text=_("侠义值: {}").format(int(self.chivalry)))
            self.profile_L13.grid(row=0, column=3, sticky=W)
            self.profile_L14.grid(row=1, column=3, sticky=W)
            self.profile_L15.grid(row=2, column=3, sticky=W)

            # B1 = Button(self.profileWin, text=_("返回"), command=partial(self.winSwitch, self.profileWin, fromWin))
            # B1.pack(side=BOTTOM)

            skill_move_image_list = []
            Label(self.skill_move_frame, text=_("外功招式")).pack()
            moves_frame = Frame(self.skill_move_frame, borderwidth = 1, relief=SUNKEN)
            moves_frame.pack()

            frame_width = 8
            frame_x = 0
            frame_y = 0
            for i in range(len(self.sml)-3):
                move = self.sml[i]
                move_image = PhotoImage(file=move.file_name)
                move_image_label = Button(moves_frame, image=move_image)
                move_image_label.grid(row=frame_y, column=frame_x)
                move_image_label.config(image=move_image)
                skill_move_image_list.append(move_image)
                self.balloon.bind(move_image_label, move.description)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1




            Label(self.skill_move_frame, text=_("内功/轻功")).pack()
            skills_frame = Frame(self.skill_move_frame)
            skills_frame.pack()

            frame_width = 8
            frame_x = 0
            frame_y = 0

            self.profile_skill_labels = []
            for i in range(len(self.skills)):
                s_frame = Frame(skills_frame, borderwidth = 1, relief=SUNKEN)
                s_frame.grid(row=frame_y, column=frame_x)

                skill = self.skills[i]
                skill_image = PhotoImage(file=skill.file_name)
                skill_image_label = Button(s_frame, image=skill_image)
                skill_image_label.pack()
                skill_image_label.config(image=skill_image)
                skill_move_image_list.append(skill_image)
                upgrade_skill_button = Button(s_frame, text="Upgrade", command=partial(self.upgradeSkill, skill))
                upgrade_skill_button.pack()
                self.balloon.bind(skill_image_label, skill.description)
                self.balloon.bind(upgrade_skill_button, _("Spend 1 upgrade point to upgrade this skill."))
                self.profile_skill_labels.append(skill_image_label)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1

            self.profileWin.mainloop()



    def upgradeSkill(self,skill):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        if skill.level >= 10:
            messagebox.showinfo("", _("Cannot upgrade skill beyond level 10."))
        elif self.upgradePoints < 1:
            messagebox.showinfo("", _("Not enough upgrade points."))
        else:
            self.useUpgradePoints(reason="Other")
            skill.upgrade_skill(1)
            messagebox.showinfo("", _("Upgraded '{}' to level {}.".format(skill.name, skill.level)))
            self.balloon.bind(self.profile_skill_labels[self.skills.index(skill)], skill.description)



    def create_self_character(self):
        self.you = character(self.character, self.level, [self.health, self.healthMax,
                                                          self.stamina, self.staminaMax, self.attack,
                                                          self.strength, self.speed, self.defence,
                                                          self.dexterity], self.sml, self.skills, [],
                             status=self.status)

    def generate_dialogue_sequence(self, fromWin, bg, dialogue_list, speaker_list, options=None,
                                   bind_text=None):

        self.dialogue_speaker = speaker_list[0]
        self.dialogue_list = dialogue_list
        self.speaker_list = speaker_list
        self.dialogue_length = len(self.dialogue_list)
        self.dialogue_count = 1  # we are displaying 0th index at beginning
        self.dialogueFromWin = fromWin
        self.dialogue_options = options
        if self.dialogueFromWin:
            self.dialogueFromWin.withdraw()

        self.dialogueWin = Toplevel(self.mainWin)

        dialogue_bg = PhotoImage(file=bg)
        canvas = Canvas(self.dialogueWin, width=dialogue_bg.width() - 1, height=dialogue_bg.height() - 1, bg='white')
        canvas.create_image(0, 0, anchor=NW, image=dialogue_bg)
        canvas.pack()
        self.dialogue_display_frame = Frame(self.dialogueWin)
        self.dialogue_display_frame.pack()
        self.dialogue_options_frame = Frame(self.dialogueWin)
        self.dialogue_options_frame.pack()

        self.speaker_avatar_image = PhotoImage(file="{}_icon.ppm".format(self.dialogue_speaker))
        self.speaker_avatar_label = Label(self.dialogue_display_frame, image=self.speaker_avatar_image)
        self.speaker_avatar_label.grid(row=0, column=0)
        Label(self.dialogue_display_frame, text="  ").grid(row=0, column=1)
        if os.name == "nt":
            self.dialogue_label = Label(self.dialogue_display_frame, text=self.dialogue_list[0], wraplength=270,
                                        width=30, height=6, relief=SUNKEN, borderwidth=2, font=("TkDefaultFont", 12))
        else:
            self.dialogue_label = Label(self.dialogue_display_frame, text=self.dialogue_list[0], wraplength=350,
                                        width=30, height=4, relief=SUNKEN, borderwidth=2, font=("TkDefaultFont", 18))
        self.dialogue_label.grid(row=0, column=2)

        self.dialogueWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.dialogueWin.bind("<Button-1>", self.advance_dialogue)
        if bind_text:
            self.balloon.bind(self.dialogueWin, bind_text)
        self.dialogueWin.mainloop()

    def advance_dialogue(self, event):

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        self.speaker_avatar_image = PhotoImage(file="{}_icon.ppm".format(self.dialogue_speaker))

        if self.dialogue_count < self.dialogue_length:
            self.dialogue_speaker = self.speaker_list[self.dialogue_count]
            self.speaker_avatar_image = PhotoImage(file="{}_icon.ppm".format(self.dialogue_speaker))
            self.speaker_avatar_label.config(image=self.speaker_avatar_image)
            self.dialogue_label.config(text=self.dialogue_list[self.dialogue_count])

        elif self.dialogue_count == self.dialogue_length:
            if self.dialogue_options:
                self.dialogue_display_frame.pack_forget()
                for option in self.dialogue_options:
                    Button(self.dialogue_options_frame, text=option[0], command=option[1]).pack()
            else:
                self.dialogueWin.quit()
                self.dialogueWin.destroy()
                if self.dialogueFromWin:
                    self.dialogueFromWin.deiconify()

        self.dialogue_count += 1

    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    ##     BATTLE    BATTLE    BATTLE    BATTLE    BATTLE    BATTLE    BATTLE    BATTLE  ##
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################

    def battleMenu(self, you, opp, bg_file_name, postBattleSoundtrack, fromWin, battleType="normal",
                   destinationWinList=None, postBattleCmd=None):

        sleepTime = 1
        time.sleep(sleepTime)

        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()
        self.stopSoundtrack()
        self.postBattleSoundtrack = postBattleSoundtrack
        self.battleType = battleType
        if self.battleType in ("normal", "training"):
            self.currentBGM = pickOne(["fangshiyu.mp3", "huangfeihong.mp3", "chushitaiji.mp3", "jy_battle2.mp3", "jy_battle5.mp3"])
            followup = pickOne(["fangshiyu.mp3", "huangfeihong.mp3", "chushitaiji.mp3", "jy_battle2.mp3", "jy_battle5.mp3"])
            if self.currentBGM == "fangshiyu.mp3":
                followupDelay = 152
            elif self.currentBGM == "huangfeihong.mp3":
                followupDelay = 177
            elif self.currentBGM == "chushitaiji.mp3":
                followupDelay = 98
            elif self.currentBGM == "jy_battle2.mp3":
                followupDelay = 114
            elif self.currentBGM == "jy_battle5.mp3":
                followupDelay = 114


        elif self.battleType in ("test"):
            self.currentBGM = pickOne(["xianjianqiyuan.mp3"])
            followup = pickOne(["xianjianqiyuan.mp3"])
            if self.currentBGM == "xianjianqiyuan.mp3":
                followupDelay = 156

        self.startSoundtrackThread(followup=followup, followupDelay=followupDelay)

        self.battleFromWin = fromWin
        if self.battleFromWin:
            self.battleFromWin.withdraw()
        if not destinationWinList:
            self.destinationWinList = [None, None, None]
        else:
            self.destinationWinList = destinationWinList
        self.postBattleCmd = postBattleCmd

        self.create_self_character()
        # self.you = copy(you)
        self.opp = copy(opp)

        self.turnEnded = False
        self.battleEnded = False

        self.youHaveMoved = False
        self.oppHasMoved = False
        self.youActionPoints = self.you.speed
        self.oppActionPoints = self.opp.speed

        self.oppVengeance = False
        self.vengeance = False
        self.youAcupunctureCount = 0
        self.oppAcupunctureCount = 0

        # apply any bonuses from skills
        self.apply_skill_bonus(self.you)
        self.apply_skill_bonus(self.opp)

        ###################

        self.accuracy = (self.you.attack - self.opp.dexterity + 25) / 30
        self.damage_scale = (self.you.strength / self.opp.defence)

        self.oppaccuracy = (self.opp.attack - self.you.dexterity + 25) / 30
        self.oppdamage_scale = (self.opp.strength / self.you.defence)

        ###################

        if self.accuracy <= .3:
            self.accuracy = .3

        if self.damage_scale <= .2:
            self.damage_scale = .2

        if self.oppaccuracy <= .3:
            self.oppaccuracy = .3

        if self.oppdamage_scale <= .2:
            self.oppdamage_scale = .2

        print(self.accuracy, self.damage_scale)
        print(self.oppaccuracy, self.oppdamage_scale)

        self.callAfterStatChange()

        # update exp gain rate
        for move in self.sml:
            move.exp_rate = int(10 * (self.perception / 50))

        self.battleWin = Toplevel(self.battleFromWin)

        self.you_x, self.you_y = 100, 50
        self.opp_x, self.opp_y = 336, 50
        self.animation_target = 1
        self.animation_x = -300
        self.animation_y = -300
        self.animation_in_progress = False
        self.animation_frame_delta_dic = {
            _("菜鸟拳"): 200,
            _("五毒掌"): 200,
            _("火焰掌"): 200,
            _("寒冰掌"): 200,
            _("少林罗汉拳"): 100,
            _("落樱指法"): 50,
            _("基本剑法"): 40,
            _("Cherry Blossom Drifting Snow Blade"): 70,
            _("Dodge"): 100,
            _("休息"): 100,
            _("逃跑"): 100
        }
        self.you_animation_index = 0
        self.opp_animation_index = 0
        self.you_animation_list = []
        self.you_animation_img_list = []
        self.opp_animation_list = []
        self.opp_animation_img_list = []
        self.animation_buttons = []
        self.animation_buttons_img_list = []

        map_image = PhotoImage(file=bg_file_name)
        self.canvas = Canvas(self.battleWin, width=map_image.width() - 1, height=map_image.height() - 1, bg='white')
        self.canvas.create_image(0, 0, anchor=NW, image=map_image)


        you_img = PhotoImage(file="you_icon.ppm")
        you_canvas_img = self.canvas.create_image(self.you_x, self.you_y, anchor=NW, image=you_img)
        opp_img = PhotoImage(file=_("{}_icon.ppm").format(self.opp.name))
        opp_canvas_img = self.canvas.create_image(self.opp_x, self.opp_y, anchor=NW, image=opp_img)
        self.canvas.tag_bind(you_canvas_img,"<Button-1>",self.battleCheckProfile)
        self.canvas.tag_bind(opp_canvas_img,"<Button-1>",self.oppBattleCheckProfile)


        F1 = Frame(self.canvas, borderwidth=2, relief=SUNKEN)
        F1.pack()
        F2 = Frame(self.canvas, borderwidth=2, relief=SUNKEN)
        F2.pack()
        self.nameLabel1 = Label(F1, text=_("{} (Level {})").format(self.you.name, self.you.level))
        self.nameLabel1.grid(row=0, column=0, sticky=W)
        self.hptext = _("气血: {}/{}").format(int(self.you.health), int(self.you.healthMax))
        self.staminatext = _("内力: {}/{}").format(int(self.you.stamina), int(self.you.staminaMax))
        self.statustext = _("状态: {}").format(self.you.status)

        self.hplabel = Label(F1, text=self.hptext)
        self.hplabel.grid(row=2, column=0, sticky=W)
        self.staminalabel = Label(F1, text=self.staminatext)
        self.staminalabel.grid(row=3, column=0, sticky=W)
        self.statuslabel = Label(F1, text=self.statustext)
        self.statuslabel.grid(row=4, column=0, sticky=W)

        #############################################################################

        self.nameLabel2 = Label(F2, text=_("{} (Level {})").format(self.opp.name, self.opp.level))
        self.nameLabel2.grid(row=0, column=0, sticky=W)
        self.opphptext = _("气血: {}/{}").format(int(self.opp.health), int(self.opp.healthMax))
        self.oppstaminatext = _("内力: {}/{}").format(int(self.opp.stamina), int(self.opp.staminaMax))
        self.oppstatustext = _("状态: {}").format(self.opp.status)

        self.opphplabel = Label(F2, text=self.opphptext)
        self.opphplabel.grid(row=2, column=0, sticky=W)
        self.oppstaminalabel = Label(F2, text=self.oppstaminatext)
        self.oppstaminalabel.grid(row=3, column=0, sticky=W)
        self.oppstatuslabel = Label(F2, text=self.oppstatustext)
        self.oppstatuslabel.grid(row=4, column=0, sticky=W)

        F1.place(x=self.you_x - 32, y=self.you_y + 80)
        F2.place(x=self.opp_x - 32, y=self.opp_y + 80)
        self.balloon.bind(F1, "Click on your picture to view detailed battle-related stats.")
        self.balloon.bind(F2, "Click on the opponent's picture to view detailed battle-related stats.")

        self.canvas.pack()

        self.move_image_button_frame = Frame(self.battleWin)
        self.move_image_button_frame.pack()
        button_frame_width = 6
        button_frame_x = 0
        button_frame_y = 0
        for i in range(len(self.sml)):
            move = self.sml[i]
            move_image = PhotoImage(file=move.file_name)
            move_image_button = Button(self.move_image_button_frame, command=partial(self.move_buffer_zone, move))
            move_image_button.grid(row=button_frame_y, column=button_frame_x)
            move_image_button.config(image=move_image)
            self.animation_buttons_img_list.append(move_image)
            self.animation_buttons.append(move_image_button)
            self.balloon.bind(move_image_button, move.description)
            button_frame_x += 1
            if button_frame_x >= button_frame_width:
                button_frame_x = 0
                button_frame_y += 1

        self.battleWin.protocol("WM_DELETE_WINDOW", self.on_exit)
        self.battleWin.mainloop()


    def apply_skill_bonus(self, targ_obj):
        skills = targ_obj.skills
        for skill in skills:
            skill_level = skill.level
            if skill.name == _("樱花水上漂"):
                targ_obj.speed += 2 * skill_level
                targ_obj.dexterity += skill_level

    def update_move_button_tooltips(self, move):
        self.balloon.bind(self.animation_buttons[self.sml.index(move)], move.description)

    def load_animation(self, move_name, animation_file_list, target=1):

        self.animation_target = target
        if animation_file_list:

            if self.animation_target:
                self.animation_x, self.animation_y = self.opp_x, self.opp_y
            else:
                self.animation_x, self.animation_y = self.you_x, self.you_y

            for img_file in animation_file_list[::-1]:
                img = PhotoImage(file=img_file)
                canvas_img = self.canvas.create_image(self.animation_x, self.animation_y, anchor=NW, image=img)
                if self.animation_target:
                    self.you_animation_img_list.append(img)
                    self.you_animation_list.append(canvas_img)
                else:
                    self.opp_animation_img_list.append(img)
                    self.opp_animation_list.append(canvas_img)

            frame_delta = self.animation_frame_delta_dic[move_name]
            self.animate(frame_delta)

    def animation_clean_up(self):

        if self.animation_target:
            self.you_animation_list = []
            self.you_animation_img_list = []
            self.you_animation_index = 0
        else:
            self.opp_animation_list = []
            self.opp_animation_img_list = []
            self.opp_animation_index = 0

    def animate(self, frame_delta):
        self.animation_in_progress = True
        if self.animation_target:
            self.canvas.delete(self.you_animation_list[::-1][self.you_animation_index])
            self.you_animation_index += 1
            if self.you_animation_index < len(self.you_animation_list):
                self.canvas.after(frame_delta, self.animate, frame_delta)
            else:
                self.animation_clean_up()
        else:
            self.canvas.delete(self.opp_animation_list[::-1][self.opp_animation_index])
            self.opp_animation_index += 1
            if self.opp_animation_index < len(self.opp_animation_list):
                self.canvas.after(frame_delta, self.animate, frame_delta)
            else:
                self.animation_clean_up()

    def updateLabels(self):

        try:
            if self.you.healthMax <= 0:
                self.you.healthMax = 0

            if self.opp.healthMax <= 0:
                self.opp.healthMax = 0

            if self.you.health <= 0:
                self.you.health = 0

            if self.opp.health <= 0:
                self.opp.health = 0

            if self.you.health >= self.you.healthMax:
                self.you.health = self.you.healthMax

            if self.opp.health >= self.opp.healthMax:
                self.opp.health = self.opp.healthMax

            self.hptext = _("气血: {}/{}").format(int(self.you.health), int(self.you.healthMax))
            self.opphptext = _("气血: {}/{}").format(int(self.opp.health), int(self.opp.healthMax))
            self.staminatext = _("内力: {}/{}").format(int(self.you.stamina), int(self.you.staminaMax))
            self.oppstaminatext = _("内力: {}/{}").format(int(self.opp.stamina), int(self.opp.staminaMax))
            self.statustext = _("状态: {}").format(self.you.status)
            self.oppstatustext = _("状态: {}").format(self.opp.status)

            self.hplabel.config(text=self.hptext)
            self.opphplabel.config(text=self.opphptext)

            self.staminalabel.config(text=self.staminatext)
            self.oppstaminalabel.config(text=self.oppstaminatext)

            self.statuslabel.config(text=self.statustext)
            self.oppstatuslabel.config(text=self.oppstatustext)

        except:
            pass

    def restore(self, h, s, full=False):

        if full or (h + self.health >= self.healthMax and s + self.stamina >= self.staminaMax and h > 0 and s > 0):
            self.health = self.healthMax
            self.stamina = self.staminaMax
            print("{} recovers to full health and stamina.\n".format(self.character))


        elif h + self.health >= self.healthMax and s <= 0:
            self.health = self.healthMax
            print("{} recovers to full health.\n".format(self.character))


        elif s + self.stamina >= self.staminaMax and h <= 0:
            self.stamina = self.staminaMax
            print("{} recovers to full stamina.\n".format(self.character))


        else:

            if h + self.health >= self.healthMax:
                h = int(self.healthMax) - int(self.health)

            if s + self.stamina >= self.staminaMax:
                s = int(self.staminaMax) - int(self.stamina)

            self.health += h
            self.stamina += s

            if h > 0 and s > 0:
                print("{} recovers {} health and {} stamina.\n".format(self.character, int(h), int(s)))

            elif h > 0:
                print("{} recovers {} health.\n".format(self.character, int(h)))

            elif s > 0:
                print("{} recovers {} stamina.\n".format(self.character, int(s)))

    def setStatus(self, status, targ=None):

        if targ:
            targ.status = status
            if status == "Normal":
                print("{}'s status has returned to 'Normal'.".format(self.character))

        else:
            self.status = status



    def battleCheckProfile(self, event):
        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        try:
            if self.battleProfileWin.winfo_exists():
                return
            else:
                raise

        except:

            self.battleProfileWin = Toplevel(self.battleWin)

            self.battle_stat_frame = Frame(self.battleProfileWin)
            self.battle_stat_frame.pack(side=LEFT)

            self.battle_skill_move_frame = Frame(self.battleProfileWin, borderwidth=3, relief=SOLID)
            self.battle_skill_move_frame.pack(side=RIGHT, expand=Y, fill=Y)

            F0 = Frame(self.battle_stat_frame)
            F0.pack(side=TOP)

            F0_0 = Frame(F0)
            F0_0.grid(row=0, column=0)

            photo1 = PhotoImage(file="you.ppm", master=self.battleProfileWin)
            pic1 = Label(F0_0, image=photo1)
            pic1.pack(side=LEFT)

            F0_1 = Frame(F0)
            F0_1.grid(row=0, column=1)
            F1 = Frame(F0_1)
            F1.pack()
            self.levelUpThreshold = self.levelThresholds[self.level - 1]

            self.profile_L0 = Label(F1, text=_("姓名: {}").format(self.character))
            self.profile_L1 = Label(F1, text=_("等级: {}").format(int(self.level)))
            self.profile_L2 = Label(F1, text=_("经验: {}").format(int(self.exp), self.levelUpThreshold))
            self.profile_L3 = Label(F1, text=_("修为点: {}").format(int(self.upgradePoints)))
            self.profile_L4 = Label(F1, text=_("状态: {}").format(self.you.status))

            self.profile_L0.grid(row=0, column=0, sticky=W)
            self.profile_L1.grid(row=1, column=0, sticky=W)
            self.profile_L2.grid(row=2, column=0, sticky=W)
            self.profile_L3.grid(row=3, column=0, sticky=W)
            self.profile_L4.grid(row=4, column=0, sticky=W)

            F0_2 = Frame(F0)
            F0_2.grid(row=0, column=2)
            # Label(F0_2, text="\t").pack(side=TOP)
            # Label(F0_2, text="\t").pack(side=TOP)
            # inventory_icon = PhotoImage(file="inventory_icon.ppm")
            # imageButton1 = Button(F0_2, command=partial(self.checkInv,self.profileWin))
            # imageButton1.pack(side=BOTTOM)
            # imageButton1.config(image=inventory_icon)

            F2 = Frame(self.battle_stat_frame, borderwidth=3, relief=SOLID)
            F2.pack()
            self.profile_L6 = Label(F2, text=_("气血: {}/{}").format(int(self.you.health), int(self.you.healthMax)))
            self.profile_L7 = Label(F2, text=_("内力: {}/{}").format(int(self.you.stamina), int(self.you.staminaMax)))
            self.profile_L8 = Label(F2, text=_("攻击: {}").format(int(self.you.attack)))
            self.profile_L9 = Label(F2, text=_("力量: {}").format(int(self.you.strength)))
            self.profile_L10 = Label(F2, text=_("速度: {}").format(int(self.you.speed)))
            self.profile_L11 = Label(F2, text=_("防御: {}").format(int(self.you.defence)))
            self.profile_L12 = Label(F2, text=_("敏捷: {}").format(int(self.you.dexterity)))

            self.profile_L6.grid(row=0, column=0, sticky=W)
            self.profile_L7.grid(row=1, column=0, sticky=W)
            self.profile_L8.grid(row=2, column=0, sticky=W)
            self.profile_L9.grid(row=3, column=0, sticky=W)
            self.profile_L10.grid(row=4, column=0, sticky=W)
            self.profile_L11.grid(row=5, column=0, sticky=W)
            self.profile_L12.grid(row=6, column=0, sticky=W)


            self.profile_L13 = Label(F2, text=_("悟性: {}").format(int(self.perception)))
            self.profile_L14 = Label(F2, text=_("福缘: {}").format(int(self.luck)))
            self.profile_L15 = Label(F2, text=_("侠义值: {}").format(int(self.chivalry)))
            self.profile_L13.grid(row=0, column=3, sticky=W)
            self.profile_L14.grid(row=1, column=3, sticky=W)
            self.profile_L15.grid(row=2, column=3, sticky=W)

            # B1 = Button(self.profileWin, text=_("返回"), command=partial(self.winSwitch, self.profileWin, fromWin))
            # B1.pack(side=BOTTOM)

            skill_move_image_list = []
            Label(self.battle_skill_move_frame, text="Attacks/Moves").pack()
            moves_frame = Frame(self.battle_skill_move_frame, borderwidth=1, relief=SUNKEN)
            moves_frame.pack()

            frame_width = 8
            frame_x = 0
            frame_y = 0
            for i in range(len(self.sml) - 3):
                move = self.sml[i]
                move_image = PhotoImage(file=move.file_name)
                move_image_label = Button(moves_frame, image=move_image)
                move_image_label.grid(row=frame_y, column=frame_x)
                move_image_label.config(image=move_image)
                skill_move_image_list.append(move_image)
                self.balloon.bind(move_image_label, move.description)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1

            Label(self.battle_skill_move_frame, text="Inner Energy/Agility Techniques").pack()
            skills_frame = Frame(self.battle_skill_move_frame)
            skills_frame.pack()

            frame_width = 8
            frame_x = 0
            frame_y = 0

            for i in range(len(self.skills)):
                skill = self.skills[i]
                skill_image = PhotoImage(file=skill.file_name)
                skill_image_label = Button(skills_frame, image=skill_image)
                skill_image_label.grid(row=frame_y, column=frame_x)
                skill_image_label.config(image=skill_image)
                skill_move_image_list.append(skill_image)
                self.balloon.bind(skill_image_label, skill.description)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1

            self.battleProfileWin.mainloop()


    def oppBattleCheckProfile(self,event):
        self.currentEffect = "button-20.mp3"
        self.startSoundEffectThread()

        try:
            if self.oppBattleProfileWin.winfo_exists():
                return
            else:
                raise

        except:

            self.oppBattleProfileWin = Toplevel(self.battleWin)

            opp_battle_stat_frame = Frame(self.oppBattleProfileWin)
            opp_battle_stat_frame.pack(side=LEFT)

            opp_battle_skill_move_frame = Frame(self.oppBattleProfileWin, borderwidth=3, relief=SOLID)
            opp_battle_skill_move_frame.pack(side=RIGHT, expand=Y, fill=Y)

            F0 = Frame(opp_battle_stat_frame)
            F0.pack(side=TOP)

            F0_0 = Frame(F0)
            F0_0.grid(row=0, column=0)

            photo1 = PhotoImage(file=_("{}_icon.ppm").format(self.opp.name), master=self.oppBattleProfileWin)
            pic1 = Label(F0_0, image=photo1)
            pic1.pack(side=LEFT)

            F0_1 = Frame(F0)
            F0_1.grid(row=0, column=1)
            F1 = Frame(F0_1)
            F1.pack()

            self.profile_L0 = Label(F1, text=_("姓名: {}").format(self.opp.name))
            self.profile_L1 = Label(F1, text=_("等级: {}").format(int(self.opp.level)))
            self.profile_L2 = Label(F1, text=_("状态: {}").format(self.opp.status))

            self.profile_L0.grid(row=0, column=0, sticky=W)
            self.profile_L1.grid(row=1, column=0, sticky=W)
            self.profile_L2.grid(row=2, column=0, sticky=W)

            F0_2 = Frame(F0)
            F0_2.grid(row=0, column=2)

            F2 = Frame(opp_battle_stat_frame, borderwidth=3, relief=SOLID)
            F2.pack()
            self.profile_L6 = Label(F2, text=_("气血: {}/{}").format(int(self.opp.health), int(self.opp.healthMax)))
            self.profile_L7 = Label(F2,
                                    text=_("内力: {}/{}").format(int(self.opp.stamina), int(self.opp.staminaMax)))
            self.profile_L8 = Label(F2, text=_("攻击: {}").format(int(self.opp.attack)))
            self.profile_L9 = Label(F2, text=_("力量: {}").format(int(self.opp.strength)))
            self.profile_L10 = Label(F2, text=_("速度: {}").format(int(self.opp.speed)))
            self.profile_L11 = Label(F2, text=_("防御: {}").format(int(self.opp.defence)))
            self.profile_L12 = Label(F2, text=_("敏捷: {}").format(int(self.opp.dexterity)))

            self.profile_L6.grid(row=0, column=0, sticky=W)
            self.profile_L7.grid(row=1, column=0, sticky=W)
            self.profile_L8.grid(row=2, column=0, sticky=W)
            self.profile_L9.grid(row=3, column=0, sticky=W)
            self.profile_L10.grid(row=4, column=0, sticky=W)
            self.profile_L11.grid(row=5, column=0, sticky=W)
            self.profile_L12.grid(row=6, column=0, sticky=W)

            skill_move_image_list = []
            Label(opp_battle_skill_move_frame, text="Attacks/Moves").pack()
            moves_frame = Frame(opp_battle_skill_move_frame, borderwidth=1, relief=SUNKEN)
            moves_frame.pack()

            frame_width = 8
            frame_x = 0
            frame_y = 0
            for i in range(len(self.opp.sml)):
                move = self.opp.sml[i]
                move_image = PhotoImage(file=move.file_name)
                move_image_label = Button(moves_frame, image=move_image)
                move_image_label.grid(row=frame_y, column=frame_x)
                move_image_label.config(image=move_image)
                skill_move_image_list.append(move_image)
                self.balloon.bind(move_image_label, move.description)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1

            Label(opp_battle_skill_move_frame, text="Inner Energy/Agility Techniques").pack()
            skills_frame = Frame(opp_battle_skill_move_frame)
            skills_frame.pack()

            frame_width = 8
            frame_x = 0
            frame_y = 0

            for i in range(len(self.opp.skills)):
                skill = self.opp.skills[i]
                skill_image = PhotoImage(file=skill.file_name)
                skill_image_label = Button(skills_frame, image=skill_image)
                skill_image_label.grid(row=frame_y, column=frame_x)
                skill_image_label.config(image=skill_image)
                skill_move_image_list.append(skill_image)
                self.balloon.bind(skill_image_label, skill.description)

                frame_x += 1
                if frame_x >= frame_width:
                    frame_x = 0
                    frame_y += 1

            self.oppBattleProfileWin.mainloop()



    def endTurn(self):

        self.youHaveMoved = False
        self.oppHasMoved = False
        self.turnEnded = False
        self.animation_in_progress = False

        t = Thread(target=self.allOn, args=(self.animation_buttons,))
        self.battleWin.after(1000, t.start)

        if not self.battleEnded:
            self.battleWin.after(500, self.checkForEffects)

    def checkForDeath(self):

        # print(self.you.health)
        # print(self.opp.health)


        if self.opp.health <= 0:
            self.youHaveMoved = True
            self.oppHasMoved = True
            self.battleEnded = True
            t = Thread(target=self.battleVictory, args=(self.destinationWinList[0],))
            self.battleWin.after(1000, t.start)

        elif self.you.health <= 0:
            self.youHaveMoved = True
            self.oppHasMoved = True
            self.battleEnded = True
            time.sleep(1)
            self.battleLoss(self.destinationWinList[1])

        else:
            return None

    def gainExp(self, amount):
        self.exp += int(amount)
        print(_("{} gains {} experience.\n\n").format(self.character, amount))
        while self.exp >= self.levelThresholds[self.level - 1]:
            self.level += 1
            self.upgradePoints += int(random() * self.perception / 8) + self.level
            self.staminaMax += self.level * 10
            self.healthMax += self.level * 10
            print(_("{} leveled up to level {}!").format(self.character, self.level))
            print(_("Health and stamina upper limits increased by {}!").format(self.level * 10))


    def battleVictory(self, destinationWin=None):
        self.health = self.you.health
        self.stamina = self.you.stamina
        # self.status = self.you.status
        self.status = "Normal"

        self.battleExperience = int(1.25 * (max([int(self.opp.level ** 2.75), 100]) + int(.2 * (self.you.health)) + int(
            .8 * (100 * (max([self.opp.level - self.you.level + 2, 1]))))))
        self.gainExp(self.battleExperience)

        time.sleep(1)
        self.stopSoundtrack()

        self.currentBGM = self.postBattleSoundtrack
        self.startSoundtrackThread()


        # self.battleWin.destroy()
        self.battleWin.withdraw()

        if self.battleID == "HuangXiaodongTrainingTest":
            self.battleWin.after(100, self.cheater_ending, self.battleID)
            return
        elif self.battleID == "ShaolinTrainingTest":
            self.battleWin.after(100, self.cheater_ending, self.battleID)
            return


        if destinationWin:
            destinationWin.deiconify()
        if self.postBattleCmd:
            self.battleWin.after(100, self.postBattleCmd)

    def battleLoss(self, destinationWin=None):

        print(_("You have been defeated.\n"))
        # self.battleWin.destroy()
        try:
            self.battleWin.withdraw()
        except:
            pass
        self.faint(destinationWin)

    def faint(self, destinationWin):

        self.stopSoundtrack()

        if self.battleID == "HuangXiaodongTrainingTest":
            self.battleWin.after(100, self.cherry_blossom_island_training_reward)
            self.battleID = None
            return

        elif self.battleID == "ShaolinTrainingTest":
            self.battleWin.after(100, self.shaolin_training_reward)
            self.battleID = None
            return


        if self.battleType == "training":
            self.battleExperience = int(.25 * max([int(self.opp.level ** 2.75), 100])) + 50
            self.gainExp(self.battleExperience)
            self.restore(0, 0, True)
            self.status = "Normal"
            self.currentBGM = self.postBattleSoundtrack
            self.startSoundtrackThread()
            print(_("You wake up from the battle, finding yourself fully healed."))
            try:
                if destinationWin:
                    destinationWin.deiconify()
                if self.postBattleCmd:
                    self.battleWin.after(100, self.postBattleCmd)
            except:
                pass

        elif self.inv[_("金钱")] >= 300 and self.opp.name == "Scrub":
            self.inv[_("金钱")] -= 300
            self.battleExperience = int(.25 * max([int(self.opp.level ** 2.75), 100])) + 50
            self.gainExp(self.battleExperience)
            self.restore(0, 0, True)
            self.status = "Normal"
            print(_("You wake up at Scrub's House after passing out for an unknown length\nof time. Your gold pouch feels lighter than before..."))

            try:
                self.scrubHouseWin.deiconify()
            except:
                pass

            self.currentBGM = "jy_calm2.mp3"
            self.startSoundtrackThread()

        else:
            self.display_lose_screen()

    def display_lose_screen(self):

        self.currentBGM = "jy_gameover.mp3"
        self.startSoundtrackThread()
        self.lose_screen_win = Toplevel(self.mainWin)

        bg = PhotoImage(file="GameOverEn.png", master=self.lose_screen_win)
        panel = Label(self.lose_screen_win, image=bg)
        panel.pack(expand="yes")
        panel.image = bg

        Button(self.lose_screen_win, text=_("Back to Main Menu"), command=self.restartGame).pack()

        self.lose_screen_win.mainloop()


    def restartGame(self):

        self.stopSoundtrack()
        self.currentEffect = "button-20.mp3"
        self.currentBGM = "jy_intro.mp3"
        self.startSoundEffectThread()
        self.startSoundtrackThread()

        self.lose_screen_win.quit()
        self.lose_screen_win.destroy()
        self.destroy_all(self.mainWin)
        self.mainWin.deiconify()


    def destroy_all(self, targ):
        for widget in targ.winfo_children():
            if isinstance(widget, Toplevel):
                widget.destroy()



    def cheater_ending(self, battleID):

        if battleID == "HuangXiaodongTrainingTest":
            self.generate_dialogue_sequence(
                None,
                "cherry_blossom_island.png",
                [_("I... I won!"),
                 _("Unbelievable! I actually won!"),
                 _("But why do I feel so...empty?"),
                 _("..."),
                 _("I just started this journey, yet somehow, I've already beaten one of the Elite Four..."),
                 _("If that's the case, what is there to look forward to?"),
                 _("*Sigh* ... There is no point... no challenge... no motivation..."),
                 _("I suppose I'd better just jump off a cliff and end it here."),],
                ["you"]*8
            )

        elif battleID == "ShaolinTrainingTest":
            self.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("I... I won!"),
                 _("Unbelievable! I actually won!"),
                 _("But why do I feel so...empty?"),
                 _("..."),
                 _("I just started this journey, yet somehow, I've already beaten the Abbot of Shaolin..."),
                 _("If that's the case, what is there to look forward to?"),
                 _("*Sigh* ... There is no point... no challenge... no motivation..."),
                 _("I suppose I'd better just jump off a cliff and end it here."),],
                ["you"]*8
            )
        self.display_lose_screen()



    def checkForEffects(self):

        if self.you.status != _("正常") or self.opp.status != _("正常"):
            if _("毒") in self.you.status:
                print(_("You are hurt by poison!\n"))
                if _("毒++") in self.you.status:
                    self.you.health -= int(self.you.healthMax * .1)
                elif _("毒+") in self.you.status:
                    self.you.health -= int(self.you.healthMax * .075)
                elif _("毒") in self.you.status:
                    self.you.health -= int(self.you.healthMax * .05)

            if _("毒") in self.opp.status:
                print(_("{} is hurt by poison!\n".format(self.opp.name)))
                if _("毒++") in self.opp.status:
                    self.opp.health -= int(self.opp.healthMax * .1)
                elif _("毒+") in self.opp.status:
                    self.opp.health -= int(self.opp.healthMax * .075)
                elif _("毒") in self.opp.status:
                    self.opp.health -= int(self.opp.healthMax * .05)

            if _("内伤") in self.you.status:
                print(_("You lost some stamina due to internal injury!\n"))
                if _("内伤++") in self.you.status:
                    self.you.stamina -= int(self.you.staminaMax * .1)
                elif _("内伤+") in self.you.status:
                    self.you.stamina -= int(self.you.staminaMax * .075)
                elif _("内伤") in self.you.status:
                    self.you.stamina -= int(self.you.staminaMax * .05)

            if _("内伤") in self.opp.status:
                print(_("{} loses some stamina due to internal injury!\n".format(self.opp.name)))
                if _("内伤++") in self.opp.status:
                    self.opp.stamina -= int(self.opp.staminaMax * .1)
                elif _("内伤+") in self.opp.status:
                    self.opp.stamina -= int(self.opp.staminaMax * .075)
                elif _("内伤") in self.opp.status:
                    self.opp.stamina -= int(self.opp.staminaMax * .05)


            if self.you.health <= 0:
                self.you.health = 0
            if self.you.stamina <= 0:
                self.you.stamina = 0
            if self.opp.health <= 0:
                self.opp.health = 0
            if self.opp.stamina <= 0:
                self.opp.stamina = 0

            self.updateLabels()
            self.checkForDeath()
            return True
        else:
            return False

    def youGotHit(self, damage, move, mute=False):

        if mute:
            pass
        else:
            self.currentEffect = move.sfx_file_name
            self.startSoundEffectThread()

        damage = int(damage)
        self.you.health = int(self.you.health - damage)
        if self.you.health <= 0:
            self.you.health = 0

        self.load_animation(move.name, move.animation_file_list, target=0)

        # Apply move effects
        effects = []
        r = random()
        if move.name == _("五毒掌") and move.level >= 3:
            if (move.level >= 10) or (move.level >= 7 and r <= .6) or (move.level >= 5 and r <= .4) or (move.level >= 3 and r <= .2):
                effects.append(_("毒"))
                print(_("You've been poisoned!"))

        elif move.name == _("落樱指法") and move.level >= 3:
            if (move.level >= 10 and r <= .4) or (move.level >= 7 and r <= .3) or (move.level >= 5 and r <= .2) or (move.level >= 3 and r <= .1):
                self.youAcupunctureCount = 3
                effects.append(_("封 (3)"))
                print(_("Your acupuncture points have been sealed!"))

        elif move.name == _("少林罗汉拳"):
            if move.level >= 10:
                self.opp.stamina += int(self.opp.staminaMax*.2)
                print(_("Opponent recovers 20% stamina."))

            elif move.level >= 6:
                self.opp.stamina += int(self.opp.staminaMax*.1)
                print(_("Opponent recovers 10% stamina."))

            elif move.level >= 3:
                self.opp.stamina += int(self.opp.staminaMax*.05)
                print(_("Opponent recovers 5% stamina."))

            self.updateLabels()


        elif move.name == _("火焰掌"):
            if (move.level >= 10 and r <= .8) or (move.level >= 7 and r <= .6) or (move.level >= 5 and r <= .4) or (move.level >= 3 and r <= .2):
                effects.append(_("内伤"))
                print(_("You're suffering from internal injury!"))


        if effects:
            if self.you.status == _("正常"):
                self.you.status = ""
            for effect in effects:
                if effect == _("毒") and _("毒") in self.you.status:
                    if _("毒++") not in self.you.status:
                        self.you.status = self.you.status.replace(_("毒"), _("毒+"))
                elif effect == _("内伤") and _("内伤") in self.you.status:
                    if _("内伤++") not in self.you.status:
                        self.you.status = self.you.status.replace(_("内伤"), _("内伤+"))
                elif _("封") in effect and _("封") in self.you.status and self.youAcupunctureCount > 0:
                    self.you.status = re.sub(_("Seal \(\d\)"), effect, self.you.status)
                elif effect not in self.you.status:
                    if self.you.status == "":
                        self.you.status += "\n" + " " + effect
                    else:
                        first_line = self.you.status.split("\n")[0]
                        self.you.status += "\n" + " "*(len(first_line)-13) + effect

            self.you.status = self.you.status.strip()  # remove extra spaces

        self.updateLabels()
        self.checkForDeath()

    def oppGotHit(self, damage, move, mute=False):

        if mute:
            pass
        else:
            self.currentEffect = move.sfx_file_name
            self.startSoundEffectThread()

        damage = int(damage)
        self.opp.health = int(self.opp.health - damage)
        if self.opp.health <= 0:
            self.opp.health = 0

        self.load_animation(move.name, move.animation_file_list, target=1)

        # Apply move effects
        effects = []
        r = random()

        if move.name == _("五毒掌") and move.level >= 3:
            if (move.level >= 10) or (move.level >= 7 and r <= .6) or (move.level >= 5 and r <= .4) or (
                    move.level >= 3 and r <= .2):
                effects.append(_("毒"))
                print(_("Opponent has been poisoned!"))

        elif move.name == _("落樱指法") and move.level >= 3:
            if (move.level >= 10 and r <= .4) or (move.level >= 7 and r <= .3) or (
                    move.level >= 5 and r <= .2) or (move.level >= 3 and r <= .1):
                self.oppAcupunctureCount = 3
                effects.append(_("封 (3)"))
                print(_("Opponent's acupuncture points have been sealed!"))

        elif move.name == _("少林罗汉拳"):
            if move.level >= 10:
                self.you.stamina += int(self.you.staminaMax * .2)
                print(_("You recover 20% stamina."))

            elif move.level >= 6:
                self.you.stamina += int(self.you.staminaMax * .1)
                print(_("You recover 10% stamina."))

            elif move.level >= 3:
                self.you.stamina += int(self.you.staminaMax * .05)
                print(_("You recover 5% stamina."))

            self.updateLabels()

        elif move.name == _("菜鸟拳"):
            if (move.level >= 10 and r <= .5) or (move.level >= 8 and r <= .3) or (move.level >= 5 and r <= .2):
                self.oppActionPoints -= self.opp.speed
                print(_("Opponent was stunned by the attack!"))

        elif move.name == _("火焰掌"):
            if (move.level >= 10 and r <= .8) or (move.level >= 7 and r <= .6) or (move.level >= 5 and r <= .4) or (move.level >= 3 and r <= .2):
                effects.append(_("内伤"))
                print(_("The opponent is suffering from internal injury!"))


        if effects:
            if self.opp.status == "Normal":
                self.opp.status = ""
            print(effects)
            for effect in effects:
                if effect == _("毒") and _("毒") in self.opp.status:
                    if _("毒++") not in self.opp.status:
                        self.opp.status = self.opp.status.replace(_("毒"), _("毒+"))
                elif effect == _("内伤") and _("内伤") in self.opp.status:
                    if _("内伤++") not in self.opp.status:
                        self.opp.status = self.opp.status.replace(_("内伤"), _("内伤+"))
                elif _("封") in effect and _("封") in self.opp.status and self.oppAcupunctureCount > 0:
                    self.opp.status = re.sub(_("Seal \(\d\)"), effect, self.opp.status)
                elif effect not in self.opp.status:
                    if self.opp.status == "":
                        self.opp.status += "\n" + " " + effect
                    else:
                        first_line = self.opp.status.split("\n")[0]
                        self.opp.status += "\n" + " "*(len(first_line)-13) + effect

            self.opp.status = self.opp.status.strip()  # remove extra spaces

        self.updateLabels()
        self.checkForDeath()

    def gainHealth(self, person, gain):

        if person == self.you:

            gain = int(gain)
            if self.you.health + gain >= self.you.healthMax:
                gain = int(self.you.healthMax - self.you.health)

            self.you.health += gain

            self.updateLabels()
            print("{} recovers {} health.\n".format(self.you.name, gain))

        else:
            gain = int(gain)

            if self.opp.health + gain >= self.opp.healthMax:
                gain = int(self.opp.healthMax - self.opp.health)

            self.opp.health += gain

            self.updateLabels()
            print("{} recovers {} health.\n".format(self.opp.name, gain))

    def youUseStamina(self, loss):

        self.you.stamina -= loss
        self.updateLabels()

    def oppUseStamina(self, loss):

        self.opp.stamina -= loss
        self.updateLabels()

    def gainStamina(self, person, gain):

        if person == self.you:

            gain = int(gain)

            if self.you.stamina + gain >= self.you.staminaMax:
                gain = int(self.you.staminaMax - self.you.stamina)

            self.you.stamina += gain

            self.updateLabels()
            print("{} recovers {} stamina.\n".format(self.you.name, gain))

        else:
            gain = int(gain)

            if self.opp.stamina + gain >= self.opp.staminaMax:
                gain = int(self.opp.staminaMax - self.opp.stamina)

            self.opp.stamina += gain

            self.updateLabels()
            print("{} recovers {} stamina.\n".format(self.opp.name, gain))

    def updateStats(self, person, datk=0, dstr=0, dspd=0, ddef=0, ddex=0):

        person.attack += datk
        person.strength += dstr
        person.speed += dspd
        person.defence += ddef
        person.dexterity += ddex

        if person.attack <= 0:
            person.attack = 0

        if person.strength <= 0:
            person.strength = 0

        if person.speed <= 0:
            person.speed = 0

        if person.defence <= 0:
            person.defence = 0

        if person.dexterity <= 0:
            person.dexterity = 0

        self.callAfterStatChange()

        self.updateLabels()

    def callAfterStatChange(self):

        self.accuracy = (self.you.attack - self.opp.dexterity + 25) / 30
        self.damage_scale = (self.you.strength ** .5 / self.opp.defence ** .5)

        self.oppaccuracy = (self.opp.attack - self.you.dexterity + 25) / 30
        self.oppdamage_scale = (self.opp.strength ** .5 / self.you.defence ** .5)

        if "Sting" in self.inv and self.opp.name in ["Giant Spider", "Venomous Spider"]:
            self.damage_scale = self.damage_scale * 2

        if self.accuracy <= .3:
            self.accuracy = .3

        if self.damage_scale <= .2:
            self.damage_scale = .2

        if self.oppaccuracy <= .3:
            self.oppaccuracy = .3

        if self.oppdamage_scale <= .2:
            self.oppdamage_scale = .2

    def move_buffer_zone(self, move):

        if self.you.stamina >= move.base_stamina:

            self.allOff(self.animation_buttons)

            # print("You Action Pts", self.youActionPoints)
            # print("Opp Action Pts", self.oppActionPoints)
            # print("\n")

            if self.youActionPoints < self.oppActionPoints:
                self.youActionPoints += self.you.speed
                self.oppChooseMove()
            else:
                self.oppActionPoints += self.opp.speed
                self.oppHasMoved = True
                self.you_move(move)
                if self.youAcupunctureCount > 0 and move.base_stamina == 0:
                    self.youAcupunctureCount -= 1
                    self.you.status = self.you.status.replace(_("Seal ({})").format(self.youAcupunctureCount + 1),
                                                              _("Seal ({})").format(self.youAcupunctureCount))
                    self.you.status = self.you.status.replace(_("封 (0)"), "")
                    self.you.status = self.you.status.strip()

                    if self.you.status == "":
                        self.you.status = "Normal"

                    self.updateLabels()

            if self.youHaveMoved and self.oppHasMoved:
                self.endTurn()
            else:
                # print("Initializing next move thread")
                t = Thread(target=self.move_buffer_zone, args=(move,))
                self.battleWin.after(1800, t.start)

        else:
            messagebox.showinfo("", _("Not enough stamina to use this move."))

    def you_move(self, move):

        self.youUseStamina(move.base_stamina)
        self.updateLabels()
        move.gain_exp(move.exp_rate)
        self.update_move_button_tooltips(move)

        if move.name == _("逃跑"):
            self.flee(targ=0)
        elif move.name == _("休息"):
            self.rest(self.you)
        elif move.name == _("物品"):
            self.checkInv(fromWin=self.battleWin, purpose="battle")
        else:
            self.execute_attack(move, 1)

        self.youHaveMoved = True

    def execute_attack(self, move, targ):

        move_damage = int(move.base_damage * self.damage_scale)
        accuracy_multiplier = 1.0

        if move.name == "Basic Sword Technique" and move.level >= 3:
            if move.level == 3:
                accuracy_multiplier *= 1.1
            elif move.level == 6:
                accuracy_multiplier *= 1.2
            elif move.level == 10:
                accuracy_multiplier *= 1.4

        if targ:
            move_accuracy = self.accuracy * 100

            if move_accuracy * accuracy_multiplier >= randrange(1, 101):
                print(_("You used {}, dealing {} damage to the opponent!").format(move.name, move_damage))
                self.oppGotHit(move_damage, move)
            else:
                self.currentEffect = pickOne(["sfx_attack_missed.mp3", "sfx_attack_missed2.mp3"])
                self.startSoundEffectThread()
                print(_("You used {}, but the opponent dodged it!").format(move.name))
                self.load_animation("Dodge",
                                    ["a_dodge_{}1.gif".format(self.opp.name), "a_dodge_{}2.gif".format(self.opp.name),
                                     "a_dodge_{}3.gif".format(self.opp.name), "a_dodge_{}4.gif".format(self.opp.name),
                                     "a_dodge_{}5.gif".format(self.opp.name), "a_dodge_{}6.gif".format(self.opp.name),
                                     "a_dodge_{}7.gif".format(self.opp.name),
                                     "a_dodge_{}1.gif".format(self.opp.name), ],
                                    target=1)

        else:
            move_accuracy = self.oppaccuracy * 100

            if move_accuracy * accuracy_multiplier >= randrange(1, 101):
                print(_("{} used {} and deals {} damage to you!").format(self.opp.name, move.name, move_damage))
                self.youGotHit(move_damage, move)
            else:
                self.currentEffect = pickOne(["sfx_attack_missed.mp3", "sfx_attack_missed2.mp3"])
                self.startSoundEffectThread()
                print(_("{} used {}, but you dodged it!").format(self.opp.name, move.name))
                self.load_animation("Dodge",
                                    ["a_dodge_you6.gif", "a_dodge_you5.gif", "a_dodge_you4.gif",
                                     "a_dodge_you3.gif", "a_dodge_you2.gif", "a_dodge_you1.gif", "a_dodge_you7.gif"],
                                    target=0)

    def rest(self, targ_obj):

        self.currentEffect = "sfx_recover.mp3"
        self.startSoundEffectThread()

        # self.load_animation("Rest", [],
        #                    target=0, frame_delta=50)

        rHealth = int(targ_obj.healthMax * .05)
        rStamina = int(targ_obj.staminaMax * .2)
        self.gainHealth(targ_obj, rHealth)
        self.gainStamina(targ_obj, rStamina)
        self.updateLabels()

    def flee(self, targ=0, useSmokeBomb=False, destinationWin=None):

        if targ:
            pass

        else:
            if self.battleType in ("boss", "test"):
                print("Can't escape!\n")
                return None

            if self.oppHasMoved:

                successRate = .3 + .6 * (self.you.level - self.opp.level) / min(
                    [self.you.level, self.opp.level]) + .4 * (
                                      self.you.speed - self.opp.speed) / min([self.you.speed, self.opp.speed])

                if successRate >= 1:
                    successRate = 1
                if successRate <= .2:
                    successRate = .2
                # print(successRate)

                if useSmokeBomb:
                    successRate = 1

                if random() <= successRate:
                    self.health = self.you.health
                    self.stamina = self.you.stamina
                    self.you.status = "Normal"
                    self.status = self.you.status
                    self.battleID = None
                    print("{} successfully fled from battle.\n".format(self.character))
                    self.battleFromWin.deiconify()
                    self.battleWin.destroy()
                    self.stopSoundtrack()

                    self.currentBGM = self.postBattleSoundtrack
                    self.startSoundtrackThread()

                    return None


                else:
                    print(_("You fail to escape!\n"))

                self.youHaveMoved = True

                if not self.oppHasMoved:
                    self.oppChooseMove()



            else:
                self.bufferedMove = self.flee
                self.oppChooseMove()

    def oppChooseMove(self, priority=None):

        if self.opp.health <= 0:
            return None

        move = priority

        try:
            minStaminaRequired = min([move.base_stamina for move in self.opp.sml])
        except:
            # print("No special moves")
            minStaminaRequired = 999999999

        if self.oppAcupunctureCount > 0:
            minStaminaRequired = 999999999
        # print(minStaminaRequired)

        if priority == None:

            itemCount = len(self.opp.itemList)

            if itemCount == 0 or self.opp.healthMax - self.opp.health < 50:
                if self.opp.stamina <= self.opp.staminaMax / 2 and random() <= .5:
                    self.rest(self.opp)
                    self.oppHasMoved = True
                elif self.opp.stamina >= minStaminaRequired:
                    move = pickOne([m for m in self.opp.sml if m.base_stamina <= self.opp.stamina])
                    self.opp_move(move)
                else:
                    self.rest(self.opp)
                    self.oppHasMoved = True


            elif itemCount > 0:
                if self.opp.stamina >= minStaminaRequired:
                    r = randrange(len(self.opp.sml) + 1)
                    if r == len(self.opp.sml):
                        self.opp_use_item()
                        self.oppHasMoved = True

                    else:
                        if self.opp.stamina <= self.opp.staminaMax / 2 and random() <= .5:
                            self.rest(self.opp)
                            self.oppHasMoved = True
                        else:
                            move = pickOne([m for m in self.opp.sml if m.base_stamina <= self.opp.stamina])
                            self.opp_move(move)

                else:
                    self.rest(self.opp)
                    self.oppHasMoved = True

        if self.oppAcupunctureCount > 0:
            self.oppAcupunctureCount -= 1
            self.opp.status = self.opp.status.replace(_("Seal ({})").format(self.oppAcupunctureCount + 1),
                                                      _("Seal ({})").format(self.oppAcupunctureCount))
            self.opp.status = self.opp.status.replace(_("封 (0)"), "")
            self.opp.status = self.opp.status.strip()

            if self.opp.status == "":
                self.opp.status = "Normal"

            self.updateLabels()

    def opp_move(self, move):
        try:
            self.oppUseStamina(move.base_stamina)
            self.updateLabels()
            self.execute_attack(move, targ=0)
            self.oppHasMoved = True
        except:
            print("Opp move aborted...")

    def opp_use_item(self):
        pass

    def allOff(self, button_list):
        try:
            for button in button_list:
                button.config(state=DISABLED)
        except:
            pass

    def allOn(self, button_list):
        try:
            self.allOff(self.animation_buttons)

            if self.youAcupunctureCount > 0:
                for button in button_list[-3:]:
                    button.config(state=NORMAL)
            else:
                for button_i in range(len(button_list)):
                    if self.sml[button_i].base_stamina <= self.you.stamina:
                        button = button_list[button_i]
                        button.config(state=NORMAL)

        except:
            pass


class character:

    def __init__(self, name, level, attributeList=[], sml=[], skills=[], itemList=[], status="Normal"):

        self.name = name
        self.level = level
        self.itemList = itemList
        self.sml = sml.copy()
        self.skills = skills.copy()
        self.status = status

        if len(attributeList) == 0:
            self.health = randrange(100 + 25 * self.level, 100 + 50 * self.level)
            self.healthMax = self.health
            self.stamina = randrange(100 + 25 * self.level, 100 + 50 * self.level)
            self.staminaMax = self.stamina
            self.attack = randrange(45 + int(self.level * 1.5), int(50 + self.level * 3.5))
            self.strength = randrange(45 + int(self.level * 1.5), int(50 + self.level * 3.5))
            self.speed = randrange(45 + int(self.level * 1.5), int(50 + self.level * 3.5))
            self.defence = randrange(45 + int(self.level * 1.5), int(50 + self.level * 3.5))
            self.dexterity = randrange(45 + int(self.level * 1.5), int(50 + self.level * 3.5))

            if name != "Yuki Onna" and name != "Scrub":
                if self.attack >= 100:
                    self.attack = 100
                if self.strength >= 100:
                    self.strength = 100
                if self.speed >= 100:
                    self.speed = 100
                if self.defence >= 100:
                    self.defence = 100
                if self.dexterity >= 100:
                    self.dexterity = 100


        else:
            self.health = attributeList[0]
            self.healthMax = attributeList[1]
            self.stamina = attributeList[2]
            self.staminaMax = attributeList[3]
            self.attack = attributeList[4]
            self.strength = attributeList[5]
            self.speed = attributeList[6]
            self.defence = attributeList[7]
            self.dexterity = attributeList[8]


class item:

    def __init__(self, itemName, count, heal=False, effect=False,
                 dhealth=0, dstamina=0, datk=0, dstr=0, dspd=0, ddef=0,
                 ddex=0, func=None):
        self.itemName = itemName
        self.count = count
        self.heal = heal
        self.effect = effect
        self.func = func

        self.dhealth = dhealth
        self.dstamina = dstamina
        self.datk = datk
        self.dstr = dstr
        self.dspd = dspd
        self.ddef = ddef
        self.ddex = ddex


class skill:
    def __init__(self, name, level=1):
        self.name = name
        self.level = level
        self.description = ""
        self.update_description()
        if self.name == _("樱花水上漂"):
            self.file_name = "Cherry Blossoms Floating on the Water_icon.ppm"

    def upgrade_skill(self, amount=1):
        if self.level < 10:
            self.level += amount
            self.update_description()

    def update_description(self):
        if self.name == _("樱花水上漂"):
            self.description = _("{} (level: {})\nIn battle, increases speed by {} and dexterity by {}.".format(self.name, self.level, 2 * self.level, self.level))


class special_move:
    def __init__(self, name, level=1, damage_multiplier=1.1, effects=None):
        self.level_exp_thresholds = [
            0, 100, 250, 450, 700, 1000, 1350, 1800, 2300, 3000
        ]
        self.level = level
        self.exp = 0
        self.exp_rate = 10
        self.name = name
        self.effects = effects

        if self.name == _("菜鸟拳"):
            self.file_name = "scrub_fist_icon.ppm"
            self.sfx_file_name = "sfx_SGSHitSoundLoud.mp3"
            self.animation_file_list = ["a_Scrub_Fist1.gif", "a_Scrub_Fist2.gif", "a_Scrub_Fist3.gif",
                                        "a_Scrub_Fist4.gif", "a_Scrub_Fist3.gif", "a_Scrub_Fist4.gif"]
            self.base_stamina = 15
            self.base_damage = 10
            self.damage_multiplier = 1.2
            self.description_template = _("菜鸟拳\n-------------------------\n技能等级: {}\n经验: {}/{}\n基本伤害: {}\n伤害增长率: {}%\n内力消耗: {}\n招式类型: {}\n-------------------------\n第5级: 20%几率对敌人造成晕眩\n第8级: 30%几率对敌人造成晕眩\n第10级: 50%几率对敌人造成晕眩")
            self.type = _("Unarmed")


        elif self.name == _("休息"):
            self.file_name = "rest_icon.ppm"
            self.sfx_file_name = "sfx_recover.mp3"
            self.animation_file_list = []
            self.base_stamina = 0
            self.base_damage = 0
            self.damage_multiplier = damage_multiplier
            self.description_template = _("恢复5%气血和20%内力;可通过人物技能或装备增加")
            self.type = _("Basic")


        elif self.name == _("物品"):
            self.file_name = "inventory_icon.ppm"
            self.sfx_file_name = "button-20.mp3"
            self.animation_file_list = []
            self.base_stamina = 0
            self.base_damage = 0
            self.damage_multiplier = damage_multiplier
            self.description_template = _("使用包裹中的物品")
            self.type = _("Basic")


        elif self.name == _("逃跑"):
            self.file_name = "flee_icon.ppm"
            self.sfx_file_name = "button-20.mp3"
            self.animation_file_list = []
            self.base_stamina = 0
            self.base_damage = 0
            self.damage_multiplier = damage_multiplier
            self.description_template = _("从战斗中逃跑!成功率取决于你与对手的等级和速度差别")
            self.type = _("Basic")


        elif self.name == _("火焰掌"):
            self.file_name = "fire_palm_icon.gif"
            self.sfx_file_name = "sfx_fire_palm.mp3"
            self.animation_file_list = ["a_Fire_Palm1.gif", "a_Fire_Palm2.gif", "a_Fire_Palm3.gif", "a_Fire_Palm4.gif",
                                        "a_Fire_Palm5.gif", "a_Fire_Palm6.gif", "a_Fire_Palm7.gif"]
            self.base_stamina = 40
            self.base_damage = 30
            self.damage_multiplier = 1.1
            self.description_template = _(
                "Fire Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nAt level 3, gains 20% chance of causing internal injury.\nAt level 5, gains 40% chance of causing internal injury.\nAt level 7, gains 60% chance of causing internal injury.\nAt level 10, gains 100% chance of causing internal injury.")
            self.type = _("Unarmed")


        elif self.name == _("五毒掌"):
            self.file_name = "five_poison_palm_icon.png"
            self.sfx_file_name = "sfx_five_poison_palm.mp3"
            self.animation_file_list = ["a_five_poison_palm1.png", "a_five_poison_palm2.png", "a_five_poison_palm3.png",
                                        "a_five_poison_palm4.png", "a_five_poison_palm5.png", "a_five_poison_palm4.png",
                                        "a_five_poison_palm3.png", "a_five_poison_palm2.png", "a_five_poison_palm1.png"]
            self.base_stamina = 40
            self.base_damage = 20
            self.damage_multiplier = 1.15
            self.description_template = _("五毒掌\n-------------------------\n技能等级: {}\n经验: {}/{}\n基本伤害: {}\n伤害增长率: {}%\n内力消耗: {}\n招式类型: {}\n-------------------------\n第3级, 20%几率使敌人中毒\n第5级, 40%几率使敌人中毒\n第7级, 60%几率使敌人中毒\n第10级, 100%几率使敌人中毒")
            self.type = _("Unarmed")


        elif self.name == _("寒冰掌"):
            self.file_name = "ice_palm_icon.png"
            self.sfx_file_name = "sfx_ice_palm.mp3"
            self.animation_file_list = ["a_ice_palm1.png", "a_ice_palm2.png", "a_ice_palm3.png", "a_ice_palm4.png",
                                        "a_ice_palm5.png", "a_ice_palm6.png", "a_ice_palm7.png", "a_ice_palm8.png"]
            self.base_stamina = 55
            self.base_damage = 25
            self.damage_multiplier = 1.2
            self.description_template = _("寒冰掌\n-------------------------\n技能等级: {}\n经验: {}/{}\n基本伤害: {}\n伤害增长率: {}%\n内力消耗: {}\n招式类型: {}\n-------------------------\n第3级, 20%几率使敌人中毒\n第5级, 40%几率使敌人中毒\n第7级, 60%几率使敌人中毒\n第10级, 100%几率使敌人中毒")
            self.type = _("Unarmed")


        elif self.name == _("少林罗汉拳"):
            self.file_name = "luohan_fist_icon.png"
            self.sfx_file_name = "sfx_luohan_fist.mp3"
            self.animation_file_list = ["a_luohan_fist8.png", "a_luohan_fist8.png", "a_luohan_fist7.png",
                                        "a_luohan_fist6.png", "a_luohan_fist5.png", "a_luohan_fist4.png",
                                        "a_luohan_fist3.png", "a_luohan_fist2.png", "a_luohan_fist1.png"]
            self.base_stamina = 30
            self.base_damage = 20
            self.damage_multiplier = 1.1
            self.description_template = _("少林罗汉拳\n-------------------------\n技能等级: {}\n经验: {}/{}\n基本伤害: {}\n伤害增长率: {}%\n内力消耗: {}\n招式类型: {}\n-------------------------\n第3级, 命中时恢复5%内力\n第6级, 命中时恢复10%内力\n第10级, 命中时恢复20%内力")
            self.type = _("Unarmed")


        elif self.name == _("落樱指法"):
            self.file_name = "falling_cherry_blossom_finger_technique_icon.png"
            self.sfx_file_name = "sfx_finger.mp3"
            self.animation_file_list = ["a_falling_cherry_blossom_finger_technique{}.png".format(i) for i in
                                        range(1, 20)]
            self.base_stamina = 15
            self.base_damage = 10
            self.damage_multiplier = 1.12
            self.description_template = _("落樱指法\n-------------------------\n技能等级: {}\n经验: {}/{}\n基本伤害: {}\n伤害增长率: {}%\n内力消耗: {}\n招式类型: {}\n-------------------------\n第3级, 10%几率封锁敌人穴位, 持续3回合\n第5级, 20%几率封锁敌人穴位, 持续3回合\n第7级, 30%几率封锁敌人穴位, 持续3回合\n第10级, 40%几率封锁敌人穴位, 持续3回合")
            self.type = _("Unarmed")


        elif self.name == _("基本剑法"):
            self.file_name = "basic_sword_technique_icon.png"
            self.sfx_file_name = "sfx_basic_sword_technique.mp3"
            self.animation_file_list = ["a_basic_sword_technique1.gif", "a_basic_sword_technique2.gif",
                                        "a_basic_sword_technique3.gif", "a_basic_sword_technique4.gif",
                                        "a_basic_sword_technique5.gif", "a_basic_sword_technique6.gif",
                                        "a_basic_sword_technique7.gif", "a_basic_sword_technique8.gif",
                                        "a_basic_sword_technique9.gif", "a_basic_sword_technique10.gif",
                                        "a_basic_sword_technique11.gif", "a_basic_sword_technique12.gif",
                                        "a_basic_sword_technique13.gif", "a_basic_sword_technique14.gif"]
            self.base_stamina = 20
            self.base_damage = 15
            self.damage_multiplier = 1.1
            self.description_template = _("基本剑法\n-------------------------\n技能等级: {}\n经验: {}/{}\n基本伤害: {}\n伤害增长率: {}%\n内力消耗: {}\n招式类型: {}\n-------------------------\n第3级, +10%命中率\n第6级, +20%命中率\n第10级, +40%命中率")
            self.type = _("Armed")


        elif self.name == _("Cherry Blossom Drifting Snow Blade"):
            self.file_name = "cherry_blossom_drifting_snow_blade_icon.png"
            self.sfx_file_name = "sfx_cherry_blossom_drifting_snow_blade.mp3"
            self.animation_file_list = ["a_cherry_blossom_drifting_snow_blade1.gif", "a_cherry_blossom_drifting_snow_blade2.gif",
                                        "a_cherry_blossom_drifting_snow_blade3.gif", "a_cherry_blossom_drifting_snow_blade4.gif",
                                        "a_cherry_blossom_drifting_snow_blade5.gif", "a_cherry_blossom_drifting_snow_blade6.gif",
                                        "a_cherry_blossom_drifting_snow_blade7.gif", "a_cherry_blossom_drifting_snow_blade8.gif",
                                        "a_cherry_blossom_drifting_snow_blade9.gif", "a_cherry_blossom_drifting_snow_blade10.gif",
                                        "a_cherry_blossom_drifting_snow_blade11.gif", "a_cherry_blossom_drifting_snow_blade12.gif",
                                        "a_cherry_blossom_drifting_snow_blade13.gif", "a_cherry_blossom_drifting_snow_blade14.gif"]

            self.base_stamina = 40
            self.base_damage = 30
            self.damage_multiplier = 1.15
            self.description_template = _("Cherry Blossom Drifting Snow Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nAt level 3, gains 10% chance of blinding opponent for 3 turns.\nAt level 5, gains 20% chance of blinding opponent for 3 turns.\nAt level 7, gains 30% chance of blinding opponent for 3 turns.\nAt level 10, gains 40% chance of blinding opponent for 3 turns.")
            self.type = _("Armed")

        # self.base_damage = int(self.base_damage * self.damage_multiplier)
        # self.base_stamina = int(self.base_stamina * self.damage_multiplier)
        if self.level != 1:
            self.calibrate_damage()
        self.description = ""
        self.set_description()

    def gain_exp(self, amount):
        self.exp += amount
        if self.level >= 10:
            return
        elif self.exp >= self.level_exp_thresholds[self.level]:
            self.level += 1
            self.base_damage = int(self.base_damage * self.damage_multiplier)
            self.base_stamina = int(self.base_stamina * self.damage_multiplier)
        self.set_description()

    def calibrate_damage(self):
        self.base_damage = int(self.base_damage * self.damage_multiplier ** self.level)
        self.base_stamina = int(self.base_stamina * self.damage_multiplier ** self.level)

    def set_description(self):
        self.description = self.description_template.format(self.level, self.exp,
                                                            self.level_exp_thresholds[min([9, self.level])],
                                                            self.base_damage, int(self.damage_multiplier * 100 - 100),
                                                            self.base_stamina, self.type)


tkWin = Tk()
g = game(tkWin)