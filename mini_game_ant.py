from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext
from settings import*

_ = gettext
class ant_mini_game:
    def __init__(self, main_game):
        self.main_game = main_game
        self.game_width, self.game_height = LANDSCAPE_WIDTH, int(LANDSCAPE_HEIGHT*1.5)
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        pg.font.init()
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True

    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.spiders = pg.sprite.Group()
        self.background = Static(0, self.game_height*1.5-900, "light_brown_dirt.png")
        self.all_sprites.add(self.background)

        self.ant = Ant(self,600,300)
        self.all_sprites.add(self.ant)

        spider_parameters = []
        num_spiders = self.main_game.gamble_amount//25 + 16
        for i in range(num_spiders):
            x = 600 + randrange(150,500)*choice([-1,1])
            y = 300 + randrange(150,250)*choice([-1,1])
            if random() <= .5:
                x_vel = randrange(4,8)*choice([-1,1])
                y_vel = 0
            else:
                x_vel = 0
                y_vel = randrange(4, 8) * choice([-1, 1])
            spider_parameters.append([x,y,x_vel,y_vel])

        for p in spider_parameters:
            spider = Spider(self, *p)
            self.spiders.add(spider)
            self.all_sprites.add(spider)
        self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            try:
                if self.running:
                    self.clock.tick(FPS)
                    self.events()
                    self.update()
                    self.draw()
            except:
                pass


    def update(self):

        self.all_sprites.update()
        collision = pg.sprite.spritecollide(self.ant, self.spiders, False, pg.sprite.collide_circle_ratio(.8))
        if collision:
            self.playing = False
            self.running = False
            survival_time = (pg.time.get_ticks()- self.start_time)/1000
            multiplier = survival_time//15
            print(survival_time)
            print(multiplier)
            self.main_game.gambling_mini_game_reward(multiplier)


    def events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.playing = False
                self.running = False
                survival_time = (pg.time.get_ticks() - self.start_time) / 1000
                multiplier = survival_time // 15
                print(survival_time)
                print(multiplier)
                self.main_game.gambling_mini_game_reward(multiplier)


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move. Avoid the spiders.", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    if start:
                        self.start_time = pg.time.get_ticks()
                        waiting = False
                        self.running = True


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)


if __name__ == "__main__":
    g = ant_mini_game(None)
    g.new()
    pg.quit()
