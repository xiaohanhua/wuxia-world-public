from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class assassinate_xuan_sheng_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = 1200, 600
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina
        pg.font.init()
        pg.mixer.init()


    def new(self):

        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.monks = pg.sprite.Group()
        self.monk_line_of_sights = pg.sprite.Group()
        self.target = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()

        self.background = Static(-50, self.game_height-1350, "shaolin_long_bg.png")
        self.all_sprites.add(self.background)

        self.player = Player(self, 50, self.game_height - 100)
        self.all_sprites.add(self.player)

        self.xuan_sheng = Static(600, self.game_height-1100, "sprite_xuan_sheng.png")
        self.target.add(self.xuan_sheng)
        self.all_sprites.add(self.xuan_sheng)

        platform_parameters = [[0, self.game_height - 40, 1300//23],
                               [0, self.game_height - 17, 1300//23],
                               [0, self.game_height + 6, 1300//23],
                               [0, self.game_height + 29, 1300//23],
                               [0, self.game_height + 52, 1300//23],
                               [0, self.game_height + 75, 1300//23],
                               [0, self.game_height + 98, 1300//23],
                               [0, self.game_height + 121, 1300//23],
                               [0, self.game_height + 144, 1300//23],
                               [200, self.game_height - 224, 2],
                               [200, self.game_height - 201, 2],
                               [200, self.game_height - 178, 2],
                               [200, self.game_height - 155, 2],
                               [200, self.game_height - 132, 2],
                               [200, self.game_height - 109, 2],
                               [200, self.game_height - 86, 2],
                               [200, self.game_height - 63, 2],
                               [48*23, self.game_height - 200, 5],
                               [0, self.game_height - 350, 10],
                               [15*23, self.game_height - 350, 30],
                               [15*23, self.game_height - 350, 30],
                               [33*23, self.game_height - 373, 3],
                               [33*23, self.game_height - 396, 3],
                               [33*23, self.game_height - 419, 3],
                               [33*23, self.game_height - 442, 3],
                               [46*23, self.game_height - 530, 4],
                               [5*23, self.game_height - 610, 37],
                               [18*23, self.game_height - 633, 3],
                               [18*23, self.game_height - 656, 3],
                               [18*23, self.game_height - 679, 3],
                               [0*23, self.game_height - 879, 4],
                               [28*23, self.game_height - 839, 28],
                               [11*23, self.game_height - 1050, 31],
                               ]

        monk_parameters = [[620, self.game_height-40],
                           [950, self.game_height-40],
                           [21*23, self.game_height-350],
                           [28*23, self.game_height-350],
                           [25*23, self.game_height-610],
                           [8*23, self.game_height-610],
                           [40*23, self.game_height-610],
                           [19*23, self.game_height-679],
                           ]

        for p in platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Rock(x + 23 * i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)

        for p in monk_parameters:
            monk = Monk(*p, self)
            self.all_sprites.add(monk)
            self.monks.add(monk)

        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()



    def update(self):

        self.all_sprites.update()

        #PLATFORM COLLISION
        on_screen_platforms = [platform for platform in self.platforms if platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)
        if collision:
            if (self.player.vel.y > self.player.player_height//2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200*1.005**(fall_height-450))
                self.take_damage(damage)
                print("啊！好痛！你因从高处摔落受到{}点伤害".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width*1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height/2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x


        # monk COLLISION
        monk_collision = pg.sprite.spritecollide(self.player, self.monks, True,
                                                   pg.sprite.collide_circle_ratio(.75))
        if monk_collision:
            monk_collision[0].line_of_sight_image.kill()
            self.running = False
            opp = character(_("Monk"), 5,
                            sml=[special_move(_("Shaolin Luohan Fist"), level=3),
                                special_move(_("Basic Punching Technique"), level=3)],
                            skills=[skill(_("Shaolin Mind Clearing Method"), level=2),
                              skill(_("Shaolin Inner Energy Technique"), level=2)]
                            )

            cmd = lambda: self.main_game.resume_mini_game("jy_shanguxingjin2.mp3")
            self.main_game.battleMenu(self.main_game.you, opp, "battleground_shaolin_night.png",
                                      postBattleSoundtrack="jy_shanguxingjin2.mp3",
                                      fromWin=None,
                                      battleType="task", destinationWinList=[] * 3,
                                      postBattleCmd=cmd
            )


        # targ COLLISION
        targ_collision = pg.sprite.spritecollide(self.player, self.target, True, pg.sprite.collide_circle_ratio(.75))
        if targ_collision:
            self.running = False
            self.playing = False
            self.main_game.assassinate_xuan_sheng(1)


        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
            for monk in self.monks:
                monk.rect.y += abs(round(self.player.vel.y))
            for targ in self.target:
                targ.rect.y += abs(round(self.player.vel.y))
            for los in self.monk_line_of_sights:
                los.rect.y += abs(round(self.player.vel.y))

            self.screen_bottom += abs(round(self.player.vel.y))
            self.background.rect.y += abs(round(self.player.vel.y)/4)


        elif self.player.rect.bottom >= self.game_height * 2 //3 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
            for monk in self.monks:
                monk.rect.y -= abs(round(self.player.vel.y))
            for targ in self.target:
                targ.rect.y -= abs(round(self.player.vel.y))
            for los in self.monk_line_of_sights:
                los.rect.y -= abs(round(self.player.vel.y))

            self.screen_bottom -= abs(round(self.player.vel.y))
            self.background.rect.y -= abs(round(self.player.vel.y) / 4)



    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def take_damage(self, damage):
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def show_start_screen(self):
        self.draw_text("用方向键移动. 避开少林僧人的视线，刺杀《玄生》.", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("(提示: 可以在僧人背对着你的时候触碰他们，进入战斗.)", 22, WHITE, self.game_width//2, self.game_height//2.5)
        self.draw_text("按任意键开始游戏", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("游戏失败!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("按R重试或Q返回主菜单.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)


    def detected(self):
        self.playing = False
        self.running = False
        self.main_game.detected_at_shaolin()