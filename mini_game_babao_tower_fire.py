from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class babao_tower_fire_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = 1000, PORTRAIT_HEIGHT
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        pg.font.init()
        pg.mixer.init()
        self.male_grunt = pg.mixer.Sound("sfx_male_voice.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")

        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina


    def new(self):

        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.moving_platforms = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.projectiles = pg.sprite.Group()
        self.goal = pg.sprite.Group()


        self.background = Static(0, self.game_height-1000, "Tower Fire BG.png")
        self.all_sprites.add(self.background)

        self.player = Player(self, 480, self.game_height- 60)
        self.all_sprites.add(self.player)

        self.enemy = Zhang_Andi(950, self.game_height - 385, self)
        self.all_sprites.add(self.enemy)



        platform_parameters = [[920, self.game_height - 385, 4],
                               [920, self.game_height - 362, 4],
                               [920, self.game_height - 339, 4],
                               [920, self.game_height - 316, 4],
                               [920, self.game_height - 293, 4],
                               [920, self.game_height - 270, 4],
                               [920, self.game_height - 247, 4],
                               [920, self.game_height - 224, 4],
                               [920, self.game_height - 201, 4],
                               [920, self.game_height - 178, 4],
                               [920, self.game_height - 155, 4],
                               [920, self.game_height - 132, 4],
                               [920, self.game_height - 109, 4],
                               [920, self.game_height - 86, 4],
                               [920, self.game_height - 63, 4],
                               [920, self.game_height - 40, 4],
                               [920, self.game_height - 17, 4],
                               [920, self.game_height + 6, 4],
                               [920, self.game_height + 29, 4],
                               [920, self.game_height + 52, 4],
                               [920, self.game_height + 75, 4],
                               [920, self.game_height + 98, 4],
                               [920, self.game_height + 121, 4],
                               [920, self.game_height + 144, 4],
                               [920, self.game_height + 167, 4],
                               [920, self.game_height + 190, 4],
                               [920, self.game_height + 213, 4],
                               [580, self.game_height + 10, 4],
                               [400, self.game_height - 40, 6],
                               [200, self.game_height - 10, 4],
                               [50, self.game_height - 110, 3],
                               ]

        for p in platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Rock(x + 23 * i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)


        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30+16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)


        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height

        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            try:
                if self.running:
                    self.clock.tick(FPS)
                    self.events()
                    self.update()
                    self.draw()
            except Exception as e:
                pass


    def update(self):

        self.all_sprites.update()
        # PLATFORM COLLISION

        on_screen_platforms = [platform for platform in self.platforms if
                               platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)
        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x


        # ENEMY PLATFORM COLLISION
        enemy_platform_collision = pg.sprite.spritecollide(self.enemy, on_screen_platforms, False)
        if enemy_platform_collision:
            col = enemy_platform_collision[0]
            self.enemy.rect.bottom = col.rect.top


        if self.player.vel.x > 0 and self.player.pos.x >= self.enemy.rect.left - self.player.player_width*3:
            self.player.pos.x -= self.player.vel.x


        # GOAL COLLISION
        if self.enemy.throw_count >= 200 and self.playing:
            self.playing = False
            self.running = False
            self.main_game.mini_game_in_progress = False
            pg.display.quit()
            self.scene.post_tower_challenge(_("Zhang Andi"))


        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
            for platform in self.moving_platforms:
                platform.y_range[0] += abs(round(self.player.vel.y))
                platform.y_range[1] += abs(round(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y += abs(round(self.player.vel.y))
            for item in self.goal:
                item.rect.y += abs(round(self.player.vel.y))

            self.background.rect.y += abs(round(self.player.vel.y)/4)
            self.screen_bottom += abs(round(self.player.vel.y))
            self.enemy.rect.y += abs(round(self.player.vel.y))

        elif self.player.rect.bottom >= self.game_height * 1 // 2 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
            for platform in self.moving_platforms:
                platform.y_range[0] -= abs(round(self.player.vel.y))
                platform.y_range[1] -= abs(round(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y -= abs(round(self.player.vel.y))
            for item in self.goal:
                item.rect.y -= abs(round(self.player.vel.y))

            self.background.rect.y -= abs(round(self.player.vel.y) / 4)
            self.screen_bottom -= abs(round(self.player.vel.y))
            self.enemy.rect.y -= abs(round(self.player.vel.y))


        #Remove projectile sprites that go off screen
        for projectile in self.projectiles:
            if projectile.rect.x <= -50 or projectile.rect.x > self.game_width + 50:
                projectile.kill()

        # PROJECTILE COLLISION
        projectile_collision = pg.sprite.spritecollide(self.player, self.projectiles, True,
                                                       pg.sprite.collide_circle_ratio(.75))
        if projectile_collision:
            self.take_damage(projectile_collision[0].damage)
            self.player.stun_count += projectile_collision[0].stun

        #SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom*1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def generate_projectile(self, x, y, speed, weapons):

        self.male_grunt.play()
        weapon_choice = choice(weapons)
        if weapon_choice == 'Fire Palm':
            x_dist = self.player.rect.centerx - x
            y_dist = self.player.rect.centery - y
            dist = (x_dist ** 2 + y_dist ** 2) ** .5
            x_vel = x_dist / dist * randrange(15, 23)
            y_vel = y_dist / dist * randrange(15, 23)
            projectile = Projectile(x, y, x_vel, y_vel*(.5+random()), "Fire Palm", "sprite_fire_palm1.png", 300, 0, None)
            self.all_sprites.add(projectile)
            self.projectiles.add(projectile)



    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def take_damage(self, damage):
        self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump.", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)

