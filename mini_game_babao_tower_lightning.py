from mini_game_sprites import *
from character import *
from skill import *
from special_move import *
from item import *
from gettext import gettext

_ = gettext
MAP_STRING = """
..................................................................................................................................................................................XXX................................................................................XXX
..................................................................................................................................................................................XXX................................................................................XXX
..................................................................................................................................................................................XXX................................................................................XXX
..................................................................................................................................................................................XXX................................................................................XXX
..................................................................................................................................................................................XXX................................................................................XXX
..................................................................................................................................................................................XXX................................................................................XXX
..................................................................................................................................................................................XXX................................................................................XXX
..................................................................................................................................................................................XXX................................................................................XXX
..................................................................................................................................................................................XXX................................................................................XXX
...............................................................................................................................................X..................................XXX................................................................................XXX
..................................................................................................................................................................................XXX................................................................................XXX
.............................................................................................................................................X......XX............................XXX................................................................................XXX
..................................................................................................................................................................................XXX................................................................................XXX
...........................................................................................................................................X......................................XXX................................................................................XXX
....................................XXX...............XXXXXXX...........................................................................................XX........................XXX................................................................................XXX
....................................XXX...................................XXXXX..........................................................X........................................XXX................................................................................XXX
....................................XXX...........................................................................................................................................XXX................................................................................XXX
...........................X........XXX............................XX..................................................................X..........................................XXX................................................................................XXX
....................................XXX.............................................................................................................................XX............XXX................................................................................XXX
....................................XXX..............................................................................................X............................................XXX..........................XXXXX....................XXX..........................XXX
...............XXX..................XXX...........................................................................................................................................XXX................................................................................XXX
....................................XXX........XXX.........................XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX................X..............................................XXX.......................................XXXXX....................................XXX
....................................XXX...........................................................................................................................................XXX................................................................................XXX
....................................XXX.......................................................................................XXXX....X.......................................XXXXXXX................................................................................XXX
....................................XXX..................XX.............................................................XXX...XXXX......X.........................................XXX.......................................................................D.......EXXX
.......................XX...........XXX.......................................................................................XXXX........X.......................................XXX.................................................................XXXXXXXXXXXXXXXXXX
....................................XXX.......................................................................................XXXX................................................XXX...................................................................................
....................................XXX.......................XXX...................................................XX........XXXX..................................XX............XXX...................................................................................
....................................XXX.......................................................................................XXXX................................................XXX..........................................................XX.......................
..............................XX....XXX.......................................................................................XXXX................................................XXX...................................................................................
....................................XXX....................................XXXXXXXXXXXXXXXXXXXXXXXXX..........................XXXX................................................XXX...................................................................................
....................................XXX.......................................................................................XXXX................................................XXX..................................................XXX..............................
....................................XXX.......................................................................................XXXX................................................XXX.............................XXXXX.................................................
....................................XXX.......................................................................................XXXX...........................XXXX.................XXX...................................................................................
....................................XXX..................................................................XXXXXX...............XXXX......................................................................................................................................
..................XXXXXXX...........XXX.......................................................................................XXXX....................................................................XXXXX..................XXX........................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
XXXXXXXX............................XXX.......................................................................................XXXX....................................XXX...............................................................................................
....................................XXX...................XXXXXXXXXXXX........................................................XXXX..........................................................XX..........................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX.............................................XXX......................................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX....................................................XXX...............................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
....................................XXX.......................................................................................XXXX......................................................................................................................................
"""


class babao_tower_lightning_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT + 200
        self.screen = pg.display.set_mode((self.game_width, self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina
        pg.font.init()
        pg.mixer.init()
        self.male_grunt = pg.mixer.Sound("sfx_male_voice.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")

        #pg.mixer.pre_init()
        #pg.mixer.init()
        #pg.mixer.music.load("sfx_rain_3.ogg")
        #pg.mixer.music.play()


    def new(self):

        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.falling_platforms = pg.sprite.Group()
        self.projectiles = pg.sprite.Group()
        self.drops = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.rain_sprites = pg.sprite.Group()
        self.goal = pg.sprite.Group()

        self.background = Static(0, self.game_height - 1500, "Tower Lightning Thunder BG.png")
        self.all_sprites.add(self.background)

        self.player = Player(self, 60, self.game_height - 420)
        self.all_sprites.add(self.player)

        map_string_split = MAP_STRING.strip().split("\n")
        map_string_split.reverse()
        base_y = self.game_height - 40
        for i in range(len(map_string_split)):
            row = map_string_split[i]
            for j in range(len(row)):
                char = row[j]
                if char == "X":
                    tile = Tile_Rock(j * 23, base_y - 23 * i)
                    self.all_sprites.add(tile)
                    self.platforms.add(tile)
                elif char == "D":
                    self.door = Static(j * 23, base_y - 23 * i - 65, "sprite_castledoors.png")
                    self.all_sprites.add(self.door)
                    self.goal.add(self.door)
                elif char == "E":
                    self.enemy = Li_Mianzhuo(j * 23, base_y - 23 * i + 30, self)
                    self.all_sprites.add(self.enemy)


        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30 + 16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)

        self.last_rain_animation = 0

        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        if not self.retry:
            self.show_start_screen()
        self.run()


    def generate_rain(self, n):
        try:
            if self.playing and self.running:
                for i in range(n):
                    random_x = randrange(int(self.game_width*1.5))
                    random_y = randrange(-200,-100)
                    random_falling_speed = randrange(11, 16)
                    rain = Rain(random_x, random_y, random_falling_speed, self)
                    self.all_sprites.add(rain)
                    self.rain_sprites.add(rain)
        except:
            pass


    def generate_projectile(self, x, y, speed, weapons):
        self.male_grunt.play()
        weapon_choice = choice(weapons)
        if weapon_choice == 'Lightning Finger Technique':
            x_dist = self.player.rect.centerx - x
            y_dist = self.player.rect.centery - y
            dist = (x_dist ** 2 + y_dist ** 2) ** .5
            x_vel = x_dist / dist * randrange(20, 26)
            y_vel = y_dist / dist * randrange(15, 26)
            projectile = Projectile(x, y, x_vel, y_vel, "Lightning Finger Technique",
                                    "sprite_lightning_finger_technique1.png", 100, 0, None)
            self.all_sprites.add(projectile)
            self.projectiles.add(projectile)


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def update(self):

        self.all_sprites.update()
        on_screen_platforms = [platform for platform in self.platforms if
                               platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)

        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height * 3 // 5) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x


        # PROJECTILE COLLISION
        projectile_collision = pg.sprite.spritecollide(self.player, self.projectiles, True,
                                                       pg.sprite.collide_circle_ratio(.75))
        if projectile_collision:
            self.take_damage(projectile_collision[0].damage)
            self.player.stun_count += projectile_collision[0].stun
            self.player.vel.x -= 20


        # GOAL COLLISION
        goal_collision = pg.sprite.spritecollide(self.player, self.goal, False)
        if goal_collision and self.playing:
            self.playing = False
            self.running = False
            self.main_game.mini_game_in_progress = False
            pg.display.quit()
            self.scene.post_tower_challenge(_("Li Mianzhuo"))


        # WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
                if type(platform) == Tile_Ice:
                    platform.dissolve_height += abs(round(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y += abs(round(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y += abs(round(self.player.vel.y))
            for item in self.goal:
                item.rect.y += abs(round(self.player.vel.y))
            for rain in self.rain_sprites:
                rain.rect.y += abs(round(self.player.vel.y))

            self.screen_bottom += abs(round(self.player.vel.y))
            self.background.rect.y += abs(round(self.player.vel.y) / 4)
            self.enemy.rect.y += abs(round(self.player.vel.y))


        elif self.player.rect.bottom >= self.game_height * 1 // 2 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
                if type(platform) == Tile_Ice:
                    platform.dissolve_height += abs(round(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y -= abs(round(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y -= abs(round(self.player.vel.y))
            for item in self.goal:
                item.rect.y -= abs(round(self.player.vel.y))
            for rain in self.rain_sprites:
                rain.rect.y -= abs(round(self.player.vel.y))

            self.screen_bottom -= abs(round(self.player.vel.y))
            self.background.rect.y -= abs(round(self.player.vel.y) / 4)
            self.enemy.rect.y -= abs(round(self.player.vel.y))


        if self.player.rect.right >= self.game_width * 1 // 2 and self.player.vel.x > 0.5 and self.player.absolute_pos < int(
                self.game_width * 11):
            self.player.pos.x -= abs(self.player.vel.x)
            for platform in self.platforms:
                platform.rect.x -= abs(self.player.vel.x)
            for projectile in self.projectiles:
                projectile.rect.x -= abs(self.player.vel.x)
            for drop in self.drops:
                drop.rect.x -= abs(self.player.vel.x)
            for item in self.goal:
                item.rect.x -= abs(self.player.vel.x)
            for rain in self.rain_sprites:
                rain.rect.x -= abs(self.player.vel.x)

            self.background.rect.x -= abs(self.player.vel.x / 4)
            self.enemy.rect.x -= abs(self.player.vel.x)


        # Remove projectile sprites that go off the screen
        for projectile in self.projectiles:
            if projectile.rect.x <= -50:
                projectile.kill()

        # SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom * 1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()

        now = pg.time.get_ticks()
        if now - self.last_rain_animation > 200:
            self.generate_rain(100)
            self.last_rain_animation = now


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()

    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def take_damage(self, damage):
        self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()

    def reset_falling_platforms(self, value=0):
        for fp in self.falling_platforms:
            fp.total_standing_time = value

    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump.", 22, WHITE, self.game_width // 2, self.game_height // 3)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()

    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0, 0, 0, 200))
        self.screen.blit(self.dim_screen, (0, 0))
        self.draw_text("You died!", 22, WHITE, self.game_width // 2, self.game_height // 3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2,
                       self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)

    def listen_for_key(self, start=True):  # if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()

    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)