from mini_game_sprites import *
from character import *
from skill import *
from special_move import *
from item import *
from gettext import gettext

_ = gettext
MAP_STRING = """
.................S.....S.......................................S.............S........................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
............................................................................................................................................................VVVVVVVVVVVVVVVVVVVVSVVVVSO..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O.....SVVVVVVVVVS............................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
........................VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV....VVVVVVVVVV..........................................................................................................O..O.O.....O.O..O..O.O.O...O.O.O...O...O.O...O...O.O..O.O............................................
......................................................................................................................................................................................VVVVSVVVVVVVSVSSVSSVSVVVSSSVVVSVSSSVVVVVSVSSSVVVVVSVVVVSVVVV.........................................
...........................................................................................................................................................................................................................................................................................
........................................................................................................................................................................................................................................................................S.....S.....S...D..
.............................................................................................................................................................................................................................................................................OOOOOOOOOOOOOO
...........................................................................................................................................................................................................................................................................................
...........................................................................................................................................................................................................................................................................................
.....................................................................................................................................................................................................................................................................OOOO..................
...........................................................................................................................................................................................................................................................................................
...........................................................................................................................................................................................................................................................................................
............................................................................................................................................................................OOO..XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX..OOO........................
...........................................................................................................................................................................................................................................................................................
...........................................................................................................................................................................................................................................................................................
......................................................................................................................................................................O....................................................................................................................
...........................................................................................................................................................................................................................................................................................
...........................................................................................................................................................................................................................................................................................
.................................................................OOO.......................................................................................................................................................................................................................
..................................................................................................................................................................O........................................................................................................................
........................................................................XXX................................................................................................................................................................................................................
..............................................................................XXX..........................................................................................................................................................................................................
...................................OOO..X..X..OOO..............................................................................................OO....XXX....OOO............................................................................................................................
...............................................................................................................XXX.........................................................................................................................................................................
........................................................................................................................................OOO................................................................................................................................................
.............................................................O.............................................................................................................................................................................................................................
.....................................................................................OO.S.O....OO.S.XX...OO................................................................................................................................................................................
...........................................................................................................................................................................................................................................................................................
..........................................................O...........................................................XXXX.................................................................................................................................................................
...........X..............................................................................................................................................................................................................................................................OOOOOOOO.........
..............X...............XX...........................................................................................................................................................................................................................................................
.................OO...OOO..X........................XXX....................................................................................................................................................................................................................................
OOOOOOOO......................................................................................................................OOOOOOO......................................................................................................................................................
...........................................................................................................................................................................................................................................................................................
.....................................................................................................................................................................OOOOOOOOOOOOOOOOOOOOOOOOO.............................................................................................
...........................................................................................................................................................................................................................................................................................
.........................................OOOOOOO...............................................................................................OOOOOOO.....................................................................................................................................
..................................................................OOOOO................OOOOOO..............................................................................................................................................................................................
............................................................................OOO.......................OOOOOOOOO............................................................................................................................................................................
...........................................................................................................................................................................................................................................................................................
"""


class babao_tower_metal_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT + 200
        self.screen = pg.display.set_mode((self.game_width, self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")


    def new(self):

        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.conveyors = pg.sprite.Group()
        self.spike_ceilings = pg.sprite.Group()
        self.spike_walls = pg.sprite.Group()
        self.falling_spike_balls = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.goal = pg.sprite.Group()

        self.background = Static(0, 0, "Tower Metal BG.png")
        self.all_sprites.add(self.background)

        self.spike_wall = Spike_Wall(-50, self.game_height-2700, 4, self)
        self.spike_walls.add(self.spike_wall)
        self.all_sprites.add(self.spike_wall)

        self.player = Player(self, 150, self.game_height - 220)
        self.all_sprites.add(self.player)

        metal_platform_parameters = []
        spike_platform_parameters = []
        conveyor_parameters = []
        map_string_split = MAP_STRING.strip().split("\n")
        map_string_split.reverse()
        base_y = self.game_height - 40
        for i in range(len(map_string_split)):
            row = map_string_split[i]
            for j in range(len(row)):
                char = row[j]
                if char == "X":
                    conveyor_parameters.append([j * 82, base_y - 25 * i, 1])
                elif char == "O":
                    metal_platform_parameters.append([j * 82, base_y - 25 * i, 1])
                elif char == "V":
                    spike_platform_parameters.append([j * 82, base_y - 25 * i, 1])
                elif char == "S":
                    fsb = Falling_Spike_Ball(j*82, 1, self)
                    self.falling_spike_balls.add(fsb)
                    self.all_sprites.add(fsb)
                elif char == "D":
                    self.door = Static(j * 82, base_y - 25 * i - 65, "sprite_castledoors.png")
                    self.all_sprites.add(self.door)
                    self.goal.add(self.door)

        for p in metal_platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Static(x + 82 * i, y, "tile_metal_platform.png")
                self.all_sprites.add(platform)
                self.platforms.add(platform)

        for p in spike_platform_parameters:
            x, y, r = p
            for i in range(r):
                spike_ceiling = Static(x + 82 * i, y, "sprite_spike_ceiling.png")
                self.all_sprites.add(spike_ceiling)
                self.platforms.add(spike_ceiling)
                self.spike_ceilings.add(spike_ceiling)

        for p in conveyor_parameters:
            x, y, r = p
            for i in range(r):
                platform = Static(x + 82 * i, y, "sprite_conveyor_belt_right_1.png",
                                  animations=[pg.image.load("sprite_conveyor_belt_right_{}.png".format(i)) for i in range(1,4)])
                self.all_sprites.add(platform)
                self.platforms.add(platform)
                self.conveyors.add(platform)


        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30 + 16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)


        self.last_wall_speed_up = 0
        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def update(self):

        self.all_sprites.update()

        # PLATFORM COLLISION
        on_screen_platforms = [platform for platform in self.platforms if
                               platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)
        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x


        # CONVEYOR COLLISION
        conveyor_collision = pg.sprite.spritecollide(self.player, self.conveyors, False)
        if conveyor_collision:
            self.player.vel.x += 1


        # SPIKE CEILING COLLISION
        spike_ceiling_collision = pg.sprite.spritecollide(self.player, self.spike_ceilings, False)
        if spike_ceiling_collision:
            if self.player.rect.bottom >= spike_ceiling_collision[0].rect.bottom:
                self.take_damage(100)
                self.player.stun_count += randrange(8,16)


        # FALLING SPIKE BALL COLLISION
        falling_spike_ball_collision = pg.sprite.spritecollide(self.player, self.falling_spike_balls, False,
                                                       pg.sprite.collide_circle_ratio(.75))
        if falling_spike_ball_collision:
            fsbc = falling_spike_ball_collision[0]
            self.take_damage(int(fsbc.moving_speed)*30)
            self.player.stun_count += int(fsbc.moving_speed/2)
            self.player.vel.y -= (fsbc.moving_speed/2)
            if self.player.vel.y <= 0:
                self.player.vel.y = 0
            if fsbc.rect.left >= self.player.rect.right - self.player.player_width:
                self.player.vel.x += -int(fsbc.moving_speed/2)
            else:
                self.player.vel.x += int(fsbc.moving_speed/2)


        # SPIKE WALL COLLISION
        spike_wall_collision = pg.sprite.spritecollide(self.player, self.spike_walls, False)
        if spike_wall_collision:
            self.take_damage(200)
            self.player.vel.x += spike_wall_collision[0].moving_speed*6


        # GOAL COLLISION
        goal_collision = pg.sprite.spritecollide(self.player, self.goal, False)
        if goal_collision and self.playing:
            self.playing = False
            self.running = False
            self.main_game.mini_game_in_progress = False
            pg.display.quit()
            self.scene.post_tower_challenge(_("Chen Jin"))

        # WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
            for fsb in self.falling_spike_balls:
                fsb.rect.y += abs(round(self.player.vel.y))
            for item in self.goal:
                item.rect.y += abs(round(self.player.vel.y))
            for spike_wall in self.spike_walls:
                spike_wall.rect.y += abs(round(self.player.vel.y))

            self.screen_bottom += abs(round(self.player.vel.y))
            #self.background.rect.y += abs(round(self.player.vel.y) // 4)


        elif self.player.rect.bottom >= self.game_height * 1 // 2 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
            for fsb in self.falling_spike_balls:
                fsb.rect.y -= abs(round(self.player.vel.y))
            for item in self.goal:
                item.rect.y -= abs(round(self.player.vel.y))
            for spike_wall in self.spike_walls:
                spike_wall.rect.y -= abs(round(self.player.vel.y))

            self.screen_bottom -= abs(round(self.player.vel.y))
            #self.background.rect.y -= abs(round(self.player.vel.y) // 4)

        if self.player.rect.right >= self.game_width * 1 // 2 and self.player.vel.x > 0.5 and self.player.absolute_pos < int(
                self.game_width * 22):
            self.player.pos.x -= abs(self.player.vel.x)
            for platform in self.platforms:
                platform.rect.x -= abs(self.player.vel.x)
            for fsb in self.falling_spike_balls:
                fsb.rect.x -= abs(self.player.vel.x)
                fsb.starting_x -= abs(self.player.vel.x)
            for item in self.goal:
                item.rect.x -= abs(self.player.vel.x)
            for spike_wall in self.spike_walls:
                spike_wall.rect.x -= abs(self.player.vel.x)

            #self.background.rect.x -= abs(self.player.vel.x // 6)


        # SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom * 1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()

        now = pg.time.get_ticks()
        if now - self.last_wall_speed_up >= randrange(180000, 250000):
            self.last_wall_speed_up = now
            self.spike_wall.moving_speed += 1


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()

    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()

    def take_damage(self, damage):
        self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()

    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump.", 22, WHITE, self.game_width // 2, self.game_height // 3)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()

    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0, 0, 0, 200))
        self.screen.blit(self.dim_screen, (0, 0))
        self.draw_text("You died!", 22, WHITE, self.game_width // 2, self.game_height // 3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2,
                       self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)

    def listen_for_key(self, start=True):  # if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()

    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)