from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class babao_tower_wind_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = 1000, PORTRAIT_HEIGHT
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.wind_sound = pg.mixer.Sound("wind-howl-01.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")

        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina


    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.whirlwinds = pg.sprite.Group()
        self.goal = pg.sprite.Group()


        self.background = Static(0, self.game_height-3600, "Tower Wind BG.png")
        self.all_sprites.add(self.background)

        self.player = Player(self, 80, self.game_height- 60)
        self.all_sprites.add(self.player)

        self.door = Static(930, self.game_height-2555, "sprite_castledoors.png")
        self.all_sprites.add(self.door)
        self.goal.add(self.door)

        self.black_clouds = Black_Clouds(self)
        self.all_sprites.add(self.black_clouds)
        self.last_cloud_animation = 0


        platform_parameters = [[0, self.game_height - 40, 20],
                               [350, self.game_height - 200, 10],
                               [790, self.game_height - 250, 4],
                               [450, self.game_height - 400, 8],
                               [30, self.game_height - 450, 5],
                               [250, self.game_height - 600, 8],
                               [600, self.game_height - 740, 5],
                               [800, self.game_height - 930, 5],
                               [550, self.game_height - 1030, 5],
                               [375, self.game_height - 1200, 5],
                               [0, self.game_height - 1370, 11],
                               [120, self.game_height - 1550, 3],
                               [0, self.game_height - 1730, 3],
                               [400, self.game_height - 1500, 4],
                               [650, self.game_height - 1650, 4],
                               [800, self.game_height - 1800, 4],
                               [580, self.game_height - 1850, 5],
                               [50, self.game_height - 2000, 20],
                               [300, self.game_height - 2300, 10],
                               [800, self.game_height - 2480, 12],
                               ]

        for p in platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Rock(x + 23 * i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)

        whirlwind_parameters = [[400, self.game_height - 240],
                                [500, self.game_height - 440],
                                [300, self.game_height - 640],
                                [670, self.game_height - 780],
                                [420, self.game_height - 1240],
                                [30, self.game_height - 1410],
                                [150, self.game_height - 1410],
                                [870, self.game_height - 1840],
                                [190, self.game_height - 2040],
                                [450, self.game_height - 2340],
                                [870, self.game_height - 2520]]

        for p in whirlwind_parameters:
            whirlwind = Static(*p, "sprite_whirlwind1.png", [pg.image.load("sprite_whirlwind{}.png".format(i)) for i in range(1,5)])
            self.all_sprites.add(whirlwind)
            self.whirlwinds.add(whirlwind)


        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30+16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)


        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height

        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            try:
                if self.running:
                    self.clock.tick(FPS)
                    self.events()
                    self.update()
                    self.draw()
            except Exception as e:
                pass


    def update(self):

        self.all_sprites.update()
        # PLATFORM COLLISION
        collision = pg.sprite.spritecollide(self.player, self.platforms, False)

        if collision:

            if True:
                if (self.player.vel.y > self.player.player_height // 2) or \
                        (self.player.vel.y > 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height * 3 // 5) or \
                        (self.player.vel.y < 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height // 2):
                    self.player.pos.y = collision[0].rect.top
                    self.player.vel.y = 0

                elif self.player.vel.y < 0:
                    self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                    self.player.vel.y = 0

                if self.player.vel.y == 0:
                    for col in collision:
                        if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                            self.player.pos.y = col.rect.top
                            self.player.vel.y = 0

                for col in [platform for platform in self.platforms if platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]:
                    if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                            self.player.pos.x <= col.rect.left and \
                            self.player.pos.y - col.rect.top <= self.player.player_height and \
                            self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                        self.player.pos.x -= self.player.vel.x
                        self.player.absolute_pos -= self.player.vel.x
                    elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                            self.player.pos.x >= col.rect.right and \
                            self.player.pos.y - col.rect.top <= self.player.player_height and \
                            self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                        self.player.pos.x -= self.player.vel.x
                        self.player.absolute_pos -= self.player.vel.x

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                print("Ouch! Lost {} health from fall.".format(damage))
                self.take_damage(damage)

            self.player.last_height = current_height
            self.player.jumping = False


        # GOAL COLLISION
        goal_collision = pg.sprite.spritecollide(self.player, self.goal, False)
        if goal_collision and self.playing:
            self.playing = False
            self.running = False
            self.main_game.mini_game_in_progress = False
            pg.display.quit()
            self.scene.post_tower_challenge(_("Wen Yulan"))


        # WHIRLWIND COLLISION
        whirlwind_collision =  pg.sprite.spritecollide(self.player, self.whirlwinds, False,
                                                       pg.sprite.collide_circle_ratio(.85))
        if whirlwind_collision:
            self.player.vel.x += randrange(15,21) * choice([-1,1])
            self.player.vel.y -= randrange(15,21)
            self.take_damage(100)


        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
            for item in self.goal:
                item.rect.y += abs(round(self.player.vel.y))
            for ww in self.whirlwinds:
                ww.rect.y += abs(round(self.player.vel.y))

            self.background.rect.y += abs(round(self.player.vel.y)/4)
            self.screen_bottom += abs(round(self.player.vel.y))

        elif self.player.rect.bottom >= self.game_height * 2 // 3 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
            for item in self.goal:
                item.rect.y -= abs(round(self.player.vel.y))
            for ww in self.whirlwinds:
                ww.rect.y -= abs(round(self.player.vel.y))

            self.background.rect.y -= abs(round(self.player.vel.y) / 4)
            self.screen_bottom -= abs(round(self.player.vel.y))


        #SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom*1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()


        #BLACK CLOUD ANIMATION

        now = pg.time.get_ticks()
        if now - self.last_cloud_animation > randrange(3500, 5000):
            self.wind_sound.play()
            self.black_clouds.set_on_screen()
            self.black_clouds.vx = randrange(10,31)
            self.last_cloud_animation = now



    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def take_damage(self, damage):
        self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()

    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump.", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)

