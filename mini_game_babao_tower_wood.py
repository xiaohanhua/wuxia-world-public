from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class babao_tower_wood_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = 1000, PORTRAIT_HEIGHT
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")

        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina

    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.moving_platforms = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.goal = pg.sprite.Group()

        self.background = Static(0, self.game_height-4800, "Tower Wood BG.png")
        self.all_sprites.add(self.background)

        self.player = Player(self, 80, self.game_height- 60)
        self.all_sprites.add(self.player)

        self.door = Static(50, self.game_height-4175, "sprite_castledoors.png")
        self.all_sprites.add(self.door)
        self.goal.add(self.door)


        platform_parameters = [[0, self.game_height - 40, 20],
                               [0, self.game_height - 17, 20],
                               [0, self.game_height + 6, 20],
                               [0, self.game_height + 29, 20],
                               [0, self.game_height + 52, 20],
                               [0, self.game_height + 75, 20],
                               [0, self.game_height + 98, 20],
                               [0, self.game_height + 121, 20],
                               [0, self.game_height + 144, 20],
                               [0, self.game_height + 167, 20],
                               [150, self.game_height - 880, 5],
                               [150, self.game_height - 1500, 5],
                               [850, self.game_height - 2400, 5],
                               [550, self.game_height - 2550, 5],
                               [150, self.game_height - 3050, 30],
                               [50, self.game_height - 3700, 5],
                               [0, self.game_height - 4100, 15],
                               ]

        for p in platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Earth(x + 23 * i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)


        #__init__(self, x, y, w, h, vx=0, vy=0, x_range=(0,0), y_range=(0,0), moving=False):
        moving_platform_parameters = [[140, self.game_height - 150, 140, 20, -4, 0, [140, 800], [0, 0], True],
                                      [540, self.game_height - 250, 100, 20, 6, 0, [50, 850], [0, 0], True],
                                      [40, self.game_height - 360, 100, 20, 3, 0, [40, 300], [0, 0], True],
                                      [200, self.game_height - 500, 150, 20, -5, 0, [40, 600], [0, 0], True],
                                      [400, self.game_height - 640, 100, 20, 5, 0, [200, 800], [0, 0], True],
                                      [600, self.game_height - 770, 80, 20, 3, 0, [600, 800], [0, 0], True],
                                      [600, self.game_height - 1050, 80, 20, -5, 0, [300, 800], [0, 0], True],
                                      [500, self.game_height - 1200, 50, 20, 4, 0, [0, 800], [0, 0], True],
                                      [400, self.game_height - 1350, 50, 20, -4, 0, [0, 800], [0, 0], True],
                                      [300, self.game_height - 1700, 70, 20, -4, 0, [0, 800], [0, 0], True],
                                      [300, self.game_height - 1870, 70, 20, 4, 0, [0, 800], [0, 0], True],
                                      [300, self.game_height - 2070, 70, 20, -4, 0, [0, 800], [0, 0], True],
                                      [300, self.game_height - 2250, 70, 20, 4, 0, [0, 800], [0, 0], True],
                                      [200, self.game_height - 2700, 100, 20, 6, 0, [0, 800], [0, 0], True],
                                      [300, self.game_height - 2800, 100, 20, -6, 0, [0, 800], [0, 0], True],
                                      [400, self.game_height - 2900, 100, 20, 6, 0, [0, 800], [0, 0], True],
                                      [250, self.game_height - 3200, 100, 20, 7, 0, [0, 800], [0, 0], True],
                                      [450, self.game_height - 3370, 100, 20, 8, 0, [0, 800], [0, 0], True],
                                      [350, self.game_height - 3550, 100, 20, -7, 0, [0, 800], [0, 0], True],
                                      [700, self.game_height - 3850, 50, 20, 10, 0, [0, 800], [0, 0], True],
                                      #[400, self.game_height - 1200, 150, 20, 0, 5, [0, 0], [self.game_height-1400, self.game_height-900], True],
                                      ]

        for p in moving_platform_parameters:
            platform = Platform_Earth(*p)
            self.all_sprites.add(platform)
            self.platforms.add(platform)
            self.moving_platforms.add(platform)





        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30+16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)


        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height

        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            try:
                if self.running:
                    self.clock.tick(FPS)
                    self.events()
                    self.update()
                    self.draw()
            except:
                pass


    def update(self):

        self.all_sprites.update()
        # PLATFORM COLLISION
        collision = pg.sprite.spritecollide(self.player, self.platforms, False)
        # MOVING PLATFORM COLLISION
        moving_platform_collision = pg.sprite.spritecollide(self.player, self.moving_platforms, False)

        if collision:

            if moving_platform_collision:
                moving_platform = moving_platform_collision[0]
                if self.player.rect.bottom >= moving_platform.rect.top:
                    if abs(self.player.vel.x) <= abs(moving_platform.vx):
                        self.player.pos.x += moving_platform.vx
                self.player.standing_on_moving_platform = True

            else:
                self.player.standing_on_moving_platform = False

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            if True:
                if (self.player.vel.y > self.player.player_height // 2) or \
                        (self.player.vel.y > 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height * 3 // 5) or \
                        (self.player.vel.y < 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height // 2):
                    self.player.pos.y = collision[0].rect.top
                    self.player.vel.y = 0

                elif self.player.vel.y < 0:
                    self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                    self.player.vel.y = 0

                for col in [platform for platform in self.platforms if platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]:
                    if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                            self.player.pos.x <= col.rect.left and \
                            self.player.pos.y - col.rect.top <= self.player.player_height and \
                            self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                        self.player.pos.x -= self.player.vel.x
                        self.player.absolute_pos -= self.player.vel.x
                    elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                            self.player.pos.x >= col.rect.right and \
                            self.player.pos.y - col.rect.top <= self.player.player_height and \
                            self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                        self.player.pos.x -= self.player.vel.x
                        self.player.absolute_pos -= self.player.vel.x

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                print("Ouch! Lost {} health from fall.".format(damage))
                self.take_damage(damage)

            self.player.last_height = current_height
            self.player.jumping = False


        # GOAL COLLISION
        goal_collision = pg.sprite.spritecollide(self.player, self.goal, False)
        if goal_collision and self.playing:
            self.playing = False
            self.running = False
            self.main_game.mini_game_in_progress = False
            pg.display.quit()
            self.scene.post_tower_challenge(_("Han Lin"))


        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
            for platform in self.moving_platforms:
                platform.y_range[0] += abs(round(self.player.vel.y))
                platform.y_range[1] += abs(round(self.player.vel.y))
            for item in self.goal:
                item.rect.y += abs(round(self.player.vel.y))

            self.background.rect.y += abs(round(self.player.vel.y)/4)
            self.screen_bottom += abs(round(self.player.vel.y))

        elif self.player.rect.bottom >= self.game_height * 2 // 3 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
            for platform in self.moving_platforms:
                platform.y_range[0] -= abs(round(self.player.vel.y))
                platform.y_range[1] -= abs(round(self.player.vel.y))
            for item in self.goal:
                item.rect.y -= abs(round(self.player.vel.y))

            self.background.rect.y -= abs(round(self.player.vel.y) / 4)
            self.screen_bottom -= abs(round(self.player.vel.y))


        #SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom*1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def take_damage(self, damage):
        self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()

    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump.", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)

