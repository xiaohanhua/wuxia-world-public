from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from helper_funcs import*
from gettext import gettext
from random import*

_ = gettext

class explore_eagle_sect_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = PORTRAIT_WIDTH, PORTRAIT_HEIGHT
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        pg.font.init()
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina


    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.drops = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()


        self.background = Static(0, -600, "Eagle Sect.png")
        self.all_sprites.add(self.background)

        self.player = Player(self,200,250)
        self.all_sprites.add(self.player)

        platform_parameters = [[0, 250, 12],
                               [500, 380, 3],
                               [750, 550, 2],
                               [500, 710, 3],
                               [300, 730, 3],
                               [600, 920, 3],
                               [400, 1190, 3],
                               [200, 1440, 3],
                               [0, 1650, 2],
                               [200, 1900, 3],
                               [500, 2000, 3],
                               [700, 2240, 3],
                               [560, 2440, 2],
                               [160, 2550, 6],
                               ]

        self.collected_drops = []
        self.generate_drop(200,2525,1)
        self.generate_drop(180,2525,.8)
        self.generate_drop(310,705,.4)
        self.generate_drop(220,1415,.6)
        self.generate_drop(515,1975,.8)
        self.generate_drop(725,2215,.8)


        base_platform_parameters = [[0, self.game_height + 3600, 40],
                                   [0, self.game_height + 3623, 40],
                                   [0, self.game_height + 3646, 40],
                                   [0, self.game_height + 3669, 40],
                                   [0, self.game_height + 3692, 40],
                                   [0, self.game_height + 3715, 40],
                                   [0, self.game_height + 3738, 40],
                                   [0, self.game_height + 3761, 40],
                                   [0, self.game_height + 3784, 40],
                                   [0, self.game_height + 3807, 40],
                                   [0, self.game_height + 3830, 40],
                                   [0, self.game_height + 3853, 40],
                                   [0, self.game_height + 3876, 40],
                                   [0, self.game_height + 3899, 40],
                                   [0, self.game_height + 3922, 40]]

        for p in platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Rock(x+23*i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)

        for p in base_platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Rock(x+23*i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)

        self.original_height = self.player.pos.y
        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            if self.running:
                try:
                    self.update()
                    self.clock.tick(FPS)
                    self.events()
                    self.draw()
                except:
                    pass


    def update(self):

        self.all_sprites.update()

        #PLATFORM COLLISION
        collision = pg.sprite.spritecollide(self.player, self.platforms, False)
        if collision:

            if (self.player.vel.y > self.player.player_height//2) or (self.player.vel.y > 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2) or (self.player.vel.y < 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
                current_height = collision[0].rect.top
                fall_height = current_height - self.player.last_height
                self.player.last_height = current_height
                if fall_height >= 450:
                    damage = int(200 * 1.005 ** (fall_height - 450))
                    print("Ouch! Lost {} health from fall.".format(damage))
                    self.main_game.health -= damage
                    if self.main_game.health <= 0:
                        self.main_game.health = 0
                        self.playing = False
                        self.running = False
                        self.show_game_over_screen()

            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            for col in [platform for platform in self.platforms if platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]:
                if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                        self.player.pos.x <= col.rect.left and \
                        self.player.pos.y - col.rect.top <= self.player.player_height and \
                        self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                    self.player.pos.x -= self.player.vel.x
                    self.player.absolute_pos -= self.player.vel.x
                elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                        self.player.pos.x >= col.rect.right and \
                        self.player.pos.y - col.rect.top <= self.player.player_height and \
                        self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                    self.player.pos.x -= self.player.vel.x
                    self.player.absolute_pos -= self.player.vel.x

            self.player.jumping = False
            self.player.friction = PLAYER_FRICTION

        else:
            self.player.friction = PLAYER_FRICTION

        #DROP COLLISION
        drop_collision = pg.sprite.spritecollide(self.player, self.drops, True, pg.sprite.collide_circle_ratio(.75))
        if drop_collision:
            drop = drop_collision[0]
            self.pick_up_drop(drop)

        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 5 and self.player.vel.y < 0:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y += abs(round(self.player.vel.y))

            self.original_height += abs(round(self.player.vel.y))
            self.background.rect.y += abs(round(self.player.vel.y)/10)


        elif self.player.rect.bottom >= self.game_height * 2//5 and self.player.vel.y > 0:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y -= abs(round(self.player.vel.y))

            self.original_height -= abs(round(self.player.vel.y))
            self.background.rect.y -= abs(round(self.player.vel.y)/8)


        #Check for return to original spot
        if self.player.pos.x <= 50 and self.player.vel.x < 0 and self.player.pos.y <= self.original_height and self.playing and self.running:
            self.playing = False
            self.running = False
            self.scene.done_exploring(self.collected_drops)


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump.", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Collect the herbs and return to the starting spot.", 22, WHITE, self.game_width//2, self.game_height//2)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height * 2//3)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)


    def generate_drop(self, x, y, prob):
        month = calculate_month_day_year(self.main_game.gameDate)["Month"]
        if month in [4, 5, 6, 7, 8, 9]:
            prob *= 1.5
        if random() <= prob:
            drop_pool = [Drops(x, y, "Elderberry", "sprite_elderberry.png"),
                         Drops(x, y, "Spiral Aloe Leaf", "sprite_spiral_aloe_leaf.png"),
                         Drops(x, y, "White Snakeroot", "sprite_white_snakeroot.png"),
                         Drops(x, y, "Poison Ivy", "sprite_poison_ivy.png"),
                         Drops(x, y, "Stinging Nettle", "sprite_stinging_nettle.png"),]
            if random() <= .05:
                drop = Drops(x, y, "Caterpillar Fungus", "sprite_caterpillar_fungus.png")
            else:
                drop = choice(drop_pool)
            self.drops.add(drop)
            self.all_sprites.add(drop)


    def pick_up_drop(self, drop):
        self.main_game.currentEffect = "button-19.mp3"
        self.main_game.startSoundEffectThread()
        self.collected_drops.append(drop.name)
        print(_("Picked up"), drop.name)
        #print(self.collected_drops)