from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class hattori_ninjutsu_level_3_mini_game:
    def __init__(self, main_game, scene, time_limit):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = 1200, 650
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        self.time_limit = time_limit
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")
        self.smoke_bomb_sound = pg.mixer.Sound("sfx_smoke_bomb.wav")


    def new(self):

        self.all_sprites = pg.sprite.Group()
        self.player_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.spike_ceilings = pg.sprite.Group()
        self.spike_floors = pg.sprite.Group()
        self.wooden_gates = pg.sprite.Group()
        self.ninjas = pg.sprite.Group()
        self.shurikens = pg.sprite.Group()
        self.enemy_smoke_bombs = pg.sprite.Group()
        self.drops = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.smoke_bombs = pg.sprite.Group()
        self.goal = pg.sprite.Group()

        self.background = Static(-200, 0, "Tower Metal BG.png")
        self.all_sprites.add(self.background)

        self.door = Static(36*82, self.game_height + 1000 - 85, "sprite_castledoors.png")
        self.all_sprites.add(self.door)
        self.goal.add(self.door)

        self.player = Player(self, 70, self.game_height - 400)
        self.player_sprites.add(self.player)
        self.all_sprites.add(self.player)

        platform_parameters = [[0, self.game_height - 900, 50],
                               [0, self.game_height - 400, 10],
                               [82*4, self.game_height - 100, 10],
                               [0, self.game_height + 200, 10],
                               [82*4, self.game_height + 500, 10],
                               [0, self.game_height + 800, 10],
                               [82*4, self.game_height + 1100, 10],
                               [0, self.game_height + 1400, 6],
                               [0, self.game_height + 1673, 50],
                               [82*15, self.game_height + 1320, 1],
                               [82*17, self.game_height + 1320, 1],
                               [82*19, self.game_height + 1320, 1],
                               [82*21, self.game_height + 1320, 1],
                               [82*23, self.game_height + 1320, 1],
                               [82*25, self.game_height + 1320, 10],
                               [82*14, self.game_height + 1673, 1],
                               [82*16, self.game_height + 1673, 1],
                               [82*18, self.game_height + 1673, 1],
                               [82*20, self.game_height + 1673, 1],
                               [82*22, self.game_height + 1673, 1],
                               [82*24, self.game_height + 1673, 1],
                               [82*16, self.game_height + 1000, 22],
                               ]

        vertical_platform_parameters = [[-25, self.game_height - 900, 40],
                                        [82*14, self.game_height - 900, 27],
                                        [82*38, self.game_height - 900, 40],
                                        ]

        spike_ceiling_parameters = [[82*14, self.game_height + 1320, 1],
                                    [82*16, self.game_height + 1320, 1],
                                    [82*18, self.game_height + 1320, 1],
                                    [82*20, self.game_height + 1320, 1],
                                    [82*22, self.game_height + 1320, 1],
                                    [82*24, self.game_height + 1320, 1],
                                    [82*14, self.game_height + 700, 24],]

        spike_floor_parameters = [[82*15, self.game_height + 1673 - 25, 1],
                                  [82*17, self.game_height + 1673 - 25, 1],
                                  [82*19, self.game_height + 1673 - 25, 1],
                                  [82*21, self.game_height + 1673 - 25, 1],
                                  [82*23, self.game_height + 1673 - 25, 1],
                                  [82*25, self.game_height + 1673 - 25, 1],]

        wooden_gate_parameters = []

        ninja_parameters = [[600, self.game_height - 400, self],
                            [700, self.game_height - 400, self, randrange(50,101)],
                            [randrange(300,500), self.game_height + 200, self],
                            [randrange(550,700), self.game_height + 200, self],
                            [randrange(300,500), self.game_height + 800, self],
                            [randrange(550,700), self.game_height + 800, self],
                            [randrange(50,200), self.game_height + 1400, self],
                            [randrange(250,400), self.game_height + 1400, self],
                            [randrange(600,800), self.game_height - 100, self],
                            [randrange(850,1000), self.game_height - 100, self],
                            [randrange(600,700), self.game_height + 500, self],
                            [randrange(850,1000), self.game_height + 500, self],
                            [randrange(600,700), self.game_height + 1100, self],
                            [randrange(850,1000), self.game_height + 1100, self],
                            [82 * 28, self.game_height + 1673, self],
                            [82 * 29, self.game_height + 1673, self],
                            [82 * 30, self.game_height + 1673, self],
                            [randrange(82*17, 82*32), self.game_height + 1000, self],
                            [randrange(82*17, 82*32), self.game_height + 1000, self],
                            [randrange(82*17, 82*32), self.game_height + 1000, self],
                            [randrange(82*17, 82*32), self.game_height + 1000, self],
                            [randrange(82*17, 82*32), self.game_height + 1000, self]]


        for p in platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Static(x + 82 * i, y, "tile_metal_platform.png")
                self.all_sprites.add(platform)
                self.platforms.add(platform)

        for p in vertical_platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Static(x, y + 82 * i, "tile_metal_platform_vertical.png")
                self.all_sprites.add(platform)
                self.platforms.add(platform)

        for p in spike_ceiling_parameters:
            x, y, r = p
            for i in range(r):
                spike_ceiling = Static(x + 82 * i, y, "sprite_spike_ceiling.png")
                self.all_sprites.add(spike_ceiling)
                self.platforms.add(spike_ceiling)
                self.spike_ceilings.add(spike_ceiling)

        for p in spike_floor_parameters:
            x, y, r = p
            for i in range(r):
                spike_floor = Static(x + 82 * i, y, "sprite_spike_floor.png")
                self.all_sprites.add(spike_floor)
                self.platforms.add(spike_floor)
                self.spike_floors.add(spike_floor)

        for p in wooden_gate_parameters:
            x, y, r = p
            for i in range(r):
                wooden_gate = Wooden_Gate(x + 82 * i, y)
                self.all_sprites.add(wooden_gate)
                self.platforms.add(wooden_gate)
                self.wooden_gates.add(wooden_gate)

        for p in ninja_parameters:
            ninja = Ninja(*p, weapons=['Shuriken', 'Shuriken', 'Smoke Bomb'])
            self.all_sprites.add(ninja)
            self.ninjas.add(ninja)

        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30+16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)

        self.screen_bottom = self.game_height + 2000
        self.screen_bottom_difference = self.game_height - self.player.last_height
        self.last_threw_smoke_bomb = 0
        self.time_remaining = self.time_limit*30

        if not self.retry:
            self.show_start_screen()
        self.run()


    def throw_smoke_bomb(self, x, y):

        self.last_threw_smoke_bomb = pg.time.get_ticks()
        smoke_bomb_x = x
        smoke_bomb_y = y
        x_dist = smoke_bomb_x - self.player.rect.centerx
        y_dist = smoke_bomb_y - self.player.rect.centery
        dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1

        smoke_bomb_move = [m for m in self.main_game.sml if _("Smoke Bomb") in m.name][0]
        smoke_bomb_level = smoke_bomb_move.level

        x_vel = (x_dist / dist) * randrange(10-(11-smoke_bomb_level)//2, 11) * (dist**.12)
        y_vel = (y_dist / dist) * randrange(10-(11-smoke_bomb_level)//2, 11) * (dist**.12)
        smoke_bomb = SmokeBomb(self.player.rect.centerx, self.player.rect.centery, x_vel, y_vel)
        self.all_sprites.add(smoke_bomb)
        self.smoke_bombs.add(smoke_bomb)


    def generate_projectile(self, x, y, speed, weapons):

        self.projectile_sound.play()
        weapon_choice = choice(weapons)
        if weapon_choice == 'Shuriken':
            for i in range(randrange(2,4)):
                self.projectile_sound.play()
                shuriken_x = x
                shuriken_y = y
                x_dist = self.player.rect.centerx - shuriken_x
                y_dist = (self.player.rect.centery - shuriken_y)
                if x_dist != 0:
                    y_dist -= abs(x_dist * (random())) * (1 + abs(y_dist / x_dist))
                dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1

                x_vel = x_dist / dist * randrange(12, 15)
                y_vel = y_dist / dist * randrange(10, 15)
                shuriken = Shuriken(x, y, x_vel, y_vel, 200, 0)
                self.shurikens.add(shuriken)
                self.all_sprites.add(shuriken)

        elif weapon_choice == 'Smoke Bomb':
            for i in range(randrange(1,3)):
                smoke_bomb_x = x
                smoke_bomb_y = y
                x_dist = self.player.rect.centerx - smoke_bomb_x
                y_dist = self.player.rect.centery - smoke_bomb_y
                if x_dist != 0:
                    y_dist -= abs(x_dist*(random()))*(1+abs(y_dist/x_dist))
                dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1

                x_vel = x_dist / dist * randrange(10, 15)
                y_vel = y_dist / dist * randrange(10, 15)
                smoke_bomb = SmokeBomb(smoke_bomb_x, smoke_bomb_y, x_vel, y_vel)
                self.enemy_smoke_bombs.add(smoke_bomb)
                self.all_sprites.add(smoke_bomb)


    def generate_drop(self, x, y, name=None, filename=None):
        if not name:
            if randrange(4) == 0:
                drop = Drops(x, y, "Health Potion", "sprite_health_potion.png")
            else:
                drop = Drops(x, y, "Gold", "sprite_gold.png")
        else:
            drop = Drops(x, y, name, filename)
        self.drops.add(drop)
        self.all_sprites.add(drop)


    def pick_up_drop(self, drop):
        if drop.name == "Gold":
            self.main_game.currentEffect = "button-19.mp3"
            self.main_game.startSoundEffectThread()
            r = int(randrange(500, 701)*self.main_game.luck/50)
            self.main_game.inv[_("Gold")] += r
            print(_("Picked up Gold x {}!").format(r))
        elif drop.name == "Health Potion":
            self.main_game.restore(h=self.main_game.healthMax*30//100,s=0,full=False)
        elif drop.name == "Chest":
            self.running = False
            self.scene.picked_up_dungeon_chest()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()



    def update(self):

        self.all_sprites.update()
        if self.playing and self.running:
            self.time_remaining -= 1
        #PLATFORM COLLISION
        collision = pg.sprite.spritecollide(self.player, self.platforms, False)
        if collision:
            col = collision[0]
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - col.rect.top <= self.player.player_height // 2) \
                    or (self.player.vel.y < 0 and self.player.rect.bottom - col.rect.top <= self.player.player_height // 2):
                self.player.pos.y = col.rect.top
                self.player.vel.y = 0
                current_height = col.rect.top
                fall_height = current_height - self.player.last_height
                self.player.last_height = current_height
                if fall_height >= 450:
                    damage = int(200 * 1.005 ** (fall_height - 450))
                    print("Ouch! Lost {} health from fall.".format(damage))
                    self.main_game.health -= damage
                    if self.main_game.health <= 0:
                        self.main_game.health = 0
                        self.playing = False
                        self.running = False
                        self.show_game_over_screen()

                self.player.last_height = current_height
                self.player.jumping = False

            elif self.player.vel.y < 0:
                self.player.pos.y = col.rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

        for platform in self.platforms:
            if platform in self.spike_floors or platform in self.spike_ceilings:
                pass
            elif self.player.vel.x > 0 and platform.rect.left - self.player.pos.x <= self.player.player_width * 1.05 and \
                    self.player.pos.x <= platform.rect.left and \
                    (abs(platform.rect.top - self.player.rect.centery) <= platform.image.get_height() / 2 or \
                     abs(platform.rect.bottom - self.player.rect.centery) <= platform.image.get_height() / 2):
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= platform.rect.right + self.player.player_width * 1.05 and \
                    self.player.pos.x >= platform.rect.right and \
                    (abs(platform.rect.top - self.player.rect.centery) <= platform.image.get_height() / 2 or \
                     abs(platform.rect.bottom - self.player.rect.centery) <= platform.image.get_height() / 2):
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x


        #shuriken player COLLISION
        shuriken_collision = pg.sprite.spritecollide(self.player, self.shurikens, True, pg.sprite.collide_circle_ratio(.85))
        if shuriken_collision:
            self.take_damage(shuriken_collision[0].damage)
            self.player.stun_count += shuriken_collision[0].stun

        # SPIKE CEILING COLLISION
        spike_ceiling_collision = pg.sprite.spritecollide(self.player, self.spike_ceilings, False)
        if spike_ceiling_collision:
            if self.player.rect.bottom >= spike_ceiling_collision[0].rect.bottom:
                self.take_damage(100)
                self.player.stun_count += randrange(8, 16)

        # SPIKE FLOOR COLLISION
        spike_floor_collision = pg.sprite.spritecollide(self.player, self.spike_floors, False)
        if spike_floor_collision:
            if self.player.rect.bottom <= spike_floor_collision[0].rect.bottom:
                self.take_damage(300)
                self.player.vel.y -= randrange(5,11)
                if self.player.player_acceleration >= 0:
                    self.player.pos.x -= self.player.vel.x
                else:
                    self.player.pos.x += self.player.vel.x

        # SHURIKEN - PLATFORM COLLISION
        for shuriken in self.shurikens:
            if not shuriken.dead:
                projectile_platform_collision = pg.sprite.spritecollide(shuriken, self.platforms, False)
                if projectile_platform_collision:
                    shuriken.x_vel = 0
                    shuriken.y_vel = 0
                    shuriken.dead = True
            elif random() <= .003:
                shuriken.kill()
            elif shuriken.dead and not pg.sprite.spritecollide(shuriken, self.platforms, False):
                shuriken.kill()


        # SMOKE BOMB COLLISION
        for smoke_bomb in self.smoke_bombs:
            smoke_bomb_collision = pg.sprite.spritecollide(smoke_bomb, self.platforms, False, pg.sprite.collide_rect)
            if smoke_bomb_collision and not smoke_bomb.exploded:
                smoke_bomb.explode()
                self.smoke_bomb_sound.play()
                if smoke_bomb_collision[0] in self.wooden_gates:
                    smoke_bomb_collision[0].isHit = True
            if smoke_bomb.exploded:
                pg.sprite.spritecollide(smoke_bomb, self.ninjas, True, pg.sprite.collide_circle_ratio(1))

        # ENEMY SMOKE BOMB COLLISION
        for smoke_bomb in self.enemy_smoke_bombs:
            smoke_bomb_collision = pg.sprite.spritecollide(smoke_bomb, self.platforms, False, pg.sprite.collide_rect)
            if smoke_bomb_collision and not smoke_bomb.exploded:
                smoke_bomb.explode()
                self.smoke_bomb_sound.play()
                if smoke_bomb_collision[0] in self.wooden_gates:
                    smoke_bomb_collision[0].isHit = True

            if smoke_bomb.exploded:
                if pg.sprite.spritecollide(smoke_bomb, self.player_sprites, False, pg.sprite.collide_circle_ratio(1.5)) and not smoke_bomb.dead:
                    self.take_damage(smoke_bomb.damage)
                    self.player.stun_count += smoke_bomb.stun
                    smoke_bomb.dead = True


        #drop COLLISION
        drop_collision = pg.sprite.spritecollide(self.player, self.drops, True, pg.sprite.collide_circle_ratio(.75))
        if drop_collision:
            drop = drop_collision[0]
            self.pick_up_drop(drop)


        #ninja COLLISION
        ninja_collision = pg.sprite.spritecollide(self.player, self.ninjas, True, pg.sprite.collide_circle_ratio(.75))
        if ninja_collision:
            self.running=False

            opp = character(_("Shadow Clones"), 10,
                            sml=[special_move(_("Kendo"), level=7),
                                 special_move(_("Superior Smoke Bomb"), level=7),
                                 special_move(_("Spider Poison Powder"), level=10),],
                            skills=[skill(_("Bushido"), level=5),
                                    skill(_("Kenjutsu I"), level=5),
                                    skill(_("Ninjutsu I"), level=5),],
                            preferences=[2, 1, 3, 1, 2],
                            multiple=3)
            self.main_game.battleID = "hattori_ninjas"

            drop_x = ninja_collision[0].rect.x
            drop_y = ninja_collision[0].rect.centery
            if self.player.rect.x > ninja_collision[0].rect.x:
                drop_x -= 15
            else:
                drop_x += 15

            cmd = lambda: self.main_game.resume_mini_game("jy_suspenseful1.mp3", [drop_x, drop_y])
            self.main_game.battleMenu(self.main_game.you, opp, "battleground_dark_forest.png",
                            postBattleSoundtrack="jy_suspenseful1.mp3",
                            fromWin=None,
                            battleType="normal", destinationWinList=[] * 3,
                            postBattleCmd=cmd
                            )

        # GOAL COLLISION
        goal_collision = pg.sprite.spritecollide(self.player, self.goal, False)
        if goal_collision and self.playing:
            self.playing = False
            self.running = False
            self.main_game.mini_game_in_progress = False
            pg.display.quit()
            self.scene.post_ninjutsu_training(3)


        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(int(self.player.vel.y))
            self.player.last_height += abs(int(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(int(self.player.vel.y))
            for ninja in self.ninjas:
                ninja.rect.y += abs(int(self.player.vel.y))
            for shuriken in self.shurikens:
                shuriken.rect.y += abs(int(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y += abs(int(self.player.vel.y))
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.y += abs(int(self.player.vel.y))
            for smoke_bomb in self.enemy_smoke_bombs:
                smoke_bomb.rect.y += abs(int(self.player.vel.y))
            for item in self.goal:
                item.rect.y += abs(int(self.player.vel.y))

            self.screen_bottom += abs(int(self.player.vel.y))
            #self.background.rect.y += abs(int(self.player.vel.y)/4)


        elif self.player.rect.bottom >= self.game_height//2 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(int(self.player.vel.y))
            self.player.pos.y -= abs(int(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(int(self.player.vel.y))
            for ninja in self.ninjas:
                ninja.rect.y -= abs(int(self.player.vel.y))
            for shuriken in self.shurikens:
                shuriken.rect.y -= abs(int(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y -= abs(int(self.player.vel.y))
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.y -= abs(int(self.player.vel.y))
            for smoke_bomb in self.enemy_smoke_bombs:
                smoke_bomb.rect.y -= abs(int(self.player.vel.y))
            for item in self.goal:
                item.rect.y -= abs(int(self.player.vel.y))

            self.screen_bottom -= abs(int(self.player.vel.y))
            #self.background.rect.y -= abs(int(self.player.vel.y) / 4)


        if self.player.rect.right >= self.game_width // 2 and self.player.vel.x > 0.1 and self.player.absolute_pos < 82*60:
            self.player.pos.x -= abs(int(self.player.vel.x))
            for platform in self.platforms:
                platform.rect.x -= abs(int(self.player.vel.x))
            for ninja in self.ninjas:
                ninja.rect.x -= abs(int(self.player.vel.x))
            for shuriken in self.shurikens:
                shuriken.rect.x -= abs(int(self.player.vel.x))
            for drop in self.drops:
                drop.rect.x -= abs(int(self.player.vel.x))
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.x -= abs(int(round(self.player.vel.x)))
            for smoke_bomb in self.enemy_smoke_bombs:
                smoke_bomb.rect.x -= abs(int(round(self.player.vel.x)))
            for item in self.goal:
                item.rect.x -= abs(int(self.player.vel.x))

            #self.background.rect.x -= abs(int(self.player.vel.x / 4))

        elif self.player.rect.right <= self.game_width * 3 // 7 and self.player.vel.x < -0.1 and self.player.absolute_pos > self.game_width//2:
            self.player.pos.x += abs(int(self.player.vel.x))
            for platform in self.platforms:
                platform.rect.x += abs(int(self.player.vel.x))
            for ninja in self.ninjas:
                ninja.rect.x += abs(int(self.player.vel.x))
            for shuriken in self.shurikens:
                shuriken.rect.x += abs(int(self.player.vel.x))
            for drop in self.drops:
                drop.rect.x += abs(int(self.player.vel.x))
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.x += abs(int(round(self.player.vel.x)))
            for smoke_bomb in self.enemy_smoke_bombs:
                smoke_bomb.rect.x += abs(int(round(self.player.vel.x)))
            for item in self.goal:
                item.rect.x += abs(int(self.player.vel.x))

            #self.background.rect.x += abs(int(self.player.vel.x / 4))


        #SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom*1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()

        #OUT OF TIME
        if self.time_remaining <= 0:
            self.playing = False
            self.running = False
            self.show_game_over_screen(timeout=True)


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()

            pos = pg.mouse.get_pos()
            if event.type == pg.MOUSEBUTTONUP and pg.time.get_ticks() - self.last_threw_smoke_bomb >= 1000 and self.player.stun_count==0:
                if self.main_game.check_for_item(_("Smoke Bomb")) and (_("Smoke Bomb") in [m.name for m in self.main_game.sml] or _("Superior Smoke Bomb") in [m.name for m in self.main_game.sml]):
                    self.throw_smoke_bomb(pos[0], pos[1])


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            self.draw_text("Time Remaining: {}".format(self.time_remaining//30), 16, RED, 1100, 30)
            pg.display.flip()


    def take_damage(self, damage):
        self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()

    def show_start_screen(self):
        self.draw_text("Reach the end of the dungeon in {} seconds.".format(self.time_limit), 20, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press any key to begin.", 20, WHITE, self.game_width // 2, self.game_height//2)
        pg.display.flip()
        self.listen_for_key()

    def show_game_over_screen(self, timeout=False):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        if timeout:
            self.draw_text("You ran out of time!", 22, WHITE, self.game_width // 2, self.game_height // 3)
        else:
            self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to exit training.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.running = False
                            self.playing = False
                            self.scene.exit_training()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)