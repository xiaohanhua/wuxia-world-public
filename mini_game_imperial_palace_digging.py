from mini_game_sprites import *
from item import *
from gettext import gettext

_ = gettext
MAP_STRING = """
-------------------------RRRRR----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR-------------RRRRRRRRRRRRRRRRRRRRRRRRRRRR
-------------------------RRRRR----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR-------------RRRRRRRRRRRRRRRRRRRRRRRRRRRR
-------------------------RRRRR----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR-------------RRRRRRRRRRRRRRRRRRRRRRRRRRRR
-------------------------RRRRR----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR-------------RRRRRRRRRRRRRRRRRRRRRRRRRRRR
-------------------------RRRRR----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR-------------RRRRRRRRRRRRRRRRRRRRRRRRRRRR
-------------------------RRRRR--------S----S---S-----------S---S-----S---S-----S-S----S--S----S--S--------SS--S-S--S--SS--S---S------SS------S--S-----S-S--------S---S-S--S-----S-------S-S--SS--S---SS--S--S---S--S---SS----S-S-----S---S----S----S-------S-S---S--S----S-----S-S----S-S----S-S-----S-S--S--S---S-S--------RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR.............RRRRRRRRRRRRRRRRRRRRRRRRRRRR
-------------------------RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR.............RRRRRRRRRRRRRRRRRRRRRRRRRRRR
-------------------------RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR.............RRRRRRRRRRRRRRRRRRRRRRRRRRRR
-------------------------RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR.............RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................RRR...........................................RRR.........................................................................................................................................................................RRRR..........................RRRRRRR........................................................................RRRRRRR.............RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................RRR...........................................RRR.........................................................................................................................................................................RRRR..........................RRRRRRR........................................................................RRRRRRR.............RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................RRR...........................................RRR.......................................................................................................................................................................................................RRRRRRR........................................................................RRRRRRR.............RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................RRR...........................................RRR.......................................................................................................................................................................................................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................RRR...........................................RRR.......................................................................................................................................................................................................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................RRR...........................................RRR..............................RRRRRRRRRRRRRRRRRRRRRRRR...RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR...............................RRRR..........................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.......................................................................RRR..............................RRRRRRRRRRRRRRRRRRRRRRRR...RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR...............................RRRR..........................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.......................................................................RRR.........................................................RRR.........................................................................RRRRR...............................RRRR..........................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.......................................................................RRR.........................................................RRR.........................................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR..........................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................RRRR..........................................RRR...............RRR.......................................RRR.........................................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR..........................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................RRRR..........................................RRR...............RRR.......................................RRR...........................................................................................................................................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................RRRR..........................................RRR...............RRR.......................................RRR................................RRRRRRR....................................................................................................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................RRRR..........................................RRR...............RRR.......................................RRR................................RRRRRRR....................................................................................................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.......................................................................RRR...............RRR.......................................RRR................................RRRRRRR....................................................................................................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.......................................................................RRR...............RRR..........................................................................RRRRRRR....................................................................................................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................................................................................RRR..........................................................................RRRRRRR....................................................................................................RRRRRRR...................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................................................................................RRR..........................................................................RRRRRRR..............................................................................................................................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................................................................................RRR..........................................................................RRRRRRR..............................................................................................................................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
.........................................................................................RRR..........................................................................RRRRRRR..............................................................................................................................................RRRRRRR..................................................RRRRRRRRRRRRRRRRRRRRRRRRRRRR
RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
"""


class imperial_palace_digging_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, 25*23
        self.screen = pg.display.set_mode((self.game_width, self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")
        self.digging_sound = pg.mixer.Sound("digging_sound.wav")


    def new(self):

        self.show_loading_screen()

        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.soft_tiles = pg.sprite.Group()
        self.falling_platforms = pg.sprite.Group()
        self.projectiles = pg.sprite.Group()
        self.drops = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.spearmen = pg.sprite.Group()

        self.background = Static(-50, -800+3*23, "imperial_palace_night_long_bg.png")
        self.all_sprites.add(self.background)

        self.arrow_up = Static(-3000, -3000, "arrow_up_1.png",
                               animations=[pg.image.load("arrow_up_{}.png".format(i)) for i in [2,3,2,1]])
        self.all_sprites.add(self.arrow_up)

        self.player = Player(self, 60, self.game_height - 23*22)
        self.all_sprites.add(self.player)

        map_string_split = MAP_STRING.strip().split("\n")
        map_string_split.reverse()
        base_y = self.game_height + 23
        for i in range(len(map_string_split)):
            row = map_string_split[i]
            for j in range(len(row)):
                char = row[j]

                if char == "R":
                    tile = Tile_Rock(j * 23, base_y - 23 * i)
                    self.all_sprites.add(tile)
                    self.platforms.add(tile)
                elif char == ".":
                    tile = Tile_Earth(j * 23, base_y - 23 * i)
                    self.all_sprites.add(tile)
                    self.platforms.add(tile)
                    self.soft_tiles.add(tile)
                elif char == "S":
                    spearman = Static(j*23, base_y-23*i-18, "Sprite_Spearman_1.png")
                    self.all_sprites.add(spearman)
                    self.spearmen.add(spearman)


        self.shovel_animated_down = Static(-3000, -3000, "sprite_shovel.png",
                                      animations=[pg.image.load("sprite_shovel_animated_{}.png".format(i)) for i in
                                                  range(1, 4)])
        self.shovel_animated_right = Static(-3000, -3000, "sprite_shovel.png",
                                      animations=[pg.image.load("sprite_shovel_animated_{}.png".format(i)) for i in
                                                  range(1, 4)], rotation=90)
        self.shovel_animated_up = Static(-3000, -3000, "sprite_shovel.png",
                                      animations=[pg.image.load("sprite_shovel_animated_{}.png".format(i)) for i in
                                                  range(1, 4)], rotation=180)
        self.shovel_animated_left = Static(-3000, -3000, "sprite_shovel.png",
                                      animations=[pg.image.load("sprite_shovel_animated_{}.png".format(i)) for i in
                                                  range(1, 4)], rotation=270)
        self.all_sprites.add(self.shovel_animated_left)
        self.all_sprites.add(self.shovel_animated_up)
        self.all_sprites.add(self.shovel_animated_right)
        self.all_sprites.add(self.shovel_animated_down)


        #self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        #self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        #self.mp_bar = Dynamic(30, 30 + 16, "bar_mp.png", "Stamina", self)
        #self.all_sprites.add(self.hp_mp_frame)
        #self.all_sprites.add(self.hp_bar)
        #self.all_sprites.add(self.mp_bar)

        self.last_action = 0
        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        pg.display.flip()
        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def update(self):

        self.all_sprites.update()

        on_screen_platforms = [platform for platform in self.platforms if platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)

        # SAND COLLISION
        sand_collision = pg.sprite.spritecollide(self.player, self.sand_sprites, False)

        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                for col in collision:
                    if col.rect.top <= self.player.rect.top and self.player.rect.top - col.rect.bottom <= col.image.get_height():
                        self.player.vel.y = 0
                        self.player.pos.y = col.rect.bottom + int(self.player.player_height*1.1)

            #if self.player.vel.y == 0:
            #    for col in collision:
            #        if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
            #            self.player.pos.y = col.rect.top
            #            self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    abs(self.player.rect.centery - col.rect.top) <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 4:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    abs(self.player.rect.centery - col.rect.top) <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 4:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x

        if sand_collision:
            self.player.vel.y *= .5
            current_height = sand_collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 600:
                damage = int(200 * 1.005 ** (fall_height - 600))
                print("Ouch! Lost {} health from fall.".format(damage))
                self.take_damage(damage)

            self.player.last_height = current_height
            self.player.jumping = False
            if self.main_game.stamina > 3:
                self.main_game.stamina -= 3
            else:
                self.take_damage(5, silent=True)


        else:
            if self.main_game.stamina <= self.main_game.staminaMax:
                self.main_game.stamina += 1

        # GOAL COLLISION
        if self.player.pos.y <= 0 and self.player.absolute_pos >= 365*23 and self.playing:
            self.playing = False
            self.running = False
            self.main_game.mini_game_in_progress = False
            pg.display.quit()
            self.scene.post_imperial_palace_digging()

        elif self.player.pos.y <= 0 and self.player.absolute_pos < 340*23:
            self.player.jump_cut()


        if self.player.absolute_pos >= 23*360:
            self.arrow_up.rect.centerx = self.game_width*4//5
            self.arrow_up.rect.centery = 200

        # WINDOW SCROLLING
        if self.player.rect.right >= self.game_width//2 and self.player.vel.x > 0.5:
            self.player.pos.x -= abs(self.player.vel.x)
            for platform in self.platforms:
                platform.rect.x -= int(abs(self.player.vel.x))
            for projectile in self.projectiles:
                projectile.rect.x -= int(abs(self.player.vel.x))
            for drop in self.drops:
                drop.rect.x -= int(abs(self.player.vel.x))
            for sand in self.sand_sprites:
                sand.rect.x -= int(abs(self.player.vel.x))
            for spearman in self.spearmen:
                spearman.rect.x -= int(abs(self.player.vel.x))


        elif self.player.rect.left < self.game_width//2 and self.player.vel.x < -0.5 and self.player.absolute_pos >= self.game_width//2:
            self.player.pos.x += abs(self.player.vel.x)
            for platform in self.platforms:
                platform.rect.x += int(abs(self.player.vel.x))
            for projectile in self.projectiles:
                projectile.rect.x += int(abs(self.player.vel.x))
            for drop in self.drops:
                drop.rect.x += int(abs(self.player.vel.x))
            for sand in self.sand_sprites:
                sand.rect.x += int(abs(self.player.vel.x))
            for spearman in self.spearmen:
                spearman.rect.x += int(abs(self.player.vel.x))

        # SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom * 1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()


        now = pg.time.get_ticks()
        if now - self.last_action >= 150:
            self.shovel_animated_down.rect.x = -3000
            self.shovel_animated_down.rect.y = -3000
            self.shovel_animated_up.rect.x = -3000
            self.shovel_animated_up.rect.y = -3000
            self.shovel_animated_left.rect.x = -3000
            self.shovel_animated_left.rect.y = -3000
            self.shovel_animated_right.rect.x = -3000
            self.shovel_animated_right.rect.y = -3000


    def events(self):
        now = pg.time.get_ticks()
        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if now - self.last_action >= 150:
                    self.last_action = now
                    if event.key == pg.K_a:
                        self.dig("left")
                    elif event.key == pg.K_d:
                        self.dig("right")
                    elif event.key == pg.K_w:
                        self.dig("up")
                    elif event.key == pg.K_s:
                        self.dig("down")

                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()
                else:
                    keys = pg.key.get_pressed()
                    if keys[pg.K_SPACE] and keys[pg.K_r]:
                        self.new()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def dig(self, direction):
        self.digging_sound.play()
        on_screen_platforms = [platform for platform in self.soft_tiles if
                               platform.rect.left >= -100 and platform.rect.right <= self.game_width + 100]
        if direction == "left":
            self.shovel_animated_left.rect.centerx = self.player.rect.left
            self.shovel_animated_left.rect.centery = self.player.rect.centery
            shovel_collision_tiles = pg.sprite.spritecollide(self.shovel_animated_left, on_screen_platforms,
                                                             False, pg.sprite.collide_circle_ratio(1.1))
            for tile in shovel_collision_tiles:
                if tile.rect.centerx <= self.shovel_animated_left.rect.centerx and random() <= .8:
                    self.all_sprites.remove(tile)
                    tile.kill()

        elif direction == "right":
            self.shovel_animated_right.rect.centerx = self.player.rect.right
            self.shovel_animated_right.rect.centery = self.player.rect.centery
            shovel_collision_tiles = pg.sprite.spritecollide(self.shovel_animated_right, on_screen_platforms,
                                                             False, pg.sprite.collide_circle_ratio(1.1))
            for tile in shovel_collision_tiles:
                if tile.rect.centerx >= self.shovel_animated_right.rect.centerx and random() <= .8:
                    self.all_sprites.remove(tile)
                    tile.kill()

        elif direction == "up":
            self.shovel_animated_up.rect.centerx = self.player.rect.centerx
            self.shovel_animated_up.rect.centery = self.player.rect.top
            shovel_collision_tiles = pg.sprite.spritecollide(self.shovel_animated_up, on_screen_platforms,
                                                             False, pg.sprite.collide_circle_ratio(1.1))
            for tile in shovel_collision_tiles:
                if tile.rect.centery <= self.shovel_animated_up.rect.centery and random() <= .8:
                    self.all_sprites.remove(tile)
                    tile.kill()

        elif direction == "down":
            self.shovel_animated_down.rect.centerx = self.player.rect.centerx
            self.shovel_animated_down.rect.centery = self.player.rect.centery
            shovel_collision_tiles = pg.sprite.spritecollide(self.shovel_animated_down, on_screen_platforms,
                                                             False, pg.sprite.collide_circle_ratio(1.1))
            for tile in shovel_collision_tiles:
                if tile.rect.centery >= self.shovel_animated_down.rect.centery and random() <= .8:
                    self.all_sprites.remove(tile)
                    tile.kill()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def take_damage(self, damage, silent=False):
        if not silent:
            self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def reset_falling_platforms(self, value=0):
        for fp in self.falling_platforms:
            fp.total_standing_time = value


    def show_loading_screen(self):
        self.draw_text("Loading - please wait...", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()


    def show_start_screen(self):
        self.screen.fill(BLACK)
        self.draw_text("Use the arrow keys to move/jump. Press WSAD to dig up, down, left, and right.", 19, WHITE, self.game_width // 2, self.game_height // 3)
        self.draw_text("Press <Spacebar> AND 'r' at the same time to reset the level at any time.", 19, WHITE, self.game_width // 2, self.game_height//2)
        self.draw_text("Press <Spacebar> to begin.", 19, WHITE, self.game_width // 2, self.game_height*2//3)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0, 0, 0, 200))
        self.screen.blit(self.dim_screen, (0, 0))
        self.draw_text("You died!", 22, WHITE, self.game_width // 2, self.game_height // 3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2,
                       self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True):  # if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start and event.key == pg.K_SPACE:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)