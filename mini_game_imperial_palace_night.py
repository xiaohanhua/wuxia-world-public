from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class imperial_palace_night_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")
        self.smoke_bomb_sound = pg.mixer.Sound("sfx_smoke_bomb.wav")


    def new(self):

        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.archers = pg.sprite.Group()
        self.spearmen = pg.sprite.Group()
        self.projectiles = pg.sprite.Group()
        self.drops = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.smoke_bombs = pg.sprite.Group()

        self.background = Static(-50, self.game_height-800, "imperial_palace_night_long_bg.png")
        self.all_sprites.add(self.background)

        self.player = Player(self, 60, self.game_height - 100)
        self.all_sprites.add(self.player)

        platform_parameters = [[-50, self.game_height - 40, self.game_width*7//23],
                               [-50, self.game_height - 17, self.game_width*7//23],
                               [-50, self.game_height + 6, self.game_width*7//23],
                               [-50, self.game_height + 29, self.game_width*7//23],
                               [-50, self.game_height + 52, self.game_width*7//23],
                               [-50, self.game_height + 75, self.game_width*7//23],
                               [550, self.game_height - 178, 6],
                               [550, self.game_height - 155, 6],
                               [550, self.game_height - 132, 6],
                               [550, self.game_height - 109, 6],
                               [550, self.game_height - 86, 6],
                               [550, self.game_height - 63, 6],
                               [2000, self.game_height - 155, 50],
                               #[2000, self.game_height - 132, 50],
                               #[2000, self.game_height - 109, 50],
                               [4000, self.game_height - 132, 3],
                               [4000, self.game_height - 109, 3],
                               [4000, self.game_height - 86, 3],
                               [4000, self.game_height - 63, 3],
                               [4400, self.game_height - 132, 3],
                               [4400, self.game_height - 109, 3],
                               [4400, self.game_height - 86, 3],
                               [4400, self.game_height - 63, 3],
                               ]




        archer_parameters = [[400, self.game_height - 40, self],
                            [590, self.game_height - 178, self, randrange(50,101)],
                            [650, self.game_height - 178, self, randrange(50, 101)],
                            [800, self.game_height - 40, self, randrange(50,101)],
                            [1360, self.game_height - 40, self, randrange(50,101)],
                            [1520, self.game_height - 40, self],
                            [1920, self.game_height - 40, self],
                            [2220, self.game_height - 155, self, randrange(50,101)],
                            [2290, self.game_height - 155, self, randrange(50, 101)],
                            [2570, self.game_height - 155, self, randrange(50, 101)],
                            [2720, self.game_height - 155, self, randrange(50, 101)],
                            [2850, self.game_height - 155, self],
                            [2910, self.game_height - 155, self],
                            [2120, self.game_height - 40, self],
                            [2520, self.game_height - 40, self],
                            [2700, self.game_height - 40, self],
                            [2900, self.game_height - 40, self],
                            [3300, self.game_height - 40, self, randrange(50,101)],
                            [3450, self.game_height - 40, self],
                            [3700, self.game_height - 40, self],
                            [3900, self.game_height - 40, self],
                            [4120, self.game_height - 40, self, randrange(50,101)],
                            [4320, self.game_height - 40, self, randrange(50,101)],
                            [4050, self.game_height - 132, self, randrange(50, 101)],
                            [4450, self.game_height - 132, self],
                            [5530, self.game_height - 40, self],
                            [5430, self.game_height - 40, self],
                            [4930, self.game_height - 40, self],
                            [5070, self.game_height - 40, self],
                            [5155, self.game_height - 40, self],
                            [5230, self.game_height - 40, self],
                            [5290, self.game_height - 40, self],
                            ]

        spearman_parameters = [[350, self.game_height - 40, self],
                               [620, self.game_height - 178, self],
                               [770, self.game_height - 40, self],
                               [1300, self.game_height - 40, self],
                               [1400, self.game_height - 40, self],
                               [1920, self.game_height - 40, self],
                               [5480, self.game_height - 40, self],
                               [4880, self.game_height - 40, self],
                               [5110, self.game_height - 40, self],
                               [4450, self.game_height - 132, self],
                               [3600, self.game_height - 40, self],
                               [3800, self.game_height - 40, self],
                             ]

        for p in platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Rock(x + 23 * i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)

        for p in archer_parameters:
            archer = Archer(*p, weapons=['Arrow'])
            self.all_sprites.add(archer)
            self.archers.add(archer)

        for i in range(min([self.main_game.taskProgressDic["imperial_palace"],12])):
            p = spearman_parameters[i]
            spearman = Spearman(*p)
            self.all_sprites.add(spearman)
            self.spearmen.add(spearman)


        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30+16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)

        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        self.last_threw_smoke_bomb = 0

        if not self.retry:
            self.show_start_screen()
        self.run()


    def throw_smoke_bomb(self, x, y):

        self.last_threw_smoke_bomb = pg.time.get_ticks()
        smoke_bomb_x = x
        smoke_bomb_y = y
        x_dist = smoke_bomb_x - self.player.rect.centerx
        y_dist = smoke_bomb_y - self.player.rect.centery
        dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1

        smoke_bomb_move = [m for m in self.main_game.sml if _("Smoke Bomb") in m.name][0]
        smoke_bomb_level = smoke_bomb_move.level
        smoke_bomb_move.gain_exp(10)

        x_vel = x_dist / dist * randrange(10-(11-smoke_bomb_level)//2, 11) * (dist**.12)
        y_vel = y_dist / dist * randrange(10-(11-smoke_bomb_level)//2, 11) * (dist**.12)
        smoke_bomb = SmokeBomb(self.player.rect.centerx, self.player.rect.centery, x_vel, y_vel)
        self.all_sprites.add(smoke_bomb)
        self.smoke_bombs.add(smoke_bomb)


    def generate_projectile(self, x, y, speed, weapons):

        damage = min([self.main_game.taskProgressDic["imperial_palace"]*10 + 50, 150])
        self.projectile_sound.play()
        weapon_choice = choice(weapons)
        if weapon_choice == 'Arrow':
            projectile = Projectile(x, y, speed, 0, "Arrow", "Sprite_Arrow.png", damage, 0, BLACK)
        self.all_sprites.add(projectile)
        self.projectiles.add(projectile)


    def generate_drop(self, x, y):
        #print("Dropped item")
        if randrange(4) == 0:
            drop = Drops(x, y, "Health Potion", "sprite_health_potion.png")
        else:
            drop = Drops(x, y, "Gold", "sprite_gold.png")
        self.drops.add(drop)
        self.all_sprites.add(drop)

    def pick_up_drop(self, drop):
        if drop.name == "Gold":
            self.main_game.currentEffect = "button-19.mp3"
            self.main_game.startSoundEffectThread()
            r = int(randrange(50, 101) * self.main_game.luck / 50)
            self.main_game.inv[_("Gold")] += r
            print(_("Picked up Gold x {}!").format(r))
        elif drop.name == "Health Potion":
            self.main_game.restore(h=self.main_game.healthMax*30//100, s=0, full=False)


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()



    def update(self):

        self.all_sprites.update()

        #PLATFORM COLLISION
        on_screen_platforms = [platform for platform in self.platforms if platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)
        if collision:
            if (self.player.vel.y > self.player.player_height//2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200*1.005**(fall_height-450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width*1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height/2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x

        #PROJECTILE COLLISION
        projectile_collision = pg.sprite.spritecollide(self.player, self.projectiles, True, pg.sprite.collide_circle_ratio(.75))
        if projectile_collision:
            self.take_damage(projectile_collision[0].damage)
            self.player.stun_count += projectile_collision[0].stun


        #drop COLLISION
        drop_collision = pg.sprite.spritecollide(self.player, self.drops, True, pg.sprite.collide_circle_ratio(.75))
        if drop_collision:
            drop = drop_collision[0]
            self.pick_up_drop(drop)


        #archer COLLISION
        archer_collision = pg.sprite.spritecollide(self.player, self.archers, True, pg.sprite.collide_circle_ratio(.75))
        if archer_collision:
            self.running=False
            opp = character(_("Archers"), 10,
                            attributeList=[1300,1300,200,200,120,90,110,10,10],
                            sml=[special_move(_("Archery"), level=10)]
                            )

            drop_x = archer_collision[0].rect.x
            drop_y = archer_collision[0].rect.centery
            if self.player.rect.x > archer_collision[0].rect.x:
                drop_x -= 15
            else:
                drop_x += 15
            cmd = lambda: self.main_game.resume_mini_game("jy_daojiangu.mp3", (drop_x, drop_y))
            self.main_game.battleMenu(self.main_game.you, opp, "battleground_imperial_palace_night.png",
                            postBattleSoundtrack="jy_daojiangu.mp3",
                            fromWin=None,
                            battleType="task", destinationWinList=[] * 3,
                            postBattleCmd=cmd
                            )

        # spear COLLISION
        spearman_collision = pg.sprite.spritecollide(self.player, self.spearmen, True,
                                                   pg.sprite.collide_circle_ratio(.5))
        if spearman_collision:
            self.running = False
            opp = character(_("Spearmen"), 10,
                            attributeList=[1300, 1300, 200, 200, 100, 120, 70, 50, 10],
                            sml=[special_move(_("Spear Attack"), level=10)]
                            )

            drop_x = spearman_collision[0].rect.x
            drop_y = spearman_collision[0].rect.centery
            if self.player.rect.x > spearman_collision[0].rect.x:
                drop_x -= 15
            else:
                drop_x += 15
            cmd = lambda: self.main_game.resume_mini_game("jy_daojiangu.mp3", (drop_x, drop_y))
            self.main_game.battleMenu(self.main_game.you, opp, "battleground_imperial_palace_night.png",
                                      postBattleSoundtrack="jy_daojiangu.mp3",
                                      fromWin=None,
                                      battleType="task", destinationWinList=[] * 3,
                                      postBattleCmd=cmd)

        # SMOKE BOMB COLLISION

        for smoke_bomb in self.smoke_bombs:
            smoke_bomb_collision = pg.sprite.spritecollide(smoke_bomb, self.platforms, False,pg.sprite.collide_rect)
            if smoke_bomb_collision and not smoke_bomb.exploded:
                smoke_bomb.explode()
                self.smoke_bomb_sound.play()

            if smoke_bomb.exploded:
                pg.sprite.spritecollide(smoke_bomb, self.archers, True, pg.sprite.collide_circle_ratio(1.5))
                pg.sprite.spritecollide(smoke_bomb, self.spearmen, True, pg.sprite.collide_circle_ratio(1.5))

        #REACHED END
        if self.player.absolute_pos >= int(self.game_width*5.25):
            self.playing = False
            self.running = False
            self.main_game.mini_game_in_progress = False
            pg.display.quit()
            self.main_game.mapWin.deiconify()


        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 4:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
            for archer in self.archers:
                archer.rect.y += abs(round(self.player.vel.y))
            for spearman in self.spearmen:
                spearman.rect.y += abs(round(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y += abs(round(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y += abs(round(self.player.vel.y))
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.y += abs(round(self.player.vel.y))

            self.screen_bottom += abs(round(self.player.vel.y))
            self.background.rect.y += abs(round(self.player.vel.y)/4)


        elif self.player.rect.bottom >= self.game_height * 3 //4 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
            for archer in self.archers:
                archer.rect.y -= abs(round(self.player.vel.y))
            for spearman in self.spearmen:
                spearman.rect.y -= abs(round(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y -= abs(round(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y -= abs(round(self.player.vel.y))
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.y -= abs(round(self.player.vel.y))

            self.screen_bottom -= abs(round(self.player.vel.y))
            self.background.rect.y -= abs(round(self.player.vel.y) / 4)


        if self.player.pos.x >= self.game_width // 2 and self.player.vel.x > 0.5 and self.player.absolute_pos < int(self.game_width*5):
            self.player.pos.x -= abs(self.player.vel.x)
            for platform in self.platforms:
                platform.rect.x -= abs(self.player.vel.x)
            for archer in self.archers:
                archer.rect.x -= abs(self.player.vel.x)
            for spearman in self.spearmen:
                spearman.rect.x -= abs(self.player.vel.x)
            for projectile in self.projectiles:
                projectile.rect.x -= abs(self.player.vel.x)
            for drop in self.drops:
                drop.rect.x -= abs(self.player.vel.x)
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.x -= abs(round(self.player.vel.x))

            self.background.rect.x -= abs(self.player.vel.x // 6)


        #Remove projectile sprites that go off the screen
        for projectile in self.projectiles:
            if projectile.rect.x <= -150 or projectile.rect.x > self.game_width+150:
                projectile.kill()


        #SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom*1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()

            pos = pg.mouse.get_pos()
            if event.type == pg.MOUSEBUTTONUP and pg.time.get_ticks() - self.last_threw_smoke_bomb >= 1000 and self.player.stun_count==0:
                if self.main_game.check_for_item(_("Smoke Bomb")) and (_("Smoke Bomb") in [m.name for m in self.main_game.sml] or _("Superior Smoke Bomb") in [m.name for m in self.main_game.sml]):
                    self.main_game.add_item(_("Smoke Bomb"),-1)
                    self.throw_smoke_bomb(pos[0], pos[1])

    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def take_damage(self, damage):
        self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump. Escape from the guards.", 20, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("If you have Smoke Bombs and have learned the move, you can left click to throw a smoke bomb.", 20, WHITE, self.game_width // 2, self.game_height// 2)
        self.draw_text("Press any key to begin.", 20, WHITE, self.game_width // 2, self.game_height *2// 3)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)