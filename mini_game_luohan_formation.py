from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class luohan_formation_mini_game:
    def __init__(self, main_game, scene, mode="Normal"):
        self.main_game = main_game
        self.scene = scene
        self.mode = mode
        self.game_width, self.game_height = 1200, LANDSCAPE_HEIGHT
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina
        pg.font.init()
        pg.mixer.init()


        self.projectile_sound = pg.mixer.Sound("staff_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")


    def new(self):

        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.monks = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()

        self.background = Static(-200, self.game_height-1350, "shaolin_long_bg.png")
        self.all_sprites.add(self.background)

        self.player = Player(self, 100, self.game_height - 86)
        self.all_sprites.add(self.player)

        platform_parameters = [[0, self.game_height - 86, 2100//23],
                               [0, self.game_height - 63, 2100//23],
                               [0, self.game_height - 40, 2100//23],
                               [0, self.game_height - 17, 2100//23],
                               [0, self.game_height + 6, 2100//23],
                               [0, self.game_height + 29, 2100//23],
                               [0, self.game_height + 52, 2100//23],
                               [0, self.game_height + 75, 2100//23],
                               [1200-16*23, self.game_height - 250, 5],
                               [16*23, self.game_height - 250, 5],
                               ]

        monk_parameters = [[280, self.game_height-86],
                           [330, self.game_height-86],
                           [360, self.game_height-86],
                           [430, self.game_height-86],
                           [500, self.game_height-86],
                           [580, self.game_height-86],
                           [650, self.game_height-86],
                           [720, self.game_height-86],
                           [800, self.game_height-86],
                           [910, self.game_height-86],
                           [1010, self.game_height-86],
                           [1100, self.game_height-86],
                           [1130, self.game_height-86],
                           [1170, self.game_height-86],]

        stationary_monk_parameters = [[380, self.game_height-250],
                                      [440, self.game_height-250],
                                      [840, self.game_height-250],
                                      [900, self.game_height-250],]

        for p in platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Rock(x + 23 * i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)


        if self.mode == "Normal":
            self.monk_damage = 50 + 20*self.main_game.taskProgressDic["luohanzhen"]
        else:
            self.monk_damage = 50 + 20 * 20

        for p in monk_parameters:
            monk = Luohan_Monk(*p, self, self.monk_damage)
            self.monks.add(monk)
            self.all_sprites.add(monk)

        for p in stationary_monk_parameters:
            monk = Luohan_Monk(*p, self, self.monk_damage, stationary=True)
            self.monks.add(monk)
            self.all_sprites.add(monk)


        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30 + 16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)


        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()



    def update(self):

        self.all_sprites.update()

        #PLATFORM COLLISION
        on_screen_platforms = [platform for platform in self.platforms if platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)
        if collision:
            if (self.player.vel.y > self.player.player_height//2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200*1.005**(fall_height-450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width*1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height/2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x

        #REACHED END
        if self.player.absolute_pos >= int(self.game_width*7.25):
            self.playing = False
            self.running = False
            self.main_game.mini_game_in_progress = False
            pg.display.quit()
            self.main_game.scripture_library()


        # monk COLLISION
        monk_collision = pg.sprite.spritecollide(self.player, self.monks, True,
                                                   pg.sprite.collide_circle_ratio(.4))
        if monk_collision:
            self.running = False
            if self.mode == "Normal":
                monk_level = 12+self.main_game.taskProgressDic["luohanzhen"]
            else:
                monk_level = 20
            opp = character(_("Luohan Monk"), monk_level,
                            sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                                 special_move(_("Luohan Staff"), level=10),],
                            skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                              skill(_("Shaolin Inner Energy Technique"), level=10)]
                            )

            cmd = lambda: self.main_game.resume_mini_game(None)
            self.main_game.battleMenu(self.main_game.you, opp, "battleground_shaolin_night.png",
                                      postBattleSoundtrack=None,
                                      fromWin=None,
                                      battleType="luohanzhen", destinationWinList=[] * 3,
                                      postBattleCmd=cmd
            )



        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 4:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
            for monk in self.monks:
                monk.rect.bottom += abs(round(self.player.vel.y))

            self.screen_bottom += abs(round(self.player.vel.y))
            self.background.rect.y += abs(round(self.player.vel.y)/4)


        elif self.player.rect.bottom >= self.game_height * 2 // 3 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
            for monk in self.monks:
                monk.rect.bottom -= abs(round(self.player.vel.y))

            self.screen_bottom -= abs(round(self.player.vel.y))
            self.background.rect.y -= abs(round(self.player.vel.y) / 4)


        #Check if all monks have been defeated
        if len(self.monks) == 0:
            self.playing = False
            self.running = False
            if self.mode == "Normal":
                self.scene.luohanzhen_rewards()
            else:
                self.scene.post_yagyu_clan_battle(-3)



    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def take_damage(self, damage):
        self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def show_start_screen(self):
        self.draw_text("Use arrow keys to move. Defeat all 18 Luohan Monks.", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)


    def detected(self):
        self.playing = False
        self.running = False

