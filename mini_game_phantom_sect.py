from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class phantom_sect_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = PORTRAIT_WIDTH, PORTRAIT_HEIGHT
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        pg.font.init()
        pg.mixer.init()
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")
        self.last_generated_projectile_list = {"850,50":0, "-50,50":0, "400,-50":0}


    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.enemies = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.projectiles = pg.sprite.Group()


        self.background = Static(0, self.game_height-1300, "phantom_sect_minigame_bg.png")
        self.all_sprites.add(self.background)

        self.player = Player(self, self.game_width-100,self.game_height-100)
        self.all_sprites.add(self.player)

        self.lgp_sprite = Static(760, self.game_height - 4140, "sprite_li_guiping_standing.png")
        self.enemies.add(self.lgp_sprite)
        self.all_sprites.add(self.lgp_sprite)

        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30+16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)

        platform_parameters = [[0, self.game_height - 40, 40],
                               [280, self.game_height - 180, 5],
                               [550, self.game_height - 280, 3],
                               [0, self.game_height - 380, 3],
                               [60, self.game_height - 610, 3],
                               [370, self.game_height - 500, 4],
                               [400, self.game_height - 770, 4],
                               [600, self.game_height - 680, 4],
                               [100, self.game_height - 920, 7],
                               [500, self.game_height - 1120, 3],
                               [200, self.game_height - 1320, 4],
                               [40, self.game_height - 1480, 2],
                               [210, self.game_height - 1650, 3],
                               [490, self.game_height - 1860, 3],
                               [390, self.game_height - 2100, 3],
                               [290, self.game_height - 2350, 3],
                               [190, self.game_height - 2600, 3],
                               [90, self.game_height - 2850, 3],
                               [0, self.game_height - 3100, 3],
                               [90, self.game_height - 3350, 3],
                               [190, self.game_height - 3600, 3],
                               [290, self.game_height - 3850, 3],
                               [390, self.game_height - 4100, 32],
                               ]

        base_platform_parameters = [[0, self.game_height - 17, 40],
                                   [0, self.game_height + 6, 40],
                                   [0, self.game_height + 29, 40],
                                   [0, self.game_height + 52, 40],
                                   [0, self.game_height + 75, 40],
                                   [0, self.game_height + 98, 40],
                                   [0, self.game_height + 121, 40]]

        for p in platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Rock(x+23*i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)

        for p in base_platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Rock(x+23*i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)



        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            if self.running:
                try:
                    self.update()
                    self.clock.tick(FPS)
                    self.events()
                    self.draw()
                except:
                    pass


    def update(self):

        self.all_sprites.update()

        #PLATFORM COLLISION
        collision = pg.sprite.spritecollide(self.player, self.platforms, False)
        if collision:

            if (self.player.vel.y > self.player.player_height//2) or (self.player.vel.y > 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2) or (self.player.vel.y < 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
                current_height = collision[0].rect.top
                fall_height = current_height - self.player.last_height
                self.player.last_height = current_height
                if fall_height >= 450:
                    damage = int(200 * 1.005 ** (fall_height - 450))
                    print("Ouch! Lost {} health from fall.".format(damage))
                    self.main_game.health -= damage
                    if self.main_game.health <= 0:
                        self.main_game.health = 0
                        self.playing = False
                        self.running = False
                        self.show_game_over_screen()

            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            for col in [platform for platform in self.platforms if platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]:
                if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                        self.player.pos.x <= col.rect.left and \
                        self.player.pos.y - col.rect.top <= self.player.player_height and \
                        self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                    self.player.pos.x -= self.player.vel.x
                    self.player.absolute_pos -= self.player.vel.x
                elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                        self.player.pos.x >= col.rect.right and \
                        self.player.pos.y - col.rect.top <= self.player.player_height and \
                        self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                    self.player.pos.x -= self.player.vel.x
                    self.player.absolute_pos -= self.player.vel.x

            self.player.jumping = False
            self.player.friction = PLAYER_FRICTION

        else:
            self.player.friction = PLAYER_FRICTION

        # PROJECTILE COLLISION
        projectile_collision = pg.sprite.spritecollide(self.player, self.projectiles, True,
                                                       pg.sprite.collide_circle_ratio(.75))
        if projectile_collision:
            self.take_damage(projectile_collision[0].damage)
            self.player.stun_count += projectile_collision[0].stun

        #ENEMY COLLISION
        enemy_collision = pg.sprite.spritecollide(self.player, self.enemies, True, pg.sprite.collide_circle_ratio(.75))
        if enemy_collision:
            self.playing = False
            self.running = False
            self.scene.post_mini_game()

        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 4:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
            for enemy in self.enemies:
                enemy.rect.y += abs(round(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y += abs(round(self.player.vel.y))

            self.background.rect.y += abs(round(self.player.vel.y)/8)


        elif self.player.rect.bottom >= self.game_height * 3 //4 and self.player.vel.y > 0:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
            for enemy in self.enemies:
                enemy.rect.y -= abs(round(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y -= abs(round(self.player.vel.y))

            self.background.rect.y -= abs(round(self.player.vel.y) / 8)


        #Projectiles
        now = pg.time.get_ticks()
        for location, ticker in self.last_generated_projectile_list.items():
            if now - ticker >= randrange(700, 1200):
                self.last_generated_projectile_list[location] = now
                x,y = location.split(",")
                self.generate_projectile(int(x), int(y))


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump.", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Charge past the Phantom Sect disciples. Watch out for their projectiles.", 22, WHITE, self.game_width // 2, self.game_height // 2)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height *2// 3)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)


    def generate_projectile(self, x, y):
        self.projectile_sound.play()
        projectile_x = x
        projectile_y = y
        x_dist = self.player.rect.centerx - projectile_x
        y_dist = self.player.rect.centery - projectile_y
        dist = (x_dist**2 + y_dist**2)**.5
        if random() <= .8:
            x_vel = x_dist / dist * randrange(15, 20)
            y_vel = y_dist / dist * randrange(15, 20)
            proj = Projectile(projectile_x, projectile_y, x_vel, y_vel, "Poison Needle", "sprite_poison_needle.png", 50, 0)

        else:
            x_vel = x_dist / dist * randrange(10, 16)
            y_vel = y_dist / dist * randrange(10, 16)
            proj = Projectile(projectile_x, projectile_y, x_vel, y_vel, "Boulder", "sprite_boulder.png", 100, randrange(10,21))

        self.all_sprites.add(proj)
        self.projectiles.add(proj)


    def take_damage(self, damage):
        self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()