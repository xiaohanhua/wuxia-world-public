from mini_game_sprites import *
from character import *
from skill import *
from special_move import *
from item import *
from gettext import gettext
from helper_funcs import*

_ = gettext



class potters_field_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = 1200, 700
        self.screen = pg.display.set_mode((self.game_width, self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")
        self.thunder_sound = pg.mixer.Sound("Thunder.wav")
        self.ghost_sound = pg.mixer.Sound("ghost_amplified.wav")
        self.ghost_attack_sound = pg.mixer.Sound("ghost_attack_amplified.wav")
        self.ghost_scream_sound = pg.mixer.Sound("ghost_scream.wav")
        self.flame_ignition_sound = pg.mixer.Sound("flame_ignition.wav")
        self.wood_crack_sound = pg.mixer.Sound("wood_crack.wav")
        self.digging_sound = pg.mixer.Sound("digging_sound.wav")
        self.drop_sound = pg.mixer.Sound("button-19.wav")
        self.click_sound = pg.mixer.Sound("button-20.wav")
        self.door_lock_sound = pg.mixer.Sound("door_lock.wav")
        self.door_unlock_sound = pg.mixer.Sound("door_unlock.wav")


    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.falling_platforms = pg.sprite.Group()
        self.drops = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.trees = pg.sprite.Group()
        self.goal = pg.sprite.Group()

        self.background = Static(-2000, self.game_height - 1000, "Potters Field.png")
        self.all_sprites.add(self.background)

        self.cemetery_gate = Static(int(self.game_width*5.6), self.game_height-100-90, "sprite_cemetery_gates.png")
        self.goal.add(self.cemetery_gate)
        self.all_sprites.add(self.cemetery_gate)

        self.player = Player(self, self.game_width//2, self.game_height - 150)
        self.all_sprites.add(self.player)

        self.torch = Static(self.player.rect.right, self.player.rect.centery - self.player.player_height*.5,
                            filename = "sprite_torch_1.png",
                            animations = [pg.image.load("sprite_torch_{}.png".format(i)) for i in range(1,5)])
        self.all_sprites.add(self.torch)

        platform_parameters = [[-int(self.game_width*6), self.game_height - 100, int(self.game_width*12), self.game_height//2],
                               [1700, self.game_height - 450, 300, 20],
                               [2200, self.game_height - 550, 150, 20],
                               [-600, self.game_height - 350, 400, 20],
                               [-2500, self.game_height - 350, 350, 20],
                               [-5000, self.game_height - 500, 100, 20],
                               ]

        for pp in platform_parameters:
            platform = Platform_Earth(*pp)
            self.platforms.add(platform)
            self.all_sprites.add(platform)


        tree_parameters = [[1000, self.game_height-100-120, "sprite_dead_tree.png"],
                           [3000, self.game_height-100-120, "sprite_dead_tree.png"],
                           [-1200, self.game_height-100-120, "sprite_dead_tree.png"],
                           [-2800, self.game_height-100-120, "sprite_dead_tree.png"],
                           [-4000, self.game_height-100-120, "sprite_dead_tree.png"],
                           [-4450, self.game_height-100-120, "sprite_dead_tree.png"],
                           [-5200, self.game_height-100-120, "sprite_dead_tree.png"]]

        for tp in tree_parameters:
            tree = Static(*tp)
            self.trees.add(tree)
            self.all_sprites.add(tree)


        drop_parameters = [[-3500, self.game_height-100-30],
                           [-4200, self.game_height-100-30],]
        for dp in drop_parameters:
            drop = Static(*dp, "sprite_health_potion.png")
            self.drops.add(drop)
            self.all_sprites.add(drop)


        self.skeleton = Static(-1600, self.game_height - 100 - 30, "sprite_skeleton_sitting.png")
        self.all_sprites.add(self.skeleton)

        self.skeleton_drawing = Static(1800, self.game_height - 450 - 20, "sprite_parchment.png")
        self.all_sprites.add(self.skeleton_drawing)

        self.shovel_digging_drawing = Static(int(self.game_width*4.7), self.game_height-100-20, "sprite_parchment_white.png")
        self.all_sprites.add(self.shovel_digging_drawing)

        self.shovel = Static(int(self.game_width*4.75), self.game_height-100-20, "sprite_shovel.png")
        self.all_sprites.add(self.shovel)


        self.ghost = Static(-3000, -3000, "sprite_ren_zhichu_ghost.png")
        self.all_sprites.add(self.ghost)
        self.attacking_ghost = Ren_Zhichu_Ghost(-3000, -3000, self)
        self.all_sprites.add(self.attacking_ghost)


        self.visibility_bubble = Static(self.player.rect.centerx, self.player.rect.centery, "sprite_visibility_bubble.png")
        self.all_sprites.add(self.visibility_bubble)

        self.bloody_screen = Static(-3000, -3000, "sprite_bloody_screen.png")
        self.all_sprites.add(self.bloody_screen)


        # Define base platform
        self.base_platform = None
        for platform in self.platforms:
            if platform.image.get_width() >= self.game_width:
                self.base_platform = platform


        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30 + 16, "bar_mp.png", "Stamina", self)
        self.icon_starting_x = self.game_width*9//10
        self.tree_bark_icon = Static(- 3000, -3000, "sprite_tree_bark.png")
        self.skeleton_drawing_icon = Static(-3000, -3000, "sprite_parchment.png")
        self.shovel_icon = Static(-3000, -3000, "sprite_shovel.png")
        self.shovel_digging_drawing_icon = Static(-3000, -3000, "sprite_parchment_white.png")
        self.shovel_animated = Static(-3000, -3000, "sprite_shovel.png",
                                      animations=[pg.image.load("sprite_shovel_animated_{}.png".format(i)) for i in range(1,4)])
        self.rusty_key_icon = Static(-3000, -3000, "sprite_rusty_key.png")
        self.bronze_key_icon = Static(-3000, -3000, "sprite_bronze_key.png")
        self.silver_key_icon = Static(-3000, -3000, "sprite_silver_key.png")
        self.gold_key_icon = Static(-3000, -3000, "sprite_gold_key.png")
        self.health_potion_icon = Static(-3000, -3000, "sprite_health_potion.png")
        self.shovel_digging_drawing_full = Static(-3000, -3000, "shovel_digging_drawing.png")
        self.skeleton_drawing_full = Static(-3000, -3000, "skeleton_drawing.png")
        self.skeleton_sitting = Static(-3000, -3000, "skeleton_sitting.png")

        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)
        self.all_sprites.add(self.tree_bark_icon)
        self.all_sprites.add(self.skeleton_drawing_icon)
        self.all_sprites.add(self.shovel_icon)
        self.all_sprites.add(self.shovel_digging_drawing_icon)
        self.all_sprites.add(self.shovel_animated)
        self.all_sprites.add(self.rusty_key_icon)
        self.all_sprites.add(self.bronze_key_icon)
        self.all_sprites.add(self.silver_key_icon)
        self.all_sprites.add(self.gold_key_icon)
        self.all_sprites.add(self.health_potion_icon)
        self.all_sprites.add(self.skeleton_drawing_full)
        self.all_sprites.add(self.skeleton_sitting)
        self.all_sprites.add(self.shovel_digging_drawing_full)


        self.last_thunder = 0
        self.last_flash = 0
        self.thundering = False
        self.ghost_attacking = False
        self.ghost_aggressive = False
        self.last_ghost_attack = 0


        self.game_message = ""
        self.last_action = 0
        self.last_dig = 0
        self.viewing_item = None
        self.skeleton_sitting_buttons_pressed = [0, 0, 0, 0, 0]
        self.player_inventory = {}
        self.player_inventory_icons = {"tree_bark": self.tree_bark_icon,
                                       "skeleton_drawing": self.skeleton_drawing_icon,
                                       "rusty_key": self.rusty_key_icon,
                                       "bronze_key": self.bronze_key_icon,
                                       "silver_key": self.silver_key_icon,
                                       "gold_key": self.gold_key_icon,
                                       "shovel_digging_drawing": self.shovel_digging_drawing_icon,
                                       "shovel": self.shovel_icon,
                                       "health_potion": self.health_potion_icon,
                                       }

        self.tree_collision = None
        self.goal_collision = None
        self.skeleton_collision = None
        self.skeleton_drawing_collision = None
        self.shovel_collision = None
        self.shovel_digging_drawing_collision = None
        self.drop_collision = None

        self.torch_life = 100
        self.torch_status = "Normal"



        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        if not self.retry:
            self.show_start_screen()
        self.run()


    def check_torch_life(self):
        self.torch_life -= .03
        if self.torch_life >= 100:
            self.torch_life = 100
        elif self.torch_life <= 0:
            self.torch_life = 0

        if self.torch_life > 75:
            if self.torch_status != "Normal":
                self.torch_status = "Normal"
                self.visibility_bubble.image = pg.image.load("sprite_visibility_bubble.png")
        elif self.torch_life > 50:
            if self.torch_status != "Small":
                self.torch_status = "Small"
                self.visibility_bubble.image = pg.image.load("sprite_visibility_bubble_small.png")
        elif self.torch_life > 25:
            if self.torch_status != "Tiny":
                self.torch_status = "Tiny"
                self.visibility_bubble.image = pg.image.load("sprite_visibility_bubble_tiny.png")
        elif self.torch_life <= 25:
            if self.torch_status != "Minuscule":
                self.torch_status = "Minuscule"
                self.visibility_bubble.image = pg.image.load("sprite_visibility_bubble_minuscule.png")



    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def update(self):

        self.all_sprites.update()
        self.check_torch_life()
        now = pg.time.get_ticks()

        if now - self.last_action > 2500 or self.viewing_item in ["skeleton_drawing", "shovel_digging_drawing"]:
            self.game_message = ""
        if now >= 180000 and not self.ghost_aggressive:
            self.ghost_aggressive = True
        #on_screen_platforms = [platform for platform in self.platforms if platform.rect.centerx >= -100 and platform.rect.centerx <= self.game_width+100]
        on_screen_platforms = self.platforms
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)

        # SAND COLLISION
        sand_collision = pg.sprite.spritecollide(self.player, self.sand_sprites, False)

        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height * 3 // 5) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x

        if sand_collision:
            self.player.vel.y *= .5
            current_height = sand_collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 600:
                damage = int(200 * 1.005 ** (fall_height - 600))
                print("Ouch! Lost {} health from fall.".format(damage))
                self.take_damage(damage)

            self.player.last_height = current_height
            self.player.jumping = False
            if self.main_game.stamina > 3:
                self.main_game.stamina -= 3
            else:
                self.take_damage(5, silent=True)

        else:
            if self.main_game.stamina <= self.main_game.staminaMax:
                self.main_game.stamina += 0


        # GOAL COLLISION
        self.goal_collision = pg.sprite.spritecollide(self.player, self.goal, False)
        if self.goal_collision and self.playing and self.game_message == "":
            self.game_message = _("Press <Spacebar> to enter.")

        # TREE COLLISION
        self.tree_collision = pg.sprite.spritecollide(self.player, self.trees, False)
        if self.tree_collision and self.game_message == "":
            self.game_message = _("Press <Spacebar> to look for some wood for your torch.")

        # SKELETON COLLISION
        self.skeleton_collision = pg.sprite.collide_rect(self.player, self.skeleton)
        if self.skeleton_collision and self.game_message == "" and self.viewing_item == None:
            self.game_message = _("Press <Spacebar> to examine skeleton.")
        elif not self.skeleton_collision and self.viewing_item == "skeleton_sitting":
            self.viewing_item = None
            self.all_sprites.remove(self.skeleton_sitting)

        # SKELETON DRAWING COLLISION
        self.skeleton_drawing_collision = pg.sprite.collide_rect(self.player, self.skeleton_drawing)
        if self.skeleton_drawing_collision and self.game_message == "":
            self.game_message = _("Press <Spacebar> to pick up paper.")

        # SHOVEL DIGGING DRAWING COLLISION
        self.shovel_digging_drawing_collision = pg.sprite.collide_rect(self.player, self.shovel_digging_drawing)
        if self.shovel_digging_drawing_collision and self.game_message == "":
            self.game_message = _("Press <Spacebar> to pick up paper.")

        # SHOVEL COLLISION
        self.shovel_collision = pg.sprite.collide_rect(self.player, self.shovel)
        if self.shovel_collision and self.game_message == "":
            self.game_message = _("Press <Spacebar> to pick up shovel.")

        # DROP COLLISION
        self.drop_collision = pg.sprite.spritecollide(self.player, self.drops, False, pg.sprite.collide_circle_ratio(.75))
        if self.drop_collision and self.game_message == "":
            self.game_message = _("Press <Spacebar> to pick up health potion.")


        # WINDOW SCROLLING

        if self.player.rect.right > self.game_width*8//15 and self.player.vel.x > 0.1 and self.player.absolute_pos < int(
                self.game_width * 5.5):
            self.player.pos.x -= abs(self.player.vel.x)
            for platform in self.platforms:
                platform.rect.x -= int(abs(self.player.vel.x))
            for drop in self.drops:
                drop.rect.x -= int(abs(self.player.vel.x))
            for sand in self.sand_sprites:
                sand.rect.x -= int(abs(self.player.vel.x))
            for tree in self.trees:
                tree.rect.x -= int(abs(self.player.vel.x))
            for item in self.goal:
                item.rect.x -= int(abs(self.player.vel.x))

            self.skeleton.rect.x -= int(abs(self.player.vel.x))
            self.skeleton_drawing.rect.x -= int(abs(self.player.vel.x))
            self.shovel_digging_drawing.rect.x -= int(abs(self.player.vel.x))
            self.shovel.rect.x -= int(abs(self.player.vel.x))

            if self.ghost.rect.x > 0 and self.ghost.rect.y > 0:
                self.ghost.rect.x -= int(abs(self.player.vel.x))
            if self.ghost_attacking:
                self.attacking_ghost.rect.x -= int(abs(self.player.vel.x))
            if self.background.rect.right > self.game_width + 100:
                self.background.rect.x -= abs(self.player.vel.x / 6)

        elif self.player.rect.right < self.game_width*7//15 and self.player.vel.x < -0.1 and self.player.absolute_pos > int(
                self.game_width * -5):
            self.player.pos.x += abs(self.player.vel.x)
            for platform in self.platforms:
                platform.rect.x += int(abs(self.player.vel.x))
            for drop in self.drops:
                drop.rect.x += int(abs(self.player.vel.x))
            for sand in self.sand_sprites:
                sand.rect.x += int(abs(self.player.vel.x))
            for tree in self.trees:
                tree.rect.x += int(abs(self.player.vel.x))
            for item in self.goal:
                item.rect.x += int(abs(self.player.vel.x))

            self.skeleton.rect.x += int(abs(self.player.vel.x))
            self.skeleton_drawing.rect.x += int(abs(self.player.vel.x))
            self.shovel_digging_drawing.rect.x += int(abs(self.player.vel.x))
            self.shovel.rect.x += int(abs(self.player.vel.x))

            if self.ghost.rect.x > 0 and self.ghost.rect.y > 0:
                self.ghost.rect.x += int(abs(self.player.vel.x))
            if self.ghost_attacking:
                self.attacking_ghost.rect.x += int(abs(self.player.vel.x))
            if self.background.rect.left < -100:
                self.background.rect.x += abs(self.player.vel.x / 6)


        # MAKE SURE CEMETERY ALWAYS VISIBLE
        if self.player.absolute_pos >= int(self.game_width * 5.25):
            self.cemetery_gate.rect.right = self.game_width


        # SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom * 1.2:
            self.playing = False
            self.running = False
            self.show_game_over_screen()
        elif self.player.rect.top < self.screen_bottom and self.player.rect.top > 0:
            self.visibility_bubble.rect.centerx = self.player.rect.centerx
            self.visibility_bubble.rect.centery = self.player.rect.centery

        if self.player.moving:
            if self.player.direction == "Right":
                self.torch.rect.x = self.player.rect.right + self.player.player_width*.2
            else:
                self.torch.rect.x = self.player.rect.left - self.player.player_width*.25
            self.torch.rect.y = self.player.rect.centery - self.player.player_height * .5

        else:
            if self.player.direction == "Right":
                self.torch.rect.x = self.player.rect.right - self.player.player_width*.1
            else:
                self.torch.rect.x = self.player.rect.left
            self.torch.rect.y = self.player.rect.centery - self.player.player_height * .5


        if self.ghost.rect.centerx <= self.player.rect.centerx:
            self.ghost.image = pg.transform.flip(pg.image.load("sprite_ren_zhichu_ghost.png"), True, False)
        else:
            self.ghost.image = pg.image.load("sprite_ren_zhichu_ghost.png")

        if self.thundering:
            time_since_thunder = now - self.last_thunder
            if time_since_thunder <= 900:
                if random() <= .6:
                    self.visibility_bubble.rect.x = -3000
                    self.visibility_bubble.rect.y = -3000

            elif time_since_thunder >= 5900:
                self.thundering = False
                self.ghost.rect.x = -3000
                self.ghost.rect.y = -3000

            elif self.last_flash < self.last_thunder:
                self.visibility_bubble.rect.x = -3000
                self.visibility_bubble.rect.y = -3000
                self.thunder_sound.play()
                self.last_flash = now

            if abs(self.ghost.rect.centerx - self.player.rect.centerx) <= 200: #make ghost disappear if player too close
                self.ghost.rect.x = -3000
                self.ghost.rect.y = -3000

        else:
            if now - self.last_thunder > randrange(20000,35000) and now >= 20000:
                self.last_thunder = now
                self.thundering = True

                if not self.ghost_attacking and not self.ghost_aggressive:
                    if self.player.rect.x >= 900:
                        self.ghost.rect.x = 200
                    elif self.player.rect.x <= 300:
                        self.ghost.rect.x = 1000
                    else:
                        if randrange(2):
                            self.ghost.rect.x = 1000
                        else:
                            self.ghost.rect.x = 200
                    self.ghost.rect.y = self.game_height - 170
                    self.ghost_sound.play()


            if now - self.last_ghost_attack >= randrange(40000, 50000) and self.ghost_aggressive and not self.ghost_attacking:
                self.ghost_attacking = True
                if self.player.rect.x >= 900:
                    self.attacking_ghost.rect.x = 100
                elif self.player.rect.x <= 300:
                    self.attacking_ghost.rect.x = 1100
                else:
                    if randrange(2):
                        self.attacking_ghost.rect.x = 1100
                    else:
                        self.attacking_ghost.rect.x = 100
                self.attacking_ghost.rect.y = self.game_height - 170
                self.ghost_sound.play()
                self.ghost_attack_sound.play()

            if now - self.last_dig >= 1000:
                self.shovel_animated.rect.x = -3000
                self.shovel_animated.rect.y = -3000


    def pick_up_item(self, item, sound, quantity=1):
        sound.play()
        if item in self.player_inventory:
            self.player_inventory[item] += quantity
        else:
            self.player_inventory[item] = quantity


    def use_item(self, item, sound, quantity=1):
        if sound:
            sound.play()
        self.player_inventory[item] -= quantity


    def events(self):
        now = pg.time.get_ticks()
        for event in pg.event.get():
            #Key press
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

                if event.key == pg.K_a:
                    self.last_action = now
                    if "tree_bark" not in self.player_inventory:
                        self.game_message = _("You don't have any wood to burn.")
                    elif self.player_inventory["tree_bark"] > 0:
                        self.torch_life += randrange(10, 21)
                        self.use_item("tree_bark", self.wood_crack_sound)
                    else:
                        self.game_message = _("You don't have any wood to burn.")

                if event.key == pg.K_s:
                    self.last_action = now
                    if self.main_game.stamina >= 1000:
                        self.torch_life = 100
                        self.main_game.stamina -= 1000
                        self.flame_ignition_sound.play()
                    else:
                        self.game_message = _("Not enough stamina to perform action.")

                if event.key == pg.K_d:
                    self.last_action = now
                    if "shovel" in self.player_inventory and not self.player.jumping:
                        if now - self.last_dig >= 2000:
                            self.last_dig = now
                            self.digging_sound.play()
                            self.shovel_animated.rect.centerx = self.player.rect.centerx
                            self.shovel_animated.rect.centery = self.player.rect.centery
                            found_item = False
                            for pc in pg.sprite.spritecollide(self.player, self.platforms, False):
                                pcw = pc.image.get_width()
                                if pcw == 150 and "bronze_key" not in self.player_inventory:
                                    self.pick_up_item("bronze_key", self.drop_sound)
                                    self.game_message = _("You dig for a while and find a bronze key.")
                                    found_item = True
                                elif pcw == 350 and "silver_key" not in self.player_inventory:
                                    self.pick_up_item("silver_key", self.drop_sound)
                                    self.game_message = _("You dig for a while and find a silver key.")
                                    found_item = True

                            if self.skeleton.rect.centerx - self.player.rect.centerx >=50 and self.skeleton.rect.x - self.player.rect.centerx <= 250 and "gold_key" not in self.player_inventory:
                                self.pick_up_item("gold_key", self.drop_sound)
                                self.game_message = _("You dig for a while and find a gold key.")
                                found_item = True
                            
                            if not found_item:
                                self.game_message = _("You did not find anything interesting.")

                    else:
                        self.game_message = _("You cannot perform this action right now.")

                if event.key == pg.K_f:
                    self.last_action = now
                    if "health_potion" not in self.player_inventory:
                        self.game_message = _("You don't have any health potions to drink.")
                    elif self.player_inventory["health_potion"] > 0:
                        self.main_game.restore(h=self.main_game.healthMax * 30 // 100, s=0, full=False)
                        self.use_item("health_potion", None)
                    else:
                        self.game_message = _("You don't have any health potions to drink.")

                if event.key == pg.K_SPACE:
                    if self.tree_collision and now - self.last_action >= 2000:
                        self.last_action = now
                        if random() <= .75:
                            self.game_message = _("You manage to find some nice wood to burn.")
                            self.pick_up_item("tree_bark", self.wood_crack_sound)
                            if random() <= .3:
                                self.tree_collision[0].kill()
                        else:
                            self.game_message = _("After a brief search, you were unable to find any good wood to burn.")

                    elif self.skeleton_drawing_collision:
                        self.skeleton_drawing.rect.x = -3000
                        self.skeleton_drawing.rect.y = -3000
                        self.skeleton_drawing.kill()
                        self.pick_up_item("skeleton_drawing", self.drop_sound)

                    elif self.shovel_digging_drawing_collision:
                        self.shovel_digging_drawing.rect.x = -3000
                        self.shovel_digging_drawing.rect.y = -3000
                        self.shovel_digging_drawing.kill()
                        self.pick_up_item("shovel_digging_drawing", self.drop_sound)

                    elif self.shovel_collision:
                        self.shovel.rect.x = -3000
                        self.shovel.rect.y = -3000
                        self.shovel.kill()
                        self.pick_up_item("shovel", self.drop_sound)

                    elif self.drop_collision:
                        self.drop_collision[0].rect.x = -3000
                        self.drop_collision[0].rect.y = -3000
                        self.drop_collision[0].kill()
                        self.pick_up_item("health_potion", self.drop_sound)

                    elif self.skeleton_collision:
                        if self.viewing_item != "skeleton_sitting":
                            self.skeleton_sitting_buttons_pressed = [0, 0, 0, 0, 0]
                            self.last_action = now
                            self.ghost_aggressive = True
                            self.viewing_item = "skeleton_sitting"
                            self.game_message = ""
                            self.skeleton_sitting.rect.centerx = self.game_width // 2
                            self.skeleton_sitting.rect.centery = self.game_height // 2
                            self.all_sprites.add(self.skeleton_sitting)

                    elif self.goal_collision:
                        self.last_action = now
                        self.ghost_aggressive = True
                        keysFound = 0
                        for key in ["rusty_key", "bronze_key", "silver_key", "gold_key"]:
                            if key in self.player_inventory:
                                keysFound += 1

                        if keysFound == 4:
                            self.door_unlock_sound.play()
                            self.playing = False
                            self.running = False
                            self.main_game.mini_game_in_progress = False
                            pg.display.quit()
                            self.main_game.go_potters_field(stage=2)
                        else:
                            self.game_message = _("You need 4 keys to unlock the gate.")
                            self.door_lock_sound.play()

                    else:
                        self.game_message = ""

            #Key release
            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()

            #Mouse over
            pos = pg.mouse.get_pos()
            if self.skeleton_drawing_icon.rect.collidepoint(pos):
                self.game_message = _("Click to view.")
                self.last_action = now
            if self.shovel_digging_drawing_icon.rect.collidepoint(pos):
                self.game_message = _("Click to view.")
                self.last_action = now
            if self.tree_bark_icon.rect.collidepoint(pos):
                self.game_message = _("Press <A> to use wood to feed your torch.")
                self.last_action = now
            if self.shovel_icon.rect.collidepoint(pos):
                self.game_message = _("Press <D> to dig using the shovel.")
                self.last_action = now
            if self.health_potion_icon.rect.collidepoint(pos):
                self.game_message = _("Press <F> to drink health potion.")
                self.last_action = now

            # Mouse button release
            if event.type == pg.MOUSEBUTTONUP:
                self.game_message = ""
                if self.skeleton_drawing_icon.rect.collidepoint(pos) and not self.viewing_item:
                    self.viewing_item = "skeleton_drawing"
                    self.skeleton_drawing_full.rect.centerx = self.game_width//2
                    self.skeleton_drawing_full.rect.centery = self.game_height//2
                    self.all_sprites.add(self.skeleton_drawing_full)
                elif self.shovel_digging_drawing_icon.rect.collidepoint(pos) and not self.viewing_item:
                    self.viewing_item = "shovel_digging_drawing"
                    self.shovel_digging_drawing_full.rect.centerx = self.game_width//2
                    self.shovel_digging_drawing_full.rect.centery = self.game_height//2
                    self.all_sprites.add(self.shovel_digging_drawing_full)
                elif self.skeleton_sitting.rect.collidepoint(pos):
                    self.click_sound.play()
                    self.last_action = now
                    pressed = False
                    correct_coords = [(637, 264),(645, 229),(683, 472),(633, 351),(579, 349)]
                    for correct_coord in correct_coords:
                        if calculate_distance(*pos, *correct_coord) <= 10:
                            index = correct_coords.index(correct_coord)
                            self.skeleton_sitting_buttons_pressed[index] = 1
                            pressed = True
                            self.game_message = _("You touch the skeleton and find a soft spot that can be pushed in slightly.")
                            break
                    if not pressed:
                        self.game_message = _("You touch the skeleton, but nothing interesting happens.")
                    if sum(self.skeleton_sitting_buttons_pressed) == 5 and _("rusty_key") not in self.player_inventory:
                        self.game_message = _("You press the skeleton and hear a click. A key drops out from the bottom of the skeleton.")
                        self.pick_up_item("rusty_key", self.drop_sound)

                else:
                    if self.viewing_item == "skeleton_drawing":
                        self.viewing_item = None
                        self.all_sprites.remove(self.skeleton_drawing_full)
                    elif self.viewing_item == "shovel_digging_drawing":
                        self.viewing_item = None
                        self.all_sprites.remove(self.shovel_digging_drawing_full)




    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            item_index = 0
            for item, amount in self.player_inventory.items():
                self.draw_text("x {}".format(amount), 16, WHITE, self.icon_starting_x - item_index*100, 50)
                self.player_inventory_icons[item].rect.x = self.icon_starting_x - (item_index*100+50)
                self.player_inventory_icons[item].rect.y = 30
                item_index += 1
            self.draw_text("{}".format(self.game_message), 16, WHITE, self.game_width//2, self.game_height//2)
            pg.display.flip()


    def take_damage(self, damage, silent=False):
        if not silent:
            self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.main_game.display_lose_screen()


    def reset_falling_platforms(self, value=0):
        for fp in self.falling_platforms:
            fp.total_standing_time = value


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump. The lighting from your torch will slowly diminish over time.", 16,
                       WHITE, self.game_width // 2, self.game_height*2//10)
        self.draw_text("Press <A> to use up wood that you've collected to fuel your torch.", 16,
                       WHITE, self.game_width // 2, self.game_height*3//10)
        self.draw_text("Press <S> to spend 1000 stamina and re-light the torch.", 16,
                       WHITE, self.game_width // 2, self.game_height*4//10)
        self.draw_text("Press <Spacebar> to begin.", 16, WHITE, self.game_width // 2, self.game_height*6//10)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0, 0, 0, 200))
        self.screen.blit(self.dim_screen, (0, 0))
        self.draw_text("You died!", 22, WHITE, self.game_width // 2, self.game_height // 3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2,
                       self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True):  # if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start and event.key == pg.K_SPACE:
                        waiting = False
                        self.running = True


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)