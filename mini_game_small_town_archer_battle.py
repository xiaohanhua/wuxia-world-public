from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class small_town_archer_battle_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = 1300, int(LANDSCAPE_HEIGHT*1.25)
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")
        self.smoke_bomb_sound = pg.mixer.Sound("sfx_smoke_bomb.wav")

        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina

    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.archers = pg.sprite.Group()
        self.spearmen = pg.sprite.Group()
        self.projectiles = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.smoke_bombs = pg.sprite.Group()
        self.spike_walls = pg.sprite.Group()

        #self.background = Static(0, self.game_height-1200, "Plains BG.jpg")
        self.background = Static(0, self.game_height - 1200, "Cemetery BG.png")
        self.all_sprites.add(self.background)

        spike_wall_parameters = [[200, self.game_height-900, 0, self],
                                 [1100, self.game_height-900, 0, self, True]]
        for p in spike_wall_parameters:
            spike_wall = Spike_Wall(*p)
            self.spike_walls.add(spike_wall)
            self.all_sprites.add(spike_wall)

        self.player = Player(self, self.game_width//2, self.game_height-200)
        self.all_sprites.add(self.player)

        platform_parameters = [[-100, self.game_height - 200, 2000, self.game_height * 2 // 3],
                               [400, self.game_height - 300, 150, 20],
                               [850, self.game_height - 375, 50, 20],
                               [0, self.game_height - 350, 150, 20],
                               [0, self.game_height - 500, 150, 20],
                               [0, self.game_height - 650, 150, 20],
                               [1200, self.game_height - 350, 150, 20],
                               [1200, self.game_height - 500, 150, 20],
                               [1200, self.game_height - 650, 150, 20],
                               ]

        for pp in platform_parameters:
            platform = Platform_Earth(*pp)
            self.all_sprites.add(platform)
            self.platforms.add(platform)

        archer_parameters = [[80, self.game_height-200],
                             [60, self.game_height-200],
                             [70, self.game_height-350],
                             [60, self.game_height-200],
                             [110, self.game_height-200],
                             [80, self.game_height-350],
                             [130, self.game_height-350],
                             [100, self.game_height-350],
                             [120, self.game_height-200],
                             [130, self.game_height-650],
                             [100, self.game_height-650],
                             [130, self.game_height-350],
                             [120, self.game_height-500],
                             [130, self.game_height-500],
                             [80, self.game_height-500],
                             [40, self.game_height-500],
                             [120, self.game_height-200],
                             [130, self.game_height-350],
                             [30, self.game_height-650],
                             [70, self.game_height-650],
                             [1260, self.game_height - 200],
                             [1250, self.game_height - 200],
                             [1300, self.game_height - 200],
                             [1240, self.game_height - 200],
                             [1260, self.game_height - 350],
                             [1240, self.game_height - 350],
                             [1280, self.game_height - 350],
                             [1220, self.game_height - 350],
                             [1270, self.game_height - 500],
                             [1250, self.game_height - 500],
                             [1300, self.game_height - 500],
                             [1230, self.game_height - 500],
                             [1210, self.game_height - 500],
                             [1290, self.game_height - 650],
                             [1260, self.game_height - 650],
                             [1240, self.game_height - 650],
                             ]


        for p in archer_parameters:
            archer = Archer(*p, self)
            self.all_sprites.add(archer)
            self.archers.add(archer)

        spearman_parameters = [[100, self.game_height - 200, self],
                               [100, self.game_height - 350, self],
                               [100, self.game_height - 500, self],
                               [100, self.game_height - 650, self],
                               [1300, self.game_height - 200, self],
                               [1300, self.game_height - 350, self],
                               [1300, self.game_height - 500, self],
                               [1300, self.game_height - 650, self],
                               ]

        for p in spearman_parameters:
            spearman = Spearman(*p)
            self.all_sprites.add(spearman)
            self.spearmen.add(spearman)

        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30+16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)


        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        self.last_threw_smoke_bomb = 0

        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def throw_smoke_bomb(self, x, y):

        self.last_threw_smoke_bomb = pg.time.get_ticks()
        smoke_bomb_x = x
        smoke_bomb_y = y
        x_dist = smoke_bomb_x - self.player.rect.centerx
        y_dist = smoke_bomb_y - self.player.rect.centery
        dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1

        smoke_bomb_move = [m for m in self.main_game.sml if _("Smoke Bomb") in m.name][0]
        smoke_bomb_level = smoke_bomb_move.level
        smoke_bomb_move.gain_exp(10)

        x_vel = x_dist / dist * randrange(10-(11-smoke_bomb_level)//2, 11) * (dist**.12)
        y_vel = y_dist / dist * randrange(10-(11-smoke_bomb_level)//2, 11) * (dist**.12)
        smoke_bomb = SmokeBomb(self.player.rect.centerx, self.player.rect.centery, x_vel, y_vel)
        self.all_sprites.add(smoke_bomb)
        self.smoke_bombs.add(smoke_bomb)


    def generate_projectile(self, x, y, speed, weapons):

        self.projectile_sound.play()

        projectile_x = x
        projectile_y = y
        x_dist = self.player.rect.centerx - projectile_x
        y_dist = (self.player.rect.centery - projectile_y)
        if x_dist != 0:
            y_dist -= abs(x_dist*(random()))*(1+abs(y_dist/x_dist))
        dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1

        x_vel = x_dist / dist * randrange(20, 25)
        y_vel = y_dist / dist * randrange(5, 15)
        projectile = Angled_Arrow(x, y, x_vel, y_vel, "Angled Arrow", "Sprite_Arrow.png", 50, 0, None)
        self.all_sprites.add(projectile)
        self.projectiles.add(projectile)


    def update(self):

        self.all_sprites.update()
        # PLATFORM COLLISION
        on_screen_platforms = self.platforms
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)

        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 5 // 9 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x


        #PROJECTILE COLLISION
        projectile_collision = pg.sprite.spritecollide(self.player, self.projectiles, True, pg.sprite.collide_circle_ratio(.75))
        if projectile_collision and not projectile_collision[0].dead:
            self.take_damage(projectile_collision[0].damage)
            self.player.stun_count += projectile_collision[0].stun

        #PROJECTILE - PLATFORM COLLISION
        for proj in self.projectiles:
            if not proj.dead:
                projectile_platform_collision = pg.sprite.spritecollide(proj, on_screen_platforms, False)
                if projectile_platform_collision:
                    proj.set_orientation()
                    proj.x_vel = 0
                    proj.y_vel = 0
                    proj.dead = True
            elif random() <= .001:
                proj.kill()


        # archer COLLISION
        archer_collision = pg.sprite.spritecollide(self.player, self.archers, True,
                                                   pg.sprite.collide_circle_ratio(.75))
        if archer_collision:
            self.running = False
            opp = character(_("Archers"), 10,
                            attributeList=[2000,2000,100,100,100,130,110,10,10],
                            sml=[special_move(_("Archery"), level=10)],
                            equipmentList=[Equipment(_("Golden Chainmail"))]
                            )

            cmd = lambda: self.main_game.resume_mini_game("jy_daojiangu.mp3")
            self.main_game.battleMenu(self.main_game.you, opp, "battleground_trade_ship.png",
                                      postBattleSoundtrack="jy_daojiangu.mp3",
                                      fromWin=None,
                                      battleType="task", destinationWinList=[] * 3,
                                      postBattleCmd=cmd
                                      )

        #SPEARMAN COLLISION
        spearman_collision = pg.sprite.spritecollide(self.player, self.spearmen, True,
                                                     pg.sprite.collide_circle_ratio(.5))
        if spearman_collision:
            self.running = False
            opp = character(_("Spearmen"), 10,
                            attributeList=[1300, 1300, 200, 200, 100, 120, 70, 50, 10],
                            sml=[special_move(_("Spear Attack"), level=10)]
                            )
            cmd = lambda: self.main_game.resume_mini_game("jy_daojiangu.mp3")
            self.main_game.battleMenu(self.main_game.you, opp, "battleground_trade_ship.png",
                                      postBattleSoundtrack="jy_daojiangu.mp3",
                                      fromWin=None,
                                      battleType="task", destinationWinList=[] * 3,
                                      postBattleCmd=cmd
                                      )

        # SMOKE BOMB COLLISION

        for smoke_bomb in self.smoke_bombs:
            smoke_bomb_collision_1 = pg.sprite.spritecollide(smoke_bomb, self.platforms, False,
                                                           pg.sprite.collide_rect)
            smoke_bomb_collision_2 = pg.sprite.spritecollide(smoke_bomb, self.spike_walls, False,
                                                           pg.sprite.collide_rect)
            if (smoke_bomb_collision_1 or smoke_bomb_collision_2) and not smoke_bomb.exploded:
                smoke_bomb.explode()
                self.smoke_bomb_sound.play()

            if smoke_bomb.exploded:
                pg.sprite.spritecollide(smoke_bomb, self.archers, True, pg.sprite.collide_circle_ratio(1.5))
                pg.sprite.spritecollide(smoke_bomb, self.spearmen, True, pg.sprite.collide_circle_ratio(1.5))


        # SPIKE WALL COLLISION
        spike_wall_collision = pg.sprite.spritecollide(self.player, self.spike_walls, False)
        if spike_wall_collision:
            if spike_wall_collision[0].rect.x <= self.game_width//2:
                if self.player.vel.x < 0 and self.player.rect.centerx >= spike_wall_collision[0].rect.centerx:
                    self.take_damage(200)
                    self.player.vel.x -= self.player.vel.x * 6
                    self.player.vel.y = 0
                elif self.player.vel.x > 0 and self.player.rect.centerx <= spike_wall_collision[0].rect.centerx:
                    self.player.vel.x -= 10
                else:
                    if self.player.rect.centerx < spike_wall_collision[0].rect.centerx:
                        self.player.vel.x -= 10
                        self.player.vel.y -= 5
                    else:
                        self.take_damage(200)
                        self.player.vel.x += 10
                        self.player.vel.y -= 5

            if spike_wall_collision[0].rect.x >= self.game_width//2:
                if self.player.vel.x > 0 and self.player.rect.centerx <= spike_wall_collision[0].rect.centerx:
                    self.take_damage(200)
                    self.player.vel.x -= self.player.vel.x * 6
                    self.player.vel.y = 0
                elif self.player.vel.x < 0 and self.player.rect.centerx >= spike_wall_collision[0].rect.centerx:
                    self.player.vel.x += 10
                else:
                    if self.player.rect.centerx > spike_wall_collision[0].rect.centerx:
                        self.player.vel.x -= 10
                        self.player.vel.y -= 5
                    else:
                        self.take_damage(200)
                        self.player.vel.x += 10
                        self.player.vel.y -= 5

        #WINDOW SCROLLING

        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(self.player.vel.y)
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += int(abs(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y += int(abs(self.player.vel.y))
            for archer in self.archers:
                archer.rect.y += int(abs(self.player.vel.y))
            for spearman in self.spearmen:
                spearman.rect.y += int(abs(self.player.vel.y))
            for spike_wall in self.spike_walls:
                spike_wall.rect.y += int(abs(self.player.vel.y))
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.y += int(abs(self.player.vel.y))

            self.screen_bottom += int(abs(self.player.vel.y))
            self.background.rect.y += int(abs(self.player.vel.y)/4)


        elif self.player.rect.bottom >= self.game_height * 2//3 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.pos.y -= abs(self.player.vel.y)
            self.player.last_height -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= int(abs(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y -= int(abs(self.player.vel.y))
            for archer in self.archers:
                archer.rect.y -= int(abs(self.player.vel.y))
            for spearman in self.spearmen:
                spearman.rect.y -= int(abs(self.player.vel.y))
            for spike_wall in self.spike_walls:
                spike_wall.rect.y -= int(abs(self.player.vel.y))
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.y -= int(abs(self.player.vel.y))

            self.screen_bottom -= int(abs(self.player.vel.y))
            self.background.rect.y -= int(abs(self.player.vel.y) / 4)


        #End condition
        survival_time = (pg.time.get_ticks() - self.start_time) / 1000
        if (self.main_game.health > 0 and self.main_game.health < 100) or survival_time >= 120 and self.playing and self.running:
            # Determine if townspeople escaped successfully
            self.playing = False
            self.running = False
            if random() * 100 <= survival_time and survival_time >= 30:
                self.main_game.taskProgressDic["small_town"] = 10
            else:
                self.main_game.taskProgressDic["small_town"] = 20
            self.scene.town_treason(3)


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()

            pos = pg.mouse.get_pos()
            if event.type == pg.MOUSEBUTTONUP and pg.time.get_ticks() - self.last_threw_smoke_bomb >= 1000 and self.player.stun_count==0:
                if self.main_game.check_for_item(_("Smoke Bomb")) and _("Smoke Bomb") in [m.name for m in
                                                                                          self.main_game.sml]:
                    self.main_game.add_item(_("Smoke Bomb"), -1)
                    self.throw_smoke_bomb(pos[0], pos[1])


    def take_damage(self, damage, silent=False):
        if not silent:
            self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump. Distract the archers for as long as you can.", 20, WHITE, self.game_width//2, self.game_height//5)
        self.draw_text("The longer you distract them, the more likely the townspeople will escape successfully.", 20, WHITE, self.game_width // 2, self.game_height*2//5)
        self.draw_text("If you have Smoke Bombs and have learned the move, you can left click to throw a smoke bomb.", 20, WHITE, self.game_width // 2, self.game_height*3//5)
        self.draw_text("You will automatically flee when your health is below 100. Press any key to begin.", 20, WHITE, self.game_width // 2, self.game_height*4//5)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    if start:
                        self.start_time = pg.time.get_ticks()
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)