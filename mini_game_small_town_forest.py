from mini_game_sprites import *
from character import *
from skill import *
from special_move import *
from item import *
from gettext import gettext
from helper_funcs import *

_ = gettext


class small_town_forest_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = 1200, 700
        self.screen = pg.display.set_mode((self.game_width, self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")
        self.thunder_sound = pg.mixer.Sound("Thunder.wav")
        self.flame_ignition_sound = pg.mixer.Sound("flame_ignition.wav")
        self.wood_crack_sound = pg.mixer.Sound("wood_crack.wav")
        self.drop_sound = pg.mixer.Sound("button-19.wav")
        self.click_sound = pg.mixer.Sound("button-20.wav")


    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.drops = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.trees = pg.sprite.Group()
        self.goal = pg.sprite.Group()

        self.background = Static(-100, self.game_height - 1600, "Cemetery BG.png")
        self.all_sprites.add(self.background)

        self.small_hut = Static(5900, self.game_height-100-120, "sprite_hut_in_woods.png")
        self.goal.add(self.small_hut)
        self.all_sprites.add(self.small_hut)

        self.player = Player(self, 200, self.game_height - 150)
        self.all_sprites.add(self.player)

        self.torch = Static(self.player.rect.right, self.player.rect.centery - self.player.player_height * .5,
                            filename="sprite_torch_1.png",
                            animations=[pg.image.load("sprite_torch_{}.png".format(i)) for i in range(1, 5)])
        self.all_sprites.add(self.torch)

        platform_parameters = [
            [-100, self.game_height - 100, 500, self.game_height * 2 // 3],
            [600, self.game_height - 100, 250, self.game_height * 2 // 3],
            [1500, self.game_height - 100, 700, self.game_height * 2 // 3],
            [300, self.game_height - 300, 50, 20],
            [0, self.game_height - 480, 100, 20],
            [550, self.game_height - 450, 100, 20],
            [400, self.game_height - 650, 100, 20],
            [100, self.game_height - 820, 100, 20],
            [0, self.game_height - 1020, 100, 20],
            [250, self.game_height - 1200, 100, 20],
            [450, self.game_height - 1400, 600, 20],
            [1250, self.game_height - 1300, 100, 20],
            [1500, self.game_height - 1150, 100, 20],
            [1400, self.game_height - 1000, 100, 20],
            [1700, self.game_height - 1100, 600, 20],
            [1650, self.game_height - 800, 100, 20],
            [1500, self.game_height - 600, 50, 20],
            [1700, self.game_height - 450, 50, 20],
            [1900, self.game_height - 250, 50, 20],
            [2450, self.game_height - 100, 300, self.game_height * 2 // 3],
            [3000, self.game_height - 100, 300, self.game_height * 2 // 3],
            [3500, self.game_height - 100, 300, self.game_height * 2 // 3],
            [3900, self.game_height - 250, 100, 20],
            [4200, self.game_height - 450, 50, 20],
            [4450, self.game_height - 300, 50, 20],
            [3650, self.game_height - 300, 50, 20],
            [3350, self.game_height - 500, 100, 20],
            [3600, self.game_height - 700, 50, 20],
            [3400, self.game_height - 950, 50, 20],
            [3100, self.game_height - 1100, 100, 20],
            [3350, self.game_height - 1300, 100, 20],
            [3600, self.game_height - 1500, 800, 20],
            [4600, self.game_height - 1400, 100, 20],
            [4800, self.game_height - 1250, 100, 20],
            [5000, self.game_height - 1100, 100, 20],
            [5200, self.game_height - 950, 100, 20],
            [4900, self.game_height - 800, 100, 20],
            [5100, self.game_height - 600, 50, 20],
            [5300, self.game_height - 400, 50, 20],
            [5200, self.game_height - 100, 1400, self.game_height * 2 // 3],
        ]

        for pp in platform_parameters:
            platform = Platform_Earth(*pp)
            self.platforms.add(platform)
            self.all_sprites.add(platform)

        tree_parameters = [[300, self.game_height - 100 - 120, "sprite_dead_tree.png"],
                           [700, self.game_height - 100 - 120, "sprite_dead_tree.png"],
                           [3100, self.game_height - 100 - 120, "sprite_dead_tree.png"],]

        for tp in tree_parameters:
            tree = Static(*tp)
            self.trees.add(tree)
            self.all_sprites.add(tree)

        self.visibility_bubble = Static(self.player.rect.centerx, self.player.rect.centery,
                                        "sprite_visibility_bubble.png")
        self.all_sprites.add(self.visibility_bubble)


        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30 + 16, "bar_mp.png", "Stamina", self)
        self.icon_starting_x = self.game_width * 9 // 10
        self.tree_bark_icon = Static(- 3000, -3000, "sprite_tree_bark.png")


        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)
        self.all_sprites.add(self.tree_bark_icon)

        self.last_thunder = 0
        self.last_flash = 0
        self.thundering = False


        self.game_message = ""
        self.last_action = 0
        self.player_inventory = {}
        self.player_inventory_icons = {"tree_bark": self.tree_bark_icon,
                                       }

        self.tree_collision = None
        self.goal_collision = None
        self.drop_collision = None

        self.torch_life = 100
        self.torch_status = "Normal"

        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        if not self.retry:
            self.show_start_screen()
        self.run()


    def check_torch_life(self):
        self.torch_life -= .03
        if self.torch_life >= 100:
            self.torch_life = 100
        elif self.torch_life <= 0:
            self.torch_life = 0

        if self.torch_life > 75:
            if self.torch_status != "Normal":
                self.torch_status = "Normal"
                self.visibility_bubble.image = pg.image.load("sprite_visibility_bubble.png")
        elif self.torch_life > 50:
            if self.torch_status != "Small":
                self.torch_status = "Small"
                self.visibility_bubble.image = pg.image.load("sprite_visibility_bubble_small.png")
        elif self.torch_life > 25:
            if self.torch_status != "Tiny":
                self.torch_status = "Tiny"
                self.visibility_bubble.image = pg.image.load("sprite_visibility_bubble_tiny.png")
        elif self.torch_life <= 25:
            if self.torch_status != "Minuscule":
                self.torch_status = "Minuscule"
                self.visibility_bubble.image = pg.image.load("sprite_visibility_bubble_minuscule.png")

    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def update(self):

        self.all_sprites.update()
        self.check_torch_life()
        now = pg.time.get_ticks()

        if now - self.last_action > 2000:
            self.game_message = ""
        #on_screen_platforms = [platform for platform in self.platforms if platform.rect.centerx >= -100 and platform.rect.centerx <= self.game_width+100]
        on_screen_platforms = self.platforms
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)
        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 5 // 9 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= col.image.get_height() - self.player.player_height / 2 and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= col.image.get_height() - self.player.player_height / 2 and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x



        # SAND COLLISION
        sand_collision = pg.sprite.spritecollide(self.player, self.sand_sprites, False)

        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height * 3 // 5) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x

        if sand_collision:
            self.player.vel.y *= .5
            current_height = sand_collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 600:
                damage = int(200 * 1.005 ** (fall_height - 600))
                print("Ouch! Lost {} health from fall.".format(damage))
                self.take_damage(damage)

            self.player.last_height = current_height
            self.player.jumping = False
            if self.main_game.stamina > 3:
                self.main_game.stamina -= 3
            else:
                self.take_damage(5, silent=True)

        else:
            if self.main_game.stamina <= self.main_game.staminaMax:
                self.main_game.stamina += 0

        # GOAL COLLISION
        self.goal_collision = pg.sprite.spritecollide(self.player, self.goal, False)
        if self.goal_collision:
            self.running = False
            self.playing = False
            self.scene.post_forest_mini_game()

        # TREE COLLISION
        self.tree_collision = pg.sprite.spritecollide(self.player, self.trees, False)
        if self.tree_collision and self.game_message == "":
            self.game_message = _("Press <Spacebar> to look for some wood for your torch.")

        # DROP COLLISION
        self.drop_collision = pg.sprite.spritecollide(self.player, self.drops, False,
                                                      pg.sprite.collide_circle_ratio(.75))
        if self.drop_collision and self.game_message == "":
            self.game_message = _("Press <Spacebar> to pick up health potion.")

        # WINDOW SCROLLING

        if self.player.rect.right > self.game_width * 8 // 15 and self.player.vel.x > 0.1 and self.player.absolute_pos < 5800:
            self.player.pos.x -= abs(self.player.vel.x)
            for platform in self.platforms:
                platform.rect.x -= int(abs(self.player.vel.x))
            for drop in self.drops:
                drop.rect.x -= int(abs(self.player.vel.x))
            for sand in self.sand_sprites:
                sand.rect.x -= int(abs(self.player.vel.x))
            for tree in self.trees:
                tree.rect.x -= int(abs(self.player.vel.x))
            for item in self.goal:
                item.rect.x -= int(abs(self.player.vel.x))

            if self.background.rect.right > self.game_width + 100:
                self.background.rect.x -= abs(self.player.vel.x / 6)

        elif self.player.rect.right < self.game_width * 7 // 15 and self.player.vel.x < -0.1 and self.player.absolute_pos > self.game_width//2:
            self.player.pos.x += abs(self.player.vel.x)
            for platform in self.platforms:
                platform.rect.x += int(abs(self.player.vel.x))
            for drop in self.drops:
                drop.rect.x += int(abs(self.player.vel.x))
            for sand in self.sand_sprites:
                sand.rect.x += int(abs(self.player.vel.x))
            for tree in self.trees:
                tree.rect.x += int(abs(self.player.vel.x))
            for item in self.goal:
                item.rect.x += int(abs(self.player.vel.x))

            if self.background.rect.left < -100:
                self.background.rect.x += abs(self.player.vel.x / 6)


        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(self.player.vel.y)
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += int(abs(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y += int(abs(self.player.vel.y))
            for sand in self.sand_sprites:
                sand.rect.y += int(abs(self.player.vel.y))
            for tree in self.trees:
                tree.rect.y += int(abs(self.player.vel.y))
            for item in self.goal:
                item.rect.y += int(abs(self.player.vel.y))

            self.screen_bottom += abs(round(self.player.vel.y))
            self.background.rect.y += abs(round(self.player.vel.y)/4)


        elif self.player.rect.bottom >= self.game_height * 2 //5 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.pos.y -= abs(self.player.vel.y)
            self.player.last_height -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= int(abs(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y -= int(abs(self.player.vel.y))
            for sand in self.sand_sprites:
                sand.rect.y -= int(abs(self.player.vel.y))
            for tree in self.trees:
                tree.rect.y -= int(abs(self.player.vel.y))
            for item in self.goal:
                item.rect.y -= int(abs(self.player.vel.y))

            self.screen_bottom -= abs(round(self.player.vel.y))
            self.background.rect.y -= abs(round(self.player.vel.y) / 4)


        # MAKE SURE GOAL ALWAYS VISIBLE
        if self.player.absolute_pos >= int(self.game_width * 5.25):
            self.small_hut.rect.right = self.game_width

        # SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom * 1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()
        elif self.player.rect.top < self.screen_bottom and self.player.rect.top > 0:
            self.visibility_bubble.rect.centerx = self.player.rect.centerx
            self.visibility_bubble.rect.centery = self.player.rect.centery

        if self.player.moving:
            if self.player.direction == "Right":
                self.torch.rect.x = self.player.rect.right + self.player.player_width * .2
            else:
                self.torch.rect.x = self.player.rect.left - self.player.player_width * .25
            self.torch.rect.y = self.player.rect.centery - self.player.player_height * .5

        else:
            if self.player.direction == "Right":
                self.torch.rect.x = self.player.rect.right - self.player.player_width * .1
            else:
                self.torch.rect.x = self.player.rect.left
            self.torch.rect.y = self.player.rect.centery - self.player.player_height * .5


        #THUNDER/LIGHTNING
        if self.thundering:
            time_since_thunder = now - self.last_thunder
            if time_since_thunder <= 900:
                if random() <= .6:
                    self.visibility_bubble.rect.x = -3000
                    self.visibility_bubble.rect.y = -3000

            elif time_since_thunder >= 5900:
                self.thundering = False

            elif self.last_flash < self.last_thunder:
                self.visibility_bubble.rect.x = -3000
                self.visibility_bubble.rect.y = -3000
                self.thunder_sound.play()
                self.last_flash = now


        else:
            if now - self.last_thunder > randrange(15000,30000) and now >= 15000:
                self.last_thunder = now
                self.thundering = True




    def pick_up_item(self, item, sound, quantity=1):
        sound.play()
        if item in self.player_inventory:
            self.player_inventory[item] += quantity
        else:
            self.player_inventory[item] = quantity


    def use_item(self, item, sound, quantity=1):
        if sound:
            sound.play()
        self.player_inventory[item] -= quantity


    def events(self):
        now = pg.time.get_ticks()
        for event in pg.event.get():
            # Key press
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

                if event.key == pg.K_a:
                    self.last_action = now
                    if "tree_bark" not in self.player_inventory:
                        self.game_message = _("You don't have any wood to burn.")
                    elif self.player_inventory["tree_bark"] > 0:
                        self.torch_life += randrange(10, 21)
                        self.use_item("tree_bark", self.wood_crack_sound)
                    else:
                        self.game_message = _("You don't have any wood to burn.")

                if event.key == pg.K_s:
                    self.last_action = now
                    if self.main_game.stamina >= 1000:
                        self.torch_life = 100
                        self.main_game.stamina -= 1000
                        self.flame_ignition_sound.play()
                    else:
                        self.game_message = _("Not enough stamina to perform action.")

                if event.key == pg.K_SPACE:
                    if self.tree_collision and now - self.last_action >= 1500:
                        self.last_action = now
                        if random() <= .75:
                            self.game_message = _("You manage to find some nice wood to burn.")
                            self.pick_up_item("tree_bark", self.wood_crack_sound)
                            if random() <= .3:
                                self.tree_collision[0].kill()
                        else:
                            self.game_message = _(
                                "After a brief search, you were unable to find any good wood to burn.")

                    elif self.drop_collision:
                        self.drop_collision[0].rect.x = -3000
                        self.drop_collision[0].rect.y = -3000
                        self.drop_collision[0].kill()
                        self.pick_up_item("health_potion", self.drop_sound)

                    elif self.goal_collision:
                        self.last_action = now

                    else:
                        self.game_message = ""

            # Key release
            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            item_index = 0
            for item, amount in self.player_inventory.items():
                self.draw_text("x {}".format(amount), 16, WHITE, self.icon_starting_x - item_index * 100, 50)
                self.player_inventory_icons[item].rect.x = self.icon_starting_x - (item_index * 100 + 50)
                self.player_inventory_icons[item].rect.y = 30
                item_index += 1
            self.draw_text("{}".format(self.game_message), 16, WHITE, self.game_width // 2, self.game_height // 2)
            pg.display.flip()

    def take_damage(self, damage, silent=False):
        if not silent:
            self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump. The lighting from your torch will slowly diminish over time.",
                       16, WHITE, self.game_width // 2, self.game_height * 2 // 10)
        self.draw_text("Press <A> to use up wood that you've collected to fuel your torch.", 16,
                       WHITE, self.game_width // 2, self.game_height * 3 // 10)
        self.draw_text("Press <S> to spend 1000 stamina and re-light the torch.", 16,
                       WHITE, self.game_width // 2, self.game_height * 4 // 10)
        self.draw_text("Continue deeper into the forest until you find the dark figure.", 16,
                       WHITE, self.game_width // 2, self.game_height * 5 // 10)
        self.draw_text("Press <Spacebar> to begin.", 16, WHITE, self.game_width // 2, self.game_height * 6 // 10)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0, 0, 0, 200))
        self.screen.blit(self.dim_screen, (0, 0))
        self.draw_text("You died!", 22, WHITE, self.game_width // 2, self.game_height // 3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2,
                       self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True):  # if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start and event.key == pg.K_SPACE:
                        waiting = False
                        self.running = True
                    elif not start:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            for item, icon in self.player_inventory_icons.items():
                                icon.rect.x = -3000
                                icon.rect.y = -3000
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.playing=False
                            self.running=False
                            pg.display.quit()
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)