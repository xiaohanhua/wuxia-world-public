from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class target_practice_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = 772, 500
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.got_hit_count = 0
        self.projectile_count = 0
        self.last_generated_projectile = 0
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")


    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.enemies = pg.sprite.Group()
        self.projectiles = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()

        self.background = Static(0, 0, "target_practice_rock_bg.png")
        self.all_sprites.add(self.background)
        self.player = Player(self, 50, 420)
        self.all_sprites.add(self.player)


        platform_parameters = [[620, 500 - 400],
                               [643, 500 - 400],
                               [150, 500 - 150],
                               [173, 500 - 150],]

        for i in range(34):
            for j in range(3):
                parameters = [i*23, 500 - j*23]
                platform_parameters.append(parameters)

        for i in range(20,34):
            for j in range(3,15):
                parameters = [i * 23, 500 - j * 23]
                platform_parameters.append(parameters)

        enemy_parameters = [[650, 500 - 440]]


        for p in platform_parameters:
            x, y = p
            platform = Tile_Rock(x, y)
            self.all_sprites.add(platform)
            self.platforms.add(platform)


        for p in enemy_parameters:
            enemy = Static_Enemy(*p, "sprite_li_guiping_standing.png")
            self.all_sprites.add(enemy)
            self.enemies.add(enemy)

        self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            if self.running:
                try:
                    self.clock.tick(FPS)
                    self.events()
                    self.update()
                    self.draw()
                except:
                    pass



    def update(self):

        self.all_sprites.update()
        # PLATFORM COLLISION
        collision = pg.sprite.spritecollide(self.player, self.platforms, False)

        if collision:
            if (self.player.vel.y > self.player.player_height//2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2) or\
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height//2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0
            self.player.jumping = False

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0



        if self.player.vel.x > 0 and self.player.pos.x >= 20*23 - self.player.player_width:
            self.player.pos.x -= self.player.vel.x

        # PROJECTILE COLLISION
        projectile_collision = pg.sprite.spritecollide(self.player, self.projectiles, True, pg.sprite.collide_circle_ratio(.75))
        if projectile_collision:
            self.got_hit_count += 1
            self.main_game.health -= projectile_collision[0].damage
            if self.main_game.health <= 0:
                self.main_game.health = 10
            self.ouch_sound.play()


        now = pg.time.get_ticks()
        if self.projectile_count >= 45:
            self.playing = False
            self.running = False
            self.scene.post_li_guiping_target_practice(self.got_hit_count)

        elif now - self.last_generated_projectile >= randrange(700, 1200):
            self.last_generated_projectile = now
            self.generate_projectile()


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump.", 22, WHITE, PORTRAIT_WIDTH//2, 500//3)
        self.draw_text("Try to dodge as many needles as possible.", 22, WHITE, PORTRAIT_WIDTH//2, 500//2)
        self.draw_text("Press any key to begin.", 22, WHITE, PORTRAIT_WIDTH // 2, 500*2//3)
        pg.display.flip()
        self.listen_for_key()


    def listen_for_key(self):
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    waiting = False
                    self.running = True


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)


    def generate_projectile(self):
        if self.projectile_count < 45:
            self.projectile_sound.play()
            projectile_x = 625
            projectile_y = 500 - 430
            x_dist = self.player.rect.centerx - projectile_x
            y_dist = self.player.rect.centery - projectile_y
            dist = (x_dist**2 + y_dist**2)**.5
            x_vel = x_dist/dist*randrange(15,20)
            y_vel = y_dist/dist*randrange(15,20)
            proj = Projectile(projectile_x, projectile_y, x_vel, y_vel, "Poison Needle", "sprite_poison_needle.png", 50, 0)
            self.all_sprites.add(proj)
            self.projectiles.add(proj)
            self.projectile_count += 1



if __name__ == "__main__":
    g = target_practice_mini_game(None)
    g.new()
    pg.quit()
