from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class trade_ship_mini_game:
    def __init__(self, main_game, scene):
        self.main_game = main_game
        self.scene = scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        pg.font.init()
        pg.mixer.init()
        self.projectile_sound = pg.mixer.Sound("sfx_whoosh.wav")
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")
        self.male_grunt_sound = pg.mixer.Sound("sfx_male_voice.wav")
        self.smoke_bomb_sound = pg.mixer.Sound("sfx_smoke_bomb.wav")

        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina


    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.archers = pg.sprite.Group()
        self.spearmen = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.projectiles = pg.sprite.Group()
        self.smoke_bombs = pg.sprite.Group()
        self.drops = pg.sprite.Group()
        self.chest_of_goods = pg.sprite.Group()

        self.background = Static(0, self.game_height-850, "bg_trade_ship.png")
        self.all_sprites.add(self.background)

        self.player = Player(self, 80, self.game_height- 60)
        self.all_sprites.add(self.player)

        self.goods = Static(5*self.game_width, self.game_height - 24 - 56, "sprite_chest_of_goods.png")
        self.chest_of_goods.add(self.goods)
        self.all_sprites.add(self.goods)

        for i in range(14):
            self.generate_drop((225+.5*i)*23, self.game_height - 489, True)
        for i in range(14):
            self.generate_drop((225+.5*i)*23, self.game_height - 509, True)


        platform_parameters = [[10*23, self.game_height - 600, 170],
                               [5*23, self.game_height - 538, 3],
                               [5*23, self.game_height - 515, 3],
                               [5*23, self.game_height - 492, 3],
                               [5*23, self.game_height - 469, 3],
                               [5*23, self.game_height - 446, 3],
                               [5*23, self.game_height - 423, 3],
                               [5*23, self.game_height - 400, 3],
                               [5*23, self.game_height - 377, 3],
                               [5*23, self.game_height - 354, 3],
                               [5*23, self.game_height - 331, 3],
                               [5*23, self.game_height - 308, 3],
                               [5*23, self.game_height - 285, 3],
                               [5*23, self.game_height - 262, 3],
                               [5*23, self.game_height - 239, 3],
                               [5*23, self.game_height - 216, 3],
                               [5*23, self.game_height - 193, 3],
                               [5*23, self.game_height - 170, 3],
                               [5*23, self.game_height - 147, 3],
                               [5*23, self.game_height - 124, 3],
                               #[5*23, self.game_height - 101, 3],
                               [0, self.game_height - 6, 20],
                               [0, self.game_height + 17, 20],
                               [0, self.game_height + 40, 20],
                               [0, self.game_height + 63, 20],
                               [0, self.game_height + 86, 20],
                               [0, self.game_height + 109, 20],
                               [0, self.game_height + 132, 20],
                               [0, self.game_height + 155, 20],
                               [0, self.game_height + 178, 20],
                               [40*23, self.game_height + 6, 20],
                               [40*23, self.game_height + 29, 20],
                               [40*23, self.game_height + 52, 20],
                               [40*23, self.game_height + 75, 20],
                               [40*23, self.game_height + 98, 20],
                               [40*23, self.game_height + 121, 20],
                               [40*23, self.game_height + 144, 20],
                               [40*23, self.game_height + 167, 20],
                               [60*23, self.game_height - 109, 4],
                               [60*23, self.game_height - 86, 4],
                               [60*23, self.game_height - 63, 4],
                               [60*23, self.game_height - 40, 4],
                               [60*23, self.game_height - 17, 4],
                               [60*23, self.game_height + 6, 4],
                               [60*23, self.game_height + 29, 4],
                               [60*23, self.game_height + 52, 4],
                               [60*23, self.game_height + 75, 4],
                               [60*23, self.game_height + 98, 4],
                               [60*23, self.game_height + 121, 4],
                               [60*23, self.game_height + 144, 4],
                               [60*23, self.game_height + 167, 4],
                               [94*23, self.game_height + 6, 10],
                               [94*23, self.game_height + 29, 10],
                               [94*23, self.game_height + 52, 10],
                               [94*23, self.game_height + 75, 10],
                               [94*23, self.game_height + 98, 10],
                               [94*23, self.game_height + 121, 10],
                               [94*23, self.game_height + 144, 10],
                               [94*23, self.game_height + 167, 10],
                               [111*23, self.game_height - 120, 4],
                               [120*23, self.game_height - 20, 25],
                               [120*23, self.game_height + 3, 25],
                               [120*23, self.game_height + 26, 25],
                               [120*23, self.game_height + 49, 25],
                               [120*23, self.game_height + 72, 25],
                               [120*23, self.game_height + 95, 25],
                               [120*23, self.game_height + 118, 25],
                               [120*23, self.game_height + 141, 25],
                               [120*23, self.game_height + 164, 25],
                               [145*23, self.game_height - 89, 3],
                               [145*23, self.game_height - 66, 3],
                               [145*23, self.game_height - 43, 3],
                               [145*23, self.game_height - 20, 3],
                               [145*23, self.game_height + 3, 3],
                               [145*23, self.game_height + 26, 3],
                               [145*23, self.game_height + 49, 3],
                               [145*23, self.game_height + 72, 3],
                               [145*23, self.game_height + 95, 3],
                               [145*23, self.game_height + 118, 3],
                               [145*23, self.game_height + 141, 3],
                               [145*23, self.game_height + 164, 3],
                               [168*23, self.game_height - 20, 20],
                               [168*23, self.game_height + 3, 20],
                               [168*23, self.game_height + 26, 20],
                               [168*23, self.game_height + 49, 20],
                               [168*23, self.game_height + 72, 20],
                               [168*23, self.game_height + 95, 20],
                               [168*23, self.game_height + 118, 20],
                               [168*23, self.game_height + 141, 20],
                               [168*23, self.game_height + 146, 20],
                               [195*23, self.game_height - 24, 50],
                               [195*23, self.game_height - 1, 50],
                               [195*23, self.game_height + 22, 50],
                               [195*23, self.game_height + 45, 50],
                               [195*23, self.game_height + 68, 50],
                               [195*23, self.game_height + 91, 50],
                               [195*23, self.game_height + 114, 50],
                               [195*23, self.game_height + 137, 50],
                               [195*23, self.game_height + 160, 50],
                               [225*23, self.game_height - 469, 7],
                               [229*23, self.game_height - 446, 3],
                               [229*23, self.game_height - 423, 3],
                               [229*23, self.game_height - 400, 3],
                               [229*23, self.game_height - 377, 3],
                               [229*23, self.game_height - 354, 3],
                               [229*23, self.game_height - 331, 3],
                               [229*23, self.game_height - 308, 3],
                               [229*23, self.game_height - 285, 3],
                               [229*23, self.game_height - 262, 3],
                               [229*23, self.game_height - 239, 3],
                               [229*23, self.game_height - 216, 3],
                               [229*23, self.game_height - 193, 3],
                               [229*23, self.game_height - 170, 3],
                               [229*23, self.game_height - 147, 3],
                               #[229*23, self.game_height - 124, 3],
                               #[229*23, self.game_height - 101, 3],
                               ]

        spearman_parameters = [[300, self.game_height - 6],
                                [400, self.game_height - 6],
                                [960, self.game_height + 6],
                                [1000, self.game_height + 6],
                                [1420, self.game_height - 109],
                                [2200, self.game_height + 6],
                                [2650, self.game_height - 120], #115*23
                                [123*23, self.game_height - 20],
                                [125*23, self.game_height - 20],
                                [140*23, self.game_height - 20],
                                [144*23, self.game_height - 20],
                                #[170*23, self.game_height - 20],
                                [198*23, self.game_height - 24],
                                [201*23, self.game_height - 24],
                                [208*23, self.game_height - 24],
                                [212*23, self.game_height - 24],
                               ]

        archer_parameters = [[1030, self.game_height+6],
                             [1280, self.game_height+6],
                             [2280, self.game_height+6],
                             [2310, self.game_height+6],
                             [2360, self.game_height+6],
                             [130*23, self.game_height-20],
                             [133*23, self.game_height-20],
                             [138*23, self.game_height-20],
                             [171*23, self.game_height-20],
                             [172*23, self.game_height-20],
                             [175*23, self.game_height-20],
                             [177*23, self.game_height-20],
                             ]

        sand_parameters = [[20*23, self.game_height + 26, 20],
                           [20*23, self.game_height + 49, 20],
                           [20*23, self.game_height + 72, 20],
                           [20*23, self.game_height + 95, 20],
                           [20*23, self.game_height + 118, 20],
                           [20*23, self.game_height + 141, 20],
                           [20*23, self.game_height + 164, 20],
                           [64*23, self.game_height + 11, 30],
                           [64*23, self.game_height + 34, 30],
                           [64*23, self.game_height + 57, 30],
                           [64*23, self.game_height + 80, 30],
                           [64*23, self.game_height + 103, 30],
                           [64*23, self.game_height + 126, 30],
                           [64*23, self.game_height + 149, 30],
                           [64*23, self.game_height + 172, 30],
                           [148*23, self.game_height - 15, 20],
                           [148*23, self.game_height + 8, 20],
                           [148*23, self.game_height + 31, 20],
                           [148*23, self.game_height + 54, 20],
                           [148*23, self.game_height + 77, 20],
                           [148*23, self.game_height + 100, 20],
                           [148*23, self.game_height + 123, 20],
                           [148*23, self.game_height + 146, 20],
                           [148*23, self.game_height + 169, 20],]



        for p in platform_parameters:
            x, y, r = p

            for i in range(r):
                platform = Tile_Rock(x + 23 * i, y)
                #platform = Tile_Earth(x + 23 * i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)


        for p in spearman_parameters:
            spearman = Spearman(*p, self)
            self.all_sprites.add(spearman)
            self.spearmen.add(spearman)

        for p in archer_parameters:
            archer = Archer(*p, self)
            self.all_sprites.add(archer)
            self.archers.add(archer)

        for p in sand_parameters:
            x, y, r = p
            for i in range(r):
                sand = Tile_Sand(x + 23 * i, y)
                self.all_sprites.add(sand)
                self.sand_sprites.add(sand)


        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30+16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)


        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        self.last_threw_smoke_bomb = 0


        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def throw_smoke_bomb(self, x, y):

        self.last_threw_smoke_bomb = pg.time.get_ticks()
        smoke_bomb_x = x
        smoke_bomb_y = y
        x_dist = smoke_bomb_x - self.player.rect.centerx
        y_dist = smoke_bomb_y - self.player.rect.centery
        dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1

        smoke_bomb_move = [m for m in self.main_game.sml if _("Smoke Bomb") in m.name][0]
        smoke_bomb_level = smoke_bomb_move.level
        smoke_bomb_move.gain_exp(10)

        x_vel = x_dist / dist * randrange(10-(11-smoke_bomb_level)//2, 11) * (dist**.12)
        y_vel = y_dist / dist * randrange(10-(11-smoke_bomb_level)//2, 11) * (dist**.12)
        smoke_bomb = SmokeBomb(self.player.rect.centerx, self.player.rect.centery, x_vel, y_vel)
        self.all_sprites.add(smoke_bomb)
        self.smoke_bombs.add(smoke_bomb)


    def generate_projectile(self, x, y, speed, weapons):

        self.projectile_sound.play()
        weapon_choice = choice(weapons)
        if weapon_choice == 'Arrow':
            projectile = Projectile(x, y, speed, 0, "Arrow", "Sprite_Arrow.png", 50, 0, BLACK)
        self.all_sprites.add(projectile)
        self.projectiles.add(projectile)


    def generate_drop(self, x, y, force_gold=False):
        #print("Dropped item")

        if force_gold:
            drop = Drops(x, y, "Gold", "sprite_gold.png")
        elif randrange(5) == 0:
            drop = Drops(x, y, "Health Potion", "sprite_health_potion.png")
        else:
            drop = Drops(x, y, "Gold", "sprite_gold.png")

        self.drops.add(drop)
        self.all_sprites.add(drop)


    def pick_up_drop(self, drop):
        if drop.name == "Gold":
            self.main_game.currentEffect = "button-19.mp3"
            self.main_game.startSoundEffectThread()
            r = int(randrange(50, 101) * self.main_game.luck / 50)
            self.main_game.inv[_("Gold")] += r
            print(_("Picked up Gold x {}!").format(r))
        elif drop.name == "Health Potion":
            self.main_game.restore(h=self.main_game.healthMax*30//100,s=0,full=False)


    def update(self):

        self.all_sprites.update()
        # PLATFORM COLLISION
        on_screen_platforms = [platform for platform in self.platforms if
                               platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)
        # SAND COLLISION
        sand_collision = pg.sprite.spritecollide(self.player, self.sand_sprites, False)

        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 5 // 9 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x

        if sand_collision:
            self.player.vel.y *= .5
            current_height = sand_collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 600:
                damage = int(200 * 1.005 ** (fall_height - 600))
                print("Ouch! Lost {} health from fall.".format(damage))
                self.take_damage(damage)

            self.player.last_height = current_height
            self.player.jumping = False
            if self.main_game.stamina > 3:
                self.main_game.stamina -= 3
            else:
                self.take_damage(5, silent=True)


        #PROJECTILE COLLISION
        projectile_collision = pg.sprite.spritecollide(self.player, self.projectiles, True, pg.sprite.collide_circle_ratio(.75))
        if projectile_collision:
            self.take_damage(projectile_collision[0].damage)
            self.player.stun_count += projectile_collision[0].stun


        #drop COLLISION
        drop_collision = pg.sprite.spritecollide(self.player, self.drops, True, pg.sprite.collide_circle_ratio(.75))
        if drop_collision:
            drop = drop_collision[0]
            self.pick_up_drop(drop)


        # archer COLLISION
        archer_collision = pg.sprite.spritecollide(self.player, self.archers, True,
                                                   pg.sprite.collide_circle_ratio(.75))
        if archer_collision:
            self.running = False
            opp = character(_("Archers"), 5,
                            attributeList=[300,300,100,100,100,50,90,10,10],
                            sml=[special_move(_("Archery"), level=7)],
                            equipmentList=[Equipment(_("Golden Chainmail"))]
                            )

            drop_x = archer_collision[0].rect.x
            drop_y = archer_collision[0].rect.centery
            if self.player.rect.x > archer_collision[0].rect.x:
                drop_x -= 15
            else:
                drop_x += 15

            cmd = lambda: self.main_game.resume_mini_game("sea_bg.mp3", [drop_x, drop_y])
            self.main_game.battleMenu(self.main_game.you, opp, "battleground_trade_ship.png",
                                      postBattleSoundtrack="sea_bg.mp3",
                                      fromWin=None,
                                      battleType="task", destinationWinList=[] * 3,
                                      postBattleCmd=cmd
                                      )

        # spear COLLISION
        spearman_collision = pg.sprite.spritecollide(self.player, self.spearmen, True,
                                                     pg.sprite.collide_circle_ratio(.5))
        if spearman_collision:
            self.running = False
            opp = character(_("Spearmen"), 5,
                            attributeList=[500, 500, 140, 140, 60, 80, 70, 40, 10],
                            sml=[special_move(_("Spear Attack"), level=7)],
                            equipmentList=[Equipment(_("Golden Chainmail"))]
                            )

            drop_x = spearman_collision[0].rect.x
            drop_y = spearman_collision[0].rect.centery
            if self.player.rect.x > spearman_collision[0].rect.x:
                drop_x -= 15
            else:
                drop_x += 15

            cmd = lambda: self.main_game.resume_mini_game("sea_bg.mp3", [drop_x, drop_y])
            self.main_game.battleMenu(self.main_game.you, opp, "battleground_trade_ship.png",
                                      postBattleSoundtrack="sea_bg.mp3",
                                      fromWin=None,
                                      battleType="task", destinationWinList=[] * 3,
                                      postBattleCmd=cmd
                                      )

        # GOT GOODS
        if pg.sprite.spritecollide(self.player, self.chest_of_goods, True, pg.sprite.collide_circle_ratio(.75)):
            self.playing = False
            self.running = False
            self.main_game.mini_game_in_progress = False
            pg.display.quit()
            self.scene.obtainGoods()


        # SMOKE BOMB COLLISION

        for smoke_bomb in self.smoke_bombs:
            smoke_bomb_collision = pg.sprite.spritecollide(smoke_bomb, self.platforms, False, pg.sprite.collide_rect)
            if smoke_bomb_collision and not smoke_bomb.exploded:
                smoke_bomb.explode()
                self.smoke_bomb_sound.play()

            if smoke_bomb.exploded:
                pg.sprite.spritecollide(smoke_bomb, self.archers, True, pg.sprite.collide_circle_ratio(1.5))
                pg.sprite.spritecollide(smoke_bomb, self.spearmen, True, pg.sprite.collide_circle_ratio(1.5))


        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
                if type(platform) == Tile_Ice:
                    platform.dissolve_height += abs(round(self.player.vel.y))
            for archer in self.archers:
                archer.rect.y += abs(round(self.player.vel.y))
            for spearman in self.spearmen:
                spearman.rect.y += abs(round(self.player.vel.y))
            for sand in self.sand_sprites:
                sand.rect.y += abs(round(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y += abs(round(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y += abs(round(self.player.vel.y))
            for goods in self.chest_of_goods:
                goods.rect.y += abs(round(self.player.vel.y))
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.y += abs(round(self.player.vel.y))
            #self.background.rect.y += abs(round(self.player.vel.y)/4)
            self.screen_bottom += abs(round(self.player.vel.y))


        elif self.player.rect.bottom >= self.game_height * 1//2 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
                if type(platform) == Tile_Ice:
                    platform.dissolve_height += abs(round(self.player.vel.y))
            for archer in self.archers:
                archer.rect.y -= abs(round(self.player.vel.y))
            for spearman in self.spearmen:
                spearman.rect.y -= abs(round(self.player.vel.y))
            for sand in self.sand_sprites:
                sand.rect.y -= abs(round(self.player.vel.y))
            for projectile in self.projectiles:
                projectile.rect.y -= abs(round(self.player.vel.y))
            for drop in self.drops:
                drop.rect.y -= abs(round(self.player.vel.y))
            for goods in self.chest_of_goods:
                goods.rect.y -= abs(round(self.player.vel.y))
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.y -= abs(round(self.player.vel.y))
            #self.background.rect.y -= abs(round(self.player.vel.y) / 4)
            self.screen_bottom -= abs(round(self.player.vel.y))


        if self.player.pos.x >= self.game_width * 1 // 2 and self.player.vel.x > 0.5 and self.player.absolute_pos < int(self.game_width * 4.6):
            self.player.pos.x -= abs(self.player.vel.x)
            for platform in self.platforms:
                platform.rect.x -= abs(self.player.vel.x)
            for sand in self.sand_sprites:
                sand.rect.x -= abs(self.player.vel.x)
            for archer in self.archers:
                archer.rect.x -= abs(self.player.vel.x)
            for spearman in self.spearmen:
                spearman.rect.x -= abs(self.player.vel.x)
            for projectile in self.projectiles:
                projectile.rect.x -= abs(self.player.vel.x)
            for drop in self.drops:
                drop.rect.x -= abs(self.player.vel.x)
            for goods in self.chest_of_goods:
                goods.rect.x -= abs(self.player.vel.x)
            for smoke_bomb in self.smoke_bombs:
                smoke_bomb.rect.x -= abs(round(self.player.vel.x))

            self.background.rect.x -= abs(self.player.vel.x / 6)


        #SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom*1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()

            pos = pg.mouse.get_pos()
            if event.type == pg.MOUSEBUTTONUP and pg.time.get_ticks() - self.last_threw_smoke_bomb >= 1000 and self.player.stun_count==0:
                if self.main_game.check_for_item(_("Smoke Bomb")) and (_("Smoke Bomb") in [m.name for m in self.main_game.sml] or _("Superior Smoke Bomb") in [m.name for m in self.main_game.sml]):
                    self.main_game.add_item(_("Smoke Bomb"),-1)
                    self.throw_smoke_bomb(pos[0], pos[1])



    def take_damage(self, damage, silent=False):
        if not silent:
            self.ouch_sound.play()
        self.main_game.health -= damage
        if self.main_game.health <= 0:
            self.main_game.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump. Get past the guards to rob a chest of goods stored at the port.", 18, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("If you have Smoke Bombs and have learned the move, you can left click to throw a smoke bomb.", 18, WHITE, self.game_width // 2, self.game_height//2)
        self.draw_text("Press any key to begin.", 18, WHITE, self.game_width // 2, self.game_height*2//3)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)



if __name__ == "__main__":
    g = trade_ship_mini_game(None)
    g.new()
    pg.quit()
