from mini_game_sprites import*
from character import*
from skill import*
from special_move import*
from item import*
from gettext import gettext

_ = gettext

class winterberry_mini_game:
    def __init__(self, main_game, scene, training=True):
        self.main_game = main_game
        self.scene = scene
        self.training = training
        self.game_width, self.game_height = PORTRAIT_WIDTH, PORTRAIT_HEIGHT
        self.screen = pg.display.set_mode((self.game_width,self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        pg.font.init()
        self.initial_health = self.main_game.health
        self.initial_stamina = self.main_game.stamina


    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.winterberries = pg.sprite.Group()
        self.falling_platforms = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.snow_sprites = pg.sprite.Group()
        self.last_snow_animation = 0

        self.background = Static(0, self.game_height-1400, "snowy_mountain_bg.png")
        self.all_sprites.add(self.background)
        self.player = Player(self, self.game_width-200, self.game_height-100)
        self.all_sprites.add(self.player)
        self.winterberry = Static(30, self.game_height - 2520, "sprite_winterberry.png")
        self.winterberries.add(self.winterberry)
        self.all_sprites.add(self.winterberry)

        platform_parameters = [[0, self.game_height - 40, 30],
                               [30*23, self.game_height + 29, 10],
                               [100, self.game_height - 140, 5],
                               [500, self.game_height - 190, 3],
                               [300, self.game_height - 280, 2],
                               [200, self.game_height - 380, 3],
                               [60, self.game_height - 460, 3],
                               [170, self.game_height - 570, 3],
                               [300, self.game_height - 730, 4],
                               [560, self.game_height - 720, 3],
                               [740, self.game_height - 770, 2],
                               [600, self.game_height - 940, 3],
                               [730, self.game_height - 1110, 3],
                               [560, self.game_height - 1260, 3],
                               [240, self.game_height - 1230, 4],
                               [90, self.game_height - 1390, 3],
                               [10, self.game_height - 1550, 2],
                               [210, self.game_height - 1690, 2],
                               [350, self.game_height - 1850, 1],
                               [480, self.game_height - 1750, 7],
                               [280, self.game_height - 1950, 2],
                               [180, self.game_height - 2050, 2],
                               [30, self.game_height - 2170, 3],
                               [400, self.game_height - 2250, 8],
                               [700, self.game_height - 2330, 4],
                               [400, self.game_height - 2420, 2],
                               [200, self.game_height - 2450, 2],
                               [0, self.game_height - 2500, 4],
                               ]

        base_platform_parameters = [[0, self.game_height - 17, 30],
                                    [0, self.game_height + 6, 30],
                                    [0, self.game_height + 29, 30],
                                    [0, self.game_height + 52, 40],
                                    [0, self.game_height + 75, 40],
                                    [0, self.game_height + 98, 40],
                                    [0, self.game_height + 121, 40]]

        for p in platform_parameters:
            x, y, r = p
            if random() <= .25 and y <= self.game_height - 100:
                for i in range(r):
                    platform = Tile_Ice(x + 23 * i, y)
                    self.all_sprites.add(platform)
                    self.falling_platforms.add(platform)
                    self.platforms.add(platform)
            else:
                for i in range(r):
                    platform = Tile_Snow(x + 23 * i, y)
                    self.all_sprites.add(platform)
                    self.platforms.add(platform)



        for p in base_platform_parameters:
            x, y, r = p
            for i in range(r):
                platform = Tile_Rock(x + 23 * i, y)
                self.all_sprites.add(platform)
                self.platforms.add(platform)


        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        self.original_height = self.player.pos.y
        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            if self.running:
                self.update()
                self.clock.tick(FPS)
                self.events()
                self.draw()


    def update(self):

        self.all_sprites.update()

        #PLATFORM COLLISION
        collision = pg.sprite.spritecollide(self.player, self.platforms, False)
        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height * 3 // 5) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
                current_height = collision[0].rect.top
                fall_height = current_height - self.player.last_height
                self.player.last_height = current_height
                if fall_height >= 450:
                    damage = int(200 * 1.005 ** (fall_height - 450))
                    print("Ouch! Lost {} health from fall.".format(damage))
                    self.main_game.health -= damage
                    if self.main_game.health <= 0:
                        self.main_game.health = 0
                        self.playing = False
                        self.running = False
                        self.show_game_over_screen()

            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            for col in [platform for platform in self.platforms if platform.rect.left >= -100 and platform.rect.right <= self.game_width+100]:
                if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                        self.player.pos.x <= col.rect.left and \
                        self.player.pos.y - col.rect.top <= self.player.player_height and \
                        self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                    self.player.pos.x -= self.player.vel.x
                    self.player.absolute_pos -= self.player.vel.x
                elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                        self.player.pos.x >= col.rect.right and \
                        self.player.pos.y - col.rect.top <= self.player.player_height and \
                        self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                    self.player.pos.x -= self.player.vel.x
                    self.player.absolute_pos -= self.player.vel.x


            self.player.jumping = False
            self.player.friction = PLAYER_FRICTION/4


        else:
            self.player.friction = PLAYER_FRICTION

        #WINTERBERRY COLLISION
        winterberry_collision = pg.sprite.spritecollide(self.player, self.winterberries, True, pg.sprite.collide_circle_ratio(.75))
        if winterberry_collision:
            self.playing = False
            self.running = False
            self.scene.found_winterberry(self.training)

        # Check for return to original spot
        if self.player.pos.x >= self.game_width - 50 and self.player.vel.x > 0 and self.player.pos.y >= self.original_height and self.playing and self.running:
            self.playing = False
            self.running = False
            pg.display.quit()
            self.main_game.mini_game_in_progress = False
            self.main_game.shanhu_sect_win.deiconify()

        #FALLING PLATFORM COLLISION
        falling_platform_collision = pg.sprite.spritecollide(self.player, self.falling_platforms, False)
        if falling_platform_collision:
            falling_platform_collision[0].total_standing_time += 1
        else:
            self.reset_falling_platforms()


        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 4:
            self.player.pos.y += abs(round(self.player.vel.y))
            self.player.last_height += abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y += abs(round(self.player.vel.y))
                if type(platform) == Tile_Ice:
                    platform.dissolve_height += abs(round(self.player.vel.y))
            for winterberry in self.winterberries:
                winterberry.rect.y += abs(round(self.player.vel.y))
            for snow in self.snow_sprites:
                snow.rect.y += abs(round(self.player.vel.y))

            self.original_height += abs(round(self.player.vel.y))
            self.screen_bottom += abs(round(self.player.vel.y))
            self.background.rect.y += abs(round(self.player.vel.y)/8)


        elif self.player.rect.bottom >= self.game_height * 3 //4 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(round(self.player.vel.y))
            self.player.pos.y -= abs(round(self.player.vel.y))
            for platform in self.platforms:
                platform.rect.y -= abs(round(self.player.vel.y))
                if type(platform) == Tile_Ice:
                    platform.dissolve_height -= abs(round(self.player.vel.y))
            for winterberry in self.winterberries:
                winterberry.rect.y -= abs(round(self.player.vel.y))
            for snow in self.snow_sprites:
                snow.rect.y -= abs(round(self.player.vel.y))

            self.original_height -= abs(round(self.player.vel.y))
            self.screen_bottom -= abs(round(self.player.vel.y))
            self.background.rect.y -= abs(round(self.player.vel.y) / 8)


        #SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom*1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()


        now = pg.time.get_ticks()
        if now - self.last_snow_animation > 200:
            self.generate_snow(100)
            self.last_snow_animation = now


    def generate_snow(self, n):
        try:
            if self.playing and self.running:
                for i in range(n):
                    random_x = randrange(int(self.game_width*1.5))
                    random_y = randrange(-200,-100)
                    random_falling_speed = randrange(3, 8)
                    snow = Snow(random_x, random_y, random_falling_speed, self)
                    self.all_sprites.add(snow)
                    self.snow_sprites.add(snow)
        except:
            pass


    def reset_falling_platforms(self, value=0):
        for fp in self.falling_platforms:
            fp.total_standing_time = value


    def events(self):
        try:
            for event in pg.event.get():
                if event.type == pg.KEYDOWN:
                    if event.key == pg.K_UP and self.player.stun_count == 0:
                        self.player.jump()

                if event.type == pg.KEYUP:
                    if event.key == pg.K_UP and self.player.stun_count == 0:
                        self.player.jump_cut()
        except:
            pass

    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump. Look for the winterberry.", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit to main menu.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.main_game.health = int(self.initial_health)
                            self.main_game.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.main_game.display_lose_screen()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)


if __name__ == "__main__":
    g = winterberry_mini_game(None, None)
    g.show_start_screen()
    g.new()
    pg.quit()
