from tkinter import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
import tkinter.messagebox as messagebox

_ = gettext

class Scene_nongyuan_valley_train:
    def __init__(self, game):
        self.game = game
        self.game.days_spent_in_training = int((self.game.taskProgressDic["join_sect"] - 2) * 100)
        try:
            if self.game.taskProgressDic["join_sect"] == 2.0:
                try:
                    self.game.nongyuan_valley_train_win = Toplevel(self.game.scrubHouseWin)
                except:
                    self.game.nongyuan_valley_train_win = Toplevel(self.game.continueGameWin)

            else:
                self.game.nongyuan_valley_train_win = Toplevel(self.game.continueGameWin)
        except:
            self.game.nongyuan_valley_train_win = Toplevel(self.game.continueGameWin)

        self.game.nongyuan_valley_train_win.title(_("Nongyuan Valley"))

        bg = PhotoImage(file="Nongyuan Valley.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.nongyuan_valley_train_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Su Ling_icon_large.ppm")
        imageButton1 = Button(F1, command=partial(self.clickedOnSuLing, True))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("Su Ling"))
        label.grid(row=1, column=0)
        F1.place(x=w // 4 - pic1.width() // 2, y=h//4 -  pic1.height()//2)

        F2 = Frame(canvas)
        pic2 = PhotoImage(file="Li Jinyi_icon_large.ppm")
        imageButton2 = Button(F2, command=partial(self.clickedOnLiJinyi, True))
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(F2, text=_("Li Jinyi"))
        label.grid(row=1, column=0)
        F2.place(x=w * 3//4 - pic2.width() // 2, y=h//4 - pic2.height()//2)

        F3 = Frame(canvas)
        pic3 = PhotoImage(file="Xu Ting_icon_large.ppm")
        imageButton3 = Button(F3, command=partial(self.clickedOnXuTing, True))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        label = Label(F3, text=_("Xu Ting"))
        label.grid(row=1, column=0)
        F3.place(x=w // 4 - pic3.width() // 2, y=h*3//4 -  pic1.height()//2)


        F4 = Frame(canvas)
        pic4 = PhotoImage(file="forest_explore_icon_large.ppm")
        imageButton4 = Button(F4, command=self.nongyuan_valley_hunt)
        imageButton4.grid(row=0, column=0)
        imageButton4.config(image=pic4)
        label = Label(F4, text=_("Explore"))
        label.grid(row=1, column=0)
        F4.place(x=w * 3//4 - pic2.width() // 2, y=h*3//4 -  pic1.height()//2)


        self.game.days_spent_in_training_label = Label(self.game.nongyuan_valley_train_win,
                                                  text=_("Days trained: {}").format(self.game.days_spent_in_training))
        self.game.days_spent_in_training_label.pack()
        self.game.nongyuan_valley_train_win_F1 = Frame(self.game.nongyuan_valley_train_win)
        self.game.nongyuan_valley_train_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.nongyuan_valley_train_win,
                                            options=["Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.nongyuan_valley_train_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.nongyuan_valley_train_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.nongyuan_valley_train_win.winfo_width(), self.game.nongyuan_valley_train_win.winfo_height()
        self.game.nongyuan_valley_train_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.nongyuan_valley_train_win.focus_force()
        self.game.nongyuan_valley_train_win.mainloop()###


    def clickedOnSuLing(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.nongyuan_valley_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.nongyuan_valley_train_win_F1, text=_("Spar"),
                   command=partial(self.nongyuan_valley_train_spar, targ=_("Su Ling"))).grid(row=1, column=0)
            Button(self.game.nongyuan_valley_train_win_F1, text=_("Learn"),
                   command=partial(self.nongyuan_valley_train_learn, learnFrom = _("Su Ling"))).grid(row=2, column=0)
            Button(self.game.nongyuan_valley_train_win_F1, text=_("Heal"),
                   command=self.nongyuan_valley_train_heal).grid(row=3, column=0)

        else:
            if self.game.taskProgressDic["nongyuan_valley"] == 10:
                if self.game.check_for_item(_("Snow Lotus"), 10):
                    self.game.add_item(_("Snow Lotus"), -10)
                    self.game.taskProgressDic["nongyuan_valley"] = 100
                    self.game.generate_dialogue_sequence(
                        None,
                        "Nongyuan Valley.png",
                        [_("Here! 10 Snow lotuses!"),
                         _("Excellent! Thank you! With this, the patients in critical condition will be saved!"),
                         _("But what about the hundreds of others?"),
                         _("Ahahaha! No worries, lad! I've got them covered! My method worked!"),
                         _("Of course, the recovery time is far slower than snow lotus, but I can treat far more patients than her!"),
                         _("Don't forget, your method is only effective on those who do not have severe symptoms."),
                         _("Fine... we'll call it a tie this time..."), #Row 2
                         _("Ayyy come on, guys, stop arguing. I think you two worked together wonderfully as a team!"),
                         _("Speaking of team, you contributed quite a lot too, young man."),
                         _("Allow me to repay you for your hard efforts.")],
                        ["you", "Su Ling", "you", "Li Jinyi", "Li Jinyi", "Su Ling",
                         "Li Jinyi", "you", "Su Ling", "Su Ling"]
                    )
                    if _("Recuperation") in [s.name for s in self.game.skills]:
                        self.game.add_item(_("Big Recovery Pill"), 10)
                        self.game.add_item(_("Bezoar Pill"), 10)
                        messagebox.showinfo("", _("Su Ling gives you 10 Big Recovery Pills and 10 Bezoar Pills!"))
                    else:
                        self.game.learn_skill(skill(_("Recuperation")))
                        messagebox.showinfo("", _("Su Ling taught you a new skill: Recuperation!"))

                else:
                    self.game.generate_dialogue_sequence(
                        self.game.nongyuan_valley_win,
                        "Nongyuan Valley.png",
                        [_("Can you remind me what you need please?"),
                         _("Sure, I need 10 snow lotuses, as soon as possible!"),
                         _("No problem! I'm on my way now!"),
                         _("Thanks!")],
                        ["you", "Su Ling", "you", "Su Ling"]
                    )
                return
            
            for widget in self.game.nongyuan_valley_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.nongyuan_valley_win_F1, text=_("Spar"),
                   command=partial(self.nongyuan_valley_train_spar, targ=_("Su Ling"), training=False)).grid(row=1, column=0)
            Button(self.game.nongyuan_valley_win_F1, text=_("Shop"),
                   command=self.su_ling_shop).grid(row=2, column=0)
            if self.game.taskProgressDic['xiao_han_jia_wen'] == 10:
                Button(self.game.nongyuan_valley_win_F1, text=_("Ask for Snow Lotus Pill"),
                       command=partial(self.talk_to_su_ling, "Snow Lotus")).grid(row=3, column=0)
            elif self.game.taskProgressDic['xiao_han_jia_wen'] == 11:
                Button(self.game.nongyuan_valley_win_F1, text=_("Ask for Snow Lotus Pill"),
                       command=partial(self.talk_to_su_ling, "Snow Lotus Repeat")).grid(row=3, column=0)
            elif self.game.taskProgressDic['xiao_han_jia_wen'] in list(range(12,20)):
                Button(self.game.nongyuan_valley_win_F1, text=_("I've got the Snow Lotus!"),
                       command=partial(self.talk_to_su_ling, "Snow Lotus Obtained")).grid(row=3, column=0)


    def talk_to_su_ling(self, choice):
        if choice == "Snow Lotus":
            self.game.generate_dialogue_sequence(
                self.game.nongyuan_valley_win,
                "Nongyuan Valley.png",
                [_("Hi, Doctor Su Ling, I am in desperate need of a Snow Lotus Pill."),
                 _("A little girl is dying, and this is the only thing that can save her."),
                 _("I'm sorry, but the snow lotus is very difficult to obtain, and I happen to be out of stock."),
                 _("If you can bring me some snow lotus, I can make the pill for you free of charge."),
                 _("And where can I go to find a snow lotus?"),
                 _("If you travel Northwest from here to the mountains, you should be able to find someone named Xiao Yong."),
                 _("He will be able to help you."),
                 _("Ok, thank you so much!")],
                ["you", "you", "Su Ling", "Su Ling", "you", "Su Ling", "Su Ling", "you"]
            )
            self.game.taskProgressDic['xiao_han_jia_wen'] = 11
            self.clickedOnSuLing()

        elif choice == "Snow Lotus Repeat":
            self.game.generate_dialogue_sequence(
                self.game.nongyuan_valley_win,
                "Nongyuan Valley.png",
                [_("Do you have the Snow Lotus yet?"),
                 _("Ah, not yet, working on it... Can you remind me where to look again?"),
                 _("Sure, go to the Snow Mountains Northwest of here."),
                 _("Got it, thanks!")],
                ["Su Ling", "you", "Su Ling", "you"]
            )

        elif choice == "Snow Lotus Obtained":

            if self.game.inv[_("Snow Lotus")] > 0:
                self.game.generate_dialogue_sequence(
                    self.game.nongyuan_valley_win,
                    "Nongyuan Valley.png",
                    [_("Great! Let me make the Snow Lotus Pill for you..."),
                     _("Thank you!")],
                    ["Su Ling", "you"]
                )
                self.game.inv[_("Snow Lotus")] -= 1
                if _("Snow Lotus Pill") in self.game.inv:
                    self.game.inv[_("Snow Lotus Pill")] += 1
                else:
                    self.game.inv[_("Snow Lotus Pill")] = 1

                if self.game.taskProgressDic["xiao_han_jia_wen"] == 12:
                    self.game.taskProgressDic["xiao_han_jia_wen"] = 13

                messagebox.showinfo("", _("Received Snow Lotus Pill x 1!"))

            else:
                self.game.generate_dialogue_sequence(
                    self.game.nongyuan_valley_win,
                    "Nongyuan Valley.png",
                    [_("Oh just kidding... I thought I had it haha... nevermind..."),
                     _("........")],
                    ["you", "Su Ling"]
                )


    def clickedOnLiJinyi(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.nongyuan_valley_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.nongyuan_valley_train_win_F1, text=_("Ask for task"),
                   command=partial(self.nongyuan_valley_train_task)).grid(row=1, column=0)
            Button(self.game.nongyuan_valley_train_win_F1, text=_("Learn"),
                   command=partial(self.nongyuan_valley_train_learn, learnFrom=_("Li Jinyi"))).grid(row=2, column=0)

        else:
            if self.game.taskProgressDic["nongyuan_valley"] == 20:
                condition1 = self.game.check_for_item(_("Poison Ivy"), 5)
                condition2 = self.game.check_for_item(_("Stinging Nettle"), 5)
                condition3 = self.game.check_for_item(_("Spiral Aloe Leaf"), 5)
                condition4 = self.game.check_for_item(_("White Snakeroot"), 3)
                condition5 = self.game.check_for_item(_("Elderberry"), 3)
                if condition1 and condition2 and condition3 and condition4 and condition5:
                    self.game.taskProgressDic["nongyuan_valley"] = 200
                    self.game.add_item(_("Poison Ivy"), -5)
                    self.game.add_item(_("Stinging Nettle"), -5)
                    self.game.add_item(_("Spiral Aloe Leaf"), -5)
                    self.game.add_item(_("White Snakeroot"), -3)
                    self.game.add_item(_("Elderberry"), -3)
                    self.game.generate_dialogue_sequence(
                        None,
                        "Nongyuan Valley.png",
                        [_("Here! I've found everything you need!"),
                         _("Excellent! Thank you! With this, I will be able to cure a few hundred patients in no time!"),
                         _("Of course, the recovery time is far slower than snow lotus, but I can treat far more patients than my wife!"),
                         _("Don't forget, your method is only effective on those who do not have severe symptoms."),
                         _("Those who are in critical condition will still require the snow lotus..."),
                         _("Fine... we'll call it a tie this time..."),  # Row 2
                         _("Ayyy come on, guys, stop arguing. I think you two worked together wonderfully as a team!"),
                         _("Speaking of team, you contributed quite a lot too, young lad."),
                         _("Allow me to repay you for your hard efforts.")],
                        ["you", "Li Jinyi", "Li Jinyi", "Su Ling", "Su Ling",
                         "Li Jinyi", "you", "Li Jinyi", "Li Jinyi"]
                    )

                    if _("Countering Poison With Poison") not in [s.name for s in self.game.skills] and self.game.luck >= 100:
                        self.game.learn_skill(skill(_("Countering Poison With Poison"), level=10))
                        messagebox.showinfo("", _("Li Jinyi teaches you a new skill: Countering Poison With Poison!"))
                    elif _("Spider Poison Powder") not in [s.name for s in self.game.skills]:
                        self.game.learn_move(special_move(_("Spider Poison Powder")))
                        self.game.learn_move(special_move(_("Centipede Poison Powder")))
                        messagebox.showinfo("", _("Li Jinyi teaches you how to use Spider Poison Powder and Centipede Poison Powder!"))
                    else:
                        self.game.add_item(_("Spider Poison Powder"), 100)
                        self.game.add_item(_("Centipede Poison Powder"), 100)
                        messagebox.showinfo("", _("Li Jinyi gives you 'Spider Poison Powder' x 100 and 'Centipede Poison Powder' x 100!"))

                else:
                    self.game.generate_dialogue_sequence(
                        self.game.nongyuan_valley_win,
                        "Nongyuan Valley.png",
                        [_("Can you remind me what you need please?"),
                         _("Sure, listen carefully now. I need:"),
                         _("'Poison Ivy' x 5, 'Stinging Nettle' x 5, 'Spiral Aloe Leaf' x 5, 'White Snakeroot' x 3, 'Elderberry' x 3"),
                         _("You should find plenty of these growing on the cliffs of Eagle Sect, which is only a short distance away."),
                         _("No problem! I'll go look for these right away!"),
                         _("Thanks, lad! You be careful!")],
                        ["you", "Li Jinyi", "Li Jinyi", "Li Jinyi", "you", "Li Jinyi"]
                    )
                return

            for widget in self.game.nongyuan_valley_win_F1.winfo_children():
                widget.destroy()
            Button(self.game.nongyuan_valley_win_F1, text=_("Sell venomous spiders (50 gold ea)."),
                   command=partial(self.nongyuan_valley_train_task, 11)).grid(row=1, column=0)
            Button(self.game.nongyuan_valley_win_F1, text=_("Sell venomous centipedes (50 gold ea)."),
                   command=partial(self.nongyuan_valley_train_task, 21)).grid(row=2, column=0)
            if self.game.taskProgressDic["join_sect"] == 300:
                Button(self.game.nongyuan_valley_win_F1, text=_("Ask for task"),
                       command=partial(self.nongyuan_valley_train_task, 30)).grid(row=3, column=0)


    def clickedOnXuTing(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.nongyuan_valley_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.nongyuan_valley_train_win_F1, text=_("Spar"),
                   command=partial(self.nongyuan_valley_train_spar, targ=_("Xu Ting"))).grid(row=1, column=0)
            Button(self.game.nongyuan_valley_train_win_F1, text=_("What are you up to?"),
                   command=partial(self.talk_to_xu_ting, 0)).grid(row=2, column=0)

        else:
            for widget in self.game.nongyuan_valley_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.nongyuan_valley_win_F1, text=_("Spar"),
                   command=partial(self.nongyuan_valley_train_spar, targ=_("Xu Ting"), training=False)).grid(row=1, column=0)
            if self.game.taskProgressDic["join_sect"] == 300:
                Button(self.game.nongyuan_valley_win_F1, text=_("Heeyyyy, guess who's back?~"),
                           command=partial(self.talk_to_xu_ting, 1)).grid(row=2, column=0)
            else:
                Button(self.game.nongyuan_valley_win_F1, text=_("Who are you?"),
                           command=partial(self.talk_to_xu_ting, 2)).grid(row=2, column=0)


    def talk_to_xu_ting(self, choice=0):

        if choice == 0:
            self.game.generate_dialogue_sequence(
                self.game.nongyuan_valley_train_win,
                "Nongyuan Valley.png",
                [_("Being busy, duh~~"),
                 _("Whoaaa, so sassy!"),
                 _("Hahaha, I'm just messing with ya~ I'm trying to learn about spider and centipede venom..."),
                 _("Oooh, sounds fun but dangerous... you be careful, kiddo~"),
                 _("I know, I know... You better worry about yourself... I'm gonna drop a spider on your face while you sleep ehehehehe..."),
                 _(".........")],
                ["Xu Ting", "you", "Xu Ting", "you", "Xu Ting", "you"]
            )

        elif choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.nongyuan_valley_win,
                "Nongyuan Valley.png",
                [_("Oh, look, it's you, loser!"),
                 _("Hehe~ didn't get enough butt kicking while you were training with us? Back for more?"),
                 _("Hahaha, sassy as always, eh?"),
                 _("Ehehehe, welcome back, {}!").format(self.game.character),
                 _("Nice seeing ya, kiddo!")],
                ["Xu Ting", "Xu Ting", "you", "Xu Ting", "you"]
            )

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.nongyuan_valley_win,
                "Nongyuan Valley.png",
                [_("Hi there, I'm Doctor Su Ling's disciple!"),
                 _("Who are you, and why are you here?"),
                 _("I uh... I'm just a traveler... Happened to pass by is all..."),
                 _("What sect are you from? What moves have you learned? Do you like spiders?"),
                 _("Don't ask so many questions, kiddo... Being too curious can get you into trouble sometimes.")],
                ["Xu Ting", "Xu Ting", "you", "Xu Ting", "you"]
            )


    def nongyuan_valley_train_heal(self):
        messagebox.showinfo("", _("Su Ling restores you to full health and stamina."))
        self.game.restore(0,0,True)
        self.nongyuan_valley_training_update(.01)


    def nongyuan_valley_train_learn(self, learnFrom):

        if learnFrom == "Li Jinyi":
            try:
                if self.game.inv[_("Spider Poison Powder")] >= 3 and self.game.inv[_("Centipede Poison Powder")] >= 3:
                    if _("Spider Poison Powder") not in [s.name for s in self.game.sml]:
                        self.game.generate_dialogue_sequence(
                            self.game.nongyuan_valley_train_win,
                            "Nongyuan Valley.png",
                            [_("Oh right, I've been asking you to collect these spiders and centipedes for me but never taught you what to do with them..."),
                             _("Here, let me teach you how to use the poison powders so you have a way of defending yourself.")],
                            ["Li Jinyi", "Li Jinyi"]
                        )
                        self.game.learn_move(special_move(_("Spider Poison Powder")))
                        self.game.learn_move(special_move(_("Centipede Poison Powder")))
                        messagebox.showinfo("", _("Li Jinyi teaches you new moves: Spider Poison Powder & Centipede Poison Powder!"))


                    else:
                        self.game.generate_dialogue_sequence(
                            self.game.nongyuan_valley_train_win,
                            "Nongyuan Valley.png",
                            [_("I don't have anything else to teach you at the moment.")],
                            ["Li Jinyi"]
                        )
                        return

                else:
                    self.game.generate_dialogue_sequence(
                        self.game.nongyuan_valley_train_win,
                        "Nongyuan Valley.png",
                        [_("I don't have anything else to teach you at the moment.")],
                        ["Li Jinyi"]
                    )
                    return

            except:
                self.game.generate_dialogue_sequence(
                    self.game.nongyuan_valley_train_win,
                    "Nongyuan Valley.png",
                    [_("I don't have anything to teach you at the moment. Come back to me later.")],
                    ["Li Jinyi"]
                )

        else:
            increment = randrange(10,31)
            self.game.staminaMax += increment
            self.game.healthMax += increment
            messagebox.showinfo("", _("You spend 2 days learning about various medicine. As you taste various medicine and their effects, your health upper limit increases by {}.").format(increment))

            if random() <= .1 + self.game.luck/1000 and _("Body Cleansing Technique") not in [m.name for m in self.game.sml]:
                self.game.generate_dialogue_sequence(
                    self.game.nongyuan_valley_train_win,
                    "Nongyuan Valley.png",
                    [_("I see you've been studying very diligently in the past few days."),
                     _("Let me teach you something that will help you to recover from illnesses.")],
                    ["Su Ling", "Su Ling"]
                )

                new_move = special_move(_("Body Cleansing Technique"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("Su Ling taught you a new skill: Body Cleansing Technique!"))

        self.nongyuan_valley_training_update(.02)


    def nongyuan_valley_train_task(self, choice=0):

        if choice == 0:
            self.game.generate_dialogue_sequence(
                self.game.nongyuan_valley_train_win,
                "Nongyuan Valley.png",
                [_("I'm looking for 3 venomous spiders and/or centipedes."),
                 _("If you could find some for me, that would save me a lot of time."),],
                ["Li Jinyi", "Li Jinyi"],
                [[_("Give 3 venomous spiders to Li Jinyi."), partial(self.nongyuan_valley_train_task,1)],
                 [_("Give 3 venomous centipedes to Li Jinyi."), partial(self.nongyuan_valley_train_task,2)],
                 [_("Come back later..."), partial(self.nongyuan_valley_train_task,-1)]]
            )
            return


        elif choice == 1:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            if _("Venomous Spider") in self.game.inv:
                if self.game.inv[_("Venomous Spider")] >= 3:
                    self.game.inv[_("Venomous Spider")] -= 3
                    if _("Spider Poison Powder") in self.game.inv:
                        self.game.inv[_("Spider Poison Powder")] += 1
                    else:
                        self.game.inv[_("Spider Poison Powder")] = 1
                    self.game.generate_dialogue_sequence(
                        self.game.nongyuan_valley_train_win,
                        "Nongyuan Valley.png",
                        [_("Thanks, young lad! Here, take this jar of Spider Poison Powder as a small token of appreciation."), ],
                        ["Li Jinyi"]
                    )
                    print(_("Received item: Spider Poison Powder x 1"))

                else:
                    messagebox.showinfo("", _("Not enough Venomous Spiders in your inventory."))
            else:
                messagebox.showinfo("", _("Not enough Venomous Spiders in your inventory."))

        elif choice == 2:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            if _("Venomous Centipede") in self.game.inv:
                if self.game.inv[_("Venomous Centipede")] >= 3:
                    self.game.inv[_("Venomous Centipede")] -= 3
                    if _("Centipede Poison Powder") in self.game.inv:
                        self.game.inv[_("Centipede Poison Powder")] += 1
                    else:
                        self.game.inv[_("Centipede Poison Powder")] = 1

                    self.game.generate_dialogue_sequence(
                        self.game.nongyuan_valley_train_win,
                        "Nongyuan Valley.png",
                        [_("Thanks, young lad! Here, take this jar of Centipede Poison Powder as a small token of appreciation."), ],
                        ["Li Jinyi"]
                    )
                    print(_("Received item: Centipede Poison Powder x 1"))

                else:
                    messagebox.showinfo("", _("Not enough Venomous Spiders in your inventory."))

            else:
                messagebox.showinfo("", _("Not enough Venomous Spiders in your inventory."))


        elif choice == 11:
            if _("Venomous Spider") in self.game.inv:
                if self.game.inv[_("Venomous Spider")] > 0:
                    amount = self.game.inv[_("Venomous Spider")]*50
                    self.game.inv[_("Venomous Spider")] = 0
                    self.game.inv[_("Gold")] += amount
                    self.game.generate_dialogue_sequence(
                        self.game.nongyuan_valley_win,
                        "Nongyuan Valley.png",
                        [_("Ahoy there, lad! Those are some fine specimen you have! It's a deal!")],
                        ["Li Jinyi"]
                    )
                    messagebox.showinfo("", _("Received Gold x {}!").format(amount))

                else:
                    messagebox.showinfo("", _("You don't have any Venomous Spiders to sell."))

            else:
                messagebox.showinfo("", _("You don't have any Venomous Spiders to sell."))

            return

        elif choice == 21:
            if _("Venomous Centipede") in self.game.inv:
                if self.game.inv[_("Venomous Centipede")] > 0:
                    amount = self.game.inv[_("Venomous Centipede")] * 50
                    self.game.inv[_("Venomous Centipede")] = 0
                    self.game.inv[_("Gold")] += amount
                    self.game.generate_dialogue_sequence(
                        self.game.nongyuan_valley_win,
                        "Nongyuan Valley.png",
                        [_("Ahoy there, lad! Those are some fine specimen you have! It's a deal!")],
                        ["Li Jinyi"]
                    )
                    messagebox.showinfo("", _("Received Gold x {}!").format(amount))

                else:
                    messagebox.showinfo("", _("You don't have any Venomous Spiders to sell."))

            else:
                messagebox.showinfo("", _("You don't have any Venomous Spiders to sell."))
            return


        elif choice == 30:
            self.game.generate_dialogue_sequence(
                self.game.nongyuan_valley_win,
                "Nongyuan Valley.png",
                [_("I'm looking for 3 venomous spiders and/or centipedes."),
                 _("If you could find some for me, that would save me a lot of time."),],
                ["Li Jinyi", "Li Jinyi"],
                [[_("Give 3 venomous spiders to Li Jinyi."), partial(self.nongyuan_valley_train_task,31)],
                 [_("Give 3 venomous centipedes to Li Jinyi."), partial(self.nongyuan_valley_train_task,32)],
                 [_("Come back later..."), partial(self.nongyuan_valley_train_task,-31)]]
            )
            return

        elif choice == 31:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            if _("Venomous Spider") in self.game.inv:
                if self.game.inv[_("Venomous Spider")] >= 3:
                    self.game.inv[_("Venomous Spider")] -= 3
                    if _("Spider Poison Powder") in self.game.inv:
                        self.game.inv[_("Spider Poison Powder")] += 1
                    else:
                        self.game.inv[_("Spider Poison Powder")] = 1
                    self.game.generate_dialogue_sequence(
                        self.game.nongyuan_valley_win,
                        "Nongyuan Valley.png",
                        [_("Thanks, young lad! Here, take this jar of Spider Poison Powder as a small token of appreciation."), ],
                        ["Li Jinyi"]
                    )
                    print(_("Received item: Spider Poison Powder x 1"))

                else:
                    messagebox.showinfo("", _("Not enough Venomous Spiders in your inventory."))
            else:
                messagebox.showinfo("", _("Not enough Venomous Spiders in your inventory."))

        elif choice == 32:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            if _("Venomous Centipede") in self.game.inv:
                if self.game.inv[_("Venomous Centipede")] >= 3:
                    self.game.inv[_("Venomous Centipede")] -= 3
                    if _("Centipede Poison Powder") in self.game.inv:
                        self.game.inv[_("Centipede Poison Powder")] += 1
                    else:
                        self.game.inv[_("Centipede Poison Powder")] = 1

                    self.game.generate_dialogue_sequence(
                        self.game.nongyuan_valley_win,
                        "Nongyuan Valley.png",
                        [_("Thanks, young lad! Here, take this jar of Centipede Poison Powder as a small token of appreciation."), ],
                        ["Li Jinyi"]
                    )
                    print(_("Received item: Centipede Poison Powder x 1"))

                else:
                    messagebox.showinfo("", _("Not enough Venomous Spiders in your inventory."))

            else:
                messagebox.showinfo("", _("Not enough Venomous Spiders in your inventory."))


        elif choice == -31:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.nongyuan_valley_win.deiconify()
            return

        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        if choice >= 30:
            self.game.nongyuan_valley_win.deiconify()
        else:
            self.game.nongyuan_valley_train_win.deiconify()


    def nongyuan_valley_train_spar(self, targ, training=True):

        if training:
            if self.game.taskProgressDic["join_sect"] < 100 and self.game.taskProgressDic["join_sect"]-2 < .59 and random() <= .5:
                self.nongyuan_valley_training_update(.01)

        if targ == _("Xu Ting"):
            if training:
                opp = character(_("Xu Ting"), min([7, self.game.level]),
                      sml=[special_move(_("Spider Poison Powder")),
                           special_move(_("Centipede Poison Powder")),
                           special_move(_("Basic Sword Technique"), level = min([10,self.game.level])),
                           special_move(_("Basic Punching Technique"), level = min([10,self.game.level]))],
                      skills=[skill(_("Countering Poison With Poison"), level=10)])

            else:
                opp = character(_("Xu Ting"), 9,
                      sml=[special_move(_("Spider Poison Powder")),
                           special_move(_("Centipede Poison Powder")),
                           special_move(_("Basic Sword Technique"), level = 10),
                           special_move(_("Basic Punching Technique"), level = 10)],
                      skills=[skill(_("Countering Poison With Poison"), level=10)])

        elif targ == _("Su Ling"):
            if training:
                opp = character(_("Su Ling"), 20,
                                sml=[special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Basic Punching Technique"), level=10)],
                                skills=[skill(_("Recuperation"), level=10)],
                                preferences = [1,1,3,3,3])
            else:
                opp = character(_("Su Ling"), 25,
                                sml=[special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Basic Punching Technique"), level=10),
                                     special_move(_("Body Cleansing Technique"), level=10)],
                                skills=[skill(_("Recuperation"), level=10)],
                                preferences = [1,1,3,3,3])


        if training:
            self.game.battleMenu(self.game.you, opp,"battleground_nongyuan_valley.png",
                            "jy_huanle.mp3",fromWin = self.game.nongyuan_valley_train_win,
                            battleType = "training", destinationWinList = [self.game.nongyuan_valley_train_win] * 3)
        else:
            self.game.battleMenu(self.game.you, opp, "battleground_nongyuan_valley.png",
                            "jy_huanle.mp3", fromWin=self.game.nongyuan_valley_win,
                            battleType="training", destinationWinList=[self.game.nongyuan_valley_win] * 3)


    def nongyuan_valley_hunt(self):
        self.game.hunt(self.game.nongyuan_valley_train_win)
        self.nongyuan_valley_training_update(.01)


    def nongyuan_valley_training_update(self, increment):
        self.game.taskProgressDic["join_sect"] += increment
        self.game.taskProgressDic["join_sect"] = round(self.game.taskProgressDic["join_sect"],2)
        self.game.days_spent_in_training = int(self.game.taskProgressDic["join_sect"]*100 - 200)
        self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
        if self.game.days_spent_in_training >= 60:
            self.game.days_spent_in_training = 60
            self.game.taskProgressDic["join_sect"] = 2.6
            self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
            self.game.nongyuan_valley_train_win.withdraw()

            if self.game.luck >= 80:
                new_move = special_move(name=_("Recuperation"))
                print(_("You learned a new skill: Recuperation!"))
                self.game.learn_move(new_move)

                self.game.generate_dialogue_sequence(
                    None,
                    "Nongyuan Valley.png",
                    [_("Unfortunately, our time together must come to an end."),
                     _("I hope you've learned a lot here. Before you go, please take these Snow Lotus Pills."),
                     _("Also, let me teach you some advanced breathing techniques to help you recover faster while resting.")],
                    ["Su Ling", "Su Ling", "Su Ling"]
                )
                messagebox.showinfo("", _("Received Snow Lotus Pill x 3!\nLearned new skill: Recuperation!"))

                self.game.generate_dialogue_sequence(
                    None,
                    "Nongyuan Valley.png",
                    [_("Thank you so much Doctor Su! Also, thank you Uncle Li and Junior Sister Xu for taking care of me."),
                     _("No problem, lad. We enjoyed your company!"),
                     _("It was great to have you with us!"),
                     _("Keep yourself safe and healthy out there, and come back to visit every now and then!"),
                     _("I will! Take care, guys!"),],
                    ["you", "Li Jinyi", "Xu Ting", "Su Ling", "you"]
                )

            elif self.game.luck >= 75:
                new_move = special_move(name=_("Countering Poison With Poison"), level=10)
                print(_("You learned a new move: Countering Poison With Poison!"))
                self.game.learn_move(new_move)

                self.game.generate_dialogue_sequence(
                    None,
                    "Nongyuan Valley.png",
                    [_("Unfortunately, our time together must come to an end."),
                     _("I hope you've learned a lot here. Before you go, please take these Snow Lotus Pills."),
                     _("Hey lad, let me teach you something to help you deal with villains out there who might try to harm you.")],
                    ["Su Ling", "Su Ling", "Li Jinyi"]
                )
                messagebox.showinfo("", _("Received Snow Lotus Pill x 3!\nLearned new move: Countering Poison With Poison!"))

                self.game.generate_dialogue_sequence(
                    None,
                    "Nongyuan Valley.png",
                    [_("Thank you so much Doctor Su and Uncle Li! Thanks Sister Xu for taking care of me."),
                     _("No problem, lad. We enjoyed your company!"),
                     _("It was great to have you with us!"),
                     _("Keep yourself safe and healthy out there, and come back to visit every now and then!"),
                     _("I will! Take care, guys!"), ],
                    ["you", "Li Jinyi", "Xu Ting", "Su Ling", "you"]
                )

            else:

                self.game.generate_dialogue_sequence(
                    None,
                    "Nongyuan Valley.png",
                    [_("Unfortunately, our time together must come to an end."),
                     _("I hope you've learned a lot here. Before you go, please take these Snow Lotus Pills.")],
                    ["Su Ling", "Su Ling"]
                )
                messagebox.showinfo("", _("Received Snow Lotus Pill x 3!"))

                self.game.generate_dialogue_sequence(
                    None,
                    "Nongyuan Valley.png",
                    [_("Thank you so much Doctor Su! Also, thank you Uncle Li and Sister Xu for taking care of me."),
                     _("No problem, lad. We enjoyed your company!"),
                     _("It was great to have you with us!"),
                     _("Keep yourself safe and healthy out there, and come back to visit every now and then!"),
                     _("I will! Take care, guys!")],
                    ["you", "Li Jinyi", "Xu Ting", "Su Ling", "you"]
                )

            self.game.inv[_("Snow Lotus Pill")] = 3
            self.game.taskProgressDic["join_sect"] = 300
            self.game.nongyuan_valley_train_win.quit()
            self.game.nongyuan_valley_train_win.destroy()
            self.game.generateGameWin()


    def su_ling_shop(self):

        self.su_ling_shop_prices = {
            _("Small Recovery Pill"): 600,
            _("Big Recovery Pill"): 2000,
            _("Bezoar Pill"): 200,
            _("Thousand Year Ginseng Extract"): 15000,
        }
        self.game.selected_shop_item = None
        self.game.nongyuan_valley_win.withdraw()
        self.game.nongyuan_valley_menu_win = Toplevel(self.game.nongyuan_valley_win)

        F1 = Frame(self.game.nongyuan_valley_menu_win, borderwidth=2, relief=SUNKEN, padx=5, pady=5)
        F1.pack()

        self.game.shop_item_buttons = []
        self.game.shop_item_icons = []
        self.game.item_price_labels = []
        self.game.selected_shop_item = None
        self.game.selected_shop_item_index = None


        item_x = 0
        item_y = 0
        item_index = 0
        for item_name in self.su_ling_shop_prices:
            item_icon = PhotoImage(file=_("i_{}.ppm").format(item_name))
            item_button = Button(F1, image=item_icon, command=partial(self.game.selectShopItem, item_name, item_index))
            item_button.image = item_icon
            item_button.grid(row=item_y, column=item_x)

            item_description = item_name + "\n------------------------------\n" + ITEM_DESCRIPTIONS[item_name]
            self.game.balloon.bind(item_button, item_description)
            self.game.shop_item_icons.append(item_icon)
            self.game.shop_item_buttons.append(item_button)

            price_label = Label(text="Price:" + str(self.su_ling_shop_prices[item_name]), master=F1)
            price_label.grid(row=item_y + 1, column=item_x)
            self.game.item_price_labels.append(price_label)

            item_x += 1
            item_index += 1


        F2 = Frame(self.game.nongyuan_valley_menu_win, padx=5, pady=5)
        F2.pack()

        Button(F2, text="Back", command=partial(self.game.winSwitch, self.game.nongyuan_valley_menu_win, self.game.nongyuan_valley_win)).grid(row=0, column=0)
        Label(F2, text = "  ").grid(row=0, column=1)
        Button(F2, text="Buy", command=partial(self.game.buy_selected_shop_item, self.su_ling_shop_prices)).grid(row=0, column=2)
        Label(F2, text="  ").grid(row=0, column=3)

        self.game.shop_gold_amount_label = Label(F2, text=_("Your gold: ")+ str(self.game.inv[_("Gold")]))
        self.game.shop_gold_amount_label.grid(row=0,column=4)


        self.game.nongyuan_valley_menu_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.nongyuan_valley_menu_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.nongyuan_valley_menu_win.winfo_width(), self.game.nongyuan_valley_menu_win.winfo_height()
        self.game.nongyuan_valley_menu_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.nongyuan_valley_menu_win.focus_force()
        self.game.nongyuan_valley_menu_win.mainloop()###



class Scene_nongyuan_valley(Scene_nongyuan_valley_train):
    def __init__(self, game):
        self.game = game

        if self.game.taskProgressDic["xiao_han_jia_wen"] >= 14 and self.game.taskProgressDic["nongyuan_valley"] == -1:
            self.game.generate_dialogue_sequence(
                None,
                "Nongyuan Valley.png",
                [_("You're crazy... absolutely crazy!"),
                 _("These people came to us for help! I'm not about to feed them poison!"),
                 _("But it's to heal them! Not to harm them! Have you not seen how, in the past, I've used poison to treat patients who have been poisoned?"),
                 _("Yes, but that's because they had no alternatives. They would've died anyway..."),
                 _("Well, these people are in the same situation!"),
                 _("No, they're not! The snow lotus works perfectly to treat their pneumonia!"),
                 _("Yeah, and how much snow lotus do you have? There are hundreds, maybe thousands of people who have fallen ill from this disease!"), #Row 2
                 _("You can only treat 1 patient with each snow lotus."),
                 _("Where the heck are you going to find that much snow lotus?"),
                 _("We can start by treating those with worse symptoms and then... and then figure something out in the meantime."),
                 _("Why don't you just let me treat them?! There's plenty of what I need near the Eagle Sect cliffs."),
                 _("With my method, we can gather enough herbs and flowers to cure 100 patients a day!"),
                 _("But if something goes wrong, you'll be liable and may even be accused of poisoning your patients to death!")
                 ],
                ["Su Ling", "Su Ling", "Li Jinyi", "Su Ling", "Li Jinyi", "Su Ling",
                 "Li Jinyi", "Li Jinyi", "Li Jinyi", "Su Ling", "Li Jinyi", "Li Jinyi", "Su Ling",],
                [[_("I agree! It's crazy to try to use poison to cure people!"), partial(self.nongyuan_valley_choice,1)],
                 [_("Desperate times call for desperate measures. It's worth the risk!"), partial(self.nongyuan_valley_choice,2)]]
            )
            return

        self.game.nongyuan_valley_win = Toplevel(self.game.mapWin)
        self.game.nongyuan_valley_win.title(_("Nongyuan Valley"))

        bg = PhotoImage(file="Nongyuan Valley.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.nongyuan_valley_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Su Ling_icon_large.ppm")
        imageButton1 = Button(F1, command=partial(Scene_nongyuan_valley_train.clickedOnSuLing,self))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("Su Ling"))
        label.grid(row=1, column=0)
        F1.place(x=w // 4 - pic1.width() // 2, y=h // 4 - pic1.height() // 2)

        F2 = Frame(canvas)
        pic2 = PhotoImage(file="Li Jinyi_icon_large.ppm")
        imageButton2 = Button(F2, command=partial(Scene_nongyuan_valley_train.clickedOnLiJinyi, self))
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(F2, text=_("Li Jinyi"))
        label.grid(row=1, column=0)
        F2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 4 - pic2.height() // 2)

        F3 = Frame(canvas)
        pic3 = PhotoImage(file="Xu Ting_icon_large.ppm")
        imageButton3 = Button(F3, command=partial(Scene_nongyuan_valley_train.clickedOnXuTing, self))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        label = Label(F3, text=_("Xu Ting"))
        label.grid(row=1, column=0)
        F3.place(x=w // 4 - pic3.width() // 2, y=h * 3 // 4 - pic1.height() // 2)

        self.game.nongyuan_valley_win_F1 = Frame(self.game.nongyuan_valley_win)
        self.game.nongyuan_valley_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.nongyuan_valley_win,
                                            options=["Map", "Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.nongyuan_valley_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.nongyuan_valley_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.nongyuan_valley_win.winfo_width(), self.game.nongyuan_valley_win.winfo_height()
        self.game.nongyuan_valley_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.nongyuan_valley_win.focus_force()
        self.game.nongyuan_valley_win.mainloop()  ###


    def nongyuan_valley_choice(self, choice):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        if choice == 1:
            if self.game.taskProgressDic["join_sect"] == 300:
                player = self.game.character
            else:
                player = "This young man"
            self.game.taskProgressDic["nongyuan_valley"] = 10
            self.game.generate_dialogue_sequence(
                None,
                "Nongyuan Valley.png",
                [_("{} is right! 2 against 1! Hah!").format(player),
                 _("T-that's.... wait... Let's ask Xu Ting about this. What do you think, a-Ting?"),
                 _("I... uh... erm... I think I'm going to stay out of this..."),
                 _("Hahahaha! Very good! So it's decided then. We'll use my method!"),
                 _("What?! No! You can use your method all you want! I'll offer my services to those who are brave and want to get well sooner!"),
                 _("Whatever, just don't drag me down with you... {} will be my aid instead of you.").format(player),
                 _("Yes, ma'am! What do you need, Dr. Su?"),
                 _("I need you to collect 10 snow lotuses for me. I need them as soon as possible!"),
                 _("Got it! You can count on me!")],
                ["Su Ling", "Li Jinyi", "Xu Ting", "Su Ling", "Li Jinyi", "Su Ling", "you", "Su Ling", "you"]
            )

        elif choice == 2:
            if self.game.taskProgressDic["join_sect"] == 300:
                player = self.game.character
            else:
                player = "This young lad"
            self.game.taskProgressDic["nongyuan_valley"] = 20
            self.game.generate_dialogue_sequence(
                None,
                "Nongyuan Valley.png",
                [_("Ahahaha! {} is right! See? What will you say now?").format(player),
                 _("..... I don't care. There's no way I'm going to help you poison these patients."),
                 _("I don't need your help! I have this bright, young lad to be my assistant!"),
                 _("Yes, sir! What can I do for you?"),
                 _("Ok, here's the deal, m'lad. I need you to go fetch a few poisonous herbs and plants for me."),
                 _("'Poison Ivy' x 5, 'Stinging Nettle' x 5, 'Spiral Aloe Leaf' x 5, 'White Snakeroot' x 3, 'Elderberry' x 3"),
                 _("You should find plenty of these growing on the cliffs of Eagle Sect, which is only a short distance away."),
                 _("No problem! I'll go look for these right away!"),
                 _("Thanks, lad! You be careful!")],
                ["Li Jinyi", "Su Ling", "Li Jinyi", "you", "Li Jinyi", "Li Jinyi", "Li Jinyi", "you", "Li Jinyi"]
            )

        self.__init__(self.game)