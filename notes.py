"""
TASK PROGRESS

----- join_sect -----
700: Normal Huashan ending
701: Huashan pushed off cliff, 702: returned and defeated Guo Junda

400: Fled from Phantom Sect
401: Marry Li Guiping
     - Ren Zhichu locked in cave --> learns YYSAT --> comes after you
402: Thrown in cave --> escapes


*ENDINGS*
100: Cherry Blossom Island
200: Shanhu Sect
300: Nongyuan Valley
400: Phantom Sect
500: Shaolin
600: Wudang
700: Huashan
800: Rogue


----- return_to_phantom_sect -----
0: not started yet

1: returned (after running away)
    - Ren Zhichu locked in cave --> learns YYSAT --> seeks revenge on Li Guiping (triggers when you return to Phantom Sect)
       After cutscene where Li Guiping is killed, Ren Zhichu expresses that he no longer wishes to live because he is
       not a "man" anymore after practicing YYSAT. He thanks you for trying to save him when Li Guiping threw him into
       the cave and passes on his Inner Energy to you (+2000) before committing suicide by dashing his head against a rock.
    - When returning to Phantom Sect, the place will be empty, until doing the Liu Village quest --> get to Lu Xifeng directly

11: returned (after marrying Li Guiping)
    - Returning to Phantom Sect will result in facing off against Lu Xifeng

21: - Returning to Phantom Sect will trigger fight against Li Guiping + Ren Zhichu after passing the rocks, but Lu Xifeng
      will not appear yet. Kill them both after battle.
    - When returning to Phantom Sect again, the place will be empty, until doing the Liu Village quest --> get to Lu Xifeng directly
    - 22: passed mini game
    - 23: killed LGP and RZC for revenge


----- guo_zhiqiang -----
1: Help GZQ & ZHL
11: Defeat GJD --> GZQ & ZHL leave together
12: Lose to GJD --> ZHL jumps off cliff --> learns Wild Wind Blade; GZQ leaves Huashan
15: ZHL returns to avenge GJD's wife
16: ZHL defeats GJD and takes over Huashan; waits for GZQ to return
17: GZQ returned to Huashan; learned Thousand Swords/Huashan Sword Technique/receive 3000 gold

2: Help GJD
21: GZQ & ZHL commit suicide --> you learn Huashan Sword


----- feng_xinyi -----
3: ready to receive dark suit; ready to sneak into Shaolin
4: went to Shaolin once; ready for Masked Thief to appear
5: saw Masked Thief trying to steal manual from Shaolin
100: Shaolin Disciple ending


----- imperial_palace -----
0: warned by guard
1: chased by guards once


----- liu_village -----
11: help kid
12: defeated Ninjas searching village at night (found smoke bombs)
13: found out liu_village story
14: encountered ninjas in dark forest and defeated them
15: ready to encounter ninjas again --> find ninjas with kid
    - if threw RZC in cave when training at Phantom Sect, RZC (as Mysterious), appears and snatches the manual.
      Masked Assassins get angry at you and think it's your accomplice; attack you --> fight Masked Thief --> 1000
      If return to dark forest at a later time, level >= 25 --> meet RZC (now having learned DMCB as well) --> final fight
    - else continue to 16
16: ready to go to Phantom Sect
17: got past Phantom Sect mini game
18: got past Li Guiping --> fighting Lu Xifeng
19: defeated Lu Xifeng --> finds out he doesn't have manual --> go to Huashan and ask for help searching the area
    - if finished GZQ task with ZHL returning (currently head of Huashan), then GJD shows up after having practiced
      Devil Crescent Moon Blade and tries to regain control of Huashan
    - otherwise, go to Huashan for help; GJD agrees and sends some men with you, but they ambush you instead...
      turns out, the masked assassins are hired men from Eagle Sect (including leader = Masked Thief you encounter
      at Shaolin), which explains why Eagle Sect has been empty for the past 2-3 months...
      after you defeat Eagle Sect (masked men), GJD (now with Devil Crescent Moon Blade) is final boss you have to fight

21: bully kid --> 12

1000: done
1001: defeated Evil Ren Zhichu


----- li_guiying -----

100: help LGY instead of BS
101: ready to learn FPP
102: fighting Blind Sniper + Li Kefan
103: defeated Blind Sniper + Li Kefan

110: help LGY because married to LGP
111: ready to learn FPP
112: fighting Blind Sniper + Li Kefan
113: defeated Blind Sniper + Li Kefan


200: help BS instead of LGY
201: LGY dies; if player does not help BS, then must talk to CYD to continue Liu Village plot

300: observe and help neither


----- wu_hua_que -----
-1: dead (also sets wudang_task to 1)
1: don't help JW or let go in exchange for Dragonfly Manual


----- wudang_task -----
-2000: Wudang traitor, wiped out Wudang
-1000: Wudang traitor --> will have to face 3 Wudang Warriors + Zhang Tianjian
-1: mission failed (let him go)
1: started 1st mission
2: 1st mission complete
3: 1st mission reward claimed or wu_hua_que already dead or wu_hua_que let go; ready for mission 2
4: started 2nd mission
5: 2nd mission reward claimed; ready for mission 3
6: started 3rd mission
7: finished 3rd mission
8: reward for 3rd mission claimed


----- li_guiping_task -----
-1: not started
0: received task to beat up Ren Zhichu
1: finished beating up Ren Zhichu
2: received task to assassinate Xuan Sheng
3: Xuan Sheng assassinated
4: claimed reward for assassinating Xuan Sheng
5: received task to deliver sack (containing message) to Li Guiying
6: delivered sack
7: Li Guiying training
8: Li Guiying mastered FPP --> wants to take you to challenge righteous sects
9: Received mask --> ready to go challenge sects
10: Completed and received reward!
11: Ready to beat up Ren Zhichu again --> he dies of injuries --> unlocks Potter's Field
12: Ready for Potter's field
50: opened sack --> took a few items --> won't be able to complete task



----- ouyang_nana -----
0: appear at inn (after being rescued; otherwise, value still -1 but will appear if wudang_task >= 3)
1+: depends on choices made during task;
    *if refuse to help Nana then --> 200
    *if gets above {}, she will fall in love with you;
         if sleep with Ouyang Shanshan --> 200
         else Duel w/ Ouyang Xiong, regardless of outcome, he will let you marry her --> 100
         (If chose to go Rogue in join_sect, Ouyang Xiong will teach you Violent Dragon Palm & Dragon Roar; otherwise only Dragon Roar)

    *otherwise
     If accept duel w/ Ouyang Xiong --> receive item --> 400
     If refuse to duel w/ Ouyang Xiong, then --> 300

100: ending 1 - married Ouyang Nana; can return to Dragon Sect
200: ending 2 - Nana "commits suicide" --> leave Dragon Sect and never return
300: ending 3 - Nana ending unknown --> leave Dragon Sect and never return
400: ending 4 - Nana has freedom; can return to Dragon Sect


----- ouyang_nana_xu_jun -----
-1: not started; can only start if ouyang_nana ending is 200 or 300, triggered by going to the inn again

If ouyang_nana ending = 200, must regain her trust by
    0: help her beat the gambler 5 times in a row
    5: beat gambler 5 times; talk to her again to help her meet Xu Jun

    If refuse to help beat gambler,
        -20: gets drugged while having meal with her and sold to Eunuchs to be castrated.

If ouyang_nana ending = 300, must regain her trust by
    0: help her beat the gambler 5 times in a row
    5: beat gambler 5 times; talk to her again to help her meet Xu Jun

    If refuse to help beat gambler,
        -10: Ouyang Nana leaves and never appears again


5: if has prior conflict w/ Wudang, tells Ouyang Nana; Ouyang Nana thinks you are useless and storms off angrily --> -5
   otherwise go to Wudang:
   - if chivalry <= 0 and did not train with Wudang, 3 Warriors of Wudang will dismiss your words --> Nana storms off angrily --> -5
   - else if trained with Wudang or chivalry > 0, the 3 Warriors of Wudang will consider your words
     --> challenge Ouyang Nana to capture the Duo Goblins of Jiangnan --> 10

10: Jiangnan Forest scene; several options to choose from
    (Spar with Ouyang Nana)
    (Hop on trees)
    (Draw attention to yourself)
    (Leave) --> lose chivalry

20: Ready for Goblins to appear
30: Task complete


----- castrated -----
0: no
1: yes


----- feng_pan -----
-1: not started
0: overheard General Feng's conversation with son about Feng Xinyi
10: take Feng Xinyi back home by force --> 11: brought her back, get 10K reward
20: promise to help her, ready for General Feng's army to show up
21: fight Feng Junyi
22: fight Feng Pan
23: fight army of archers
24/34/44: Yang Kai appears and takes you to temp hiding location
33: lost fight w/ Feng Pan and/or archer army --> Yang Kai appears and saves the day
43: lost fight w/ Feng Pan and/or archer army --> Yang Kai appears and saves the day (if joined Shanhu Sect)
50: start wooden house scene
60: discourage Feng Xinyi from her feelings towards Yang Kai --> Yang Kai leaves; Feng Xinyi goes back to inn with you
70: encourage Feng Xinyi --> Yang Kai and Feng Xinyi get together and return to Shanhu Sect; you return to inn




----- xiao_han_jia_wen -----
0: Xiao Mei cutscene done
10: agreed to help Xiao Mei
11: ready to search for Snow Lotus
12: got Snow Lotus (got Bamboo Flute)
13: Received pill
14: Xiao Mei healed
15: got Gold Necklace
16: returned Gold Necklace; learned move from Xiao Yong


20: Xiao Mei dies


----- eagle_sect -----
-1: never been to Eagle Sect
0: went to Eagle Sect: saw empty
1: Jiang Yuqi appeared
11: JYQ taken to Scrub's house
100: married JYQ

1000: task done (Jiang Yuqi declines help or committed suicide)


----- jiang_yuqi_liu_village_choice -----
-1: not prompted to make choice
1: prompted already


----- wife_intimacy -----
-1: no wife
0: has wife
100: can engage in intimate activity
-1000: if self castrate, wife leaves



----- luohanzhen -----
-1: not qualified
0: can challenge luohanzhen
+1 for every time passed; max 10
10: make choice to pick between Prajna or Sumeru Palm Manual
20: choice made



----- huang_yuwei -----
-1: not started
1: completed box challenge


----- huang_yuwei_birthday -----
-20: failed on stage 2 spar
-10: failed on stage 1 spar
-1: not started
0: received task from Jia Wen
1: began stage 1 spar
10: won stage 1 spar --> began stage 2 (maze)
20: stage 2 spar (against Huang Xiaodong, level depends on cherry blossoms collected from Maze)


----- nongyuan_valley -----
-1: not started
10: side w/ Su Ling
20: side w/ Li Jinyi
100: completed Su Ling
200: completed Li Jinyi


----- ma_guobao -----
-1: not started
0: talked to Xuan Chen and learned about the opportunity
1: got background on Ma Guobao
2: paid fees
3: learned Yunhuan Taichi Fist
4-10: depends on proficiency level of Yunhuan Taichi Fist
20: defeated Ma Guobao --> exposed as fake --> taken to Wudang


----- small_town -----
-1000: townspeople all arrested; swordsman dies trying to save the villagers
-1: not started
0: heard town history from Lin Anning
1: heard town history from swordsman
2: help townspeople --> fight General Feng's archers
10: townspeople escaped
20: did not buy enough time for townspeople to escape
100: came back and found townspeople escaped
200: came back and found townspeople dead

----- small_town_ghost -----
-1: not started
1: encountered customer and inn keeper argument
2: encountered ghost at night 1st time
3: entered forest after 2nd ghost encounter
4: defeated "ghost"
10: both inn keeper and murderer are killed
20: only inn keeper is killed



----- yagyu_clan -----
-1: not started
0: won spar --> entering inner court for first time
1: gained access to inner court
2: defeated Yagyu Tadashi in spar --> able to receive offer to join clan (if Rogue) or help him with his ambitions
3: agreed to help
4: received instructions to poison Abbot from Ashikaga
5: need to look for skin mask
6: talked to sea merchant, who offers to trade skin mask for Dragon's Tear
7: traded Dragon Tear for Skin Mask
10: successfully poisoned Abbot --> 11: ready to go to Shaolin
20: failed to poison Abbot --> 21: ready to go to Shaolin
30: defeated Shaolin
X: heard plans from Ashikaga --> tournament against all righteous sects to take place in 30 days. X = gameDate
10000: tournament start
111111111: yagyu is Supreme Leader
222222222: you are Supreme Leader
999999999: broke alliance with Yagyu Clan
-3: turned down offer to help
-4: overheard convo about going to Shaolin
-5: began tournament vs yagyu
-10: post tournament; will be ambushed by ninjas
-11: ambushed by ninjas
-12: ninjas --> shadow clones
-13: defeated shadow clones
-14: went to Shaolin and found out that Abbot has been kidnapped
-15: snuck into Yagyu Clan and found out Abbot has been kidnapped
-16: snuck in and found key but also saw Kasumi getting locked up; need to return to Shaolin
-17: told Elder Xuanjing; Elder Xuanjing and some Luohan Monks to assist;
     they will take distract the samurai while you sneak into the dungeon
-100: Yagyu Clan and Hattori Ninjas wiped out
-101: plundered Yagyu Clan


----- yagyu_clan_join -----
-1: not joined
1: joined



----- babao_tower -----
-1: not started
0: intro cutscene done
1000: sect leader; other values will vary depending on who you choose as winner


----- blind_sniper -----
Blind Sniper background story: previous lover from a well-off family, was one of the women chosen to be candidate
to become princess (to the now-emperor, who was a prince 20 years ago). Made plans to escape, but somehow, got
caught by guards --> blinded --> rescued by one of the 3 Warriors of Wudang --> learned some Wudang techniques
--> changed name to Blind Sniper and began to roam the land challenging random people to spars to improve

Lover became empress; player has choice of:

1) Bring her out of palace to reunite with Blind Sniper --> plot continues
2) Let it go --> gains skill Firm Resolve

Blind Sniper expresses that he would like to, but he's blind and though skilled, has no way of getting into the palace.
You should go back to Scrub to seek his advice. Scrub suggests talking to Nie Tu at Babao Tower,
but if you haven't resolved their conflict with each other, you need to do that first.

After resolving conflict, the Eight Treasures all recommend Nie Tu, who is capable of digging tunnels underground.
However, Nie Tu refuses to help because his father was killed 9 years ago by ____, the leader of the 3 Evils of Wulin.
His father had dueled ____ many times, each resulting in a draw. Prior to their final duel, the Blind Sniper, due to his
nature, insisted that he spar with Nie Tu's father. They sparred for several hours without a clear victor. Blind
Sniper finally decides to give it a break and rematch another day. However, because of this, Nie Tu's father had
depleted half of his energy and lost to ____.

You promise to help Nie Tu kill ____ to avenge his father. --> Begin ____ task.

After completing ___ task, Nie Tu agrees to let go of past grievances with Blind Sniper and help you. He tells you
to dig an underground tunnel to the Empress's palace. --> mini game

After meeting the Empress, she seems hesitant (can't easily leave palace) but after your urging, she agrees to
prepare and go with you after a few nights of preparation, if you manage to sneak back into her Palace. Upon returning,
you get ambushed and the Empress reveals her true colors. She values her riches and power far more than her
previous relationship with the Blind Sniper. If you manage to escape, you can choose to go back to Blind Sniper and

1) Tell him the truth (chivalry +10) --> he is heart broken --> disappears into the woods
2) Lie to him and say you were incompetent --> he tells you not to worry about it --> gives you 5000 gold for trying


-1: Not started
-100: Heard background story; decided to leave things as is
-1000: killed Nie Tu so you can learn some moves from Chen Wei; can't continue
0: Heard background story; ready to talk to Scrub to help
1: Couldn't find Scrub at his house --> look around instead
2: Found Scrub at Wanhua Pavillion --> found out he's trying to track down the 3 Tailed Fox, one of the 3 Evils of the Martial Arts Community.
   You explain to Scrub your situation. Scrub recommends you talk to Nie Tu in Babao Tower who might be able to help.
3: Talked to Nie Tu; heard back story... ready to begin tournament to trap Chen Wei. If lose any of the rounds, reset to 3.
4: won round 1 of tournament
5: won round 2 of tournament
6: won round 3 of tournament
7: finished sparring w/ Nie Tu; cutscene to take you to 'see the manual'
   --> Nie Tu gets taken hostage due to his impatience for revenge
   --> have to find Chen Wei (Big Beard) at Tibetan Plains to rescue him
8: helped Nie Tu kill Chen Wei --> go back to Babao Tower and discuss plans for sneaking into the palace
9: Nie Tu tells you about plan for digging an underground tunnel; talk to him again when ready to go
X: met Empress; delivered message;
1000000: returned to Empress; fought Eunuch
1000001: tell Blind Sniper the truth; chivalry +30; he retires from Wulin
1000002: don't tell him the truth; he thanks you for your efforts and teaches you Wudang Agility Technique or gives you some Hundred Seeds Pills


----- big_beard -----
-1: no interaction
0: introduction cutscene
X: sold X items to Big Beard



----- wanhua_pavillion -----
-1: Not started; have not visited yet
0: intro cutscene done; can order wine or request ladies to build up to 5
5: max for relationship building --> increased chance of getting 'Drunken Bliss' when ordering wine
6: sparred with A-Xiu; felt her legs/feet and confirmed they are real
7: A-Xiu decides to remain at Wanhua Pavillion
8: slept with Xian-er
9: passing of time; ready for Xian-er to appear and try to kill you
10: fought Xian-er, decided to let her go --> learn 'Rainbow Fragrance' + option to buy more from A-Xiu
20: insist on handing Xian-er over to the officials --> she commits suicide --> Wanhua Pavillion closes
100: either castrated or did not have wife/say you had wife after sleeping with Xian-er
200: A-Xiu joins you so you can help her solve the mystery of her mother's death --> learn 'Fairy Plucking Flowers'
     Three-Tailed Fox plot aborted.


----- cai_wuqiong -----
-100: aborted
-1: Not started
0: intro cutscene done; can do "tasks" for him to build up to 5
5: if blind_sniper progress is at least 2, then CWQ will ask you to steal A-Xiu's shoes for him --> 10
10: ready to steal A-Xiu's shoes
11: stole A-Xiu's shoes
12: cutscene for A-Xiu's shoes done
13: gave shoes to Cai Wuqiong, who rewarded you w/ 30000 Gold and offers you a night of Xian-er's service;
    unlocks option to see Xian-er
14: slept with Xian-er


----- a_xiu -----
-1: Not started
0: A-Xiu joins your team so you can help her solve the mystery of her mother's death
   - wanhua_pavillion value is set to 200
   - cai_wuqiong value is set to -100



----- supreme_leader -----
-1: not started
0: not leader (yagyu is leader instead)
1: leader


----- shin_scrub -----
-1: not started
0: ready to fight shin_scrub
1: beat shin_scrub


----- huashan_tribute -----
-1: not started
X: last collected on day X


----- shaolin_tribute -----
-1: not started
X: last collected on day X


----- wudang_tribute -----
-1: not started
X: last collected on day X


----- shanhu_sect_tribute -----
-1: not started
X: last collected on day X


----- dragon_sect_tribute -----
-1: not started
X: last collected on day X


----- phantom_sect_tribute -----
-1: not started
X: last collected on day X


----- scrub_tutorial -----
-1: not started
1: done





嗜血银蝠: Silver Vampire
赤胆诛心: Red Sea Urchin
七寸蛇杖: Seven-inch Serpent Staff

阴霆电掣刀: Lightning Saber
五雷鸣阳剑: Thunder Blade
花岗石靴: Granite Boots
寒刺入骨: Ice Shard
蜓翼剑: Wings of the Dragonfly
烈火剑：Flame Blade


山崩地裂：Splitting Mountains and Earth
大鹏展翅：Spreading Wings
冰天雪地: Ice Burst


李可凡
朱士廷
杨凯
李桂萍
李桂英
凤心怡
凤潘
凤俊义
许俊
高雅罗
仁智初
刘智远
欧阳雄
江雨琪
江元庆
玄尘
玄生
玄静


温玉岚
张安狄
雷孟德
李黾卓

陈金
翰林
李小洋
聂土

梁哲


云幻太极拳


----- Ideas yet to be implemented for future releases -----
Safe exit for non-critical minigames (DONE in Beta 1.3)
Allow carry over of full stack of 1 item (DONE in Beta 1.3)
Current location indicator (DONE in Beta 1.3)
Location/Travel time description on tooltips (DONE in Beta 1.3)
In-game time system (DONE in Beta 1.3)
Seasonal occurrences (DONE in Beta 1.3)
Additional plot/quests/characters (DONE in Beta 1.3)
Additional moves (DONE in Beta 1.3)


Enable smoke bomb usage in mini-games (DONE in Beta 1.4)
Yagyu Clan plot (DONE in Beta 1.4)
Additional moves (DONE in Beta 1.4)
Jump cut smoothing in mini games (DONE in Beta 1.4)
Increased stat buff/debuff effects (DONE in Beta 1.4)
Battle interface - width adjustment based on moves learned (DONE in Beta 1.4)
Tooltips for battle status (DONE in Beta 1.4)
Flee button disabled for unescapable battles (DONE in Beta 1.4)
Extended Imperial Palace plot (DONE in Beta 1.4)
Changed Huashanlunjian to annual event; improved rewards (DONE in Beta 1.4)
JYQ's father - plot closing if wife is JYQ (DONE in Beta 1.4)
Extended Ouyang Nana plot (DONE in Beta 1.4)
Can challenge "Real Scrub" repeatedly; difficulty increases gradually along with rewards (DONE in Beta 1.4)
Three-Tailed Fox Plot (DONE in Beta 1.4)
Blind Sniper Plot (DONE in Beta 1.4)
Major bug fix - Max recursion crash (DONE In Beta 1.4)
Other bug fixes (DONE in Beta 1.4)
Added Discord/README in-game button (DONE in Beta 1.4)
Interface enhancements (Done in Beta 1.4)
Selected moveset for battle limit to 10 moves (DONE in Beta 1.4)
Auto-battle option (DONE in Beta 1.4)



Display damage bonus for armed/unarmed attacks in profile
Romance events at different thresholds
Torch flickering
Reputation system
Text-size vs display resolution
"""