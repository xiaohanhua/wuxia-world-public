from functools import partial
from helper_funcs import*
from threading import Thread
from mini_game_target_practice import*
from mini_game_phantom_sect import*
import tkinter.messagebox as messagebox
import time

_ = gettext

class Scene_phantom_sect_train:

    def __init__(self, game, skip_to_fm=False):
        self.game=game
        if skip_to_fm:
            self.li_guiping_foot_massage(self.game.scrubHouseWin)
            return
        self.game.days_spent_in_training = int((self.game.taskProgressDic["join_sect"] - 3) * 100)
        try:
            if self.game.taskProgressDic["join_sect"] == 3.0:
                try:
                    self.game.phantom_sect_train_win = Toplevel(self.game.scrubHouseWin)
                except:
                    self.game.phantom_sect_train_win = Toplevel(self.game.continueGameWin)

            else:
                self.game.phantom_sect_train_win = Toplevel(self.game.continueGameWin)
        except:
            self.game.phantom_sect_train_win = Toplevel(self.game.continueGameWin)

        self.game.phantom_sect_train_win.title(_("Phantom Sect"))

        bg = PhotoImage(file="Phantom Sect.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.phantom_sect_train_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.game.phantom_sect_F1 = Frame(canvas)
        pic1 = PhotoImage(file="Li Guiping_icon_large.ppm")
        imageButton1 = Button(self.game.phantom_sect_F1, command=partial(self.clickedOnLiGuiping, True))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.game.phantom_sect_F1, text=_("Li Guiping"))
        label.grid(row=1, column=0)
        self.game.phantom_sect_F1.place(x=w // 4 - pic1.width() // 2, y=h // 2)

        if self.game.taskProgressDic["ren_zhichu_count"] == 0:
            self.game.phantom_sect_F2 = Frame(canvas)
            pic2 = PhotoImage(file="Ren Zhichu_icon_large.ppm")
            imageButton2 = Button(self.game.phantom_sect_F2, command=partial(self.clickedOnRenZhichu, True))
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            label = Label(self.game.phantom_sect_F2, text=_("Ren Zhichu"))
            label.grid(row=1, column=0)
            self.game.phantom_sect_F2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 2)


        self.game.days_spent_in_training_label = Label(self.game.phantom_sect_train_win,
                                                  text=_("Days trained: {}").format(self.game.days_spent_in_training))
        self.game.days_spent_in_training_label.pack()
        self.game.phantom_sect_train_win_F1 = Frame(self.game.phantom_sect_train_win)
        self.game.phantom_sect_train_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.phantom_sect_train_win,
                                            options=["Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.phantom_sect_train_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.phantom_sect_train_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.phantom_sect_train_win.winfo_width(), self.game.phantom_sect_train_win.winfo_height()
        self.game.phantom_sect_train_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.phantom_sect_train_win.focus_force()
        self.game.phantom_sect_train_win.mainloop()###


    def clickedOnLiGuiping(self, training):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.phantom_sect_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.phantom_sect_train_win_F1, text=_("Spar"),
                   command=partial(self.phantom_sect_train_spar, targ=_("Li Guiping"))).grid(row=1, column=0)
            Button(self.game.phantom_sect_train_win_F1, text=_("Learn"),
                   command=self.phantom_sect_train_learn).grid(row=2, column=0)

        else:
            if self.game.taskProgressDic["li_guiping_task"] == 3:
                self.game.taskProgressDic["li_guiping_task"] = 4
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_win,
                    "Phantom Sect.png",
                    [_("HAHAHAHAHA~ Nicely done, {}!").format(self.game.character),
                     _("I can almost hear the mourning of those stupid monks!"),
                     _("As promised... your reward...")],
                    ["Li Guiping", "Li Guiping", "Li Guiping"]
                )
                if self.game.chivalry <= -100 or self.game.luck >= 100:
                    self.game.learn_move(special_move(_("Phantom Claws")))
                    self.game.learn_skill(skill(_("Phantom Steps")))
                    messagebox.showinfo("", _("Li Guiping taught you a new move: 'Phantom Claws' and skill: 'Phantom Steps'!"))
                else:
                    self.game.learn_move(special_move(_("Phantom Claws")))
                    messagebox.showinfo("", _("Li Guiping taught you a new move: 'Phantom Claws'!"))

            for widget in self.game.phantom_sect_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.phantom_sect_win_F1, text=_("Spar (Li Guiping)"),
                   command=partial(self.phantom_sect_train_spar, targ=_("Li Guiping"), training=False)).grid(row=1, column=0)
            Button(self.game.phantom_sect_win_F1, text=_("Talk"),
                   command=self.talk_to_li_guiping).grid(row=2, column=0)
            Button(self.game.phantom_sect_win_F1, text=_("Task"),
                   command=self.li_guiping_task).grid(row=3, column=0)


    def talk_to_li_guiping(self, choice=0):
        if choice == 0:
            r = randrange(3)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_win,
                    "Phantom Sect.png",
                    [_("I know you are naturally drawn towards me and want to talk, but I'm busy right now.")],
                    ["Li Guiping"]
                )
            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_win,
                    "Phantom Sect.png",
                    [_("If you want to be my slave, ask me nicely and I'll consider it."),
                     _("Ah, I think I am unworthy to serve you... thanks though..."),
                     _("(................)")],
                    ["Li Guiping", "you", "you"]
                )
            elif r == 2:
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_win,
                    "Phantom Sect.png",
                    [_("What are you looking at?"),
                     _("N-nothing, I just wanted to ask--"),
                     _("If you're not here to offer me your service, then get lost.")],
                    ["Li Guiping", "you", "Li Guiping"]
                )


    def clickedOnRenZhichu(self, training):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.phantom_sect_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.phantom_sect_train_win_F1, text=_("Spar"),
                   command=partial(self.phantom_sect_train_spar, targ=_("Ren Zhichu"))).grid(row=0, column=0)
            Button(self.game.phantom_sect_train_win_F1, text=_("Tell me about this sect"),
                   command=partial(self.talk_to_ren_zhichu, 1)).grid(row=1, column=0)
            Button(self.game.phantom_sect_train_win_F1, text=_("Can you show me around?"),
                   command=partial(self.talk_to_ren_zhichu, 2)).grid(row=2, column=0)
            Button(self.game.phantom_sect_train_win_F1, text=_("Who's Li Guiping and why does she seem so harsh?"),
                   command=partial(self.talk_to_ren_zhichu, 3)).grid(row=3, column=0)
            Button(self.game.phantom_sect_train_win_F1, text=_("Why does everyone cover their faces here?"),
                   command=partial(self.talk_to_ren_zhichu, 4)).grid(row=4, column=0)

        else:

            if self.game.taskProgressDic["li_guiping_task"] == 0:
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_win,
                    "Phantom Sect.png",
                    [_("Hey man... That lady over there told me to teach you a lesson."),
                     _("W-what? What did I do wrong??"),
                     _("I have no idea to be honest... something about you not being a good slave..."),
                     _("B-but I've done everything she asked me to... I --"),
                     _("Don't explain to me. It's no use."),
                     _(".....")],
                    ["you", "Ren Zhichu", "you", "Ren Zhichu", "you", "Ren Zhichu"]
                )
                opp = character(_("Ren Zhichu"), 8,
                                sml=[special_move(_("Poison Needle"), level=8)],
                                skills=[])

                self.game.battleMenu(self.game.you, opp, "battleground_phantom_sect.png",
                                     "kindaichi 3.mp3", fromWin=self.game.phantom_sect_win,
                                     battleType="task", destinationWinList=[], postBattleCmd=self.bully_ren_zhichu)
                return


            elif self.game.taskProgressDic["li_guiping_task"] == 11:
                self.game.phantom_sect_win.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Hello... I'm back..."),
                     _("H-hi... Welcome back..."),
                     _("Listen, I really don't wanna do this to you but..."),
                     _("Wait! Did she... did she tell you to..."),
                     _("Yep..."),
                     _("Was it because of that cup that I broke on accident???"),
                     _("Yep..."), #Row 2
                     _("But that was because she was pressuring me to hurry"),
                     _("And the previous day, she stuck needles under my fingernails..."),
                     _("I could barely hold the cup with 2 hands!"),
                     _("I really didn't mean--"),
                     _("You know, your life really sucks... Sometimes I wonder how you are still alive."),
                     _("Yet as much as I feel sorry for you, I can't pass this opportunity for a lucrative reward..."), #Row 3
                     _("Sorry man..."),
                     _("Wait! Please! I'm begging you! I really can't take it anymore! Please!")],
                    ["you", "Ren Zhichu", "you", "Ren Zhichu", "you", "Ren Zhichu",
                     "you", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "you",
                     "you", "you", "Ren Zhichu"]
                )

                self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
                self.game.startSoundEffectThread()
                time.sleep(.5)
                self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
                self.game.startSoundEffectThread()
                self.game.currentEffect = "male_scream.wav"
                self.game.startSoundEffectThread()
                time.sleep(1)

                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("No... Please... Please spare me..."),
                     _("HAHAHAHA~ Keep going! Keep going!~~"),],
                    ["Ren Zhichu", "Li Guiping"]
                )

                self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
                self.game.startSoundEffectThread()
                time.sleep(.5)
                self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
                self.game.startSoundEffectThread()
                self.game.currentEffect = "male_scream.wav"
                self.game.startSoundEffectThread()
                time.sleep(1)
                self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
                self.game.startSoundEffectThread()
                time.sleep(.5)
                self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
                self.game.startSoundEffectThread()
                self.game.currentEffect = "male_scream.wav"
                self.game.startSoundEffectThread()
                time.sleep(1)

                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Hahahahahah~~~ Don't you love the way he screams??"),
                     _("I can't resist anymore! I'm going to join in on the fun~"), ],
                    ["Li Guiping", "Li Guiping"]
                )

                for i in range(30):
                    self.game.currentEffect = "sfx_poison_dart.mp3"
                    self.game.startSoundEffectThread()
                    time.sleep(.1)
                    if i % 5 == randrange(3):
                        self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
                        self.game.startSoundEffectThread()
                        time.sleep(.5)

                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_win,
                    "Phantom Sect.png",
                    [_("Hahahahahah~~~ Come on! Scream! Louder!"),
                     _("Hey... why isn't he screaming anymore?"),
                     _("..... Hey! Ren Zhichu! You better not play dead!"),
                     _("Erm... maybe we went a little overboard..."),
                     _("I... I..."),
                     _("Wow,  he's so weak now that I can barely hear what he's saying..."),
                     _("I will be waiting for you... in the potter's field near the foot of the mountain..."),
                     _("*With one last breath, Ren Zhichu collapses to the ground, dead.*"),
                     _("*As you stare into his lifeless eyes, you remember his final words and shudder.*"),
                     _("Wait, what was he saying? Something about the potter's field...?"),
                     _("No idea..."),
                     _("(Hmmm, maybe I ought to ask Scrub what the heck he meant by that...)"),
                     _("So what do we do now?"), #Row 3
                     _("Take his body away from here and bury it somewhere far away..."),
                     _("I'll give you some money for your labor... Just keep your mouth shut about this incident.")],
                    ["Li Guiping", "Li Guiping", "Li Guiping", "you", "Ren Zhichu", "Li Guiping",
                     "Ren Zhichu", "Blank", "Blank", "you", "Li Guiping", "you",
                     "you", "Li Guiping", "Li Guiping"]
                )

                self.game.add_item(_("Gold"), 2000)
                self.game.taskProgressDic["li_guiping_task"] = 12
                self.game.taskProgressDic["potters_field"] = 0
                self.game.phantom_sect_F2.place(x=-3000, y=-3000)
                messagebox.showinfo("", _("Received 'Gold' x 2000!"))
                return
            

            for widget in self.game.phantom_sect_win_F1.winfo_children():
                widget.destroy()

            if self.game.taskProgressDic["li_guiping_task"] >= 1:
                self.bully_ren_zhichu()
                
            else:
                Button(self.game.phantom_sect_win_F1, text=_("Spar (Ren Zhichu)"),
                       command=partial(self.phantom_sect_train_spar, targ=_("Ren Zhichu"), training=False)).grid(row=1,column=0)
                Button(self.game.phantom_sect_win_F1, text=_("Tell me about this sect"),
                       command=partial(self.talk_to_ren_zhichu, 1)).grid(row=2, column=0)
                Button(self.game.phantom_sect_win_F1, text=_("Can you show me around?"),
                       command=partial(self.talk_to_ren_zhichu, 12)).grid(row=3, column=0)
                Button(self.game.phantom_sect_win_F1, text=_("Who's Li Guiping and why does she seem so harsh?"),
                       command=partial(self.talk_to_ren_zhichu, 3)).grid(row=4, column=0)


    def talk_to_ren_zhichu(self, choice=0):

        if self.game.taskProgressDic["join_sect"] < 100 and self.game.taskProgressDic["join_sect"] >= 0:
            dialogue_from_win = self.game.phantom_sect_train_win
        else:
            dialogue_from_win = self.game.phantom_sect_win

        if choice == 1:
            self.game.generate_dialogue_sequence(
                dialogue_from_win,
                "Phantom Sect.png",
                [_("Our sect is well-known in the martial arts community, though not very respected."),
                 _("Master's martial arts techniques are unmatched in both strength and cruelty."),
                 _("Even though I know some of the things I learn here are dishonorable, I don't really care."),
                 _("I lost both of my parents at a young age and was frequently picked on; I've had 2 near-death experiences."),
                 _("This world is cruel, and sometimes, you need to do whatever it takes to survive..."),
                 _("As a member of the Phantom Sect, I can learn the skills I need to protect myself and destroy those who look down on me..."),],
                ["Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu"]
            )

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                dialogue_from_win,
                "Phantom Sect.png",
                [_("Where do you think you're going?"),
                 _("Didn't Master specifically warn you about wandering around without permission?"),
                 _("Looks like you two need a lesson in obedience..."),
                 _("Wait, Senior, please, I wasn't planning on ---"),
                 _("Don't talk back to me, you scum."),
                 _("AHHHHHHH!!! MY FACE!!! AHHHH!!! HELP!!!"),
                 _("Wait, why did you just shoot him with a needle?"),
                 _("Senior, I was wrong!!! Please have mercy! Please give me the antidote!"),
                 _("Since you look like you've learned your lesson, fine, take it..."),
                 _("As for you, {}...").format(self.game.character),
                 _("(Why do I have a bad feeling about this...)"),
                 ],
                ["Li Guiping", "Li Guiping", "Li Guiping", "Ren Zhichu", "Li Guiping", "Ren Zhichu", "you",
                 "Ren Zhichu", "Li Guiping", "Li Guiping", "you"]
            )

            self.game.battleID = "LiGuipingPunishment"
            self.phantom_sect_train_spar(targ=_("Li Guiping"), punishment=True)

        elif choice == 12:
            self.game.generate_dialogue_sequence(
                dialogue_from_win,
                "Phantom Sect.png",
                [_("Ren Zhichu! Where do you think you're going? Why are you giving this stranger a tour?"),
                 _("Didn't Master specifically warn us about wandering around without permission?"),
                 _("Looks like you need a lesson in obedience..."),
                 _("Wait, Senior, please, I wasn't planning on ---"),
                 _("Don't talk back to me, you scum."),
                 _("AHHHHHHH!!! MY FACE!!! AHHHH!!! HELP!!!"),
                 _("Wait, why did you just shoot him with a needle?"),
                 _("I'm disciplining a junior disciple. Got a problem?"),
                 _("............")
                 ],
                ["Li Guiping", "Li Guiping", "Li Guiping", "Ren Zhichu", "Li Guiping", "Ren Zhichu", "you",
                 "Li Guiping", "you"]
            )

        elif choice == 3:
            self.game.generate_dialogue_sequence(
                dialogue_from_win,
                "Phantom Sect.png",
                [_("She's currently one of the strongest disciples in the Phantom Sect."),
                 _("I joined a little over 1 year before Li Guiping and her sister did, and I had great potential but..."),
                 _("*Sigh* I don't know what it is... Maybe Master doesn't trust me, maybe he prefers female disciples..."),
                 _("Anyway, he decided to teach more advanced techniques to the 2 of them rather than me."),
                 _("And it's not just me... A couple of other male disciples also received unfair treatment."),
                 _("We were all more gifted than they were, but after learning the other techniques, we are no match for them."),
                 _("Wait, this Li Guiping has a sister?"),
                 _("Yep, her sister, Li Guiying, is 4 years older than her."),
                 _("She left about 6 months ago to carry out Master's orders, but none of us know the details."),
                 _("Even though she is quite cold, she's not cruel like her younger sister."),
                 _("Cruel?"),
                 _("Yep, Li Guiping often gives other disciples unreasonable orders... She treats us like slaves."),
                 _("But because of the special treatment she receives from Master, none of us dare mess with her."),
                 _("I'd avoid messing with her if possible... for your own good as well as ours..."),
                 ],
                ["Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu",
                 "you", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "you", "Ren Zhichu", "Ren Zhichu",
                 "Ren Zhichu"]
            )

        elif choice == 4:
            self.game.generate_dialogue_sequence(
                dialogue_from_win,
                "Phantom Sect.png",
                [_("As you might know, Master never shows his face."),
                 _("Out of reverence for him, we also cover our faces."),
                 _("So, does that mean I should cover mine too?"),
                 _("You are a special case; since you are only here for 2 months, I think it's okay...")
                 ],
                ["Ren Zhichu", "Ren Zhichu", "you", "Ren Zhichu"]
            )


    def phantom_sect_train_learn(self, task=None, choice=None):

        if not task:
            if self.game.taskProgressDic["ren_zhichu_count"] == 0:
                r = randrange(2+max([0,self.game.taskProgressDic["li_guiping_count"]//2]))
            else:
                r = 0

            if r == 0:
                self.game.phantom_sect_train_win.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("You want to learn? How about you train with me first..."),
                     _("I need a target to practice my poison needles on."),],
                    ["Li Guiping", "Li Guiping"],
                    [[_("Yes, Senior, I will do as you say!"), partial(self.phantom_sect_train_learn, "target_practice", 1)],
                     [_("Wait, what? But I came here to learn martial arts, not be a target!"), partial(self.phantom_sect_train_learn, "target_practice", 2)]]
                )
                return

            else:
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_train_win,
                    "Phantom Sect.png",
                    [_("I've had a long day... Why don't you massage my feet first?")],
                    ["Li Guiping"],
                    [[_("My pleasure! I will do anything for you!"),
                      partial(self.phantom_sect_train_learn, "foot_massage", 1)],
                     [_("Heck no! I'm not your slave!"),
                      partial(self.phantom_sect_train_learn, "foot_massage", 2)]]
                )
                return

        elif task == "target_practice":
            if choice == 1:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.taskProgressDic["li_guiping_count"] += 1
                self.game.mini_game_in_progress = True
                self.game.mini_game = target_practice_mini_game(self.game, self)
                self.game.mini_game.new()

            elif choice == 2:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_train_win,
                    "Phantom Sect.png",
                    [_("You're here to do as I say!"),
                     _("Looks like you need a lesson in obedience..."),
                     _("...")],
                    ["Li Guiping", "Li Guiping", "you"]
                )

                self.game.battleID = "LiGuipingPunishment"
                self.phantom_sect_train_spar(targ=_("Li Guiping"), punishment=True)
                return


        elif task == "foot_massage":

            if choice == 1:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.li_guiping_foot_massage()
                return
            elif choice == 2:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_train_win,
                    "Phantom Sect.png",
                    [_("It's a priviledge to serve me, you unworthy weakling!"),
                     _("Looks like you need a lesson in obedience..."),
                     _("...")],
                    ["Li Guiping", "Li Guiping", "you"]
                )

                self.game.battleID = "LiGuipingPunishment"
                self.phantom_sect_train_spar(targ=_("Li Guiping"), punishment=True)
                return


        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        self.game.phantom_sect_train_win.deiconify()
        self.phantom_sect_training_update(.01)


    def bully_ren_zhichu(self):
        if self.game.taskProgressDic["li_guiping_task"] == 0:
            self.game.taskProgressDic["li_guiping_task"] = 1
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_win,
                "Phantom Sect.png",
                [_("AHHH!! Owwww! Please! I'm sorry! Please stop!"),
                 _(".... Fine, I guess that's enough for now..."),
                 _("Hahahahahaha~ That's what you get, you scum..."),
                 _("You'd better remember to obey my every command from now on!"),
                 _("Y-yes, I understand... I'm sorry..."),
                 _("{}, as long as you help me with whatever I need, you can treat Ren Zhichu like your slave too...").format(self.game.character),
                 _("I might even join you in beating him up if I feel like it~"),
                 _("Ah ok, hahaha...")],
                ["Ren Zhichu", "you", "Li Guiping", "Li Guiping", "Ren Zhichu", "Li Guiping", "Li Guiping", "you"]
            )

        elif self.game.taskProgressDic["li_guiping_task"] == 1:
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_win,
                "Phantom Sect.png",
                [_("No please! Please don't hurt me..."),
                 _("..."),],
                ["Ren Zhichu", "you"]
            )

        elif self.game.taskProgressDic["li_guiping_task"] >= 2:
            if random() <= .4:
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_win,
                    "Phantom Sect.png",
                    [_("Hehehehe..."),
                     _("Hi, what can I do for you? Please, I'll do anything... Just don't hurt me please..."),
                     _("I'm hungry and need some money to buy food..."),
                     _("Here! Take some from me! Please spare me!")],
                    ["you", "Ren Zhichu", "you", "Ren Zhichu"]
                )
                self.game.add_item(_("Gold"), 20)
                messagebox.showinfo("", _("Ren Zhichu gives you 20 Gold."))

            else:
                self.game.phantom_sect_win.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_(".................."),
                     _("Hi, what can I do for you? Please, I'll do anything... Just don't hurt me please..."),
                     _("I'm not in a good mood today... Need to take it out on someone..."),
                     _("Wait! No! Please --")],
                    ["you", "Ren Zhichu", "you", "Ren Zhichu"]
                )
                self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
                self.game.startSoundEffectThread()
                time.sleep(.5)
                self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
                self.game.startSoundEffectThread()
                self.game.currentEffect = "male_scream.wav"
                self.game.startSoundEffectThread()
                time.sleep(1)
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Hahahahaha~ Isn't it fun to bully this useless slave?"),
                     _("I think I'm slowly learning from you haha..."),
                     _("Please... I can't... t--"),
                     _("Shut up, you worthless dog."),
                     _("Watch this, {}. It's so fun to stab him with my needles...").format(self.game.character)],
                    ["Li Guiping", "you", "Ren Zhichu", "Li Guiping", "Li Guiping"]
                )

                self.game.currentEffect = "sfx_poison_dart.mp3"
                self.game.startSoundEffectThread()
                time.sleep(.5)
                self.game.currentEffect = "male_scream.wav"
                self.game.startSoundEffectThread()
                time.sleep(1)

                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Hahahahaha~ Don't you love the way he screams?~~")],
                    ["Li Guiping"]
                )
                self.game.phantom_sect_win.deiconify()


    def li_guiping_task(self):
        if self.game.taskProgressDic["li_guiping_task"] == -1:
            self.game.taskProgressDic["li_guiping_task"] = 0
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_win,
                "Phantom Sect.png",
                [_("You see that wimp over there? I haven't been satisfied with his service lately."),
                 _("Go teach him a lesson for me. I'll reward you well."),
                 _("....... Ok.")],
                ["Li Guiping", "Li Guiping", "you"]
            )

        elif self.game.taskProgressDic["li_guiping_task"] in [0, 11]:
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_win,
                "Phantom Sect.png",
                [_("Come on, what are you waiting for?"),
                 _("Teach him to be an obedient dog."),
                 _("....... Ok.")],
                ["Li Guiping", "Li Guiping", "you"]
            )

        elif self.game.taskProgressDic["li_guiping_task"] == 1:
            self.game.taskProgressDic["li_guiping_task"] = 2
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_win,
                "Phantom Sect.png",
                [_("The stupid monk Xuansheng from Shaolin insulted me last week in front of the Martial Arts Community..."),
                 _("He called me a cruel witch... Heh... I'll show him what cruel really means..."),
                 _("You want me to get rid of him?"),
                 _("Try to make it slow and painful if you can. If not, then just make sure he's dead."),
                 _("Oh, by the way, you'll want to put on a ninja suit to help you sneak into Shaolin..."),
                 _("Where do I get one of those?"),
                 _("Don't look at me. I'm not giving you anything for free...")],
                ["Li Guiping", "Li Guiping", "you", "Li Guiping", "Li Guiping", "you", "Li Guiping"]
            )

        elif self.game.taskProgressDic["li_guiping_task"] == 2:
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_win,
                "Phantom Sect.png",
                [_("I want that Xuansheng monk dead. Don't forget to put on a ninja suit to keep your identity hidden."),
                 _("Torture him slowly if you have time, but it's up to you."),
                 _("....... Ok.")],
                ["Li Guiping", "Li Guiping", "you"]
            )

        elif self.game.taskProgressDic["li_guiping_task"] == 4:
            self.game.taskProgressDic["li_guiping_task"] = 5
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_win,
                "Phantom Sect.png",
                [_("I need you to deliver something to my sister."),
                 _("She wears black clothes and is quite attractive, like me."),
                 _("...... Ok."),
                 _("What's with that attitude?! Are you not willing or do you not think I'm attractive?!"),
                 _("No no no! Not at all! I think you are beautiful and would love to make this trip for you!"),
                 _("(Geez... this lady...)")],
                ["Li Guiping", "Li Guiping", "you", "Li Guiping", "you", "you"]
            )

            self.game.add_item(_("Sack"))
            messagebox.showinfo("", _("Received 'Sack' x 1!"))


        elif self.game.taskProgressDic["li_guiping_task"] == 5:
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_win,
                "Phantom Sect.png",
                [_("Why are you just standing here?"),
                 _("I know it's hard to take your eyes off of me, but the message is urgent! Go find my sister already."),
                 _("Ah, right... will do!")],
                ["Li Guiping", "Li Guiping", "you"]
            )

        elif self.game.taskProgressDic["li_guiping_task"] in [10, 50]:
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_win,
                "Phantom Sect.png",
                [_("Finally done? Where were you all this time???"),
                 _("I was deliv--"),
                 _("Of course I know that! But what took you so long?!"),
                 _("Well, your sister also--"),
                 _("Whatever, forget it... Not important anymore."),
                 _("Listen, I need you to teach that piece of garbage a lesson again..."),
                 _("Uh oh, what did he do this time...?"),
                 _("He can't do anything right. Yesterday, he broke the cup while pouring me tea."), #Row 2
                 _("I think we've been too soft on him, and he hasn't learned his lesson."),
                 _("Also, there's something about the way he looks at me that makes me very uncomfortable..."),
                 _("This time, we need to really discipline him..."),
                 _("Errr, ok, got it...")],
                ["Li Guiping", "you", "Li Guiping", "you", "Li Guiping", "Li Guiping", "you",
                 "Li Guiping", "Li Guiping", "Li Guiping", "Li Guiping", "you"]
            )
            self.game.taskProgressDic["li_guiping_task"] = 11


        else:
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_win,
                "Phantom Sect.png",
                [_("There's nothing I need from you at the moment.")],
                ["Li Guiping"]
            )


    def post_li_guiping_target_practice(self, got_hit_count):

        self.game.mini_game_in_progress = False
        pg.display.quit()

        if got_hit_count == 0:
            self.game.luck += 20
            messagebox.showinfo("", "You somehow manage to dodge all the needles. Your luck + 20!")
        elif got_hit_count <= 3:
            if random() <= .5:
                self.game.luck += 5
                messagebox.showinfo("", "You manage to only get a few minor scratches. Your luck + 5!")
            else:
                self.game.dexterity += 3
                messagebox.showinfo("", "You manage to only get a few minor scratches. Your dexterity + 3!")
        elif got_hit_count < 10:
            pass
        else:
            self.game.taskProgressDic["li_guiping_count"] += 1
            self.game.generate_dialogue_sequence(
                None,
                "Phantom Sect.png",
                [_("You sure know how to be a good target! I like it!"),
                 _("Keep up the good work!")],
                ["Li Guiping", "Li Guiping"]
            )

        time.sleep(.25)
        self.game.stopSoundtrack()
        self.game.currentBGM = "kindaichi 3.mp3"
        self.game.startSoundtrackThread()
        self.game.phantom_sect_train_win.deiconify()


    def li_guiping_foot_massage(self, fromWin=None):

        if not fromWin:
            fromWin = self.game.phantom_sect_train_win
        self.game.stopSoundtrack()
        self.game.currentBGM = "jy_romantic.mp3"
        self.game.startSoundtrackThread()

        fromWin.withdraw()
        self.game.footMassageWin = Toplevel(fromWin)
        self.game.off_screen_x, self.game.off_screen_y = -3000, -3000
        if "hx" in VERSION:
            bg_img = PhotoImage(file="li_guiping_foot_bg_hx.png")
        else:
            bg_img = PhotoImage(file="li_guiping_foot_bg.png")

        self.game.foot_massage_canvas_width = bg_img.width()
        self.game.foot_massage_canvas_height = bg_img.height()

        self.game.foot_massage_canvas = Canvas(self.game.footMassageWin, width=self.game.foot_massage_canvas_width,
                                          height=self.game.foot_massage_canvas_height, bg='grey')
        self.game.foot_massage_canvas.pack(side=LEFT)

        self.game.bg_img = self.game.foot_massage_canvas.create_image(self.game.foot_massage_canvas_width - bg_img.width(),
                                                            self.game.foot_massage_canvas_height - bg_img.height(),
                                                            anchor=NW, image=bg_img)

        lg = PhotoImage(file="arrowLeftGreen.png")
        rg = PhotoImage(file="arrowRightGreen.png")
        ug = PhotoImage(file="arrowUpGreen.png")
        dg = PhotoImage(file="arrowDownGreen.png")
        lr = PhotoImage(file="arrowLeftRed.png")
        rr = PhotoImage(file="arrowRightRed.png")
        ur = PhotoImage(file="arrowUpRed.png")
        dr = PhotoImage(file="arrowDownRed.png")

        self.game.lg_img = self.game.foot_massage_canvas.create_image(self.game.off_screen_x, self.game.off_screen_y, anchor=NW,
                                                            image=lg)
        self.game.rg_img = self.game.foot_massage_canvas.create_image(self.game.off_screen_x, self.game.off_screen_y, anchor=NW,
                                                            image=rg)
        self.game.ug_img = self.game.foot_massage_canvas.create_image(self.game.off_screen_x, self.game.off_screen_y, anchor=NW,
                                                            image=ug)
        self.game.dg_img = self.game.foot_massage_canvas.create_image(self.game.off_screen_x, self.game.off_screen_y, anchor=NW,
                                                            image=dg)
        self.game.lr_img = self.game.foot_massage_canvas.create_image(self.game.off_screen_x, self.game.off_screen_y, anchor=NW,
                                                            image=lr)
        self.game.rr_img = self.game.foot_massage_canvas.create_image(self.game.off_screen_x, self.game.off_screen_y, anchor=NW,
                                                            image=rr)
        self.game.ur_img = self.game.foot_massage_canvas.create_image(self.game.off_screen_x, self.game.off_screen_y, anchor=NW,
                                                            image=ur)
        self.game.dr_img = self.game.foot_massage_canvas.create_image(self.game.off_screen_x, self.game.off_screen_y, anchor=NW,
                                                            image=dr)

        self.game.foot_massage_instructions_frame = Frame(self.game.footMassageWin, borderwidth=2, relief=SUNKEN)
        self.game.foot_massage_instructions_frame.pack(side=RIGHT, fill=Y)

        Label(self.game.foot_massage_instructions_frame,
              text="Press the arrow key on the keyboard\nthat corresponds to the GREEN arrows\nthat appear. Ignore the RED arrows.").pack()

        self.game.foot_massage_count = 0
        self.game.foot_massage_score = 0
        self.game.correct_foot_massage_direction = None

        t = Thread(target=self.generate_correct_foot_massage_direction, args=(5,))
        t.start()

        self.game.footMassageWin.after(65000, self.foot_massage_results)
        self.game.footMassageWin.bind("<KeyPress>", self.foot_massage_direction)
        self.game.footMassageWin.update_idletasks()
        toplevel_w, toplevel_h = self.game.footMassageWin.winfo_width(), self.game.footMassageWin.winfo_height()
        self.game.footMassageWin.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.footMassageWin.focus_force()
        self.game.footMassageWin.mainloop()###


    def foot_massage_direction(self, event):
        if event.keysym == self.game.correct_foot_massage_direction:
            self.game.foot_massage_score += 1
            self.game.correct_foot_massage_direction = None
            #extra = open("extra.txt", "r").read()
            #if extra == "Yes":
            #    self.game.currentEffect = pickOne(["female_moan{}.m4a".format(i) for i in range(1,5)])
            #else:
            #    self.game.currentEffect = "jy_pleasant_sound.mp3"
            self.game.currentEffect = "jy_pleasant_sound.mp3"
            self.game.startSoundEffectThread()
        else:
            self.game.foot_massage_score -= 1


    def generate_correct_foot_massage_direction(self, buffer=0):
        time.sleep(buffer)
        self.game.correct_foot_massage_direction = choice(["Left", "Right", "Up", "Down", "None", "None"])
        on_screen_x = randrange(170, 330)
        on_screen_y = randrange(240, 500)
        if self.game.correct_foot_massage_direction == "Left":
            self.game.r_direction_img = self.game.lg_img
        elif self.game.correct_foot_massage_direction == "Right":
            self.game.r_direction_img = self.game.rg_img
        elif self.game.correct_foot_massage_direction == "Up":
            self.game.r_direction_img = self.game.ug_img
        elif self.game.correct_foot_massage_direction == "Down":
            self.game.r_direction_img = self.game.dg_img
        else:
            self.game.r_direction_img = choice([self.game.lr_img, self.game.rr_img, self.game.ur_img, self.game.dr_img])
            self.game.foot_massage_count -= 1


        try:
            self.game.foot_massage_count += 1
            self.game.foot_massage_canvas.move(self.game.r_direction_img, on_screen_x - self.game.off_screen_x,
                                          on_screen_y - self.game.off_screen_y)

            if self.game.foot_massage_count < 50:
                time.sleep(1)
                self.game.foot_massage_canvas.move(self.game.r_direction_img, self.game.off_screen_x - on_screen_x,
                                              self.game.off_screen_y - on_screen_y)
                self.generate_correct_foot_massage_direction()
        except:
            self.game.foot_massage_count = 50


    def foot_massage_results(self):
        #print(self.game.foot_massage_score)
        self.game.foot_massage_count = 50
        self.game.footMassageWin.quit()
        self.game.footMassageWin.destroy()

        if self.game.taskProgressDic["wife_intimacy"] >= 0:
            if self.game.foot_massage_score >= 35 and self.game.luck >= 2500 and VERSION == "FF":
                self.game.taskProgressDic["wife_intimacy"] += 30
                self.game.generate_dialogue_sequence(
                    self.game.scrubHouseWin,
                    "li_guiping_foot_bg_small.png",
                    [_("Li Guiping takes off her boots and places her feet in front of you."),
                     _("You caress her feet with your hands and press on the acupuncture points on her soles."),
                     _("As you feel her smooth, soft skin and stare at her beautifully shaped feet, your heart begins to race."),
                     _("Suddenly, you bring her right foot towards your face and start to kiss and suck on her toes."),
                     _("She lets out a soft moan and pushes her foot further into your mouth, signaling for you to continue."),
                     _("At the same time, she places her other foot onto your face so that the sole of her foot is in direct contact with your nose."),
                     _("As you inhale the intoxicating scent of foot sweat and leather from her soles, your tongue works its way between her toes."),
                     _("The combination of the vinegary smell and salty taste of her feet drive you crazy, and you can no longer restrain yourself."),
                     _("Soon, you throw yourself on top of her, and the two of you begin to kiss passionately."),
                     _("{}, that was the best foot massage I've ever received...").format(self.game.character),
                     _("And that was the best one I've ever given... Guiping, you drive me crazy..."),
                     _("Come on, now, do with me whatever you wish..."),
                     _("*You spend an intensely passionate night with your wife.*")],
                    ["Blank", "Blank", "Blank", "Blank", "Blank", "Blank", "Blank", "Blank", "Blank",
                     "Li Guiping", "you", "Li Guiping", "Blank"]
                )
                self.game.stamina = 0
            elif self.game.foot_massage_score >= 30:
                self.game.taskProgressDic["wife_intimacy"] += 20
                self.game.generate_dialogue_sequence(
                    self.game.scrubHouseWin,
                    "scrub_house.ppm",
                    [_("That felt amazing!~ Thank you, {}!").format(self.game.character)],
                    ["Li Guiping"]
                )
            else:
                self.game.taskProgressDic["wife_intimacy"] += 10
                self.game.generate_dialogue_sequence(
                    self.game.scrubHouseWin,
                    "scrub_house.ppm",
                    [_("Thanks, {}!").format(self.game.character)],
                    ["Li Guiping"]
                )
            return


        self.phantom_sect_training_update(.01)
        if self.game.foot_massage_score >= 35:
            self.game.taskProgressDic["li_guiping_count"] += 3
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_train_win,
                "Phantom Sect.png",
                [_("Amazing~ That felt so good~"),
                 _("I'm starting to like you a lot, {}.").format(self.game.character),
                 _("Keep up the good work!"),
                 _("I am honored to be able to serve you!")],
                ["Li Guiping", "Li Guiping", "Li Guiping", "you"]
            )


            if self.game.taskProgressDic["li_guiping_count"] > 3:

                move_names = [m.name for m in self.game.sml]
                skill_names = [s.name for s in self.game.skills]

                if _("Poison Needle") not in move_names:
                    new_move = special_move(name=_("Poison Needle"))
                    self.game.learn_move(new_move)
                    messagebox.showinfo("", _("Li Guiping teaches you a new move: Poison Needle!"))

                elif _("Phantom Steps") not in skill_names:
                    new_skill = skill(name=_("Phantom Steps"))
                    self.game.learn_skill(new_skill)
                    messagebox.showinfo("", _("Li Guiping teaches you a new skill: Phantom Steps!"))

                elif _("Phantom Claws") not in move_names:
                    new_move = special_move(name=_("Phantom Claws"))
                    self.game.learn_move(new_move)
                    messagebox.showinfo("", _("Li Guiping teaches you a new move: Phantom Claws!"))


        elif self.game.foot_massage_score >= 30:
            self.game.taskProgressDic["li_guiping_count"] += 1
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_train_win,
                "Phantom Sect.png",
                [_("Hmmm, not bad... I guess you are not useless after all."),
                 _("If you keep obeying me, good things will happen to you."),
                 _("Thank you, Senior!")],
                ["Li Guiping", "Li Guiping", "you"]
            )

        else:
            self.game.taskProgressDic["li_guiping_count"] -= 1
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_train_win,
                "Phantom Sect.png",
                [_("What are you doing?! Can't you even give a proper foot massage?"),
                 _("Get out of my sight..."),],
                ["Li Guiping", "Li Guiping"]
            )

        if self.game.taskProgressDic["li_guiping_count"] >= 12 and self.game.taskProgressDic["ren_zhichu_count"] == 0:

            self.game.stopSoundtrack()
            self.game.phantom_sect_train_win.withdraw()

            self.game.generate_dialogue_sequence(
                None,
                "Phantom Sect.png",
                [_("Ren Zhichu, come here."),
                 _("I-I'm here."),
                 _("I'm thirsty; bring me some tea..."),
                 _("Ah yes, right away..."),],
                ["Li Guiping", "Ren Zhichu", "Li Guiping", "Ren Zhichu"]
            )

            self.game.currentEffect = "sfx_glass_break.mp3"
            self.game.startSoundEffectThread()
            self.game.currentBGM = "Kindaichi 10.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Phantom Sect.png",
                [_("Ah! I-I'm sorry, Senior Sister! I didn't mean t---"),
                 _("Useless idiot!"),
                 _("Can't you do anything properly?!"),
                 _("You last maybe 2 seconds when I need a target to practice on."),
                 _("Your foot massages feel like someone pouring ants on my feet."),
                 _("And now you can't even pour tea?!"),
                 _("I-I'll go pour another one for ---"),
                 _("Shut your mouth, you pathetic worm..."),
                 _("You are clearly good for nothing... "),
                 _("I've dealt with you for long enough..."),
                 _("{}, lock him up in the cave and don't give him any food for 10 days.").format(self.game.character),
                 _("If you do this, I will reward you greatly..."),],
                ["Ren Zhichu", "Li Guiping", "Li Guiping", "Li Guiping", "Li Guiping", "Li Guiping",
                 "Ren Zhichu", "Li Guiping", "Li Guiping", "Li Guiping", "Li Guiping", "Li Guiping"],
                [[_("Your wish is my command!"), partial(self.ren_zhichu_cave_choice, 1)],
                 [_("That's surely going to kill him; maybe we can think of a less severe punishment."), partial(self.ren_zhichu_cave_choice, 2)]]
            )


    def ren_zhichu_cave_choice(self, choice):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        if choice == 1:
            self.game.generate_dialogue_sequence(
                None,
                "Phantom Sect.png",
                [_("Sorry, mate, I have to do this..."),
                 _("Wait, please, give me another chance. I promise I'll---"),
                 _("AHHHHHH!!!"),
                 _("{}, I have something to tell you...").format(self.game.character),
                 _("After seeing how you've served me so faithfully in the past few days, I'm very touched."),
                 _("I don't think I'll find another person who treats me this well. I'm willing to give up everything just to be with you."),
                 _("Guiping..."),
                 _("{}... let us leave this place and be together forever!").format(self.game.character),
                 _("Yes, forever..."),
                 _("We'll need to be careful though, since if Master finds out, he will be furious and might come after us."),
                 _("Don't worry; I'll do whatever it takes to protect you."),],
                ["you", "Ren Zhichu", "Ren Zhichu", "Li Guiping", "Li Guiping", "Li Guiping",
                 "you", "Li Guiping", "you", "Li Guiping", "you"]
            )

            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_romantic.mp3"
            self.game.startSoundtrackThread()
            self.game.taskProgressDic["wife_intimacy"] = 0
            self.game.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("Scrub! I'm back!"),
                 _("Oh hi, welcome back... who is this?"),
                 _("Ah, this is Guiping... she's my... uh..."),
                 _("Ahhhh, I see... congratulations, and nice to meet you."),
                 _("Nice to meet you too."),
                 _("Hey Scrub, can you preside over our wedding ceremony please? We want to get married tonight."),
                 _("Sure, it would be my honor!")],
                ["you", "Scrub", "you", "Scrub", "Li Guiping", "you", "Scrub"]
            )

            messagebox.showinfo("", _("You and Li Guiping are now officially husband and wife!"))
            self.game.staminaMax += 200
            self.game.healthMax += 200
            self.game.attack += 20
            self.game.strength += 20
            self.game.speed += 20
            self.game.defence += 20
            self.game.dexterity += 20
            self.game.taskProgressDic["ren_zhichu_count"] = 1
            self.game.taskProgressDic["join_sect"] = 401
            self.game.generateGameWin()

        else:
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_train_win,
                "Phantom Sect.png",
                [_("After all this time, you still don't know how I feel?"),
                 _("You won't even do this small thing for me to show your loyalty?"),
                 _("............"),
                 _("Very well, I'll do this myself then."),
                 _("Wait, please, give me another chance. I promise I'll---"),
                 _("AHHHHHH!!!"),
                 _("............")],
                ["Li Guiping", "Li Guiping", "you", "Li Guiping", "Ren Zhichu", "Ren Zhichu", "you"]
            )
            self.game.phantom_sect_F2.place(x=-3000,y=-3000)
            self.game.taskProgressDic["li_guiping_count"] -= 10
            self.game.taskProgressDic["ren_zhichu_count"] = 2


    def phantom_sect_train_spar(self, targ, punishment=False, training=True):

        if training:
            if self.game.taskProgressDic["join_sect"] < 100 and self.game.taskProgressDic["join_sect"]-3 < .59:
                self.phantom_sect_training_update(.01)

            if targ == _("Li Guiping"):
                opp = character(_("Li Guiping"), 12,
                                sml=[special_move(_("Poison Needle"), level=10),
                                     special_move(_("Phantom Claws"), level=7)],
                                skills=[skill(_("Phantom Steps"), level=8)])
    
            elif targ == _("Ren Zhichu"):
                opp = character(_("Ren Zhichu"), min([5,self.game.level]),
                                sml=[special_move(_("Poison Needle"), level= min([7,self.game.level]))],
                                skills=[])

            if punishment:
                self.game.battleMenu(self.game.you, opp, "battleground_phantom_sect.png",
                                "kindaichi 3.mp3", fromWin=self.game.phantom_sect_train_win,
                                battleType="training", destinationWinList=[self.game.phantom_sect_train_win] * 3,
                                     postBattleCmd=self.li_guiping_punishment)

            else:
                self.game.battleMenu(self.game.you, opp, "battleground_phantom_sect.png",
                                "kindaichi 3.mp3", fromWin=self.game.phantom_sect_train_win,
                                battleType="training", destinationWinList=[self.game.phantom_sect_train_win] * 3)

        else:
            if targ == _("Li Guiping"):
                opp = character(_("Li Guiping"), 14,
                                sml=[special_move(_("Poison Needle"), level=10),
                                     special_move(_("Phantom Claws"), level=8)],
                                skills=[skill(_("Phantom Steps"), level=8)])

            elif targ == _("Ren Zhichu"):
                opp = character(_("Ren Zhichu"), 8,
                                sml=[special_move(_("Poison Needle"), level=8)],
                                skills=[])

            self.game.battleMenu(self.game.you, opp, "battleground_phantom_sect.png",
                                 "kindaichi 3.mp3", fromWin=self.game.phantom_sect_win,
                                 battleType="training", destinationWinList=[self.game.phantom_sect_win] * 3)


    def li_guiping_punishment(self, won):
        if won:
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_train_win,
                "Phantom Sect.png",
                [_("I... I went easy on you and you took this fight seriously?!"),
                 _("How dare you disrespect your Senior!"),
                 _("If you don't accept your punishment, then I'm going to tell Master about this."),
                 _("Wait! Don't tell him! I'll accept my punishment..."),
                 _("AHHHH!!!")],
                ["Li Guiping", "Li Guiping", "Li Guiping", "you", "you"]
            )

            attributes = [_("Attack"), _("Strength"), _("Speed"), _("Defence"), _("Dexterity")]
            r = pickOne(attributes)
            if r == _("Attack"):
                self.game.attack -= 2
            elif r == _("Strength"):
                self.game.strength -= 2
            elif r == _("Speed"):
                self.game.speed -= 2
            elif r == _("Defence"):
                self.game.defence -= 2
            elif r == _("Dexterity"):
                self.game.dexterity -= 2

            self.game.taskProgressDic["li_guiping_count"] -= 3
            messagebox.showinfo("", _("Li Guiping stabs you with a Poison Needle. Your {} decreases by 2.").format(r))

        else:
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_train_win,
                "Phantom Sect.png",
                [_("That should teach you a lesson..."),
                 _("As for your punishment..."),
                 _("AHHHH!!!")],
                ["Li Guiping", "Li Guiping", "you"]
            )

            attributes = [_("Attack"), _("Strength"), _("Speed"), _("Defence"), _("Dexterity")]
            r = pickOne(attributes)
            if r == _("Attack"):
                self.game.attack -= 1
            elif r == _("Strength"):
                self.game.strength -= 1
            elif r == _("Speed"):
                self.game.speed -= 1
            elif r == _("Defence"):
                self.game.defence -= 1
            elif r == _("Dexterity"):
                self.game.dexterity -= 1
            self.game.taskProgressDic["li_guiping_count"] -= randrange(1,3)
            messagebox.showinfo("", _("Li Guiping stabs you with a Poison Needle. Your {} decreases by 1.").format(r))

        if self.game.taskProgressDic["li_guiping_count"] <= -5:
            self.game.phantom_sect_train_win.withdraw()
            if self.game.taskProgressDic["ren_zhichu_count"] == 0:
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("You clearly came here just to cause trouble..."),
                     _("I've dealt with you for long enough, and I'm sick of it."),
                     _("Listen up, Ren Zhichu, I want {} thrown into the cave; lock him there for 10 days without food as punishment.").format(self.game.character),
                     _("If you do as I say, I will reward you greatly..."),
                     _("Sorry, mate, I have no choice..."),
                     _("Wait, what are you doing?! H--- AHHHHHHH!"),],
                    ["Li Guiping", "Li Guiping", "Li Guiping", "Li Guiping", "Ren Zhichu", "you"]
                )

                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect Cave.png",
                    [_("..."),
                     _("I can't believe I'm locked here by that crazy woman..."),
                     _("10 days without food?! She must be trying to kill me. I'd better start looking for a way out of here..."),],
                    ["you", "you", "you"]
                )

                self.phantom_sect_cave()

            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("This crazy woman looks like she's ready to twist my head off at any moment..."),
                     _("I'd better sneak out of here before I end up like Ren Zhichu..."),],
                    ["you", "you"]
                )
                self.game.taskProgressDic["join_sect"] = 400
                self.game.generateGameWin()


    def phantom_sect_cave(self):
        self.game.stopSoundtrack()
        self.game.currentBGM = "Kindaichi 7.mp3"
        self.game.startSoundtrackThread()
        self.game.phantom_sect_cave_win = Toplevel(self.game.phantom_sect_train_win)
        self.game.phantom_sect_cave_win.title(_("Cave"))

        bg = PhotoImage(file="Phantom Sect Cave.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.phantom_sect_cave_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)


        self.game.phantom_sect_cave_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.phantom_sect_cave_win.bind("<Button-1>", self.phantom_sect_cave_search)
        self.game.phantom_sect_cave_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.phantom_sect_cave_win.winfo_width(), self.game.phantom_sect_cave_win.winfo_height()
        self.game.phantom_sect_cave_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.phantom_sect_cave_win.focus_force()
        self.game.phantom_sect_cave_win.mainloop()###


    def phantom_sect_cave_search(self, event):
        x = event.x
        y = event.y

        if x >= 280 and x <= 300 and y >= 50 and y <= 65:
            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_cave_win,
                "Phantom Sect Cave.png",
                [_("Wait, this spot feels hollow... maybe if I punch this spot...")],
                ["you"]
            )

            self.game.currentEffect = "sfx_male_voice.mp3"
            self.game.startSoundEffectThread()

            self.game.currentEffect = "sfx_zap8.wav"
            self.game.startSoundEffectThread()

            self.game.generate_dialogue_sequence(
                self.game.phantom_sect_cave_win,
                "Phantom Sect Cave.png",
                [_("Ah ha! I did it!"),
                 _("Ooooh, what's this behind the cave wall?")],
                ["you", "you"]
            )
            if self.game.luck >= 75:
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_cave_win,
                    "Phantom Sect Cave.png",
                    [_("Hmmm, an old book... can't really see the details in the dark though."),
                     _("Whatever, finders keepers... Let me get out of here first...")],
                    ["you", "you"]
                )
                messagebox.showinfo("", _("You place the book in your inventory."))
                self.game.inv[_("Yin Yang Soul Absorption Manual")] = 1

            else:
                self.game.generate_dialogue_sequence(
                    self.game.phantom_sect_cave_win,
                    "Phantom Sect Cave.png",
                    [_("A chest of gold! Ahahahaha! I'm rich!"),
                     _("Anyway, I'd better get out of here before someone finds out...")],
                    ["you", "you"]
                )
                messagebox.showinfo("", _("Found 5000 gold!"))
                self.game.inv["Gold"] += 5000

            self.game.taskProgressDic["join_sect"] = 402
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.phantom_sect_cave_win.quit()
            self.game.phantom_sect_cave_win.destroy()
            try:
                self.game.scrubHouseWin.deiconify()
            except:
                self.game.generateGameWin()

        else:
            print(_("You press around on the rocks to feel for a soft spot."))


    def phantom_sect_training_update(self, increment):
        self.game.taskProgressDic["join_sect"] += increment
        self.game.taskProgressDic["join_sect"] = round(self.game.taskProgressDic["join_sect"],2)
        self.game.days_spent_in_training = int(self.game.taskProgressDic["join_sect"]*100 - 300)
        self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
        if self.game.days_spent_in_training >= 40:
            self.game.days_spent_in_training = 40
            self.game.taskProgressDic["join_sect"] = 3.6
            self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
            self.game.phantom_sect_train_win.withdraw()

            self.game.generate_dialogue_sequence(
                None,
                "Phantom Sect.png",
                [_("Man, if I keep staying here, I'm either gonna turn into this woman's slave or die by her hands..."),
                 _("I'd better sneak out of here before I end up like Ren Zhichu..."), ],
                ["you", "you"]
            )
            self.game.taskProgressDic["join_sect"] = 400
            self.game.generateGameWin()




class Scene_phantom_sect(Scene_phantom_sect_train):
    def __init__(self, game):
        
        self.game = game
        if self.game.taskProgressDic["join_sect"] == 401:
            if self.game.taskProgressDic["return_to_phantom_sect"] == 0:
                self.game.mapWin.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Wait, {}! If we go back now, Master will surely be angry at us...").format(self.game.character),
                     _("He might even kill us... Are you sure you want to do this?")],
                    ["Li Guiping", "Li Guiping"]
                )
                response = messagebox.askquestion("", "Return to Phantom Sect?")
                if response == "yes":
                    self.return_to_phantom_sect()
                else:
                    self.game.mapWin.deiconify()
                return
        elif self.game.taskProgressDic["join_sect"] in [400,402] and self.game.taskProgressDic["liu_village"] < 16:
            if self.game.taskProgressDic["return_to_phantom_sect"] == 0:
                self.game.mapWin.withdraw()
                self.return_to_phantom_sect()
                return
            else:
                self.game.mapWin.withdraw()
                self.game.stopSoundtrack()
                self.game.currentBGM = "jy_suspenseful2.mp3"
                self.game.startSoundtrackThread()
                if self.game.taskProgressDic["join_sect"] in [400, 402]:
                    self.game.taskProgressDic["liu_village"] = 18
                    self.game.generate_dialogue_sequence(
                        None,
                        "Phantom Sect.png",
                        [_("Lu Xifeng! Hand over the Devil Crescent Moon Blade Manual, and I'll leave right away."),
                         _("Not many have the guts to make demands from Phantom Sect..."),
                         _("The only thing you will receive is death..."),
                         _("HAHAHAHAHA! It seems this can't be resolved peacefully... Fine by me...")],
                        ["you", "Lu Xifeng", "Lu Xifeng", "you"]
                    )
                    opp = character(_("Lu Xifeng"), 25,
                                    sml=[special_move(_("Phantom Claws"), level=10),
                                         special_move(_("Heart Shattering Fist"), level=10),
                                         special_move(_("Five Poison Palm"), level=10),
                                         special_move(_("Shadow Blade"), level=10),
                                         special_move(_("Centipede Poison Powder"))],
                                    skills=[skill(_("Phantom Steps"), level=10),
                                            skill(_("Yin Yang Soul Absorption Technique"), level=10),
                                            skill(_("Countering Poison With Poison"), level=10)])

                    self.game.battleMenu(self.game.you, opp, "battleground_phantom_sect.png",
                                         "jy_suspenseful2.mp3", fromWin=None,
                                         battleType="task", destinationWinList=[None] * 3,
                                         postBattleCmd=self.post_mini_game)
                    return
        elif self.game.taskProgressDic["liu_village"] == 16:
            self.game.mapWin.withdraw()
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful2.mp3"
            self.game.startSoundtrackThread()
            if self.game.taskProgressDic["join_sect"] in [400, 402]:
                self.game.taskProgressDic["liu_village"] = 18
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Lu Xifeng! Hand over the Devil Crescent Moon Blade Manual, and I'll leave right away."),
                     _("Not many have the guts to make demands from Phantom Sect..."),
                     _("The only thing you will receive is death..."),
                     _("HAHAHAHAHA! It seems this can't be resolved peacefully... Fine by me...")],
                    ["you", "Lu Xifeng", "Lu Xifeng", "you"]
                )
                opp = character(_("Lu Xifeng"), 25,
                                sml=[special_move(_("Phantom Claws"), level=10),
                                     special_move(_("Heart Shattering Fist"), level=10),
                                     special_move(_("Five Poison Palm"), level=10),
                                     special_move(_("Shadow Blade"), level=10),
                                     special_move(_("Centipede Poison Powder"))],
                                skills=[skill(_("Phantom Steps"), level=10),
                                        skill(_("Yin Yang Soul Absorption Technique"), level=10),
                                        skill(_("Countering Poison With Poison"), level=10)])

                self.game.battleMenu(self.game.you, opp, "battleground_phantom_sect.png",
                                     "jy_suspenseful2.mp3", fromWin=None,
                                     battleType="task", destinationWinList=[None] * 3,
                                     postBattleCmd=self.post_mini_game)
                return
            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("I don't have time to waste. Tell Lu Xifeng to come out and see me."),
                     _("My Master is busy. What do you want?"),
                     _("The Devil Crescent Moon Blade Manual... Tell him to hand it over..."),
                     _("Hahahahaha! Who do you think you are? Get out of my sight before I turn you into a cactus."),
                     _("Always have to play rough huh... fine then... I'll get rid of you guys and then get it from him."),
                     _("You're looking for death aren't you...")],
                    ["you", "Li Guiping", "you", "Li Guiping", "you", "Li Guiping"]
                )
                self.game.mini_game_in_progress = True
                self.game.mini_game = phantom_sect_mini_game(self.game, self)
                self.game.mini_game.new()
                return

        elif self.game.taskProgressDic["liu_village"] == 19:
            self.game.generate_dialogue_sequence(
                self.game.mapWin,
                "Phantom Sect.png",
                [_("Aren't you going to Huashan to request their help to find the masked men?"),
                 _("You'd better hurry before they get too far...")],
                ["Lu Xifeng", "Lu Xifeng"]
            )
            return
        elif self.game.chivalry >= 0:
            self.game.generate_dialogue_sequence(
                self.game.mapWin,
                "Phantom Sect.png",
                [_("Pshhh... Phantom Sect does not welcome the likes of you."),
                 _("Leave now while I'm still in a good mood; otherwise, I'll use you for slave labor.")],
                ["Li Guiping", "Li Guiping"]
            )
            return

        self.game.stopSoundtrack()
        self.game.currentBGM = "kindaichi 3.mp3"
        self.game.startSoundtrackThread()
        self.game.mapWin.withdraw()
        self.game.phantom_sect_win = Toplevel(self.game.mapWin)
        self.game.phantom_sect_win.title(_("Phantom Sect"))

        bg = PhotoImage(file="Phantom Sect.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.phantom_sect_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        if self.game.taskProgressDic["return_to_phantom_sect"] == 0:
            self.game.phantom_sect_F1 = Frame(canvas)
            pic1 = PhotoImage(file="Li Guiping_icon_large.ppm")
            imageButton1 = Button(self.game.phantom_sect_F1, command=partial(self.clickedOnLiGuiping, False))
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            label = Label(self.game.phantom_sect_F1, text=_("Li Guiping"))
            label.grid(row=1, column=0)
            self.game.phantom_sect_F1.place(x=w // 4 - pic1.width() // 2, y=h // 2)

            if self.game.taskProgressDic["potters_field"] < 0:
                self.game.phantom_sect_F2 = Frame(canvas)
                pic2 = PhotoImage(file="Ren Zhichu_icon_large.ppm")
                imageButton2 = Button(self.game.phantom_sect_F2, command=partial(self.clickedOnRenZhichu, False))
                imageButton2.grid(row=0, column=0)
                imageButton2.config(image=pic2)
                label = Label(self.game.phantom_sect_F2, text=_("Ren Zhichu"))
                label.grid(row=1, column=0)
                self.game.phantom_sect_F2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 2)

        self.game.phantom_sect_win_F1 = Frame(self.game.phantom_sect_win)
        self.game.phantom_sect_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.phantom_sect_win,
                                                 options=["Map", "Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.phantom_sect_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.phantom_sect_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.phantom_sect_win.winfo_width(), self.game.phantom_sect_win.winfo_height()
        self.game.phantom_sect_win.geometry("+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.phantom_sect_win.focus_force()
        self.game.phantom_sect_win.mainloop()  ###


    def post_mini_game(self):

        self.game.mini_game_in_progress = False
        pg.display.quit()

        if self.game.taskProgressDic["join_sect"] == 402 and self.game.taskProgressDic["return_to_phantom_sect"] != 23:
            if self.game.taskProgressDic["return_to_phantom_sect"] == 21:
                self.game.taskProgressDic["return_to_phantom_sect"] = 22
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Ren Zhichu, be careful... He's gotten stronger somehow..."),
                     _("Don't hold back. If we don't kill him, he's going to kill us.")],
                    ["Li Guiping", "Li Guiping"]
                )
                opp_list = []
                opp = character(_("Ren Zhichu"), 12,
                                sml=[special_move(_("Poison Needle"), level=10),
                                     special_move(_("Phantom Claws"), level=4)],
                                skills=[skill(_("Phantom Steps"), level=5)])
                opp_list.append(opp)
                opp = character(_("Li Guiping"), 16,
                                sml=[special_move(_("Poison Needle"), level=10),
                                     special_move(_("Phantom Claws"), level=9)],
                                skills=[skill(_("Phantom Steps"), level=9)])
                opp_list.append(opp)
                self.game.battleMenu(self.game.you, opp_list, "battleground_phantom_sect.png",
                                     "jy_suspenseful2.mp3", fromWin=None,
                                     battleType="task", destinationWinList=[None] * 3,
                                     postBattleCmd=self.post_mini_game)

            elif self.game.taskProgressDic["return_to_phantom_sect"] == 22:
                self.game.taskProgressDic["return_to_phantom_sect"] = 23
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Now now... what should I do with you guys?"),
                     _("WAIT! I'm sorry! I... I had no choice, man... If I didn't throw you into the cave, she would've killed me."),
                     _("It's all her fault! Please don't kill me!"),
                     _("Why do people like you exist? To waste resources?"),
                     _("*You grab Ren Zhichu by the shoulder and hurl his body into the rocks. His head cracks open, and a stream of blood bursts out.*"),
                     _("Ah... satisfying..."),
                     _("You think you can live in peace after killing us? Master will hunt you down... He'll make you pay..."), #Row 2
                     _("Yep, I'm aware... but before that... let me make you pay for all the times you tortured and humiliated me...")],
                    ["you", "Ren Zhichu", "Ren Zhichu", "you", "Blank", "you",
                     "Li Guiping", "you"]
                )
                for i in range(15):
                    self.game.currentEffect = "sfx_poison_dart.mp3"
                    self.game.startSoundEffectThread()
                    time.sleep(.1)

                self.game.currentEffect = "female_scream2.ogg"
                self.game.startSoundEffectThread()

                for i in range(15):
                    self.game.currentEffect = "sfx_poison_dart.mp3"
                    self.game.startSoundEffectThread()
                    time.sleep(.1)

                time.sleep(1)
                messagebox.showinfo("", _("Before leaving, you take one last look at Li Guiping's body, covered in hundreds of needles, and can't help but admire your artwork..."))
                self.game.mapWin.deiconify()


        else:
            if self.game.taskProgressDic["liu_village"] == 16:
                self.game.taskProgressDic["liu_village"] = 17
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("You're not as weak as you look..."),
                     _("Doesn't change the fact that I'll be feeding your dead body to the wolves.")],
                    ["Li Guiping", "Li Guiping"]
                )

                opp = character(_("Li Guiping"), 16,
                                    sml=[special_move(_("Poison Needle"), level=10),
                                         special_move(_("Phantom Claws"), level=9)],
                                    skills=[skill(_("Phantom Steps"), level=9)])

                self.game.battleMenu(self.game.you, opp, "battleground_phantom_sect.png",
                                     "jy_suspenseful2.mp3", fromWin=None,
                                     battleType="task", destinationWinList=[None] * 3, postBattleCmd=self.post_mini_game)

            elif self.game.taskProgressDic["liu_village"] == 17:
                self.game.taskProgressDic["liu_village"] = 18
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Come on, you're too easy..."),
                     _("Your master might be more of a challenge for me."),
                     _("Hahahaha... So we have a guest..."),
                     _("Master! This guy... he..."),
                     _("I know, my dear, I know..."),
                     _("(There's something about Lu Xifeng that makes me feel very uncomfortable... It's not even his mask...)"),
                     _("(His voice is... gentle... yet creepy and intimidating... Not quite what I expected at all...)"), #Row 2
                     _("I heard your demanding for my presence."),
                     _("Yep... I'm here for the Devil Crescent Moon Blade Manual. Hand it over, and I'll leave."),
                     _("Hahahahaha! You fool... Still unaware of the situation you're in right now?"),
                     _("Forget leaving here with anything... whether you can escape alive is highly questionable."),
                     _("You really need to come out of your cave every once in a while."),
                     _("I'm afraid you missed seeing your disciples get their butts kicked by me."), #Row 3
                     _("Even if you are 10 times stronger than that lady, you're probably still no match for me."),
                     _("Did I allow my training to be interrupted just to hear your ostentatious speech?"),
                     _("Itching for action, eh? Bring it...")
                    ],
                    ["you", "you", "Lu Xifeng", "Li Guiping", "Lu Xifeng", "you",
                     "you", "Lu Xifeng", "you", "Lu Xifeng", "Lu Xifeng", "you",
                     "you", "you", "Lu Xifeng", "you"
                     ]
                )

                opp = character(_("Lu Xifeng"), 25,
                                sml=[special_move(_("Phantom Claws"), level=10),
                                     special_move(_("Heart Shattering Fist"), level=10),
                                     special_move(_("Five Poison Palm"), level=10),
                                     special_move(_("Shadow Blade"), level=10),
                                     special_move(_("Centipede Poison Powder"))],
                                skills=[skill(_("Phantom Steps"), level=10),
                                        skill(_("Yin Yang Soul Absorption Technique"), level=10),
                                        skill(_("Countering Poison With Poison"), level=10)])


                self.game.battleMenu(self.game.you, opp, "battleground_phantom_sect.png",
                         "jy_suspenseful2.mp3", fromWin=None,
                         battleType="task", destinationWinList=[None] * 3, postBattleCmd=self.post_mini_game)


            elif self.game.taskProgressDic["liu_village"] == 18:
                self.game.taskProgressDic["liu_village"] = 19
                if _("Yin Yang Soul Absorption Technique") in [s.name for s in self.game.skills]:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Phantom Sect.png",
                        [_("The Yin Yang Soul Absorption Technique... How... How did you learn it???"),
                         _("Well, I have to thank your stupid disciples who locked me in the cave..."),
                         _("... I hid it there; not even Guiying or Guiping knew about it... I suppose it's fate..."),
                         _("Young man, you sacrificed your manhood to learn the technique... Do you have any regrets?"),
                         _("Regrets? Are you kidding me? I'm so powerful now that I can barely find a rival."),
                         _("Enough talk. Give me the Devil Crescent Moon Blade Manual... Once I have that, I'll truly be invincible... HAHAHAHAHA!!!"),
                         _("I'm afraid you came to the wrong place... I don't have it..."), #Row 2
                         _("What?! You're lying! Aren't those men-in-black your people???"),
                         _("All my disciples are here with the exception of Li Guiying, my top disciple."),
                         _("Even I haven't stepped out of Phantom Sect in 5 years..."),
                         _("You mean, those guys who use poison and smoke bombs aren't acting on your orders?"), # Row 3
                         _("I am fond of using poison, but gun powder? That's a disgrace to martial arts..."),
                         _("Then... Then who are those guys??"),
                         _("Where did you encounter them?"),
                         _("First at Liu Village, then at the Dark Forest near Huashan."),
                         _("In that case, why don't you go ask Huashan Sect to help you look for them."),
                         _("After all, I'm sure they are much more familiar with the area than you are..."),  # Row 4
                         _("How do I know you're not lying to me?"),
                         _("I am already the most-hated person by those of the chivalrous sects. Do I need to hide anything?"),
                         _("If a chivalrous sect gets the manual, they will no doubt destroy it."),
                         _("Without it, none of them are a threat to me. Tan Keyong might be a match for me, and the Shaolin Abbot can perhaps last a few bouts against me."),
                         _("Why would I risk becoming a thorn in the side of the evil sects by going after the manual?"),
                         _("Hmmm, what you say does make some sense..."),  # Row 5
                         _("(I'd better go to Huashan and ask for some help...)"),
                         _("Thanks for the information... Bye.")],
                        ["Lu Xifeng", "you", "Lu Xifeng", "Lu Xifeng", "you", "you",
                         "Lu Xifeng", "you", "Lu Xifeng", "Lu Xifeng",
                         "you", "Lu Xifeng", "you", "Lu Xifeng", "you", "Lu Xifeng",
                         "Lu Xifeng", "you", "Lu Xifeng", "Lu Xifeng", "Lu Xifeng", "Lu Xifeng",
                         "you", "you", "you"
                         ]
                    )
                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Phantom Sect.png",
                        [_("Impossible... My Yin Yang Soul Absorption Technique... How could I lose?"),
                         _("You can figure that out on your own later... Now give me the manual..."),
                         _("I'm afraid you came to the wrong place... I don't have it..."),
                         _("What?! You're lying! Aren't those men-in-black your people???"),
                         _("All my disciples are here with the exception of Li Guiying, my top disciple."),
                         _("Even I haven't stepped out of Phantom Sect in 5 years..."),
                         _("You mean, those guys who use poison and smoke bombs aren't acting on your orders?"), #Row 2
                         _("I am fond of using poison, but gun powder? That's a disgrace to martial arts..."),
                         _("Then... Then who are those guys??"),
                         _("Where did you encounter them?"),
                         _("First at Liu Village, then at the Dark Forest near Huashan."),
                         _("In that case, why don't you go ask Huashan Sect to help you look for them."),
                         _("After all, I'm sure they are much more familiar with the area than you are..."), #Row 3
                         _("How do I know you're not lying to me?"),
                         _("I am already the most-hated person by those of the chivalrous sects. Do I need to hide anything?"),
                         _("If a chivalrous sect gets the manual, they will no doubt destroy it."),
                         _("Without it, none of them are a threat to me. Tan Keyong might be a match for me, and the Shaolin Abbot can perhaps last a few bouts against me."),
                         _("Why would I risk becoming a thorn in the side of the evil sects by going after the manual?"),
                         _("Hmmm, what you say does make some sense..."), #Row 4
                         _("(I'd better go to Huashan and ask for some help...)"),
                         _("Thanks for the information... Bye.")
                         ],
                        ["Lu Xifeng", "you", "Lu Xifeng", "you", "Lu Xifeng", "Lu Xifeng",
                         "you", "Lu Xifeng", "you", "Lu Xifeng", "you", "Lu Xifeng",
                         "Lu Xifeng", "you", "Lu Xifeng", "Lu Xifeng", "Lu Xifeng", "Lu Xifeng",
                         "you", "you", "you"
                         ]
                    )

                self.game.mapWin.deiconify()


    def return_to_phantom_sect(self):

        if self.game.taskProgressDic["join_sect"] == 400:
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()
            if self.game.taskProgressDic["return_to_phantom_sect"] == 0:
                self.game.taskProgressDic["return_to_phantom_sect"] = 1
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Oh, look who's back..."),
                     _("Why, did you miss being my slave?"),
                     _("Haha... you crazy woman... If I stayed any longer, I probably would be dead by now."),
                     _("HAHAHAHAHAHAHA"),
                     _("R-Ren Zhichu???"),
                     _("Wait, what???"),
                     _("How the heck did you get out? I thought you'd be dead by now..."), #Row 2
                     _("You useless piece of garbage... who gave you permission to come out of the cave?"),
                     _("HAHAHAHAHAHA... Li Guiping! I've had enough of you!"),
                     _("All these years you've treated me like a slave, abused me, insulted me..."),
                     _("I endured all of it, hoping that one day my situation would change..."),
                     _("Never did I think you would try to kill me, and all because of a cup of spilled tea???"),
                     _("YOU TREAT ME WORSE THAN A DOG!!!"), #Row 3
                     _("That's because you are worthless... Calm down, would you?"),
                     _("You're so angry that your voice sounds high-pitched... almost like a woman! Hahahahaha!"),
                     _("Is this little doggy upset? Awwww... Come on, then... Make me regret my actions~ Hahahahaha~"),
                     _("Die, you witch!")],
                    ["Li Guiping", "Li Guiping", "you", "Ren Zhichu", "Li Guiping", "you",
                     "Li Guiping", "Li Guiping", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu",
                     "Ren Zhichu", "Li Guiping", "Li Guiping", "Li Guiping", "Ren Zhichu"
                     ]
                )

                self.game.currentEffect = "sfx_poison_dart.mp3"
                self.game.startSoundEffectThread()
                time.sleep(.2)
                self.game.currentEffect = "sfx_poison_dart.mp3"
                self.game.startSoundEffectThread()
                time.sleep(1)
                self.game.currentEffect = "sfx_phantom_claws.mp3"
                self.game.startSoundEffectThread()

                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("MY EYES!!!! AHHHHHHHHHHHH!!!"),
                     _("You love to stab people with your needles, don't you? Well, let me treat you to a free acupuncture session...")],
                    ["Li Guiping", "Ren Zhichu"]
                )

                for i in range(15):
                    self.game.currentEffect = "sfx_poison_dart.mp3"
                    self.game.startSoundEffectThread()
                    time.sleep(.1)

                self.game.currentEffect = "female_scream2.ogg"
                self.game.startSoundEffectThread()

                for i in range(15):
                    self.game.currentEffect = "sfx_poison_dart.mp3"
                    self.game.startSoundEffectThread()
                    time.sleep(.1)

                time.sleep(1.2)

                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("AHAHAHAHAHAHAHAHAHA!!!!!"),
                     _("FINALLY!!! I GOT MY REVENGE!!! AHAHAHAHAHAHAHAHAHAHA~~~~"),
                     _("W-wait, how did you kill her so easily? She was much stronger than both of us before..."),
                     _("And how did you manage to get out of the cave?"),
                     _("Oh hey, {}... Sorry about that; I got too excited that I almost forgot you were here...").format(self.game.character),
                     _("That day, when Li Guiping threw me into the cave, I thought I was dead for sure..."),
                     _("But I didn't want to die yet... at the very least, I needed to take Li Guiping down with me..."), #Row 2
                     _("In desperation, I started feeling around the walls of the cave looking for a soft spot."),
                     _("I was lucky and found a hollow spot. After punching through the rocks, I found a tunnel that led to the outside, near the base of the mountain."),
                     _("It appears that God was on my side. Not only did I escape, I found the manual for Phantom Sect's secret technique inside the tunnel..."),
                     _("The Yin Yang Soul Absorption Technique..."),
                     _("This is the same technique that allowed Master to secure his place in the Elite Four."),
                     _("However, to practice this technique, one must first... first..."), #Row 3
                     _("Heh... one needs to undergo castration..."),
                     _("C-castration...??? So that's why your voice..."),
                     _("Yep... but in order to get revenge, I was willing to do anything... HAHAHAHAHAHA..."),
                     _("Being able to kill Li Guiping with my own hands by turning her into a cactus makes everything worth it..."),
                     _("That also explains why Master deals more favorably with female disciples..."),
                     _("He doesn't have to worry about them potentially learning the technique and surpassing him... HAHAHAHAHAHA..."), #Row 4
                     _("{}, now that I've killed that woman, life feels very empty... without a purpose...").format(self.game.character),
                     _("I am no longer a complete man... There's no point in living anymore..."),
                     _("I remember you tried to save me when Li Guiping wanted to throw me into the cave; now's my chance to repay you..."),
                     _("Wait, what?")],
                    ["Ren Zhichu", "Ren Zhichu", "you", "you", "Ren Zhichu", "Ren Zhichu",
                     "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu",
                     "Ren Zhichu", "Ren Zhichu", "you", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu",
                     "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "Ren Zhichu", "you",
                     ]
                )

                self.game.staminaMax += 2000
                messagebox.showinfo("", _("With lightning speed, Ren Zhichu rushes to your side and places his palms on your back. You feel a surge of energy rush into your body.\n\nYour stamina upper limit increased by 2000!"))

                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Even though I can't give you the Yin Yang Soul Absorption Manual, I can pass on my inner energy to you."),
                     _("Thank you, {}, for being the only person in my life who's been kind to me.").format(self.game.character),
                     _("*Ren Zhichu suddenly dashes his head against the rocks at full speed. His head splits open, and a fountain of blood bursts out from the crack.*"),
                     _("*Sigh*... Rest in peace..."),
                     _("*You quickly bury Ren Zhichu and depart from Phantom Sect.*")
                     ],
                    ["Ren Zhichu", "Ren Zhichu", "Blank", "you", "Blank"]
                )

                self.game.mapWin.deiconify()


        elif self.game.taskProgressDic["join_sect"] == 401:
            if self.game.taskProgressDic["return_to_phantom_sect"] == 0:
                self.game.taskProgressDic["return_to_phantom_sect"] = 11
                self.game.stopSoundtrack()
                self.game.currentBGM = "jy_suspenseful1.mp3"
                self.game.startSoundtrackThread()
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Interesting... You actually decided to come back..."),
                     _("Master, I'm sorry... I fell in love with {} while training with him.").format(self.game.character),
                     _("I just want to be with him; nothing else."),
                     _("If you wish, I will relinquish the 10 years of Kung-Fu I've learned and vow never to practice martial arts again!"),
                     _("I taught you everything. I can take it back anytime I want to!"),
                     _("So, what is it that you want then?"),
                     _("Hahaha... What do I want?"), #Row 2
                     _("Guiping, you tell him. What is the punishment for a traitor of Phantom Sect?"),
                     _("Master, please... Please have mercy on us..."),
                     _("Guiping, you've always been one of my favorite disciples... If you kill this brat, I will overlook your offenses."),
                     _("Master, I... I can't do that... I love him and would die for him."),
                     _("In that case, allow me to send both of you to hell...")
                     ],
                    ["Lu Xifeng", "Li Guiping", "Li Guiping", "Li Guiping", "Lu Xifeng", "you",
                     "Lu Xifeng", "Lu Xifeng", "Li Guiping", "Lu Xifeng", "Li Guiping", "Lu Xifeng",]
                )

                opp = character(_("Lu Xifeng"), 25,
                                sml=[special_move(_("Phantom Claws"), level=10),
                                     special_move(_("Heart Shattering Fist"), level=10),
                                     special_move(_("Five Poison Palm"), level=10),
                                     special_move(_("Shadow Blade"), level=10),
                                     special_move(_("Centipede Poison Powder"))],
                                skills=[skill(_("Phantom Steps"), level=10),
                                        skill(_("Yin Yang Soul Absorption Technique"), level=10),
                                        skill(_("Countering Poison With Poison"), level=10)])

                self.game.battleMenu(self.game.you, opp, "battleground_phantom_sect.png",
                                     "jy_suspenseful2.mp3", fromWin=None,
                                     battleType="task", destinationWinList=[None] * 3, postBattleCmd=self.return_to_phantom_sect)


            elif self.game.taskProgressDic["return_to_phantom_sect"] == 11:
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("H-how... How could I lose?"),
                     _("So, that was the Yin Yang Soul Absorption Technique, eh?"),
                     _("Quite powerful, I must admit. If you don't mind... I would like to learn it too, so give me the manual."),
                     _("I no longer have the manual... It went missing around the same time you and Guiping ran away."),
                     _("I thought you guys had stolen it, not that you would practice it now that you and Guiping are married..."),
                     _("What do you mean? Why wouldn't I want to learn such a powerful technique?"),
                     _("HAHAHAHAHAHA... Just trust me and enjoy the pleasures of intimacy with a spouse... something I can never experience..."), #Row 2
                     _("Well, yeah, if you keep wearing that creepy mask, no woman's gonna want to marry you."),
                     _("Hahahaha... one day, you will find out... one day..."),
                     _("Master, take care... {}, let's go...").format(self.game.character)],
                    ["Lu Xifeng", "you", "you", "Lu Xifeng", "Lu Xifeng", "you",
                     "Lu Xifeng", "you", "Lu Xifeng", "Li Guiping"]
                )
                self.game.mapWin.deiconify()


        elif self.game.taskProgressDic["join_sect"] == 402:
            if self.game.taskProgressDic["return_to_phantom_sect"] == 0:
                self.game.taskProgressDic["return_to_phantom_sect"] = 21
                self.game.stopSoundtrack()
                self.game.currentBGM = "jy_suspenseful2.mp3"
                self.game.startSoundtrackThread()
                self.game.generate_dialogue_sequence(
                    None,
                    "Phantom Sect.png",
                    [_("Hahahahaha! Li Guiping! Ren Zhichu!"),
                     _("{}??? How did you...").format(self.game.character),
                     _("You two disgusting human beings... I will not allow you to see the sunrise tomorrow!"),
                     _("You could've lived longer if you had just stayed hidden, but no... you decided to look for trouble..."),
                     _("Listen up, whoever kills {} will be rewarded greatly... Show no mercy...").format(self.game.character)],
                    ["you", "Li Guiping", "you", "Li Guiping", "Li Guiping",]
                )

                self.game.mini_game_in_progress = True
                self.game.mini_game = phantom_sect_mini_game(self.game, self)
                self.game.mini_game.new()