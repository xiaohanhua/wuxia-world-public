from tkinter import *
from character import *
from item import *
from random import *
import time
from gettext import gettext
from functools import partial
from helper_funcs import *
from special_move import *
from skill import *
from mini_game_trade_ship import *
from helper_funcs import*
import tkinter.messagebox as messagebox

_ = gettext


class Scene_sea_shore:

    def __init__(self, game):
        self.game = game
        self.game.sea_shore_win = Toplevel(self.game.mapWin)
        self.game.sea_shore_win.title(_("Sea Shore"))

        bg = PhotoImage(file="Sea Shore.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.sea_shore_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.game.sea_shoreF1 = Frame(canvas)
        pic1 = PhotoImage(file="Sea Merchant_icon_large.ppm")
        imageButton1 = Button(self.game.sea_shoreF1, command=self.talkToSeaMerchant)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.game.sea_shoreF1, text=_("Sea\nMerchant"))
        label.grid(row=1, column=0)
        self.game.sea_shoreF1.place(x=w * 1 // 4 - pic1.width() // 2, y=h // 4 - pic1.height() // 2)

        self.game.sea_shoreF2 = Frame(canvas)
        pic2 = PhotoImage(file="Trade Ship_icon_large.ppm")
        imageButton2 = Button(self.game.sea_shoreF2, command=self.goTradeShip)
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(self.game.sea_shoreF2, text=_("Trade\nShip"))
        label.grid(row=1, column=0)
        self.game.sea_shoreF2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 4 - pic2.height() // 2)

        menu_frame = self.game.create_menu_frame(self.game.sea_shore_win)
        menu_frame.pack()
        self.game.sea_shore_win_F1 = Frame(self.game.sea_shore_win)
        self.game.sea_shore_win_F1.pack()

        self.game.sea_shore_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.sea_shore_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.sea_shore_win.winfo_width(), self.game.sea_shore_win.winfo_height()
        self.game.sea_shore_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.sea_shore_win.focus_force()
        self.game.sea_shore_win.mainloop()  ###


    def talkToSeaMerchant(self):

        if self.game.taskProgressDic["yagyu_clan"] == 5:
            self.game.sea_shore_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Sea Shore.png",
                [_("Greetings! I have some high-quality go--"),
                 _("I'm looking for a skin mask. Got any?"),
                 _("Skin mask, eh? Surely you are aware that the resources and labor required is --"),
                 _("Very expensive? How much you want?"),
                 _("Before that, allow me to ask, what do you need a skin mask for?"),],
                ["Sea Merchant", "you", "Sea Merchant", "you", "Sea Merchant"],
                [[_("I need to disguise as a monk to sneak into Shaolin during the day."), partial(self.sea_merchant_task_dialogue, 11)],
                 [_("My mom says I'm too ugly."), partial(self.sea_merchant_task_dialogue, 21)],
                 [_("It's none of your business!"), partial(self.sea_merchant_task_dialogue, 31)]]
            )
            return

        elif self.game.taskProgressDic["yagyu_clan"] == 6:
            if self.game.check_for_item(_("Dragon Tear")):
                self.game.generate_dialogue_sequence(
                    self.game.sea_shore_win,
                    "Sea Shore.png",
                    [_("*You hand over the Dragon Tear and receive a Skin Mask.*"),
                     _("Pleasure doing business with you, my friend...")],
                    ["Blank", "Sea Merchant"],
                )
                self.game.taskProgressDic["yagyu_clan"] = 7
                self.game.add_item(_("Skin Mask"))
                self.game.add_item(_("Dragon Tear"), -1)
            else:
                self.game.generate_dialogue_sequence(
                    self.game.sea_shore_win,
                    "Sea Shore.png",
                    [_("Did you manage to steal the Dragon Tear from the imperial palace?"),
                     _("Not yet, working on it...")],
                    ["Sea Merchant", "you"],
                )
            return


        if _("Smoke Bomb") not in [m.name for m in self.game.sml] and random() <= .25:
            response = messagebox.askquestion("", _("The Sea Merchant has offered to teach you how to use Smoke Bombs. Pay 1000 Gold to learn? (You currently have {} gold)".format(self.game.inv[_("Gold")])))
            if response == "yes":
                if self.game.inv[_("Gold")] >= 1000:
                    new_move = special_move(name=_("Smoke Bomb"))
                    self.game.learn_move(new_move)
                    self.game.add_item(_("Gold"), -1000)
                    messagebox.showinfo("", _("You pay 1000 Gold to learn a new move: 'Smoke Bomb'."))
                else:
                    messagebox.showinfo("", _("Not enough gold."))
            return

        self.sea_merchant_prices = {
            _("Tin Bar"): 20,
            _("Bronze Bar"): 50,
            _("Steel Bar"): 100,
            _("Spider Poison Powder"): 75,
            _("Centipede Poison Powder"): 100,
            _("Black Iron Bar"): 900,
            _("Silk Skirt"): 500,
            _("Smoke Bomb"): 50,
            _("Emerald"): 600,
            _("Adamantite"): 1200,
            _("Topaz"): 1000,
            _("Chrysocolla"): 1800,
            _("Amethyst"): 2200,
        }

        items = []

        for key, price in self.sea_merchant_prices.items():
            if price > 1000:
                items += [key]
            elif price >= 100:
                items += [key]*2
            else:
                items += [key]*4

        self.r_item = pickOne(items)
        self.r_price = self.sea_merchant_prices[self.r_item]
        self.r_amount = 1
        if self.r_price >= 1000:
            pass
        elif self.r_price >= 500:
            self.r_amount = randrange(1,3)
        else:
            self.r_amount = randrange(10,21)

        self.game.sea_shore_win.withdraw()
        self.game.generate_dialogue_sequence(
            None,
            "Sea Shore.png",
            [_("Greetings! I have some high-quality goods, if you are interested..."),
             _("{} x {} for {} gold, a great bargain! (You currently have {} gold)".format(self.r_item, self.r_amount, self.r_price*self.r_amount, self.game.inv[_("Gold")]))],
            ["Sea Merchant", "Sea Merchant"],
            [[_("Sounds like a deal!"), partial(self.sea_merchant_trade, 1)],
             [_("I'll fight you for it!"), partial(self.sea_merchant_trade, 2)],
             [_("No thanks."), partial(self.sea_merchant_trade, 3)]]
        )


    def sea_merchant_task_dialogue(self, choice):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        if choice == 11:
            self.game.generate_dialogue_sequence(
                None,
                "Sea Shore.png",
                [_("Interesting..."),
                 _("Are you still going to help me?"),
                 _("You see, I am a businessman. I'm only concerned with what I can benefit from the transaction."),
                 _("The only reason I ask is to better understand your needs."),
                 _("As for your intentions, it's none of my business."),
                 _("Smart man, I like it... So, when can I get it, and how much do you charge?"),
                 _("Quick and to the point, I like it. A client of mine has been looking for something for quite a while."),
                 _("He's willing to pay a large sum of money for it..."), #Row 2
                 _("Oh yeah? What's this item?"),
                 _("Dragon's Tear. It's a precious gem that, as far as I know, can only be found in the Imperial Palace."),
                 _("The Emperor received a few over the years as tribute."),
                 _("Get me the Dragon's Tear, and I'll make sure you get the most well-crafted skin mask in the land.")],
                ["Sea Merchant", "you", "Sea Merchant", "Sea Merchant", "Sea Merchant", "you", "Sea Merchant",
                 "Sea Merchant", "you", "Sea Merchant", "Sea Merchant", "Sea Merchant"],
                [[_("Deal! I'll get the gem for you!"), partial(self.sea_merchant_task_dialogue, 12)],
                 [_("Why don't you go steal it yourself?"), partial(self.sea_merchant_task_dialogue, 13)]]
            )

        elif choice == 12:
            self.game.generate_dialogue_sequence(
                None,
                "Sea Shore.png",
                [_("I look forward to doing business with you..."),],
                ["Sea Merchant"],
            )
            self.game.taskProgressDic["yagyu_clan"] = 6
            self.game.sea_shore_win.deiconify()

        elif choice == 13:
            self.game.generate_dialogue_sequence(
                None,
                "Sea Shore.png",
                [_("You see, I'm a businessman, not a thief or robber."),
                 _("I can't risk getting in trouble with the government..."),
                 _("..... You mean to tell me that all those goods you try to sell me aren't from you raiding the trade ships?"),
                 _("Wh-what are you... *ahem*... Like I said, I'm a --"),
                 _("Businessman, yeah, I know..."),
                 _("Yes, good... so have we got a deal or what?"),
                 _("Fine, the mask had better be in good condition... and very versatile..."),
                 _("Worry not! I always ensure my goods are of top quality!")],
                ["Sea Merchant", "Sea Merchant", "you", "Sea Merchant", "you", "Sea Merchant", "you", "Sea Merchant"],
            )
            self.game.taskProgressDic["yagyu_clan"] = 6
            self.game.sea_shore_win.deiconify()

        elif choice == 21:
            self.game.generate_dialogue_sequence(
                None,
                "Sea Shore.png",
                [_("I'm not convinced..."),
                 _("Come on man, you've gotta help me..."),
                 _("I am a businessman. As long as you agree to pay, I see no reason to refuse your request."),
                 _("Alright! So, what's your price?"),
                 _("Dragon's Tear. It's a precious gem that, as far as I know, can only be found in the Imperial Palace."),
                 _("The Emperor received a few over the years as tribute."),
                 _("Get me the Dragon's Tear, and I'll make sure you get the most well-crafted skin mask in the land.")
                 ],
                ["Sea Merchant", "you", "Sea Merchant", "you", "Sea Merchant", "Sea Merchant", "Sea Merchant"],
                [[_("Deal! I'll get the gem for you!"), partial(self.sea_merchant_task_dialogue, 12)],
                 [_("Why don't you go steal it yourself?"), partial(self.sea_merchant_task_dialogue, 13)]]
            )

        elif choice == 31:
            self.game.generate_dialogue_sequence(
                None,
                "Sea Shore.png",
                [_("No questions allowed, eh? Very well."),
                 _("I can get you a skin mask, no problem, but I'll need more than just gold in return."),
                 _("Name your price..."),
                 _("Dragon's Tear. It's a precious gem that, as far as I know, can only be found in the Imperial Palace."),
                 _("The Emperor received a few over the years as tribute."),
                 _("Get me the Dragon's Tear, and I'll make sure you get the most well-crafted skin mask in the land."),
                 _("Deal.")
                 ],
                ["Sea Merchant", "Sea Merchant", "you", "Sea Merchant", "Sea Merchant", "Sea Merchant", "you"]
            )
            self.game.taskProgressDic["yagyu_clan"] = 6
            self.game.sea_shore_win.deiconify()



    def sea_merchant_trade(self, choice):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        if choice == 1:
            if self.game.inv[_("Gold")] >= self.r_price*self.r_amount:
                self.game.add_item(_("Gold"), -self.r_price*self.r_amount)
                self.game.add_item(self.r_item, self.r_amount)
                messagebox.showinfo("", _("You purchase {} x {} for {} gold.").format(self.r_item, self.r_amount, self.r_price*self.r_amount))
            else:
                messagebox.showinfo("", _("Not enough gold."))
        elif choice == 2:
            self.game.generate_dialogue_sequence(
                None,
                "Sea Shore.png",
                [_("Hahahaha! Very well. While doing business, I've met various people who have taught me a few moves."),
                 _("I love making new friends through martial arts! If you defeat me, I'll gift you the goods free of charge!")],
                ["Sea Merchant", "Sea Merchant"]
            )
            opp = character(_("Sea Merchant"), 15,
                              attributeList = [2500, 2500, 1600, 1600, 90, 90, 90, 90, 90],
                              sml=[special_move(_("Smoke Bomb"), level=10),
                                   special_move(_("Falling Cherry Blossom Finger Technique"), level=10),
                                   special_move(_("Scrub Fist"), level=10),
                                   special_move(_("Flee"))],
                              skills=[skill(_("Basic Agility Technique"), level=10),
                                      skill(_("Dragonfly Dance"), level=10)])
            self.game.battleMenu(self.game.you, opp, "Sea Shore.png",
                                 "sea_bg.mp3", fromWin=self.game.sea_shore_win,
                                 battleType="training", destinationWinList=[None] * 3,
                                 postBattleCmd=self.defeated_sea_merchant)
            return

        self.game.sea_shore_win.deiconify()


    def defeated_sea_merchant(self):
        self.game.generate_dialogue_sequence(
            None,
            "Sea Shore.png",
            [_("Impressive! As promised, you can have the goods for free!")],
            ["Sea Merchant"]
        )
        self.game.add_item(self.r_item, self.r_amount)
        messagebox.showinfo("", _("You received {} x {}!").format(self.r_item, self.r_amount))
        self.game.sea_shore_win.deiconify()


    def goTradeShip(self):
        response = messagebox.askquestion("", _("Looks like there's a trade ship at the dock! Rush past the guards and take some of the goods?"))
        if response == "yes":
            self.game.sea_shore_win.quit()
            self.game.sea_shore_win.destroy()

            #self.game.stopSoundtrack()
            #self.game.currentBGM = "jy_daojiangu.mp3"
            #self.game.startSoundtrackThread()
            self.game.mini_game_in_progress = True
            self.game.mini_game = trade_ship_mini_game(self.game, self)
            self.game.mini_game.new()


    def obtainGoods(self):
        rewards_dic = {
            _("Tin Bar"): pickOne([0,0,1,1,2,2,2,3,4]),
            _("Bronze Bar"): pickOne([0,0,1,1,1,2,2,2,3]),
            _("Steel Bar"): pickOne([0,0,1,1,1,2,2]),
            _("Black Iron Bar"): pickOne([0,0,0,0,0,1,1,1,2]),
            _("Emerald"): pickOne([0,0,0,0,0,1,1,1]),
            _("Topaz"): pickOne([0,0,0,0,0,0,0,1,1]),
            _("Adamantite"): pickOne([0,0,0,0,0,0,0,1,1]),
            _("Chrysocolla"): pickOne([0,0,0,0,0,0,0,0,0,0,1,1]),
            _("Amethyst"): pickOne([0,0,0,0,0,0,0,0,0,0,0,0,1]),
            _("Gold"): randrange(100,301),
            _("Smoke Bomb"): pickOne([0,0,10,10,10,20,30])
        }
        reward_str = _("You grab one of the chests and escape from the port. You open it to find:")
        month = calculate_month_day_year(self.game.gameDate)["Month"]
        if month in [2, 9]:
            r = .5
        else:
            r = 1
        for item, amount in rewards_dic.items():
            if amount > 0:
                self.game.add_item(item, amount)
                reward_str += "\n" + item + " x " + str(amount)
            elif random() > r:
                self.game.add_item(item, 1)
                reward_str += "\n" + item + " x " + str(1)
        messagebox.showinfo("", reward_str)
        self.game.mapWin.deiconify()