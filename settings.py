import os
import sys
from gettext import*

def rp(relative_path):
    """Get absolute path to resource, works for dev and for PyInstaller"""
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

RESOURCES_DIR = "Resources"
SAVE_SLOTS_DIR = "SaveSlots"
VERSION = "1.0"
_ = gettext

PORTRAIT_WIDTH = 800
PORTRAIT_HEIGHT = 600
LANDSCAPE_WIDTH = 1000
LANDSCAPE_HEIGHT = 400
FPS = 30
TITLE = "Game"
if os.name == "nt":
    FONT_NAME = rp("freesansbold.ttf")
else:
    FONT_NAME = "freesansbold.ttf"

PLAYER_ACC = .75
PLAYER_FRICTION = -.12
GRAVITY = 1
JUMP_VELOCITY = 18

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GRAY = (128, 128, 128)
MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
UNLEARNABLE_MOVES = ["Dodge", "Rest", "Items", "Flee", "Spear Attack", "Basic Punching Technique", "Real Scrub Fist",
                     "Archery", "Kiss of Death", "Hensojutsu", "Kusarigama", "Superior Smoke Bomb",
                     "Hun Yuan Shapeshifting Fist", "Crossbow", "Yunhuan Taichi Fist"]

UNLEARNABLE_SKILLS = ["Divine Protection", "Marrow Cleansing Technique", "Yin Yang Soul Absorption Technique",
                      "Hun Yuan Yin Qi", "Taichi 18 Forms", "Seductive Dance"]

SKILL_NAMES = ["Cherry Blossoms Floating on the Water", "Shaolin Mind Clearing Method", "Tendon Changing Technique",
                            "Shaolin Inner Energy Technique", "Pure Yang Qi Skill", "Wudang Agility Technique",
                            "Countering Poison With Poison", "Phantom Steps", "Recuperation",
                            "Yin Yang Soul Absorption Technique", "Huashan Agility Technique", "Dragonfly Dance",
                            "Divine Protection", "Leaping Over Mountain Peaks", "Burst of Potential",
                            "Basic Agility Technique", "Spreading Wings", "Flower and Body Unite",
                            "Taichi 18 Forms", "Splitting Mountains and Earth", "Toad Form",
                            "Firm Resolve", "Hun Yuan Yin Qi", "Bushido", "Kenjutsu I", "Kenjutsu II",
                            "Ninjutsu I", "Ninjutsu II", "Ninjutsu III"]


BATTLE_STATUS_DESCRIPTIONS = {
    "Poison": "Poison: At end of each turn, lose 5% health (based on upper limit)\n7.5% and 10% for Poison + and Poison++, respectively.\n\n",
    "Internal Injury": "Internal Injury: At end of each turn, lose 5% stamina (based on upper limit)\n7.5% and 10% for Internal Injury + and Internal Injury++, respectively.\n\n",
    "Bleeding": "Bleeding: At end of each turn, lose health and stamina equal to opponent's strength.\n\n",
    "Seal": "Seal (X): Unable to perform any moves that require stamina for X turns.\n\n",
    "Blind": "Blind (X): Accuracy is reduced by 50% for X turns.\n\n",
    "Death": "Death (X): At the end of each turn, there's an X% chance that character's health will drop to 0 immediately.\n\n"
}

SECT_ATTRIBUTES = {
            "phantom_sect": {"bg":"Phantom Sect.png", "tribute":5000, "treasures":["Phantom Steps Manual", "Shadow Blade Manual", "Five Poison Palm Manual"], "chivalry":0, "rep":"Lu Xifeng"},
            "dragon_sect": {"bg":"Dragon Sect.png", "tribute":8000, "treasures":["Dragon Roar Manual", "Violent Dragon Palm Manual"], "chivalry":100, "rep":"Ouyang Xiong"},
            "shaolin": {"bg":"Shaolin.png", "tribute":4000, "treasures":["Tendon Changing Manual","Marrow Cleansing Manual"], "chivalry":150, "rep":"Abbot"},
            "huashan": {"bg":"Huashan.png", "tribute":3000, "treasures":["Thousand Swords Manual", "Huashan Agility Technique Manual"], "chivalry":60, "rep":"Guo Junda"},
            "wudang": {"bg":"Wudang.png", "tribute":5000, "treasures":["Taichi Fist Manual", "Taichi Sword Manual", "Taichi 18 Forms Manual"], "chivalry":150, "rep":"Zhang Tianjian"},
            "shanhu_sect": {"bg":"Shanhu Sect.png", "tribute":7000, "treasures":["Shanhu Fist Manual", "Divine Protection Manual", "Demon Suppressing Blade Manual"], "chivalry":150, "rep":"Tan Keyong"},
        }

PICKPOCKET_ITEMS = {
    "Abbot": { "Luohan Fist Manual":(1,2,.1), "Shaolin Inner Energy Manual":(1,2,.1), "Small Recovery Pill": (1,3,.5), "Gold":(1,6,1)},
    "Su Ling": {"Big Recovery Pill": (1,2,.1), "Small Recovery Pill": (1,3,.5), "Snow Lotus Pill": (1,2,.3), "Bezoar Pill":(1,3,1)},
    "Li Jinyi": {"Spider Poison Powder": (3, 6, .25), "Centipede Poison Powder": (3, 6, .25), "Elderberry":(2,4,1)},
    "Scrub": {"Linen Shirt":(1,2,.1), "Gold":(30,51,1)},
    "Sea Merchant": {"Topaz":(1,2,.1), "Emerald":(1,2,.2), "Smoke Bomb":(5,11,.5), "Steel Bar":(2,4,.5), "Gold":(50,101,1)},
    "Li Guiping": {"Leather Boots":(1,2,.1), "Poison Needle Manual":(1,2,.1), "Spider Poison Powder":(1,3,1)},
    "Ouyang Nana": {"Wool Boots":(1,2,.1), "Silk Skirt":(1,2,.3), "Gold":(50,81,1)},
    "Jiang Yuqi": {"Amethyst Necklace":(1,2,.05), "Wool Boots":(1,2,.1), "Silk Fan":(1,2,.4), "Gold":(30,51,1)},
}


ITEM_DESCRIPTIONS = {_("Gold"): _("I need this to purchase stuff."),
            _("Crab"): _("Mmmmm, a tasty crab!\n\nRestores 60 health and 60 stamina"),
            _("Fish"): _("I hope I don't choke while eating this...\n\nRestores 80 health and 20 stamina"),
            _("Soup"): _("A warm bowl of soup!\n\nRestores 70 health"),
            _("Dumplings"): _("Pork dumplings, my favorite!\n\nRestores 100 stamina"),
            _("Wine"): _("Use in battle: increases strength by 10, lowers attack by 2."),
            _("Tea Eggs"): _("Freshly boiled tea eggs marinated in quality tea leaves and herbs.\n\nRestores 300 health and 300 stamina"),
            _("Cherry Blossom Island Map"): _("With this map, I will no longer get lost in the Cherry Blossom Maze!"),
            _("Cherry Blossom Papaya Tremella Soup"): _("A healthy soup made with special ingredients\n\nRestores 25% health and 40% stamina"),
            _("Venomous Centipede"): _("Even a tiny amount of venom from this creature is lethal."),
            _("Venomous Spider"): _("Even a tiny amount of venom from this creature is lethal."),
            _("Snake Heart"): _("Freshly harvested from a snake.\n\nIncreases health upper limit by 10."),
            _("Bear Bile"): _("Freshly harvested from a bear.\n\nIncreases stamina upper limit by 15."),
            _("Poisonous Moth1"): _("One of the 5 most poisonous moths in the land.\nIt is terrifying to even look at them."),
            _("Poisonous Moth2"): _("One of the 5 most poisonous moths in the land.\nIt is terrifying to even look at them."),
            _("Poisonous Moth3"): _("One of the 5 most poisonous moths in the land.\nIt is terrifying to even look at them."),
            _("Poisonous Moth4"): _("One of the 5 most poisonous moths in the land.\nIt is terrifying to even look at them."),
            _("Poisonous Moth5"): _("One of the 5 most poisonous moths in the land.\nIt is terrifying to even look at them."),
            _("Shaolin Diamond Finger Manual"): _("Use 3 upgrade points to learn 'Shaolin Diamond Finger'.\n\nRequires 85+ perception."),
            _("Shapeless Palm Manual"): _("Use 3 upgrade points to learn 'Shapeless Palm'.\n\nRequires 70+ perception."),
            _("Guan Yin Palm Manual"): _("Use 3 upgrade points to learn 'Guan Yin Palm'.\n\nRequires 85+ perception."),
            _("Prajna Palm Manual"): _("Use 5 upgrade points to learn 'Prajna Palm'.\n\nRequires 85+ perception."),
            _("Sumeru Palm Manual"): _("Use 5 upgrade points to learn 'Sumeru Palm'.\n\nRequires 95+ perception."),
            _("Taichi Fist Manual"): _("Use 3 upgrade points to learn 'Taichi Fist'.\n\nRequires 85+ perception."),
            _("Taichi Sword Manual"): _("Use 3 upgrade points to learn 'Taichi Sword'.\n\nRequires 85+ perception."),
            _("Five Poison Palm Manual"): _("Use 5 upgrade points to learn 'Five Poison Palm'.\n\nRequires 85+ perception."),
            _("Thousand Swords Manual"): _("Use 2 upgrade points to learn 'Thousand Swords Technique'.\n\nRequires 60+ perception."),
            _("Dragonfly Dance Manual"): _("Use 3 upgrade points to learn 'Dragonfly Dance'.\n\nRequires 70+ perception."),
            _("Huashan Agility Technique Manual"): _("Use 3 upgrade points to learn 'Huashan Agility Technique'.\n\nRequires 75+ perception."),
            _("Eagle Claws Manual"): _("Use 3 upgrade points to learn 'Eagle Claws'.\n\nRequires 75+ perception."),
            _("Devil Crescent Moon Blade Manual"): _("Use 10 upgrade points to learn 'Devil Crescent Moon Blade'.\n\nRequires -150 chivalry or lower."),
            _("Small Recovery Pill"): _("Restores 30% health"),
            _("Big Recovery Pill"): _("Restores 50% health"),
            _("Bezoar Pill"): _("Cures 'Poison' status"),
            _("Spider Poison Powder"): _("75% chance of poisoning opponent."),
            _("Centipede Poison Powder"): _("Reduces opponent's stamina by 50% of upper limit."),
            _("Rainbow Fragrance"): _("A colorful powder made from a variety of flowers. Due to its mesmerizing scent,\nthe Rainbow Fragrance can be used as an aphrodisiac in small amounts\nor as a debilitating weapon against enemies."),
            _("Snow Lotus Pill"): _("Restores 20% health and sets status to 'Normal'."),
            _("Yin Yang Soul Absorption Manual"): _("'Phantom Sect's most powerful inner energy technique.\nFirst, one must undergo castration...'"),
            _("Hundred Seeds Pill"): _("Increases a random attribute/stat by 3."),
            _("Snow Lotus"): _("A rare flower that grows only in extremely cold climates.\nIts flowers and roots are used to make expensive medicine."),
            _("Bamboo Flute"): _("Xiao Yong's bamboo flute."),
            _("Gold Necklace"): _("Jia Wen's gold necklace. I could probably sell this for a lot of money."),
            _("Zither"): _("A beautiful stringed instrument."),
            _("Smoke Bomb"): _("May not be the most honorable weapon\nbut a very powerful one nevertheless."),
            _("Shaolin Inner Energy Manual"): _("Use 3 upgrade points to learn 'Shaolin Inner Energy Technique'."),
            _("Shaolin Mind Clearing Manual"): _("Use 3 upgrade points to learn 'Shaolin Mind Clearing Technique'."),
            _("Shaolin Luohan Fist Manual"): _("Use 3 upgrade points to learn 'Shaolin Luohan Fist'."),
            _("Poison Needle Manual"): _("Use 3 upgrade points to learn 'Poison Needle'."),
            _("Empty Force Fist Manual"): _("Use 5 upgrade points to learn 'Empty Force Fist'.\n\nRequires 85+ perception."),
            _("Demon Suppressing Blade Manual"): _("Use 4 upgrade points to learn 'Demon Suppressing Blade'.\n\nRequires 80+ perception."),
            _("Dragon Roar Manual"): _("Use 5 upgrade points to learn 'Dragon Roar'.\n\nRequires 85+ perception."),
            _("Shadow Blade Manual"): _("Use 5 upgrade points to learn 'Shadow Blade'.\n\nRequires 80+ perception."),
            _("Shanhu Fist Manual"): _("Use 6 upgrade points to learn 'Shanhu Fist'.\n\nRequires 90+ perception."),
            _("Violent Dragon Palm Manual"): _("Use 6 upgrade points to learn 'Violent Dragon Palm'.\n\nRequires 90+ perception."),
            _("Phantom Steps Manual"): _("Use 3 upgrade points to learn 'Phantom Steps'.\n\nRequires 75+ perception."),
            _("Divine Protection Manual"): _("Use 8 upgrade points to learn 'Divine Protection'.\n\nRequires 100+ perception."),
            _("Taichi 18 Forms Manual"): _("Use 8 upgrade points to learn 'Taichi 18 Forms'.\n\nRequires 90+ perception."),
            _("Tendon Changing Manual"): _("Use 8 upgrade points to learn 'Tendon Changing Technique'.\n\nRequires 90+ perception."),
            _("Marrow Cleansing Manual"): _("Use 8 upgrade points to learn 'Marrow Cleansing Technique'.\n\nRequires 100+ perception."),
            _("Sunstone"): _("A rare gem that glows like the sun.\n\nBonuses during forgery:\n\n+80% chance for [Enhanced]\n+50% chance for [Rare]\n+25% chance for [Ultra Rare]"),
            _("Dragon Tear"): _("A pearl shaped like the tear of a dragon.\n\nBonuses during forgery:\n\n+100% chance for [Enhanced]\n+60% chance for [Rare]\n+30% chance for [Ultra Rare]"),
            _("Emerald"): _("A precious gemstone.\n\nBonuses during forgery:\n\n+50% chance for [Enhanced]"),
            _("Silk Skirt"): _("What woman would not want one of these?"),
            _("Topaz"): _("A precious gemstone.\n\nBonuses during forgery:\n\n+50% chance for [Enhanced]\n+20% chance for [Rare]"),
            _("Chrysocolla"): _("A precious gemstone.\n\nBonuses during forgery:\n\n+75% chance for [Rare]"),
            _("Amethyst"): _("A precious gemstone.\n\nBonuses during forgery:\n\n+75% chance for [Ultra Rare]"),
            _("Tin Bar"): _("A common metal used to forge weapons/armor."),
            _("Bronze Bar"): _("A common metal used to forge weapons/armor."),
            _("Steel Bar"): _("A common metal used to forge weapons/armor."),
            _("Black Iron Bar"): _("A rare metal used to forge weapons/armor."),
            _("Adamantite"): _("A rare ore that can be smelted to form the toughest metal on earth."),
            _("Caterpillar Fungus"): _("An expensive fungus that provides great health benefits when consumed.\n\nIncreases health and stamina upper limits by 100."),
            _("Elderberry"): _("Though often used for medicinal purposes, these berries are poisonous if consumed raw."),
            _("Poison Ivy"): _("I probably shouldn't touch this..."),
            _("White Snakeroot"): _("A poisonous herb that is pleasant to the eyes but not the mouth."),
            _("Spiral Aloe Leaf"): _("Leaves of an endangered plant that is toxic to humans."),
            _("Stinging Nettle"): _("Prickly leaves!"),
            _("Sack"): _("Wonder what's in here. Fruit? Vegetables? Rice? Should I open it?"),
            _("Winterberry"): _("A tasty berry that grows in extremely cold climates.\n\nRestores 500 health."),
            _("Thousand Year Ginseng Extract"): _("Made with 1000-year-old ginseng.\n\nIncreases health and stamina upper limits by 500."),
            _("Yagyu Family Tree"): _("A detailed family tree of the renowned Yagyu Muneyoshi."),
            _("Dungeon Key"): _("Hey to the entrance of the secret Yagyu/Hattori Dungeon."),
            _("Skin Mask"): _("I'm not sure I feel comfortable wearing this."),
            _("A-Xiu's Shoes"): _("A-Xiu's delicately crafted shoes. Cai Wuqiong will pay me a lot for these."),
        }

ITEM_SELLING_PRICES = {_("Crab"): 15,
            _("Fish"): 9,
            _("Soup"): 12,
            _("Dumplings"): 10,
            _("Wine"): 15,
            _("Tea Eggs"): 30,
            _("Snake Heart"): 5,
            _("Bear Bile"): 10,
            _("Small Recovery Pill"): 250,
            _("Big Recovery Pill"): 800,
            _("Bezoar Pill"): 100,
            _("Spider Poison Powder"): 50,
            _("Centipede Poison Powder"): 50,
            _("Rainbow Fragrance"): 100,
            _("Poisonous Moth1"): 40,
            _("Poisonous Moth2"): 30,
            _("Poisonous Moth3"): 75,
            _("Poisonous Moth4"): 35,
            _("Poisonous Moth5"): 50,
            _("Snow Lotus Pill"): 400,
            _("Hundred Seeds Pill"): 1000,
            _("Snow Lotus"): 300,
            _("Gold Necklace"): 2000,
            _("Smoke Bomb"): 50,
            _("Sunstone"): 2500,
            _("Dragon Tear"): 3000,
            _("Emerald"): 600,
            _("Silk Skirt"): 300,
            _("Topaz"): 1000,
            _("Chrysocolla"): 1800,
            _("Amethyst"): 2200,
            _("Tin Bar"): 20,
            _("Bronze Bar"): 50,
            _("Steel Bar"): 100,
            _("Black Iron Bar"): 900,
            _("Adamantite"): 1200,
            _("Caterpillar Fungus"): 1500,
            _("Elderberry"): 300,
            _("Poison Ivy"): 90,
            _("White Snakeroot"): 240,
            _("Spiral Aloe Leaf"): 210,
            _("Stinging Nettle"): 130,
            _("Winterberry"): 500,
            _("Thousand Year Ginseng Extract"): 15000,
        }

