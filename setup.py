from cx_Freeze import setup, Executable
import os

resource_dir = os.path.join(os.getcwd(), "Resources")
setup(name='Wuxia World',
      version='1.0',
      description='Official',
      executables = [Executable("main.py", icon=os.path.join(resource_dir,"game_main_icon.ico"))])

#C:\Users\xndl1\AppData\Local\Programs\Python\Python36-32\python.exe setup.py build