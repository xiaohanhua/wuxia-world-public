from tkinter import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
from mini_game_winterberry import*
import tkinter.messagebox as messagebox

_ = gettext

class Scene_shanhu_sect_train:

    def __init__(self, game):
        self.game = game
        self.game.days_spent_in_training = int((self.game.taskProgressDic["join_sect"] - 1) * 100)
        try:
            if self.game.taskProgressDic["join_sect"] == 1.00:
                try:
                    self.game.shanhu_sect_train_win = Toplevel(self.game.scrubHouseWin)
                except:
                    self.game.shanhu_sect_train_win = Toplevel(self.game.continueGameWin)

            else:
                self.game.shanhu_sect_train_win = Toplevel(self.game.continueGameWin)
        except:
            self.game.shanhu_sect_train_win = Toplevel(self.game.continueGameWin)

        self.game.shanhu_sect_train_win.title(_("Shanhu Sect"))

        bg = PhotoImage(file="Shanhu Sect.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.shanhu_sect_train_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Zhu Shi Ting_icon_large.ppm")
        imageButton1 = Button(F1, command=partial(self.clickedOnZhuShiTing, True))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("Zhu Shi Ting"))
        label.grid(row=1, column=0)
        F1.place(x=w // 4 - pic1.width() // 2, y=h // 2)

        F2 = Frame(canvas)
        pic2 = PhotoImage(file="Tan Keyong_icon_large.ppm")
        imageButton2 = Button(F2, command=partial(self.clickedOnTanKeyong, True))
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(F2, text=_("Tan Keyong"))
        label.grid(row=1, column=0)
        F2.place(x=w // 2 - pic1.width() // 2, y=h // 2)

        F3 = Frame(canvas)
        pic3 = PhotoImage(file="Yang Kai_icon_large.ppm")
        imageButton3 = Button(F3, command=partial(self.clickedOnYangKai, True))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        label = Label(F3, text=_("Yang Kai"))
        label.grid(row=1, column=0)
        F3.place(x=w * 3 // 4 - pic1.width() // 2, y=h // 2)

        self.game.days_spent_in_training_label = Label(self.game.shanhu_sect_train_win,
                                                       text=_("Days trained: {}").format(
                                                           self.game.days_spent_in_training))
        self.game.days_spent_in_training_label.pack()
        self.game.shanhu_sect_train_win_F1 = Frame(self.game.shanhu_sect_train_win)
        self.game.shanhu_sect_train_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.shanhu_sect_train_win,
                                                 options=["Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.shanhu_sect_train_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.shanhu_sect_train_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.shanhu_sect_train_win.winfo_width(), self.game.shanhu_sect_train_win.winfo_height()
        self.game.shanhu_sect_train_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.shanhu_sect_train_win.focus_force()
        self.game.shanhu_sect_train_win.mainloop()  ###


    def clickedOnZhuShiTing(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.shanhu_sect_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shanhu_sect_train_win_F1, text=_("Spar (Zhu Shi Ting)"),
                   command=partial(self.shanhu_sect_train_spar, targ=_("Zhu Shi Ting"))).grid(row=1, column=0)
            Button(self.game.shanhu_sect_train_win_F1, text=_("Learn"),
                   command=partial(self.shanhu_sect_train_learn, "Zhu Shi Ting")).grid(row=2, column=0)

        else:
            for widget in self.game.shanhu_sect_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shanhu_sect_win_F1, text=_("Spar (Zhu Shi Ting)"),
                   command=partial(self.shanhu_sect_train_spar, targ=_("Zhu Shi Ting"), training=False)).grid(row=1, column=0)
            Button(self.game.shanhu_sect_win_F1, text=_("Talk"),
                   command=self.talk_to_zhu_shi_ting).grid(row=2, column=0)


    def talk_to_zhu_shi_ting(self):
        r = randrange(2)
        if r == 0:
            self.game.generate_dialogue_sequence(
                self.game.shanhu_sect_win,
                "Shanhu Sect.png",
                [_("Turn from evil and do good; seek peace and pursue it.")],
                ["Zhu Shi Ting"]
            )
        elif r == 1:
            self.game.generate_dialogue_sequence(
                self.game.shanhu_sect_win,
                "Shanhu Sect.png",
                [_("Music can calm the mind and soothe the soul.")],
                ["Zhu Shi Ting"]
            )



    def clickedOnTanKeyong(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.shanhu_sect_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shanhu_sect_train_win_F1, text=_("Spar (Tan Keyong)"),
                   command=partial(self.shanhu_sect_train_spar, targ=_("Tan Keyong"))).grid(row=1, column=0)
            Button(self.game.shanhu_sect_train_win_F1, text=_("Learn"),
                   command=partial(self.shanhu_sect_train_learn, "Tan Keyong")).grid(row=2, column=0)
            Button(self.game.shanhu_sect_train_win_F1, text=_("Tell me about this sect"),
                   command=partial(self.talk_to_tan_keyong, 1)).grid(row=3, column=0)
            Button(self.game.shanhu_sect_train_win_F1, text=_("Who are these 2 friends of yours?"),
                   command=partial(self.talk_to_tan_keyong, 2)).grid(row=4, column=0)

        else:
            for widget in self.game.shanhu_sect_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shanhu_sect_win_F1, text=_("Spar (Tan Keyong)"),
                   command=partial(self.shanhu_sect_train_spar, targ=_("Tan Keyong"), training=False)).grid(row=1, column=0)
            Button(self.game.shanhu_sect_win_F1, text=_("Talk"),
                   command=partial(self.talk_to_tan_keyong, 3)).grid(row=2, column=0)


    def talk_to_tan_keyong(self, choice):
        if choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.shanhu_sect_train_win,
                "Shanhu Sect.png",
                [_("Shanhu Sect's history began 25 years ago; it was founded by Master Liu Qingfeng."),
                 _("The Sect's primary purpose is to help the poor and oppressed and to combat the evil within the Martial Arts Community."),
                 _("Over the years, whether for money, fame, or power, many sects have turned from the ways of chivalry."),
                 _("They will sacrifice honor to rise to the top."),
                 _("One of our toughest enemies at the moment is the Phantom Sect. Their leader, Lu Xifeng, is cruel and practices perverse techniques."),
                 _("Even I cannot defeat him easily."),
                 _("Why don't all the righteous sects team up against him?"),
                 _("Every sect has their own priorities. Shaolin is a strong ally but are peaceful in nature."),
                 _("They will not go on the offensive."),
                 _("Wudang also has plenty on their plate and cannot divert too much of their resources to supress Phantom Sect.")],
                ["Tan Keyong", "Tan Keyong", "Tan Keyong", "Tan Keyong", "Tan Keyong", "Tan Keyong", "you",
                 "Tan Keyong", "Tan Keyong", "Tan Keyong"]
            )

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.shanhu_sect_train_win,
                "Shanhu Sect.png",
                [_("They've been with me through some tough times and are the 2 Elders of Shanhu Sect."),
                 _("They are both skilled in martial arts and have excellent character."),
                 _("When I first assumed leadership of the sect, they helped me tremendously with management and logistics."),
                 _("Elder Yang's swordsmanship is topnotch and well-respected in the Martial Arts Community."),
                 _("Elder Zhu's inner energy cultivation skills are excellent, but his musical knowledge is even more impressive."),
                 _("Sometimes when I am stressed, he plays the zither to help me relax."),
                 _("You should try to listen sometime... It's quite soothing."),
                 _("On occasion, when our dear friend Xiao Yong, who resides in the nearby mountains, visits, they play a duet."),
                 _("Elder Zhu on the zither, and Xiao Yong on the flute... I always look forward to those duets!")],
                ["Tan Keyong", "Tan Keyong", "Tan Keyong", "Tan Keyong", "Tan Keyong", "Tan Keyong",
                 "Tan Keyong", "Tan Keyong", "Tan Keyong"]
            )

        elif choice == 3:
            r = randrange(3)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_win,
                    "Shanhu Sect.png",
                    [_("One who has unreliable friends soon comes to ruin."),
                     _("Be careful who you surround yourself with.")],
                    ["Tan Keyong", "Tan Keyong"]
                )

            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_win,
                    "Shanhu Sect.png",
                    [_("The highway of the upright avoids evil; those who guard their ways preserve their lives.")],
                    ["Tan Keyong"]
                )

            elif r == 2:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_win,
                    "Shanhu Sect.png",
                    [_("Many have chosen the path of evil in a frivolous pursuit of power..."),
                     _("They end up losing those closest to them in the process.")],
                    ["Tan Keyong", "Tan Keyong"]
                )


    def clickedOnYangKai(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.shanhu_sect_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shanhu_sect_train_win_F1, text=_("Spar (Yang Kai)"),
                   command=partial(self.shanhu_sect_train_spar, targ=_("Yang Kai"))).grid(row=1, column=0)
            Button(self.game.shanhu_sect_train_win_F1, text=_("Ask for task"),
                   command=partial(self.shanhu_sect_train_task)).grid(row=2, column=0)

        else:
            for widget in self.game.shanhu_sect_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shanhu_sect_win_F1, text=_("Spar (Yang Kai)"),
                   command=partial(self.shanhu_sect_train_spar, targ=_("Yang Kai"), training=False)).grid(row=1, column=0)
            Button(self.game.shanhu_sect_win_F1, text=_("Talk"),
                   command=self.talk_to_yang_kai).grid(row=2, column=0)
            Button(self.game.shanhu_sect_win_F1, text=_("Look for winterberry"),
                   command=self.look_for_winterberry).grid(row=3, column=0)
            if self.game.taskProgressDic["feng_pan"] == 70:
                Button(self.game.shanhu_sect_win_F1, text=_("How's Xinyi doing?"),
                       command=partial(self.talk_to_yang_kai, 1)).grid(row=4, column=0)



    def talk_to_yang_kai(self, choice=None):
        if choice == 1:
            if random() <= .5:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_win,
                    "Shanhu Sect.png",
                    [_("She's gotten much stronger! I think she's in the middle of meditation right now."),
                     _("Want me to get her?"),
                     _("Ah, it's alright. Don't disturb her this time."),
                     _("Tell her I said hi. I'll come visit again soon!")],
                    ["Yang Kai", "Yang Kai", "you", "you"]
                )

            else:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_win,
                    "Shanhu Sect.png",
                    [_("She's doing well! Yesterday, we--"),
                     _("{}! Great to see you again!").format(self.game.character),
                     _("Hehehe, likewise, old friend!"),
                     _("Have you been uh... visiting a certain place recently to erm, read up on some... books?"),
                     _("Ah, that... um... (come on, not so loud!)"),
                     _("Hmmm? What are you guys talking about?"),
                     _("Ahhhh nothing important hahaha... anyway, I gotta get back to what I was doing."),
                     _("I'll catch you guys later!")],
                    ["Yang Kai", "Feng Xinyi", "you", "Feng Xinyi", "you", "Yang Kai", "you", "you"]
                )

        else:
            r = randrange(2)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_win,
                    "Shanhu Sect.png",
                    [_("Love must be sincere. Hate what is evil; cling to what is good.")],
                    ["Yang Kai"]
                )

            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_win,
                    "Shanhu Sect.png",
                    [_("One of my favorite drinks is winterberry tea."),
                     _("You can eat the fruit by itself too for some health benefits."),],
                    ["Yang Kai", "Yang Kai"]
                )


    def look_for_winterberry(self):
        self.game.shanhu_sect_win.withdraw()
        self.game.generate_dialogue_sequence(
            None,
            "Shanhu Sect.png",
            [_("You want to look for winterberries?"),
             _("You can find them on the snow mountains around here. Let me take you to a good starting spot."),
             _("The path is icy and slippery, so be very careful!")],
            ["Yang Kai", "Yang Kai", "Yang Kai"]
        )
        self.game.mini_game_in_progress = True
        self.game.mini_game = winterberry_mini_game(self.game, self, False)
        self.game.mini_game.new()



    def shanhu_sect_train_learn(self, learnFrom):

        if learnFrom == "Tan Keyong":
            if self.game.perception < 70:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_train_win,
                    "Shanhu Sect.png",
                    [_("Your perception is not high enough to learn what I want to teach you."),
                     _("Please come back when you are ready.")],
                    ["Tan Keyong", "Tan Keyong"]
                )

            elif _("Shanhu Fist") not in [m.name for m in self.game.sml]:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_train_win,
                    "Shanhu Sect.png",
                    [_("Very well, let me teach you the Shanhu Fist."),
                     _("Thank you, Master Tan!")],
                    ["Tan Keyong", "you"]
                )
                new_move = special_move(name=_("Shanhu Fist"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("You learned a new move: Shanhu Fist!"))

            else:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_train_win,
                    "Shanhu Sect.png",
                    [_("Practice what I've taught you diligently."),
                     _("Also, spend some time with Elder Zhu and Elder Yang."),
                     _("You will benefit much from interacting with them.")],
                    ["Tan Keyong", "Tan Keyong", "Tan Keyong"]
                )

        elif learnFrom == "Zhu Shi Ting":
            if random() <= .25 and _("Zither") not in self.game.inv:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_train_win,
                    "Shanhu Sect.png",
                    [_("I see you are quite interested in music."),
                     _("Instead of always talking about theory, why don't you learn how to play?"),
                     _("Here, take this zither and learn with me."),],
                    ["Zhu Shi Ting", "Zhu Shi Ting", "Zhu Shi Ting"]
                )
                self.game.inv[_("Zither")] = 1
                messagebox.showinfo("", _("Received 'Zither' x 1!"))

            elif random() <= .33:
                self.game.staminaMax += 30
                messagebox.showinfo("", _("You practice inner energy cultivation with Zhu Shi Ting. Your stamina upper limit increased by 30!"))
            else:
                self.game.perception += 2
                messagebox.showinfo("", _("You learn some music theory with Zhu Shi Ting. Your perception increased by 2!"))

            self.shanhu_sect_training_update(.02)


    def shanhu_sect_train_task(self):
        self.game.shanhu_sect_train_win.withdraw()
        self.game.generate_dialogue_sequence(
            None,
            "Shanhu Sect.png",
            [_("I need some winterberries to make tea."),
             _("Can you fetch some for me from the nearby snow mountains please?"),
             _("Be careful not to slip and fall though."),
             _("No problem!")],
            ["Yang Kai", "Yang Kai", "Yang Kai", "you"]
        )
        self.game.mini_game_in_progress = True
        self.game.mini_game = winterberry_mini_game(self.game, self)
        self.game.mini_game.new()


    def found_winterberry(self, training=True):
        self.game.mini_game_in_progress = False
        pg.display.quit()
        self.game.currentBGM = "jy_ambient3.mp3"
        self.game.startSoundEffectThread()

        if training:
            self.game.generate_dialogue_sequence(
                self.game.shanhu_sect_train_win,
                "Shanhu Sect.png",
                [_("Here you go!"),
                 _("Thank you, {}!").format(self.game.character)],
                ["you", "Yang Kai"]
            )

            if random() <= .25 and _("Leaping Over Mountain Peaks") not in [s.name for s in self.game.skills]:
                self.game.generate_dialogue_sequence(
                    self.game.shanhu_sect_train_win,
                    "Shanhu Sect.png",
                    [_("Say, you've been taking quite a big risk getting the winterberries for me!"),
                     _("Here, let me teach you something to make it easier for you.")],
                    ["Yang Kai", "Yang Kai"]
                )
                self.game.speed += 3
                self.game.dexterity += 3
                self.game.learn_skill(skill(_("Leaping Over Mountain Peaks")))
                messagebox.showinfo("", _("Yang Kai teaches you a new skill: Leaping Over Mountain Peaks!"))

            else:
                r = pickOne([1,1,2])
                self.game.speed += r
                self.game.dexterity += r
                messagebox.showinfo("", _("With the experience gained from ascending the mountain, you feel more agile.\nYour speed and dexterity +{}!").format(r))

            self.shanhu_sect_training_update(.01)

        else:
            self.game.add_item(_("Winterberry"))
            self.game.shanhu_sect_win.deiconify()


    def shanhu_sect_train_spar(self, targ, training=True):

        if training:
            if self.game.taskProgressDic["join_sect"] < 100 and self.game.taskProgressDic["join_sect"]-1 < .59 and random() <= .5:
                self.shanhu_sect_training_update(.01)

            if targ == _("Tan Keyong"):
                opp = character(_("Tan Keyong"), 20,
                      sml=[special_move(_("Shanhu Fist"), level=10),
                           special_move(_("Basic Punching Technique"), level=10)],
                      skills=[skill(_("Divine Protection"), level=10),
                              skill(_("Leaping Over Mountain Peaks"), level=10)])

            elif targ == _("Zhu Shi Ting"):
                opp = character(_("Zhu Shi Ting"), 16,
                                sml=[special_move(_("Shanhu Fist"), level=9),
                                     special_move(_("Basic Punching Technique"), level=10),
                                     special_move(_("Soothing Song"))],
                                skills=[skill(_("Divine Protection"), level=8),
                                        skill(_("Leaping Over Mountain Peaks"), level=8)])

            elif targ == _("Yang Kai"):
                opp = character(_("Yang Kai"), 16,
                                sml=[special_move(_("Shanhu Fist"), level=5),
                                     special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Demon Suppressing Blade"), level=10)],
                                skills=[skill(_("Divine Protection"), level=5),
                                        skill(_("Leaping Over Mountain Peaks"), level=10)])

            else:
                opp = None

            self.game.battleMenu(self.game.you, opp,"battleground_shanhu_sect.png",
                            "jy_ambient3.mp3",fromWin = self.game.shanhu_sect_train_win,
                            battleType = "training", destinationWinList = [self.game.shanhu_sect_train_win] * 3)


        else:
            year = calculate_month_day_year(self.game.gameDate)["Year"]

            if targ == _("Tan Keyong"):
                opp = character(_("Tan Keyong"), min([22+year, 24]),
                      sml=[special_move(_("Shanhu Fist"), level=10),
                           special_move(_("Demon Suppressing Blade"), level=10)],
                      skills=[skill(_("Divine Protection"), level=10),
                              skill(_("Leaping Over Mountain Peaks"), level=10)])

            elif targ == _("Zhu Shi Ting"):
                opp = character(_("Zhu Shi Ting"), min([18+year, 21]),
                                sml=[special_move(_("Shanhu Fist"), level=9),
                                     special_move(_("Basic Punching Technique"), level=10)],
                                skills=[skill(_("Divine Protection"), level=8),
                                        skill(_("Leaping Over Mountain Peaks"), level=8)])

            elif targ == _("Yang Kai"):
                opp = character(_("Yang Kai"), min([18+year,21]),
                                sml=[special_move(_("Shanhu Fist"), level=5),
                                     special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Demon Suppressing Blade"), level=10)],
                                skills=[skill(_("Divine Protection"), level=5),
                                        skill(_("Leaping Over Mountain Peaks"), level=10)])

            else:
                opp = None

            self.game.battleMenu(self.game.you, opp,"battleground_shanhu_sect.png",
                            "jy_ambient3.mp3",fromWin = self.game.shanhu_sect_win,
                            battleType = "training", destinationWinList = [self.game.shanhu_sect_win] * 3)


    def shanhu_sect_training_update(self,increment):
        self.game.taskProgressDic["join_sect"] += increment
        self.game.taskProgressDic["join_sect"] = round(self.game.taskProgressDic["join_sect"],2)
        self.game.days_spent_in_training = int(self.game.taskProgressDic["join_sect"]*100-100)
        self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
        if self.game.days_spent_in_training >= 60:
            self.game.days_spent_in_training = 60
            self.game.taskProgressDic["join_sect"] = 1.6
            self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
            self.game.shanhu_sect_train_win.withdraw()

            self.game.generate_dialogue_sequence(
                None,
                "Shanhu Sect.png",
                [_("Time sure does fly. I can't believe it's time for you to go!"),
                 _("Yeah, I know, right? I definitely learned a lot here!"),
                 _("Thank you all so much for your kindness!"),
                 _("You're most welcome. Just remember, you can come back and visit anytime."),
                 _("Take care, {}.").format(self.game.character),
                 _("Very glad to have met you, {}.").format(self.game.character),
                 _("Thank you! I'll be back to visit you guys soon!")],
                ["Tan Keyong", "you", "you", "Tan Keyong", "Yang Kai", "Zhu Shi Ting", "you"]
            )

            self.game.taskProgressDic["join_sect"] = 200
            self.game.generateGameWin()


class Scene_shanhu_sect(Scene_shanhu_sect_train):
    def __init__(self, game):
        self.game = game
        self.game.shanhu_sect_win = Toplevel(self.game.mapWin)
        self.game.shanhu_sect_win.title(_("Shanhu Sect"))

        bg = PhotoImage(file="Shanhu Sect.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.shanhu_sect_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Zhu Shi Ting_icon_large.ppm")
        imageButton1 = Button(F1, command=partial(self.clickedOnZhuShiTing, False))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("Zhu Shi Ting"))
        label.grid(row=1, column=0)
        F1.place(x=w // 4 - pic1.width() // 2, y=h // 2)

        F2 = Frame(canvas)
        pic2 = PhotoImage(file="Tan Keyong_icon_large.ppm")
        imageButton2 = Button(F2, command=partial(self.clickedOnTanKeyong, False))
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(F2, text=_("Tan Keyong"))
        label.grid(row=1, column=0)
        F2.place(x=w // 2 - pic1.width() // 2, y=h // 2)

        F3 = Frame(canvas)
        pic3 = PhotoImage(file="Yang Kai_icon_large.ppm")
        imageButton3 = Button(F3, command=partial(self.clickedOnYangKai, False))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        label = Label(F3, text=_("Yang Kai"))
        label.grid(row=1, column=0)
        F3.place(x=w * 3 // 4 - pic1.width() // 2, y=h // 2)


        self.game.shanhu_sect_win_F1 = Frame(self.game.shanhu_sect_win)
        self.game.shanhu_sect_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.shanhu_sect_win,
                                                 options=["Map", "Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.shanhu_sect_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.shanhu_sect_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.shanhu_sect_win.winfo_width(), self.game.shanhu_sect_win.winfo_height()
        self.game.shanhu_sect_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.shanhu_sect_win.focus_force()
        self.game.shanhu_sect_win.mainloop()  ###