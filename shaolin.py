from tkinter import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
from mini_game_luohan_formation import*
import tkinter.messagebox as messagebox


_ = gettext

class Scene_shaolin_train:

    def __init__(self, game):
        self.game = game
        self.game.days_spent_in_training = int((self.game.taskProgressDic["join_sect"] - 4) * 100)
        try:
            if self.game.taskProgressDic["join_sect"] == 4.0:
                try:
                    self.game.shaolin_train_win = Toplevel(self.game.scrubHouseWin)
                except:
                    self.game.shaolin_train_win = Toplevel(self.game.continueGameWin)

            else:
                self.game.shaolin_train_win = Toplevel(self.game.continueGameWin)
        except:
            self.game.shaolin_train_win = Toplevel(self.game.continueGameWin)

        self.game.shaolin_train_win.title(_("Shaolin Temple"))

        bg = PhotoImage(file="Shaolin.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.shaolin_train_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Elder Xuanjing_icon_large.ppm")
        imageButton1 = Button(F1, command=partial(self.clickedOnElderXuanjing, True))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("Elder Xuanjing"))
        label.grid(row=1, column=0)
        F1.place(x=w // 4 - pic1.width()//2, y=h//2)

        if self.game.taskProgressDic["li_guiping_task"] < 3:
            F2 = Frame(canvas)
            pic2 = PhotoImage(file="Elder Xuansheng_icon_large.ppm")
            imageButton2 = Button(F2, command=partial(self.clickedOnElderXuansheng, True))
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            label = Label(F2, text=_("Elder Xuansheng"))
            label.grid(row=1, column=0)
            F2.place(x=w // 2 - pic1.width()//2, y=h//2)

        F3 = Frame(canvas)
        pic3 = PhotoImage(file="Xu Qing_icon_large.ppm")
        imageButton3 = Button(F3, command=partial(self.clickedOnXuQing, True))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        label = Label(F3, text=_("Xu Qing"))
        label.grid(row=1, column=0)
        F3.place(x=w * 3// 4 - pic1.width()//2, y=h//2)

        self.game.days_spent_in_training_label = Label(self.game.shaolin_train_win,
                                                  text=_("Days trained: {}").format(self.game.days_spent_in_training))
        self.game.days_spent_in_training_label.pack()
        self.game.shaolin_train_win_F1 = Frame(self.game.shaolin_train_win)
        self.game.shaolin_train_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.shaolin_train_win,
                                            options=["Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.shaolin_train_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.shaolin_train_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.shaolin_train_win.winfo_width(), self.game.shaolin_train_win.winfo_height()
        self.game.shaolin_train_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.shaolin_train_win.focus_force()
        self.game.shaolin_train_win.mainloop()###


    def clickedOnElderXuanjing(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        if training:
            for widget in self.game.shaolin_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shaolin_train_win_F1, text=_("Spar (Elder Xuanjing)"),
                   command=partial(self.shaolin_train_spar, targ=_("Elder Xuanjing"))).grid(row=1, column=0)
            Button(self.game.shaolin_train_win_F1, text=_("Learn"),
                   command=partial(self.shaolin_train_learn)).grid(row=2, column=0)

        else:
            for widget in self.game.shaolin_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shaolin_win_F1, text=_("Spar (Elder Xuanjing)"),
                   command=partial(self.shaolin_train_spar, targ=_("Elder Xuanjing"), training=False)).grid(row=1, column=0)
            if self.game.taskProgressDic["luohanzhen"] >= 0:
                Button(self.game.shaolin_win_F1, text=_("Challenge the Luohan Formation"), command=self.luohanzhen).grid(row=2, column=0)


    def luohanzhen(self):
        self.game.shaolin_win.withdraw()
        self.game.generate_dialogue_sequence(
            None,
            "Shaolin.png",
            [_("So, you've decided to challenge the Luohan Formation! You must be very careful, {}.".format(self.game.character)),
             _("The 18 Luohan Monks are hand picked by the Abbot himself. You must not underestimate them."),
             _("Many challengers have failed and left with broken bones, or worse, became handicapped for the rest of their lives."),
             _("I hope you came prepared... Good luck!")],
            ["Elder Xuanjing", "Elder Xuanjing", "Elder Xuanjing", "Elder Xuanjing"]
        )

        self.game.stopSoundtrack()
        self.game.currentBGM = "luohanzhen.mp3"
        self.game.startSoundtrackThread()
        self.game.mini_game_in_progress = True
        self.game.mini_game = luohan_formation_mini_game(self.game, self)
        self.game.mini_game.new()


    def luohanzhen_rewards(self):
        self.game.mini_game_in_progress = False
        pg.display.quit()
        self.game.stopSoundtrack()
        self.game.currentBGM = "jy_shaolin1.mp3"
        self.game.startSoundtrackThread()
        if self.game.taskProgressDic["luohanzhen"] < 10:
            self.game.taskProgressDic["luohanzhen"] += 1


        rewards = [_("Empty Force Fist Manual"), _("Shaolin Diamond Finger Manual"), _("Tendon Changing Manual"),
                   _("Guan Yin Palm Manual"), _("Guan Yin Palm Manual"), _("Empty Force Fist Manual"),
                   _("Shaolin Diamond Finger Manual"), _("Marrow Cleansing Manual"),
                   #TODO: Shaolin Agility Manual
                   ]
        r = choice(rewards)
        count = 0
        while self.game.check_for_item(r) and count <= 30:
            r = choice(rewards)
            count += 1

        if self.game.taskProgressDic["luohanzhen"] == 10:
            self.game.taskProgressDic["luohanzhen"] = 20
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Amitabha, {}. You are truly gifted in martial arts.".format(self.game.character)),
                 _("I now give you the option of joining the Prajna Hall or the Dharma Institute."),
                 _("Regardless of which you choose, you will be given a chance to learn the top Shaolin techniques.")],
                ["Elder Xuanjing", "Elder Xuanjing", "Elder Xuanjing"],
                [[_("Join the Prajna Hall."), self.join_prajna_hall],
                 [_("Join the Dharma Institute."), self.join_dharma_institute]]
            )

        elif self.game.check_for_item(r):
            r_amount = randrange(4, 7)
            self.game.add_item(_("Big Recovery Pill"), r_amount)
            messagebox.showinfo("", _("Received 'Big Recovery Pill' x {}!").format(r_amount))

        else:
            self.game.add_item(r)
            messagebox.showinfo("", _("Received '{}'!").format(r))

        self.game.shaolin_win.deiconify()


    def join_prajna_hall(self):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        self.game.generate_dialogue_sequence(
            None,
            "Shaolin.png",
            [_("Very well. I now give you the Prajna Palm Manual."),
             _("Rarely anyone is gifted enough to learn this powerful technique, but you may be that fortunate person."),
             _("Thank you, Elder Xuanjing! I'll do my best!"),
             _("Received 'Prajna Palm Manual' x 1!")],
            ["Elder Xuanjing", "Elder Xuanjing", "you", "Blank"],
        )
        self.game.add_item(_("Prajna Palm Manual"))
        self.game.shaolin_win.deiconify()


    def join_dharma_institute(self):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        self.game.generate_dialogue_sequence(
            None,
            "Shaolin.png",
            [_("Very well. I now give you the Sumeru Palm Manual."),
             _("Rarely anyone is gifted enough to learn this powerful technique, but you may be that fortunate person."),
             _("Thank you, Elder Xuanjing! I'll do my best!"),
             _("Received 'Sumeru Palm Manual' x 1!")],
            ["Elder Xuanjing", "Elder Xuanjing", "you", "Blank"],
        )
        self.game.add_item(_("Sumeru Palm Manual"))
        self.game.shaolin_win.deiconify()


    def clickedOnElderXuansheng(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        if training:
            for widget in self.game.shaolin_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shaolin_train_win_F1, text=_("Spar (Elder Xuansheng)"),
                   command=partial(self.shaolin_train_spar, targ=_("Elder Xuansheng"))).grid(row=1, column=0)
            Button(self.game.shaolin_train_win_F1, text=_("Medidate"),
                   command=partial(self.shaolin_train_meditate)).grid(row=2, column=0)

        else:
            for widget in self.game.shaolin_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shaolin_win_F1, text=_("Spar (Elder Xuansheng)"),
                   command=partial(self.shaolin_train_spar, targ=_("Elder Xuansheng"), training=False)).grid(row=1, column=0)


    def clickedOnXuQing(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        if training:
            for widget in self.game.shaolin_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shaolin_train_win_F1, text=_("Spar (Xu Qing)"),
                   command=partial(self.shaolin_train_spar, targ=_("Xu Qing"))).grid(row=1, column=0)
            Button(self.game.shaolin_train_win_F1, text=_("Talk"), command=self.talkToXuQing).grid(row=2, column=0)

        else:
            for widget in self.game.shaolin_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.shaolin_win_F1, text=_("Spar (Xu Qing)"),
                   command=partial(self.shaolin_train_spar, targ=_("Xu Qing"), training=False)).grid(row=1, column=0)
            Button(self.game.shaolin_win_F1, text=_("Talk"), command=partial(self.talkToXuQing, False)).grid(row=2, column=0)
            Button(self.game.shaolin_win_F1, text=_("Tease"), command=self.teaseXuQing).grid(row=3, column=0)


    def teaseXuQing(self):

        r = randrange(3)
        if r == 0:
            self.game.generate_dialogue_sequence(
                self.game.shaolin_win,
                "Shaolin.png",
                [_("I heard there's a restaurant nearby with delicious seafood and wine."),
                 _("Want to come? My treat!"),
                 _("Amitabha... We cannot eat meat. It contributes to the killing of another life."),
                 _("Please, do consider vegetarian options... Amitabha...")],
                ["you", "you", "Xu Qing", "Xu Qing"]
            )

        elif r == 1:
            self.game.generate_dialogue_sequence(
                self.game.shaolin_win,
                "Shaolin.png",
                [_("Hey, want me to introduce you to some pretty girls?"),
                 _("Amitabha, I'm not allowed to leave the Shaolin temple..."),
                 _("That's ok; I can sneak them into the temple~"),
                 _("Ah, no, I... I can't... Amitabha..."),
                 _("Must abstain from lustful thoughts...")],
                ["you", "Xu Qing", "you", "Xu Qing", "Xu Qing"]
            )

        elif r == 2:
            if _("Dumplings") in self.game.inv:
                if self.game.inv[_("Dumplings")] > 0:
                    self.game.add_item(_("Dumplings"), -1)
                    self.game.generate_dialogue_sequence(
                        self.game.shaolin_win,
                        "Shaolin.png",
                        [_("*You take out a pork dumpling and bite into it slowly.*"),
                         _("Mmmmm~ so juicy~ soooo savory~~~ mmmmmm~~"),
                         _("Amitabha... Amitabha..."),],
                        ["Blank", "you", "Xu Qing"]
                    )
                else:
                    self.game.generate_dialogue_sequence(
                        self.game.shaolin_win,
                        "Shaolin.png",
                        [_("I bet you've never had pork dumplings before. One day, I'll bring you some!"),
                         _("Amitabha... Please don't do that, sir..."), ],
                        ["you", "Xu Qing"]
                    )
            else:
                self.game.generate_dialogue_sequence(
                    self.game.shaolin_win,
                    "Shaolin.png",
                    [_("I bet you've never had pork dumplings before. One day, I'll bring you some!"),
                     _("Amitabha... Please don't do that, sir..."), ],
                    ["you", "Xu Qing"]
                )


    def talkToXuQing(self, training=True):
        r = randrange(2)
        if training:
            fromWin = self.game.shaolin_train_win
        else:
            fromWin = self.game.shaolin_win

        if self.game.taskProgressDic["li_guiping_task"] >= 3:
            self.game.generate_dialogue_sequence(
                fromWin,
                "Shaolin.png",
                [_("So cruel.. I can't believe someone would murder Elder Xuansheng..."),
                 _("He was such a nice man...")],
                ["Xu Qing", "Xu Qing"]
            )

        elif r == 0:
            self.game.generate_dialogue_sequence(
                fromWin,
                "Shaolin.png",
                [_("Have you ever had mushroom, carrots, and tofu with rice?"),
                 _("It's delicious!")],
                ["Xu Qing", "Xu Qing"]
            )

        elif r == 1:
            self.game.generate_dialogue_sequence(
                fromWin,
                "Shaolin.png",
                [_("Amitabha... As monks, we are not permitted to eat meat."),
                 _("You should try a vegetarian diet. It's great for your health!")],
                ["Xu Qing", "Xu Qing"]
            )



    def shaolin_train_spar(self, targ, training=True):

        if training:
            if self.game.taskProgressDic["join_sect"] < 100 and self.game.taskProgressDic["join_sect"]-4 < .59 and random() <= .5:
                self.shaolin_training_update(.01)

            if targ == _("Elder Xuanjing"):
                opp = character(_("Elder Xuanjing"), 14,
                      sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                           special_move(_("Shaolin Diamond Finger"), level=8)],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=6),
                              skill(_("Shaolin Inner Energy Technique"), level=10),
                              skill(_("Shaolin Agility Technique"), level=10),])
            elif targ == _("Elder Xuansheng"):
                opp = character(_("Elder Xuansheng"), 12,
                      sml=[special_move(_("Shaolin Luohan Fist"), level=8),
                           special_move(_("Shaolin Diamond Finger"), level=6)],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                              skill(_("Shaolin Inner Energy Technique"), level=10),
                              skill(_("Shaolin Agility Technique"), level=10),])
            elif targ == _("Xu Qing"):
                opp = character(_("Xu Qing"), min([8, self.game.level]),
                      sml=[special_move(_("Shaolin Luohan Fist"), level=3),
                           special_move(_("Basic Punching Technique"), min([10, self.game.level]))],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=min([self.game.level, 4])),
                              skill(_("Shaolin Inner Energy Technique"), level=4)])
            else:
                opp = None

            self.game.battleMenu(self.game.you, opp,"Shaolin.png",
                            "jy_shaolin1.mp3",fromWin = self.game.shaolin_train_win,
                            battleType = "training", destinationWinList = [self.game.shaolin_train_win] * 3)

        else:
            year = calculate_month_day_year(self.game.gameDate)["Year"]
            if targ == _("Elder Xuanjing"):
                opp = character(_("Elder Xuanjing"), min([16+year*1, 19]),
                      sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                           special_move(_("Shaolin Diamond Finger"), level=9)],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=8),
                              skill(_("Shaolin Inner Energy Technique"), level=10),
                              skill(_("Shaolin Agility Technique"), level=10),])
            elif targ == _("Elder Xuansheng"):
                opp = character(_("Elder Xuansheng"), min([14+year*1, 17]),
                      sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                           special_move(_("Shaolin Diamond Finger"), level=8)],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                              skill(_("Shaolin Inner Energy Technique"), level=10),
                              skill(_("Shaolin Agility Technique"), level=10),])
            elif targ == _("Xu Qing"):
                opp = character(_("Xu Qing"), min([9+year*2, 13]),
                      sml=[special_move(_("Shaolin Luohan Fist"), level=5),
                           special_move(_("Basic Punching Technique"), 10)],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=5),
                              skill(_("Shaolin Inner Energy Technique"), level=5)])
            else:
                opp = None

            self.game.battleMenu(self.game.you, opp,"Shaolin.png",
                            "jy_shaolin1.mp3",fromWin = self.game.shaolin_win,
                            battleType = "training", destinationWinList = [self.game.shaolin_win] * 3)


    def shaolin_train_meditate(self):

        r = random()
        if r <= .15 and _("Shaolin Mind Clearing Method") not in [s.name for s in self.game.skills]:
            self.game.learn_skill(skill(_("Shaolin Mind Clearing Method")))
            messagebox.showinfo("", _("After spending some time in meditation with Elder Xuansheng, you learned a new skill: Shaolin Mind Clearing Method!"))
        else:
            self.game.perception += 1
            messagebox.showinfo("", _("You spend 2 days meditating with Elder Xuansheng. Your perception increased by 1!"))

        self.shaolin_training_update(.02)

    def shaolin_train_learn(self):

        if _("Shaolin Luohan Fist") not in [m.name for m in self.game.sml]:
            self.game.generate_dialogue_sequence(
                self.game.shaolin_train_win,
                "Shaolin.png",
                [_("I will teach you the Shaolin Luohan Fist. Practice diligently.")],
                ["Elder Xuanjing"]
            )

            new_move = special_move(name=_("Shaolin Luohan Fist"))
            print(_("You learned a new move: Shaolin Luohan Fist!"))
            self.game.learn_move(new_move)
            self.shaolin_training_update(.01)

        else:

            if self.game.perception >= 70 and _("Shaolin Inner Energy Technique") not in [s.name for s in self.game.skills]:
                self.game.learn_skill(skill(_("Shaolin Inner Energy Technique")))
                messagebox.showinfo("", _("Elder Xuanjing teaches you a new skill: Shaolin Inner Energy Technique!"))
                self.game.staminaMax += 10

            else:
                self.game.generate_dialogue_sequence(
                    self.game.shaolin_train_win,
                    "Shaolin.png",
                    [_("Life is not all about martial arts. Perhaps some meditation will be beneficial for you...")],
                    ["Elder Xuanjing"]
                )


    def shaolin_training_update(self,increment):
        self.game.taskProgressDic["join_sect"] += increment
        self.game.taskProgressDic["join_sect"] = round(self.game.taskProgressDic["join_sect"],2)
        self.game.days_spent_in_training = int(self.game.taskProgressDic["join_sect"]*100-400)
        self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
        if self.game.days_spent_in_training >= 60:
            self.game.days_spent_in_training = 60
            self.game.taskProgressDic["join_sect"] = 4.6
            self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
            self.game.shaolin_train_win.withdraw()

            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Your 60-day training period has come to an end. I hope you've used this time wisely."),
                 _("Before I send you off, let me test your skills to see how much progress you've made..."),
                 _("Don't worry; I will not go all out on you.")],
                ["Abbot", "Abbot", "Abbot"]
            )

            self.game.restore(0, 0, full=True)

            opp = character(_("Abbot"), 20,
                            sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                                 special_move(_("Shaolin Diamond Finger"), level=10),
                                 special_move(_("Empty Force Fist"), level=8),],
                            skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                    skill(_("Tendon Changing Technique"), level=5),
                                    skill(_("Shaolin Inner Energy Technique"), level=10),
                                    skill(_("Shaolin Agility Technique"), level=10),])


            self.game.battleID = "ShaolinTrainingTest"
            self.game.battleMenu(self.game.you, opp, "Shaolin.png",
                            postBattleSoundtrack="jy_shaolin1.mp3",
                            fromWin=self.game.shaolin_train_win,
                            battleType="test", destinationWinList=[self.game.shaolin_train_win] * 3,
                            postBattleCmd=self.shaolin_training_reward
                            )


    def shaolin_training_reward(self):
        self.game.taskProgressDic["join_sect"] = 500
        self.game.taskProgressDic["luohanzhen"] = 0

        total_levels = 0
        for move in self.game.sml:
            if "Shaolin" in move.name:
                total_levels += move.level
        for skill in self.game.skills:
            if "Shaolin" in skill.name:
                total_levels += skill.level

        self.game.restore(0, 0, full=True)
        if self.game.luck * total_levels / 10 >= 95:
            self.game.generate_dialogue_sequence(
                self.game.shaolin_train_win,
                "Shaolin.png",
                [_("Hmmmm, I am very pleased with the progress you've made."),
                 _("Here's the manual for the Shaolin Diamond Finger. Study it carefully and practice diligently."),
                 _("Thank you, Abbot! Your kindness is overwhelming!"),
                 _("Don't mention it; I hope you will use your martial arts to do good, protect the weak, and help the poor."),
                 _("If you wish, you can return at any time to challenge the Luohan Formation."),
                 _("If you are able to defeat the 18 Luohan Monks, I will reward you greatly."),
                 _("Take care, {}!".format(self.game.character)),
                 _("Don't worry, Abbot; I won't disappoint you!"),
                 _("Farewell, Elder Xuanjing, Elder Xuansheng, and Xu Qing!")],
                ["Abbot", "Abbot", "you", "Abbot", "Abbot", "Abbot", "Abbot", "you", "you"]
            )

            self.game.inv[_("Shaolin Diamond Finger Manual")] = 1
            print(_("Received Shaolin Diamond Finger Manual!"))

        elif self.game.luck * total_levels / 10 >= 80:
            self.game.generate_dialogue_sequence(
                self.game.shaolin_train_win,
                "Shaolin.png",
                [_("Hmmmm, I am very pleased with the progress you've made."),
                 _("Here, take these 3 Small Recovery Pills, Shaolin's acclaimed medicine."),
                 _("Thank you, Abbot! Your kindness is overwhelming!"),
                 _("Don't mention it; I hope you will use your martial arts to do good, protect the weak, and help the poor."),
                 _("If you wish, you can return at any time to challenge the Luohan Formation."),
                 _("If you are able to defeat the 18 Luohan Monks, I will reward you greatly."),
                 _("Take care, {}!".format(self.game.character)),
                 _("Don't worry, Abbot; I won't disappoint you!"),
                 _("Farewell, Elder Xuanjing, Elder Xuansheng, and Xu Qing!")],
                ["Abbot", "Abbot", "you", "Abbot", "Abbot", "Abbot", "Abbot", "you", "you"]
            )

            self.game.inv[_("Small Recovery Pill")] = 3
            print(_("Received Small Recovery Pill x 3!"))


        else:
            self.game.generate_dialogue_sequence(
                self.game.shaolin_train_win,
                "Shaolin.png",
                [_("Hmmmm, I am fairly disappointed with the progress you've made."),
                 _("However, if you wish, you can return at any time to challenge the Luohan Formation."),
                 _("If you are able to defeat the 18 Luohan Monks, I will reward you greatly."),
                 _("Take care, {}!".format(self.game.character)),
                 _("Sorry, Abbot, I should've worked harder, but when I return, I won't disappoint you again!"),
                 _("Thank you for allowing me to train here for the past 2 months."),
                 _("Farewell, Elder Xuanjing, Elder Xuansheng, and Xu Qing!")],
                ["Abbot", "Abbot", "Abbot", "Abbot", "you", "you", "you"]
            )


        self.game.shaolin_train_win.quit()
        self.game.shaolin_train_win.destroy()
        self.game.generateGameWin()



class Scene_shaolin(Scene_shaolin_train):
    def __init__(self, game):
        self.game = game

        if self.game.taskProgressDic["yagyu_clan"] == -4:
            time.sleep(.5)
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Amitabha... Welcome to Shaolin. You've... *Cough*... traveled far to be here and... *Cough*"),
                 _("We're most honored to have... *Cough*... you as guests..."),
                 _("So, this is the renowned Shaolin Temple, eh?"),
                 _("We've long admired your fame even overseas. Most joyous day!"),
                 _("Amitabha... You flatter us. It is not often that we have foreign friends visit us. Please, come in for a light meal and some tea."),
                 _("Thanks, but that won't be necessary, Elder Xuan Jing. I'm sure you have many things to attend to."),
                 _("That's right. We traveled all the way here not to taste some tofu and vegetables, but rather, we wanted to witness the power of Shaolin Kung-Fu."),
                 _("I am unskilled and perhaps considered unworthy to be your opponent, but even if I lose miserably, I will have no regrets."), #Row 2
                 _("Indeed, even if we were to lose, we would have a unique experience to share with our friends and family back home."),
                 _("I fear you may have come at an inopportune... *Cough*... time..."),
                 _("I've not been feeling too... *Cough*... well lately, so I'm afraid I am unable to spar at the moment."),
                 _("What happened Abbot? Why are you feeling unwell?"),
                 _("A couple of days ago, someone poisoned Abbot's food. We were unable to catch the culprit."),
                 _("Luckily, Abbot used his Tendon Changing Technique to heal himself, but now, he is greatly weakened."),
                 _("It will likely take at least another 3-4 weeks before his health is fully restored."), #Row 3
                 _("We're terribly sorry to hear that, Abbot. We wish you a speedy recovery; however, we never intended to spar with you."),
                 _("We are merely students of the Yagyu clan; our status deems us unworthy to spar with you."),
                 _("We know that Shaolin is full of hidden experts. If we could even spar with perhaps a young monk, we would be satisfied."),
                 _("What you propose is reasonable... In that case, Xu Qing."),
                 _("Yes, Elder Xuanjing."),
                 _("You are my student, and I've seen you make tremendous progress over the years."),
                 _("Why don't you spar with our guests here?"), #Row 4
                 _("Be sure to demonstrate all that you've learned, but also, be careful not to hurt anyone."),
                 _("Yes, Elder Xuanjing."),
                 _("Excellent! Thank you, Elder Xuanjing, for giving us this opportunity."),
                 _("Xu-san, please allow me to get a taste of Shaolin Kung-Fu.")
                 ],
                ["Abbot", "Abbot", "Yagyu Tadashi", "Yagyu Tadashi", "Elder Xuanjing", "Yagyu Tadashi", "Ashikaga Yoshiro",
                 "Ashikaga Yoshiro", "Yamamoto Takeshi", "Abbot", "Abbot", "Yagyu Tadashi", "Elder Xuanjing", "Elder Xuanjing",
                 "Elder Xuanjing", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Elder Xuanjing", "Xu Qing", "Elder Xuanjing",
                 "Elder Xuanjing", "Elder Xuanjing", "Xu Qing", "Yamamoto Takeshi", "Yamamoto Takeshi"
                 ]
            )

            you = character(_("Xu Qing"), 13,
                      sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                           special_move(_("Basic Punching Technique"), 10),
                           special_move(_("Rest"))],
                      skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                              skill(_("Shaolin Inner Energy Technique"), level=10)])

            opp = character(_("Yamamoto Takeshi"), 19,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Hachiman Blade"), level=4)],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Kenjutsu II"), level=5)],
                            preferences=[2, 4, 2, 3, 2])

            self.game.battleID = "shaolin_yagyu_spar"

            self.game.battleMenu(you, opp, "Shaolin.png",
                                 "jy_suspenseful1.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[None] * 3,
                                 postBattleCmd=self.post_yagyu_clan_battle)

            return


        elif self.game.taskProgressDic["yagyu_clan"] == -13:
            self.game.taskProgressDic["yagyu_clan"] = -14
            time.sleep(.5)
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("{}! Something terrible has happened... Abbot is missing...").format(self.game.character),
                 _("Wait, what happened?!"),
                 _("I.. I'm not sure... One of the young monks found Abbot's door open when delivering breakfast in the morning."),
                 _("We've conducted a search of the temple and the surrounding forest, but nothing..."),
                 _("I think I know who might be behind this... I'll investigate and let you know if I find anything."),
                 ],
                ["Elder Xuanjing", "you", "Elder Xuanjing", "Elder Xuanjing", "you"
                 ]
            )
            self.game.mapWin.deiconify()
            return

        elif self.game.taskProgressDic["yagyu_clan"] == -14:
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("I think I know who might be behind this... I'll investigate and let you know if I find anything."),
                 _("Thank you, {}!").format(self.game.character)
                 ],
                ["you", "Elder Xuanjing"]
            )
            self.game.mapWin.deiconify()
            return

        elif self.game.taskProgressDic["yagyu_clan"] == -15:
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Any news?"),
                 _("I'm very close. Give me a bit more time, and I'll have a plan!"),
                 _("(I'd better go see if Kasumi found the key first...)")
                 ],
                ["Elder Xuanjing", "you", "you"]
            )
            self.game.mapWin.deiconify()
            return

        elif self.game.taskProgressDic["yagyu_clan"] == -16:
            time.sleep(.5)
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful2.mp3"
            self.game.startSoundtrackThread()
            self.game.taskProgressDic["yagyu_clan"] = -17
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Elder Xuanjing, the situation does not look good at all..."),
                 _("What happened?? Tell me."),
                 _("The Yagyu Clan has taken the Abbot captive with the help of Hattori ninjas..."),
                 _("They've locked him up in a hidden dungeon, but I know how to get in now."),
                 _("Hattori ninjas... I've heard that they are highly skilled at sabotage and assassinations."),
                 _("But it seems that they are even scarier than they're described in the legends..."),
                 _("A couple of them tried to assassinate me. They're fairly strong."), #Row 2
                 _("We must go rescue the Abbot right away!"),
                 _("Yes, but we need a plan... We're dealing with both the Hattori ninjas and the Yagyu samurai."),
                 _("Not going to be easy..."),
                 _("I propose that you and the rest of the Shaolin monks distract the Yagyu samurai while I sneak in."),
                 _("Though my disciples may not be as strong as the Yagyu samurai, we outnumber them by quite a bit."),
                 _("So we should be able to keep them busy for a long time."),
                 _("Perfect, I'll brave the dungeon alone. If I succeed, I'll escape to the forest in the North."), #Row 3
                 _("Once you've diverted their attention for about 1 hour, you and your men can retreat there."),
                 _("{}, I can sense that this will be extremely dangerous for you. Please, be careful.").format(self.game.character),
                 _("Thanks, Elder Xuanjing. I'll make sure I'm prepared."),
                 _("The fate of Shaolin rests on you, young man."),
                 _("I will start gathering my men. I will meet you there.")
                 ],
                ["you", "Elder Xuanjing", "you", "you", "Elder Xuanjing", "Elder Xuanjing",
                 "you", "Elder Xuanjing", "you", "you", "you", "Elder Xuanjing", "Elder Xuanjing",
                 "you", "you", "Elder Xuanjing", "you", "Elder Xuanjing", "Elder Xuanjing"]
            )
            self.game.mapWin.deiconify()
            return

        elif self.game.taskProgressDic["yagyu_clan"] == -17:
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("We will meet you at the Yagyu Clan headquarters, {}.").format(self.game.character)],
                ["Elder Xuanjing"]
            )
            self.game.mapWin.deiconify()
            return

        elif self.game.taskProgressDic["yagyu_clan"] == 4:
            self.game.taskProgressDic["yagyu_clan"] = 5
            self.game.stopSoundtrack()
            time.sleep(.5)
            self.game.currentBGM = "jy_shaolin3.mp3"
            self.game.startSoundtrackThread()
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("(Since my mission is to poison the Abbot, I'd better go check out the kitchen...)")],
                ["you"]
            )

            self.game.generate_dialogue_sequence(
                None,
                "Shaolin Kitchen BG.png",
                [_("Alright, hurry up, everyone! Only 20 more minutes till lunch time!"),
                 _("Xu Qing, watch the tofu! Make sure you don't overcook it."),
                 _("Xu Ran, get those noodles into the bowls. Yes, yes, just like that..."),
                 _("Good work, my lads. This tofu and vegetable soup will surely receive much praise!"),
                 _("Xu Qing, once the tofu is ready, go ahead and add them to the noodles."),
                 _("Then take a bowl to the Abbot. Remember to add extra tofu for the Abbot. He loves tofu."),
                 _("Yes, Martial Uncle Head Chef!"),
                 _("(Looks like it won't be too difficult to intercept the food by knocking one of these little monks out...)"),
                 _("(But in order to get the Abbot to eat the food, I'll need to disguise myself as one of them.)"), #Row 2
                 _("(And if I don't want to shave my beautiful hair, I'd better find a skin mask.)"),
                 _("(I bet the Sea Merchant can supply me with one...)")
                 ],
                ["Chef Monk", "Chef Monk", "Chef Monk", "Chef Monk", "Chef Monk", "Chef Monk", "Xu Qing", "you",
                 "you", "you", "you"]
            )

            self.game.mapWin.deiconify()
            return

        elif self.game.taskProgressDic["yagyu_clan"] in [5,6]:
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin Kitchen BG.png",
                [_("(I'd better go talk to the Sea Merchant to see if he can sell me a skin mask...)")],
                ["you"]
            )
            self.game.mapWin.deiconify()
            return

        elif self.game.taskProgressDic["yagyu_clan"] == 7:
            if not self.game.check_for_item(_("Centipede Poison Powder")):
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("(I'd better get some more Centipede Poison Powder before going...)"),
                     ],
                    ["you"]
                )
                self.game.mapWin.deiconify()
                return

            self.game.taskProgressDic["yagyu_clan"] = 8
            self.game.stopSoundtrack()
            time.sleep(.5)
            self.game.currentBGM = "jy_shanguxingjin2.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Shaolin Kitchen BG.png",
                [_("Excellent work, everyone! This stir-fried tofu and eggplant makes my mouth water!"),
                 _("I must say, I'm quite proud of this recipe hahahaha..."),
                 _("Anyway, let's get moving."),
                 _("Xu Qing, deliver a bowl to the Abbot, would ya?"),
                 _("My pleasure, Martial Uncle Head Chef!"),
                 _("*You silently follow Xu Qing as he walks towards Abbot's room.*"),
                 _("*As he reaches 30 feet from the door to Abbot's room, you rush up behind him and jab him in his Zhiyang acupuncture point.*"),
                 _("*You catch the plate of food while Xu Qing falls forward onto the ground.*"),
                 _("Shhhh, make any noise and you're dead. Got it?"),
                 _("Now, I need to know how you normally deliver the food to Abbot."), #Row 2
                 _("Wh-what do you intend to do with Abbot's food?"),
                 _("I just want to add a bit more... flavor... that's all..."),
                 _("Abbot! Someone's--"),
                 ],
                ["Chef Monk", "Chef Monk", "Chef Monk", "Chef Monk", "Xu Qing", "Blank", "Blank", "Blank", "you",
                 "you", "Xu Qing", "you", "Xu Qing"]
            )

            self.game.currentEffect = "sfx_smack.wav"
            self.game.startSoundEffectThread()
            time.sleep(.5)

            self.game.generate_dialogue_sequence(
                None,
                "Shaolin Kitchen BG.png",
                [_("*You quickly strike Xu Qing on the head, knocking him unconscious.*"),
                 _("Stupid monk...."),
                 _("*After dragging him into the bushes, you remove his robes and change into them.*"),
                 _("*Then, having sprinkled some Centipede Poison Powder into the food and put on the skin mask, you knock on Abbot's door.*")
                 ],
                ["Blank", "you", "Blank", "Blank"]
            )

            self.game.add_item(_("Monk Robe"))
            self.game.add_item(_("Centipede Poison Powder"), -1)
            self.abbot_suspicion = 0

            self.game.generate_dialogue_sequence(
                None,
                "Scripture Library.png",
                [_("Yes? Who is it?"),
                 _("Abbot, it's me, Xu Qing. I've brought your lunch."),
                 ],
                ["Abbot", "Xu Qing"],
                [[_("Bring the food over to Abbot."), partial(self.abbot_poison_choice, 1, 1)],
                 [_("Put the food on the table and leave."), partial(self.abbot_poison_choice, 1, 2)]]
            )
            return

        elif self.game.taskProgressDic["yagyu_clan"] in [10,20]:
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("(I should speak to Ashikaga to receive further instructions before causing more trouble...)"),
                 ],
                ["you"]
            )
            self.game.mapWin.deiconify()
            return

        elif self.game.taskProgressDic["yagyu_clan"] in [11,21]:
            time.sleep(.5)
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()

            if self.game.taskProgressDic["yagyu_clan"] == 11:
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Amitabha... Welcome to Shaolin. We apologize in advance, but now's not the best time for visitation to our temple."),
                     _("We believe someone tried to harm the Abbot and are conducting a full investigation."),
                     _("Is that so? Very sorry to hear that, but don't worry. We won't stay long."),
                     _("You see, we have long admired Shaolin's reputation and matchless martial prowess."),
                     _("We have traveled far to witness it ourselves."),
                     _("Ah, we are humbled by your kind words, but--"), #Row 2
                     _("Indeed, my friend Yamamoto speaks the truth! We are quite eager to have a friendly spar to learn from the Shaolin masters."),
                     _("Perhaps we will be fortunate enough to even receive a bit of guidance, if you would be so generous."),
                     _("We are students of the Yagyu Clan and have traveled all the way from Japan for this purpose."),
                     _("Amitabha... This puts me in quite a difficult position..."),
                     _("Why don't you 3 reside in our guest lodging for a few days."), #Row 3
                     _("Once we've conducted a full investigation, we promise to perform our duties as hospitable hosts."),
                     _("What?! A few days?! I can't stand such a long time without meat or wine!"),
                     _("Yamamoto is right. Plus, we have other important tasks to complete for our Master."),
                     _("Such a long delay will surely result in our punishment..."),
                     _("Please, Elder, I know there are many hidden experts in Shaolin."), #Row 4
                     _("Why, I'm sure we would benefit from a spar with even a young monk."),
                     _("Surely you cannot reject such a simple request, honestly made!"),
                     _("Amitabha... Very well. Xu Qing, would you please demonstrate Shaolin Luohan Fist to our guests?"),
                     _("Yes, Elder Xuanjing."),
                     _("Excellent! Young Master, please take it easy on me."), #Row 5
                     _("Amitabha, like-wise, good sir."),
                     ],
                    ["Elder Xuanjing", "Elder Xuanjing", "Yamamoto Takeshi", "Yamamoto Takeshi", "Yamamoto Takeshi",
                     "Elder Xuanjing", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Elder Xuanjing",
                     "Elder Xuanjing", "Elder Xuanjing", "Yamamoto Takeshi", "Ashikaga Yoshiro", "Ashikaga Yoshiro",
                     "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Elder Xuanjing", "Xu Qing",
                     "Yamamoto Takeshi", "Xu Qing"
                     ]
                )

            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Amitabha... Welcome to Shaolin."),
                     _("Greetings, good sir. What an honor to be here!"),
                     _("You see, we have long admired Shaolin's reputation and matchless martial prowess."),
                     _("We have traveled far to witness it ourselves."),
                     _("Ah, we are humbled by your kind words, but--"),
                     _("Indeed, my friend Ashikaga speaks the truth! We are quite eager to have a friendly spar to learn from the Shaolin masters."), #Row 2
                     _("Perhaps we will be fortunate enough to even receive a bit of guidance, if you would be so generous."),
                     _("We are students of the Yagyu Clan and have traveled all the way from Japan for this purpose."),
                     _("Why, I'm sure we would benefit from a spar with even a young monk."),
                     _("Surely you cannot reject such a simple request, honestly made!"),
                     _("Amitabha... Very well. Xu Qing, would you please demonstrate Shaolin Luohan Fist to our guests?"), #Row 3
                     _("Yes, Elder Xuanjing."),
                     _("Excellent! Young Master, please take it easy on me."),
                     _("Amitabha, like-wise, good sir."),
                     ],
                    ["Elder Xuanjing", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Elder Xuanjing",
                     "Yamamoto Takeshi", "Yamamoto Takeshi",  "Yamamoto Takeshi","Yamamoto Takeshi", "Yamamoto Takeshi",
                     "Elder Xuanjing", "Xu Qing", "Yamamoto Takeshi", "Xu Qing"
                     ]
                )

            opp = character(_("Xu Qing"), 13,
                            sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                                 special_move(_("Basic Punching Technique"), 10),],
                            skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                    skill(_("Shaolin Inner Energy Technique"), level=10)])

            you = character(_("Yamamoto Takeshi"), 19,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Hachiman Blade"), level=4),
                                 special_move(_("Rest"))],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Kenjutsu II"), level=5)],
                            equipmentList=[Equipment(_("Iron Sword"))],
                            preferences=[2, 4, 2, 3, 2])

            self.game.battleID = "shaolin_yagyu_spar"
            cmd = lambda: self.post_yagyu_clan_battle(-1)

            self.game.battleMenu(you, opp, "Shaolin.png",
                                 "jy_suspenseful1.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[None] * 3,
                                 postBattleCmd=cmd)
            return


        elif self.game.taskProgressDic["yagyu_clan"] >= 30:
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("What are you doing here?! Shaolin does not welcome you!"),],
                ["Elder Xuanjing"]
            )
            self.game.mapWin.deiconify()
            return




        self.game.shaolin_win = Toplevel(self.game.mapWin)
        self.game.shaolin_win.title(_("Shaolin Temple"))

        bg = PhotoImage(file="Shaolin.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.shaolin_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Elder Xuanjing_icon_large.ppm")
        imageButton1 = Button(F1, command=partial(self.clickedOnElderXuanjing, False))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("Elder Xuanjing"))
        label.grid(row=1, column=0)
        F1.place(x=w // 4 - pic1.width() // 2, y=h // 2)

        if self.game.taskProgressDic["li_guiping_task"] < 3:
            F2 = Frame(canvas)
            pic2 = PhotoImage(file="Elder Xuansheng_icon_large.ppm")
            imageButton2 = Button(F2, command=partial(self.clickedOnElderXuansheng, False))
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            label = Label(F2, text=_("Elder Xuansheng"))
            label.grid(row=1, column=0)
            F2.place(x=w // 2 - pic1.width() // 2, y=h // 2)

        F3 = Frame(canvas)
        pic3 = PhotoImage(file="Xu Qing_icon_large.ppm")
        imageButton3 = Button(F3, command=partial(self.clickedOnXuQing, False))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        label = Label(F3, text=_("Xu Qing"))
        label.grid(row=1, column=0)
        F3.place(x=w * 3 // 4 - pic1.width() // 2, y=h // 2)

        self.game.shaolin_win_F1 = Frame(self.game.shaolin_win)
        self.game.shaolin_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.shaolin_win,
                                                 options=["Map", "Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.shaolin_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.shaolin_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.shaolin_win.winfo_width(), self.game.shaolin_win.winfo_height()
        self.game.shaolin_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.shaolin_win.focus_force()
        self.game.shaolin_win.mainloop()  ###


    def post_abbot_poison_failure(self):
        self.game.generate_dialogue_sequence(
            None,
            "Scripture Library.png",
            [_("(Welp, that was a complete failure, but at least I escaped alive.)"),
             _("(I'd better go back and tell Ashikaga what happened...)"),
             ],
            ["you", "you"]
        )
        self.game.mapWin.deiconify()


    def abbot_poison_choice(self, stage, choice):

        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        if stage == 1:
            if choice == 1:
                self.abbot_suspicion += 1
                self.game.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Here, Abbot, please eat it while it's warm."),
                     _("Ah, thank you, Xu Qing. You can place it over there on that table like you usually do."),
                     _("Sure thing!"),
                     _("I heard some noise just now. Is everything ok?"),
                     ],
                    ["Xu Qing", "Abbot", "Xu Qing", "Abbot"],
                    [[_("Yeah, I almost tripped over a rock."), partial(self.abbot_poison_choice, 2, 1)],
                     [_("Huh? What noise?"), partial(self.abbot_poison_choice, 2, 2)]]
                )
            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("I'll place it over here on the table for you Abbot."),
                     _("Ah, thank you, Xu Qing. That's perfect."),
                     _("My pleasure, Abbot."),
                     _("I heard some noise just now. Is everything ok?"),
                     ],
                    ["Xu Qing", "Abbot", "Xu Qing", "Abbot"],
                    [[_("Yeah, I almost tripped over a rock."), partial(self.abbot_poison_choice, 2, 1)],
                     [_("Huh? What noise?"), partial(self.abbot_poison_choice, 2, 2)]]
                )

        elif stage == 2:
            if choice == 1:
                self.game.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Sorry to have disturbed you, Abbot."),
                     _("Ahh I see... No worries at all...")
                     ],
                    ["Xu Qing", "Abbot"],
                )
            else:
                self.abbot_suspicion += 1
                self.game.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("Ahhh, nevermind then. I must be getting old..."),
                     ],
                    ["Abbot"],
                )

            if self.abbot_suspicion < 2 and random() >= self.abbot_suspicion*50/self.game.luck:

                self.game.taskProgressDic["yagyu_clan"] = 10
                self.game.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("I'll be going now. Enjoy your lunch, Abbot!"),
                     _("Thanks again, Xu Qing."),
                     _("(Oh yes, enjoy your food, old man. AHAHAHAHAHA!)")
                     ],
                    ["Xu Qing", "Abbot", "Xu Qing"]
                )
                self.game.mapWin.deiconify()

            else:
                self.game.taskProgressDic["yagyu_clan"] = 20
                self.game.generate_dialogue_sequence(
                    None,
                    "Scripture Library.png",
                    [_("By the way, how was this morning's meditation session?"),
                     _("Oh! It, uh, it went really well... I.. I really enjoyed it."),
                     _("Hmmm, any new insights that you'd like to share with me?"),
                     _("Y-yes... I, uh, I think I have a better grasp of compassion and uh... Inner peace..."),
                     _("I'm glad to hear that. Xu Qing, I just want to thank you deeply for all that you've done these 2 years."),
                     _("Spending many hours in the kitchen while your martial brothers are practicing and learning new techniques must not be easy."),
                     _("You have selflessly sacrificed your pursuit of martial arts to serve others. I am very grateful."),
                     _("I-I'm very blessed to have this opportunity! I love helping Martial Uncle Head Chef with cooking."),
                     _("I learn many interesting things from him daily!"),
                     _("Is that so?"),
                     _("Most definitely!"),
                     _("Well, that is strange. Xu Qing was only recently assigned kitchen duty last month."),
                     _("And there was no meditation session this morning. You're not Xu Qing. Who are you?"),
                     _("I... I... Gahhhh dang it! You sly old monk!"),
                     _("Please kindly reveal your identity as well as the whereabouts of Xu Qing."),
                     _("Not a chance, old man."),
                     _("Amitabha... You leave me no choice then...")
                     ],
                    ["Abbot", "Xu Qing", "Abbot", "Xu Qing", "Abbot", "Abbot", "Abbot",
                     "Xu Qing", "Xu Qing", "Abbot", "Xu Qing",
                     "Abbot", "Abbot", "Xu Qing", "Abbot", "Xu Qing", "Abbot", ]
                )

                opp = character(_("Abbot"), 25,
                                sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                                     special_move(_("Shaolin Diamond Finger"), level=10),
                                     special_move(_("Guan Yin Palm"), level=10),
                                     special_move(_("Empty Force Fist"), level=10), ],
                                skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                        skill(_("Tendon Changing Technique"), level=5),
                                        skill(_("Shaolin Inner Energy Technique"), level=10),
                                        skill(_("Shaolin Agility Technique"), level=10),])

                self.game.battleMenu(self.game.you, opp, "Scripture Library.png",
                                     postBattleSoundtrack="jy_shaolin1.mp3",
                                     fromWin=None,
                                     battleType="task", destinationWinList=[None] * 3,
                                     postBattleCmd=self.post_abbot_poison_failure
                                     )




    def post_yagyu_clan_battle(self, stage=1):
        if stage == 1:
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Ahhhhh!"),
                 _("*Xu Qing spits out a pool of blood after receiving a final kick to the abdomen and is sent flying several feet back.*"),
                 _("Xu Qing! Quick! Take him to the ward and give him some small recovery pills!"),
                 _("Hahahahaha! So this is Shaolin Kung-Fu, eh? I must say, I'm a little disappointed..."),
                 _("You... You... Amitabha... Don't you think your strikes were too hard for a spar?"),
                 _("My apologies, Elder Xuanjing. I didn't dare hold back. After all, Shaolin Kung-Fu is #1 in the world!"),
                 _("I had to give it my all to stand a chance!"), #Row 2
                 _("Amitabha... Just what are your intentions, really?"),
                 _("Why, we're merely hoping for an opportunity to learn. That's all!"),
                 _("Amitabha... Your skill level far surpasses that of a commoner."),
                 _("To be honest, even I cannot say with confidence that I can defeat you."),
                 _("Excuse me for being blunt, but your humility seems insincere..."), #Row 3
                 _("We understand that you're upset because your student was injured, Elder Xuanjing..."),
                 _("However, we really expected much more from Shaolin..."),
                 _("And I'll be blunt as well. If what we just saw was Shaolin Kung-Fu, then I must say"),
                 _("Shaolin is only worthy of tying our sandals, hahahahahaha!"),
                 _("Y-you... How dare you speak such arrogant words! I'll--"), #Row 4
                 _("Xuanjing... Amitabha... *Cough*... It appears that our guests came with ill intentions..."),
                 _("Yagyu-san, what is it that you're after? *Cough*... Speak plainly, speak clearly..."),
                 _("Honesty and transparency are necessary to arrive at an agreement."),
                 _("Hahahaha! True words, indeed, Abbot! In that case, I will give you a straightforward response."),
                 _("We've come to challenge Shaolin. The proud warriors of the Yagyu Clan wish to claim the fame that we deserve!"),
                 _("Let's engage in a simple tournament: best of 3 fights. What do you say?"),
                 _("No, Abbot! You can't! You're already sick. They are taking advantage of the situation!"), #Row 5
                 _("Amitabha... *Cough*... We have no choice, Xuanjing..."),
                 _("Even though I am sick, I can still hold my own against Yamamoto, based on the fight I just observed..."),
                 _("You are also capable... But... *Cough*... What I'm concerned about is the 3rd round..."),
                 _("Who will step up to ... *Cough*... *Cough cough*...")
                 ],
                ["Xu Qing", "Blank", "Elder Xuanjing", "Yamamoto Takeshi", "Elder Xuanjing", "Yamamoto Takeshi",
                 "Yamamoto Takeshi", "Elder Xuanjing", "Yamamoto Takeshi", "Elder Xuanjing", "Elder Xuanjing",
                 "Elder Xuanjing", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro",
                 "Elder Xuanjing", "Abbot", "Abbot", "Abbot", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi",
                 "Elder Xuanjing", "Abbot", "Abbot", "Abbot", "Abbot"
                 ]
            )

            if self.game.taskProgressDic["li_guiping_task"] < 3:
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Abbot, I know my skills are insufficient, but as one of the elders, its my responsibility to step up."),
                     _("Xuansheng... Even Xuanjing only has at most 50% chance of winning against Yamamoto..."),
                     _("And we don't even... *Cough*... know anything about the other 2."),
                     _("I know, Abbot, but I must protect Shaolin's dignity, even if it costs me my life.")
                     ],
                    ["Elder Xuansheng", "Abbot", "Abbot", "Elder Xuansheng"]
                )

            if self.game.taskProgressDic["join_sect"] == 500:
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Hold on a second! I'll go, Abbot! I'll represent Shaolin!"),
                     _("{}?? H-how... why are you here?").format(self.game.character),
                     _("I'd encountered these people previously and found out about their ambitions."),
                     _("I came here to warn you, but it seems I was a bit late..."),
                     _("{}-san, this is between the Yagyu Clan and Shaolin. I hope you will stay out of this.").format(self.game.character), # Row 2
                     _("I told you I would not allow you to carry out your evil schemes..."),
                     _("I've been courteous with you up till now. Don't make me..."),
                     _("Then why don't you turn around and go home?"),
                     _("Hah... I am one step away from success. You think I'd give up now?"),
                     _("Then I guess I'll face you in the tournament..."),
                     _("You're not even a disciple of Shaolin! You can't participate in the tournament!"),  # Row 3
                     _("Actually, I've trained under Shaolin for a brief period of time."),
                     _("Though I am far from understanding the true essence of Shaolin Kung-Fu, it's sufficient to take care of degenerates like you."),
                     _("So, you have chosen death. Very well. We will gladly bathe our swords in your blood.")
                     ],
                    ["you", "Abbot", "you", "you",
                     "you", "Abbot", "Yagyu Tadashi", "you", "Yagyu Tadashi", "you", "Yagyu Tadashi", "you",
                     "Yagyu Tadashi", "you", "you", "Yagyu Tadashi"
                     ]
                )

            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Hold on a second! I'll go, Abbot! I'll represent Shaolin!"),
                     _("And you are...?"),
                     _("{}-san, this is between the Yagyu Clan and Shaolin. I hope you will stay out of this.").format(self.game.character),
                     _("I told you I would not allow you to carry out your evil schemes..."),
                     _("I've been courteous with you up till now. Don't make me..."),
                     _("Then why don't you turn around and go home?"),
                     _("Hah... I am one step away from success. You think I'd give up now?"),
                     _("Then I guess I'll face you in the tournament..."),
                     _("You're not even a disciple of Shaolin! You can't participate in the tournament!"), #Row 2
                     _("Hahahahaha! Haven't you heard of the phrase 'All martial arts under heaven originated from Shaolin'?"),
                     _("I am, therefore, a distant disciple of Shaolin."),
                     _("Though I am far from understanding the true essence of Shaolin Kung-Fu, it's sufficient to take care of degenerates like you."),
                     _("So, you have chosen death. Very well. We will gladly bathe our swords in your blood.")
                     ],
                    ["you", "Abbot", "Yagyu Tadashi", "you", "Yagyu Tadashi", "you", "Yagyu Tadashi", "you",
                     "Yagyu Tadashi", "you", "you", "you", "Yagyu Tadashi"]
                )

            self.game.taskProgressDic["yagyu_clan"] = -5
            self.tournament_rounds_won = 0
            self.tournament_round = 1
            self.tournament_participants = ["you", "Elder Xuanjing", "Abbot"]
            self.tournament_opp_participants = ["Ashikaga Yoshiro", "Yamamoto Takeshi", "Yagyu Tadashi"]
            self.tournament_vs_yagyu_clan(1)


        elif stage == -1: #played as Yamamoto vs Xu Qing
            self.game.restore(0,0,full=True)
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Ahhhhh!"),
                 _("*Xu Qing spits out a pool of blood after receiving a final kick to the abdomen and is sent flying several feet back.*"),
                 _("Xu Qing! Quick! Take him to the ward and give him some small recovery pills!"),
                 _("Hahahahaha! So this is Shaolin Kung-Fu, eh? I must say, I'm a little disappointed..."),
                 _("You... You... Amitabha... Don't you think your strikes were too hard for a spar?"),
                 _("My apologies, Elder Xuanjing. I didn't dare hold back. After all, Shaolin Kung-Fu is #1 in the world!"),
                 _("I had to give it my all to stand a chance!"), #Row 2
                 _("Amitabha... Just what are your intentions, really?"),
                 _("Why, we're merely hoping for an opportunity to learn. That's all!"),
                 _("Amitabha... Your skill level far surpasses that of a commoner."),
                 _("To be honest, even I cannot say with confidence that I can defeat you."),
                 _("Excuse me for being blunt, but your humility seems insincere..."), #Row 3
                 _("We understand that you're upset because your student was injured, Elder Xuanjing..."),
                 _("However, we really expected much more from Shaolin..."),
                 _("And I'll be blunt as well. If what we just saw was Shaolin Kung-Fu, then I must say"),
                 _("Shaolin is only worthy of tying our sandals, hahahahahaha!"),
                 _("Y-you... How dare you speak such arrogant words! I'll--"), #Row 4
                 ],
                ["Xu Qing", "Blank", "Elder Xuanjing", "Yamamoto Takeshi", "Elder Xuanjing", "Yamamoto Takeshi",
                 "Yamamoto Takeshi", "Elder Xuanjing", "Yamamoto Takeshi", "Elder Xuanjing", "Elder Xuanjing",
                 "Elder Xuanjing", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro",
                 "Elder Xuanjing",
                 ]
            )

            if self.game.taskProgressDic["yagyu_clan"] == 11:
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Xuanjing... Amitabha... *Cough*... It appears that our guests came with ill intentions..."),
                     _("A-Abbot! Why, you need rest right now. Please, I will handle this matter."),
                     _("Ah, the Shaolin Abbot finally appears! What's the matter? You look... unwell..."),
                     _("Someone recently poisoned Abbot's food. Fortunately, he knows the Tendon Changing Technique, so his life was unharmed."),
                     _("However, he has been weakened and will require a few weeks to recover to full strength."),
                     _("This is why we are conducting an investigation, as I told you earlier."), #Row 2
                     _("Now if you would be so kind, please allow us to settle the internal affairs first."),
                     _("No problem. That's understandable... We will leave, as soon as Abbot promises to do one thing, of course."),
                     _("How may I... *Cough*... help you?"),
                     _("It's quite simple. Within 3 days, announce to the martial arts community that Shaolin has been defeated by the mighty Yagyu Clan!"),
                     _("Recognize Yagyu Clan as the number one Sect in all of Wulin. That is all."), #Row 2
                     _("Amitabha... You and your friends are... *Cough* ... no doubt quite skilled."),
                     _("However, we have yet to... *Cough*... have a fair match to decide on anything."),
                     _("Though I cannot... *Cough*... say that Shaolin is undefeated, to proclaim that we are inferior to the Yagyu Clan without a proper contest is irresponsible."),
                     _("As such, I cannot agree to your request."),
                     _("Simple, let's have a quick spar right now. It won't take more than 5 minutes..."),
                     _("You... How dishonorable of you to take advantage of the situation..."),
                     _("I thought you might say that... It's quite unfortunate that Abbot's been poisoned, but we won't challenge him."), #Row 3
                     _("If anyone... Anyone in Shaolin can defeat this friend of mine, we will leave right away."),
                     _("{}, it's your chance to shine!").format(self.game.character),],
                    ["Abbot", "Elder Xuanjing", "Ashikaga Yoshiro", "Elder Xuanjing", "Elder Xuanjing",
                     "Elder Xuanjing", "Elder Xuanjing", "Ashikaga Yoshiro", "Abbot", "Ashikaga Yoshiro",
                     "Ashikaga Yoshiro", "Abbot", "Abbot", "Abbot", "Abbot", "Yamamoto Takeshi", "Elder Xuanjing",
                     "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro"
                     ]
                )

                if self.game.taskProgressDic["join_sect"] == 500:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Shaolin.png",
                        [_("{}! You... How could you do something like this?!"),
                         _("Have you forgotten how we taught you and cared for you?!"),
                         _("Sorry, Elder Xuanjing... I've found friends who are stronger than you."),
                         _("You should be happy for me, ahahahahaha!"),
                         _("Since you and Abbot have been kind to me in the past, I won't harm you if you simply surrender."),
                         _("You... you traitor... As the Head of the Shaolin Luohan School, I will teach you a lesson today and incapacitate you!")],
                        ["Elder Xuanjing", "Elder Xuanjing", "you", "you", "you", "Elder Xuanjing",]
                    )
                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Shaolin.png",
                        [_("Who is unafraid of pain? Come forward and receive a humiliating defeat."),
                         _("As the Head of the Shaolin Luohan School, it is my duty to accept this challenge."),
                         _("Ha... I'm afraid you won't last 3 minutes...")],
                        ["you", "Elder Xuanjing", "you"]
                    )

                opp = character(_("Elder Xuanjing"), 20,
                          sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                               special_move(_("Shaolin Diamond Finger"), level=10),
                               special_move(_("Empty Force Fist"), level=7),],
                          skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                  skill(_("Shaolin Inner Energy Technique"), level=10),
                                  skill(_("Shaolin Agility Technique"), level=10),])

                self.game.create_self_character()
                self.game.battleID = None
                cmd = lambda: self.post_yagyu_clan_battle(-2)
                self.game.battleMenu(self.game.you, opp, "Shaolin.png",
                                     "jy_suspenseful1.mp3", fromWin=None,
                                     battleType="task", destinationWinList=[None] * 3,
                                     postBattleCmd=cmd)



            else:

                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Xuanjing... Amitabha... It appears that our guests came with ill intentions..."),
                     _("Ah, the Shaolin Abbot finally appears!"),
                     _("How may I help you?"),
                     _("It's quite simple. Within 3 days, announce to the martial arts community that Shaolin has been defeated by the mighty Yagyu Clan!"),
                     _("Recognize Yagyu Clan as the number one Sect in all of Wulin. That is all."),
                     _("Amitabha... You and your friends are no doubt quite skilled."),#Row 2
                     _("However, we have yet to have a fair match to decide on anything."),
                     _("Though I cannot say that Shaolin is undefeated, to proclaim that we are inferior to the Yagyu Clan without a proper contest is irresponsible."),
                     _("As such, I cannot agree to your request."),
                     _("Simple, let's have a quick spar right now. It won't take more than 5 minutes..."),
                     _("If anyone... Anyone in Shaolin can defeat this friend of mine, we will leave right away."),
                     _("{}, it's your chance to shine!").format(self.game.character)],
                    ["Abbot", "Ashikaga Yoshiro", "Abbot", "Ashikaga Yoshiro",  "Ashikaga Yoshiro",
                     "Abbot", "Abbot", "Abbot", "Abbot", "Yamamoto Takeshi", "Yamamoto Takeshi", "Yamamoto Takeshi"]
                )

                if self.game.taskProgressDic["join_sect"] == 500:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Shaolin.png",
                        [_("{}! You... How could you do something like this?!"),
                         _("Have you forgotten how we taught you and cared for you?!"),
                         _("Sorry, Elder Xuanjing... I've found friends who are stronger than you."),
                         _("You should be happy for me, ahahahahaha!"),
                         _("Since you and Abbot have been kind to me in the past, I won't harm you if you simply surrender."),
                         _("You... you traitor... As the Head of the Shaolin Luohan School, I will teach you a lesson today and incapacitate you!")],
                        ["Elder Xuanjing", "Elder Xuanjing", "you", "you", "you", "Elder Xuanjing", ]
                    )
                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Shaolin.png",
                        [_("Who is unafraid of pain? Come forward and receive a humiliating defeat."),
                         _("As the Head of the Shaolin Luohan School, it is my duty to accept this challenge."),
                         _("Ha... I'm afraid you won't last 3 minutes...")],
                        ["you", "Elder Xuanjing", "you"]
                    )

                opp = character(_("Elder Xuanjing"), 20,
                                sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                                     special_move(_("Shaolin Diamond Finger"), level=10),
                                     special_move(_("Empty Force Fist"), level=7), ],
                                skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                        skill(_("Shaolin Inner Energy Technique"), level=10),
                                        skill(_("Shaolin Agility Technique"), level=10),])

                self.game.create_self_character()
                self.game.battleID = None
                cmd = lambda: self.post_yagyu_clan_battle(-2)
                self.game.battleMenu(self.game.you, opp, "Shaolin.png",
                                     "jy_suspenseful1.mp3", fromWin=None,
                                     battleType="task", destinationWinList=[None] * 3,
                                     postBattleCmd=cmd)


        elif stage == -2: #defeated Elder Xuanjing
            if self.game.taskProgressDic["yagyu_clan"] == 11:
                if self.game.taskProgressDic["join_sect"] == 500 and self.game.taskProgressDic["luohanzhen"] > 0:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Shaolin.png",
                        [_("Sorry, Elder Xuanjing, looks like you are no match for me."),
                         _("I cannot represent all of Shaolin. My defeat is but my own incompetence."),
                         _("Since you seek a real challenge, why don't you enter the Luohan Formation?"),
                         _("Luohan Formation? I've already defeated those lousy monks."),
                         _("I have yet to publicly challenge the Abbot though. Come on, Abbot, let's go..."),
                         _("You are unworthy to challenge the Abbot."),
                         _("Junior Brother Xuannian..."),
                         _("Abbot, please, allow me to take your place in this spar."), #Row 2
                         _("Wait, Junior Brother Xuannian, these guys are not average fighters."),
                         _("Even though you've learned some fundamental Shaolin Kung-Fu, I'm afraid you've spent most of your time in the kitchen."),
                         _("And thus might not be prepared for such challenges..."),
                         _("Elder Xuanjing, it's quite alright... Since he has the heart to stand up for Shaolin, please give him the opportunity."),
                         _("Thank you, Abbot!"),
                         _("Now I know that Shaolin is truly desperate!"), #Row 3
                         _("What is this chef going to do? Throw tofu in my face? Ahahahaha!"),
                         _("This is an insult! Is Shaolin so arrogant that they would send a chef to fight us?!"),
                         _("Let me go twist his head off! AHHHHHHH!")],
                        ["you", "Elder Xuanjing", "Elder Xuanjing", "you", "you", "Chef Monk", "Abbot",
                         "Chef Monk", "Elder Xuanjing", "Elder Xuanjing", "Elder Xuanjing", "Abbot", "Chef Monk",
                         "you", "you", "Yamamoto Takeshi", "Yamamoto Takeshi"]
                    )

                    opp = character(_("Chef Monk"), 25,
                                    sml=[special_move(_("Prajna Palm"), level=10),],
                                    skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                            skill(_("Shaolin Inner Energy Technique"), level=10),
                                            skill(_("Shaolin Agility Technique"), level=10),])

                    you = character(_("Yamamoto Takeshi"), 19,
                                    sml=[special_move(_("Kendo"), level=10),
                                         special_move(_("Hachiman Blade"), level=4),
                                         special_move(_("Rest"))],
                                    skills=[skill(_("Bushido"), level=10),
                                            skill(_("Kenjutsu I"), level=10),
                                            skill(_("Kenjutsu II"), level=5)],
                                    equipmentList=[Equipment(_("Iron Sword"))],
                                    preferences=[2, 4, 2, 3, 2])

                    self.game.battleID = "shaolin_yagyu_spar"
                    cmd = lambda: self.post_yagyu_clan_battle(-4)

                    self.game.battleMenu(you, opp, "Shaolin.png",
                                         "jy_suspenseful1.mp3", fromWin=None,
                                         battleType="task", destinationWinList=[None] * 3,
                                         postBattleCmd=cmd)
                    return


                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Shaolin.png",
                        [_("Sorry, Elder Xuanjing, looks like you are no match for me."),
                         _("I cannot represent all of Shaolin. My defeat is but my own incompetence."),
                         _("Since you seek a real challenge, why don't you enter the Luohan Formation?"),
                         _("Luohan Formation? I've always wanted to try it!"),
                         _("I guess that serves as a reasonable substitute for going against the Abbot himself."),
                         _("Very well, this way please.")],
                        ["you", "Elder Xuanjing", "Elder Xuanjing", "you", "you", "Elder Xuanjing"]
                    )
                    self.game.stopSoundtrack()
                    self.game.currentBGM = "luohanzhen.mp3"
                    self.game.startSoundtrackThread()
                    self.game.mini_game_in_progress = True
                    self.game.mini_game = luohan_formation_mini_game(self.game, self, mode="Yagyu")
                    self.game.mini_game.new()

            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Sorry, Elder Xuanjing, looks like you are no match for me."),
                     _("I cannot represent all of Shaolin. My defeat is but my own incompetence."),
                     _("You are quite incompetent indeed. Perhaps the Abbot would be a more exciting opponent..."),
                     _("I have yet to publicly challenge the Abbot. Come on, Abbot, let's go..."),
                     _("You are unworthy to challenge the Abbot."),
                     _("Junior Brother Xuannian..."),
                     _("Abbot, please, allow me to take your place in this spar."),  # Row 2
                     _("Wait, Junior Brother Xuannian, these guys are not average fighters."),
                     _("Even though you've learned some fundamental Shaolin Kung-Fu, I'm afraid you've spent most of your time in the kitchen."),
                     _("And thus might not be prepared for such challenges..."),
                     _("Elder Xuanjing, it's quite alright... Since he has the heart to stand up for Shaolin, please give him the opportunity."),
                     _("Thank you, Abbot!"),
                     _("Now I know that Shaolin is truly desperate!"),  # Row 3
                     _("What is this chef going to do? Throw tofu in my face? Ahahahaha!"),
                     _("This is an insult! Is Shaolin so arrogant that they would send a chef to fight us?!"),
                     _("Let me go twist his head off! AHHHHHHH!")],
                    ["you", "Elder Xuanjing", "you", "you", "Chef Monk", "Abbot",
                     "Chef Monk", "Elder Xuanjing", "Elder Xuanjing", "Elder Xuanjing", "Abbot", "Chef Monk",
                     "you", "you", "Yamamoto Takeshi", "Yamamoto Takeshi"]
                )

                opp = character(_("Chef Monk"), 25,
                                sml=[special_move(_("Prajna Palm"), level=10),],
                                skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                        skill(_("Shaolin Inner Energy Technique"), level=10),
                                        skill(_("Shaolin Agility Technique"), level=10), ])

                you = character(_("Yamamoto Takeshi"), 19,
                                sml=[special_move(_("Kendo"), level=10),
                                     special_move(_("Hachiman Blade"), level=4),
                                     special_move(_("Rest"))],
                                skills=[skill(_("Bushido"), level=10),
                                        skill(_("Kenjutsu I"), level=10),
                                        skill(_("Kenjutsu II"), level=5)],
                                equipmentList=[Equipment(_("Iron Sword"))],
                                preferences=[2, 4, 2, 3, 2])

                self.game.battleID = "shaolin_yagyu_spar"
                cmd = lambda: self.post_yagyu_clan_battle(-4)

                self.game.battleMenu(you, opp, "Shaolin.png",
                                     "jy_suspenseful1.mp3", fromWin=None,
                                     battleType="task", destinationWinList=[None] * 3,
                                     postBattleCmd=cmd)
                return


        elif stage == -3: #beat Luohan Formation
            self.game.taskProgressDic["yagyu_clan"] = 30
            self.game.mini_game_in_progress = False
            pg.display.quit()

            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Hahahaha! Not even the mighty Luohan Formation can stop me!"),
                 _("Amitabha... *Cough*... Shaolin has always been friendly towards chivalrous clans and gracious towards others."),
                 _("Why have you... *Cough* ... chosen to create disunity within the Martial Arts Community?"),
                 _("The strength of the Yagyu Clan must be acknowledged! So, Abbot, do we have a deal?"),
                 _("You may have obtained victory... *Cough*... through dishonorable means, but we will not lose our dignity."),
                 _("You will not get what you wish for! *Cough cough cough*..."),
                 _("Listen, old monk, if you don't --"),
                 _("Yamamoto, Yamamoto... Don't be so harsh towards the Abbot..."), #Row 2
                 _("Now, Abbot, I realize this is a difficult decision to make."),
                 _("Perhaps it will help if you came back with me to the Yagyu Clan to have some tea."),
                 _("You can enjoy our companionship whilst contemplating the potential consequences of your decision."),
                 _("No! Abbot! You can't!"),
                 _("I fail to understand why anyone would reject our kind offer, but if they insist..."), #Row 3
                 _("I can always try to convince them, with my fists... But I assure you I will not be as merciful as my friend Yamamoto was towards that young monk."),
                 _("I will go with you."),
                 _("I am very pleased to hear that."),
                 _("Don't worry, monks of Shaolin. We will take good care of your Abbot, hahahahaha!"),
                 _("{}, a fantastic performance from you, dear comrade! I could not be more proud to fight alongside you!").format(self.game.character), #Row 4
                 _("What's the next step?"),
                 _("There are too many people here. I will tell you once we're back home.")],
                ["you", "Abbot", "Abbot", "Ashikaga Yoshiro", "Abbot", "Abbot", "Yamamoto Takeshi",
                 "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Elder Xuanjing",
                 "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Abbot", "Ashikaga Yoshiro", "Ashikaga Yoshiro",
                 "Ashikaga Yoshiro", "you", "Ashikaga Yoshiro",]
            )

            self.game.mapWin.deiconify()
            return


        elif stage == -4: #played as Yamamoto vs Chef Monk and lost
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("I-impossible! How... how can a chef be so strong?!"),
                 _("Abbot, what is going on? Was that Prajna Palm?"),
                 _("Wh-when did Brother Xuannian learn such powerful techniques?"),
                 _("Amitabha... This all goes back to 15 years ago, when I first became Abbot."),
                 _("Brother Xuannian had been helping in the kitchen for a few years already, and his passion for culinary arts was obvious to everyone."),
                 _("However, he wasn't as interested in martial arts."),
                 _("He frequently shared his new recipes with me and had me test them. Over time, we formed a strong friendship."),  # Row 2
                 _("After some time, I convinced him to learn some basic techniques for self defence, and he agreed."),
                 _("I began by teaching him Shaolin Luohan Fist and was surprised at how quickly he picked it up."),
                 _("He was able to master the technique much quicker than either you or me, Elder Xuanjing."),
                 _("It's as if he has God-given talent for martial arts..."),
                 _("I could not let this gift go to waste, so I began to introduce him to snippets of the Prajna Sutra, telling him they came from manuals for fundamental techniques."),
                 _("He did not disappoint me, and within a few months, he learned the Prajna Palm."),
                 _("The last person to do so passed away over a century ago."),
                 _("I was overjoyed and told him the truth and also urged him to keep this a secret, in case other monks became jealous of him."),
                 _("He has been practicing it secretly ever since, and I believe he has mastered the art now."),
                 _("Prajna Palm... Incredible! Truly incredible!!!"), #Row 3
                 _("N-no way... The Prajna Palm... Shaolin's most powerful palm technique..."),
                 _("Yamamoto, it is no wonder, then, that you were no match for him."),
                 _("The two of us combined would have difficulty against him."),
                 _("Looks like I've underestimated you, fat monk, but good. This will at least be entertaining for me."),
                 _("The fate and renown of Shaolin rests in your hands...")
                 ],
                ["Yamamoto Takeshi", "Elder Xuanjing", "Elder Xuanjing", "Abbot", "Abbot", "Abbot",
                 "Abbot", "Abbot", "Abbot", "Abbot", "Abbot", "Abbot", "Abbot", "Abbot", "Abbot", "Abbot",
                 "Elder Xuanjing", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "you", "you"]
            )

            opp = character(_("Chef Monk"), 25,
                            sml=[special_move(_("Prajna Palm"), level=10),],
                            skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                    skill(_("Shaolin Inner Energy Technique"), level=10),
                                    skill(_("Shaolin Agility Technique"), level=10), ])
            self.game.create_self_character()
            self.game.battleID = None
            cmd = lambda: self.post_yagyu_clan_battle(-5)
            self.game.battleMenu(self.game.you, opp, "Shaolin.png",
                                 "jy_suspenseful1.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[None] * 3,
                                 postBattleCmd=cmd)


        elif stage == -5: #defeated Chef Monk
            self.game.taskProgressDic["yagyu_clan"] = 30

            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("Ahahahahaha!!! Bow before my might!"),
                 _("Abbot, I... I've failed you... and I've failed Shaolin..."),
                 _("Amitabha... This is not your fault. It is the will of Heaven..."),
                 _("You have witnessed the strength of the Yagyu Clan with your own eyes! So, Abbot, when will you make the announcement?"),
                 _("You may have obtained victory through dishonorable means, but we will not lose our dignity."),
                 _("You will not get what you wish for!"),
                 _("Listen, old monk, if you don't --"),
                 _("Yamamoto, Yamamoto... Don't be so harsh towards the Abbot..."),  # Row 2
                 _("Now, Abbot, I realize this is a difficult decision to make."),
                 _("Perhaps it will help if you came back with me to the Yagyu Clan to have some tea."),
                 _("You can enjoy our companionship whilst contemplating the potential consequences of your decision."),
                 _("No! Abbot! You can't!"),
                 _("I fail to understand why anyone would reject our kind offer, but if they insist..."),  # Row 3
                 _("I can always try to convince them, with my fists... But I assure you I will not be as merciful as my friend Yamamoto was towards that young monk."),
                 _("I will go with you."),
                 _("I am very pleased to hear that."),
                 _("Don't worry, monks of Shaolin. We will take good care of your Abbot, hahahahaha!"),
                 _("{}, a fantastic performance from you, dear comrade! I could not be more proud to fight alongside you!").format(self.game.character),
                 # Row 4
                 _("What's the next step?"),
                 _("There are too many people here. I will tell you once we're back home.")],
                ["you", "Chef Monk", "Abbot", "Ashikaga Yoshiro", "Abbot", "Abbot", "Yamamoto Takeshi",
                 "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Elder Xuanjing",
                 "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Abbot", "Ashikaga Yoshiro", "Ashikaga Yoshiro",
                 "Ashikaga Yoshiro", "you", "Ashikaga Yoshiro", ]
            )

            self.game.mapWin.deiconify()
            return


    def tournament_vs_yagyu_clan(self, stage):
        if stage == 1:
            self.game.pre_tournament_win = Toplevel(self.game.mapWin)
            self.game.pre_tournament_win.title(_("Tournament"))

            self.tournament_label = Label(self.game.pre_tournament_win,
                                          text=_("Select participant for round {}.").format(self.tournament_round))
            self.tournament_label.pack()

            self.tournament_F1 = Frame(self.game.pre_tournament_win)
            self.tournament_F1.pack()
            self.participant_buttons = []
            self.participant_images = []
            for i in range(len(self.tournament_participants)):
                frame = Frame(self.tournament_F1)
                frame.grid(row=0, column=i)
                participant = self.tournament_participants[i]
                pic = PhotoImage(file="{}_icon_large.ppm".format(participant))
                imageButton = Button(frame, command=partial(self.initiate_tournament_fight, participant))
                imageButton.grid(row=0, column=i)
                imageButton.config(image=pic)
                if participant == "Abbot":
                    label = Label(frame, text=_("Abbot (weakened)"))
                else:
                    label = Label(frame, text=participant)
                label.grid(row=1, column=i)
                self.participant_buttons.append(imageButton)
                self.participant_images.append(pic)

            self.game.pre_tournament_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
            self.game.pre_tournament_win.update_idletasks()
            toplevel_w, toplevel_h = self.game.pre_tournament_win.winfo_width(), self.game.pre_tournament_win.winfo_height()
            self.game.pre_tournament_win.geometry("+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2,
                                                       5))
            self.game.pre_tournament_win.focus_force()
            self.game.pre_tournament_win.mainloop()  ###

        elif stage == 2:
            self.game.taskProgressDic["yagyu_clan"] = -10
            self.game.restore(0,0,full=True)
            if self.tournament_rounds_won >= 2:
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("It seems you guys could do with a few more years of training..."),
                     _("We lost fair and square. Shaolin is indeed the Supreme Sect in Wulin."),
                     _("Yagyu-san, we can't just let them--"),
                     _("Say no more, Yamamoto. A true samurai accepts defeat with humility."),
                     _("Abbot, we will take our leave now. Thank you for your hospitality."),
                     _("Farewell..."),
                     _("But Yagyu-san, what about--"), #Row 2
                     _("Yamamoto, we will discuss other matters once we are back home... Let's go."),
                     _("*The 3 of them depart from Shaolin.*"),
                     _("Thank you, {}! You've truly saved Shaolin's reputation today.").format(self.game.character),
                     _("Don't mention it, Abbot. I'm just glad I didn't lose face for Shaolin."),
                     _("You have great potential... Here... *Cough*... Take this Marrow Cleansing Manual."),
                     _("The Marrow Cleansing Technique is one of the top skills of Shaolin."), #Row 3
                     _("Unfortunately, I've not had enough time to learn it. Perhaps you will be able to master it."),
                     _("Thank you Abbot! I will do my best to learn it. Please be careful after I leave."),
                     _("I have a feeling those guys are up to no good..."),
                     _("I will; don't worry. Take care now... Amitabha...")
                     ],
                    ["you", "Yagyu Tadashi", "Yamamoto Takeshi", "Yagyu Tadashi", "Yagyu Tadashi", "Abbot",
                     "Yamamoto Takeshi", "Yagyu Tadashi", "Blank", "Abbot", "you", "Abbot",
                     "Abbot", "Abbot", "you", "you", "Abbot"]
                )

                self.game.add_item(_("Marrow Cleansing Manual"), 1)
                self.game.mapWin.deiconify()
                
            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Shaolin.png",
                    [_("Hahahahahaha! We have emerged victorious!"),
                     _("Yagyu Clan is strong indeed. You... *Cough*... won... Congratulations."),
                     _("As it stands, Shaolin should no longer be considered the Supreme Sect of Wulin."),
                     _("Perhaps, an announcement should be made to the martial arts community that the Yagyu Clan has taken Shaolin's place?"),
                     _("I don't think that's necessary, considering our Abbot was not feeling his best."),
                     _("Plus, the result of a quick spar cannot be used to evaluate a sect's true strength..."),
                     _("I disagree. It is clear from this tournament that even with outsider intervention, Shaolin is no match for us."), #Row 2
                     _("Abbot, please make the public announcement that from now on, Shaolin will be considered a sub-sect of the Yagyu Clan..."),
                     _("Amitabha... Although Shaolin wishes to remain non-competitive, I will not... *Cough*... allow our reputation to be tainted."),
                     _("Your outrageous demands will not be met! *Cough*..."),
                     _("I strongly urge you to reconsider, Abbot... Think about the potential consequences of your decision..."),
                     _("I remain firm in my decision. Now, if you wish... *Cough*... to join us for lunch, we will happily serve you."),
                     _("Otherwise, farewell and... *Cough*... take care..."),
                     _("What a shameful display of leadership... You and your people will suffer for your foolish stubborness..."), #Row 3
                     _("Ashikaga, Yamamoto... Let's go..."),
                     _("*The 3 of them storm off angrily.*"),
                     _("Sorry I wasn't much help Abbot..."),
                     _("Please don't say that. We're very grateful... *Cough*... for your bravery."),
                     _("Here, {}, take some of these to help with your wounds.").format(self.game.character),
                     _("*You receive 'Big Recovery Pill' x 3.*"),
                     _("Thank you Abbot and Elder Xuanjing! I will leave now; please be careful."),
                     _("We will; thank you. Please take care.")
                     ],
                    ["Yagyu Tadashi", "Abbot", "Yagyu Tadashi", "Yagyu Tadashi", "Elder Xuanjing", "Elder Xuanjing",
                     "Yagyu Tadashi", "Yagyu Tadashi", "Abbot", "Abbot", "Yagyu Tadashi", "Abbot", "Abbot",
                     "Yagyu Tadashi", "Yagyu Tadashi", "Blank", "you", "Abbot", "Elder Xuanjing", "Blank", "you", "Abbot"]
                )

                self.game.add_item(_("Big Recovery Pill"), 3)
                self.game.mapWin.deiconify()


    def initiate_tournament_fight(self, choice):
        shuffle(self.tournament_opp_participants)
        opp_choice = self.tournament_opp_participants.pop()
        self.game.pre_tournament_win.withdraw()

        #Make sure player can't re-select participant
        choice_index =  self.tournament_participants.index(choice)
        self.participant_buttons[choice_index].config(state=DISABLED)


        if opp_choice == "Ashikaga Yoshiro":
            opp = character(_("Ashikaga Yoshiro"), 20,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Hachiman Blade"), level=5)],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Kenjutsu II"), level=7)],
                            preferences=[2, 2, 4, 3, 4])
        elif opp_choice == "Yamamoto Takeshi":
            opp = character(_("Yamamoto Takeshi"), 19,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Hachiman Blade"), level=4)],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Kenjutsu II"), level=5)],
                            preferences=[2, 4, 2, 3, 2])

        else:
            opp = character(_("Yagyu Tadashi"), 25,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Hachiman Blade"), level=10)],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Kenjutsu II"), level=10),
                                    skill(_("Ninjutsu I"), level=10)],
                            preferences=[1, 3, 3, 1, 1])


        if choice == "you":
            self.game.health = self.game.healthMax
            self.game.stamina = self.game.staminaMax
            self.game.battleID = "shaolin_yagyu_tournament_self"
            self.game.battleMenu(self.game.you, opp, "Shaolin.png",
                                 "jy_suspenseful1.mp3", fromWin=self.game.pre_tournament_win,
                                 battleType="task", destinationWinList=[self.game.pre_tournament_win] * 3,
                                 postBattleCmd=self.post_tournament)

        else:
            if choice == "Elder Xuanjing":
                you = character(_("Elder Xuanjing"), 20,
                          sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                               special_move(_("Shaolin Diamond Finger"), level=10),
                               special_move(_("Empty Force Fist"), level=7),
                               special_move(_("Rest"))],
                          skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                  skill(_("Shaolin Inner Energy Technique"), level=10),
                                  skill(_("Shaolin Agility Technique"), level=10),])

            else:
                you = character(_("Abbot"), 20,
                                sml=[special_move(_("Shaolin Luohan Fist"), level=10),
                                     special_move(_("Shaolin Diamond Finger"), level=10),
                                     special_move(_("Guan Yin Palm"), level=10),
                                     special_move(_("Empty Force Fist"), level=10),
                                     special_move(_("Rest"))],
                                skills=[skill(_("Shaolin Mind Clearing Method"), level=10),
                                        skill(_("Tendon Changing Technique"), level=8),
                                        skill(_("Shaolin Inner Energy Technique"), level=10),
                                        skill(_("Shaolin Agility Technique"), level=10),])


            self.game.battleID = "shaolin_yagyu_tournament"
            self.game.battleMenu(you, opp, "Shaolin.png",
                                 "jy_suspenseful1.mp3", fromWin=self.game.pre_tournament_win,
                                 battleType="task", destinationWinList=[self.game.pre_tournament_win] * 3,
                                 postBattleCmd=self.post_tournament)



    def post_tournament(self, result):
        if result == 1:
            self.tournament_rounds_won += 1
        if self.tournament_round >= 3 or self.tournament_round - self.tournament_rounds_won == 2 or self.tournament_rounds_won >= 2:
            self.game.pre_tournament_win.quit()
            self.game.pre_tournament_win.destroy()
            self.tournament_vs_yagyu_clan(2)
        else:
            self.tournament_round += 1
            self.tournament_label.config(text=_("Select participant for round {}.").format(self.tournament_round))
            self.game.pre_tournament_win.deiconify()