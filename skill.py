from gettext import gettext
_ = gettext

class skill:
    def __init__(self, name, level=1):
        self.name = name
        self.level = level
        self.description = ""
        self.update_description()
        if self.name == _("Cherry Blossoms Floating on the Water"):
            self.file_name = "Cherry Blossoms Floating on the Water_icon.ppm"
        elif self.name == _("Shaolin Mind Clearing Method"):
            self.file_name = "Shaolin_Mind_Clearing_Method.png"
        elif self.name == _("Tendon Changing Technique"):
            self.file_name = "tendon_changing_technique_icon.png"
        elif self.name == _("Shaolin Inner Energy Technique"):
            self.file_name = "shaolin_inner_energy_technique_icon.ppm"
        elif self.name == _("Shaolin Agility Technique"):
            self.file_name = "shaolin_agility_technique_icon.ppm"
        elif self.name == _("Pure Yang Qi Skill"):
            self.file_name = "pure_yang_qi_skill_icon.ppm"
        elif self.name == _("Wudang Agility Technique"):
            self.file_name = "wudang_agility_technique_icon.ppm"
        elif self.name == _("Countering Poison With Poison"):
            self.file_name = "countering_poison_with_poison_icon.ppm"
        elif self.name == _("Phantom Steps"):
            self.file_name = "phantom_steps_icon.ppm"
        elif self.name == _("Recuperation"):
            self.file_name = "recuperation_icon.ppm"
        elif self.name == _("Yin Yang Soul Absorption Technique"):
            self.file_name = "yin_yang_soul_absorption_technique_icon.ppm"
        elif self.name == _("Huashan Agility Technique"):
            self.file_name = "huashan_agility_technique_icon.ppm"
        elif self.name == _("Dragonfly Dance"):
            self.file_name = "dragonfly_dance_icon.ppm"
        elif self.name == _("Divine Protection"):
            self.file_name = "divine_protection_icon.ppm"
        elif self.name == _("Leaping Over Mountain Peaks"):
            self.file_name = "leaping_over_mountain_peaks_icon.ppm"
        elif self.name == _("Burst of Potential"):
            self.file_name = "burst_of_potential_icon.ppm"
        elif self.name == _("Basic Agility Technique"):
            self.file_name = "basic_agility_technique_icon.ppm"
        elif self.name == _("Spreading Wings"):
            self.file_name = "spreading_wings_icon.ppm"
        elif self.name == _("Flower and Body Unite"):
            self.file_name = "flower_and_body_unite_icon.ppm"
        elif self.name == _("Marrow Cleansing Technique"):
            self.file_name = "marrow_cleansing_technique_icon.ppm"
        elif self.name == _("Taichi 18 Forms"):
            self.file_name = "taichi_eighteen_forms_icon.ppm"
        elif self.name == _("Huashan Sword Qi Gong"):
            self.file_name = "huashan_sword_qi_gong_icon.ppm"
        elif self.name == _("Splitting Mountains and Earth"):
            self.file_name = "splitting_mountains_and_earth_icon.ppm"
        elif self.name == _("Toad Form"):
            self.file_name = "toad_form_icon.png"
        elif self.name == _("Kenjutsu I"):
            self.file_name = "kenjutsu_i_icon.png"
        elif self.name == _("Kenjutsu II"):
            self.file_name = "kenjutsu_ii_icon.png"
        elif self.name == _("Bushido"):
            self.file_name = "bushido_icon.png"
        elif self.name == _("Ninjutsu I"):
            self.file_name = "ninjutsu_i_icon.png"
        elif self.name == _("Ninjutsu II"):
            self.file_name = "ninjutsu_ii_icon.png"
        elif self.name == _("Ninjutsu III"):
            self.file_name = "ninjutsu_iii_icon.png"
        elif self.name == _("Firm Resolve"):
            self.file_name = "firm_resolve_icon.png"
        elif self.name == _("Drunken Bliss"):
            self.file_name = "drunken_bliss_icon.png"
        elif self.name == _("Seductive Dance"):
            self.file_name = "seductive_dance_icon.png"
        elif self.name == _("Hun Yuan Yin Qi"):
            self.file_name = "hun_yuan_yin_qi_icon.png"


    def upgrade_skill(self, amount=1):
        if self.level < 10:
            self.level += amount
            self.update_description()


    def update_description(self):
        if self.name == _("Cherry Blossoms Floating on the Water"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, increases speed by {} and dexterity by {}.").format(i, 2*i,i) for i in range(1,11)])

        elif self.name == _("Huashan Sword Qi Gong"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, increases strength by {} for every armed attack learned.").format(i, i) for i in range(1,11)])

        elif self.name == _("Shaolin Mind Clearing Method"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, reduces chance of gaining negative status effects by {}%.".format(i, 2*i)) for i in range(1,11)])

        elif self.name == _("Tendon Changing Technique"):
            self.description = _("{} (level {})\n----------------------------------------\n\nAt end of turn, 20% chance of restoring status to 'Normal'.\n").format(self.name, self.level) + "\n".join([_("Level {}: Increases health and stamina upper limits by {}.").format(i, 20*i) for i in range(2,11)])

        elif self.name == _("Marrow Cleansing Technique"):
            self.description = _("{} (level {})\n----------------------------------------\n\nIn battle, reduces chance of gaining negative status effects by 50%.\nIncreases health upper limit by 50 and stamina upper limit by 150 for each upgrade.").format(self.name, self.level)

        elif self.name == _("Shaolin Inner Energy Technique"):
            self.description = _("{} (level {})\n----------------------------------------\n\nLevel 1: in battle, increases defence by 3.\n").format(self.name, self.level) +  "\n".join([_("Level {}: Increases stamina upper limit by {}. In battle, increases defence by {}.").format(i, i*15, i*3) for i in range(2,11)])

        elif self.name == _("Pure Yang Qi Skill"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, increases defence by {} (stack effect).").format(i, i // 3 + 1) for i in range(1,11)])

        elif self.name == _("Wudang Agility Technique"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, increases speed by {} and dexterity by {}.").format(i, i, 2*i) for i in range(1,11)])

        elif self.name == _("Countering Poison With Poison"):
            self.description = _("{} (level {})\n----------------------------------------\n\nIn battle, gains immunity to poison effects.".format(self.name, self.level, self.level))

        elif self.name == _("Phantom Steps"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, increases speed by {} and dexterity by {}.").format(i,int(1.5*i),int(1.5*i)) for i in range(1,11)])

        elif self.name == _("Shaolin Agility Technique"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, increases speed by {} and dexterity by {}.").format(i,i,i) for i in range(1,11)])

        elif self.name == _("Recuperation"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, recovers additional {}% health when using 'Rest'.").format(i,i) for i in range(1,11)])

        elif self.name == _("Yin Yang Soul Absorption Technique"):
            self.description = _("{} (level {})\n----------------------------------------\n\n50% of damage dealt to opponent is added to own health and stamina.\nWith each upgrade, health and stamina upper limit increases by 200.\n\n".format(self.name, self.level)) + "\n".join([_("Level {}: in battle, increases attack, strength, and speed by {}.").format(i, 10*i) for i in range(1,11)])

        elif self.name == _("Huashan Agility Technique"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("At level {}: in battle, increases speed by {} and dexterity by 1 (stack effect).").format(i, i//5+1) for i in range(1,11)])

        elif self.name == _("Dragonfly Dance"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("At level {}: in battle, increases dexterity by {}.").format(i, i*2) for i in range(1,11)])

        elif self.name == _("Divine Protection"):
            self.description = _("{} (level {})\n----------------------------------------\n25% of damage taken is dealt back to opponent.\n\nIncreases health and stamina upper limit by 150 for each upgrade.\n\nIn battle, increases defence by 5 for each level.".format(self.name, self.level))

        elif self.name == _("Leaping Over Mountain Peaks"):
            self.description = _("{} (level {})\n----------------------------------------\nIncreases speed and dexterity by 3 when skill is obtained.\nIncreases speed and dexterity by 1 for each upgrade.".format(self.name, self.level))

        elif self.name == _("Burst of Potential"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, increases attack by {} and strength by {}.").format(i, 3*i, 3*i) for i in range(1, 11)])

        elif self.name == _("Basic Agility Technique"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, increases dexterity by {}.").format(i, int(i*1.5)) for i in range(1, 11)])

        elif self.name == _("Spreading Wings"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, increases speed by {} and attack by {}.").format(i, 2*i,i) for i in range(1,11)])

        elif self.name == _("Flower and Body Unite"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level) + "\n".join([_("Level {}: in battle, increases all stats by {}.").format(i, 2*i) for i in range(1,11)])

        elif self.name == _("Taichi 18 Forms"):
            self.description = _("{} (level {})\n----------------------------------------\n\nIn battle, receives damage bonus based on defence.\nIncreases stamina upper limit by 200 for each upgrade.").format(self.name, self.level)

        elif self.name == _("Splitting Mountains and Earth"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level)  + "\n".join([_("Level {}: in battle, increases strength by {} and increases chance of critical hit by {}%.").format(i, 3*i, 3*i) for i in range(1,11)])

        elif self.name == _("Toad Form"):
            self.description = _("{} (level {})\n----------------------------------------\n\n").format(self.name, self.level)  + "\n".join([_("Level {}: in battle, opponent takes +{}% damage from poison (applied to base poison damage).").format(i, 3*i) for i in range(1,11)])

        elif self.name == _("Kenjutsu I"):
            self.description = _("{} (level {})\n----------------------------------------\nArt of the Sword\n\n").format(self.name, self.level)  + "\n".join([_("Level {}: in battle, increases attack by {} and gives +{}% damage bonus for armed attacks.").format(i, 2*i, 2*i) for i in range(1,11)])

        elif self.name == _("Kenjutsu II"):
            self.description = _("{} (level {})\n----------------------------------------\nArt of the Sword\n\n").format(self.name, self.level)  + "\n".join([_("Level {}: in battle, increases strength by {} and gives +{}% damage bonus for armed attacks.").format(i, 3*i, round(2.5*i,1)) for i in range(1,11)])

        elif self.name == _("Bushido"):
            self.description = _("{} (level {})\n----------------------------------------\nWay of the Warrior\n\nIncreases health and stamina upper limit by 100 for each upgrade.\n\nIn battle, increases strength by 4 for each level.".format(self.name, self.level))

        elif self.name == _("Ninjutsu I"):
            self.description = _("{} (level {})\n----------------------------------------\nArt of the Ninja\n\n").format(self.name, self.level)  + "\n".join([_("Level {}: in battle, increases attack, speed, and dexterity by {}.").format(i, i) for i in range(1,11)])

        elif self.name == _("Ninjutsu II"):
            self.description = _("{} (level {})\n----------------------------------------\nArt of the Ninja\n\n").format(self.name, self.level)  + "\n".join([_("Level {}: in battle, increases attack, speed, and dexterity by {}.").format(i, 3*i) for i in range(1,11)])

        elif self.name == _("Ninjutsu III"):
            self.description = _("{} (level {})\n----------------------------------------\nArt of the Ninja\n\nIncreases health and stamina upper limit by 100 for each upgrade.\n\n").format(self.name, self.level)  + "\n".join([_("Level {}: in battle, increases chance of critical hit by {}%.").format(i, 5*i) for i in range(1,11)])

        elif self.name == _("Firm Resolve"):
            self.description = _("{}\n----------------------------------------\n\n").format(self.name) + "Nothing can sway the hearts of those with indomitable willpower.\n\nIn battle, reduces chance of gaining negative status effects by 15%."

        elif self.name == _("Drunken Bliss"):
            self.description = _("{}\n----------------------------------------\n\n").format(self.name) + "Incorporating a bit of drunkenness into your moves can greatly increase their power.\n\nIn battle, when using item 'Wine', gain an additional bonus of +10 strength."

        elif self.name == _("Seductive Dance"):
            self.description = _("{}\n----------------------------------------\n\n").format(self.name) + "Who can resist the charm of a beautiful woman?\n\nAfter taking damage from an opponent's attack, reduces opponent's stats by 3 and stamina by 10%."

        elif self.name == _("Hun Yuan Yin Qi"):
            self.description = _("{} (level {})\n----------------------------------------\nDefensive technique practiced by high-level eunuchs.\nOne must be castrated to practice it.\n\nReceives +50 defense in battle.\nIncreases stamina upper limit by 300 for each upgrade.\n\n").format(self.name, self.level)