from gettext import gettext
from functools import partial
from special_move import*
from character import*
from skill import*
from mini_game_small_town_archer_battle import*
from mini_game_small_town_forest import*
from helper_funcs import *
import tkinter.messagebox as messagebox
import time

_ = gettext


class Scene_small_town:

    def __init__(self, game):

        self.game = game

        if self.game.taskProgressDic["small_town"] == 1 and self.game.taskProgressDic["small_town_ghost"] >= 10 and self.game.taskProgressDic["feng_pan"] >= 50:
            time.sleep(.5)
            self.game.stopSoundtrack()
            self.game.currentBGM = "xlfd_suspense.mp3"
            self.game.startSoundtrackThread()
            self.game.generate_dialogue_sequence(
                None,
                "Small Town.png",
                [_("What did we do wrong? Why are you arresting us?"),
                 _("Shut up, old lady. Keep moving."),
                 _("Sir, please be gentle. We will do as you say."),
                 _("Hold on a second! Why are you guys arresting people in my territory? What crimes have they committed?"),
                 _("That... uh..."),
                 _("Ah, you must be First Lieutenant Qin Long... I am General Feng Pan."),
                 _("An honor to meet you, sir."),
                 _("Allow me to explain... You see, this lady and her husband has committed treason of the highest degree."), #Row 2
                 _("They tried to poison the Emperor..."),
                 _("Wh-what? H-how?! We've never met the Emperor!"),
                 _("There must be a mistake, General Feng. The Emperor has never set foot in our town."),
                 _("There is no mistake, Lieutenant Qin... The Emperor took a tour around the country dressed as a commoner 3 months ago."),
                 _("And when he came to this town last month, he ordered some tea eggs from this old lady."),
                 _("Soon after, he felt sick and threw up, but luckily, the doctors were able to take care of him."), #Row 3
                 _("I've been selling tea eggs for over 40 years. No one has ever made a complaint. Surely--"),
                 _("Enough, you insolent peasant! Do you think you have any say in this matter?"),
                 _("The Emperor has ordered me to arrest you and your husband and execute you publicly for treason."),
                 _("Do you dare disobey the Emperor?"),
                 _("Please, General, we are innocent. We have not done any wrong!"),
                 _("Shut up, old man! Men, arrest them!"),
                 _("Hold it! General Feng, I have known Grandma and Grandpa Kang for many years. They would not even harm a fly, much less a person."), #Row 4
                 _("Please, let us investigate this matter --"),
                 _("I think you have not heard me clearly, Lieutenant Qin... The Emperor has ordered their arrest and execution."),
                 _("Now, if you still wish to keep your job --"),
                 _("My job is to protect the people of this town... even if that means resisting a tyrant..."),
                 _("What?! What did you say??? How dare you speak of His Majesty like that! You must be out of your mind!"),
                 _("Perhaps I am. As long as I'm here, you won't be taking anyone with you today, General Feng."),
                 _("And you think you can stop me... Fool..."), #Row 5
                 _("I may not be able to stop you, but I will sooner die than to abandon my people.")
                 ],
                ["Grandma Kang", "Spearmen", "Grandpa Kang", "Qin Long", "Spearmen", "Feng Pan", "Qin Long",
                 "Feng Pan", "Feng Pan", "Grandma Kang", "Qin Long", "Feng Pan", "Feng Pan",
                 "Feng Pan", "Grandma Kang", "Feng Pan", "Feng Pan", "Feng Pan", "Grandpa Kang", "Feng Pan",
                 "Qin Long", "Qin Long", "Feng Pan", "Feng Pan", "Qin Long", "Feng Pan", "Qin Long",
                 "Feng Pan", "Qin Long"]
            )

            time.sleep(.25)
            self.game.currentEffect = "sfx_male_voice.wav"
            self.game.startSoundEffectThread()
            time.sleep(.25)
            self.game.currentEffect = "sfx_shapeless_palm.wav"
            self.game.startSoundEffectThread()
            time.sleep(.5)
            self.game.currentEffect = "male_grunt3.wav"
            self.game.startSoundEffectThread()

            self.game.generate_dialogue_sequence(
                None,
                "Small Town.png",
                [_("Uuaahhhh!!"),
                 _("Weak... "),
                 _("Qin Long! Are you ok? What's going on here?"),
                 _("What's all this commotion for?"),
                 _("Oh good, more lowly peasants... Men, arrest all of them! This entire town will be tried for treason!")],
                ["Qin Long", "Feng Pan", "Lin Anning", "Student", "Feng Pan"],
                [[_("Help the townspeople."), partial(self.town_treason, 1)],
                 [_("Sneak away before someone notices you."), partial(self.town_treason, 0)]]
            )

            return

        elif self.game.taskProgressDic["small_town"] == 10:
            self.game.taskProgressDic["small_town"] = 100
            self.game.generate_dialogue_sequence(
                None,
                "Small Town.png",
                [_("(Hmmm, looks like this place was searched thoroughly and plundered, but no traces of blood anywhere.)"),
                 _("(The townspeople must've escaped.)")],
                ["you", "you"]
            )

        elif self.game.taskProgressDic["small_town"] == 20:
            if self.game.taskProgressDic["small_town_ghost"] == 20 and random() <= .5:
                self.game.taskProgressDic["small_town"] = 100
                self.game.generate_dialogue_sequence(
                    None,
                    "Small Town.png",
                    [_("*At the entrance of the town, you notice the lifeless body of the murderer from 15 years ago, his body pierced by hundreds of arrows..*"),
                     _("*As you walk through the town, there are obvious signs of destruction but no traces of blood anywhere.*"),
                     _("(He must've sacrificed himself to buy more time for the townspeople to escape.)"),
                     _("(I suppose this is the best way for him to make up for his past sins...)")],
                    ["Blank", "Blank", "you", "you"]
                )

            else:
                self.game.taskProgressDic["small_town"] = 200
                self.game.generate_dialogue_sequence(
                    None,
                    "Small Town.png",
                    [_("*The bodies of the townspeople lay scattered in the streets.*"),
                     _("*Tears roll down your cheek as you come to the realization that you were unable to buy them enough time to escape.*"),
                     _("*You manage to identify the bodies of Qin Long, Lin Anning, Grandpa and Grandma Kang, and the Student and give them a proper burial.*"),
                     _("Rest in peace. I will avenge all of you! I will kill the tyrant who is responsible for your deaths!"),
                     _("Hey, where do you think you're going?"),
                     _("Get out of my way, Scrub... Why do you have to bother me now?"),
                     _("You really think you can avenge the townspeople? You think it's that easy to assassinate the Emperor?")],
                    ["Blank", "Blank", "Blank", "you", "Scrub", "you", "Scrub"]
                )

                if self.game.chivalry < -50:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Small Town.png",
                        [_("I don't care. I'm going to make sure the townspeople get justice."),
                         _("Hah, he who lacks integrity cries out for justice... What irony..."),
                         _("What did you say?!"),
                         _("Did I stutter? Or have you forgotten about your shameful acts?"),
                         _("You seek justice for the townspeople, but what about those whom you've mistreated?"),
                         _("What makes you think you have the right to demand justice? You're not qualified..."),
                         _("I..."),
                         _("Rather than sending yourself on a suicide mission in the name of justice, why don't you try to do more good for others?"),
                         _("It would be far more beneficial than to add your name to the list of people slaughtered by General Feng."),
                         _("....."),
                         _("Alright, I'm done. Hope you can calm down and think more clearly now, bye.")
                        ],
                        ["you", "Scrub", "you", "Scrub", "Scrub", "Scrub", "you", "Scrub", "Scrub", "you", "Scrub"]
                    )

                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Small Town.png",
                        [_("Do you even know where the Emperor is? And even if you find him, how will you kill him?"),
                         _("You've only seen a small portion of the palace and haven't even encountered 5% of the imperial guards and soldiers."),
                         _("How are you going to get past tens of thousands of well-trained soldiers?"),
                         _("General Feng's archers are nothing compared to those who are guarding the inner palace where the Emperor resides."),
                         _("Yet you were struggling with them..."),
                         _("But... I... I can get stronger... I..."),
                         _("I know you're upset, but the reality is, there's always going to be injustice in this world."),
                         _("And sometimes, there's nothing you can do, especially when it comes from those in power."),
                         _("Change takes time, and it's pointless to react emotionally to the situation."),
                         _("Alright, that's all I have to say. Hope you can calm down and think clearly. Take care.")
                         ],
                        ["Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "you", "Scrub", "Scrub", "Scrub", "Scrub"]
                    )


        elif self.game.taskProgressDic["small_town"] == -1000:
            self.game.generate_dialogue_sequence(
                    None,
                    "Small Town.png",
                    [_("(Wow, this town has turned into a ghost town.)"),
                     _("(I guess everyone was arrested and taken back to be executed. Good thing I got away safely!)")],
                    ["you", "you"]
                )


        self.game.small_town_win = Toplevel(self.game.mapWin)
        self.game.small_town_win.title(_("Small Town"))

        bg = PhotoImage(file="Small Town.png")
        self.w = bg.width()
        self.h = bg.height()

        self.canvas = Canvas(self.game.small_town_win, width=self.w, height=self.h)
        self.canvas.pack()
        self.canvas.create_image(0, 0, anchor=NW, image=bg)

        self.game.small_townF1 = Frame(self.canvas)
        pic1 = PhotoImage(file="small_town_walls_icon.ppm")
        imageButton1 = Button(self.game.small_townF1, command=self.go_small_town_walls)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        self.game.small_townF1.place(x=self.w * 1 // 4 - pic1.width() // 2, y=self.h // 4 - pic1.height() // 2)

        self.game.small_townF2 = Frame(self.canvas)
        pic2 = PhotoImage(file="Town House_icon_large.ppm")
        imageButton2 = Button(self.game.small_townF2, command=self.go_town_house)
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        self.game.small_townF2.place(x=self.w * 3 // 4 - pic2.width() // 2, y=self.h // 4 - pic2.height() // 2)

        if self.game.taskProgressDic["small_town"] in [0,1]:
            self.game.small_townF3 = Frame(self.canvas)
            pic3 = PhotoImage(file="Lin Anning_icon_large.ppm")
            imageButton3 = Button(self.game.small_townF3, command=self.clicked_on_lin_anning)
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=pic3)
            label = Label(self.game.small_townF3, text=_("Lin Anning"))
            label.grid(row=1, column=0)
            self.game.small_townF3.place(x=self.w * 1 // 4 - pic3.width() // 2, y=self.h // 2 + pic3.height() // 4)


        self.game.small_townF4 = Frame(self.canvas)
        pic4 = PhotoImage(file="small_town_inn_icon_large.ppm")
        imageButton4 = Button(self.game.small_townF4, command=self.go_small_town_inn)
        imageButton4.grid(row=0, column=0)
        imageButton4.config(image=pic4)
        self.game.small_townF4.place(x=self.w * 3 // 4 - pic4.width() // 2, y=self.h // 2 + pic4.height() // 4)


        menu_frame = self.game.create_menu_frame(self.game.small_town_win)
        menu_frame.pack()
        self.game.small_town_win_F1 = Frame(self.game.small_town_win)
        self.game.small_town_win_F1.pack()

        self.game.small_town_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.small_town_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.small_town_win.winfo_width(), self.game.small_town_win.winfo_height()
        self.game.small_town_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.small_town_win.focus_force()
        self.game.small_town_win.mainloop()  ###


    def go_town_house(self):

        if self.game.taskProgressDic["small_town"] == -1:
            self.game.taskProgressDic["small_town"] = 0
            self.game.generate_dialogue_sequence(
                self.game.small_town_win,
                "Small Town.png",
                [_("Wait!"),
                 _("......?"),
                 _("Sorry, you look like you are new here..."),
                 _("I just wanted to give you a warning before you go anywhere..."),
                 _("And you are...?"),
                 _("My name is Lin Anning. I grew up in this town."),
                 _("Ah, nice to meet you. My name is {}.").format(self.game.character),
                 _("Anyway, I saw you were about to head towards... that house... so I had to stop you."), #Row 2
                 _("Hmmm? Why is that?"),
                 _("Well, it has to do with this town's history..."),
                 _("Our town has always been peaceful and the people friendly towards each other."),
                 _("That is, until 15 years ago, when a young teenager came here..."),
                 _("I was still a small child at the time and have no recollection of this, but according to legend"),
                 _("This youngster was an orphan. No one knows why or how he came to our town, but he did."),
                 _("An elderly lady in the town was kind enough to take care of him."), #Row 3
                 _("However, a few months later, this teen went crazy and killed the elderly lady along with her grandson."),
                 _("It is unclear how or where he learned martial arts, but he became too powerful for even the local government to arrest him."),
                 _("His rampage continued, and soon, the gardener, the tailor, the businessman, and others fell victim to his blade."),
                 _("So what eventually became of this crazy teen?"),
                 _("He... still lives in the house of the elderly lady. The town officials tried many times to capture him but to no avail."),
                 _("Didn't the government get involved? Surely they can send an army over here to take care of this guy..."),
                 _("*Sigh* The government officials don't have time to care about a small town like ours."), #Row 4
                 _("Most of them are corrupt and only focus on how to get rich and maintain their positions of power."),
                 _("Plus, the murderer hasn't been a threat for the past 15 years."),
                 _("He stays in the house all day, though some claim to have seen him go into the woods at night in search of food."),
                 _("So as long as no one bothers him, we should be safe, right?"),
                 _("Yeah, I think so..."),
                 _("Alright, thanks for the warning. I'll keep that in mind..."),
                 _("No problem! Enjoy your stay!")
                 ],
                ["Lin Anning", "you", "Lin Anning", "Lin Anning", "you", "Lin Anning", "you",
                 "Lin Anning", "you", "Lin Anning", "Lin Anning", "Lin Anning", "Lin Anning", "Lin Anning",
                 "Lin Anning", "Lin Anning", "Lin Anning", "Lin Anning", "you", "Lin Anning", "you",
                 "Lin Anning", "Lin Anning", "Lin Anning", "Lin Anning", "you", "Lin Anning", "you", "Lin Anning",
                 ],
            )

            self.game.small_townF3 = Frame(self.canvas)
            self.pic3 = PhotoImage(file="Lin Anning_icon_large.ppm")
            imageButton3 = Button(self.game.small_townF3, command=self.clicked_on_lin_anning)
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=self.pic3)
            label = Label(self.game.small_townF3, text=_("Lin Anning"))
            label.grid(row=1, column=0)
            self.game.small_townF3.place(x=self.w * 1 // 4 - self.pic3.width() // 2, y=self.h // 2 + self.pic3.height() // 4)
            return


        if self.game.taskProgressDic["small_town_ghost"] not in [2] and self.game.taskProgressDic["small_town"] not in [10,20,100,200,-1000]:
            self.game.stopSoundtrack()
            self.game.currentBGM = "three_people.mp3"
            self.game.startSoundtrackThread()


        self.game.small_town_win.withdraw()
        self.game.small_town_house_win = Toplevel(self.game.small_town_win)
        self.game.small_town_house_win.title(_("House"))

        bg = PhotoImage(file="Town House.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.small_town_house_win, width=self.w, height=self.h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        if self.game.taskProgressDic["small_town_ghost"] not in [10] and self.game.taskProgressDic["small_town"] not in [10,20,100,200,-1000]:
            self.game.small_town_houseF1 = Frame(canvas)
            pic1 = PhotoImage(file="Swordsman_icon_large.ppm")
            imageButton1 = Button(self.game.small_town_houseF1, command=self.clicked_on_swordsman)
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            self.game.small_town_houseF1.place(x=self.w // 4 - pic1.width() // 2, y=self.h // 4 - pic1.height() // 2)

        self.game.small_town_houseF3 = Frame(canvas)
        pic3 = PhotoImage(file="small_town_icon_large.ppm")
        imageButton3 = Button(self.game.small_town_houseF3,
                              command=partial(self.game.winSwitch, self.game.small_town_house_win,
                                              self.game.small_town_win))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        self.game.small_town_houseF3.place(x=self.w//4 - pic3.width() // 2, y=self.h//2 + pic3.height() // 4)


        menu_frame = self.game.create_menu_frame(self.game.small_town_house_win)
        menu_frame.pack()
        self.game.small_town_house_win_F1 = Frame(self.game.small_town_house_win)
        self.game.small_town_house_win_F1.pack()

        self.game.small_town_house_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.small_town_house_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.small_town_house_win.winfo_width(), self.game.small_town_house_win.winfo_height()
        self.game.small_town_house_win.geometry("+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2,
                                                           5))
        self.game.small_town_house_win.focus_force()
        self.game.small_town_house_win.mainloop()


    def clicked_on_swordsman(self):

        if self.game.taskProgressDic["small_town_ghost"] == 2:
            self.game.generate_dialogue_sequence(
                self.game.small_town_house_win,
                "Town House.png",
                [_("Have you really been staying in this house for the past 15 years?"),
                 _("Yes. I drink water from the well and eat fruit from the trees."),
                 _("I have not stepped outside the house since the last murder I committed.")],
                ["you", "Swordsman", "Swordsman"]
            )
            return

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        for widget in self.game.small_town_house_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.small_town_house_win_F1, text=_("What is your name?"),
               command=partial(self.talk_to_swordsman,1)).grid(row=1, column=0)

        Button(self.game.small_town_house_win_F1, text=_("You heartless murderer!"),
               command=partial(self.talk_to_swordsman,2)).grid(row=2, column=0)


    def talk_to_swordsman(self, choice):
        if choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.small_town_house_win,
                "Town House.png",
                [_("My name is unimportant... What's important is that I'm someone with a dark past..."),
                 _("........")],
                ["Swordsman", "you"]
            )

        elif choice == 2:
            if self.game.taskProgressDic["small_town"] == 0:
                self.game.taskProgressDic["small_town"] = 1
            self.game.small_town_house_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Town House.png",
                [_("You're quite right; that is indeed what I am..."),
                 _("........"),
                 _("Aren't you even going to threaten me or get angry?"),
                 _("No."),
                 _("Then why did you kill those innocent people??"),
                 _("*Sigh* Very well. I will tell you everything that happened."),
                 _("Hopefully, it will help you avoid the same mistake that I made..."),
                 _("15 years ago, I came to this village as an orphan. I was 14 years old at the time."), #Row 2
                 _("I lost both parents at an early age, and having often been bullied on the streets"),
                 _("I vowed to become the strongest fighter in the world so that no one will ever look down on me again."),
                 _("Having wandered aimlessly for who knows how long, I finally came to this small town."),
                 _("A kind, elderly woman whom everyone called 'Popo' took me in right away."),
                 _("And I stayed at her house with her grandson, Xiao Wantong."),
                 _("I helped Popo with chores around the house, such as chopping wood, sweeping the floor, and drawing water from the well."),
                 _("Popo rewarded me for completing the chores, and I quickly felt that I was at home."), #Row 3
                 _("Then what made you crazy...?"),
                 _("That I still can't fully explain, but it has to do with my ambition..."),
                 _("Like I mentioned before, I wanted to become the strongest..."),
                 _("One day, while wandering around the outskirts of the town, I met a martial arts expert by chance."),
                 _("He wore a creepy mask, and there was something distinct about his voice."),
                 _("Upon learning about my ambition, he chuckled and told me he could help me achieve that goal."),
                 _("He also told me that the strongest person at the time was the leader of Shanhu Sect, who had only recently acquired the position."), #Row 4
                 _("If I wanted to become the strongest, I would need to defeat him."),
                 _("The man then proceeded to teach me some powerful techniques before leaving."),
                 _("During the day, I would help Popo with chores, and at night, I would sneak out into the woods to practice in secret."),
                 _("I didn't know why, but I had a feeling Popo would not approve of the techniques I had learned."),
                 _("After a few weeks, I began to go around the town, finding people to spar."),
                 _("Even though I was a teenager, no one in the town was a match for me, not even the strong village men who carried boulders and bricks."),
                 _("I started to get impatient and bored, and Popo's tasks and rewards also began to seem meaningless."), #Row 5
                 _("Then one day... I killed Popo and Xiao Wantong with my own hands..."),
                 _("I killed them because they no longer seemed useful to me..."),
                 _("I killed them because I sought after the thrill of murdering someone and being a wanted criminal..."),
                 _("I thought it would bring me the challenge that I needed to advance to the next level..."),
                 _("But I was wrong. I only felt emptiness."),
                 _("I thought maybe it was because killing them was too easy, so I continued my rampage..."),
                 _("The famous gardener in the town... the business man with whom I traded..."), #Row 6
                 _("The inn keeper from whom I bought dumplings... One by one... Dead..."),
                 _("The townspeople soon reported me to the officials, and they sent a group of soldiers to arrest me."),
                 _("But they, too, were too weak..."),
                 _("From that moment, I lost direction... I lost the sense of peace I had whenever I came home to Popo's house..."),
                 _("I realized how meaningless it was to become the strongest person when there's no one to love and no one who loves you."),
                 _("I tried to find that comfort, that love again so I returned here to Popo's house... but it was too late..."),
                 _("The source of that comfort and love was gone... and would never return again..."), #Row 7
                 _("I've spent the past 15 years here, living in remorse for my sins..."),
                 _("I vowed never to pick up my sword again or harm another life."),
                 _("If you wish to kill me, go ahead... I will not strike back.")
                 ],
                ["Swordsman", "you", "you", "Swordsman", "you", "Swordsman", "Swordsman",
                 "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman",
                 "Swordsman", "you", "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman",
                 "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman",
                 "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman",
                 "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman", "Swordsman",
                 "Swordsman", "Swordsman", "Swordsman", "Swordsman"],
                [[_("Die, you ruthless villain!"), self.kill_swordsman],
                 [_("Spare him for now."), partial(self.kill_swordsman, False)]]
            )


    def kill_swordsman(self, kill=True):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        if kill:
            self.game.generate_dialogue_sequence(
                self.game.small_town_house_win,
                "Town House.png",
                [_("*As you bring your blade down towards his head, you notice that the man doesn't even flinch.*"),
                 _("*You stop right as the blade is 2 inches away from his face.*"),
                 _("As much as you deserve to die, I can't kill someone who's not even going to defend himself..."),
                 _("One day, you will get what you deserve...")],
                ["Blank", "Blank", "you", "you"]
            )
        else:
            self.game.small_town_house_win.deiconify()


    def go_small_town_walls(self):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        self.game.small_town_win.withdraw()
        self.game.small_town_walls_win = Toplevel(self.game.small_town_win)
        self.game.small_town_walls_win.title(_("Town"))

        bg = PhotoImage(file="Small Town Walls.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.small_town_walls_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        if self.game.taskProgressDic["small_town"] not in [10,20,100,200,-1000]:
            self.game.small_town_wallsF1 = Frame(canvas)
            pic1 = PhotoImage(file="Qin Long_icon_large.ppm")
            imageButton1 = Button(self.game.small_town_wallsF1, command=self.clicked_on_qin_long)
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            label = Label(self.game.small_town_wallsF1, text=_("Qin Long"))
            label.grid(row=1, column=0)
            self.game.small_town_wallsF1.place(x=w // 4 - pic1.width() // 2, y=h // 4 - pic1.height() *2//3)

            self.game.small_town_wallsF2 = Frame(canvas)
            pic2 = PhotoImage(file="Grandpa Kang_icon_large.ppm")
            imageButton2 = Button(self.game.small_town_wallsF2, command=self.clicked_on_grandpa_kang)
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            label = Label(self.game.small_town_wallsF2, text=_("Grandpa Kang"))
            label.grid(row=1, column=0)
            self.game.small_town_wallsF2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 4 - pic2.height()*2//3)

            self.game.small_town_wallsF4 = Frame(canvas)
            pic4 = PhotoImage(file="Grandma Kang_icon_large.ppm")
            imageButton4 = Button(self.game.small_town_wallsF4, command=self.clicked_on_grandma_kang)
            imageButton4.grid(row=0, column=0)
            imageButton4.config(image=pic4)
            label = Label(self.game.small_town_wallsF4, text=_("Grandma Kang"))
            label.grid(row=1, column=0)
            self.game.small_town_wallsF4.place(x=w * 3 // 4 - pic4.width() // 2, y=h // 2 + pic4.height() // 5)


        self.game.small_town_wallsF3 = Frame(canvas)
        pic3 = PhotoImage(file="small_town_icon_large.ppm")
        imageButton3 = Button(self.game.small_town_wallsF3,
                              command=partial(self.game.winSwitch, self.game.small_town_walls_win,
                                              self.game.small_town_win))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        self.game.small_town_wallsF3.place(x=w // 4 - pic3.width() // 2, y=h // 2 + pic3.height() // 5)


        menu_frame = self.game.create_menu_frame(self.game.small_town_walls_win)
        menu_frame.pack()
        self.game.small_town_walls_win_F1 = Frame(self.game.small_town_walls_win)
        self.game.small_town_walls_win_F1.pack()

        self.game.small_town_walls_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.small_town_walls_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.small_town_walls_win.winfo_width(), self.game.small_town_walls_win.winfo_height()
        self.game.small_town_walls_win.geometry("+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2,
                                                          5))
        self.game.small_town_walls_win.focus_force()
        self.game.small_town_walls_win.mainloop()


    def clicked_on_qin_long(self):
        if self.game.taskProgressDic["small_town_ghost"] == 2:
            self.game.generate_dialogue_sequence(
                self.game.small_town_walls_win,
                "Small Town Walls.png",
                [_("Has anyone ever reported the sighting of ghost at the inn?"),
                 _("No one has reported it as an official case, though I've heard rumors about it..."),
                 _("Anning also told me about it since she plays music for the guests there sometimes."),
                 _("Did you look into it?"),
                 _("I tried, but the inn keeper wouldn't let me."),
                 _("And since no damage was done and no one was harmed, I couldn't open an investigation against the inn keeper's will."),
                 _("Hmmm, I see... Thanks...")],
                ["you", "Qin Long", "Qin Long", "you", "Qin Long", "Qin Long", "you"]
            )
            return

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        for widget in self.game.small_town_walls_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.small_town_walls_win_F1, text=_("Talk"),
               command=self.talk_to_qin_long).grid(row=1, column=0)

        Button(self.game.small_town_walls_win_F1, text=_("Spar"),
               command=self.spar_with_qin_long).grid(row=2, column=0)


    def talk_to_qin_long(self):
        r = randrange(2)
        if r == 0:
            self.game.generate_dialogue_sequence(
                self.game.small_town_walls_win,
                "Small Town Walls.png",
                [_("Hey, there! My name is Qin Long."),
                 _("I protect the people of this town along with the 3 score soldiers under my command.")],
                ["Qin Long", "Qin Long"]
            )
        else:
            self.game.generate_dialogue_sequence(
                self.game.small_town_walls_win,
                "Small Town.png",
                [_("Where did you learn martial arts?"),
                 _("I actually don't know much besides some basic moves. There is still much for me to learn."),
                 _("A couple of years ago, Xiao Yong from the Snowy Mountains taught me Demon Supressing Blade so that I can better protect the townspeople."),
                 _("However, I'm not gifted in martial arts and have yet to master the technique."),
                 _("Maybe I ought to consult him some more the next time he visits...")],
                ["you", "Qin Long", "Qin Long", "Qin Long", "Qin Long"]
            )


    def spar_with_qin_long(self):
        self.game.generate_dialogue_sequence(
            self.game.small_town_walls_win,
            "Small Town Walls.png",
            [_("I enjoy sparring with outsiders. It's a great way to learn new techniques and make friends!"),
             _("Please take it easy on me though...")],
            ["Qin Long", "Qin Long"]
        )
        opp = character(_("Qin Long"), 10,
                        sml=[special_move(_("Basic Sword Technique"), level=10),
                             special_move(_("Demon Suppressing Blade"), level=5)],
                        skills=[skill(_("Basic Agility Technique"), level=10)])

        self.game.battleMenu(self.game.you, opp, "Small Town Walls.png",
                             "jy_tianwaicun.mp3", fromWin=self.game.small_town_walls_win,
                             battleType="training", destinationWinList=[self.game.small_town_walls_win] * 3)


    def clicked_on_grandpa_kang(self):
        if self.game.taskProgressDic["small_town_ghost"] == 2:
            self.game.generate_dialogue_sequence(
                self.game.small_town_walls_win,
                "Small Town Walls.png",
                [_("Hey there, Grandpa Kang. Have you heard anything about a ghost in this town?"),
                 _("I see darkness... darkness ahead... Oh, what is this?"),
                 _("I see trees... a small flame... and darkness... more darkness! ..."),
                 _("...")],
                ["you", "Grandpa Kang", "Grandpa Kang", "you"]
            )
            return

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        self.game.small_town_walls_win.withdraw()
        self.game.generate_dialogue_sequence(
            None,
            "Small Town Walls.png",
            [_("Good morning, young man! I can see that something troubles you... "),
             _("Would you like me to read your palm? Perhaps it will help you."),
             _("I charge a small fee of just 10 gold.")],
            ["Grandpa Kang", "Grandpa Kang", "Grandpa Kang"],
            [[_("Sure thing."), partial(self.read_palm,1)],
             [_("No thanks."), partial(self.read_palm,0)]]
        )


    def read_palm(self, read=0):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        if read > 0:
            if not self.game.check_for_item(_("Gold"), 10):
                messagebox.showinfo("", _("You don't have enough gold."))
                self.game.small_town_walls_win.deiconify()
                return

            self.game.inv[_("Gold")] -= 10
            if random() <= .2:
                self.game.luck += 3
                self.game.generate_dialogue_sequence(
                    self.game.small_town_walls_win,
                    "Small Town Walls.png",
                    [_("Most auspicious day, young man! I see great fortune ahead of you!"),
                     _("Ahhh, pleasant words to my itching ears..."),
                     _("Your luck +3.")],
                    ["Grandpa Kang", "you", "Blank"]
                )
            else:
                r = randrange(3)
                #0: marriage    1: sect    2: random
                if r == 0:
                    if self.game.taskProgressDic["wife_intimacy"] == -1000:
                        self.game.generate_dialogue_sequence(
                            self.game.small_town_walls_win,
                            "Small Town Walls.png",
                            [_("Ayyy, young man, I see you have committed some terrible deeds in the past."),
                             _("I'm afraid you shall have no part in marriage in this life...")],
                            ["Grandpa Kang", "Grandpa Kang"]
                        )
                    elif _("Yin Yang Soul Absorption Manual") in self.game.inv:
                        self.game.generate_dialogue_sequence(
                            self.game.small_town_walls_win,
                            "Small Town Walls.png",
                            [_("Young man, if marriage is still something you desire, I urge you to be very careful..."),
                             _("I sense that your love life is hanging by a thread...")],
                            ["Grandpa Kang", "Grandpa Kang"]
                        )
                    elif self.game.taskProgressDic["wife_intimacy"] == -1:
                        self.game.generate_dialogue_sequence(
                            self.game.small_town_walls_win,
                            "Small Town Walls.png",
                            [_("I see you are not yet married."),
                             _("Perhaps you will experience a pleasant encounter near a tall cliff if you venture there towards end of the month...")],
                            ["Grandpa Kang", "Grandpa Kang"]
                        )
                    elif self.game.taskProgressDic["wife_intimacy"] >= 0:
                        self.game.generate_dialogue_sequence(
                            self.game.small_town_walls_win,
                            "Small Town Walls.png",
                            [_("May your fountain be blessed, and may you rejoice in the wife of your youth.")],
                            ["Grandpa Kang"]
                        )
                    else:
                        self.game.generate_dialogue_sequence(
                            self.game.small_town_walls_win,
                            "Small Town Walls.png",
                            [_("He who finds a wife finds a good thing...")],
                            ["Grandpa Kang"]
                        )

                elif r == 1:
                    if self.game.taskProgressDic["join_sect"] in [200,500,600]:
                        self.game.generate_dialogue_sequence(
                            self.game.small_town_walls_win,
                            "Small Town Walls.png",
                            [_("You have joined a renowned sect. Many opportunities lie ahead."),
                             _("Make good use of them, young man!")],
                            ["Grandpa Kang", "Grandpa Kang"]
                        )
                    else:
                        rs = pickOne(["Shanhu Sect", "Shaolin", "Wudang"])
                        self.game.generate_dialogue_sequence(
                            self.game.small_town_walls_win,
                            "Small Town Walls.png",
                            [_("{} would be very suitable for you... If only you were fortunate enough to join...").format(rs)],
                            ["Grandpa Kang"]
                        )

                else:
                    rr = pickOne([_("Whoever loves discipline loves knowledge, but he who hates reproof is stupid."),
                                  _("Whoever works his land will have plenty of bread, but he who follows worthless pursuits lacks sense."),
                                  _("The wicked is overthrown through his evildoing, but the righteous finds refuge in his death.")])
                    self.game.generate_dialogue_sequence(
                        self.game.small_town_walls_win,
                        "Small Town Walls.png",
                        [rr],
                        ["Grandpa Kang"]
                    )

        else:
            self.game.small_town_walls_win.deiconify()


    def clicked_on_grandma_kang(self):
        if self.game.taskProgressDic["small_town_ghost"] == 2:
            self.game.generate_dialogue_sequence(
                self.game.small_town_walls_win,
                "Small Town Walls.png",
                [_("Hey there, Grandma Kang. Have you heard anything about a ghost in this town?"),
                 _("Ghost? Oh my, I certainly hope not... Our town has always been peaceful... except for that incident many years ago..."),
                 _("What do you remember about that incident?"),
                 _("It was horrific... Many of the victims frequently bought tea eggs from me, and we had a good relationship..."),
                 _("The old Inn Keeper used to order several dozen eggs from me every day."),
                 _("He was such a kind man, always paying me extra for delivery even though I kept refusing to take it."),
                 _("How terrible that he died, leaving behind his wife and son... Wonder what became of them...")],
                ["you", "Grandma Kang", "you", "Grandma Kang", "Grandma Kang", "Grandma Kang", "Grandma Kang"]
            )
            return

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        self.game.small_town_walls_win.withdraw()
        self.game.generate_dialogue_sequence(
            None,
            "Small Town Walls.png",
            [_("Good morning, young man!"),
             _("How about some boiling hot, freshly made tea eggs? Only 50 gold per bowl!"),],
            ["Grandma Kang", "Grandma Kang"]
        )

        response = messagebox.askquestion("", _("Buy Tea Eggs for 50 gold?"))
        if response == "yes":
            if self.game.check_for_item(_("Gold"), 50):
                print(_("You purchase 'Tea Eggs' for 50 gold."))
                self.game.inv[_("Gold")] -= 50
                self.game.add_item(_("Tea Eggs"))
            else:
                messagebox.showinfo("", _("You don't have enough gold to make this purchase."))

        self.game.small_town_walls_win.deiconify()




    def go_small_town_inn(self):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        self.game.small_town_win.withdraw()

        month = calculate_month_day_year(self.game.gameDate)["Month"]
        if month in [5,6] and self.game.taskProgressDic["small_town_ghost"] == -1:
            self.game.taskProgressDic["small_town_ghost"] = 1
            self.game.generate_dialogue_sequence(
                None,
                "Small Town Inn.png",
                [_("I'm telling you, that's what I saw!"),
                 _("Nonsense! My inn is not haunted! Stop trying to ruin my reputation!"),
                 _("Are you saying that I'm lying? I'm not the only one who saw it. If you don't believe me --"),
                 _("Enough! Stop spreading rumors or you'll ruin my business. I'll return half of your money, now leave!"),
                 _("Unbelievable..."),
                 _("Stingy customers... the things they'll say for money... Oh hi there! Welcome to my humble inn!"),
                 _("Don't listen to those crazy rumors haha... I'll ensure you have a comfortable stay!"),
                 _("........")],
                ["Random1", "Inn Keeper", "Random1", "Inn Keeper", "Random1", "Inn Keeper", "Inn Keeper", "you"]
            )

        self.game.small_town_inn_win = Toplevel(self.game.small_town_win)
        self.game.small_town_inn_win.title(_("Inn"))

        bg = PhotoImage(file="Small Town Inn.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.small_town_inn_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)


        if self.game.taskProgressDic["small_town_ghost"] < 10 and self.game.taskProgressDic["small_town"] not in [10,20,100,200,-1000]:
            self.game.small_town_innF1 = Frame(canvas)
            pic1 = PhotoImage(file="Inn Keeper_icon_large.ppm")
            imageButton1 = Button(self.game.small_town_innF1, command=self.clicked_on_inn_keeper)
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            label = Label(self.game.small_town_innF1, text=_("Inn Keeper"))
            label.grid(row=1, column=0)
            self.game.small_town_innF1.place(x=w // 4 - pic1.width() // 2, y=h // 4 - pic1.height() // 2)

        if self.game.taskProgressDic["small_town"] not in [10,20,100,200,-1000]:
            self.game.small_town_innF2 = Frame(canvas)
            pic2 = PhotoImage(file="Student_icon_large.ppm")
            imageButton2 = Button(self.game.small_town_innF2, command=self.clicked_on_student)
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            label = Label(self.game.small_town_innF2, text=_("Student"))
            label.grid(row=1, column=0)
            self.game.small_town_innF2.place(x=w * 3 // 4 - pic2.width() // 2, y=h // 4 - pic2.height() // 2)

        self.game.small_town_innF3 = Frame(canvas)
        pic3 = PhotoImage(file="small_town_icon_large.ppm")
        imageButton3 = Button(self.game.small_town_innF3,
                              command=partial(self.game.winSwitch, self.game.small_town_inn_win, self.game.small_town_win))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        self.game.small_town_innF3.place(x=w // 4 - pic3.width() // 2, y=h // 2 + pic3.height() // 4)


        menu_frame = self.game.create_menu_frame(self.game.small_town_inn_win)
        menu_frame.pack()
        self.game.small_town_inn_win_F1 = Frame(self.game.small_town_inn_win)
        self.game.small_town_inn_win_F1.pack()

        self.game.small_town_inn_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.small_town_inn_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.small_town_inn_win.winfo_width(), self.game.small_town_inn_win.winfo_height()
        self.game.small_town_inn_win.geometry("+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2,
                                                            5))
        self.game.small_town_inn_win.focus_force()
        self.game.small_town_inn_win.mainloop()


    def clicked_on_inn_keeper(self):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        for widget in self.game.small_town_inn_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.small_town_inn_win_F1, text=_("Talk"),
               command=partial(self.talk_to_inn_keeper, 1)).grid(row=1, column=0)

        Button(self.game.small_town_inn_win_F1, text=_("Stay for the night (100 Gold)"),
               command=partial(self.talk_to_inn_keeper, 2)).grid(row=2, column=0)

        Button(self.game.small_town_inn_win_F1, text=_("Order food"),
               command=self.order_food).grid(row=3, column=0)


    def talk_to_inn_keeper(self, choice):
        if choice == 1:
            if self.game.taskProgressDic["small_town_ghost"] == 2:
                self.game.generate_dialogue_sequence(
                    self.game.small_town_inn_win,
                    "Small Town Inn.png",
                    [_("So you're sure you haven't done anything shady that might have upset anyone?"),
                     _("Of course not! I am an honest businessman, treating all my customers with fairnesss and respect..."),
                     _("I'm telling you, the murderer from 15 years ago is up to no good!"),
                     _("Perhaps he... perhaps he intends to strike again... What shall we do then?"),
                     _("If only I were still a strong, young man like yourself. Then I'd do something about it for sure!")],
                    ["you", "Inn Keeper", "Inn Keeper", "Inn Keeper", "Inn Keeper"]
                )
                return

            r = randrange(3)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.small_town_inn_win,
                    "Small Town Inn.png",
                    [_("Welcome, dear customer!"),
                     _("Whether you're looking to spend the night here or grab a bite to eat, we've got you covered!"),
                     _("I guarantee you won't find another inn like this one within a 200-mile radius!")],
                    ["Inn Keeper", "Inn Keeper", "Inn Keeper"]
                )

            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.small_town_inn_win,
                    "Small Town Inn.png",
                    [_("Try our freshly made dumplings!"),
                     _("Sip on our wine, fermented from top quality grapes, grown on organic soil!")],
                    ["Inn Keeper", "Inn Keeper"]
                )

            elif r == 2:
                self.game.generate_dialogue_sequence(
                    self.game.small_town_inn_win,
                    "Small Town Inn.png",
                    [_("Feeling tired from your travels? Spend the night at our inn for only 100 gold!"),
                     _("We'll provide you with the finest linens and the most comfy pillow!")],
                    ["Inn Keeper", "Inn Keeper"]
                )

        elif choice == 2:
            if not self.game.check_for_item(_("Gold"), 100):
                messagebox.showinfo("", _("Not enough gold."))
            else:
                self.game.gameDate += 1
                self.game.update_date_label()
                month = calculate_month_day_year(self.game.gameDate)["Month"]
                if self.game.taskProgressDic["small_town_ghost"] == 1 and month in [5, 6] and self.game.taskProgressDic["small_town"] == 1:
                    self.game.taskProgressDic["small_town_ghost"] = 2
                    self.game.small_town_inn_win.withdraw()
                    self.game.generate_dialogue_sequence(
                        None,
                        "Small Town Inn Night.png",
                        [_("Ahhhh... Finally, I can get some rest..."),
                         _("The pillow here is quite comfy, as advertised..."),
                         _("(Zzzzzzzz..... Zzzzzzz..... Zzzzzz.....)")],
                        ["you", "you", "you"]
                    )

                    self.game.currentEffect = "male_scream_scared2.ogg"
                    self.game.startSoundEffectThread()
                    time.sleep(1)
                    self.game.stopSoundtrack()
                    self.game.currentBGM = "bqt_mystery.mp3"
                    self.game.startSoundtrackThread()

                    self.game.generate_dialogue_sequence(
                        self.game.small_town_inn_win,
                        "Small Town Inn Night.png",
                        [_("??!!"),
                         _("HELP!!! GHOST!!! AHHHHHHH!!!"),
                         _("What happened?!"),
                         _("G-ghost... I... I saw it again..."),
                         _("What did you see???"),
                         _("D-dark figure... l-long hair... lifeless eyes..."),
                         _("Where did it go?"),
                         _("I... I think it went towards... the woods..."),
                         _("What is all this noise about? You're gonna wake up all of my guests!"), #Row 2
                         _("Sir... I saw... I saw the ghost again..."),
                         _("Shhh!!! Didn't I tell you not to say that anymore?"),
                         _("......"),
                         _("It must be that cursed murderer..."),
                         _("As if killing all those innocent people wasn't enough, now he's trying to ruin my business!"),
                         _("Just wait till I get my hands on him... I'll make him regret it, that I will..."),
                         _("How long has this been going on?"), #Row 3
                         _("*Sigh* Don't tell anyone, but it all started about 5 years ago..."),
                         _("The thing is, this only happens in May and June, the same months that the rampage took place 15 years ago."),
                         _("I don't know what it is about those 2 months that turns him crazy, but he's up to no good again..."),
                         _("What makes you so sure it's him?"),
                         _("Who else could it be? I don't have any enemies in this town..."),
                         _("(Hmmm, I'd better talk to people in this town to find out more...)"),
                         _("*Next morning*")],
                        ["you", "Student", "you", "Student", "you", "Student", "you", "Student",
                         "Inn Keeper", "Student", "Inn Keeper", "you", "Inn Keeper", "Inn Keeper", "Inn Keeper",
                         "you", "Inn Keeper", "Inn Keeper", "Inn Keeper", "you", "Inn Keeper", "you", "Blank"]
                    )

                elif self.game.taskProgressDic["small_town_ghost"] == 2 and month in [5, 6]:
                    self.game.taskProgressDic["small_town_ghost"] = 3
                    self.game.small_town_inn_win.withdraw()
                    self.game.generate_dialogue_sequence(
                        None,
                        "Small Town Inn Night.png",
                        [_("(Ok, gonna be ready tonight...)"),
                         _("Ahhhh... soooo tired... gotta get a good night's sleep..... Zzzzz..."),
                         _("2 hours later.")],
                        ["you", "you", "Blank"]
                    )

                    self.game.currentEffect = "male_scream_scared2.ogg"
                    self.game.startSoundEffectThread()
                    time.sleep(1)
                    self.game.stopSoundtrack()
                    self.game.currentBGM = "bqt_mystery.mp3"
                    self.game.startSoundtrackThread()

                    self.game.generate_dialogue_sequence(
                        None,
                        "Small Town Inn Night.png",
                        [_("(There it is again!)"),
                         _("AHHHHHHH!!!"),
                         _("Hey! Are you ok?!"),
                         _("*A shadowy figure with long hair dashes out of the window towards the woods.*"),
                         _("I'm not gonna let you get away this time..."),
                         _("W-wait! What are you doing? You can't go into the forest at night! It's too dangerous!"),
                         _("I'm not scared of that thing. Here, just give me a torch."),
                         _("*You grab a torch and run into the forest after the dark figure.*")],
                        ["you", "Student", "you", "Blank", "you", "Student", "you", "Blank"]
                    )

                    self.game.mini_game_in_progress = True
                    self.game.mini_game = small_town_forest_mini_game(self.game, self)
                    self.game.mini_game.new()

                else:
                    self.game.restore(0,0,full=True)
                    print(_("Your health and stamina are restored after a good night's sleep."))



    def order_food(self):

        self.inn_food_prices = {
            _("Soup"): 35,
            _("Dumplings"): 30,
            _("Wine"): 40
        }
        self.selected_shop_item = None
        self.game.small_town_inn_win.withdraw()
        self.small_town_inn_menu_win = Toplevel(self.game.small_town_inn_win)

        F1 = Frame(self.small_town_inn_menu_win, borderwidth=2, relief=SUNKEN, padx=5, pady=5)
        F1.pack()

        self.shop_item_buttons = []
        self.shop_item_icons = []
        self.item_price_labels = []
        self.selected_shop_item = None
        self.selected_shop_item_index = None

        item_x = 0
        item_y = 0
        item_index = 0
        for item_name in self.inn_food_prices:
            item_icon = PhotoImage(file=_("i_{}.ppm").format(item_name))
            item_button = Button(F1, image=item_icon, command=partial(self.selectShopItem, item_name, item_index))
            item_button.image = item_icon
            item_button.grid(row=item_y, column=item_x)

            item_description = item_name + "\n------------------------------\n" + ITEM_DESCRIPTIONS[item_name]
            self.game.balloon.bind(item_button, item_description)
            self.shop_item_icons.append(item_icon)
            self.shop_item_buttons.append(item_button)

            price_label = Label(text="Price:" + str(self.inn_food_prices[item_name]), master=F1)
            price_label.grid(row=item_y + 1, column=item_x)
            self.item_price_labels.append(price_label)

            item_x += 1
            item_index += 1


        F2 = Frame(self.small_town_inn_menu_win, padx=5, pady=5)
        F2.pack()

        Button(F2, text="Back", command=partial(self.game.winSwitch, self.small_town_inn_menu_win, self.game.small_town_inn_win)).grid(row=0, column=0)
        Label(F2, text = "  ").grid(row=0, column=1)
        Button(F2, text="Buy", command=partial(self.buy_selected_shop_item, self.inn_food_prices)).grid(row=0, column=2)
        Label(F2, text="  ").grid(row=0, column=3)

        self.shop_gold_amount_label = Label(F2, text=_("Your gold: ")+ str(self.game.inv[_("Gold")]))
        self.shop_gold_amount_label.grid(row=0,column=4)


        self.small_town_inn_menu_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.small_town_inn_menu_win.update_idletasks()
        toplevel_w, toplevel_h = self.small_town_inn_menu_win.winfo_width(), self.small_town_inn_menu_win.winfo_height()
        self.small_town_inn_menu_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.small_town_inn_menu_win.focus_force()
        self.small_town_inn_menu_win.mainloop()###


    def selectShopItem(self, item_name, item_index):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if self.selected_shop_item:
            self.shop_item_icons[self.selected_shop_item_index] = PhotoImage(file=_("i_{}.ppm").format(self.selected_shop_item))
            self.shop_item_buttons[self.selected_shop_item_index].config(image=self.shop_item_icons[self.selected_shop_item_index])

        self.selected_shop_item = item_name
        self.selected_shop_item_index = item_index

        self.shop_item_icons[self.selected_shop_item_index] = PhotoImage(file=_("i_{}_selected.ppm").format(self.selected_shop_item))
        self.shop_item_buttons[self.selected_shop_item_index].config(image=self.shop_item_icons[self.selected_shop_item_index])


    def buy_selected_shop_item(self, price_dic):

        if not self.selected_shop_item:
            messagebox.showinfo("", _("No item selected."))
            return

        if self.game.inv[_("Gold")]< price_dic[self.selected_shop_item]:
            messagebox.showinfo("", _("Not enough gold."))
            return

        else:
            self.game.currentEffect = "button-19.mp3"
            self.game.startSoundEffectThread()
            self.game.add_item(self.selected_shop_item)
            self.game.inv[_("Gold")] -= price_dic[self.selected_shop_item]
            self.shop_gold_amount_label.config(text=_("Your gold: ")+ str(self.game.inv[_("Gold")]))
            print(_("Purchased {}.").format(self.selected_shop_item))


    def clicked_on_student(self):

        if self.game.taskProgressDic["small_town_ghost"] == 1:
            self.game.generate_dialogue_sequence(
                self.game.small_town_inn_win,
                "Small Town Inn.png",
                [_("Hey, have you noticed anything strange happening at night?"),
                 _("H-how did you know?"),
                 _("Never mind that; just tell me..."),
                 _("Well, during the 3 years that I've worked here, strange things have happened in the months of May and June."),
                 _("I myself have seen a dark figure a few times. Other customers have also reported similar sightings."),
                 _("Some say that it's the restless ghosts of the people who were murdered in this town..."),
                 _("Others say that it's the murderer himself who's up to no good. As for me, I think--"),
                 _("Oy! Quit idling! You want me to deduct your pay?"),
                 _("Ah, no, sorry boss! I'll get back to work right away!")],
                ["you", "Student", "you", "Student", "Student", "Student", "Student", "Inn Keeper", "Student"]
            )

        elif self.game.taskProgressDic["small_town_ghost"] == 2:
                self.game.generate_dialogue_sequence(
                    self.game.small_town_inn_win,
                    "Small Town Inn.png",
                    [_("Can you describe what happened again?"),
                     _("Well, that night, I finished wiping all the tables and setting the chairs up."),
                     _("Then I headed to my room as usual to do some reading before going to sleep."),
                     _("Just as my eyes were starting to get heavy, the window burst open, and a gust of wind extinguished my lamp."),
                     _("I looked out and there it was! The ghost!"),
                     _("Under the moonlight, I could see its long, dark hair and pale face..."),
                     _("That's when I screamed, and shortly after, you and the boss came to my room."),
                     _("Are you sure it was a ghost and not a person?"),
                     _("Well, not really... but what kind of person would look so creepy like that?"),
                     _("Hmmm, ok thanks...")],
                    ["you", "Student", "Student", "Student","Student","Student","Student","you","Student","you"]
                )
                return

        elif self.game.taskProgressDic["small_town_ghost"] >= 10:
            self.game.generate_dialogue_sequence(
                self.game.small_town_inn_win,
                "Small Town Inn.png",
                [_("How strange... my boss died mysteriously during the night..."),
                 _("The coroner said the cause of death was poison."),
                 _("Hmmm... yeah... strange indeed..."),
                 _("*sigh* ... I might have to find a job elsewhere soon...")],
                ["Student", "Student", "you", "Student"]
            )

        else:
            self.game.generate_dialogue_sequence(
                self.game.small_town_inn_win,
                "Small Town Inn.png",
                [_("Who are you and what are you doing here?"),
                 _("Oh hey there, I'm just a student."),
                 _("I've been working here as a server for 3 years to save up some money so I can travel to the Capital for the nation-wide exam."),
                 _("Ah, so you want to work for the government..."),
                 _("Yes, that is my hope.")],
                ["you", "Student", "Student", "you", "Student"]
            )



    def clicked_on_lin_anning(self):

        if self.game.taskProgressDic["small_town_ghost"] == 2:
            self.game.generate_dialogue_sequence(
                self.game.small_town_win,
                "Small Town.png",
                [_("What do you know about the history of this inn?"),
                 _("The original inn keeper was one of the victims from 15 years ago."),
                 _("After his death, the current inn keeper bought the place from his widow."),
                 _("The widow and her son left the town soon afterwards."),
                 _("No one knows where they went or why they left though..."),
                 _("Thanks... Do you know anyone who might have something against the inn keeper?"),
                 _("Well, he's known for being greedy, but no one in the town hates him as far as I'm aware.")],
                ["you", "Lin Anning", "Lin Anning", "Lin Anning", "Lin Anning", "you", "Lin Anning"]
            )
            return


        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        for widget in self.game.small_town_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.small_town_win_F1, text=_("Talk"),
               command=partial(self.talk_to_lin_anning, 1)).grid(row=1, column=0)

        Button(self.game.small_town_win_F1, text=_("Anything fun to do around here?"),
               command=partial(self.talk_to_lin_anning, 2)).grid(row=2, column=0)


    def talk_to_lin_anning(self, choice):
        if choice == 1:
            if randrange(2) == 0:
                self.game.generate_dialogue_sequence(
                    self.game.small_town_win,
                    "Small Town.png",
                    [_("Have a look around the town and meet some people!"),
                     _("You can spend the night at the inn if you wish to stay longer..."),
                     _("But I've heard that in the recent few years, strange things happen at the inn during the night in May and June."),
                     _("Only those 2 months?"),
                     _("Yeah, I'm not sure why... I haven't actually seen anything myself though, so it could just be a myth.")],
                    ["Lin Anning", "Lin Anning", "Lin Anning", "you", "Lin Anning"]
                )
            else:
                self.game.generate_dialogue_sequence(
                    self.game.small_town_win,
                    "Small Town.png",
                    [_("My husband, Qin Long, is a First Lieutenant and maintains law and order around the area."),
                     _("If you ever see anyone causing trouble, be sure to tell him!")],
                    ["Lin Anning", "Lin Anning"]
                )

        elif choice == 2:
            if self.game.check_for_item(_("Zither")):
                self.game.generate_dialogue_sequence(
                    self.game.small_town_win,
                    "Small Town.png",
                    [_("Since you have an instrument, let's play together!")],
                    ["Lin Anning"]
                )
                messagebox.showinfo("", "The soothing music helps you relax...")
                self.game.restore(self.game.healthMax // 5, self.game.staminaMax // 5, full=False)

            else:
                self.game.generate_dialogue_sequence(
                    self.game.small_town_win,
                    "Small Town.png",
                    [_("You mean besides meeting new people and seeing what they have to offer?"),
                     _("Yeah..."),
                     _("Hmmm, if you want, I can teach you how to play the zither. I'm a musician."),
                     _("I play for special guests at the inn and also during special events."),
                     _("How much do I have to pay?"),
                     _("Well, I'm only going to teach you the basics, so it'll be free."),
                     _("However, you'll need a zither to learn. I can sell you one for 300 gold.")],
                    ["Lin Anning", "you", "Lin Anning", "Lin Anning", "you", "Lin Anning", "Lin Anning"],
                    [[_("Buy zither for 300 gold."), partial(self.talk_to_lin_anning, 21)],
                     [_("Not now."), partial(self.talk_to_lin_anning, 22)]]
                )

        elif choice == 21:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            if self.game.check_for_item(_("Gold"), 300):
                self.game.add_item(_("Gold"), -300)
                self.game.add_item(_("Zither"))
                messagebox.showinfo("", _("You purchase a zither for 300 gold."))
            else:
                messagebox.showinfo("", _("You don't have enough gold."))

            self.game.small_town_win.deiconify()

        elif choice == 22:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.small_town_win.deiconify()


    def post_forest_mini_game(self, stage=1):
        if stage == 1:
            self.game.mini_game_in_progress = False
            pg.display.quit()
            self.game.generate_dialogue_sequence(
                None,
                "Hut In Woods.png",
                [_("(Hmmm, there's a hut in the middle of the woods. Looks like someone has been living here for a long time...)"),
                 _("You just had to follow me here, didn't you?"),
                 _("WHOA!! ... What are -- I mean, who are you?"),
                 _("You don't need to know. You'll be dead soon.")],
                ["you", "Strange Man", "you", "Strange Man"]
            )

            opp = character(_("Strange Man"), 13,
                            sml=[special_move(_("Serpent Whip"), level=10),
                                 special_move(_("Poison Needle"), level=10)],
                            skills=[skill(_("Phantom Steps"), level=5),
                                    skill(_("Toad Form"), level=10)])

            cmd = lambda: self.post_forest_mini_game(2)
            self.game.battleMenu(self.game.you, opp, "Hut In Woods.png",
                                 "bqt_investigate.mp3", fromWin=None, battleType="task",
                                 destinationWinList=[None]*3, postBattleCmd=cmd)

        elif stage == 2:
            self.game.taskProgressDic["small_town_ghost"] = 4
            self.game.generate_dialogue_sequence(
                None,
                "Hut In Woods.png",
                [_("You don't even live in this town. Why do you involve yourself in other people's business?"),
                 _("Well, you disturbed my sleep 2 nights in a row... Why do you keep haunting that inn?"),
                 _("I know the inn keeper is not the most likeable person but..."),
                 _("HE IS NOT THE INN KEEPER!!!"),
                 _("..."),
                 _("The real inn keeper is my dad... not that greedy piece of garbage..."),
                 _("Your dad... was he a victim of the town massacre?")],
                ["Strange Man", "you", "you", "Strange Man", "you", "Strange Man", "you"]
            )

            self.game.stopSoundtrack()
            self.game.currentBGM = "three_people.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Hut In Woods.png",
                [_("I was still a young boy at the time..."),
                 _("What happened after that? How did you end up living here in the woods?"),
                 _("After my dad was murdered, the current owner of the inn, who was a business man at the time"),
                 _("Tricked my mother into putting her fingerprints on a contract that transferred ownership of the inn to him."),
                 _("He took advantage of the fact that my mother was illiterate and lied to her, claiming the contract would allow him to support our inn by providing foods at a low cost."),
                 _("She didn't think someone would take advantage of us right after we lost my father and much of the town was still grieving."),
                 _("My mother, to escape all the injustice and pain we had experienced, left the town with me, a 10-year-old child."),
                 _("We wandered through this forest, hoping to find another town on the other side of the forest."), #Row 2
                 _("We did not go far before coming upon this small hut. It was much smaller at the time we found it but still enough for 2 people."),
                 _("We decided to settle here. It worked out pretty well in my opinion since I could easily return to the town and avenge my father."),
                 _("By chance, I found some words carved into the walls and began to study them."),
                 _("It turns out, they were excerpts from a martial arts manual. It's as if the Heavens took pity on me and gave me all I needed for revenge."),
                 _("However, not all was well... My mother's health soon began to fail in this harsh environment, and just 3 years after we left the town, she died."),
                 _("This further fueled my desire for vengeance, and I practiced even harder."), #Row 3
                 _("Then one day, while chasing a deer when hunting, I slipped and rolled down a slope into a pit of venomous snakes and frogs."),
                 _("I was bitten and paralyzed for 3 days. I thought for sure that I was going to die, but who knew?"),
                 _("The venom from the snakes and frogs counteracted each other, and I not only survived but became immune to their venom."),
                 _("During the 3 days where I could not move, I observed the snakes' and frogs' movements and the way the fought each other."),
                 _("This gave me inspiration to create my own techniques: Toad Form and Serpent Whip."),
                 _("Ohhhh, so that's where your strange moves came from..."), #Row 4
                 _("I finally became strong enough where I stood a chance against the man who killed my father."),
                 _("But I wasn't sure whether he's grown stronger over the years, so I came up with a plan to pit the townspeople against the murderer."),
                 _("I began to haunt the inn every May and June, and did my best to make the murderer my scapegoat."),
                 _("I hoped that enough people would rise up against the murderer. Once that happens, I can join and increase my chances of success."),
                 _("At the same time, this serves as slow torture for the greedy inn keeper."),
                 _("Once I get tired of playing with him, I can end his life anytime..."), #Row 5
                 _("That's it. This is the truth behind the inn hauntings and the hidden secret from many years ago."),
                 _("Now that you've heard my story, surely you will not stop me from getting revenge!"),
                 _("Please, let me kill the greedy inn keeper and the heartless murderer!"),
                 _("I will teach you the Toad Form and Serpent Whip in return!")],
                ["Strange Man", "you", "Strange Man", "Strange Man", "Strange Man", "Strange Man", "Strange Man",
                 "Strange Man", "Strange Man", "Strange Man", "Strange Man", "Strange Man", "Strange Man",
                 "Strange Man", "Strange Man", "Strange Man", "Strange Man", "Strange Man", "Strange Man",
                 "you", "Strange Man", "Strange Man", "Strange Man", "Strange Man", "Strange Man",
                 "Strange Man", "Strange Man", "Strange Man", "Strange Man", "Strange Man"],
                [[_("Do as you wish."), partial(self.post_forest_mini_game, 10)],
                 [_("Go ahead, but you must spare the murderer."), partial(self.post_forest_mini_game, 20)]]
            )

        elif stage == 10:
            self.game.taskProgressDic["small_town_ghost"] = 10
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()

            new_move = special_move(name=_("Serpent Whip"))
            self.game.learn_move(new_move)
            new_skill = skill(name=_("Toad Form"))
            self.game.learn_skill(new_skill)

            self.game.generate_dialogue_sequence(
                None,
                "Hut In Woods.png",
                [_("Thank you! I am forever indebted to you!"),
                 _("*The old inn keeper's son teaches you 'Serpent Whip' and 'Toad Form'.*"),
                 _("*After a night's rest, you return to the town.*")],
                ["Strange Man", "Blank", "Blank"]
            )

            self.game.small_town_inn_win.quit()
            self.game.small_town_inn_win.destroy()
            self.game.small_town_win.deiconify()


        elif stage == 20:
            self.game.taskProgressDic["small_town_ghost"] = 20
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()

            new_move = special_move(name=_("Serpent Whip"))
            self.game.learn_move(new_move)
            new_skill = skill(name=_("Toad Form"))
            self.game.learn_skill(new_skill)

            self.game.generate_dialogue_sequence(
                None,
                "Hut In Woods.png",
                [_("But... why???"),
                 _("He's been living in remorse for his sins for the past 15 years. He is no longer the same man that murdered your father."),
                 _("Even if you kill him now, it's meaningless. Trust me..."),
                 _("I..."),
                 _("If he hasn't changed, then you wouldn't be able to kill him anyway."),
                 _("Let go, and live your life free from hatred and bitterness..."),
                 _("You could easily turn me in to the officials, but instead you are letting me go. I have no reason not to trust you."),
                 _("Very well. You have my word. I will only kill the current inn keeper."),
                 _("*The old inn keeper's son teaches you 'Serpent Whip' and 'Toad Form'.*"),
                 _("*After a night's rest, you return to the town.*")],
                ["Strange Man", "you", "you", "Strange Man", "you", "you", "Strange Man", "Strange Man", "Blank", "Blank"]
            )

            self.game.small_town_inn_win.quit()
            self.game.small_town_inn_win.destroy()
            self.game.small_town_win.deiconify()


    def town_treason(self, stage):
        if stage == 0:
            self.game.chivalry -= 30
            messagebox.showinfo("", _("Your chivalry -30!"))
            self.game.taskProgressDic["small_town"] = -1000
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.mapWin.deiconify()

        elif stage == 1:
            self.game.chivalry += 30
            messagebox.showinfo("", _("Your chivalry +30!"))
            self.game.taskProgressDic["small_town"] = 2
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.generate_dialogue_sequence(
                None,
                "Small Town.png",
                [_("Oi! I heard you're looking for lowly peasants. Here's one."),
                 _("You again! How convenient; I don't even have you hunt you down."),
                 _("Did you forget how easily I defeated you last time?"),
                 _("Silence! Archers, get him!")],
                ["you", "Feng Pan", "you", "Feng Pan"],
            )

            opp = character(_("Archers"), 15,
                            attributeList=[20000, 20000, 500, 500, 150, 250, 300, 20, 10],
                            sml=[special_move(_("Archery"), level=10)]
                            )

            cmd = lambda: self.town_treason(2)
            self.game.battleMenu(self.game.you, opp, "Small Town.png",
                                 "jy_tianwaicun.mp3", fromWin=None, battleType="task",
                                 destinationWinList=[None] * 3, postBattleCmd=cmd)


        elif stage == 2:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.restore(0,0,full=True)
            self.game.generate_dialogue_sequence(
                None,
                "Small Town.png",
                [_("Thank you, {}! We owe you our lives!").format(self.game.character),
                 _("Oh, young man, thank you so much! I don't know how to repay you..."),
                 _("Don't mention it; just doing what is right... By the way, where did Feng Pan go?"),
                 _("He ran away while you were fighting off the archers..."),
                 _("Which means... He'll probably return soon with a bigger army. We need to evacuate all the townspeople quickly."),
                 _("Qin Long, you round up the villagers and help with the evacuation."),
                 _("What about you?"),
                 _("I'm going to wait for Feng Pan to return and hold him off for as long as I can to buy you guys time to escape."), #Row 2
                 _("That's too dangerous. Let me go instead."),
                 _("Come on man, now's not the time for that. The townspeople know you and you know them."),
                 _("It's much more effective if you organize the evacuation."),
                 _("{}... I... Thank you for doing this for us. Please be careful.").format(self.game.character),
                 _("You too. Take care."),
                 _("*You bid the townspeople farewell and head out to the road to wait for Feng Pan.*"),
                 _("*1 hour later, you spot Feng Pan returning with what looks like thousands of soldiers.*")],
                ["Qin Long", "Grandma Kang", "you", "Lin Anning", "Qin Long", "you", "Qin Long",
                 "you", "Qin Long", "you", "you", "Qin Long", "you", "Blank", "Blank"],
            )

            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_daojiangu.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Small Town.png",
                [_("Oh look at that, went to get more men, I see... I must be way too strong for you..."),
                 _("We'll see who gets the last laugh..."),
                 _("You're gonna have so many arrows sticking out from your body that your own mother won't recognize you."),
                 _("Is that so? Come on then; turn me into a porcupine!"),
                 _("You seem quite confident... I know you've been able to get away in the past, which is why I brought special equipment this time."),
                 _("Men! Surround him and set up the barricades! Fire! Fire until every inch of his body is covered in arrows!")],
                ["you", "Feng Pan", "Feng Pan", "you", "Feng Pan", "Feng Pan"],
            )

            self.game.mini_game_in_progress = True
            self.game.mini_game = small_town_archer_battle_mini_game(self.game, self)
            self.game.mini_game.new()

        elif stage == 3:
            self.game.mini_game_in_progress = False
            pg.display.quit()
            self.game.generate_dialogue_sequence(
                None,
                "forest.ppm",
                [_("*You flee into the woods...*"),
                 _("(I hope that was...enough time...)"),
                 _("*You start to feel a bit dizzy as you realize you've lost too much blood...*"),
                 _("*You manage to stumble a few more steps before collapsing on the ground.*"),
                 _("*An unknown amount of time later...*")],
                ["Blank", "you", "Blank", "Blank", "Blank"],
            )

            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_mountain_scenery.mp3"
            self.game.startSoundtrackThread()
            self.game.update_map_location("snowy_mountain")
            self.game.update_date_label()
            self.game.restore(0,0,full=True)

            self.game.generate_dialogue_sequence(
                None,
                "Snowy Mountain.png",
                [_("How do you feel now?"),
                 _("I... How did I end up here?"),
                 _("I was on my way to visit the small town at the base of the mountain and saw you passed out on the ground."),
                 _("So I took you back here and took care of your wounds."),
                 _("The townspeople! ... Thank you for saving me, but I need to go now."),
                 _("(I'd better go back to the town and see what happened...)")],
                ["Xiao Yong", "you", "Xiao Yong", "Xiao Yong", "you", "you"],
            )

            self.game.mapWin.deiconify()