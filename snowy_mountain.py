from tkinter import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
from mini_game_snow_lotus import*
import tkinter.messagebox as messagebox

_ = gettext

class Scene_snowy_mountain:
    def __init__(self, game):
        self.game = game
        self.game.snowy_mountain_win = Toplevel(self.game.mapWin)
        self.game.snowy_mountain_win.title(_("Snowy Mountain"))


        bg = PhotoImage(file="Snowy Mountain.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.snowy_mountain_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.game.snowy_mountainF1 = Frame(canvas)
        pic1 = PhotoImage(file="Xiao Yong_icon_large.ppm")
        imageButton1 = Button(self.game.snowy_mountainF1, command=self.clickedOnXiaoHan)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.game.snowy_mountainF1, text=_("Xiao Yong"))
        label.grid(row=1, column=0)
        self.game.snowy_mountainF1.place(x=w // 4 - pic1.width() // 2, y=h // 4 - pic1.height()//2)

        menu_frame = self.game.create_menu_frame(self.game.snowy_mountain_win)
        menu_frame.pack()
        self.game.snowy_mountain_win_F1 = Frame(self.game.snowy_mountain_win)
        self.game.snowy_mountain_win_F1.pack()

        self.game.snowy_mountain_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.snowy_mountain_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.snowy_mountain_win.winfo_width(), self.game.snowy_mountain_win.winfo_height()
        self.game.snowy_mountain_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.snowy_mountain_win.focus_force()
        self.game.snowy_mountain_win.mainloop()###


    def clickedOnXiaoHan(self):
        if self.game.chivalry < 0 and self.game.taskProgressDic['xiao_han_jia_wen'] not in list(range(11,20)):
            messagebox.showinfo("", _("Xiao Yong does not seem interested in talking to you..."))
        elif self.game.taskProgressDic["supreme_leader"] == 1:
            messagebox.showinfo("", _("Xiao Yong does not seem interested in talking to you..."))
        elif self.game.taskProgressDic['xiao_han_jia_wen'] == 15 and self.game.inv[_("Gold Necklace")] > 0:
            self.talk_to_xiao_han("Gold Necklace")
        else:
            self.game.currentEffect = "button-20.mp3"
            self.game.startSoundEffectThread()

            for widget in self.game.snowy_mountain_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.snowy_mountain_win_F1, text=_("Spar"),
                   command=partial(self.snowy_mountain_spar, targ=_("Xiao Yong"))).grid(row=1, column=0)
            Button(self.game.snowy_mountain_win_F1, text=_("What are you doing here?"),
                   command=partial(self.talk_to_xiao_han, 1)).grid(row=2, column=0)
            if self.game.taskProgressDic['xiao_han_jia_wen'] == 11:
                Button(self.game.snowy_mountain_win_F1, text=_("Do you know where I can find Snow Lotus?"),
                       command=partial(self.talk_to_xiao_han, "Snow Lotus")).grid(row=3, column=0)
            if _("Zither") in self.game.inv:
                Button(self.game.snowy_mountain_win_F1, text=_("Let's have a duet!"),
                       command=partial(self.talk_to_xiao_han, "Duet")).grid(row=4, column=0)
            if self.game.taskProgressDic['xiao_han_jia_wen'] in list(range(12,20)):
                Button(self.game.snowy_mountain_win_F1, text=_("I want to look for more snow lotus!"),
                       command=partial(self.talk_to_xiao_han, "Snow Lotus Repeat")).grid(row=10, column=0)


    def snowy_mountain_spar(self, targ):

        if targ == "Xiao Yong":
            if self.game.chivalry < 0:
                self.game.generate_dialogue_sequence(
                    self.game.snowy_mountain_win,
                    "Snowy Mountain.png",
                    [_("Sorry, I don't feel like it right now."),
                     _("Perhaps another time...")],
                    ["Xiao Yong", "Xiao Yong"]
                )

            else:
                self.game.generate_dialogue_sequence(
                    self.game.snowy_mountain_win,
                    "Snowy Mountain.png",
                    [_("I'm down! Let's do it!")],
                    ["Xiao Yong"]
                )
                year = calculate_month_day_year(self.game.gameDate)["Year"]
                opp = character(_("Xiao Yong"), min([18+year, 21]),
                                sml=[special_move(_("Shanhu Fist"), level=10),
                                     special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Demon Suppressing Blade"), level=8),
                                     special_move(_("Ice Blade"), level=10),
                                     special_move(_("Ice Palm"), level=10),
                                     special_move(_("Soothing Song"))],
                                skills=[skill(_("Divine Protection"), level=8),
                                        skill(_("Leaping Over Mountain Peaks"), level=10)])

                self.game.battleMenu(self.game.you, opp, "battleground_snowy_mountain.png",
                                     "jy_mountain_scenery.mp3", fromWin=self.game.snowy_mountain_win,
                                     battleType="training", destinationWinList=[self.game.snowy_mountain_win] * 3)



    def talk_to_xiao_han(self, choice):
        if choice == "Snow Lotus":
            self.game.generate_dialogue_sequence(
                self.game.snowy_mountain_win,
                "Snowy Mountain.png",
                [_("Why did you travel all this way to look for the Snow Lotus?"),
                 _("It's a long story... I passed by the Bamboo Forest and saw a woman in white and a little girl who was dying..."),
                 _("The woman told me that she needed the Snow Lotus Pill to save the girl..."),
                 _("Wait, Bamboo Forest... Woman in white... Was her n---"),
                 _("I don't have time for that. The little girl's dying! Help me get the Snow Lotus first; then we'll talk."),
                 _("Right... Sorry... Here, if you go down this path and then head West, you should be able to make your way up the mountain."),
                 _("Near the top of the mountain is where Snow Lotus naturally grows."),
                 _("It's a dangerous task, though, as you can easily slip and fall to your death."),
                 _("No worries, I am not afraid. Thanks for your help.")],
                ["Xiao Yong", "you", "you", "Xiao Yong", "you", "Xiao Yong", "Xiao Yong", "Xiao Yong", "you"]
            )

            self.game.snowy_mountain_win.withdraw()
            self.game.mini_game_in_progress = True
            self.game.mini_game = snow_lotus_mini_game(self.game, self)
            self.game.mini_game.new()


        elif choice == "Duet":
            self.game.generate_dialogue_sequence(
                self.game.snowy_mountain_win,
                "Snowy Mountain.png",
                [_("What a breath-taking view we've got up here!"),
                 _("Why not take this opportunity to have a duet? I heard your flute skills are amazing!"),
                 _("Great! I never turn down a chance to play some music!")
                 ],
                ["you", "you", "Xiao Yong"]
            )

            if random() <= .25 and _("Soothing Song") not in [m.name for m in self.game.sml]:
                new_move = special_move(name=_("Soothing Song"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", "You learned a new move: 'Soothing Song'!")
            else:
                messagebox.showinfo("", "The soothing music helps you relax...")
                self.game.restore(self.game.healthMax//3, self.game.staminaMax//3,full=False)


        elif choice == "Snow Lotus Repeat":
            self.game.generate_dialogue_sequence(
                self.game.snowy_mountain_win,
                "Snowy Mountain.png",
                [_("Go ahead; just watch your step up there, ok?")],
                ["Xiao Yong"]
            )

            self.game.snowy_mountain_win.withdraw()
            self.game.mini_game_in_progress = True
            self.game.mini_game = snow_lotus_mini_game(self.game, self)
            self.game.mini_game.new()


        elif choice == "Gold Necklace":
            self.game.snowy_mountain_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Snowy Mountain.png",
                [_("I gave your bamboo flute to Jia Wen. She told me to bring this to you.")],
                ["you"]
            )
            self.game.inv[_("Gold Necklace")] = 0
            messagebox.showinfo("", _("You give the Gold Necklace to Xiao Yong."))

            total = self.game.chivalry + self.game.perception + self.game.luck
            if total >= 270:
                self.game.generate_dialogue_sequence(
                    self.game.snowy_mountain_win,
                    "Snowy Mountain.png",
                    [_("Thank you, {}, I really owe you one!").format(self.game.character),
                     _("Though I am unskilled, I've invented 2 techniques after spending much time here on this mountain."),
                     _("I think they could come in handy for you to learn if you are interested."),
                     _("Of course! I'd love to learn!"),
                     _("Great! Here, let me teach you...")],
                    ["Xiao Yong","Xiao Yong","Xiao Yong","you","Xiao Yong"]
                )
                new_move = special_move(name=_("Ice Palm"))
                self.game.learn_move(new_move)
                new_move = special_move(name=_("Ice Blade"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("You learned 2 moves: Ice Palm & Ice Blade!"))

            elif total >= 240:
                self.game.generate_dialogue_sequence(
                    self.game.snowy_mountain_win,
                    "Snowy Mountain.png",
                    [_("Thank you, {}, I really owe you one!").format(self.game.character),
                     _("Though I am unskilled, I've invented a technique after spending much time here on this mountain."),
                     _("I think it could come in handy for you to learn if you are interested."),
                     _("Of course! I'd love to learn!"),
                     _("Great! Here, let me teach you...")],
                    ["Xiao Yong","Xiao Yong","Xiao Yong","you","Xiao Yong"]
                )
                new_move = special_move(name=_("Ice Blade"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("You learned a new move: Ice Blade!"))


            elif total >= 210:
                self.game.generate_dialogue_sequence(
                    self.game.snowy_mountain_win,
                    "Snowy Mountain.png",
                    [_("Thank you, {}, I really owe you one!").format(self.game.character),
                     _("Though I am unskilled, I've invented a technique after spending much time here on this mountain."),
                     _("I think it could come in handy for you to learn if you are interested."),
                     _("Of course! I'd love to learn!"),
                     _("Great! Here, let me teach you...")],
                    ["Xiao Yong","Xiao Yong","Xiao Yong","you","Xiao Yong"]
                )
                new_move = special_move(name=_("Ice Palm"))
                self.game.learn_move(new_move)
                messagebox.showinfo("", _("You learned a new move: Ice Palm!"))

            else:
                self.game.generate_dialogue_sequence(
                    self.game.snowy_mountain_win,
                    "Snowy Mountain.png",
                    [_("Thank you, {}, I really owe you one!").format(self.game.character),
                     _("Here's a small token of appreciation; please accept it.")],
                    ["Xiao Yong","Xiao Yong"]
                )
                self.game.inv[_("Gold")] += 3000
                messagebox.showinfo("", _("Received 'Gold' x 3000!"))

            self.game.taskProgressDic['xiao_han_jia_wen'] = 16


        elif choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.snowy_mountain_win,
                "Snowy Mountain.png",
                [_("I'm fulfilling a commitment that I made."),
                 _("(Interesting...)")],
                ["Xiao Yong", "you"]
            )


    def found_snow_lotus(self):
        self.game.mini_game_in_progress = False
        pg.display.quit()
        if os.name == "nt":
            self.game.mainWin.tk.call('wm', 'iconphoto', self.game.mainWin._w, PhotoImage(file=r'game_main_icon.png'))
        else:
            self.game.mainWin.tk.call('wm', 'iconphoto', self.game.mainWin._w, PhotoImage(file=r'game_main_icon.png'))

        if _("Snow Lotus") in self.game.inv:
            self.game.inv[_("Snow Lotus")] += 1
            self.game.snowy_mountain_win.deiconify()
        else:
            self.game.inv[_("Snow Lotus")] = 1
            self.game.taskProgressDic['xiao_han_jia_wen'] = 12
            self.game.generate_dialogue_sequence(
                self.game.snowy_mountain_win,
                "Snowy Mountain.png",
                [_("I found it! Woohoo!"),
                 _("Man, I almost died up there... It was so slippery!"),
                 _("I'm glad you made it back. Say, regarding that woman in white...."),
                 _("Was her name Jia Wen by any chance?"),
                 _("Erm, yeah... how did you know?"),
                 _("She... it's a long story... "),
                 _("Five years ago, by some stroke of luck, I was saved by the Grandmaster Ye Hehua from a life-threatening situation."),
                 _("To express my gratitude to Grandmaster Ye, I decided to stay by his side and serve him for some time."),
                 _("Though he never accepted me as his official disciple, he still showed kindness to me and taught me a few things."),
                 _("Even a little bit of guidance from him proved very beneficial to me."),
                 _("Then, about a year ago, we rescued a young woman from the hands of some thugs. This woman was Jia Wen."),
                 _("We both stuck around to serve Grandmaster Ye, and over time, we fell in love."),
                 _("So... how did you guys end up thousands of miles apart?"),
                 _("Though we wanted to stay beside Grandmaster Ye, there is much evil to eradicate and many poor and oppressed people to help."),
                 _("Before leaving, Grandmaster Ye assigned me to stay around this area to help the Shanhu Sect for a year, while Jia Wen was sent to the Eastern side of the country."),
                 _("I vowed to complete this mission that was given me. It has been 9 months. I haven't seen her in 9 months..."),
                 _("{}, can you please do me a favor and give this flute to Jia Wen and also send her my greetings?").format(self.game.character),
                 _("Sure, no problem.")
                 ],
                ["you", "you", "Xiao Yong", "Xiao Yong", "you", "Xiao Yong", "Xiao Yong", "Xiao Yong", "Xiao Yong",
                 "Xiao Yong", "Xiao Yong", "Xiao Yong", "you", "Xiao Yong", "Xiao Yong", "Xiao Yong", "Xiao Yong", "you"
                 ]
            )
            self.game.inv[_("Bamboo Flute")] = 1
            messagebox.showinfo("", _("Received Bamboo Flute x 1!"))
            self.clickedOnXiaoHan()