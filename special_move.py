from gettext import gettext
_ = gettext

class special_move:
    def __init__(self, name, level=1, damage_multiplier=1.1, effects=None):
        self.level_exp_thresholds = [
            0, 100, 250, 450, 700, 1000, 1350, 1800, 2300, 3000
        ]
        self.level = min([10,level])
        self.exp = 0
        self.exp_rate = 10
        self.name = name
        self.effects = effects

        if self.name == _("Scrub Fist"):
            self.file_name = "scrub_fist_icon.ppm"
            self.sfx_file_name = "sfx_SGSHitSoundLoud.mp3"
            self.animation_file_list = ["a_Scrub_Fist1.gif", "a_Scrub_Fist2.gif", "a_Scrub_Fist3.gif",
                                        "a_Scrub_Fist4.gif", "a_Scrub_Fist3.gif", "a_Scrub_Fist4.gif"]
            self.base_stamina = 15
            self.base_damage = 10
            self.stamina = 15
            self.damage = 10
            self.damage_multiplier = 1.2
            self.description_template = _("Scrub Fist\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA technique invented by Scrub as a starter move.\n\nAt level 5, gains 20% chance of stunning opponent.\nAt level 7, gains 30% chance of stunning opponent.\nAt level 10, gains 50% chance of stunning opponent.")
            self.type = _("Unarmed")

        elif self.name == _("Real Scrub Fist"):
            self.file_name = "scrub_fist_icon.ppm"
            self.sfx_file_name = "sfx_SGSHitSoundLoud.mp3"
            self.animation_file_list = ["a_Scrub_Fist1.gif", "a_Scrub_Fist2.gif", "a_Scrub_Fist3.gif",
                                        "a_Scrub_Fist4.gif", "a_Scrub_Fist3.gif", "a_Scrub_Fist4.gif"]
            self.base_stamina = 65
            self.base_damage = 30
            self.stamina = 65
            self.damage = 30
            self.damage_multiplier = 1.2
            self.description_template = _("Real Scrub Fist\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nThe Real Scrub Fist... not the one Scrub taught you as a starter.\n\nAt level 5, gains 30% chance of stunning opponent.\nAt level 7, gains 40% chance of stunning opponent.\nAt level 10, gains 60% chance of stunning opponent.")
            self.type = _("Unarmed")

        elif self.name == _("Basic Punching Technique"):
            self.file_name = "scrub_fist_icon.ppm"
            self.sfx_file_name = "sfx_SGSHitSoundLoud.mp3"
            self.animation_file_list = ["a_Scrub_Fist1.gif", "a_Scrub_Fist2.gif", "a_Scrub_Fist3.gif",
                                        "a_Scrub_Fist4.gif", "a_Scrub_Fist3.gif", "a_Scrub_Fist4.gif"]
            self.base_stamina = 15
            self.base_damage = 15
            self.stamina = 15
            self.damage = 15
            self.damage_multiplier = 1.15
            self.description_template = _("Basic Punching Technique\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nStarting with the fundamentals...")
            self.type = _("Unarmed")


        elif self.name == _("Yunhuan Taichi Fist"):
            self.file_name = "yunhuan_taichi_fist_icon.png"
            self.sfx_file_name = "sfx_SGSHitSoundLoud.mp3"
            self.animation_file_list = ["a_Scrub_Fist1.gif", "a_Scrub_Fist2.gif", "a_Scrub_Fist3.gif",
                                        "a_Scrub_Fist4.gif", "a_Scrub_Fist3.gif", "a_Scrub_Fist4.gif"]
            self.base_stamina = 34
            self.base_damage = 18
            self.stamina = 34
            self.damage = 18
            self.damage_multiplier = 1.16
            self.description_template = _("Yunhuan Taichi Fist\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA derivative of Wudang Taichi invented by Ma Guobao's father.\n\nAt level 2, gains 10% damage reduction and 20% damage boost.\nAt level 4, gains 20% damage reduction and 40% damage boost.\nAt level 6, gains 30% damage reduction and 60% damage boost.\nAt level 8, gains 40% damage reduction and 80% damage boost.\nAt level 10, gains 50% damage reduction and 100% damage boost.")
            self.type = _("Unarmed")


        elif self.name == _("Rest"):
            self.file_name = "rest_icon.ppm"
            self.sfx_file_name = "sfx_recover.mp3"
            self.animation_file_list = ["a_rest{}.gif".format(i) for i in range(1,20)]
            self.base_stamina = 0
            self.base_damage = 0
            self.stamina = 0
            self.damage = 0
            self.damage_multiplier = damage_multiplier
            self.description_template = _("Rest\n-------------------------\nRecovers 5% of your health and 20% stamina (based on upper limit);\nactual amount can increase based on character skills obtained and/or equipment.")
            self.type = _("Basic")


        elif self.name == _("Items"):
            self.file_name = "inventory_icon.ppm"
            self.sfx_file_name = "button-20.mp3"
            self.animation_file_list = []
            self.base_stamina = 0
            self.base_damage = 0
            self.stamina = 0
            self.damage = 0
            self.damage_multiplier = damage_multiplier
            self.description_template = _("Use an item from your inventory. Spends 33% fewer action points.")
            self.type = _("Basic")


        elif self.name == _("Flee"):
            self.file_name = "flee_icon.ppm"
            self.sfx_file_name = "button-20.mp3"
            self.animation_file_list = []
            self.base_stamina = 0
            self.base_damage = 0
            self.stamina = 0
            self.damage = 0
            self.damage_multiplier = damage_multiplier
            self.description_template = _("Flee from battle! Chance of success depends on your\nand opponent's speed and difference in levels.")
            self.type = _("Basic")


        elif self.name == _("Fire Palm"):
            self.file_name = "fire_palm_icon.gif"
            self.sfx_file_name = "sfx_fire_palm.mp3"
            self.animation_file_list = ["a_Fire_Palm1.gif", "a_Fire_Palm2.gif", "a_Fire_Palm3.gif", "a_Fire_Palm4.gif",
                                        "a_Fire_Palm5.gif", "a_Fire_Palm6.gif", "a_Fire_Palm7.gif"]
            self.base_stamina = 40
            self.base_damage = 30
            self.stamina = 40
            self.damage = 30
            self.damage_multiplier = 1.12
            self.description_template = _("Fire Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nCauses a painful burning sensation in the chest.\n\nAt level 3, gains 20% chance of causing internal injury.\nAt level 5, gains 40% chance of causing internal injury.\nAt level 7, gains 60% chance of causing internal injury.\nAt level 10, gains 100% chance of causing internal injury.")
            self.type = _("Unarmed")


        elif self.name == _("Guan Yin Palm"):
            self.file_name = "guan_yin_palm_icon.ppm"
            self.sfx_file_name = "sfx_fire_palm.mp3"
            self.animation_file_list = ["a_guan_yin_palm{}.gif".format(i) for i in range(1,13)]
            self.base_stamina = 42
            self.base_damage = 25
            self.stamina = 42
            self.damage = 25
            self.damage_multiplier = 1.17
            self.description_template = _("Guan Yin Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nOne of the 72 Arts of Shaolin.\n\nAt level 2, gains +2 strength on hit.\nAt level 4, gains +4 strength on hit.\nAt level 6, gains +6 strength on hit.\nAt level 8, gains +8 strength on hit.\nAt level 10, gains +10 strength on hit.")
            self.type = _("Unarmed")


        elif self.name == _("Iron Sand Palm"):
            self.file_name = "iron_sand_palm_icon.png"
            self.sfx_file_name = "sfx_shapeless_palm.wav"
            self.animation_file_list = ["a_iron_sand_palm{}.gif".format(i) for i in range(1,13)]
            self.base_stamina = 56
            self.base_damage = 20
            self.stamina = 56
            self.damage = 20
            self.damage_multiplier = 1.18
            self.description_template = _("Iron Sand Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nOne of the 72 Arts of Shaolin. One must repeated strike iron sandbags to\npractice this technique.\n\nAt level 2, decrease opponent's defence by 2 on hit.\nAt level 4, decrease opponent's defence by 4 on hit.\nAt level 6, decrease opponent's defence by 6 on hit.\nAt level 8, decrease opponent's defence by 8 on hit.\nAt level 10, decrease opponent's defence by 10 on hit.")
            self.type = _("Unarmed")


        elif self.name == _("Five Poison Palm"):
            self.file_name = "five_poison_palm_icon.png"
            self.sfx_file_name = "sfx_five_poison_palm.mp3"
            self.animation_file_list = ["a_five_poison_palm1.png", "a_five_poison_palm2.png", "a_five_poison_palm3.png",
                                        "a_five_poison_palm4.png", "a_five_poison_palm5.png", "a_five_poison_palm4.png",
                                        "a_five_poison_palm3.png", "a_five_poison_palm2.png", "a_five_poison_palm1.png"]
            self.base_stamina = 40
            self.base_damage = 20
            self.stamina = 40
            self.damage = 20
            self.damage_multiplier = 1.15
            self.description_template = _("Five Poison Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nAn advanced palm attack that originated from Phantom Sect, this technique\nmust be learned using the 5 types of deadly poisonous moths.\n\nAt level 3, gains 10% chance of poisoning opponent.\nAt level 5, gains 20% chance of poisoning opponent.\nAt level 7, gains 30% chance of poisoning opponent.\nAt level 10, gains 50% chance of poisoning opponent.")
            self.type = _("Unarmed")


        elif self.name == _("Kiss of Death"):
            self.file_name = "kiss_of_death_icon.ppm"
            self.sfx_file_name = "sfx_five_poison_palm.mp3"
            self.animation_file_list = ["a_kiss_of_death{}.gif".format(i) for i in range(1, 12)]
            self.base_stamina = 40
            self.base_damage = 20
            self.stamina = 40
            self.damage = 20
            self.damage_multiplier = 1.15
            self.description_template = _("Kiss of Death\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nDescription\n\nAt level 5, 50% chance of inflicting 'Death' status on opponent on hit.\nAt level 10, 100% chance of inflicting 'Death' status on opponent on hit.")
            self.type = _("Unarmed")


        elif self.name == _("Ice Palm"):
            self.file_name = "ice_palm_icon.png"
            self.sfx_file_name = "sfx_ice_palm.mp3"
            self.animation_file_list = ["a_ice_palm1.png", "a_ice_palm2.png", "a_ice_palm3.png", "a_ice_palm4.png",
                                        "a_ice_palm5.png", "a_ice_palm6.png", "a_ice_palm7.png", "a_ice_palm8.png"]
            self.base_stamina = 55
            self.base_damage = 25
            self.stamina = 55
            self.damage = 25
            self.damage_multiplier = 1.2
            self.description_template = _("Ice Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA technique invented by Xiao Yong after spending many years dwelling in the extreme cold climates in the Snowy Mountains.\nEach strike is infused with cold 'Qi' that disrupts the opponent's ability to harness their energy.\n\nAt level 2, gains 10% chance of causing internal injury; reduces opponent's speed by 2 on hit.\nAt level 4, gains 20% chance of causing internal injury; reduces opponent's speed by 4 on hit.\nAt level 6, gains 30% chance of causing internal injury; reduces opponent's speed by 6 on hit.\nAt level 8, gains 40% chance of causing internal injury; reduces opponent's speed by 8 on hit.\nAt level 10, gains 50% chance of causing internal injury; reduces opponent's speed by 10 on hit.")
            self.type = _("Unarmed")


        elif self.name == _("Ice Burst"):
            self.file_name = "ice_burst_icon.ppm"
            self.sfx_file_name = "sfx_ice_palm.mp3"
            self.animation_file_list = ["a_ice_palm1.png", "a_ice_palm2.png", "a_ice_palm3.png", "a_ice_palm4.png",
                                        "a_ice_palm5.png", "a_ice_palm6.png", "a_ice_palm7.png", "a_ice_palm8.png"]
            self.base_stamina = 80
            self.base_damage = 22
            self.stamina = 80
            self.damage = 22
            self.damage_multiplier = 1.18
            self.description_template = _("Ice Burst\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA derivative technique invented by Li Xiaoyang after learning Ice Palm.\n\nReduces opponent's speed by 2X on hit, where X is the proficiency level of 'Ice Palm'.")
            self.type = _("Unarmed")


        elif self.name == _("Shaolin Luohan Fist"):
            self.file_name = "luohan_fist_icon.png"
            self.sfx_file_name = "sfx_luohan_fist.mp3"
            self.animation_file_list = ["a_luohan_fist8.png", "a_luohan_fist8.png", "a_luohan_fist7.png",
                                        "a_luohan_fist6.png", "a_luohan_fist5.png", "a_luohan_fist4.png",
                                        "a_luohan_fist3.png", "a_luohan_fist2.png", "a_luohan_fist1.png"]
            self.base_stamina = 30
            self.base_damage = 20
            self.stamina = 30
            self.damage = 20
            self.damage_multiplier = 1.12
            self.description_template = _("Shaolin Luohan Fist\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nOne of the introductory moves taught at Shaolin.\n\nAt level 3, recovers 5% stamina on hit.\nAt level 6, recovers 10% stamina on hit.\nAt level 10, recovers 20% stamina on hit.")
            self.type = _("Unarmed")


        elif self.name == _("Heart Shattering Fist"):
            self.file_name = "heart_shattering_fist_icon.ppm"
            self.sfx_file_name = "sfx_luohan_fist.mp3"
            self.animation_file_list = ["a_heart_shattering_fist{}.gif".format(i) for i in range(1,10)]
            self.base_stamina = 30
            self.base_damage = 24
            self.stamina = 30
            self.damage = 24
            self.damage_multiplier = 1.14
            self.description_template = _("Heart Shattering Fist\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nSends a strong vibration to the heart. If the victim's internal energy is too\nweak, their heart will be shattered into a hundred pieces.\n\nAt level 2, gains 10% chance of stunning opponent.\nAt level 4, gains 20% chance of stunning opponent.\nAt level 6, gains 30% chance of stunning opponent.\nAt level 8, gains 40% chance of stunning opponent.\nAt level 10, gains 50% chance of stunning opponent.")
            self.type = _("Unarmed")


        elif self.name == _("Falling Cherry Blossom Finger Technique"):
            self.file_name = "falling_cherry_blossom_finger_technique_icon.png"
            self.sfx_file_name = "sfx_finger.mp3"
            self.animation_file_list = ["a_falling_cherry_blossom_finger_technique{}.png".format(i) for i in
                                        range(1, 20)]
            self.base_stamina = 21
            self.base_damage = 15
            self.stamina = 21
            self.damage = 15
            self.damage_multiplier = 1.12
            self.description_template = _("Falling Cherry Blossom Finger Technique\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA technique, invented by Huang Xiaodong, that has enough precision to\nstrike through a falling cherry blossom from 10 feet away.\n\nAt level 3, gains 10% chance of sealing opponent's acupuncture points for 3 turns.\nAt level 5, gains 20% chance of sealing opponent's acupuncture points for 3 turns.\nAt level 7, gains 30% chance of sealing opponent's acupuncture points for 3 turns.\nAt level 10, gains 40% chance of sealing opponent's acupuncture points for 3 turns.")
            self.type = _("Unarmed")


        elif self.name == _("Shaolin Diamond Finger"):
            self.file_name = "shaolin_diamond_finger.png"
            self.sfx_file_name = "sfx_shaolin_diamond_finger.mp3"
            self.animation_file_list = ["a_shaolin_diamond_finger{}.gif".format(i) for i in
                                        range(1, 21)]
            self.base_stamina = 48
            self.base_damage = 20
            self.stamina = 48
            self.damage = 20
            self.damage_multiplier = 1.15
            self.description_template = _("Shaolin Diamond Finger\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nOne of the 72 Arts of Shaolin.\n\nAt level 3, gains 20% chance of sealing opponent's acupuncture points for 1 turn.\nAt level 5, gains 40% chance of sealing opponent's acupuncture points for 1 turn.\nAt level 7, gains 60% chance of sealing opponent's acupuncture points for 1 turn.\nAt level 10, gains 100% chance of sealing opponent's acupuncture points for 1 turn.")
            self.type = _("Unarmed")


        elif self.name == _("Lightning Finger Technique"):
            self.file_name = "lightning_finger_technique.png"
            self.sfx_file_name = "sfx_shaolin_diamond_finger.mp3"
            self.animation_file_list = ["a_lightning_finger_technique{}.gif".format(i) for i in
                                        range(1, 14)]
            self.base_stamina = 40
            self.base_damage = 18
            self.stamina = 40
            self.damage = 18
            self.damage_multiplier = 1.16
            self.description_template = _("Lightning Finger Technique\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nLi Mianzhuo's special technique.\n\nAt level 3, gains 5% chance of sealing opponent's acupuncture points for 1 turn and stunning opponent.\nAt level 5, gains 15% chance of sealing opponent's acupuncture points for 1 turn and stunning opponent.\nAt level 7, gains 25% chance of sealing opponent's acupuncture points for 1 turn and stunning opponent.\nAt level 10, gains 40% chance of sealing opponent's acupuncture points for 1 turn and stunning opponent.")
            self.type = _("Unarmed")


        elif self.name == _("Taichi Fist"):
            self.file_name = "taichi_fist_icon.png"
            self.sfx_file_name = "sfx_five_poison_palm.mp3"
            self.animation_file_list = ["a_taichi_fist{}.gif".format(i) for i in
                                        range(1, 12)]
            self.base_stamina = 22
            self.base_damage = 18
            self.stamina = 22
            self.damage = 18
            self.damage_multiplier = 1.1
            self.description_template = _("Taichi Fist\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA highly defensive technique that incorporates the essence of Tai Chi.\n\nAt level 3, gains +2 defence on hit.\nAt level 5, gains +4 defence on hit.\nAt level 7, gains +6 defence on hit.\nAt level 10, gains +10 defence on hit.")
            self.type = _("Unarmed")


        elif self.name == _("Thunder Palm"):
            self.file_name = "thunder_palm_icon.png"
            self.sfx_file_name = "Thunder.wav"
            self.animation_file_list = ["a_thunder_palm{}.gif".format(i) for i in
                                        range(1, 28)]
            self.base_stamina = 35
            self.base_damage = 15
            self.stamina = 35
            self.damage = 15
            self.damage_multiplier = 1.15
            self.description_template = _("Thunder Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nLei Mengde's special technique.\n\nAt level 3, deals 50% extra damage when opponent's acupuncture points are sealed.\nAt level 5, deals 100% extra damage when opponent's acupuncture points are sealed.\nAt level 7, deals 150% extra damage when opponent's acupuncture points are sealed.\nAt level 10, deals 250% extra damage when opponent's acupuncture points are sealed..")
            self.type = _("Unarmed")


        elif self.name == _("Phantom Claws"):
            self.file_name = "phantom_claws_icon.ppm"
            self.sfx_file_name = "sfx_phantom_claws.mp3"
            self.animation_file_list = ["a_phantom_claws{}.gif".format(i) for i in
                                        range(1, 18)]
            self.base_stamina = 30
            self.base_damage = 20
            self.stamina = 30
            self.damage = 20
            self.damage_multiplier = 1.15
            self.description_template = _(
                "Phantom Claws\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nHow satisfying to feel the opponent's blood on your fingertips...\n\nAt level 3, 5% of damage dealt to opponent is added to own health and stamina.\nAt level 5, 10% of damage dealt to opponent is added to own health and stamina.\nAt level 7, 15% of damage dealt to opponent is added to own health and stamina.\nAt level 10, 20% of damage dealt to opponent is added to own health and stamina.")
            self.type = _("Unarmed")


        elif self.name == _("Eagle Claws"):
            self.file_name = "eagle_claws_icon.ppm"
            self.sfx_file_name = "sfx_phantom_claws.mp3"
            self.animation_file_list = ["a_eagle_claws{}.gif".format(i) for i in
                                        range(1, 11)]
            self.base_stamina = 40
            self.base_damage = 22
            self.stamina = 40
            self.damage = 22
            self.damage_multiplier = 1.16
            self.description_template = _(
                "Eagle Claws\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nEagle Sect's top technique that mimics the sharp claw strikes of an eagle.\n\nAt level 5, decreases each of opponent's stats by 2.\nAt level 10, decreases each of opponent's stats by 3.")
            self.type = _("Unarmed")


        elif self.name == _("Violent Dragon Palm"):
            self.file_name = "violent_dragon_palm_icon.ppm"
            self.sfx_file_name = "sfx_violent_dragon_palm.mp3"
            self.animation_file_list = ["a_violent_dragon_palm{}.gif".format(i) for i in
                                        range(1, 11)]
            self.base_stamina = 95
            self.base_damage = 30
            self.stamina = 95
            self.damage = 30
            self.damage_multiplier = 1.2
            self.description_template = _(
                "Violent Dragon Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA powerful attack that builds upon previous strikes.\n\nWith each consecutive hit, damage increases by 4X%, where X is the proficiency level of the move.")
            self.type = _("Unarmed")


        elif self.name == _("Empty Force Fist"):
            self.file_name = "empty_force_fist_icon.ppm"
            self.sfx_file_name = "sfx_empty_force_fist.mp3"
            self.animation_file_list = ["a_empty_force_fist{}.gif".format(i) for i in
                                        range(1, 6)]
            self.base_stamina = 30
            self.base_damage = 21
            self.stamina = 30
            self.damage = 21
            self.damage_multiplier = 1.18
            self.description_template = _(
                "Empty Force Fist\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nOne of the 72 Arts of Shaolin.\n\nAt level 3, 15% chance of inflicting critical hit.\nAt level 5, 30% chance of inflicting critical hit.\nAt level 7, 50% chance of inflicting critical hit.\nAt level 10, 75% chance of inflicting critical hit.")
            self.type = _("Unarmed")


        elif self.name == _("Shapeless Palm"):
            self.file_name = "shapeless_palm_icon.ppm"
            self.sfx_file_name = "sfx_shapeless_palm.wav"
            self.animation_file_list = ["a_shapeless_palm{}.gif".format(i) for i in
                                        range(1, 12)]
            self.base_stamina = 30
            self.base_damage = 20
            self.stamina = 30
            self.damage = 20
            self.damage_multiplier = 1.17
            self.description_template = _(
                "Shapeless Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nHaving almost no shape or form, this palm strike\nis extremely difficult to anticipate.\n\nAt level 3, +15% accuracy.\nAt level 5, +30% accuracy.\nAt level 7, +50% accuracy.\nAt level 10, +75% accuracy.")
            self.type = _("Unarmed")


        elif self.name == _("Shanhu Fist"):
            self.file_name = "shanhu_fist_icon.ppm"
            self.sfx_file_name = "sfx_shanhu_fist.mp3"
            self.animation_file_list = ["a_shanhu_fist{}.gif".format(i) for i in
                                        range(1, 14)]
            self.base_stamina = 60
            self.base_damage = 22
            self.stamina = 60
            self.damage = 22
            self.damage_multiplier = 1.2
            self.description_template = _("Shanhu Fist\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA powerful Shanhu Sect technique that requires a lot of energy.\nAnyone whose internal energy is not strong enough will quickly\nbecome exhausted when trying to use this attack.\n\nAt level 2, gains 15% chance of causing internal injury and stunning opponent.\nAt level 4, gains 20% chance of causing internal injury and stunning opponent.\nAt level 6, gains 25% chance of causing internal injury and stunning opponent.\nAt level 8, gains 30% chance of causing internal injury and stunning opponent.\nAt level 10, gains 40% chance of causing internal injury and stunning opponent.")
            self.type = _("Unarmed")


        elif self.name == _("Sumeru Palm"):
            self.file_name = "sumeru_palm_icon.png"
            self.sfx_file_name = "sfx_kusarigama.mp3"
            self.animation_file_list = ["a_sumeru_palm{}.gif".format(i) for i in
                                        range(1, 9)]
            self.base_stamina = 120
            self.base_damage = 50
            self.stamina = 120
            self.damage = 50
            self.damage_multiplier = 1.18
            self.description_template = _("Sumeru Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nOne of the 72 Arts of Shaolin.\nA technique that's so difficult to learn that the last\nperson to master this technique died a century ago.\n\nRequires 75% more action points.\nOn hit, uses up additional stamina (up to the move's\nbase stamina) to deal extra damage.\n\nAt level 3, gains +20% accuracy.\nAt level 5, gains +30% accuracy.\nAt level 10, gains +50% accuracy.")
            self.type = _("Unarmed")


        elif self.name == _("Hun Yuan Shapeshifting Fist"):
            self.file_name = "hun_yuan_shapeshifting_fist_icon.png"
            self.sfx_file_name = "sfx_luohan_fist.mp3"
            self.animation_file_list = ["a_hun_yuan_shapeshifting_fist{}.gif".format(i) for i in
                                        range(1, 8)]
            self.base_stamina = 88
            self.base_damage = 25
            self.stamina = 88
            self.damage = 25
            self.damage_multiplier = 1.16
            self.description_template = _("Hun Yuan Shapeshifting Fist\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA fist technique that complements the Hun Yuan Yin Qi.\n\nDeals 10X% extra damage, where X is the proficiency level of Hun Yuan Yin Qi.")
            self.type = _("Unarmed")


        elif self.name == _("Fairy Plucking Flowers"):
            self.file_name = "fairy_plucking_flowers_icon.png"
            self.sfx_file_name = "sfx_restore_status.mp3"
            self.animation_file_list = ["a_Fairy Plucking Flowers{}.gif".format(i) for i in
                                        range(1, 16)]
            self.base_stamina = 20
            self.base_damage = 10
            self.stamina = 20
            self.damage = 10
            self.damage_multiplier = 1.1
            self.description_template = _("Fairy Plucking Flowers\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nSpecial technique of the Dexterous Fairy, most skilled thief in the world.\nGreat for pickpocketing and sealing acupuncture points.\n\nAt level 2, 20% chance of sealing opponent's acupuncture points for 1 turn and pickpocketing the opponent.\nAt level 4, 40% chance of sealing opponent's acupuncture points for 1 turn and pickpocketing the opponent.\nAt level 6, 60% chance of sealing opponent's acupuncture points for 1 turn and pickpocketing the opponent.\nAt level 8, 80% chance of sealing opponent's acupuncture points for 1 turn and pickpocketing the opponent.\nAt level 10, 100% chance of sealing opponent's acupuncture points for 1 turn and pickpocketing the opponent.")
            self.type = _("Unarmed")


        elif self.name == _("Prajna Palm"):
            self.file_name = "prajna_palm_icon.png"
            self.sfx_file_name = "sfx_luohan_fist.mp3"
            self.animation_file_list = ["a_prajna_palm{}.gif".format(i) for i in
                                        range(1, 16)]
            self.base_stamina = 40
            self.base_damage = 23
            self.stamina = 40
            self.damage = 23
            self.damage_multiplier = 1.3
            self.description_template = _("Prajna Palm\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nOne of the 72 Arts of Shaolin.\nA technique with a low learning threshold but takes\nan extraordinarily long time to master.\n\nAt level 3, gains +30% accuracy. Recovers 3% stamina.\nAt level 5, gains +50% accuracy. Recovers 5% stamina.\nAt level 7, gains +70% accuracy. Recovers 7% stamina.\nAt level 10, gains +100% accuracy. Recovers 10% stamina.")
            self.type = _("Unarmed")

            #increase threshold for leveling for Prajna Palm
            self.level_exp_thresholds = [
                0, 200, 500, 900, 1400, 2000, 2700, 3600, 4600, 6000
            ]


        elif self.name == _("Dragon Roar"):
            self.file_name = "dragon_roar_icon.ppm"
            self.sfx_file_name = "sfx_roar.wav"
            self.animation_file_list = ["a_dragon_roar{}.gif".format(i) for i in
                                        range(1, 13)]
            self.base_stamina = 90
            self.base_damage = 25
            self.stamina = 90
            self.damage = 25
            self.damage_multiplier = 1.18
            self.description_template = _("Dragon Roar\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA deafening roar that requires much energy and concentration.\n\nMove has 100% accuracy.\n\nAt level 3: 10% chance of causing internal injury to the opponent. Lowers opponent's defence by 6.\nAt level 5: 20% chance of causing internal injury to the opponent. Lowers opponent's defence by 6.\nAt level 7: 30% chance of causing internal injury to the opponent. Lowers opponent's defence by 6.\nAt level 10: 50% chance of causing internal injury to the opponent. Lowers opponent's defence by 6.")
            self.type = _("Unarmed")

        elif self.name == _("Spider Poison Powder"):
            self.file_name = "spider_poison_powder_icon.ppm"
            self.sfx_file_name = "sfx_five_poison_palm.mp3"
            self.animation_file_list = ["a_spider_poison_powder{}.png".format(i) for i in range(1,11)]
            self.base_stamina = 0
            self.base_damage = 0
            self.stamina = 0
            self.damage = 0
            self.damage_multiplier = 0
            self.description_template = _("75% chance of poisoning opponent.\nUses item: <Spider Poison Powder> x 1.")
            self.type = _("Hidden Weapon")

        elif self.name == _("Rainbow Fragrance"):
            self.file_name = "rainbow_fragrance_icon.png"
            self.sfx_file_name = "sfx_five_poison_palm.mp3"
            self.animation_file_list = ["a_rainbow_fragrance{}.gif".format(i) for i in range(1,11)]
            self.base_stamina = 0
            self.base_damage = 0
            self.stamina = 0
            self.damage = 0
            self.damage_multiplier = 0
            self.description_template = _("Blinds opponent for 1 turn.\nReduces opponent's stamina by 20%.\nReduces all opponent's stats by 2.\n\nUses item: <Rainbow Fragrance> x 1.")
            self.type = _("Hidden Weapon")

        elif self.name == _("Centipede Poison Powder"):
            self.file_name = "centipede_poison_powder_icon.ppm"
            self.sfx_file_name = "sfx_five_poison_palm.mp3"
            self.animation_file_list = ["a_centipede_poison_powder{}.png".format(i) for i in range(1, 11)]
            self.base_stamina = 0
            self.base_damage = 0
            self.stamina = 0
            self.damage = 0
            self.damage_multiplier = 0
            self.description_template = _("Reduces opponent's stamina by 50% (of upper limit).\nUses item: <Centipede Poison Powder> x 1.")
            self.type = _("Hidden Weapon")


        elif self.name == _("Body Cleansing Technique"):
            self.file_name = "body_cleansing_technique_icon.ppm"
            self.sfx_file_name = "sfx_restore_status.mp3"
            self.animation_file_list = []
            self.base_stamina = 0
            self.base_damage = 0
            self.stamina = 0
            self.damage = 0
            self.damage_multiplier = 0
            self.description_template = _("Resets status to 'Normal'.")
            self.type = _("Basic")


        elif self.name == _("Soothing Song"):
            self.file_name = "soothing_song_icon.ppm"
            self.sfx_file_name = "sfx_restore_status.mp3"
            self.animation_file_list = []
            self.base_stamina = 0
            self.base_damage = 0
            self.stamina = 0
            self.damage = 0
            self.damage_multiplier = 0
            self.description_template = _("Soothing Song\n-------------------------\nRestores 10% health and stamina. 40% chance of resetting status to 'Normal'.")
            self.type = _("Basic")


        elif self.name == _("Basic Sword Technique"):
            self.file_name = "basic_sword_technique_icon.png"
            self.sfx_file_name = "sfx_basic_sword_technique.mp3"
            self.animation_file_list = ["a_basic_sword_technique1.gif", "a_basic_sword_technique2.gif",
                                        "a_basic_sword_technique3.gif", "a_basic_sword_technique4.gif",
                                        "a_basic_sword_technique5.gif", "a_basic_sword_technique6.gif",
                                        "a_basic_sword_technique7.gif", "a_basic_sword_technique8.gif",
                                        "a_basic_sword_technique9.gif", "a_basic_sword_technique10.gif",
                                        "a_basic_sword_technique11.gif", "a_basic_sword_technique12.gif",
                                        "a_basic_sword_technique13.gif", "a_basic_sword_technique14.gif"]
            self.base_stamina = 20
            self.base_damage = 15
            self.stamina = 20
            self.damage = 15
            self.damage_multiplier = 1.1
            self.description_template = _("Basic Sword Technique\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA beginner sword technique\n\nAt level 3, gains +10% accuracy.\nAt level 6, gains +20% accuracy.\nAt level 10, gains +40% accuracy.")
            self.type = _("Armed")


        elif self.name == _("Huashan Sword Technique"):
            self.file_name = "hua_shan_sword_technique_icon.ppm"
            self.sfx_file_name = "sfx_basic_sword_technique.mp3"
            self.animation_file_list = ["a_hua_shan_sword_technique{}.gif".format(i) for i in range(1,8)]
            self.base_stamina = 25
            self.base_damage = 17
            self.stamina = 25
            self.damage = 17
            self.damage_multiplier = 1.17
            self.description_template = _("Huashan Sword Technique\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA sword technique taught to all disciples of Huashan\n\nAt level 2, gains +10% accuracy.\nAt level 4, gains +20% accuracy.\nAt level 6, gains +30% accuracy.\nAt level 8, gains +40% accuracy.\nAt level 10, gains +50% accuracy.")
            self.type = _("Armed")

        elif self.name == _("Kendo"):
            self.file_name = "kendo_icon.png"
            self.sfx_file_name = "sfx_basic_sword_technique.mp3"
            self.animation_file_list = ["a_kendo{}.gif".format(i) for i in range(1,9)]
            self.base_stamina = 60
            self.base_damage = 15
            self.stamina = 60
            self.damage = 15
            self.damage_multiplier = 1.17
            self.description_template = _("Kendo\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nWay of the Sword...\n\nMove has 100% accuracy.\n\nAt level 5, gains +4 attack and strength.\nAt level 10, gains +8 attack and strength.")
            self.type = _("Armed")

        elif self.name == _("Hachiman Blade"):
            self.file_name = "hachiman_blade_icon.png"
            self.sfx_file_name = "sfx_thousand_swords_technique.mp3"
            self.animation_file_list = ["a_hachiman_blade{}.gif".format(i) for i in range(1,11)]
            self.base_stamina = 64
            self.base_damage = 25
            self.stamina = 64
            self.damage = 25
            self.damage_multiplier = 1.18
            self.description_template = _("Hachiman Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA sword technique named after the Japanese God of War.\n\nAt level 3, gains 10% chance of inflicting 'Bleeding' status to opponent.\nAt level 5, gains 20% chance of inflicting 'Bleeding' status to opponent.\nAt level 7, gains 30% chance of inflicting 'Bleeding' status to opponent.\nAt level 10, gains 50% chance of inflicting 'Bleeding' status to opponent.")
            self.type = _("Armed")

        elif self.name == _("Thousand Swords Technique"):
            self.file_name = "thousand_swords_technique_icon.ppm"
            self.sfx_file_name = "sfx_thousand_swords_technique.mp3"
            self.animation_file_list = ["a_thousand_swords_technique{}.gif".format(i) for i in range(1,17)]
            self.base_stamina = 25
            self.base_damage = 15
            self.stamina = 25
            self.damage = 15
            self.damage_multiplier = 1.15
            self.description_template = _("Thousand Swords Technique\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nAs if thousands of sharp blades are coming at you simultaneously...\n\nAt level 3, gains +30% accuracy; +2 attack on hit.\nAt level 5, gains +50% accuracy; +4 attack on hit.\nAt level 7, gains +70% accuracy; gains +6 attack on hit.\nAt level 10, gains +100% accuracy; gains +10 attack on hit.")
            self.type = _("Armed")


        elif self.name == _("Babao Blade"):
            self.file_name = "babao_blade_icon.ppm"
            self.sfx_file_name = "sfx_babao_blade.mp3"
            self.animation_file_list = ["a_babao_blade{}.gif".format(i) for i in range(1,15)]
            self.base_stamina = 48
            self.base_damage = 24
            self.stamina = 48
            self.damage = 24
            self.damage_multiplier = 1.18
            self.description_template = _("Babao Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA technique with 8 x 8 = 64 possible transformations.\n\nAt level 3, gains 75% chance of activating one of the following effects (chosen randomly)\n\ton hit: Seal (1), Blind (1).\nAt level 5, gains 75% chance of activating one of the following effects (chosen randomly;\n\tbuilds on effects obtained from previous levels) on hit: Internal Injury, Stun.\nAt level 7, gains 75% chance of activating one of the following effects (chosen randomly;\n\tbuilds on effects obtained from previous levels) on hit: Opponent's defence -20, Opponent's speed -20.\nAt level 9, gains 75% chance of activating one of the following effects (chosen randomly;\n\tbuilds on effects obtained from previous levels) on hit: Seal (2), Blind (2)")
            self.type = _("Armed")


        elif self.name == _("Cherry Blossom Drifting Snow Blade"):
            self.file_name = "cherry_blossom_drifting_snow_blade_icon.png"
            self.sfx_file_name = "sfx_cherry_blossom_drifting_snow_blade.mp3"
            self.animation_file_list = ["a_cherry_blossom_drifting_snow_blade1.gif", "a_cherry_blossom_drifting_snow_blade2.gif",
                                        "a_cherry_blossom_drifting_snow_blade3.gif", "a_cherry_blossom_drifting_snow_blade4.gif",
                                        "a_cherry_blossom_drifting_snow_blade5.gif", "a_cherry_blossom_drifting_snow_blade6.gif",
                                        "a_cherry_blossom_drifting_snow_blade7.gif", "a_cherry_blossom_drifting_snow_blade8.gif",
                                        "a_cherry_blossom_drifting_snow_blade9.gif", "a_cherry_blossom_drifting_snow_blade10.gif",
                                        "a_cherry_blossom_drifting_snow_blade11.gif", "a_cherry_blossom_drifting_snow_blade12.gif",
                                        "a_cherry_blossom_drifting_snow_blade13.gif", "a_cherry_blossom_drifting_snow_blade14.gif"]

            self.base_stamina = 45
            self.base_damage = 30
            self.stamina = 45
            self.damage = 30
            self.damage_multiplier = 1.15
            self.description_template = _("Cherry Blossom Drifting Snow Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nIn the midst of drifting snowflakes and cherry blossoms,\none hears the sound of a blade dancing in the air...\n\nAt level 3, gains 10% chance of blinding opponent for 3 turns.\nAt level 5, gains 20% chance of blinding opponent for 3 turns.\nAt level 7, gains 30% chance of blinding opponent for 3 turns.\nAt level 10, gains 40% chance of blinding opponent for 3 turns.")
            self.type = _("Armed")


        elif self.name == _("Poison Needle"):
            self.file_name = "poison_needle_icon.ppm"
            self.sfx_file_name = "sfx_poison_dart.mp3"
            self.animation_file_list = ["a_poison_dart{}.png".format(i) for i in range(1,21)]

            self.base_stamina = 15
            self.base_damage = 10
            self.stamina = 15
            self.damage = 10
            self.damage_multiplier = 1.12
            self.description_template = _("Poison Needle\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nGreat for catching the opponent off guard\n\nSaves 25% move action points.\nAt level 2, gains 10% chance of poisoning opponent.\nAt level 4, gains 20% chance of poisoning opponent.\nAt level 6, gains 30% chance of poisoning opponent.\nAt level 8, gains 40% chance of poisoning opponent.\nAt level 10, gains 50% chance of poisoning opponent.")
            self.type = _("Hidden Weapon")


        elif self.name == _("Taichi Sword"):
            self.file_name = "taichi_sword_icon.ppm"
            self.sfx_file_name = "sfx_taichi_sword.mp3"
            self.animation_file_list = ["a_taichi_sword{}.gif".format(i) for i in range(1,10)]

            self.base_stamina = 35
            self.base_damage = 22
            self.stamina = 35
            self.damage = 22
            self.damage_multiplier = 1.15
            self.description_template = _("Taichi Sword\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA defensive technique that relies on continuous, circular sword movement\n\nAt level 3, gains +2 defence on hit.\nAt level 5, gains +4 defence on hit.\nAt level 7, gains +6 defence on hit.\nAt level 10, gains +10 defence on hit.")
            self.type = _("Armed")


        elif self.name == _("Blind Sniper Blade"):
            self.file_name = "blind_sniper_blade_icon.ppm"
            self.sfx_file_name = "sfx_blind_sniper_blade.mp3"
            self.animation_file_list = ["a_blind_sniper_blade{}.gif".format(i) for i in range(1,15)]

            self.base_stamina = 45
            self.base_damage = 28
            self.stamina = 45
            self.damage = 28
            self.damage_multiplier = 1.18
            self.description_template = _("Blind Sniper Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nStrike with the heart\n\nAt level 2, 10% chance of inflicting critical hit.\nAt level 4, 20% chance of inflicting critical hit.\nAt level 6, 30% chance of inflicting critical hit.\nAt level 8, 40% chance of inflicting critical hit.\nAt level 10, 50% chance of inflicting critical hit.")
            self.type = _("Armed")


        elif self.name == _("Smoke Bomb"):
            self.file_name = "smoke_bomb_icon.ppm"
            self.sfx_file_name = "sfx_smoke_bomb.mp3"
            self.animation_file_list = ["a_smoke_bomb{}.png".format(i) for i in range(1, 18)]
            self.base_stamina = 10
            self.base_damage = 50
            self.stamina = 10
            self.damage = 50
            self.damage_multiplier = 1.1
            self.description_template = _("Smoke Bomb\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nUses item: <Smoke Bomb> x 1.\nAt level 3, gains 15% chance of blinding opponent for 2 turns.\nAt level 5, gains 25% chance of blinding opponent for 2 turns.\nAt level 7, gains 35% chance of blinding opponent for 2 turns.\nAt level 10, gains 50% chance of blinding opponent for 2 turns.")
            self.type = _("Hidden Weapon")


        elif self.name == _("Superior Smoke Bomb"):
            self.file_name = "smoke_bomb_icon.ppm"
            self.sfx_file_name = "sfx_smoke_bomb.mp3"
            self.animation_file_list = ["a_smoke_bomb{}.png".format(i) for i in range(1, 18)]
            self.base_stamina = 10
            self.base_damage = 100
            self.stamina = 10
            self.damage = 100
            self.damage_multiplier = 1.1
            self.description_template = _("Superior Smoke Bomb\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nUses item: <Smoke Bomb> x 1.\nAt level 3, gains 15% chance of blinding opponent for 3 turns.\nAt level 5, gains 25% chance of blinding opponent for 3 turns.\nAt level 7, gains 35% chance of blinding opponent for 3 turns.\nAt level 10, gains 50% chance of blinding opponent for 3 turns.")
            self.type = _("Hidden Weapon")


        elif self.name == _("Whirlwind Staff"):
            self.file_name = "whirlwind_staff_icon.ppm"
            self.sfx_file_name = "sfx_whirlwind_staff.wav"
            self.animation_file_list = ["a_whirlwind_staff{}.gif".format(i) for i in range(1, 28)]
            self.base_stamina = 40
            self.base_damage = 24
            self.stamina = 40
            self.damage = 24
            self.damage_multiplier = 1.16
            self.description_template = _("Whirlwind Staff\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nAt level 2, gains +2 attack and +2 strength on hit; 20% chance of blinding opponent for 1 turn.\nAt level 4, gains +2 attack and +2 strength on hit; 40% chance of blinding opponent for 1 turn.\nAt level 6, gains +4 attack and +4 strength on hit; 60% chance of blinding opponent for 1 turn.\nAt level 8, gains +4 attack and +4 strength on hit; 80% chance of blinding opponent for 1 turn.\nAt level 10, gains +6 attack and +6 strength on hit; 100% chance of blinding opponent for 1 turn.")
            self.type = _("Armed")


        elif self.name == _("Luohan Staff"):
            self.file_name = "luohan_staff_icon.ppm"
            self.sfx_file_name = "sfx_blind_sniper_blade.mp3"
            self.animation_file_list = ["a_luohan_staff{}.gif".format(i) for i in range(1, 14)]
            self.base_stamina = 40
            self.base_damage = 18
            self.stamina = 40
            self.damage = 18
            self.damage_multiplier = 1.15
            self.description_template = _("Luohan Staff\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA technique used by Luohan Monks of Shaolin; very deadly in numbers.\n\nAt level 3, gains 10% chance of stunning opponent.\nAt level 5, gains 25% chance of stunning opponent.\nAt level 7, gains 40% chance of stunning opponent.\nAt level 10, gains 60% chance of stunning opponent.")
            self.type = _("Armed")


        elif self.name == _("Wild Wind Blade"):
            self.file_name = "wild_wind_blade_icon.ppm"
            self.sfx_file_name = "sfx_wild_wind_blade.wav"
            self.animation_file_list = ["a_wild_wind_blade{}.gif".format(i) for i in range(1, 9)] + ["a_wild_wind_blade{}.gif".format(i) for i in range(1, 5)]
            self.base_stamina = 50
            self.base_damage = 20
            self.stamina = 50
            self.damage = 20
            self.damage_multiplier = 1.19
            self.description_template = _("Wild Wind Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA sword technique that relies on extreme speed, not giving the opponent a chance to strike back.\n\nAt level 2, blinds opponent for 1 turn; saves 15% action points.\nAt level 4, blinds opponent for 1 turn; saves 20% action points.\nAt level 6, blinds opponent for 1 turn; saves 25% action points.\nAt level 8, blinds opponent for 1 turn; saves 30% action points.\nAt level 10, blinds opponent for 1 turn; saves 40% action points.")
            self.type = _("Armed")

        elif self.name == _("Serpent Whip"):
            self.file_name = "serpent_whip_icon.png"
            self.sfx_file_name = "sfx_serpent_whip.wav"
            self.animation_file_list = ["a_Serpent_Whip{}.gif".format(i) for i in range(1, 5)]*2
            self.base_stamina = 36
            self.base_damage = 20
            self.stamina = 36
            self.damage = 20
            self.damage_multiplier = 1.16
            self.description_template = _("Serpent Whip\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nEach strike with the whip mimics the sharp fangs of a vicious serpent.\n\nAt level 3, reduces a random opponent stat by 4.\nAt level 6, reduces a random opponent stat by 8.\nAt level 10, reduces a random opponent stat by 12.")
            self.type = _("Armed")

        elif self.name == _("Kusarigama"):
            self.file_name = "kusarigama_icon.png"
            self.sfx_file_name = "sfx_kusarigama.mp3"
            self.animation_file_list = ["a_kusarigama{}.gif".format(i) for i in range(1, 6)]
            self.base_stamina = 81
            self.base_damage = 29
            self.stamina = 81
            self.damage = 29
            self.damage_multiplier = 1.17
            self.description_template = _("Kusarigama\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA traditional Japanese weapon that consists of a sickle on a kusari-fundo\n– a type of metal chain with a heavy iron weight at the end.\n\nAt level 3, 50% chance of disabling opponent's armed attacks for 1 turn on hit.\nAt level 5, 50% chance of disabling opponent's armed attacks for 2 turns on hit.\nAt level 7, 50% chance of disabling opponent's armed attacks for 3 turns on hit.\nAt level 10, 50% chance of disabling opponent's armed attacks for 5 turns on hit.")
            self.type = _("Armed")

        elif self.name == _("Hensojutsu"):
            self.file_name = "hensojutsu_icon.png"
            self.sfx_file_name = "sfx_hensojutsu.mp3"
            self.animation_file_list = ["black.gif", "you_icon_small.gif"]*5
            self.base_stamina = 1000
            self.base_damage = 0
            self.stamina = 1000
            self.damage = 0
            self.damage_multiplier = 1
            self.description_template = _("Hensojutsu\n-------------------------\n\nThe Art of Disguise.\n\nCopies one of the opponent's stats that are higher than yours.")
            self.type = _("Armed")

        elif self.name == _("Purity Blade"):
            self.file_name = "purity_blade_icon.ppm"
            self.sfx_file_name = "sfx_purity_blade.mp3"
            self.animation_file_list = ["a_purity_blade{}.gif".format(i) for i in range(1, 11)]
            self.base_stamina = 40
            self.base_damage = 20
            self.stamina = 40
            self.damage = 20
            self.damage_multiplier = 1.15
            self.description_template = _("Purity Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA technique reserved for the most chivalrous of martial artists...\n\nAdditional damage multiplier is applied based on chivalry\n\nAt level 3, gains 20% chance of clearing negative statuses.\nAt level 5, gains 40% chance of clearing negative statuses.\nAt level 7, gains 60% chance of clearing negative statuses.\nAt level 10, gains 100% chance of clearing negative statuses.")
            self.type = _("Armed")


        elif self.name == _("Shadow Blade"):
            self.file_name = "shadow_blade_icon.ppm"
            self.sfx_file_name = "sfx_cherry_blossom_drifting_snow_blade.mp3"
            self.animation_file_list = ["a_shadow_blade{}.gif".format(i) for i in range(1, 9)]
            self.base_stamina = 38
            self.base_damage = 21
            self.stamina = 38
            self.damage = 21
            self.damage_multiplier = 1.18
            self.description_template = _(
                "Shadow Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nStrike from the shadows like a phantom...\n\nAt level 2, lowers opponent's defence and dexterity by 2 on hit.\nAt level 4, lowers opponent's defence and dexterity by 4 on hit.\nAt level 6, lowers opponent's defence and dexterity by 6 on hit.\nAt level 8, lowers opponent's defence and dexterity by 8 on hit.\nAt level 10, lowers opponent's defence and dexterity by 10 on hit.")
            self.type = _("Armed")


        elif self.name == _("Ice Blade"):
            self.file_name = "ice_blade_icon.ppm"
            self.sfx_file_name = "sfx_taichi_sword.mp3"
            self.animation_file_list = ["a_ice_blade{}.gif".format(i) for i in range(1,10)]
            self.base_stamina = 75
            self.base_damage = 32
            self.stamina = 75
            self.damage = 32
            self.damage_multiplier = 1.22
            self.description_template = _("Ice Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nA technique invented by Xiao Yong after spending many years dwelling in the extreme cold climates in the Snowy Mountains.\nEach strike is infused with cold 'Qi' that disrupts the opponent's ability to harness their energy.\n\nAt level 2, gains 10% chance of causing internal injury; reduces opponent's speed by 2 on hit.\nAt level 4, gains 20% chance of causing internal injury; reduces opponent's speed by 4 on hit.\nAt level 6, gains 30% chance of causing internal injury; reduces opponent's speed by 6 on hit.\nAt level 8, gains 40% chance of causing internal injury; reduces opponent's speed by 8 on hit.\nAt level 10, gains 50% chance of causing internal injury; reduces opponent's speed by 10 on hit.")
            self.type = _("Armed")


        elif self.name == _("Demon Suppressing Blade"):
            self.file_name = "demon_suppressing_blade_icon.ppm"
            self.sfx_file_name = "sfx_cherry_blossom_drifting_snow_blade.mp3"
            self.animation_file_list = ["a_demon_suppressing_blade{}.gif".format(i) for i in range(1,9)]
            self.base_stamina = 45
            self.base_damage = 21
            self.stamina = 45
            self.damage = 21
            self.damage_multiplier = 1.18
            self.description_template = _("Demon Suppressing Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nDrains the opponent's energy while inflicting damage.\n\nAt level 3, reduces opponent's stamina by 45% of damage dealt.\nAt level 5, reduces opponent's stamina by 100% of damage dealt.\nAt level 7, reduces opponent's stamina by 200% of damage dealt.\nAt level 10, reduces opponent's stamina by 400% of damage dealt.")
            self.type = _("Armed")


        elif self.name == _("Fairy Sword Technique"):
            self.file_name = "fairy_sword_technique_icon.ppm"
            self.sfx_file_name = "sfx_thousand_swords_technique.mp3"
            self.animation_file_list = ["a_fairy_sword_technique{}.gif".format(i) for i in range(1,8)]
            self.base_stamina = 27
            self.base_damage = 18
            self.stamina = 27
            self.damage = 18
            self.damage_multiplier = 1.15
            self.description_template = _("Fairy Sword Technique\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nAn elegant sword technique that is very suitable for women.\n\nRecovers 3% health when used.\n\nAt level 2, increases dexterity by 2.\nAt level 4, increases dexterity by 4.\nAt level 6, increases dexterity by 6.\nAt level 8, increases dexterity by 8.\nAt level 10, increases dexterity by 10.")
            self.type = _("Armed")


        elif self.name == _("Wood Combustion Blade"):
            self.file_name = "wood_combustion_blade_icon.ppm"
            self.sfx_file_name = "sfx_thousand_swords_technique.mp3"
            self.animation_file_list = ["a_wood_combustion_blade{}.gif".format(i) for i in range(1,13)]
            self.base_stamina = 49
            self.base_damage = 27
            self.stamina = 49
            self.damage = 27
            self.damage_multiplier = 1.15
            self.description_template = _("Wood Combustion Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nPractice by slicing a log 81 times. Heats the blade to such a high\ntemperature that it will ignite wood upon contact.\n\nAt level 5, gains +8 strength on hit.\nAt level 10, gains +16 strength on hit.")
            self.type = _("Armed")


        elif self.name == _("Devil Crescent Moon Blade"):
            self.file_name = "devil_moon_blade_icon.ppm"
            self.sfx_file_name = "sfx_thousand_swords_technique.mp3"
            self.animation_file_list = ["a_devil_moon_blade{}.gif".format(i) for i in range(1,12)]
            self.base_stamina = 50
            self.base_damage = 21
            self.stamina = 50
            self.damage = 21
            self.damage_multiplier = 1.25
            self.description_template = _("Devil Crescent Moon Blade\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nDestroy anyone who gets in your way. There is no turning back.\n\nAdditional damage multiplier is applied based on chivalry (lower chivalry --> higher damage).\nSeals opponent's acupuncture points and blinds opponent for 1 turn on hit.\nOpponent's health and stamina upper limits are reduced by 100% of damage dealt.\n\nAt level 4, decreases opponent's defence and dexterity by 4 on hit.\nAt level 7, decreases opponent's defence and dexterity by 6 on hit.\nAt level 10, decreases opponent's defence and dexterity by 10 on hit.")
            self.type = _("Armed")


        elif self.name == _("Archery"):
            self.file_name = "archery_icon.png"
            self.sfx_file_name = "sfx_whoosh.mp3"
            self.animation_file_list = ["a_archery{}.gif".format(i) for i in range(1,13)]
            self.base_stamina = 8
            self.base_damage = 10
            self.stamina = 8
            self.damage = 10
            self.damage_multiplier = 1.13
            self.description_template = _("Archery\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nSaves 25% action points.")
            self.type = _("Armed")


        elif self.name == _("Crossbow"):
            self.file_name = "crossbow_icon.png"
            self.sfx_file_name = "sfx_whoosh.mp3"
            self.animation_file_list = ["a_archery{}.gif".format(i) for i in range(1,13)]
            self.base_stamina = 10
            self.base_damage = 20
            self.stamina = 10
            self.damage = 20
            self.damage_multiplier = 1.15
            self.description_template = _("Crossbow\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nSaves 50% action points.")
            self.type = _("Armed")


        elif self.name == _("Spear Attack"):
            self.file_name = "spear_attack_icon.ppm"
            self.sfx_file_name = "sfx_spear_attack.wav"
            self.animation_file_list = ["a_spear_attack{}.gif".format(i) for i in range(1,10)]
            self.base_stamina = 12
            self.base_damage = 20
            self.stamina = 12
            self.damage = 20
            self.damage_multiplier = 1.15
            self.description_template = _("Spear Attack\n-------------------------\nProficiency Level: {}\nExp: {}/{}\nBase Damage: {}\nDamage Growth: {}%\nStamina Cost: {}\nMove Type: {}\n-------------------------\nBasic spear attack.")
            self.type = _("Armed")


        # self.damage = int(self.damage * self.damage_multiplier)
        # self.stamina = int(self.stamina * self.damage_multiplier)
        if self.level != 1:
            self.calibrate_damage()
        self.description = ""
        self.set_description()


    def gain_exp(self, amount):
        self.exp += amount
        if self.level >= 10:
            return
        elif self.exp >= self.level_exp_thresholds[self.level]:
            self.level += 1
            #self.damage = int(self.damage * self.damage_multiplier)
            #self.stamina = int(self.stamina * self.damage_multiplier)
            self.calibrate_damage()
        self.set_description()


    def calibrate_damage(self):
        self.damage = int(self.base_damage * self.damage_multiplier ** (self.level-1))
        self.stamina = int(self.base_stamina * self.damage_multiplier ** (self.level-1))


    def set_description(self):
        self.description = self.description_template.format(self.level, self.exp,
                                                            self.level_exp_thresholds[min([9, self.level])],
                                                            self.damage, int(self.damage_multiplier * 100 - 100),
                                                            self.stamina, self.type)
