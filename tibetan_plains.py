from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
from mini_game_tibetan_plains_quicksand import*
import tkinter.messagebox as messagebox

_ = gettext

class Scene_tibetan_plains:
    def __init__(self, game):
        self.game = game

        if self.game.taskProgressDic["blind_sniper"] == 7:
            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()
            self.game.generate_dialogue_sequence(
                None,
                "Tibetan Plains BG.png",
                [_("Ohohoho! You've come at last, young man!"),
                 _("I admire your courage. How about this; if you become my son and swear to be loyal to me, I'll let your friend go."),
                 _("Not only that, I'll teach you everything I know! You'll be matchless among your peers!"),
                 _("Not interested. How about you get on your knees and call me 'Grandfather' 3 times?"),
                 _("If you do, I'll teach you everything I know!"),
                 _("Arrogant youngster! Do you not know the immensity of heaven and earth?!"),
                 _("No one speaks to me like that and lives to tell about it..."),
                 _("Give me the manual now, and I'll make your death quick and painless."),
                 _("I don't have the manual."),
                 _("What?! You dare to make a fool of me? Fine, I'll kill this friend of yours right now!"), #Row 2
                 _("No, wait! I didn't bring the Babao Blade Manual, but I have something better..."),
                 _("Oh? Is that so? What is it?"),
                 _("Have you heard of the Scrub Fist? It's far more powerful than the Babao Blade!"),
                 _("Hah, what kind of crappy technique is that? You trying to trick me or something, boy?"),
                 _("Why don't you come and try me then?"),
                 _("If you really are as good as you say, then you should have no problem crossing the quicksand and catching me."),
                 _("I'll wait for you on the other side. If you're not there half an hour after I arrive, I'll kill your friend right away."),
                 _("*Chen Wei leaps back into the patch of quicksand behind him, his legs moving like shadows.*"),
                 _("*You waste no time and begin giving chase.*")],
                ["Chen Wei", "Chen Wei", "Chen Wei", "you", "you", "Chen Wei", "Chen Wei", "Chen Wei", "you",
                 "Chen Wei", "you", "Chen Wei", "you", "Chen Wei", "you", "Chen Wei", "Chen Wei", "Blank", "Blank"]
            )

            self.game.mini_game_in_progress = True
            self.game.mini_game = tibetan_plains_quicksand_mini_game(self.game, self)
            self.game.mini_game.new()
            return


        self.game.tibetan_plains_win = Toplevel(self.game.mapWin)
        self.game.tibetan_plains_win.title(_("Tibetan Plains"))

        bg = PhotoImage(file="Tibetan Plains BG.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.tibetan_plains_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        self.game.tibetan_plainsF1 = Frame(canvas)

        if self.game.taskProgressDic["blind_sniper"] <= 3 and self.game.taskProgressDic["blind_sniper"] != -1000:
            pic1 = PhotoImage(file="Big Beard_icon_large.ppm")
            imageButton1 = Button(self.game.tibetan_plainsF1, command=self.clickedOnBigBeard)
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            label = Label(self.game.tibetan_plainsF1, text=_("Big Beard"))
            label.grid(row=1, column=0)
            self.game.tibetan_plainsF1.place(x=w*3// 4 - pic1.width() // 3, y=h // 4 - pic1.height()//2)

        #elif self.game.taskProgressDic["blind_sniper"] == -1000:
        #    pic1 = PhotoImage(file="Chen Wei_icon_large.ppm")
        #    imageButton1 = Button(self.game.tibetan_plainsF1, command=self.clickedOnBigBeard)
        #    imageButton1.grid(row=0, column=0)
        #    imageButton1.config(image=pic1)
        #    label = Label(self.game.tibetan_plainsF1, text=_("Chen Wei"))
        #    label.grid(row=1, column=0)
        #    self.game.tibetan_plainsF1.place(x=w*3// 4 - pic1.width() // 3, y=h // 4 - pic1.height() // 2)

        menu_frame = self.game.create_menu_frame(self.game.tibetan_plains_win)
        menu_frame.pack()
        self.game.tibetan_plains_win_F1 = Frame(self.game.tibetan_plains_win)
        self.game.tibetan_plains_win_F1.pack()

        self.game.tibetan_plains_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.tibetan_plains_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.tibetan_plains_win.winfo_width(), self.game.tibetan_plains_win.winfo_height()
        self.game.tibetan_plains_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.tibetan_plains_win.focus_force()
        self.game.tibetan_plains_win.mainloop()###


    def clickedOnBigBeard(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if self.game.taskProgressDic["big_beard"] == -1:
            self.game.taskProgressDic["big_beard"] = 0
            self.game.generate_dialogue_sequence(
                self.game.tibetan_plains_win,
                "Tibetan Plains BG.png",
                [_("Greeting, sir!"),
                 _("Ohohoho! Hey there, young man! What's yer name?"),
                 _("My name is {}. How should I address you, sir?").format(self.game.character),
                 _("Ahh, you can call me Uncle Big Beard..."),
                 _("So, what are you doing in such a secluded area, Uncle Big Beard?"),
                 _("Ay... It's a long story... I wasn't always a loner. I used to be an outgoing person..."),
                 _("Had a wife and 2 kids and a few buddies here and there..."),
                 _("Then... what happened?"),
                 _("Ay... you see, me family is all killed by a plague... I'm da only one left..."),
                 _("Now, I roam around aimlessly looking for ways to pass da rest of me miserable life..."), #Row 2
                 _("I happened to come across dis area here and, well, gots me some livestock from da locals..."),
                 _("Y'know, tryin' to enjoy some peace n' quiet while raisin' some buffalo..."),
                 _("I see..."),
                 _("By da way, if you gots anything you don't want, would you let me take a look see?"),
                 _("Tis a hobby of mine, collectin' strange items... I'll pay for them of course..."),
                 _("Yeah, sure, Uncle Big Beard. I'll let you know."),
                 _("Ohohoho! Many thanks, young man!")],
                ["you", "Big Beard", "you", "Big Beard", "you", "Big Beard", "Big Beard", "you", "Big Beard",
                 "Big Beard", "Big Beard", "Big Beard", "you", "Big Beard", "Big Beard", "you", "Big Beard"]
            )

        else:
            items_of_interest = []
            for key, value in self.game.inv.items():
                if key in [eq.name for eq in self.game.equipment] + ["Dungeon Key", "Gold"]:
                    pass
                elif _("Manual") in key and value >= 1:
                    items_of_interest.append(key)
                elif key not in ITEM_SELLING_PRICES and value >= 1 and random()>=.5:
                    items_of_interest.append(key)

            if items_of_interest:
                r_item = choice(items_of_interest)
                if _("Manual") in r_item:
                    r_price = randrange(1000,2001)
                else:
                    r_price = randrange(100,1001)

                self.game.tibetan_plains_win.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Tibetan Plains BG.png",
                    [_("Hey, Uncle Big Beard! Anything of my items look interesting to you?"),
                     _("Let me see.... Ohhh! I'll pay {} gold for da '{}'! How's that, eh?").format(r_price, r_item)],
                    ["you", "Big Beard"]
                )
                response = messagebox.askquestion("", _("Sell {} for {} gold?").format(r_item, r_price))
                if response == "yes":
                    self.game.generate_dialogue_sequence(
                        None,
                        "Tibetan Plains BG.png",
                        [_("Haha, deal! Thanks, me boy!"),],
                        ["Big Beard"]
                    )
                    self.game.add_item(r_item, -1)
                    self.game.add_item(_("Gold"), r_price)
                    self.game.taskProgressDic["big_beard"] += 1

                self.game.tibetan_plains_win.deiconify()


            else:
                self.game.generate_dialogue_sequence(
                    self.game.tibetan_plains_win,
                    "Tibetan Plains BG.png",
                    [_("Hey, Uncle Big Beard! Anything of my items look interesting to you?"),
                     _("Let me see.... Hmmm... Sorry, me boy, you ain't gots nothin' that catches me eye...")],
                    ["you", "Big Beard"]
                )


    def post_quicksand_minigame(self):
        self.game.mini_game_in_progress = False
        self.game.generate_dialogue_sequence(
            None,
            "Tibetan Plains BG.png",
            [_("Not bad, not bad... You made it here alive..."),
             _("{}! Get out of here while you can! Don't worry about me!").format(self.game.character),
             _("Nie Tu! I'm here to save you!"),
             _("Hahahahaha! You are too young, too simple... even naive..."),
             _("Now, you'd better demonstrate that you're worth keeping alive. Otherwise, I'll have to go kidnap a few more of your friends."),
             _("I'm ready. Bring it...")],
            ["Chen Wei", "Nie Tu", "you", "Chen Wei", "Chen Wei", "you"]
        )

        opp = character(_("Chen Wei"), 24,
                        sml=[special_move(_("Thousand Swords Technique"), level=10),
                             special_move(_("Shadow Blade"), level=10),
                             special_move(_("Wood Combustion Blade"), level=10),
                             special_move(_("Taichi Sword"), level=10),
                             special_move(_("Kendo"), level=10),],
                        skills=[skill(_("Phantom Steps"), level=10),
                                skill(_("Basic Agility Technique"), level=10),
                                skill(_("Huashan Sword Qi Gong"), level=10),
                                skill(_("Shaolin Inner Energy Technique"), level=10),
                                skill(_("Dragonfly Dance"), level=10),])

        self.game.battleMenu(self.game.you, opp, "Tibetan Plains BG.png",
                             "jy_suspenseful1.mp3", fromWin=None, battleType="task",
                             postBattleCmd=self.post_chen_wei_battle)


    def post_chen_wei_battle(self, choice=0):
        if choice == 0:
            self.game.generate_dialogue_sequence(
                None,
                "Tibetan Plains BG.png",
                [_("I.. I never knew your Kung Fu was so good..."),
                 _("Nicely done, {}! Now please unseal my acupuncture points so I can slay this villain with my own hands!").format(self.game.character),
                 _("Wait! Wait... What will you gain from doing this? What has he to offer you?"),
                 _("If you let me go and kill him instead, I will teach you Shadow Blade, Phantom Steps, and Huashan Sword Qi Gong!"),
                 _("You will become so much stronger!"),
                 _("Don't listen to him, {}! Come on; help me out already!").format(self.game.character)],
                ["Chen Wei", "Nie Tu", "Chen Wei", "Chen Wei", "Chen Wei", "Nie Tu"],
                [[_("Help Nie Tu."), partial(self.post_chen_wei_battle, 1)],
                 [_("Help Chen Wei."), partial(self.post_chen_wei_battle, 2)]]
            )

        elif choice == 1:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.chivalry += 30
            self.game.taskProgressDic["blind_sniper"] = 8
            self.game.generate_dialogue_sequence(
                None,
                "Tibetan Plains BG.png",
                [_("I'm not betraying my friend just to learn your lousy techniques..."),
                 _("*You quickly unseal Nie Tu's acupuncture points and hand him your sword.*"),
                 _("Here, Nie Tu. Go on; avenge your father."),
                 _("I lost to {}; I have nothing to say. Go ahead and kill me; I'm not going to beg you, weakling.").format(self.game.character),
                 _("If my father wasn't exhausted from his spar with the Blind Sniper, there's no way you could've killed him."),
                 _("Now, I will do to you what my father would've done many years ago."),
                 _("*A spurt of blood... The sound of metal cutting through flesh... Chen Wei's decapitated head... He is no more.*"),
                 _("Father, I have avenged you at last..."),
                 _("Thank you, {}. I owe my life to you.").format(self.game.character),
                 _("No problem, my friend. Now let's head back to the tower and discuss our plans for getting into the Empress's Palace.")],
                ["you", "Blank", "you", "Chen Wei", "Nie Tu", "Nie Tu", "Blank", "Nie Tu", "Nie Tu", "you"],
            )
            self.game.mapWin.deiconify()

        elif choice == 2:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.chivalry -= 50
            self.game.taskProgressDic["blind_sniper"] = -1000
            self.game.generate_dialogue_sequence(
                None,
                "Tibetan Plains BG.png",
                [_("Your chivalry -50!"),
                 _("Hmmm, sorry, Nie Tu. It seems like Chen Wei has the more attractive offer here..."),
                 _("You... you dishonorable piece of--"),
                 _("*Before he can finish his sentence, your blade travels across Nie Tu's neck. Blood splatters onto the sand and rocks as he gags and breathes his last.*"),
                 _("Hahahaha! Brilliant, young man! As promised, let me teach you..."),
                 _("*Chen Wei teaches you 'Shadow Blade', 'Phantom Steps', and 'Huashan Sword Qi Gong'.*")],
                ["Blank", "you", "Nie Tu", "Blank", "Chen Wei", "Blank"],
            )
            new_move = special_move(name=_("Shadow Blade"))
            self.game.learn_move(new_move)

            new_skill = skill(name=_("Phantom Steps"))
            self.game.learn_skill(new_skill)

            new_skill = skill(name=_("Huashan Sword Qi Gong"))
            self.game.learn_skill(new_skill)

            self.game.mapWin.deiconify()