mapping_file = open("DialogueTranslation.txt", "r", encoding="utf-8")
content = mapping_file.read()
mapping_file.close()

mapping = {}
content_split = content.strip().split("`")
for cs in content_split:
    if len(cs) > 0:
        cs = cs.strip()
        cs_split = cs.split("|||")
        mapping[cs_split[0].strip()] = cs_split[1].strip()


i = open("main.txt", "r", encoding="utf-8")
i_content = i.read()
i.close()

for en in mapping:
    ch = mapping[en]
    i_content = i_content.replace('_("{}")'.format(en), '_("{}")'.format(ch))


o = open("main_ch.txt", "w", encoding="utf-8")
o.write(i_content)
o.close()