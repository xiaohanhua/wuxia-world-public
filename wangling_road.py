from tkinter import *
from character import *
from item import *
from random import *
from helper_funcs import*
from functools import partial
from special_move import *
from skill import *
import time
import tkinter.messagebox as messagebox

_ = gettext


class Scene_wangling_road:

    def __init__(self, game):
        self.game = game
        self.game.wangling_road_win = Toplevel(self.game.mapWin)
        self.game.wangling_road_win.title(_("Wangling Road"))

        bg = PhotoImage(file="Wangling Road.png")
        self.w = bg.width()
        self.h = bg.height()

        self.canvas = Canvas(self.game.wangling_road_win, width=self.w, height=self.h)
        self.canvas.pack()
        self.canvas.create_image(0, 0, anchor=NW, image=bg)

        self.game.wangling_roadF1 = Frame(self.canvas)
        pic1 = PhotoImage(file="Blacksmith_icon_large.ppm")
        imageButton1 = Button(self.game.wangling_roadF1, command=self.clickedOnBlacksmith)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.game.wangling_roadF1, text=_("Blacksmith"))
        label.grid(row=1, column=0)
        self.game.wangling_roadF1.place(x=self.w * 1 // 4 - pic1.width() // 2, y=self.h // 4 - pic1.height() // 2)
        
        self.game.wangling_roadF2 = Frame(self.canvas)
        pic2 = PhotoImage(file="Shop Owner_icon_large.ppm")
        imageButton2 = Button(self.game.wangling_roadF2, command=self.clickedOnShopOwner)
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(self.game.wangling_roadF2, text=_("Shop Owner"))
        label.grid(row=1, column=0)
        self.game.wangling_roadF2.place(x=self.w * 3 // 4 - pic2.width() // 2, y=self.h // 4 - pic2.height() // 2)


        if self.game.taskProgressDic["wudang_task"] in list(range(-1,7)):
            self.game.wangling_roadF3 = Frame(self.canvas)
            pic3 = PhotoImage(file="Xuan Chen_icon_large.ppm")
            imageButton3 = Button(self.game.wangling_roadF3, command=self.clickedOnXuanChen)
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=pic3)
            label = Label(self.game.wangling_roadF3, text=_("Old Monk"))
            label.grid(row=1, column=0)
            self.game.wangling_roadF3.place(x=self.w * 3 // 4 - pic3.width() // 2, y=self.h // 2 + pic3.height() // 4)

        if self.game.taskProgressDic["ma_guobao"] in range(2,11):
            self.game.wangling_roadF4 = Frame(self.canvas)
            pic4 = PhotoImage(file="Ma Guobao_icon_large.ppm")
            imageButton4 = Button(self.game.wangling_roadF4, command=self.talk_to_ma_guobao)
            imageButton4.grid(row=0, column=0)
            imageButton4.config(image=pic4)
            label = Label(self.game.wangling_roadF4, text=_("Ma Guobao"))
            label.grid(row=1, column=0)
            self.game.wangling_roadF4.place(x=self.w * 1 // 4 - pic4.width() // 2, y=self.h // 2 + pic4.height() // 4)


        menu_frame = self.game.create_menu_frame(self.game.wangling_road_win)
        menu_frame.pack()
        self.game.wangling_road_win_F1 = Frame(self.game.wangling_road_win)
        self.game.wangling_road_win_F1.pack()

        self.game.wangling_road_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.wangling_road_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.wangling_road_win.winfo_width(), self.game.wangling_road_win.winfo_height()
        self.game.wangling_road_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.wangling_road_win.focus_force()
        self.game.wangling_road_win.mainloop()  ###


    def clickedOnXuanChen(self):
        if self.game.taskProgressDic["wudang_task"] == 6:
            self.game.generate_dialogue_sequence(
                self.game.wangling_road_win,
                "Wangling Road.png",
                [_("Xuan Chen!!!"),
                 _("Amitabha... Young man, are you talking to me?"),
                 _("Of course I'm talking to you! Who else could I be addressing?"),
                 _("Well, there are quite a few people nearby..."),
                 _("..... You have a point... but that's not going to save you today!"),
                 _("Come with me to Wudang and receive your punishment, you old thief!")],
                ["you", "Xuan Chen", "you", "Xuan Chen", "you", "you"]
            )
            opp = character(_("Xuan Chen"), min([15, self.game.level]),
                            sml=[special_move(_("Taichi Fist"), level=3),
                                 special_move(_("Shaolin Luohan Fist"), level=10),
                                 special_move(_("Shaolin Diamond Finger"), level=7)],
                            skills=[skill(_("Shaolin Inner Energy Technique"), level=10),
                                    skill(_("Shaolin Agility Technique"), level=10)])

            self.game.battleMenu(self.game.you, opp, "Wangling Road.png",
                                 "jy_wudang.mp3", fromWin=self.game.wangling_road_win,
                                 battleType="task", destinationWinList=[], postBattleCmd=self.postXuanChenBattle)

        elif self.game.taskProgressDic["ma_guobao"] == 20:
            self.game.generate_dialogue_sequence(
                self.game.wangling_road_win,
                "Wangling Road.png",
                [_("*Sigh*... I can't believe Master Ma was a fraud..."),
                 _("I've misled so many people... Amitabha... my sins....")],
                ["Xuan Chen", "Xuan Chen"]
            )
            
        elif self.game.taskProgressDic["ma_guobao"] >= 2:
            self.game.generate_dialogue_sequence(
                self.game.wangling_road_win,
                "Wangling Road.png",
                [_("I am eager to see you improve after learning from Master Ma!")],
                ["Xuan Chen"]
            )

        else:
            for widget in self.game.wangling_road_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.wangling_road_win_F1, text=_("Hello there!"), command=partial(self.talk_to_xuan_chen,1)).pack()
            if self.game.taskProgressDic["ma_guobao"] >= 0:
                Button(self.game.wangling_road_win_F1, text=_("Buy books (beginner/intermediate)"), command=partial(self.talk_to_xuan_chen,2)).pack()
                Button(self.game.wangling_road_win_F1, text=_("I want to learn something advanced"), command=partial(self.talk_to_xuan_chen,3)).pack()


    def talk_to_xuan_chen(self, choice):
        if choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.wangling_road_win,
                "Wangling Road.png",
                [_("Hello there!"),
                 _("Amitabha... How can I help you, young man?"),
                 _("I don't need anything; just saying hi."),
                 _("..... Hmmm, you seem young and fit. Do you practice martial arts?"),
                 _("Yes, I do, why do you ask?"),
                 _("Ahhh, it is your luck day, young man!"),
                 _("You see, I have special relations with some of the top martial arts masters from all over the country!"),
                 _("The Shaolin Abbot and I have been acquaintances for a couple of decades."),
                 _("And Zhang Tianjian, the leader of Wudang, often seeks my presence."),
                 _("What does that have to do with me? Are you going to teach me something?"), #Row 2
                 _("Ohohoho... I won't teach you personally, but I do have some manuals for sale..."),
                 _("Manuals? For sale?"),
                 _("Yep! If you are smart enough, you can learn Shaolin and Wudang kung-fu!"),
                 _("How did you get the manuals...? When did Shaolin and Wudang give out their manuals like that?"),
                 _("Like I explained earlier, I'm not just some random person... My relationship with them goes way back..."),
                 _("It just so happens that recently, Shaolin and Wudang expressed a desire for more people to be exposed to their kung-fu."),
                 _("Thus, they have asked for my help in seeking suitable candidates to learn their techniques."),
                 _("Candidates must be of good character and have a solid foundation; I believe you meet such criteria."),
                 _("So, what do you say?"), #Row 3
                 _("Uh, what manuals do you have? Anything good?"),
                 _("The manuals are all beginner/intermediate techniques. You can't expect them to share the top secrets..."),
                 _("However, if you demonstrate that you have good potential, I can recommend you to another master."),
                 _("He rarely accepts students, but if I put in a good word, he might consider."),
                 _("Hmmm, interesting... I'll think about it..."),
                 _("Ok, just talk to me again whenever you want to get started!")
                 ],
                ["you", "Xuan Chen", "you", "Xuan Chen", "you", "Xuan Chen", "Xuan Chen", "Xuan Chen", "Xuan Chen",
                 "you", "Xuan Chen", "you", "Xuan Chen", "you", "Xuan Chen", "Xuan Chen", "Xuan Chen", "Xuan Chen",
                 "Xuan Chen", "you", "Xuan Chen", "Xuan Chen", "Xuan Chen", "you", "Xuan Chen",]
            )

            if self.game.taskProgressDic["ma_guobao"] == -1:
                self.game.taskProgressDic["ma_guobao"] = 0
                Button(self.game.wangling_road_win_F1, text=_("Buy books (beginner/intermediate)"),
                       command=partial(self.talk_to_xuan_chen, 2)).pack()
                Button(self.game.wangling_road_win_F1, text=_("I want to learn something advanced"),
                       command=partial(self.talk_to_xuan_chen, 3)).pack()

        elif choice == 2:
            manuals = [_("Shaolin Luohan Fist Manual"), _("Shaolin Inner Energy Manual"), _("Taichi Fist Manual"),]
            for manual in manuals:
                if self.game.check_for_item(manual, 0):
                    pass
                else:
                    self.game.wangling_road_win.withdraw()
                    self.game.generate_dialogue_sequence(
                        None,
                        "Wangling Road.png",
                        [_("Hmmm, let's see... I think this one would be a good fit for ya..."),
                         _("I'll even give you a special discount since I like your personality.")
                        ],
                        ["Xuan Chen", "Xuan Chen"]
                    )
                    r_price = pickOne([1000, 1500, 1500, 2000])
                    response = messagebox.askquestion("", _("Purchase a copy of {} for {} Gold?").format(manual, r_price))
                    if response == "yes":
                        if self.game.inv[_("Gold")] >= r_price:
                            self.game.add_item(_("Gold"), -r_price)
                            self.game.add_item(manual)
                            self.game.generate_dialogue_sequence(
                                self.game.wangling_road_win,
                                "Wangling Road.png",
                                [_("Good choice, young man!"),
                                 _("I can't wait to see your progress!")
                                 ],
                                ["Xuan Chen", "Xuan Chen"]
                            )
                        else:
                            self.game.generate_dialogue_sequence(
                                self.game.wangling_road_win,
                                "Wangling Road.png",
                                [_("You don't have enough money, young man."),
                                 _("I can't be giving out free manuals... I need to donate the proceeds to Shaolin and Wudang.")],
                                ["Xuan Chen", "Xuan Chen"]
                            )

                    else:
                        self.game.generate_dialogue_sequence(
                            self.game.wangling_road_win,
                            "Wangling Road.png",
                            [_("Not quite ready yet? No worries; come again!")],
                            ["Xuan Chen"]
                        )

                    return

            self.game.generate_dialogue_sequence(
                self.game.wangling_road_win,
                "Wangling Road.png",
                [_("That's all the manuals I have for sale!"),
                 _("Talk to me when you are ready to move on to the advanced techniques...")],
                ["Xuan Chen", "Xuan Chen"]
            )


        elif choice == 3:
            condition1 = self.game.check_for_item(_("Shaolin Luohan Fist Manual"), 0)
            condition2 = self.game.check_for_item(_("Shaolin Inner Energy Manual"), 0)
            condition3 = self.game.check_for_item(_("Taichi Fist Manual"), 0)

            if condition1 and condition2 and condition3:
                if self.game.taskProgressDic["ma_guobao"] == 0:
                    self.game.taskProgressDic["ma_guobao"] = 1
                    self.game.wangling_road_win.withdraw()
                    self.game.generate_dialogue_sequence(
                        None,
                        "Wangling Road.png",
                        [_("Ah, so you really want to challenge yourself, eh?"),
                         _("Very good, young man. I admire your perseverance and ambition..."),
                         _("Well, are you going to introduce me to a master now?"),
                         _("Ahhh yes, I have a special opportunity for you to learn from the Master of Yunhuan Taichi: Ma Guobao!"),
                         _("You will receive one-on-one instruction on the essentials of the Yunhuan Taichi Fist."),
                         _("Wait, is this Master Ma affiliated with Wudang?"),
                         _("No no no, young man, though his father may have some relationship with the grandmasters of Wudang."),
                         _("Master Ma Guobao learned Yunhuan Taichi, a derivative of Wudang Taichi that's much more powerful than the original, from his father."),
                         _("Interesting... When do we get started?"), #Row 2
                         _("Oh! We can get started anytime, but you see, there's a small fee... for the tuition..."),
                         _("..... Alright, how much?"),
                         _("For one month, I will only charge you 3000 Gold. Usually, Master Ma does not accept anything under 5000!"),
                         _("But I'll put in a good word for you...")],
                        ["Xuan Chen", "Xuan Chen", "you", "Xuan Chen", "Xuan Chen", "you", "Xuan Chen", "Xuan Chen",
                         "you", "Xuan Chen", "you", "Xuan Chen", "Xuan Chen"]
                    )

                    response = messagebox.askquestion("", _("Pay 3000 Gold for tuition?"))
                    if response == "yes":
                        if self.game.inv[_("Gold")] >= 3000:
                            self.game.add_item(_("Gold"), -3000)
                            self.talk_to_ma_guobao()
                        else:
                            self.game.generate_dialogue_sequence(
                                self.game.wangling_road_win,
                                "Wangling Road.png",
                                [_("You don't have enough money, young man."),
                                 _("Do you think Master Ma has time to teach his techniques for free?")],
                                ["Xuan Chen", "Xuan Chen"]
                            )
                    else:
                        self.game.wangling_road_win.deiconify()

                elif self.game.taskProgressDic["ma_guobao"] == 1:
                    self.game.wangling_road_win.withdraw()
                    self.game.generate_dialogue_sequence(
                        None,
                        "Wangling Road.png",
                        [_("So, have you decided, young man?"),
                         _("Do you want to learn from Master Ma or not?")],
                        ["Xuan Chen", "Xuan Chen"]
                    )
                    response = messagebox.askquestion("", _("Pay 3000 Gold for tuition?"))
                    if response == "yes":
                        if self.game.inv[_("Gold")] >= 3000:
                            self.game.add_item(_("Gold"), -3000)
                            self.talk_to_ma_guobao()
                        else:
                            self.game.generate_dialogue_sequence(
                                self.game.wangling_road_win,
                                "Wangling Road.png",
                                [_("You don't have enough money, young man."),
                                 _("Do you think Master Ma has time to teach his techniques for free?")],
                                ["Xuan Chen", "Xuan Chen"]
                            )
                    else:
                        self.game.wangling_road_win.deiconify()
                            
                elif self.game.taskProgressDic["ma_guobao"] == 2:
                    self.game.generate_dialogue_sequence(
                        self.game.wangling_road_win,
                        "Wangling Road.png",
                        [_("You're a lucky one..."),
                         _("Master Ma doesn't accept students often. I hope you value your time with him.")],
                        ["Xuan Chen", "Xuan Chen"]
                    )


            else:
                self.game.generate_dialogue_sequence(
                    self.game.wangling_road_win,
                    "Wangling Road.png",
                    [_("Patience, young man..."),
                     _("You're not ready yet... You haven't even learned all the beginner/intermediate techniques yet!")],
                    ["Xuan Chen", "Xuan Chen"]
                )


    def talk_to_ma_guobao(self):
        if self.game.taskProgressDic["ma_guobao"] == 1:
            self.game.taskProgressDic["ma_guobao"] = 2
            self.game.generate_dialogue_sequence(
                self.game.wangling_road_win,
                "Wangling Road.png",
                [_("Wait here, young man. Let me see if Master Ma has time right now..."),
                 _("*30 minutes later*"),
                 _("Good day, young man! I heard you were interested in learning Yunhuan Taichi..."),
                 _("Ah, yes, Master Ma. My name is {}, and I would be honored to receive your instruction.").format(self.game.character),
                 _("Very good, very good... A polite, humble lad with ambition and determination..."),
                 _("I deem your worthy to become my student. First, allow me to introduce myself."),
                 _("My name is Ma Guobao, current leader of Yunhuan Taichi."),
                 _("I learned Yunhuan Taichi from my father Ma Yongzhi from the age of 7."),
                 _("Approximately 50 years ago, the Mongolians invaded the village where my father lived."), #Row 2
                 _("Using Yunhuan Taichi Fist, my father fought off several dozen Mongolian soldiers empty-handed with ease!"),
                 _("He then taught Yunhuan Taichi to the other young men in the village, and from then on"),
                 _("The Mongolians never dared to raid the village again."),
                 _("Unfortunately, none of those men passed on their knowledge to the next generation."),
                 _("As a result, I am the only person alive who is qualified to teach Yunhuan Taichi."),
                 _("You are quite fortunate, young man, to have this opportunity."),
                 _("I look forward to learning much from you, Master Ma!"), #Row 3
                 _("Very good, now go warm up and get ready mentally."),
                 _("Talk to me again when you wish to begin.")],
                ["Xuan Chen", "Blank", "Ma Guobao", "you", "Ma Guobao", "Ma Guobao", "Ma Guobao", "Ma Guobao",
                 "Ma Guobao", "Ma Guobao", "Ma Guobao", "Ma Guobao", "Ma Guobao", "Ma Guobao", "Ma Guobao",
                 "you", "Ma Guobao", "Ma Guobao"]
            )

            self.game.wangling_roadF4 = Frame(self.canvas)
            self.pic4 = PhotoImage(file="Ma Guobao_icon_large.ppm")
            imageButton4 = Button(self.game.wangling_roadF4, command=self.talk_to_ma_guobao)
            imageButton4.grid(row=0, column=0)
            imageButton4.config(image=self.pic4)
            label = Label(self.game.wangling_roadF4, text=_("Ma Guobao"))
            label.grid(row=1, column=0)
            self.game.wangling_roadF4.place(x=self.w * 1 // 4 - self.pic4.width() // 2, y=self.h // 2 + self.pic4.height() // 4)


        elif self.game.taskProgressDic["ma_guobao"] == 2:
            self.game.taskProgressDic["ma_guobao"] = 3
            self.game.generate_dialogue_sequence(
                self.game.wangling_road_win,
                "Wangling Road.png",
                [_("Excellent, let's get started..."),
                 _("Now, the 3 main principles of Yunhuan Taichi are: Receive, Divert, Counter."),
                 _("So what does that mean? Receive is when we anticipate and intercept the opponent's attack."),
                 _("Divert is redirecting the force of the attack so that it is no longer a threat to us."),
                 _("And finally, counter: we strike back."),
                 _("Let me demonstrate to you how this is done. Come here. Watch and listen carefully."),
                 _("*Ma Guobao begins to teach you the forms and stances of Yunhuan Taichi Fist.*"),
                 _("... And if... if I come... if I this come... Ok! Here come..."), #Row 2
                 _("Turn body, turn body... If...If, if, if... then I kick here... must try again... "),
                 _("*Though you are confused by what he is saying, you can more or less understand his movements.*"),
                 _("*After a couple of hours, you've finally learned Yunhuan Taichi Fist!*"),
                 _("Ok, let's take a break for now. You practice hard, ok?"),
                 _("Come to me if you have any questions...")],
                ["Ma Guobao", "Ma Guobao", "Ma Guobao", "Ma Guobao", "Ma Guobao", "Ma Guobao", "Blank",
                 "Ma Guobao", "Ma Guobao", "Blank", "Blank", "Ma Guobao", "Ma Guobao"]
            )

            new_move = special_move(_("Yunhuan Taichi Fist"))
            self.game.learn_move(new_move)


        elif self.game.taskProgressDic["ma_guobao"] >= 3:
            yhtf = [move for move in self.game.sml if move.name == _("Yunhuan Taichi Fist")][0]
            yhtf_level = yhtf.level

            if self.game.taskProgressDic["ma_guobao"] == 20:
                self.game.add_item(_("Gold"), 3000)
                self.game.generate_dialogue_sequence(
                    self.game.wangling_road_win,
                    "Wangling Road.png",
                    [_("Ooooof! Ahhhhh! You... you..."),
                     _("You fraud! I can't believe I thought you were an actual Taichi master!"),
                     _("Young man, we were only sparring. I withdrew my hand right before it was about to hit your face!"),
                     _("How can you take advantage of my merciful heart to strike me?"),
                     _("That is a dishonorable act!"),
                     _("You thick-skinned liar! Looks like I have to beat the crap out of you before you wake up!"),
                     _("Wait! Young man! Don't act rashly! I..."),
                     _("You're a disgrace to Taichi! Give me my money back!"), # Row 2
                     _("*You take back the 3000 Gold that you paid for tuition.*"),
                     _("You know what, I'm not gonna get my hands dirty. You're not worthy.")
                     ],
                    ["Ma Guobao", "you", "Ma Guobao", "Ma Guobao", "Ma Guobao", "you", "Ma Guobao",
                     "you", "Blank", "you"]
                )

                if self.game.taskProgressDic["wudang_task"] <= -1000: #if conflict w/ Wudang, then extort more money
                    self.game.add_item(_("Gold"), 5000)
                    self.game.generate_dialogue_sequence(
                        self.game.wangling_road_win,
                        "Wangling Road.png",
                        [_("Give me all the money you've made from scamming other people, and I'll let you go."),
                         _("What?! That's ridiculous! This is robb--"),
                         _("One more word out of you and I'll crack your skull!"),
                         _("*Ma Guobao hands over 5000 Gold reluctantly.*"),
                         _("There... That's all I have..."),
                         _("Now get out of my sight and don't let me see you ever again!"),
                         ],
                        ["you", "Ma Guobao", "you", "Blank", "Ma Guobao", "you"]
                    )
                    self.game.wangling_roadF4.place(x=-3000,y=-3000)

                else:
                    self.game.generate_dialogue_sequence(
                        self.game.wangling_road_win,
                        "Wangling Road.png",
                        [_("I'm taking you to Wudang. Master Zhang Tianjian will know what to do with you..."),
                         _("What?! That's ridiculous! You can't--"),
                         _("One more word out of you and I'll crack your skull!"),],
                        ["you", "Ma Guobao", "you"]
                    )
                    self.game.wangling_road_win.quit()
                    self.game.wangling_road_win.destroy()

                    self.game.generate_dialogue_sequence(
                        None,
                        "Wudang.png",
                        [_("Thank you, {}, for exposing this fraud!").format(self.game.character),
                         _("If not for you, he would've continued to take advantage of people and bring shame to Taichi."),
                         _("No problem, Master Zhang! It's a pleasure to contribute to the martial arts community."),
                         _("Very good, young man. I see great potential in you and a strong desire to learn."),
                         _("Let me teach you the real essence of Taichi as my way of thanking you.")],
                        ["Zhang Tianjian", "Zhang Tianjian", "you", "Zhang Tianjian", "Zhang Tianjian"]
                    )

                    if self.game.taskProgressDic["join_sect"] == 600 or self.game.chivalry + self.game.luck >= 250:
                        self.game.generate_dialogue_sequence(
                            None,
                            "Wudang.png",
                            [_("*Zhang Tianjian teaches you: Taichi 18 Forms*"), #Row 2
                             _("This, along with the Taichi Fist, form a very powerful combination."),
                             _("Practice diligently and use it for good."),
                             _("Wow! Thank you so much, Master Zhang! I won't disappoint you!")],
                            ["Blank", "Zhang Tianjian", "Zhang Tianjian", "you"]
                        )
                        new_skill = skill(_("Taichi 18 Forms"))
                        self.game.learn_skill(new_skill)
                    else:
                        self.game.generate_dialogue_sequence(
                            None,
                            "Wudang.png",
                            [_("*Zhang Tianjian teaches you: Pure Yang Qi Skill*"),  # Row 2
                             _("This, along with the Taichi Fist, form a very powerful combination."),
                             _("Practice diligently and use it for good."),
                             _("Wow! Thank you so much, Master Zhang! I won't disappoint you!")],
                            ["Blank", "Zhang Tianjian", "Zhang Tianjian", "you"]
                        )
                        new_skill = skill(_("Pure Yang Qi Skill"))
                        self.game.learn_skill(new_skill)
                    self.game.mapWin.deiconify()

            elif yhtf.level <= 3:
                self.game.generate_dialogue_sequence(
                    self.game.wangling_road_win,
                    "Wangling Road.png",
                    [_("Why are you idling? Keep practicing!"),
                     _("I can tell you haven't spent much time practicing the Yunhuan Taichi Fist.")],
                    ["Ma Guobao", "Ma Guobao"]
                )

            else:
                self.game.taskProgressDic["ma_guobao"] = yhtf_level

                if self.game.taskProgressDic["ma_guobao"] == 10:
                    self.game.generate_dialogue_sequence(
                        self.game.wangling_road_win,
                        "Wangling Road.png",
                        [_("What are you teaching me?!"),
                         _("Whatever it is, it's not useful at all in real combat!"),
                         _("Insolence! Is that the way to speak to your master?"),
                         _("It took me many years to learn Yunhuan Taichi; why do you seek instant gratification?"),
                         _("You don't have a firm grasp of Receive, Divert, and Counter."),
                         _("That is why you think it's ineffective. Plus, your inner energy is not strong enough."),
                         _("Let's spar then; I want to see your Yunhuan Taichi in actual combat."),
                         _("Fine, today I will show you why Yunhuan Taichi is the most powerful of all in real combat."),
                         _("But don't be afraid; even though there are certain attacks I'll do that can hurt you"), #Row 2
                         _("I won't go all out on you.")
                         ],
                        ["you", "you", "Ma Guobao", "Ma Guobao", "Ma Guobao", "Ma Guobao", "you", "Ma Guobao",
                         "Ma Guobao", "Ma Guobao"]
                    )
                    self.game.taskProgressDic["ma_guobao"] = 20
                    opp = character(_("Ma Guobao"), 1, attributeList=[200,200,20,20,10,10,10,10,10],
                                    sml=[special_move(_("Basic Punching Technique"), level=5)])

                    self.game.battleMenu(self.game.you, opp, "Wangling Road.png",
                                         "jy_cheerful3.mp3", fromWin=self.game.wangling_road_win,
                                         battleType="task", destinationWinList=[], postBattleCmd=self.talk_to_ma_guobao)


                elif self.game.taskProgressDic["ma_guobao"] >= 7:
                    self.game.generate_dialogue_sequence(
                        self.game.wangling_road_win,
                        "Wangling Road.png",
                        [_("Master Ma, something is still not right..."),
                         _("The Receive, Divert, and Counter isn't working as expected."),
                         _("Ok, let me show you."),
                         _("Come... If I in come.. Right to there..."),
                         _("But if this one... this one... punch... one, two, three..."),
                         _("*You have no idea what he's saying, but his movements make a bit of sense.*"),
                         _("Hmmm, let me keep practicing, thanks...")
                         ],
                        ["you", "you", "Ma Guobao", "Ma Guobao", "Ma Guobao", "Blank", "you"]
                    )

                elif self.game.taskProgressDic["ma_guobao"] >= 4:
                    self.game.generate_dialogue_sequence(
                        self.game.wangling_road_win,
                        "Wangling Road.png",
                        [_("Master Ma, I might be doing something wrong, but what you taught me isn't working too well in an actual fight."),
                         _("Ayyy, you are too young, too impatient!"),
                         _("You have only just learned the technique; how can you expect to be doing everything right?"),
                         _("Hmmm, good point. Mind teaching me again or giving me a refresher?"),
                         _("No problem..."),
                         _("*Ma Guobao demonstrates the form once again.*"),
                         _("Just like that! Just that simple! You see? You got it now?"),
                         _("Ah... yeah I think so... Let me practice some more...")
                         ],
                        ["you", "Ma Guobao", "Ma Guobao", "you", "Ma Guobao", "Blank", "Ma Guobao", "you"]
                    )



    def postXuanChenBattle(self, choice=0):
        if choice == 0:
            self.game.taskProgressDic["wudang_task"] = 7
            self.game.generate_dialogue_sequence(
                None,
                "Wangling Road.png",
                [_("Quit struggling, old man!"),
                 _("Wait! Don't... Don't take me there..."),
                 _("Shut up... I gotta go claim my reward now."),
                 _("Take it! Take the Taichi Fist Manual for yourself and act as though you never saw me! Just let me go..."),
                 ],
                ["you", "Xuan Chen", "you", "Xuan Chen"],
                [[_("Take him back to Wudang"), partial(self.postXuanChenBattle, 1)],
                 [_("Keep the Taichi Fist Manual for yourself and let him go"), partial(self.postXuanChenBattle, 2)]]
            )

        elif choice == 1:
            self.game.taskProgressDic["wudang_task"] = 8
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.generate_dialogue_sequence(
                None,
                "Wangling Road.png",
                [_("One more word from you and I'll chop your tongue off!"),
                 _("......")],
                ["you", "Xuan Chen"],
            )
            self.game.generate_dialogue_sequence(
                None,
                "Wudang.png",
                [_("I caught him! I caught the old monk Xuan Chen!"),
                 _("Nice work, {}!").format(self.game.character),
                 _("{}, you've been a great help lately, doing much good for the Martial Arts Community.").format(self.game.character),
                 _("I am very proud of you! I have spoken to Master Zhang Tianjian, and he has decided to reward you."),
                 _("Please come with me."),
                 _("*Li Kefan takes you to see Zhang Tianjian.*"),
                 _("{}, thank you for all you've done to help Wudang.").format(self.game.character), #Row 2
                 _("I will now teach you Wudang's most powerful techniques. Please use them for acts of chivalry."),
                 _("Thank you Master Zhang!!!")
                 ],
                ["you", "Li Kefan", "Li Kefan", "Li Kefan", "Li Kefan", "Blank",
                 "Zhang Tianjian", "Zhang Tianjian", "you"],
            )

            condition_1 = _("Taichi Fist") in [m.name for m in self.game.sml]
            condition_2 = _("Taichi Sword") in [m.name for m in self.game.sml]
            condition_3 = self.game.chivalry >= 100
            condition_4 = self.game.luck >= 120
            total = condition_1 + condition_2 + condition_3 + condition_4
            rewards = []
            if total >= 3:
                rewards.append(_("Taichi 18 Forms"))
                if not condition_1:
                    rewards.append(_("Taichi Fist"))
                elif not condition_2:
                    rewards.append(_("Taichi Sword"))
                else:
                    rewards.append(_("Hundred Seeds Pill"))
            else:
                if not condition_1:
                    rewards.append(_("Taichi Fist"))
                elif not condition_2:
                    rewards.append(_("Taichi Sword"))
                else:
                    rewards.append(_("Taichi 18 Forms"))

            rewards.sort(reverse=True)
            for reward in rewards:
                if reward == _("Hundred Seeds Pill"):
                    self.game.add_item(reward, 5)
                    messagebox.showinfo("", _("Received 'Hundred Seeds Pill' x 5!"))
                elif reward == _("Taichi 18 Forms"):
                    self.game.learn_skill(skill(reward))
                    messagebox.showinfo("", _("Zhang Tianjian taught you '{}'!").format(reward))
                else:
                    self.game.learn_move(special_move(reward))
                    messagebox.showinfo("", _("Zhang Tianjian taught you '{}'!").format(reward))

            self.game.mapWin.deiconify()


        elif choice == 2:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.chivalry -= 20
            messagebox.showinfo("", _("Your chivalry -20!"))
            self.game.wangling_roadF3.place(x=-3000, y=-3000)
            self.game.generate_dialogue_sequence(
                self.game.wangling_road_win,
                "Wangling Road.png",
                [_("Fine fine fine... just don't let me see you again..."),
                 _("Thank you, young man! Here's the manual!")],
                ["you", "Xuan Chen"],
            )
            self.game.add_item(_("Taichi Fist Manual"))
            messagebox.showinfo("", _("Received 'Taichi Fist Manual' x 1!"))


    def clickedOnBlacksmith(self):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        for widget in self.game.wangling_road_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.wangling_road_win_F1, text=_("Talk"), command=self.talkToBlacksmith).pack()
        Button(self.game.wangling_road_win_F1, text=_("Shop"), command=self.shopBlacksmith).pack()
        Button(self.game.wangling_road_win_F1, text=_("Forge"), command=self.forgeBlacksmith).pack()


    def talkToBlacksmith(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        self.game.generate_dialogue_sequence(
            self.game.wangling_road_win,
            "Wangling Road.png",
            [_("Hey there, buddy! Come check out the armors and weapons I've made!"),
             _("If you wish, I also take custom orders as long as you provide the material; there will also be a small cost, of course."),],
            ["Blacksmith", "Blacksmith"]
        )


    def shopBlacksmith(self):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        self.blacksmith_shop_prices = {
            _("Golden Chainmail"): 7000,
            _("Light Armor"): 2100,
            _("Moonlight Blade"): 6400,
            _("Steel Longsword"): 3500,
            _("Iron Sword"): 1500,
            _("Iron Dagger"): 240,
            _("Bronze Gauntlets"): 2400
        }
        self.game.selected_shop_item = None
        self.game.wangling_road_win.withdraw()
        self.game.blacksmith_menu_win = Toplevel(self.game.wangling_road_win)

        F1 = Frame(self.game.blacksmith_menu_win, borderwidth=2, relief=SUNKEN, padx=5, pady=5)
        F1.pack()

        self.game.shop_item_buttons = []
        self.game.shop_item_icons = []
        self.game.item_price_labels = []
        self.game.selected_shop_item = None
        self.game.selected_shop_item_index = None

        item_x = 0
        item_y = 0
        item_index = 0
        for item_name in self.blacksmith_shop_prices:
            item_icon = PhotoImage(file=_("i_{}.ppm").format(item_name))
            item_button = Button(F1, image=item_icon, command=partial(self.game.selectShopItem, item_name, item_index))
            item_button.image = item_icon
            item_button.grid(row=item_y, column=item_x)

            equipment = Equipment(item_name)
            item_description = equipment.description
            self.game.balloon.bind(item_button, item_description)
            self.game.shop_item_icons.append(item_icon)
            self.game.shop_item_buttons.append(item_button)

            price_label = Label(text="Price:" + str(self.blacksmith_shop_prices[item_name]), master=F1)
            price_label.grid(row=item_y + 1, column=item_x)
            self.game.item_price_labels.append(price_label)

            item_x += 1
            item_index += 1

        F2 = Frame(self.game.blacksmith_menu_win, padx=5, pady=5)
        F2.pack()

        Button(F2, text="Back", command=partial(self.game.winSwitch, self.game.blacksmith_menu_win,
                                                self.game.wangling_road_win)).grid(row=0, column=0)
        Label(F2, text="  ").grid(row=0, column=1)
        Button(F2, text="Buy", command=partial(self.game.buy_selected_shop_item, self.blacksmith_shop_prices)).grid(row=0,
                                                                                                                 column=2)
        Label(F2, text="  ").grid(row=0, column=3)

        self.game.shop_gold_amount_label = Label(F2, text=_("Your gold: ") + str(self.game.inv[_("Gold")]))
        self.game.shop_gold_amount_label.grid(row=0, column=4)

        self.game.blacksmith_menu_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.blacksmith_menu_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.blacksmith_menu_win.winfo_width(), self.game.blacksmith_menu_win.winfo_height()
        self.game.blacksmith_menu_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.blacksmith_menu_win.focus_force()
        self.game.blacksmith_menu_win.mainloop()  ###


    def forgeBlacksmith(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        self.blacksmith_forge_requirements = {
            _("Black Salamander"): {_("Black Iron Bar"):8,
                                    _("Steel Bar"):5,
                                    _("Bronze Bar"): 5,
                                    _("Tin Bar"): 5,
                                    _("Gold"):7000},
            _("Golden Snake"): {_("Steel Bar"):2,
                                _("Bronze Bar"): 5,
                                _("Tin Bar"): 8,
                                _("Gold"):4500},
            _("Heavy Adamantite Sword"): {_("Adamantite"):3,
                                          _("Black Iron Bar"):3,
                                          _("Steel Bar"):5,
                                          _("Bronze Bar"): 5,
                                          _("Tin Bar"): 5,
                                          _("Gold"):9000},
            _("Steel Platelegs"): {_("Steel Bar"):6,
                                _("Bronze Bar"): 3,
                                _("Tin Bar"): 5,
                                _("Gold"):3500},
            _("Steel Platebody"): {_("Steel Bar"): 10,
                                   _("Bronze Bar"): 5,
                                   _("Tin Bar"): 8,
                                   _("Gold"): 5000},
        }

        self.blacksmith_forge_gems = [_("Emerald"), _("Topaz"), _("Chrysocolla"), _("Amethyst"), _("Sunstone"), _("Dragon Tear")]

        self.game.wangling_road_win.withdraw()
        self.game.blacksmith_forge_win = Toplevel(self.game.wangling_road_win)

        self.blacksmith_forge_var = StringVar()
        self.blacksmith_forge_var.set(None)
        self.blacksmith_gem_var = StringVar()
        self.blacksmith_gem_var.set(None)

        self.game.blacksmith_forge_frame = Frame(self.game.blacksmith_forge_win)
        self.game.blacksmith_forge_frame.pack()
        self.game.blacksmith_forge_buttons_frame = Frame(self.game.blacksmith_forge_win)
        self.game.blacksmith_forge_buttons_frame.pack()

        self.game.forge_choice_frame = Frame(self.game.blacksmith_forge_frame, borderwidth=2, relief=SOLID)
        self.game.forge_choice_frame.pack(side=LEFT, fill=Y)
        Label(self.game.forge_choice_frame, text=_("Select an equipment to forge. Hover over the\nitem to view details and requirements.")).pack()
        self.game.forge_gem_frame = Frame(self.game.blacksmith_forge_frame, borderwidth=2, relief=SOLID)
        self.game.forge_gem_frame.pack(side=RIGHT, fill=Y)
        Label(self.game.forge_gem_frame, text=_("Choose a gem from your inventory to imbue into your\nequipment. Only 1 gem can be used each time.")).pack()

        self.blacksmith_forge_images = []
        for key, forge_req in self.blacksmith_forge_requirements.items():
            option_frame = Frame(self.game.forge_choice_frame)
            option_frame.pack(anchor=W)
            Radiobutton(option_frame, text="", variable=self.blacksmith_forge_var, value=key).pack(side=LEFT)
            option_image = PhotoImage(file=_("i_{}.ppm").format(key))
            option_label = Label(option_frame, image=option_image)
            option_label.pack(side=RIGHT)
            self.blacksmith_forge_images.append(option_image)

            eq = Equipment(key)
            req_str = _("\n\nForging Requirements: ")
            for req, amount in forge_req.items():
                req_str += "\n" + req + " x " + str(amount)
            self.game.balloon.bind(option_label, eq.description + req_str)



        for gem in self.blacksmith_forge_gems:
            gem_frame = Frame(self.game.forge_gem_frame)
            gem_frame.pack(anchor=W)
            gem_amount = 0
            try:
                gem_amount = self.game.inv[gem]
            except:
                pass
            gem_str = "[Owned: {}]".format(gem_amount)

            Radiobutton(gem_frame, text=gem_str, variable=self.blacksmith_gem_var, value=gem).pack(side=LEFT)
            gem_image = PhotoImage(file=_("i_{}.ppm").format(gem))
            gem_label = Label(gem_frame, image=gem_image)
            gem_label.pack(side=RIGHT)
            self.blacksmith_forge_images.append(gem_image)

            self.game.balloon.bind(gem_label, gem + "\n------------------------\n" + ITEM_DESCRIPTIONS[gem])

        # Add 'None' option for gem
        gem_frame = Frame(self.game.forge_gem_frame)
        gem_frame.pack(anchor=W)
        Radiobutton(gem_frame, text=_("None"), variable=self.blacksmith_gem_var, value="None").pack(side=LEFT)


        Button(self.game.blacksmith_forge_buttons_frame, text=_("Forge"), command=self.forge_item).pack(side=LEFT)
        Button(self.game.blacksmith_forge_buttons_frame, text=_("Back"),
               command=partial(self.game.winSwitch, self.game.blacksmith_forge_win, self.game.wangling_road_win)).pack(side=RIGHT)


        self.game.blacksmith_forge_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.blacksmith_forge_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.blacksmith_forge_win.winfo_width(), self.game.blacksmith_forge_win.winfo_height()
        self.game.blacksmith_forge_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.blacksmith_forge_win.focus_force()
        self.game.blacksmith_forge_win.mainloop()


    def forge_item(self):
        target_item = self.blacksmith_forge_var.get()
        target_gem = self.blacksmith_gem_var.get()
        if not target_item:
            messagebox.showinfo("", _("Please select an equipment to forge."))
            return
        else:
            requirements_met = True
            for req, amount in self.blacksmith_forge_requirements[target_item].items():
                try:
                    if self.game.inv[req] < amount:
                        requirements_met = False
                        break
                except:
                    requirements_met = False
                    break

            if target_gem != 'None':
                try:
                    if self.game.inv[target_gem] < 1:
                        requirements_met = False
                except:
                    requirements_met = False

            if not requirements_met:
                messagebox.showinfo("", _("You don't have the necessary items to forge this."))
                return
            else:
                upgrade = ""
                r = random()
                if target_gem == _("Emerald") and r <= .5:
                    upgrade = " [Enhanced]"
                elif target_gem == _("Topaz"):
                    if r <= .2:
                        upgrade = " [Rare]"
                    elif r <= .5:
                        upgrade = " [Enhanced]"
                elif target_gem == _("Chrysocolla"):
                    if r <= .75:
                        upgrade = " [Rare]"
                elif target_gem == _("Amethyst"):
                    if r <= .75:
                        upgrade = " [Ultra Rare]"
                elif target_gem == _("Sunstone"):
                    if r <= .25:
                        upgrade = " [Ultra Rare]"
                    elif r <= .5:
                        upgrade = " [Rare]"
                    elif r <= .8:
                        upgrade = " [Enhanced]"
                elif target_gem == _("Dragon Tear"):
                    if r <= .3:
                        upgrade = " [Ultra Rare]"
                    elif r <= .6:
                        upgrade = " [Rare]"
                    else:
                        upgrade = " [Enhanced]"

                forged_item = target_item + upgrade
                for req, amount in self.blacksmith_forge_requirements[target_item].items():
                    self.game.add_item(req, -amount)
                if target_gem != 'None':
                    self.game.add_item(target_gem, -1)
                self.game.add_item(forged_item)
                self.game.currentEffect = "sfx_smithing.ogg"
                self.game.startSoundEffectThread()
                time.sleep(.5)
                messagebox.showinfo("", _("The blacksmith successfully forges {} for you!").format(forged_item))
                self.game.winSwitch(self.game.blacksmith_forge_win, self.game.wangling_road_win)


    def clickedOnShopOwner(self):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        for widget in self.game.wangling_road_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.wangling_road_win_F1, text=_("Talk"), command=self.talkToShopOwner).pack()
        Button(self.game.wangling_road_win_F1, text=_("Shop"), command=self.shopShopOwner).pack()


    def talkToShopOwner(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        self.game.generate_dialogue_sequence(
            self.game.wangling_road_win,
            "Wangling Road.png",
            [_("Hi there! Are you interested in my goods?"),
             _("They hand-crafted from the finest raw material in the land!"),],
            ["Shop Owner", "Shop Owner"]
        )

    def shopShopOwner(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        self.crafted_goods_shop_prices = {
            _("Wool Boots"): 1800,
            _("Leather Boots"): 1800,
            _("Leather Body"): 1500,
            _("Leather Pants"): 1500,
            _("Ninja Suit"): 2500,
            _("Silk Fan"): 450,
            _("Buddha Beads"): 650,
            _("Blood Diamond Pendant"): 10000,
        }
        self.game.selected_shop_item = None
        self.game.wangling_road_win.withdraw()
        self.game.crafted_goods_menu_win = Toplevel(self.game.wangling_road_win)

        F1 = Frame(self.game.crafted_goods_menu_win, borderwidth=2, relief=SUNKEN, padx=5, pady=5)
        F1.pack()

        self.game.shop_item_buttons = []
        self.game.shop_item_icons = []
        self.game.item_price_labels = []
        self.game.selected_shop_item = None
        self.game.selected_shop_item_index = None

        item_x = 0
        item_y = 0
        item_index = 0
        for item_name in self.crafted_goods_shop_prices:
            item_icon = PhotoImage(file=_("i_{}.ppm").format(item_name))
            item_button = Button(F1, image=item_icon, command=partial(self.game.selectShopItem, item_name, item_index))
            item_button.image = item_icon
            item_button.grid(row=item_y, column=item_x)

            equipment = Equipment(item_name)
            item_description = equipment.description
            self.game.balloon.bind(item_button, item_description)
            self.game.shop_item_icons.append(item_icon)
            self.game.shop_item_buttons.append(item_button)

            price_label = Label(text="Price:" + str(self.crafted_goods_shop_prices[item_name]), master=F1)
            price_label.grid(row=item_y + 1, column=item_x)
            self.game.item_price_labels.append(price_label)

            item_x += 1
            item_index += 1

        F2 = Frame(self.game.crafted_goods_menu_win, padx=5, pady=5)
        F2.pack()

        Button(F2, text="Back", command=partial(self.game.winSwitch, self.game.crafted_goods_menu_win,
                                                self.game.wangling_road_win)).grid(row=0, column=0)
        Label(F2, text="  ").grid(row=0, column=1)
        Button(F2, text="Buy", command=partial(self.game.buy_selected_shop_item, self.crafted_goods_shop_prices)).grid(row=0,
                                                                                                                 column=2)
        Label(F2, text="  ").grid(row=0, column=3)

        self.game.shop_gold_amount_label = Label(F2, text=_("Your gold: ") + str(self.game.inv[_("Gold")]))
        self.game.shop_gold_amount_label.grid(row=0, column=4)

        self.game.crafted_goods_menu_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.crafted_goods_menu_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.crafted_goods_menu_win.winfo_width(), self.game.crafted_goods_menu_win.winfo_height()
        self.game.crafted_goods_menu_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.crafted_goods_menu_win.focus_force()
        self.game.crafted_goods_menu_win.mainloop()  ###