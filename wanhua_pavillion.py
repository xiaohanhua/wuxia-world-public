from tkinter import *
from character import *
from item import *
from random import *
from helper_funcs import *
from functools import partial
from special_move import *
from skill import *
import time
import tkinter.messagebox as messagebox

_ = gettext


class Scene_wanhua_pavillion:

    def __init__(self, game):
        self.game = game
        if self.game.taskProgressDic["wanhua_pavillion"] == -1:
            self.game.taskProgressDic["wanhua_pavillion"] = 0
            self.game.generate_dialogue_sequence(
                None,
                "Wanhua Pavillion BG.png",
                [_("Hey there, young hero~ Welcome to Wanhua Pavillion, a place filled with beautiful women and good wine~"),
                 _("Our sisters here will treat you like an emperor and make you forget all your troubles~"),
                 _("Just let me know what type of lady you prefer, and I, A-Xiu, will ensure your needs are met~"),
                 _("Alternatively, if you want a nice drink instead, you can talk to A-Qi, our boss~"),
                 _("*You notice a distinct fragrance coming from her body. It makes you feel relaxed and slightly drunk.*"),
                 _("How should I address you, handsome?~"),
                 _("Hi, A-Xiu... my name is {}.").format(self.game.character),
                 _("*Your cheeks begin to burn as she continues to gaze into your eyes.*"),
                 _("{}... I like that name... Well, I'm here if you need anything~").format(self.game.character)],
                ["A-Xiu", "A-Xiu", "A-Xiu", "A-Xiu", "Blank", "A-Xiu", "you", "Blank", "A-Xiu"]
            )

        elif self.game.taskProgressDic["wanhua_pavillion"] == 20:
            self.game.generate_dialogue_sequence(
                None,
                "Wanhua Pavillion BG.png",
                [_("Looks like Wanhua Pavillion shut down after Xian-er's death...")],
                ["you"]
            )

        self.game.wanhua_pavillion_win = Toplevel(self.game.mapWin)
        self.game.wanhua_pavillion_win.title(_("Wanhua Pavillion"))

        bg = PhotoImage(file="Wanhua Pavillion BG.png")
        self.w = bg.width()
        self.h = bg.height()

        self.canvas = Canvas(self.game.wanhua_pavillion_win, width=self.w, height=self.h)
        self.canvas.pack()
        self.canvas.create_image(0, 0, anchor=NW, image=bg)

        if self.game.taskProgressDic["a_xiu"] == -1 and self.game.taskProgressDic["wanhua_pavillion"] != 20: #make sure A-Xiu didn't join you as a partner
            self.game.wanhua_pavillionF1 = Frame(self.canvas)
            pic1 = PhotoImage(file="A-Xiu_icon_large.ppm")
            imageButton1 = Button(self.game.wanhua_pavillionF1, command=self.clickedOnAXiu)
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            label = Label(self.game.wanhua_pavillionF1, text=_("A-Xiu"))
            label.grid(row=1, column=0)
            self.game.wanhua_pavillionF1.place(x=self.w * 1 // 4 - pic1.width() // 2, y=self.h // 4 - pic1.height() // 1.5)

        if self.game.taskProgressDic["wanhua_pavillion"] != 20:
            self.game.wanhua_pavillionF2 = Frame(self.canvas)
            pic2 = PhotoImage(file="A-Qi_icon_large.ppm")
            imageButton2 = Button(self.game.wanhua_pavillionF2, command=self.clickedOnAQi)
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            label = Label(self.game.wanhua_pavillionF2, text=_("A-Qi"))
            label.grid(row=1, column=0)
            self.game.wanhua_pavillionF2.place(x=self.w * 3 // 4 - pic2.width() // 2, y=self.h // 4 - pic2.height() // 1.5)

        if self.game.taskProgressDic["blind_sniper"] == 1:
            self.game.wanhua_pavillionF3 = Frame(self.canvas)
            pic3 = PhotoImage(file="Scrub_icon_large.ppm")
            imageButton3 = Button(self.game.wanhua_pavillionF3, command=self.clickedOnScrub)
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=pic3)
            label = Label(self.game.wanhua_pavillionF3, text=_("Scrub"))
            label.grid(row=1, column=0)
            self.game.wanhua_pavillionF3.place(x=self.w * 3 // 4 - pic3.width() // 2, y=self.h // 2 + pic3.height() // 8)

        if self.game.taskProgressDic["wanhua_pavillion"] != 20:
            self.game.wanhua_pavillionF4 = Frame(self.canvas)
            pic4 = PhotoImage(file="Cai Wuqiong_icon_large.ppm")
            imageButton4 = Button(self.game.wanhua_pavillionF4, command=self.clickedOnCaiWuqiong)
            imageButton4.grid(row=0, column=0)
            imageButton4.config(image=pic4)
            label = Label(self.game.wanhua_pavillionF4, text=_("Cai Wuqiong"))
            label.grid(row=1, column=0)
            self.game.wanhua_pavillionF4.place(x=self.w * 1 // 4 - pic4.width() // 2, y=self.h // 2 + pic4.height() // 8)

        menu_frame = self.game.create_menu_frame(self.game.wanhua_pavillion_win)
        menu_frame.pack()
        self.game.wanhua_pavillion_win_F1 = Frame(self.game.wanhua_pavillion_win)
        self.game.wanhua_pavillion_win_F1.pack()

        self.game.wanhua_pavillion_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.wanhua_pavillion_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.wanhua_pavillion_win.winfo_width(), self.game.wanhua_pavillion_win.winfo_height()
        self.game.wanhua_pavillion_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.wanhua_pavillion_win.focus_force()
        self.game.wanhua_pavillion_win.mainloop()  ###


    def clickedOnAXiu(self):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        for widget in self.game.wanhua_pavillion_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.wanhua_pavillion_win_F1, text=_("Spar (A-Xiu)"), command=self.sparAXiu).pack()
        Button(self.game.wanhua_pavillion_win_F1, text=_("Talk"), command=partial(self.talkToAXiu,1)).pack()
        if self.game.taskProgressDic["wanhua_pavillion"] == 10:
            Button(self.game.wanhua_pavillion_win_F1, text=_("Buy 'Rainbow Fragrance' x 10 (costs 1000 gold)."), command=partial(self.talkToAXiu,2)).pack()
        else:
            Button(self.game.wanhua_pavillion_win_F1, text=_("Request a lady"), command=partial(self.talkToAXiu,2)).pack()

        #if self.game.taskProgressDic["cai_wuqiong"] == 10:
        #    Button(self.game.wanhua_pavillion_win_F1, text=_("Flirt"), command=partial(self.talkToAXiu,3)).pack()


    def sparAXiu(self):
        if self.game.taskProgressDic["wanhua_pavillion"] < 5:
            self.game.generate_dialogue_sequence(
                self.game.wanhua_pavillion_win,
                "Wanhua Pavillion BG.png",
                [_("I'm a bit busy right now. Maybe another time?")],
                ["A-Xiu"]
            )

        else:
            self.game.generate_dialogue_sequence(
                self.game.wanhua_pavillion_win,
                "Wanhua Pavillion BG.png",
                [_("I'm in a good mood today. Let's go!")],
                ["A-Xiu"]
            )

            opp = character(_("A-Xiu"), 12,
                            sml=[special_move(_("Fairy Plucking Flowers"), level=10)],
                            skills=[skill(_("Basic Agility Technique"), level=10),
                                    skill(_("Dragonfly Dance"), level=10)],
                            preferences=[3,1,3,1,2])

            self.game.battleMenu(self.game.you, opp, "Dragon Sect.png", postBattleSoundtrack="jy_pipayu.mp3",
                                 fromWin=self.game.wanhua_pavillion_win, battleType="training",
                                 postBattleCmd=self.postAXiuSpar)


    def postAXiuSpar(self):

        if self.game.taskProgressDic["wanhua_pavillion"] == 5 and self.game.taskProgressDic["blind_sniper"] >= 2:
            self.game.taskProgressDic["wanhua_pavillion"] = 6
            self.game.wanhua_pavillion_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Wanhua Pavillion BG.png",
                [_("(Hmmm... her arms and legs definitely seem real. I knew it! A-Xiu is not the Three-Tailed Fox!)"),
                 _("(I need to tell Scrub about this!)"),],
                ["you", "you"]
            )

            self.game.generate_dialogue_sequence(
                None,
                "scrub_house.ppm",
                [_("Hey, Scrub! I told you A-Xiu is innocent! I had a quick spar with her and felt her hands and feet."),
                 _("They're definitely real and not prosthetic!"),
                 _("Hmmmm, really... Let me go confirm..."),
                 _("Hey wait, where --"),
                 _("*Before you can stop him, Scrub has already rushed out of his house.*")],
                ["you", "you", "Scrub", "you", "Blank"]
            )

            self.game.generate_dialogue_sequence(
                None,
                "Wanhua Pavillion BG.png",
                [_("I demand a refund immediately!"),
                 _("Hey there, customer. May I ask why and for what?"),
                 _("Your wine is poisoned! I threw up after I got home yesterday, and the only thing I ate or drank that day was your wine!"),
                 _("You must be mistaken, sir. We've never had anything besides positive reviews of our wine."),
                 _("Plus, I don't recall serving you yesterday..."),
                 _("I don't care! Give me a refund now!!!"),
                 _("Sir, I can't just give you a refund without --"),
                 _("So you want me to smash a few tables to convince you otherwise?"),
                 _("A-Qi, don't bother argue with him. This guy is just here to cause trouble."),
                 _("So what if I am? What are you going to do about it?"),
                 _("Stop it, Scrub! I already told you A-Xiu is innocent!"),
                 _("Don't worry, {}. This guy's no match for me...").format(self.game.character)],
                ["Scrub", "A-Qi", "Scrub", "A-Qi", "A-Qi", "Scrub", "A-Qi", "Scrub", "A-Xiu", "Scrub", "you", "A-Xiu"]
            )

            self.game.currentEffect = "jy_nv3.mp3"
            self.game.startSoundEffectThread()
            time.sleep(.25)

            self.game.currentEffect = "sfx_finger.mp3"
            self.game.startSoundEffectThread()
            time.sleep(.25)

            self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
            self.game.startSoundEffectThread()
            time.sleep(.25)

            self.game.currentEffect = "sfx_male_voice.mp3"
            self.game.startSoundEffectThread()
            time.sleep(.25)

            self.game.currentEffect = "sfx_attack_missed2.mp3"
            self.game.startSoundEffectThread()
            time.sleep(.25)

            self.game.currentEffect = "jy_nv1.mp3"
            self.game.startSoundEffectThread()
            time.sleep(.25)

            year_var = calculate_month_day_year(self.game.gameDate)["Year"] + 9

            self.game.generate_dialogue_sequence(
                None,
                "Wanhua Pavillion BG.png",
                [_("Fairy Plucking Flowers... Where did you learn that technique? What's your relation to the Dexterous Fairy?"),
                 _("Not bad... You recognize the move..."),
                 _("My apologies for the drama; I just wanted to test you out. It seems that you are not the Three-Tailed Fox after all."),
                 _("Just because I'm pretty doesn't mean I'm a fox!"),
                 _("Sorry about that; I just wanted to be safe... Please accept my humble apology."),
                 _("Wait, what is all this about fairies and flowers?"),
                 _("The 'Fairy Plucking Flowers' technique is a specialty of the Dexterous Fairy, the most skilled thief in the world."),
                 _("To my knowledge, she has not appeared publicly for at least {} years...").format(year_var),
                 _("That's because my mother... she... she was murdered {} years ago when I was 10 years old...").format(year_var-2),
                 _("Murdered?? Who did it? I'll help you avenge her!"),
                 _("I don't know... All I know is that it was a man that my mother fell in love with."),
                 _("They always met in secret, so I never got to see him."), #Row 2
                 _("But what about your father...?"),
                 _("I don't know who he is either. My mom said that he died around the time I was born, but I've never even seen his gravestone."),
                 _("Whenever I ask questions about him, she seems to deflect the topic right away..."),
                 _("Hmmm, strange... By the way, how did you know that your mother's lover killed her?"),
                 _("Because that scumbag had a wife; my mother was only his mistress..."),
                 _("My mom gave her heart to him and fully trusted him; she even gifted him her Amethyst Necklace, which is one of the most expensive items she's stolen."),
                 _("When she found out that he had a wife and a family of his own, she was furious and killed his wife."),
                 _("The last time I saw her, my mother told me she was going to confront him about this. That night, she never returned."),
                 _("A-Xiu, I want to help you get to the bottom of this... Why don't you come back to Scrub's house with me?"),
                 _("That way, either of us can update you right away when we find a clue.")],
                ["Scrub", "A-Xiu", "Scrub", "A-Xiu", "Scrub", "you", "Scrub", "Scrub", "A-Xiu", "you", "A-Xiu",
                 "A-Xiu", "you", "A-Xiu", "A-Xiu", "Scrub", "A-Xiu", "A-Xiu", "A-Xiu", "A-Xiu", "you", "you"]
            )

            if self.game.taskProgressDic["castrated"] == 1 or self.game.taskProgressDic["wife_intimacy"] >= 0 or (self.game.taskProgressDic["eagle_sect"] >= 11 and self.game.taskProgressDic["eagle_sect"] != 1000):
                self.game.generate_dialogue_sequence(
                    None,
                    "Wanhua Pavillion BG.png",
                    [_("Thank you, {}, but I can't let A-Qi take all the burden of running Wanhua Pavillion.").format(self.game.character),
                     _("If you find something, please let me know. I'll be here waiting for you."),
                     _("Since you've been so nice to me and are willing to help me, let me teach you this in case it comes in handy."),
                     _("*A-Xiu spends some time teaching you the 'Fairy Plucking Flowers' technique.*")],
                    ["A-Xiu", "A-Xiu", "A-Xiu", "Blank"]
                )
                new_move = special_move("Fairy Plucking Flowers")
                self.game.learn_move(new_move)
                self.game.taskProgressDic["wanhua_pavillion"] = 7
                self.game.wanhua_pavillion_win.deiconify()

            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Wanhua Pavillion BG.png",
                    [_("Thank you, {}. I'll do as you say.").format(self.game.character),
                     _("Alright, Scrub, hope you don't mind an extra guest?"),
                     _("No problem at all! The more the merrier!"),
                     _("*The 3 of you head back to Scrub's house.*")],
                    ["A-Xiu", "you", "Scrub", "Blank"]
                )
                self.game.taskProgressDic["wanhua_pavillion"] = 200
                self.game.taskProgressDic["cai_wuqiong"] = -100
                self.game.taskProgressDic["a_xiu"] = 0
                self.game.wanhua_pavillion_win.quit()
                self.game.wanhua_pavillion_win.destroy()
                self.game.return_to_scrub_house()

        elif self.game.taskProgressDic["cai_wuqiong"] == 11:
            self.game.taskProgressDic["cai_wuqiong"] = 12
            self.game.generate_dialogue_sequence(
                self.game.wanhua_pavillion_win,
                "Wanhua Pavillion BG.png",
                [_("*A-Xiu charges at you with her right hand extended towards your neck, 3 of her fingers stretched out and 2 curled inward.*"),
                 _("*You immediately recognize this move: 'Pruning the Dead Branches'.*"),
                 _("*Rotating counter-clockwise while simultaneously lowering your body, you perform a sweep, hoping to counter her attack.*"),
                 _("*At the last moment, A-Xiu leaps into the air to dodge your sweep.*"),
                 _("*Seeing an opportunity, you use the 'Brushing Aside Petals to Expose the Ovule' move from the 'Fairy Plucking Flowers' technique.*"),
                 _("*Your fingers brush along her ankle and foot in a quick but gentle motion. Swish~ Her shoe slips off her foot and ends up in your hands.*"),
                 _("*Still in mid-air, A-Xiu swings her other leg towards your face. You block it with ease and repeat the same move, gaining possession of her other shoe as well.*"),
                 _("Hey! You pervert..."),
                 _("Well, you are the one who taught me that move hehe..."),
                 _("Huh, using my own moves against me? Well, seeing that you learned well, I guess you can keep my shoes as a reward~"),
                 _("*A-Xiu gives you a shy smile and, before you can respond, heads upstairs, returning a moment later with a new pair of shoes.*")],
                ["Blank", "Blank", "Blank", "Blank", "Blank", "Blank", "Blank", "A-Xiu", "you", "A-Xiu", "Blank"]
            )

        else:
            self.game.wanhua_pavillion_win.deiconify()


    def talkToAXiu(self, choice):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        if choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.wanhua_pavillion_win,
                "Wanhua Pavillion BG.png",
                [_("So, what do you do here?"),
                 _("I'm the co-hostess. I welcome the customers and match them up with girls who are suitable to their tastes."),
                 _("I also know a few moves or two, so if you see anyone trying to cause trouble, let me know."),
                 _("You don't provide any other services then?"),
                 _("You naughty~ That's not part of my responsibility. The other sisters here are much better at that type of work.")],
                ["you", "A-Xiu", "A-Xiu", "you", "A-Xiu"]
            )

        elif choice == 2:
            if self.game.taskProgressDic["wanhua_pavillion"] == 10:
                if self.game.check_for_item(_("Gold"), 1000):
                    self.game.inv[_("Gold")] -= 1000
                    self.game.add_item(_("Rainbow Fragrance"), 10)
                    messagebox.showinfo("", _("You purchased 'Rainbow Fragrance' x 10 for 1000 gold!"))
                else:
                    messagebox.showinfo("", _("You don't have enough gold!"))

            else:
                self.game.wanhua_pavillion_win.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Wanhua Pavillion BG.png",
                    [_("A-Xiu, find me a pretty woman, would ya?"),
                     _("That'll be 1000 gold for the night.")],
                    ["you", "A-Xiu"],
                    [[_("Pay 1000 gold."), partial(self.talkToAXiu, 21)],
                     [_("Maybe later."), partial(self.talkToAXiu, 22)]]
                )

        elif choice == 21:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            if self.game.check_for_item(_("Gold"), 1000):
                self.game.inv[_("Gold")] -= 1000
                self.game.healthMax += 20
                self.game.staminaMax += 20
                self.game.gameDate += 1
                if self.game.taskProgressDic["wanhua_pavillion"] < 5:
                    self.game.taskProgressDic["wanhua_pavillion"] += 1

                if self.game.taskProgressDic["castrated"] == 1:
                    self.game.generate_dialogue_sequence(
                        self.game.wanhua_pavillion_win,
                        "Wanhua Pavillion BG.png",
                        [_("*Taking you by the arm, A-Xiu leads you to one of the rooms upstairs.*"),
                         _("Have fun~~"),
                         _("*You enter the room to find an attractive woman waiting for you on the bed.*"),
                         _("*Due to your disability after castration, you spend the night drinking tea and having long conversations with her.*"),],
                        ["Blank", "A-Xiu", "Blank", "Blank"]
                    )

                elif self.game.taskProgressDic["wife_intimacy"] >= 0:
                    self.game.taskProgressDic["wife_intimacy"] = 0
                    self.game.chivalry -= 80
                    self.game.generate_dialogue_sequence(
                        self.game.wanhua_pavillion_win,
                        "Wanhua Pavillion BG.png",
                        [_("*Taking you by the arm, A-Xiu leads you to one of the rooms upstairs.*"),
                         _("Have fun~~"),
                         _("*You enter the room to find an attractive woman waiting for you on the bed.*"),
                         _("*You spend the night gratifying the desires of your flesh.*"),
                         _("Your chivalry -80...")],
                        ["Blank", "A-Xiu", "Blank", "Blank", "Blank"]
                    )

                else:
                    self.game.chivalry -= 40
                    self.game.generate_dialogue_sequence(
                        self.game.wanhua_pavillion_win,
                        "Wanhua Pavillion BG.png",
                        [_("*Taking you by the arm, A-Xiu leads you to one of the rooms upstairs.*"),
                         _("Have fun~~"),
                         _("*You enter the room to find an attractive woman waiting for you on the bed.*"),
                         _("*You spend the night gratifying the desires of your flesh.*"),
                         _("Your chivalry -40...")],
                        ["Blank", "A-Xiu", "Blank", "Blank", "Blank"]
                    )


            else:
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("Come on, {}~ You didn't bring enough money this time.").format(self.game.character),
                     _("Our ladies can't spend the night with you for free...")],
                    ["A-Xiu", "A-Xiu"]
                )

        elif choice == 22:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.wanhua_pavillion_win.deiconify()

        elif choice == 3:
            self.game.wanhua_pavillion_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Wanhua Pavillion BG.png",
                [_("A-Xiu, find me a pretty woman, would ya?"),
                 _("That'll be 1000 gold for the night.")],
                ["you", "A-Xiu"]
            )




    def clickedOnAQi(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        for widget in self.game.wanhua_pavillion_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.wanhua_pavillion_win_F1, text=_("Talk"), command=partial(self.talkToAQi,1)).pack()
        Button(self.game.wanhua_pavillion_win_F1, text=_("Order Wine"), command=partial(self.talkToAQi,2)).pack()


    def talkToAQi(self, choice):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if choice == 1:
            self.game.generate_dialogue_sequence(
                self.game.wanhua_pavillion_win,
                "Wanhua Pavillion BG.png",
                [_("Dear customer~ You really ought to try our Heavenly Bliss wine, only 300 gold per jug~"),
                 _("It will free you from the worries of this world~")],
                ["A-Qi", "A-Qi"]
            )

        elif choice == 2:
            self.game.wanhua_pavillion_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Wanhua Pavillion BG.png",
                [_("I'd like a jug of Heavenly Bliss please!")],
                ["you"]
            )

            if self.game.check_for_item(_("Gold"), 300):
                self.game.inv[_("Gold")] -= 300
                if self.game.taskProgressDic["wanhua_pavillion"] < 5:
                    self.game.taskProgressDic["wanhua_pavillion"] += 1

                if (self.game.taskProgressDic["wanhua_pavillion"]-2)/5 > random() and _("Drunken Bliss") not in [s.name for s in self.game.skills]:
                    new_skill = skill(name=_("Drunken Bliss"), level=10)
                    self.game.learn_skill(new_skill)
                    self.game.generate_dialogue_sequence(
                        self.game.wanhua_pavillion_win,
                        "Wanhua Pavillion BG.png",
                        [_("*As you enjoy cup after cup of the Heavenly Bliss, you feel as though you are floating on top of the clouds in the heavens.*"),
                         _("*There is not a thing in this world that can ruin  your mood now...*"),
                         _("Ahhhhh... truly there is no better wine than this...")],
                        ["Blank", "Blank", "you"]
                    )
                    messagebox.showinfo("", _("You gained a new skill: Drunken Bliss!"))

                else:
                    self.game.generate_dialogue_sequence(
                        self.game.wanhua_pavillion_win,
                        "Wanhua Pavillion BG.png",
                        [_("*As you enjoy cup after cup of the Heavenly Bliss, you feel as though you are floating on top of the clouds in the heavens.*"),
                         _("*There is not a thing in this world that can ruin  your mood now...*"),
                         _("Ahhhhh... truly there is no better wine than this...")],
                        ["Blank", "Blank", "you"]
                    )


            else:
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("Looks like you didn't bring enough gold this time, customer~"),
                     _("We can't be giving out our exquisite wine for free now~")],
                    ["A-Qi", "A-Qi"]
                )


    def clickedOnScrub(self):
        self.game.wanhua_pavillion_win.quit()
        self.game.wanhua_pavillion_win.destroy()
        self.game.taskProgressDic["blind_sniper"] = 2
        self.game.generate_dialogue_sequence(
            None,
            "Wanhua Pavillion BG.png",
            [_("Scrub! I've been looking for you! I need your help with--"),
             _("..."),
             _("..."),
             _("What are you doing here???"),
             _("Shhhh... I'm here to inv--"),
             _("Wait, why are YOU here???"),
             _("Like I said, I was looking for you... I didn't think I'd find you in a place like this though..."),
             _("... It's not what it looks like..."),
             _("Says the person who built his house next to a brothel..."),
             _("Like I've been trying to tell you, I'm here to--"),
             _("Ugh... There's too many people here. Come, I'll explain..."),
             _("*You follow Scrub into a remote forest area...*")],
            ["you", "you", "Scrub", "you", "Scrub", "Scrub", "you", "Scrub", "you", "Scrub", "Scrub", "Blank"]
        )

        self.game.generate_dialogue_sequence(
            None,
            "forest.ppm",
            [_("Alright, go ahead. I'm listening."),
             _("I'm investigating a case..."),
             _("Case?"),
             _("Yup, I'm trying to track down one of the 3 Great Evils."),
             _("Never heard of them..."),
             _("In the past decade, the infamous 3 Great Evils have become a stench to the righteous sects of the Martial Arts Community."),
             _("Ranked at #1 is Chen Wei, the Bloodthirsty Leopard, who is known for stealing moves from various sects."),
             _("He will ambush his victims and once they're down, sever their tendons one at a time."),
             _("If his victims reveal the place where the manuals are hidden or explain the core essence of the moves, he will grant them a quick death."),
             _("Otherwise, he will continue to torture them until they bleed to death."),
             _("It is rumored that he is a private disciple of Lu Xifeng, or at least has received some sort of instruction from him at one point."),
             _("The Second is the Three-Tailed Fox, whose true identity is unknown. She is the one I'm currently trying to hunt down."), #Row 2
             _("Many young men have died at her hands, but strangely enough, she only kills married men but spares the wife."),
             _("Some say that the Three-Tailed Fox was born with a deformity, causing one of her limbs to be a stub."),
             _("This resulted in her peers ridiculing her and no man willing to marry her."),
             _("Thus, she became jealous of young couples and began a rampage."),
             _("Of course, this is just a rumor. Whether she actually has such a deformity, I do not know."),
             _("And who's the 3rd one?"),
             _("Ah, this guy you might know. Wu Hua Que, the Flower Staining Sparrow."),
             _("He stalks young women and uses his charm to gain their trust before finding an opportunity to rape them."),
             _("Though his combat abilities aren't great, he is very agile, making him difficult to catch."),
             _("Ok, back to the main point... I still don't get why you would be at a brothel."), #Row 3
             _("Do you really think the Three-Tailed Fox would be there?"),
             _("Well, one commonality between the victims is that they visited Wanhua Pavillion not long before their deaths."),
             _("They all died at their homes though, so they must've been followed after leaving Wanhua Pavillion."),
             _("Doesn't that mean it can't be one of the girls working there? I doubt they have time to follow anyone home and return without being noticed."),
             _("Plus, surely none of the pros-- er, 'Ladies of the Night' can have a deformity and still work there."),
             _("That's what I can't seem to figure out either..."),
             _("As for the deformity part, the girls who serve customers, of course not... but based on my investigation, 2 of the women there don't."),
             _("Oh? You mean A-Xiu?"),
             _("Yes, and also A-Qi, the boss lady who owns the place... I have observed their arms, and both seem to be real."),
             _("I'm not too sure about their legs though... Maybe if we --"),
             _("No you don't! Don't you even think about getting near A-Xiu, you pervert!"),
             _("........ Would you quit that?? What I'm saying is we need to create a situation where they need to run or fight."),
             _("Then we can observe their movements and see if it's natural..."), #Row 4
             _("Still sounds like you're up to no good..."),
             _("*Sigh* ... You know what, forget it. I'll leave you to investigate then. If you find anything, come report to me."),
             _("Now, your turn. Why were you looking for me?"),
             _("Ah, right... So, long story short, I need to sneak into the Empress' Chambers to help her reunite with the Blind Sniper."),
             _("Obviously, even though I am very powerful, getting past thousands of guards doesn't sound like a safe option."),
             _("Hmmmm... You're right. It's unrealistic to do this by force... In that case, I recommend you go to Babao Tower to find Nie Tu."),
             _("He should be able to help you. However, I heard lately that he and his buddies may be in some sort of conflict."),
             _("You'll have to help them resolve that if it still hasn't been settled yet..."),
             _("Got it, thanks Scrub!"),
             _("Ok, good luck. I'm heading back to my house now. Bye...")],
            ["you", "Scrub", "you", "Scrub", "you", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub",
             "Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "Scrub", "you", "Scrub", "Scrub", "Scrub",
             "you", "you", "Scrub", "Scrub", "you", "you", "Scrub", "Scrub", "you", "Scrub", "Scrub", "you", "Scrub",
             "Scrub", "you", "Scrub", "Scrub", "you", "you", "Scrub", "Scrub", "Scrub", "you", "Scrub"]
        )

        self.game.mapWin.deiconify()


    def clickedOnCaiWuqiong(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if self.game.taskProgressDic["cai_wuqiong"] == -1:
            self.game.taskProgressDic["cai_wuqiong"] = 0
            self.game.generate_dialogue_sequence(
                self.game.wanhua_pavillion_win,
                "Wanhua Pavillion BG.png",
                [_("Pretty ladies~ Excellent wine~ Ahhhh~~ What else can a man ask for?"),
                 _("Oh, hey there, friend! Come here, come here, don't be timid."),
                 _("Hello... And you are...?"),
                 _("What?? You don't know who I am? Surely the overwhelming scent of my extravagant wealth should give you a hint."),
                 _("Nope..."),
                 _("Oh dear, have you been living under a rock? Well then, allow me to introduce myself..."),
                 _("I am Cai Wuqiong, the richest man in the world!"),
                 _("If you ever need money, just come to me. I can give you some errands."),
                 _("And if you want, I can show you the pleasures of this world! Dozens of women by your side and endless supplies of food and wine!"),#Row 2
                 _("Can you imagine that? Of course you can't because you're dirt poor, but that's ok..."),
                 _("You can watch from the side and be envious of me~"),
                 _(".........")],
                ["Cai Wuqiong", "Cai Wuqiong", "you", "Cai Wuqiong", "you", "Cai Wuqiong", "Cai Wuqiong", "Cai Wuqiong",
                 "Cai Wuqiong", "Cai Wuqiong", "Cai Wuqiong", "you"]
            )

        elif self.game.taskProgressDic["cai_wuqiong"] == -100:
            self.game.generate_dialogue_sequence(
                self.game.wanhua_pavillion_win,
                "Wanhua Pavillion BG.png",
                [_("Heeeey there, pr... pretty lady.... c-come and s.... siiit next to me..."),
                 _("Let's have a drink a... and then... h.... have some fuuuun tonight... *hic*"),
                 _("(He looks terribly drunk...)")],
                ["Cai Wuqiong", "Cai Wuqiong", "you"]
            )
            return

        elif self.game.taskProgressDic["cai_wuqiong"] == 5 and self.game.taskProgressDic["blind_sniper"] >= 2:
            self.talkToCaiWuqiong(3)

        for widget in self.game.wanhua_pavillion_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.wanhua_pavillion_win_F1, text=_("Talk"), command=partial(self.talkToCaiWuqiong,1)).pack()
        Button(self.game.wanhua_pavillion_win_F1, text=_("Who's your favorite girl here?"), command=partial(self.talkToCaiWuqiong,2)).pack()
        Button(self.game.wanhua_pavillion_win_F1, text=_("Do you have any tasks for me?"), command=partial(self.talkToCaiWuqiong, 3)).pack()
        if self.game.taskProgressDic["cai_wuqiong"] == 13:
            Button(self.game.wanhua_pavillion_win_F1, text=_("I want to see Xian-er."), command=partial(self.talkToCaiWuqiong, 4)).pack()


    def talkToCaiWuqiong(self, choice):
        if choice == 1:
            r = randrange(4)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("I'm so rich that I throw away 50000 kg of gold each week just for fun!"),
                     _("It's so satisfying to stand at a high place and watch people down below scramble to pick up the money."),
                     _("You should come and watch me when you get a chance.")],
                    ["Cai Wuqiong", "Cai Wuqiong", "Cai Wuqiong"]
                )
            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("People will do anything if you have enough money..."),
                     _("They'll even eat the crumbs that fall off of my table as long as I offer them a few pieces of gold.")],
                    ["Cai Wuqiong", "Cai Wuqiong"]
                )
            else:
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("An advice for you, my friend... Don't get married."),
                     _("A wife is just a burden and an unwanted responsibility. When you're rich like me, you can get all the women you want!"),
                     _("Why bother with marriage?")],
                    ["Cai Wuqiong", "Cai Wuqiong", "Cai Wuqiong"]
                )

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.wanhua_pavillion_win,
                "Wanhua Pavillion BG.png",
                [_("Out of the ones I've slept with, I'd rate Red Flower and White Swan the highest."),
                 _("But they pale in comparison to Wanhua Pavillion's top-rated lady: Xian-er."),
                 _("Xian-er only serves customers once a week at a price that few can afford: 20000 Gold per night!"),
                 _("This is a small number for me, of course, so I've reserved her for the next 5 years, hahahaha!"),
                 _("What will you do once the 5 years is up?"),
                 _("By then, she'll be a bit old for my liking... Then it'll be time to switch targets..."),
                 _("You know, A-Xiu is pretty cute; too bad she is just a co-hostess..."),
                 _("If she doesn't provide that type of service and is not enticed by money, it's no use if I'm rich."), #Row 2
                 _("Maybe that's part of the reason why I find her so enchanting..."),
                 _("Well, you know what they say... The one that you can't get is always the best...")],
                ["Cai Wuqiong", "Cai Wuqiong", "Cai Wuqiong", "Cai Wuqiong", "you", "Cai Wuqiong", "Cai Wuqiong",
                 "Cai Wuqiong", "Cai Wuqiong", "you"]
            )

        elif choice == 3:
            self.game.wanhua_pavillion_win.withdraw()
            #print(self.game.taskProgressDic["cai_wuqiong"], self.game.taskProgressDic["blind_sniper"])
            if self.game.taskProgressDic["cai_wuqiong"] < 5 or self.game.taskProgressDic["blind_sniper"] < 2:
                if randrange(2) == 0:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Wanhua Pavillion BG.png",
                        [_("Hmmm... I'm quite bored, so let's play a game."),
                         _("I've got a few pieces of gold on me. Let me throw them at your face."),
                         _("Whatever I throw at you is yours to keep. You can stop whenever you want. How's that?")],
                        ["Cai Wuqiong", "Cai Wuqiong", "Cai Wuqiong"],
                        [[_("Let's go! Come at me!"), partial(self.cai_wuqiong_task,1)],
                         [_("Stop, I've had enough."), partial(self.cai_wuqiong_task,0)]]
                    )
                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Wanhua Pavillion BG.png",
                        [_("This chair is getting a bit uncomfortable."),
                         _("Why don't you kneel down on the ground and let me use you as a human chair for a bit."),
                         _("I'll pay you.")],
                        ["Cai Wuqiong", "Cai Wuqiong", "Cai Wuqiong"],
                        [[_("Sure, go ahead."), partial(self.cai_wuqiong_task, 2)],
                         [_("No way!"), partial(self.cai_wuqiong_task, 0)]]
                    )

            elif self.game.taskProgressDic["cai_wuqiong"] == 10:
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("You can do it! I have confidence in you! Remember, your reward is waiting~")],
                    ["Cai Wuqiong"]
                )

            elif self.game.taskProgressDic["cai_wuqiong"] == 12:
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("Here you go! I managed to get her shoes for you, hehehe..."),
                     _("Brilliant! You are a genius, my friend! Xian-er is yours for 1 night, whenever you'd like! Just let me know!"),
                     _("Oh, and here's 30000 Gold as promised!")],
                    ["you", "Cai Wuqiong", "Cai Wuqiong"]
                )
                self.game.add_item(_("A-Xiu's Shoes"), -1)
                self.game.add_item(_("Gold"), 30000)
                self.game.taskProgressDic["cai_wuqiong"] = 13

            elif self.game.taskProgressDic["cai_wuqiong"] == 5 and self.game.taskProgressDic["blind_sniper"] >= 2:
                self.game.taskProgressDic["cai_wuqiong"] = 10
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("You seem like a man who's willing to do anything for money."),
                     _("This is why I'm going to entrust you with a special task."),
                     _("If you succeed, I will reward you greatly!"),
                     _("Sure, what is it?"),
                     _("I've had my eyes on A-Xiu for a long time, and I really want her..."),
                     _("I know it's unlikely I'll ever get her body, but perhaps... perhaps you can steal a piece of clothing from her?"),
                     _("If I can just have an article of clothing that she has worn with her pleasant scent on it, that'll be enough."),
                     _("You're asking me to do something very dishonorable... What do I get in return?"), #Row 2
                     _("Ahhh, that's the important question!"),
                     _("30000 pieces of gold. On top of that, I'll even let you spend a night with Xian-er~"),
                     _("Deal! I'll think of a way ehehehe...")],
                    ["Cai Wuqiong", "Cai Wuqiong", "Cai Wuqiong", "you", "Cai Wuqiong", "Cai Wuqiong", "Cai Wuqiong",
                     "you", "Cai Wuqiong", "Cai Wuqiong", "you"]
                )

            else:
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("Nothing at the moment, my friend!")],
                    ["Cai Wuqiong"]
                )

        elif choice == 4:
            for widget in self.game.wanhua_pavillion_win_F1.winfo_children():
                widget.destroy()

            self.game.taskProgressDic["cai_wuqiong"] = 14
            self.game.wanhua_pavillion_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Wanhua Pavillion BG.png",
                [_("Sure thing, my friend!"),
                 _("*Ahem* A-Xiu~ "),
                 _("Yes, Young Master Cai, what can I do for you?"),
                 _("Is Xian-er available tonight?"),
                 _("Yes, she is; would you like me to tell her that you're requesting her presence tonight?"),
                 _("Please do tell her to prepare herself, but not for me..."),
                 _("Oh?"),
                 _("Hehehe, that's right... This little buddy of mine is going to have quite an enjoyable evening, hohohoho!"), #Row 2
                 _("Is that so...? Ah, haha... I'll let Xian-er know right away~"),
                 _("Thank you, brother Cai!"),
                 _("Don't mention it, brother! After all, you did give me a priceless gift hehehehe..."),
                 _("*Cai Wuqiong pulls out A-Xiu's shoes, which had been tucked into his shirt, and begins to caress them.*"),
                 _("Ahhhh~~... A-Xiu always smells so wonderful... I wonder what type of perfume she uses..."),
                 _("(Hmmm, she does carry a unique scent... I remember it from when I first met her...)"),
                 _("Hehe, don't mind me... You go ahead and get yourself all washed up and looking nice for tonight... Enjoy~ Ehehehe..."),
                 _("*Later that night...*")],
                ["Cai Wuqiong", "Cai Wuqiong", "A-Xiu", "Cai Wuqiong", "A-Xiu", "Cai Wuqiong", "A-Xiu",
                 "Cai Wuqiong", "A-Xiu", "you", "Cai Wuqiong", "Blank", "Cai Wuqiong", "you", "Cai Wuqiong", "Blank"]
            )

            self.game.generate_dialogue_sequence(
                None,
                "Wanhua Pavillion BG.png",
                [_("Xian-er is ready, {}~ Come with me...").format(self.game.character),
                 _("*You follow A-Xiu upstairs to an inner room on the Northern end of the pavillion.*"),
                 _("Here you go~ Good night~"),
                 _("Hehe, thanks, A-Xiu..."),
                 _("*You slowly push open the door, trying your best to appear like a gentleman.*"),
                 _("{}, is it?").format(self.game.character),
                 _("Ah... you must be Xian-er... Your beauty is stunning, to say the least.")],
                ["A-Xiu", "Blank", "A-Xiu", "you", "Blank", "Xian-er", "you"]
            )

            if self.game.taskProgressDic["castrated"] == 1:
                self.game.taskProgressDic["wanhua_pavillion"] = 100
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("*sigh*... It's too bad... I'm no longer a complete man. Otherwise..."),
                     _("Wh-what??! You mean, you..."),
                     _("Yep... I'm no different from a eunuch..."),
                     _("Th-then, why... why did you..."),
                     _("Ah, why did I request to see you? I was curious about your beauty, that's all. No other intentions."),
                     _("I guess this also gives you a night to take a break."),
                     _("You... you are quite strange, but thank you... I guess..."),
                     _("*You spend the rest of the night sipping on tea and chatting with Xian-er.*")],
                    ["you", "Xian-er", "you", "Xian-er", "you", "you", "Xian-er", "you"]
                )

            elif self.game.taskProgressDic["wife_intimacy"] > -1:
                self.game.taskProgressDic["wanhua_pavillion"] = 8

                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("Haha~ You have a sweet mouth... I like it..."),
                     _("I can do much more than speak flattering words with it..."),
                     _("Oh really~ Then why don't you show me?~"),
                     _("*Xian-er slowly draws near you, gazing straight into your eyes as she walks forward.*"),
                     _("*You are completely mesmerized by her. Soon, her face is only inches away from yours.*"),
                     _("*You pick up the familiar scent that you detected on A-Xiu's body. Your body begins to feel warm and relaxed.*"),
                     _("*You can no longer take your eyes off of the woman in front of you. You want her now...*"),
                     _("*... Content not suitable for minors ...*"),
                     _("*By the time you wake up the next morning, Xian-er is already gone.*")],
                    ["Xian-er", "you", "Xian-er", "Blank", "Blank", "Blank", "Blank", "Blank", "Blank"],
                )

                self.game.healthMax += 100
                self.game.staminaMax += 100
                self.game.gameDate += 1

            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Wanhua Pavillion BG.png",
                    [_("And you are a very handsome man~ You must have a beautiful wife as well."),
                     _("Why come to a place like this?"),],
                    ["Xian-er", "Xian-er"],
                    [[_("You are far prettier than my wife!"), partial(self.talkToCaiWuqiong, 41)],
                     [_("I don't have a wife."), partial(self.talkToCaiWuqiong, 42)]]
                )


        elif choice == 41:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.taskProgressDic["wanhua_pavillion"] = 8

            self.game.generate_dialogue_sequence(
                self.game.wanhua_pavillion_win,
                "Wanhua Pavillion BG.png",
                [_("Haha~ You have a sweet mouth... I like it..."),
                 _("I can do much more than speak flattering words with it..."),
                 _("Oh really~ Then why don't you show me?~"),
                 _("*Xian-er slowly draws near you, gazing straight into your eyes as she walks forward.*"),
                 _("*You are completely mesmerized by her. Soon, her face is only inches away from yours.*"),
                 _("*You pick up the familiar scent that you detected on A-Xiu's body. Your body begins to feel warm and relaxed.*"),
                 _("*You can no longer take your eyes off of the woman in front of you. You want her now...*"),
                 _("*... Content not suitable for minors ...*"),
                 _("*By the time you wake up the next morning, Xian-er is already gone.*")],
                ["Xian-er", "you", "Xian-er", "Blank", "Blank", "Blank", "Blank", "Blank", "Blank"],
            )

            self.game.healthMax += 100
            self.game.staminaMax += 100
            self.game.gameDate += 1


        elif choice == 42:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.taskProgressDic["wanhua_pavillion"] = 100

            self.game.generate_dialogue_sequence(
                self.game.wanhua_pavillion_win,
                "Wanhua Pavillion BG.png",
                [_("Wow, I'm surprised! In that case, please come visit often~"),
                 _("I will take care of your needs..."),
                 _("*Xian-er slowly draws near you, gazing straight into your eyes as she walks forward.*"),
                 _("*You are completely mesmerized by her. Soon, her face is only inches away from yours.*"),
                 _("*You pick up the familiar scent that you detected on A-Xiu's body. Your body begins to feel warm and relaxed.*"),
                 _("*You can no longer take your eyes off of the woman in front of you. You want her now...*"),
                 _("*... Content not suitable for minors ...*"),
                 _("*By the time you wake up the next morning, Xian-er is already gone.*")],
                ["Xian-er", "Xian-er", "Blank", "Blank", "Blank", "Blank", "Blank", "Blank"],
            )

            self.game.healthMax += 100
            self.game.staminaMax += 100
            self.game.gameDate += 1



    def cai_wuqiong_task(self, task):
        if task == 1:
            if self.game.health <= 100:
                self.game.dialogueWin.quit()
                self.game.dialogueWin.destroy()
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("You look like you're about to faint... We'd better stop for today."),
                     _("I don't want to accidentally kill you...")],
                    ["Cai Wuqiong", "Cai Wuqiong"]
                )

            else:
                self.game.taskProgressDic["cai_wuqiong"] += 1
                if self.game.taskProgressDic["cai_wuqiong"] >= 5:
                    self.game.taskProgressDic["cai_wuqiong"] = 5
                self.game.currentEffect = "sfx_SGSHitSoundLoud.mp3"
                self.game.startSoundEffectThread()
                print(_("Cai Wuqiong throws some gold and hits you in the face. You take 50 damage and earn Gold x 20."))
                self.game.health -= 50
                self.game.add_item(_("Gold"), 20)

        elif task == 2:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            if self.game.stamina <= 300:
                self.game.generate_dialogue_sequence(
                    self.game.wanhua_pavillion_win,
                    "Wanhua Pavillion BG.png",
                    [_("You look like you're about to faint... We'd better stop for today."),
                     _("I don't want to accidentally kill you...")],
                    ["Cai Wuqiong", "Cai Wuqiong"]
                )

            else:
                self.game.taskProgressDic["cai_wuqiong"] += 1
                if self.game.taskProgressDic["cai_wuqiong"] >= 5:
                    self.game.taskProgressDic["cai_wuqiong"] = 5
                print(_("Cai Wuqiong pays you 100 Gold to be his human chair for a while..."))
                self.game.stamina -= 300
                self.game.add_item(_("Gold"), 100)
                self.game.wanhua_pavillion_win.deiconify()

        else:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            self.game.wanhua_pavillion_win.deiconify()