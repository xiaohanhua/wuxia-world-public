from tkinter import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
import time
import tkinter.messagebox as messagebox


_ = gettext


class Scene_wooden_house:

    def __init__(self, game):
        self.game = game
        self.game.gameDate += 20
        self.game.taskProgressDic["feng_pan"] = 50
        self.game.restore(0, 0, full=True)
        self.game.wooden_house_win = Toplevel(self.game.mapWin)
        self.days = 0

        bg = PhotoImage(file="Wooden House.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.wooden_house_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Yang Kai_icon_large.ppm")
        imageButton1 = Button(F1, command=self.clickedOnYangKai)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("Yang Kai"))
        label.grid(row=1, column=0)
        F1.place(x=w // 3 - pic1.width() // 2, y= h//3 - pic1.height())

        F2 = Frame(canvas)
        pic2 = PhotoImage(file="Feng Xinyi_icon_large.ppm")
        imageButton2 = Button(F2, command=self.clickedOnFengXinyi)
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(F2, text=_("Feng Xinyi"))
        label.grid(row=1, column=0)
        F2.place(x=w*2//3 - pic2.width() // 2, y= h//3 - pic2.height())

        F3 = Frame(canvas)
        pic3 = PhotoImage(file="Chop Wood_icon_large.ppm")
        imageButton3 = Button(F3, command=partial(self.task, "Wood"))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        label = Label(F3, text=_("Chop Wood"))
        label.grid(row=1, column=0)
        F3.place(x=w // 3 - pic3.width() // 2, y=h // 2 + pic3.height()//4)

        F4 = Frame(canvas)
        pic4 = PhotoImage(file="Fetch Water_icon_large.ppm")
        imageButton4 = Button(F4, command=partial(self.task, "Water"))
        imageButton4.grid(row=0, column=0)
        imageButton4.config(image=pic4)
        label = Label(F4, text=_("Fetch Water"))
        label.grid(row=1, column=0)
        F4.place(x=w * 2 // 3 - pic4.width() // 2, y=h // 2 + pic4.height()//4)

        self.game.wooden_house_win_F1 = Frame(self.game.wooden_house_win)
        self.game.wooden_house_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.wooden_house_win,
                                                 options=["Profile", "Inv", "Settings"])
        menu_frame.pack()

        self.game.wooden_house_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.wooden_house_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.wooden_house_win.winfo_width(), self.game.wooden_house_win.winfo_height()
        self.game.wooden_house_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.wooden_house_win.focus_force()
        self.game.wooden_house_win.mainloop()


    def clickedOnYangKai(self):
        for widget in self.game.wooden_house_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.wooden_house_win_F1, text=_("Learn"),
               command=self.learn_from_yang_kai).grid(row=1, column=0)
        Button(self.game.wooden_house_win_F1, text=_("Talk"),
               command=self.talk_to_yang_kai).grid(row=2, column=0)


    def learn_from_yang_kai(self):
        self.days += 1
        r = randrange(2)
        if r == 0:
            self.game.generate_dialogue_sequence(
                self.game.wooden_house_win,
                "Wooden House.png",
                [_("Ok, {}, today we will learn how to better harness the Qi in your body.").format(self.game.character),
                 _("Pay close attention..."),
                 _("*After a day of Qi cultivation, your stamina upper limit increased by 50!*")],
                ["Yang Kai", "Yang Kai", "Blank"]
            )
            self.game.staminaMax += 50
        elif r == 1:
            self.game.generate_dialogue_sequence(
                self.game.wooden_house_win,
                "Wooden House.png",
                [_("Hmmm, let me help you unblock your Chakras to improve the flow of Qi throughout your body."),
                 _("Sit still and relax. Don't move..."),
                 _("As Yang Kai begins helping you unblock your Chakras, your health upper limit increases by 50!")],
                ["Yang Kai", "Yang Kai", "Blank"]
            )
            self.game.healthMax += 50


        if self.days >= 20:
            self.talk_to_yang_kai(1)


    def talk_to_yang_kai(self, choice=None):
        if choice==1:
            if self.game.taskProgressDic["feng_pan"] == 70:
                self.game.wooden_house_win.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Wooden House.png",
                    [_("Alright, you two, it's been fun, but I need to get on with my own mission soon."),
                     _("You're leaving?"),
                     _("Tomorrow morning..."),
                     _("....."),
                     _("What about you two?"),
                     _("I'll be wandering around to seek some adventures of my own. Xinyi, any plans?"),
                     _("I... I don't know..."),
                     _("Well, maybe we can find you a master to continue teaching you martial arts..."),
                     _("I guess..."), #Row 2
                     _("You're still young. There are a lot of doors open for you. No need to stress about it."),
                     _("Yeah, we'll figure something out. Let's get some rest now..."),
                     _("*Later that night, you wake up from your sleep to the sound of footsteps.*"),  #Row 3
                     _("Elder Yang? Where are you going?"),
                     _("I... I'm going to leave now..."),
                     _("Wait, I thought you were leaving tomorrow morning..."),
                     _("....."),
                     _("Are you trying to hide... from Xinyi? You're avoiding her on purpose, aren't you?"),
                     _("{}, this can't go on anymore. I know she has feelings for me, but...").format(self.game.character),
                     _("Then what about you?"),
                     _("Huh?"), #Row 4
                     _("Do you have feelings for her?"),
                     _("I... I... You don't understand, {}...").format(self.game.character),
                     _("I don't understand?? You think you're doing this for her good, don't you?"),
                     _("You think it's impossible for you two to be together so you want her to forget you more easily."),
                     _("You think that by disappearing without a word, she will somehow hate you for it?"),
                     _("It doesn't work like that! It'll cause even more pain because there's no closure, no resolution!"),
                     _("You'll end up causing more damage to her because you are too afraid to confront her and express your feelings!"),
                     _("I... How did you..."), #Row 5
                     _("How I know isn't important. Now listen."),
                     _("If you want to leave tonight, fine, that's up to you!"),
                     _("But I am not letting you go without a proper farewell!")],
                    ["Yang Kai", "you", "Yang Kai", "Feng Xinyi", "Yang Kai", "you", "Feng Xinyi", "you",
                     "Feng Xinyi", "Yang Kai", "you",
                     "Blank", "you", "Yang Kai", "you", "Yang Kai", "you", "Yang Kai", "you",
                     "Yang Kai", "you", "Yang Kai", "you", "you", "you", "you", "you",
                     "Yang Kai", "you", "you", "you"]
                )

                self.game.stopSoundtrack()
                self.game.currentBGM = "jy_yuanjindahai.mp3"
                self.game.startSoundtrackThread()

                self.game.generate_dialogue_sequence(
                    None,
                    "Wooden House.png",
                    [_("Brother Yang?"),
                     _("Xinyi, you... you're awake?"),
                     _("Yeah, I couldn't sleep because you said you were leaving tomorrow..."),
                     _("And then I heard you and {} talking so I came to see what happened...").format(self.game.character),
                     _("Ah, s-sorry for disturbing your sleep... I..."),
                     _("I wish you didn't have to go..."),
                     _("Me too... these past couple of weeks have been some of the most enjoyable moments of my life..."), #Row 2
                     _("You have great potential; definitely one of the smartest people I've ever taught..."),
                     _("Brother Yang... Actually, I... I..."),
                     _("I know what you want to say, but you can find someone who's more suitable for you..."),
                     _("Someone who's younger, more romantic, and more handsome..."),
                     _("No! I just want to be with you... You make me feel safe and... and..."),
                     _("Maybe it's the way you care for me or how knowledgeable you are, but I get this indescribable joy when I see you."), # Row 3
                     _("No one else has ever made me feel this way."),
                     _("But the enemies of Shanhu Sect might come after you if they find out that we..."),
                     _("With you here to protect me, why do I need to be afraid?"),
                     _("Plus, if I can be with you for just 1 day, I'll be content, even if it means death."),
                     _("Xinyi..."), #Row 4
                     _("Brother Yang..."),
                     _("*As you watch them embrace each other, you can't help but express your happiness with a wide grin.*"),
                     _("Alright, alright, starting to make me feel like a third wheel here..."),
                     _("So I assume I can leave it to you to take care of Xinyi?"),
                     _("Of course, thank you {}.").format(self.game.character),
                     _("Thanks for encouraging us, {}!").format(self.game.character),
                     _("Ahhh, no need to be so formal la... Just don't forget to invite me to your wedding!"),
                     _("Haha, we'll be sure to make you an honored guest!"), #Row 5
                     _("{}, as a way of expressing my gratitude, I want to teach you something before we leave.").format(self.game.character),
                     ],
                    ["Feng Xinyi", "Yang Kai", "Feng Xinyi", "Feng Xinyi", "Yang Kai", "Feng Xinyi",
                     "Yang Kai", "Yang Kai", "Feng Xinyi", "Yang Kai", "Yang Kai", "Feng Xinyi",
                     "Feng Xinyi", "Feng Xinyi", "Yang Kai", "Feng Xinyi", "Feng Xinyi",
                     "Yang Kai", "Feng Xinyi", "Blank", "you", "you", "Yang Kai", "Feng Xinyi", "you",
                     "Feng Xinyi", "Yang Kai",
                     ]
                )
                if self.game.chivalry + self.game.luck >= 200:
                    new_move = special_move(_("Demon Suppressing Blade"))
                    self.game.learn_move(new_move)
                    new_skill = skill(_("Divine Protection"))
                    self.game.learn_skill(new_skill)
                    messagebox.showinfo("", _("Yang Kai teaches you: 'Demon Suppressing Blade' and 'Divine Protection'!"))

                else:
                    new_move = special_move(_("Demon Suppressing Blade"))
                    self.game.learn_move(new_move)
                    messagebox.showinfo("", _("Yang Kai teaches you: 'Demon Suppressing Blade'!"))

                self.game.generate_dialogue_sequence(
                    None,
                    "Wooden House.png",
                    [_("Thanks, Elder Yang!"),
                     _("You're welcome; ok, I'll take Xinyi back to Shanhu Sect. Have fun with your own adventures!"),
                     _("Bye {}!").format(self.game.character),
                     _("See you guys later!")],
                    ["you", "Yang Kai", "Feng Xinyi", "you"]
                )
                self.game.wooden_house_win.withdraw()
                self.game.wooden_house_win.quit()
                self.game.wooden_house_win.destroy()
                try:
                    self.game.innWin.quit()
                    self.game.innWin.destroy()
                except:
                    pass
                self.game.mapWin.deiconify()


            else:
                self.game.wooden_house_win.withdraw()
                self.game.generate_dialogue_sequence(
                    None,
                    "Wooden House.png",
                    [_("Alright, you two, it's been fun, but I need to get on with my own mission soon."),
                     _("You're leaving?"),
                     _("Tomorrow morning."),
                     _("..."),
                     _("What about you two?"),
                     _("I'll be wandering around to seek some adventures of my own. Xinyi, any plans?"),
                     _("Probably going to return to Yuelai Inn. If my father comes again, I'll just go with him..."),
                     _("Erm, uh, in that case, let's get a good night's sleep to prepare for the journey ahead."),
                     _("*Later that night, you wake up from your sleep to the sound of footsteps.*"), #Row 2
                     _("Elder Yang? Where are you going?"),
                     _("I... I'm going to leave now..."),
                     _("Wait, I thought you were leaving tomorrow morning..."),
                     _("That... that was..."),
                     _("It's because of Xinyi, isn't it? You're doing this on purpose... to avoid her..."),
                     _("I... I'm not good at saying goodbyes... This will be better for both of us..."),
                     _("*sigh*... You're right. This will help her forget you sooner..."),
                     _("Thanks for understanding, {}.").format(self.game.character), #Row 3
                     _("Before I leave, I want to give you these."),
                     _("*Yang Kai hands some Hundred Seeds Pills to you.*"),
                     _("My friends from Wudang gave them to me as a gift, but I think you need them more than I do."),
                     _("Thanks!"),
                     _("Alright, I gotta go now. Take care of Xinyi..."),
                     _("No problem! Farewell, Elder Yang!")],
                    ["Yang Kai", "you", "Yang Kai", "Feng Xinyi", "Yang Kai", "you", "Feng Xinyi", "you",
                     "Blank", "you", "Yang Kai", "you", "Yang Kai", "you", "Yang Kai", "you",
                     "Yang Kai", "Yang Kai", "Blank", "Yang Kai", "you", "Yang Kai", "you"
                     ]
                )
                self.game.add_item(_("Hundred Seeds Pill"), 10)
                self.game.generate_dialogue_sequence(
                    None,
                    "Wooden House.png",
                    [_("*Next morning*"),
                     _("He... left...?"),
                     _("Yeah, last night..."),
                     _("Without even saying goodbye..."),
                     _("He... this is better... for both of you..."),
                     _("Yeah, I know... Perhaps it's fate... Let's go..."),
                     _("*You escort Feng Xinyi back to Yuelai Inn.*")],
                    ["Blank", "Feng Xinyi", "you", "Feng Xinyi", "you", "Feng Xinyi", "Blank"]
                )
                self.game.wooden_house_win.withdraw()
                self.game.wooden_house_win.quit()
                self.game.wooden_house_win.destroy()
                try:
                    self.game.innWin.quit()
                    self.game.innWin.destroy()
                except:
                    pass
                self.game.mapWin.deiconify()


        else:
            r = randrange(2)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("How are you feeling, {}?").format(self.game.character),
                     _("Pretty good, thanks! This place is quite relaxing... I like it!"),
                     _("Training hard?"),
                     _("You bet!"),
                     _("Very good. If you ever need some advice, feel free to talk to me!")],
                    ["Yang Kai", "you", "Yang Kai", "you", "Yang Kai"]
                )
            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("Xinyi told me how you guys met and became friends."),
                     _("Quite an interesting story, I must say... Dumplings, eh?"),
                     _("Hahahaha, yeah, she had quite an appetite that day!"),
                     _("(I hope she didn't tell him about the part where we snuck into the Shaolin Scripture Library...)")],
                    ["Yang Kai", "Yang Kai", "you", "you"]
                )


    def clickedOnFengXinyi(self):
        r = randrange(3)
        if self.game.taskProgressDic["feng_pan"] == 50:
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("Hey, {}! I've learned so much from Brother Yang!").format(self.game.character),
                     _("Oh? I bet you're really happy that you've got a pro mentoring you one-on-one, eh?"),
                     _("Definitely! He taught me a bunch of cool things that my dad and brother never taught me..."),
                     _("In fact, I'm pretty sure I can beat my brother now!"),
                     _("He'll probably be the one who needs to take lessons from me, hah! ... Well ..."),
                     _("If I ever get to see them again, that is..."),
                     _("Don't have negative thoughts. They're still your family, after all."), #Row 2
                     _("After a while, they'll either forget about this or, at the very least, not hold anything against you."),
                     _("I sure hope so..."),
                     _("Hey! Come on! Cheer up! Now's not the time to be depressed!"),
                     _("You've got a pro who's willing to teach you; better use this time wisely!"),
                     _("You're right! Let me go ask him about my form during {}...").format(pickOne(["Luohan Fist", "Shaolin Diamond Finger"]))],
                    ["Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi",
                     "you", "you", "Feng Xinyi", "you", "you", "Feng Xinyi"]
                )
            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("How are your injuries?"),
                     _("I'm all recovered! Just had some minor scratches, but thanks. How are you keeping?"),
                     _("Pretty well; this is a nice place. Great for training!"),
                     _("Indeed! Ok, I gotta get back to training; talk to you later!")],
                    ["you", "Feng Xinyi", "you", "Feng Xinyi"]
                )
            elif r == 2:
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("Have you learned anything from Brother Yang yet?"),
                     _("He seems very open to sharing his techniques and experience!"),
                     _("Well, I would, but it seems like he's always busy teaching you so..."),
                     _("..... *Blush*"),
                     _("Hahahaha, just kidding! Yeah I'll find some time to consult him.")],
                    ["Feng Xinyi", "Feng Xinyi", "you", "Feng Xinyi", "you"]
                )

        elif self.game.taskProgressDic["feng_pan"] == 60:
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("*sigh*..."),
                     _("How are you feeling?"),
                     _("I... I'm trying to let go but... I think he senses that something is different."),
                     _("It's just so unnatural..."),
                     _("Well, we're probably going to part ways soon. You won't have to worry about it then."),
                     _("Yeah... I guess...")],
                    ["Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi", "you", "Feng Xinyi"]
                )
            else:
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("I've been losing focus recently... My mind is a mess..."),
                     _("It must be difficult to focus with Elder Yang here. That's natural."),
                     _("Should I really not say anything? Maybe there's a chance if..."),
                     _("Don't be tempted! I know it's hard, but it's the smart thing to do. Just let go."),
                     _("You'll soon forget about him after we part ways."),
                     _("Yeah... I guess...")],
                    ["Feng Xinyi", "you", "Feng Xinyi", "you", "you", "Feng Xinyi"]
                )

        elif self.game.taskProgressDic["feng_pan"] == 70:
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("{}!!! ").format(self.game.character),
                     _("What's up? You seem to be so energetic every day haha... I wonder why..."),
                     _("Hah! Stop it!~ I gotta share with you..."),
                     _("While training with Brother Yang today, there was a moment where we made eye contact for a long time."),
                     _("Well, it was probably only 2 seconds but it felt like an eternity!"),
                     _("Then to break the silence, we both tried to say something at the same time but..."),
                     _("It just came out like gibberish. I didn't even know what I was saying; it was so awkward!!!"),
                     _("Hahahahaha! I wish I was there to see it!")],
                    ["Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi", "you"]
                )

            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("{}!!!").format(self.game.character),
                     _("Yes?"),
                     _("Today, while we were training, his hand brushed mine!"),
                     _("It was probably an accident but still... *blush*"),
                     _("Awww... tsk tsk tsk... you are so infatuated hahaha~")],
                    ["Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi", "you"]
                )

            elif r == 2:
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("Hey, {}, gotta ask you something...").format(self.game.character),
                     _("Yes?"),
                     _("Am I looking at him too much? Like... Is it very obvious?"),
                     _("Erm... yes...?"),
                     _("I can't help it, but I also don't want to be so obvious..."),
                     _("Well, I think you're a lost cause at this point."),
                     _("One time, I saw you stop in the middle of walking somewhere and just stand there to watch him walk by..."),
                     _("I... I did...? *blush*"),
                     _("Yeeeeeep...")],
                    ["Feng Xinyi", "you", "Feng Xinyi", "you", "Feng Xinyi", "you", "you", "Feng Xinyi", "you"]
                )


    def task(self, choice=None):
        self.days += 1
        if choice == "Wood":
            r = randrange(2,4)
            self.game.strength += r
            messagebox.showinfo("", _("You spend the day chopping wood. Your strength + {}!").format(r))
        elif choice == "Water":
            r = randrange(2,4)
            self.game.dexterity += r
            messagebox.showinfo("", _("You spend the day fetching water from the lake. Your dexterity + {}!").format(r))

        if self.game.taskProgressDic["feng_pan"] == 50 and self.days >= 5:
            self.talk_to_feng_xinyi(0)
        elif self.days >= 20:
            self.talk_to_yang_kai(1)


    def talk_to_feng_xinyi(self, choice=None):
        if choice == 0:
            self.game.generate_dialogue_sequence(
                self.game.wooden_house_win,
                "Wooden House.png",
                [_("Here, relax your shoulder and elbow; concentrate your Qi into the tips of your fingers..."),
                 _("Then as you extend your arm, rotate your upper body from your waist, and... Hah!"),
                 _("Hmmm... like this?"),
                 _("Yup, better... Now --- Oh, hey! {}! You're back!").format(self.game.character),
                 _("Hey guys! Practicing very diligently, I see!"),
                 _("Um, ah, yeah... Uh, tell you what, why don't you relax for a bit, {}.").format(self.game.character),
                 _("I'll go start the fire and make dinner for us."),
                 _("*Yang Kai exits towards the kitchen.*"),
                 _("So......."),  # Row 2
                 _("Hmmm?"),
                 _("I gotta be honest... I'm kinda jealous... Elder Yang seems to spend a lot of time instructing you."),
                 _("How come he doesn't seem as interested in teaching me, huh?"),
                 _("What?? You're thinking too much, {}.... *Blush*").format(self.game.character),
                 _("Hahahah! I'm just kidding la! But seriously, don't you think he's... interested in you?"),
                 _("That can't be... He, he's just super nice, that's all..."),
                 _("I mean, he teaches you stuff too, doesn't he?"),
                 _("That's true, but I feel like he's not as meticulous when teaching me..."),  # Row 3
                 _("Anyway, what's more important than that is how you feel about him..."),
                 _("I..."),
                 _("Yes?"),
                 _("Of course I like him but... I feel like I'm not good enough for him..."),
                 _("He's an Elder of a well-respected sect, and I'm just a young girl."),
                 _("He's 27 years old, and I'm only 18...")],
                ["Yang Kai", "Yang Kai", "Feng Xinyi", "Yang Kai", "you", "Yang Kai", "Yang Kai", "Blank",
                 "you", "Feng Xinyi", "you", "you", "Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi",
                 "you", "you", "Feng Xinyi", "you", "Feng Xinyi", "Feng Xinyi", "Feng Xinyi",
                 ],
                [[_("You're right. That's not gonna work out."), partial(self.talk_to_feng_xinyi, 11)],
                 [_("Why does the age gap matter if you both like each other?"), partial(self.talk_to_feng_xinyi, 12)],
                 [_("That's not a problem. Linghu Chong and Ren Yingying were also 9 years apart..."), partial(self.talk_to_feng_xinyi, 13)]
                 ]
            )

        else:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()

            if choice == 11:
                self.game.taskProgressDic["feng_pan"] = 60
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("I knew I was too naive to even think there might be a possibility..."),
                     _("Sorry to be so blunt. I feel it's important that you hear the truth, even if it hurts..."),
                     _("No need to apologize, {}. Maybe I should let go now before it's too late...").format(self.game.character),
                     _("Yeah, I think that would be best..."),],
                    ["Feng Xinyi", "you", "Feng Xinyi", "you", ],
                )

            elif choice == 12:
                self.game.taskProgressDic["feng_pan"] = 70
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("You... you really think so?"),
                     _("Yeah, of course! What's more important than your age gap is whether he feels the same way about you."),
                     _("If so, then that alone shouldn't prevent you guys from being together!"),
                     _("You're right, {}. Thanks for the encouragement!").format(self.game.character),
                     _("You're welcome! That's what friends are for!")],
                    ["Feng Xinyi", "you", "you", "Feng Xinyi", "you", ],
                )

            elif choice == 13:
                self.game.taskProgressDic["feng_pan"] = 70
                self.game.luck += 5
                self.game.generate_dialogue_sequence(
                    self.game.wooden_house_win,
                    "Wooden House.png",
                    [_("Who? What? Who's Linghu Something and Something Yingying?"),
                     _("Oh, er... Nevermind, you wouldn't know them..."),
                     _("The point is, age alone shouldn't be the determining factor."),
                     _("If he has feelings for you too, then you guys should go for it!"),
                     _("You're right, {}. Thanks for the encouragement!").format(self.game.character),
                     _("You're welcome! That's what friends are for!")],
                    ["Feng Xinyi", "you", "you", "you", "Feng Xinyi", "you"],
                )