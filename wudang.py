from tkinter import*
from gettext import gettext
from functools import partial
from helper_funcs import*
from special_move import*
from skill import*
from character import*
from item import*
from random import*
import tkinter.messagebox as messagebox


_ = gettext


class Scene_wudang_train:

    def __init__(self, game):
        self.game = game

        self.game.days_spent_in_training = int((self.game.taskProgressDic["join_sect"] - 5) * 100)
        try:
            if self.game.taskProgressDic["join_sect"] == 5.0:
                try:
                    self.game.wudang_train_win = Toplevel(self.game.scrubHouseWin)
                except:
                    self.game.wudang_train_win = Toplevel(self.game.continueGameWin)

            else:
                self.game.wudang_train_win = Toplevel(self.game.continueGameWin)
        except:
            self.game.wudang_train_win = Toplevel(self.game.continueGameWin)

        self.game.wudang_train_win.title(_("Wudang Mountain"))

        bg = PhotoImage(file="Wudang.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.wudang_train_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Zhou Enrui_icon_large.ppm")
        imageButton1 = Button(F1, command=partial(self.clickedOnZhouEnrui, True))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("Zhou Enrui"))
        label.grid(row=1, column=0)
        F1.place(x=w // 4 - pic1.width() // 2, y=h // 2)

        F2 = Frame(canvas)
        pic2 = PhotoImage(file="Li Kefan_icon_large.ppm")
        imageButton2 = Button(F2, command=partial(self.clickedOnLiKefan, True))
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(F2, text=_("Li Kefan"))
        label.grid(row=1, column=0)
        F2.place(x=w // 2 - pic1.width() // 2, y=h // 2)

        if self.game.taskProgressDic["ouyang_nana_xu_jun"] != 30:
            F3 = Frame(canvas)
            pic3 = PhotoImage(file="Xu Jun_icon_large.ppm")
            imageButton3 = Button(F3, command=partial(self.clickedOnXuJun, True))
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=pic3)
            label = Label(F3, text=_("Xu Jun"))
            label.grid(row=1, column=0)
            F3.place(x=w * 3 // 4 - pic1.width() // 2, y=h // 2)

        self.game.days_spent_in_training_label = Label(self.game.wudang_train_win,
                                                  text=_("Days trained: {}").format(self.game.days_spent_in_training))
        self.game.days_spent_in_training_label.pack()
        self.game.wudang_train_win_F1 = Frame(self.game.wudang_train_win)
        self.game.wudang_train_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.wudang_train_win,
                                            options=["Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.wudang_train_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.wudang_train_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.wudang_train_win.winfo_width(), self.game.wudang_train_win.winfo_height()
        self.game.wudang_train_win.geometry("+%d+%d" %(SCREEN_WIDTH//2 - toplevel_w//2, 5))
        self.game.wudang_train_win.focus_force()
        self.game.wudang_train_win.mainloop()###


    def clickedOnZhouEnrui(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        if training:
            for widget in self.game.wudang_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.wudang_train_win_F1, text=_("Spar (Zhou Enrui)"),
                   command=partial(self.wudang_train_spar, targ=_("Zhou Enrui"))).grid(row=1, column=0)
            Button(self.game.wudang_train_win_F1, text=_("Talk"),
                   command=partial(self.talk_to_zhou_enrui, training=True)).grid(row=2, column=0)
            Button(self.game.wudang_train_win_F1, text=_("Learn"),
                   command=partial(self.wudang_train_learn)).grid(row=3, column=0)

        else:
            for widget in self.game.wudang_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.wudang_win_F1, text=_("Spar (Zhou Enrui)"),
                   command=partial(self.wudang_train_spar, targ=_("Zhou Enrui"), training=False)).grid(row=1, column=0)
            Button(self.game.wudang_win_F1, text=_("Talk"),
                   command=self.talk_to_zhou_enrui).grid(row=2, column=0)


    def talk_to_zhou_enrui(self, choice=0, training=False):
        if training:
            fromWin = self.game.wudang_train_win
        else:
            fromWin = self.game.wudang_win

        if choice == 0:
            r = randrange(2)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    fromWin,
                    "Wudang.png",
                    [_("Externally soft, internally hard, this is the essence of Taichi..."),],
                    ["Zhou Enrui"]
                )

            elif r == 1:
                self.game.generate_dialogue_sequence(
                    fromWin,
                    "Wudang.png",
                    [_("When sparring with other people, just remember that honor is more important than victory."),],
                    ["Zhou Enrui"]
                )


    def clickedOnLiKefan(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if training:
            for widget in self.game.wudang_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.wudang_train_win_F1, text=_("Spar (Li Kefan)"),
                   command=partial(self.wudang_train_spar, targ=_("Li Kefan"))).grid(row=1, column=0)
            Button(self.game.wudang_train_win_F1, text=_("Talk"),
                   command=partial(self.talk_to_li_kefan, training=True)).grid(row=2, column=0)
            Button(self.game.wudang_train_win_F1, text=_("Task"),
                   command=self.wudang_train_task).grid(row=3, column=0)
            Button(self.game.wudang_train_win_F1, text=_("Meditate"),
                   command=self.wudang_train_meditate).grid(row=4, column=0)

        else:
            for widget in self.game.wudang_win_F1.winfo_children():
                widget.destroy()

            if self.game.taskProgressDic["join_sect"] != 6000:
                if self.game.taskProgressDic["wudang_task"] in [2,4,6,7]:
                    self.wudang_task_reward(training=False)
                    return
                Button(self.game.wudang_win_F1, text=_("Spar (Li Kefan)"),
                       command=partial(self.wudang_train_spar, targ=_("Li Kefan"), training=False)).grid(row=1, column=0)
                Button(self.game.wudang_win_F1, text=_("Talk"),
                       command=self.talk_to_li_kefan).grid(row=2, column=0)
                Button(self.game.wudang_win_F1, text=_("Do you have any tasks for me?"), command=self.wudang_task).grid(row=3, column=0)

            else:
                pass #If originally trained with Wudang


    def talk_to_li_kefan(self, choice=0, training=False):
        if training:
            fromWin = self.game.wudang_train_win
        else:
            fromWin = self.game.wudang_win

        if choice == 0:
            r = randrange(3)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    fromWin,
                    "Wudang.png",
                    [_("I wonder how Brother Zhong Yue is doing... I haven't seen him in a couple of months."), ],
                    ["Li Kefan"]
                )

            elif r == 1:
                self.game.generate_dialogue_sequence(
                    fromWin,
                    "Wudang.png",
                    [_("To uphold justice and protect the weak and poor, that is the mission of the Three Warriors of Wudang!")],
                    ["Li Kefan"]
                )

            elif r == 2:
                self.game.generate_dialogue_sequence(
                    fromWin,
                    "Wudang.png",
                    [_("Chivalry and honor before all else!")],
                    ["Li Kefan"]
                )


    def clickedOnXuJun(self, training=False):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()
        if training:
            for widget in self.game.wudang_train_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.wudang_train_win_F1, text=_("Spar (Xu Jun)"),
                   command=partial(self.wudang_train_spar, targ=_("Xu Jun"))).grid(row=1, column=0)
            Button(self.game.wudang_train_win_F1, text=_("Talk"),
                   command=partial(self.talk_to_xu_jun, training=True)).grid(row=2, column=0)

        else:
            for widget in self.game.wudang_win_F1.winfo_children():
                widget.destroy()

            Button(self.game.wudang_win_F1, text=_("Spar (Xu Jun)"),
                   command=partial(self.wudang_train_spar, targ=_("Xu Jun"), training=False)).grid(row=1, column=0)
            Button(self.game.wudang_win_F1, text=_("Talk"),
                   command=self.talk_to_xu_jun).grid(row=2, column=0)


    def talk_to_xu_jun(self, choice=0, training=False):
        if training:
            fromWin = self.game.wudang_train_win
        else:
            fromWin = self.game.wudang_win

        if choice == 0:
            r = randrange(2)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    fromWin,
                    "Wudang.png",
                    [_("It's such an honor to learn from the 3 Warriors of Wudang!"),],
                    ["Xu Jun"]
                )

            elif r == 1:
                self.game.generate_dialogue_sequence(
                    fromWin,
                    "Wudang.png",
                    [_("I can't wait for the opportunity to spar with other sects again!")],
                    ["Xu Jun"]
                )


    def wudang_train_spar(self, targ, training=True):

        if training:
            if self.game.taskProgressDic["join_sect"] < 100 and self.game.taskProgressDic["join_sect"] - 5 < .59 and random() <= .5:
                self.wudang_training_update(.01)

            if targ == _("Zhou Enrui"):
                opp = character(_("Zhou Enrui"), 14,
                                    sml=[special_move(_("Taichi Fist"), level=9),
                                         special_move(_("Basic Sword Technique"), level=10),
                                         special_move(_("Taichi Sword"), level=9)],
                                    skills=[skill(_("Wudang Agility Technique"), level=9),
                                            skill(_("Pure Yang Qi Skill"), level=8)])

            elif targ == _("Li Kefan"):
                opp = character(_("Li Kefan"), 12,
                                    sml=[special_move(_("Taichi Fist"), level=8),
                                         special_move(_("Basic Sword Technique"), level=10),
                                         special_move(_("Taichi Sword"), level=8)],
                                    skills=[skill(_("Wudang Agility Technique"), level=7),
                                            skill(_("Pure Yang Qi Skill"), level=7)])

            elif targ == _("Xu Jun"):
                opp = character(_("Xu Jun"), min([8, self.game.level]),
                                    sml=[special_move(_("Taichi Fist"), level=min([8, self.game.level])),
                                         special_move(_("Basic Sword Technique"), level=min([10, self.game.level]))],
                                    skills=[skill(_("Wudang Agility Technique"), level=3),
                                            skill(_("Pure Yang Qi Skill"), level=3)])

            self.game.battleMenu(self.game.you, opp, "Wudang.png",
                            "jy_wudang.mp3", fromWin=self.game.wudang_train_win,
                            battleType="training", destinationWinList=[self.game.wudang_train_win] * 3)


        else:
            if targ == _("Zhou Enrui"):
                opp = character(_("Zhou Enrui"), 16,
                                sml=[special_move(_("Taichi Fist"), level=10),
                                     special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Taichi Sword"), level=9)],
                                skills=[skill(_("Wudang Agility Technique"), level=10),
                                        skill(_("Pure Yang Qi Skill"), level=9)])

            elif targ == _("Li Kefan"):
                opp = character(_("Li Kefan"), 14,
                                sml=[special_move(_("Taichi Fist"), level=9),
                                     special_move(_("Basic Sword Technique"), level=10),
                                     special_move(_("Taichi Sword"), level=8)],
                                skills=[skill(_("Wudang Agility Technique"), level=8),
                                        skill(_("Pure Yang Qi Skill"), level=8)])

            elif targ == _("Xu Jun"):
                opp = character(_("Xu Jun"), 10,
                                sml=[special_move(_("Taichi Fist"), level=8),
                                     special_move(_("Basic Sword Technique"), level=10)],
                                skills=[skill(_("Wudang Agility Technique"), level=4),
                                        skill(_("Pure Yang Qi Skill"), level=4)])

            self.game.battleMenu(self.game.you, opp, "Wudang.png",
                                 "jy_wudang.mp3", fromWin=self.game.wudang_win,
                                 battleType="training", destinationWinList=[self.game.wudang_win] * 3)


    def wudang_train_meditate(self):

        self.game.perception += 1
        messagebox.showinfo("", _("You spend 2 days meditating with Li Kefan. Your perception increased by 1!"))
        self.wudang_training_update(.02)


    def wudang_train_learn(self):
        move_names = [m.name for m in self.game.sml]
        if _("Basic Sword Technique") not in move_names:
            self.game.generate_dialogue_sequence(
                self.game.wudang_train_win,
                "Wudang.png",
                [_("In order to learn more advanced techniques, you need to first master the fundamentals."),
                 _("I will teach you the Basic Sword Technique."),
                 _("Practice diligently, and once you make sufficient progress, I will teach you something else."), ],
                ["Zhou Enrui", "Zhou Enrui", "Zhou Enrui"]
            )

            new_move = special_move(name=_("Basic Sword Technique"))
            messagebox.showinfo("", _("Guo Junda taught you a new move: Basic Sword Technique!"))
            self.game.learn_move(new_move)


        elif average([move.level for move in self.game.sml if move.name in [_("Basic Sword Technique"), _("Taichi Fist"), _("Wudang Agility Technique"), _("Taichi Sword")]]) < 4.5:
            self.game.generate_dialogue_sequence(
                self.game.wudang_train_win,
                "Wudang.png",
                [_("Keep practicing. You are not yet ready to learn a new technique.")],
                ["Zhou Enrui"]
            )

        else:
            if _("Taichi Fist") not in move_names:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_train_win,
                    "Wudang.png",
                    [_("Very good; you are ready to learn the Taichi Fist."),
                     _("Watch carefully...")],
                    ["Zhou Enrui", "Zhou Enrui"]
                )

                new_move = special_move(name=_("Taichi Fist"))
                messagebox.showinfo("", _("Zhou Enrui taught you a new move: Taichi Fist!"))
                self.game.learn_move(new_move)

            elif _("Wudang Agility Technique") not in [s.name for s in self.game.skills]:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_train_win,
                    "Wudang.png",
                    [_("Very good; you are ready to learn the Wudang Agility Technique."),
                     _("Watch carefully...")],
                    ["Zhou Enrui", "Zhou Enrui"]
                )

                new_skill = skill(name=_("Wudang Agility Technique"))
                messagebox.showinfo("", _("Zhou Enrui taught you a new skill: Wudang Agility Technique!"))
                self.game.learn_skill(new_skill)

            elif _("Taichi Sword") not in move_names:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_train_win,
                    "Wudang.png",
                    [_("Very good; you are ready to learn the Taichi Sword."),
                     _("Watch carefully...")],
                    ["Zhou Enrui", "Zhou Enrui"]
                )

                new_move = special_move(name=_("Taichi Sword"))
                messagebox.showinfo("", _("Zhou Enrui taught you a new move: Taichi Sword!"))
                self.game.learn_move(new_move)

            else:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_train_win,
                    "Wudang.png",
                    [_("I have nothing else to teach you at the moment."),
                     _("Keep practicing and honing your skills.")],
                    ["Zhou Enrui", "Zhou Enrui"]
                )


    def wudang_task(self):
        if self.game.chivalry > 0:
            #Mission 1
            if (self.game.taskProgressDic["wu_hua_que"] >= 1 or self.game.taskProgressDic["xiao_han_jia_wen"] == 20) and self.game.taskProgressDic["wudang_task"] == 0:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_win,
                    "Wudang.png",
                    [_("I hear recently that Wu Hua Que, the Flower Staining Sparrow, has been quite active in the East recently."),
                     _("This lewd bastard is an infamous rapist. We have tried many times to hunt him down."),
                     _("Several times, we were almost successful, but he was able to make narrow escapes due to his agility skills."),
                     _("If you are able to track him down and get rid of him, it would be doing the Martial Arts community a huge favor."),
                     _("Alright, I'm on it! Where should I look?"),
                     _("I am unsure of his exact whereabouts, but I would look on the Eastern side of the country.")],
                    ["Li Kefan", "Li Kefan", "Li Kefan", "Li Kefan", "you", "Li Kefan"]
                )
                self.game.taskProgressDic["wudang_task"] = 1

            elif self.game.taskProgressDic["wudang_task"] == 1:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_win,
                    "Wudang.png",
                    [_("Have you found Wu Hua Que yet?"),
                     _("Not yet; I'll keep looking!")],
                    ["Li Kefan", "you"]
                )

            elif self.game.taskProgressDic["wudang_task"] == 3:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_win,
                    "Wudang.png",
                    [_("The Emperor has imposed higher taxes again."),
                     _("As a result, many people are starving."),
                     _("I want you to sneak into the Imperial Palace and steal some valuable items or gold."),
                     _("We need 5000 gold to help the nearby towns."),
                     _("No problem!")],
                    ["Li Kefan", "Li Kefan", "Li Kefan", "Li Kefan", "you"]
                )
                self.game.taskProgressDic["wudang_task"] = 4

            elif self.game.taskProgressDic["wudang_task"] == 5:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_win,
                    "Wudang.png",
                    [_("Last month, the monk Xuan Chen, who used to be a Shaolin elder, came to Wudang and stole the Taichi Fist Manual."),
                     _("He was driven out of Shaolin for attemping to steal the Tendon Changing Manual, and now he has come to Wudang to cause trouble."),
                     _("We need help tracking him down and getting the manual back."),
                     _("I'm on it! What does he look like?"),
                     _("He's quite old: white beard and mustache, small eyes, and a big nose..."),
                     _("Got it!")],
                    ["Li Kefan", "Li Kefan", "Li Kefan", "you", "Li Kefan", "you"]
                )
                self.game.taskProgressDic["wudang_task"] = 6

            else:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_win,
                    "Wudang.png",
                    [_("Thank you for your interest, but I have nothing suitable for you at the moment.")],
                    ["Li Kefan"]
                )
        # Don't offer any missions if chivalry too low
        else:
            self.game.generate_dialogue_sequence(
                self.game.wudang_win,
                "Wudang.png",
                [_("Thank you for your interest, but I have nothing suitable for you at the moment.")],
                ["Li Kefan"]
            )


    def wudang_train_task(self):
        if self.game.days_spent_in_training % 3 == 1:
            if randrange(2):
                self.game.generate_dialogue_sequence(
                    self.game.wudang_train_win,
                    "Wudang.png",
                    [_("{}, I have a task for you...").format(self.game.character),
                     _("Recently, there's been reports of a hooligan in a nearby village who is causing trouble."),
                     _("He now extorts the village people for money and harasses the women."),
                     _("Go teach him a lesson...")],
                    ["Li Kefan", "Li Kefan", "Li Kefan", "Li Kefan"]
                )

                opp = character(_("Hooligan"), min([5, self.game.level]),
                                sml=[special_move(_("Basic Punching Technique"), level=min([5,self.game.level+1]))])

                self.game.battleMenu(self.game.you, opp, "battleground_forest.ppm",
                                postBattleSoundtrack="jy_wudang.mp3",
                                fromWin=self.game.wudang_train_win,
                                battleType="normal", destinationWinList=[],
                                postBattleCmd = self.wudang_task_reward
                                )

            else:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_train_win,
                    "Wudang.png",
                    [_("{}, I have a task for you...").format(self.game.character),
                     _("A few robbers at the bottom of the mountain have been quite active in the past few days"),
                     _("Put an end to their lawlessness.")],
                    ["Li Kefan", "Li Kefan", "Li Kefan"]
                )

                opp = character(_("Masked Robber"), min([7, self.game.level]),
                                sml=[special_move(_("Basic Sword Technique"), level=min([8,self.game.level+1]))])

                self.game.battleMenu(self.game.you, opp, "battleground_forest.ppm",
                                postBattleSoundtrack="jy_wudang.mp3",
                                fromWin=self.game.wudang_train_win,
                                battleType="normal", destinationWinList=[],
                                postBattleCmd = self.wudang_task_reward
                                )


        else:
            self.game.generate_dialogue_sequence(
                self.game.wudang_train_win,
                "Wudang.png",
                [_("I have no tasks for you at the moment."),
                 _("Come back at a later time.")],
                ["Li Kefan", "Li Kefan"]
            )


    def wudang_task_reward(self, training=True):

        if training:
            self.game.generate_dialogue_sequence(
                self.game.wudang_train_win,
                "Wudang.png",
                [_("Nicely done, {}!").format(self.game.character),
                 _("As your reward, here are a few jars of Hundred Seeds Pills. These pills are made from 100 different types of seeds and nuts."),
                 _("Eating one will provide great benefits.")],
                ["Li Kefan", "Li Kefan", "Li Kefan"]
            )

            r = randrange(1,min([5,self.game.level//2+1]))
            self.game.add_item(_("Hundred Seeds Pill"), r)
            messagebox.showinfo("", _("Received 'Hundred Seeds Pill' x {}!").format(r))


            self.wudang_training_update(.02)

        else:
            total = self.game.chivalry + self.game.perception + self.game.luck
            if self.game.taskProgressDic["wudang_task"] == 2:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_win,
                    "Wudang.png",
                    [_("Nicely done, {}!").format(self.game.character),
                     _("As your reward, here are a few jars of Hundred Seeds Pills. These pills are made from 100 different types of seeds and nuts."),
                     _("Eating one will provide great benefits.")],
                    ["Li Kefan", "Li Kefan", "Li Kefan"]
                )
                r = randrange(1, total//50)
                self.game.add_item(_("Hundred Seeds Pill"), r)
                messagebox.showinfo("", _("Received 'Hundred Seeds Pill' x {}!").format(r))

                self.game.taskProgressDic["wudang_task"] = 3


            elif self.game.taskProgressDic["wudang_task"] == 4:
                if self.game.inv[_("Gold")] >= 5000:

                    response = messagebox.askquestion("", _("Give 5000 Gold to complete task?"))
                    if response != "yes":
                        return
                    self.game.generate_dialogue_sequence(
                        self.game.wudang_win,
                        "Wudang.png",
                        [_("Nicely done, {}!").format(self.game.character),
                         _("As your reward, here are a few jars of Hundred Seeds Pills. These pills are made from 100 different types of seeds and nuts."),
                         _("Eating one will provide great benefits.")],
                        ["Li Kefan", "Li Kefan", "Li Kefan"]
                    )
                    r = randrange(1, total // 50)
                    self.game.add_item(_("Gold"),-5000)
                    self.game.add_item(_("Hundred Seeds Pill"),r)
                    messagebox.showinfo("",_("Gave 5000 Gold to Li Kefan. Received 'Hundred Seeds Pill' x {}!").format(r))

                    self.game.taskProgressDic["wudang_task"] = 5

                else:
                    self.game.generate_dialogue_sequence(
                        self.game.wudang_win,
                        "Wudang.png",
                        [_("Have you stolen enough gold from the Imperial Palace yet?"),
                         _("Not yet, working on it.")],
                        ["Li Kefan", "you"]
                    )

            elif self.game.taskProgressDic["wudang_task"] == 6:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_win,
                    "Wudang.png",
                    [_("Can you remind me what Xuan Chen looks like again?"),
                     _("Sure, he's an old monk with white beard and mustache, small eyes, and a big nose.")],
                    ["you", "Li Kefan"]
                )

            elif self.game.taskProgressDic["wudang_task"] == 7:
                self.game.generate_dialogue_sequence(
                    self.game.wudang_win,
                    "Wudang.png",
                    [_("Have you found that old monk Xuan Chen yet?"),
                     _("Oh... uh... not yet... still uh, working on it...")],
                    ["Li Kefan", "you"]
                )



    def wudang_training_update(self, increment):
        self.game.taskProgressDic["join_sect"] += increment
        self.game.taskProgressDic["join_sect"] = round(self.game.taskProgressDic["join_sect"], 2)
        self.game.days_spent_in_training = int(self.game.taskProgressDic["join_sect"] * 100 - 500)
        self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
        if self.game.days_spent_in_training >= 60:
            self.game.days_spent_in_training = 60
            self.game.taskProgressDic["join_sect"] = 5.6
            self.game.days_spent_in_training_label.config(text=_("Days trained: {}").format(self.game.days_spent_in_training))
            self.game.wudang_train_win.withdraw()

            self.game.generate_dialogue_sequence(
                None,
                "Wudang.png",
                [_("Well, {}, the time has come for us to part ways.").format(self.game.character),
                 _("Sadly it is so, Master. My time here has been most rewarding."),
                 _("Thank you all for being patient with me and teaching me so much!"),
                 _("It was our pleasure."),
                 _("We are quite proud of you."),
                 _("It was fun sparring with you, {}! I will have to keep training and hopefully become as strong as you one day!").format(self.game.character),
                 _("Take care, {}. Remember to do good and to refrain from evil.").format(self.game.character),
                 _("Yes, Master. I will not let you down!")],
                ["Zhang Tianjian", "you", "you", "Li Kefan", "Zhou Enrui", "Xu Jun", "Zhang Tianjian", "you"]
            )

            self.game.taskProgressDic["join_sect"] = 600
            self.game.generateGameWin()



class Scene_wudang(Scene_wudang_train):
    def __init__(self, game):
        self.game = game

        if self.game.taskProgressDic["wudang_task"] == -1000:
            self.wudang_traitor_fight()
            return

        self.game.wudang_win = Toplevel(self.game.mapWin)
        self.game.wudang_win.title(_("Wudang Mountain"))

        bg = PhotoImage(file="Wudang.png")
        w = bg.width()
        h = bg.height()

        canvas = Canvas(self.game.wudang_win, width=w, height=h)
        canvas.pack()
        canvas.create_image(0, 0, anchor=NW, image=bg)

        F1 = Frame(canvas)
        pic1 = PhotoImage(file="Zhou Enrui_icon_large.ppm")
        imageButton1 = Button(F1, command=partial(self.clickedOnZhouEnrui, False))
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(F1, text=_("Zhou Enrui"))
        label.grid(row=1, column=0)
        F1.place(x=w // 4 - pic1.width() // 2, y=h // 2)

        F2 = Frame(canvas)
        pic2 = PhotoImage(file="Li Kefan_icon_large.ppm")
        imageButton2 = Button(F2, command=partial(self.clickedOnLiKefan, False))
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(F2, text=_("Li Kefan"))
        label.grid(row=1, column=0)
        F2.place(x=w // 2 - pic1.width() // 2, y=h // 2)

        if self.game.taskProgressDic["ouyang_nana_xu_jun"] != 30:
            F3 = Frame(canvas)
            pic3 = PhotoImage(file="Xu Jun_icon_large.ppm")
            imageButton3 = Button(F3, command=partial(self.clickedOnXuJun, False))
            imageButton3.grid(row=0, column=0)
            imageButton3.config(image=pic3)
            label = Label(F3, text=_("Xu Jun"))
            label.grid(row=1, column=0)
            F3.place(x=w * 3 // 4 - pic1.width() // 2, y=h // 2)

        self.game.wudang_win_F1 = Frame(self.game.wudang_win)
        self.game.wudang_win_F1.pack()

        menu_frame = self.game.create_menu_frame(master=self.game.wudang_win,
                                                 options=["Map", "Profile", "Inv", "Save", "Settings"])
        menu_frame.pack()

        self.game.wudang_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.wudang_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.wudang_win.winfo_width(), self.game.wudang_win.winfo_height()
        self.game.wudang_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.wudang_win.focus_force()
        self.game.wudang_win.mainloop()  ###


    def wudang_traitor_fight(self, stage=0):
        if stage == 0:
            self.game.generate_dialogue_sequence(
                None,
                "Wudang.png",
                [_("Hah! You traitor of Wudang! You dare to return?"),
                 _("{}, we have heard about what you did at the inn.").format(self.game.character),
                 _("Needless to say, we are sorely disappointed."),
                 _("I regret teaching you! I wish I'd seen through you earlier..."),
                 _("Hahahahaha! Are you all done whining? Are we going to fight or what?"),
                 _("Today, we will exterminate you for the good of the Martial Arts Community!")
                 ],
                ["Zhong Yue", "Li Kefan", "Li Kefan", "Zhou Enrui", "you", "Li Kefan"]
            )

            opp_list = []

            opp = character(_("Zhou Enrui"), 15,
                            sml=[special_move(_("Taichi Fist"), level=10),
                                 special_move(_("Taichi Sword"), level=9)],
                            skills=[skill(_("Wudang Agility Technique"), level=10),
                                    skill(_("Pure Yang Qi Skill"), level=9)])
            opp_list.append(opp)

            opp = character(_("Li Kefan"), 15,
                            sml=[special_move(_("Taichi Fist"), level=9),
                                 special_move(_("Taichi Sword"), level=8)],
                            skills=[skill(_("Wudang Agility Technique"), level=8),
                                    skill(_("Pure Yang Qi Skill"), level=8)])
            opp_list.append(opp)

            opp = character(_("Zhong Yue"), 15,
                            sml=[special_move(_("Taichi Fist"), level=8),
                                 special_move(_("Taichi Sword"), level=8)],
                            skills=[skill(_("Wudang Agility Technique"), level=8),
                                    skill(_("Pure Yang Qi Skill"), level=7)])
            opp_list.append(opp)

            cmd = lambda: self.wudang_traitor_fight(1)
            self.game.battleMenu(self.game.you, opp_list, "Wudang.png", "jy_wudang.mp3",
                                 fromWin=None, battleType="task", destinationWinList=[], postBattleCmd=cmd)


        elif stage == 1:
            self.game.generate_dialogue_sequence(
                None,
                "Wudang.png",
                [_("You guys are hardly a challenge..."),
                 _("Maybe consider retiring? Hahahahaha!"),
                 _("{}...").format(self.game.character),
                 _("Ah, Master Zhang... finally a man worthy of my time."),
                 _("I am deeply saddened that you have turned out like this; however, it's not too late to repent."),
                 _("I respect you, Master Zhang, but that doesn't mean I agree with you."),
                 _("I'm just doing what I need to make myself stronger. There's nothing wrong with that."), #Row 2
                 _("Then, I hope you will at least stop using Wudang's Kung-Fu to do it..."),
                 _("Sorry, that's up to me..."),
                 _("You leave me no choice then...")
                 ],
                ["you", "you", "Zhang Tianjian", "you", "Zhang Tianjian", "you",
                 "you", "Zhang Tianjian", "you", "Zhang Tianjian"]
            )

            opp = character(_("Zhang Tianjian"), 25,
                      sml=[special_move(_("Taichi Fist"), level=10),
                           special_move(_("Taichi Sword"), level=10)],
                      skills=[skill(_("Wudang Agility Technique"), level=10),
                              skill(_("Pure Yang Qi Skill"), level=10),
                              skill(_("Taichi 18 Forms"), level=10),
                              ])

            cmd = lambda: self.wudang_traitor_fight(2)
            self.game.battleMenu(self.game.you, opp, "Wudang.png", "jy_wudang.mp3",
                                 fromWin=None, battleType="task", destinationWinList=[], postBattleCmd=cmd)

        elif stage == 2:
            self.game.generate_dialogue_sequence(
                None,
                "Wudang.png",
                [_("HAHAHAHAHA!!! Not even the strongest person in Wudang can take me down! HAHAHAHAHA!!!!"),
                 _("What a pity... *sigh*..."),
                 _("Well, that was fun... I shall take my leave now since no one here can stop me! Hahahahahahahahaha!")
                 ],
                ["you", "Zhang Tianjian", "you"]
            )
            self.game.taskProgressDic["wudang_task"] = -2000
            self.game.mapWin.deiconify()