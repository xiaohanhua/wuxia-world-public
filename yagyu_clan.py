from mini_game_yagyu_dungeon_level_1 import*
from mini_game_yagyu_dungeon_level_2 import*
from mini_game_yagyu_dungeon_level_3 import*
from mini_game_hattori_smoke_bomb_training_level_1 import*
from mini_game_hattori_smoke_bomb_training_level_2 import*
from mini_game_hattori_ninjutsu_training_level_1 import*
from mini_game_hattori_ninjutsu_training_level_2 import*
from mini_game_hattori_ninjutsu_training_level_3 import*
from mini_game_hattori_ninjutsu_training_level_4 import*
from functools import partial
from special_move import*
from character import*
from skill import*
from helper_funcs import *
import tkinter.messagebox as messagebox
import time

_ = gettext


class Scene_yagyu_clan:

    def __init__(self, game):

        self.game = game

        if self.game.taskProgressDic["yagyu_clan"] == -17:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Inner Court Night BG.png",
                [_("Tell Yagyu Tadashi to come out and speak to us! We know he has captured our Abbot!"),
                 _("Yagyu-san is busy right now and cannot entertain guests. Please leave."),
                 _("Release the Abbot, and we'll leave right away!"),
                 _("What Abbot? Hah, has the mighty Shaolin lost their very own Abbot? What a joke!"),
                 _("We know of your shameful acts! It is no use to play dumb."),
                 _("Say what you will... Your presence is not welcome here. Leave!"), #Row 2
                 _("Charge in and rescue the Abbot! Ahhhhhhhhhhh!!!"),
                 _("We'll saturate the soil with your filthy blood!!! Ahhhhhh!!!"),
                 _("(Now's the perfect chance!)")
                ],
                ["Elder Xuanjing", "Yagyu Samurai", "Elder Xuanjing", "Yagyu Samurai", "Elder Xuanjing",
                 "Yagyu Samurai", "Elder Xuanjing", "Yagyu Samurai", "you"]
            )

            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Study BG.png",
                [_("(Ok... should be in here somewhere... 2nd row from bottom... thick book... ah!)")],
                ["you"],
            )

            self.game.currentEffect = "door_unlock.wav"
            self.game.startSoundEffectThread()
            time.sleep(.25)

            self.game.stopSoundtrack()
            self.game.currentBGM = "jy_suspenseful1.mp3"
            self.game.startSoundtrackThread()

            if self.game.taskProgressDic["yagyu_dungeon"] == 4:
                self.post_dungeon_mini_game(4)
            else:
                self.game.mini_game_in_progress = True
                if self.game.taskProgressDic["yagyu_dungeon"] == 1:
                    self.game.mini_game = yagyu_dungeon_level_1_mini_game(self.game, self)
                elif self.game.taskProgressDic["yagyu_dungeon"] == 2:
                    self.game.mini_game = yagyu_dungeon_level_2_mini_game(self.game, self)
                elif self.game.taskProgressDic["yagyu_dungeon"] == 3:
                    self.game.mini_game = yagyu_dungeon_level_3_mini_game(self.game, self)
                self.game.mini_game.new()

            return


        elif self.game.taskProgressDic["yagyu_clan"] == 999999999:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan BG.png",
                [_("You have made a fool of us! Depart from our presence at once, you spineless coward!"),
                 _("Were it not for your past contribution, I would have your head chopped off!"),],
                ["Ashikaga Yoshiro", "Ashikaga Yoshiro"]
            )
            self.game.mapWin.deiconify()
            return


        elif self.game.taskProgressDic["yagyu_clan"] == 10000:
            self.game.mapWin.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan BG.png",
                [_("Ah, {}-san! Welcome back, highly-esteemed companion! My heart is delighted to see you!").format(self.game.character),
                 _("How are preparations going?"),
                 _("Couldn't be better! Our guests should be arriving in a few hours..."),
                 _("Shanhu Sect, Dragon Sect, and Wudang are all sending their representatives... Oh, and of course, the Shaolin monks..."),
                 _("What about Huashan Sect?"),
                 _("Hah! Their leader is a coward. They declined our invitation and said they would go along with what the majority decided."),
                 _("Good, one less sect to worry about... well, not that they would've been much of a problem anyway..."), #Row 2
                 _("What about the rest of them? Do we have enough manpower to suppress them should they decide to unite against us?"),
                 _("With you on our side, it should be no problem."),
                 _("Yamamoto and I can hold off the Elders of Shanhu Sect."),
                 _("Hattori-san and his ninjas can take care of the Wudang priests and Shaolin monks, excluding their leader."),
                 _("That leaves you and Yagyu-san to deal with Tan Keyong, Zhang Tianjian, and Ouyang Xiong."),
                 _("But won't Hattori-san and his men be severely outnumbered?"), #Row 3
                 _("Ah, your concern is valid; however, there are 2 factors we need to consider."),
                 _("1) Hattori-san is much stronger than you might think. In fact, Yagyu-san would not be able to defeat him 1-on-1."),
                 _("2) The Shaolin monks have compassion, which is their weakness, while Hattori-san's ninjas strike to kill"),
                 _("And they care not whether they do so through 'honorable' means, as long as the opponent dies."),
                 _("This gives them great advantage in this battle."), #Row 4
                 _("And in the worst case scenario, we have our safety net: the Shaolin Abbot."),
                 _("Brilliant! Looks like you thought this out quite well. Yagyu-san is fortunate to have a competent aide like you, Ashikaga-san!"),
                 _("Ah, you flatter me, dear friend. Anyway, let us get ready. They will be here soon..."),],
                ["Ashikaga Yoshiro", "you", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "you", "Ashikaga Yoshiro",
                 "you", "you", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro",
                 "you", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro",
                 "Ashikaga Yoshiro", "Ashikaga Yoshiro", "you", "Ashikaga Yoshiro"
                 ]
            )

            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan BG.png",
                [_("*2 hours later*"),
                 _("Welcome to the Yagyu Clan, everyone! We are most honored that you've joined us for this exciting occasion!"),
                 _("Thank you for the invitation. In the letter, you highlighted that you wish to discuss something that pertains to the future of the Martial Arts Community."),
                 _("We are quite curious what can be of such high importance that you've gathered us all here."),
                 _("This had better be worth my time..."),
                 _("Where is our Abbot? What have you done with him?!"),
                 _("The Abbot is doing fine; I assure you of this; please do not worry."), #Row 2
                 _("We have heard that you've taken Abbot as a hostage; is this true?"),
                 _("I see that there is an unfortunate misunderstanding. Abbot is our guest, not hostage."),
                 _("I have utmost respect for the Abbot, which is why I invited him to come and discuss this matter together."),
                 _("Lies! Where is he now, then? Why isn't he free to leave as he pleases?"),
                 _("Abbot is enjoying some prized Japanese artwork as we speak. I could not bear to interrupt him."),
                 _("However, to ease your minds, I will send a servant to request the Abbot's presence."), #Row 3
                 _("While he is on his way, please kindly lend your ears to me, for I have a proposal to make."),
                 _("Let's hear what he has to say, so we can at least know why we're here..."),
                 _("Agreed. Please proceed, Yagyu-san."),
                 _("Thank you... ")],
                ["Blank", "Yagyu Tadashi", "Tan Keyong", "Zhang Tianjian", "Ouyang Xiong", "Elder Xuanjing",
                 "Yagyu Tadashi", "Tan Keyong", "Yagyu Tadashi", "Yagyu Tadashi", "Elder Xuanjing", "Yagyu Tadashi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Ouyang Xiong", "Zhang Tianjian", "Yagyu Tadashi",
                 ]
            )

            self.game.stopSoundtrack()
            self.game.currentBGM = "txdy_yagyu.mp3"
            self.game.startSoundtrackThread()

            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan BG.png",
                [_("I am sure many of us here have heard of the saying, 'Unity succeeds division and division follows unity.'"),
                 _("One is bound to be replaced by the other after a long span of time."),
                 _("This is the way with things in the world, and the same is true for the Martial Arts Community."),
                 _("From the revered Shaolin and Wudang to newer sects like Dragon Sect, Eagle Sect, and Shanhu Sect that have emerged in the recent decades"),
                 _("Each have demonstrated their own unique styles and techniques; however, one problem persists in Wulin:"),
                 _("The lack of a true leader who can unify the Martial Arts Community."),
                 _("Over the years, many conflicts have arisen between sects from simple misunderstandings."), #Row 2
                 _("Due to each's refusal to suppress their ego, the tension escalates and, in some cases, forces allied sects to get involved."),
                 _("We have no shortage of such unfortunate scenarios, and many lives are wasted as a result."),
                 _("Thus, what I am proposing is that we elect a Supreme Leader of Wulin who can settle such disputes and unite us all."),
                 _("Each sect will maintain their autonomy with regards to matters within their own clan."),
                 _("However, when a dispute must be resolved between two or more sects, the Supreme Leader can do so without the use of unnecessary violence."),
                 _("Furthermore, the Supreme Leader will decide the future direction of Wulin, and all sects must obey his orders."),
                 _("As for whom we should choose for the task, let us settle this in an honorable tournament."), #Row 3
                 _("He who is able to win 3 consecutive duels with members of another sect shall claim the title."),
                 _("I hope you will carefully consider my sincere words and see the beauty and harmony that it promises."),
                 _("Hah! You sure know how to use pretty speech to conceal your true ambitions."),
                 _("Supreme Leader of Wulin? Might as well call it 'Martial Emperor of China'!"),
                 _("I have long enjoyed my independence from other sects. If you don't bother me, I won't mess with you."),
                 _("And now you want me to submit to some Supreme Leader?"), #Row 4
                 _("Ah, but it may very well be you who becomes the--"),
                 _("Not interested. Like I said, I enjoy independence from other sects. An occasional gathering is fine."),
                 _("But to have to settle disputes and burden myself with other tiresome matters? No thank you."),
                 _("Even if you offered the position to me, I would refuse. As it stands, Dragon Sect has nothing to benefit from your proposal."),
                 _("It is a firm 'no' from me."),
                 _("How unfortunate, as your peers Huashan and Shaolin have already agreed... We just need 1 more to secure a majority vote."), #Row 5
                 _("Nonsense! Abbot would agree to no such thing! What have you done with him? Let him come out and speak for himself!"),
                 _("As I mentioned earlier, Abbot will be here shortly... Now please, may I hear what you have to say, Tan-san and Zhang-san?"),
                 _("Although I, too, desire to see peace in Wulin, I do not believe this is the way to achieve it."),
                 _("Well-said, Master Zhang, I am also concerned about the possible repercussions of giving 1 person such power."),
                 _("Please, friends, when you consider my proposal, do not consider only the bl--"),
                 _("Are you done with your rambling? I have better things to do than to listen to your crafty speeches."), #Row 6
                 _("I regret wasting my time here. I shall take my leave now."),
                 _("Wait, please, this is --"),
                 _("You have heard the decisions of other sects. Now, release the Abbot!"),
                 _("Elder Xuanjing is right; please take us to see how the Abbot is doing. We are quite concerned about him."),
                 _("I'm afraid I cannot allow anyone to leave or see the Abbot until we reach an agreement..."),
                 _("You admit, then, that you've taken the Abbot hostage!"), #Row 7
                 _("I do not wish to harm the Abbot. I must do this for the good of the Martial Arts Community..."),
                 _("What more do we need to hear??? Charge in and rescue the Abbot!!!"),
                 _("We will help you."),
                 _("Wudang and Shaolin have long been allies. We cannot stand by and watch."),
                 _("I have no interest in forming any alliances, but this guy gets on my nerves. I need to teach him some humility."),
                 _("So you have chosen senseless violence over peaceful negotiation, eh? Very well. The Yagyu Clan fears no one."), #Row 8
                 _("Yamamoto, Ashikaga, prepare for battle...")],
                ["Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Ouyang Xiong", "Ouyang Xiong", "Ouyang Xiong",
                 "Ouyang Xiong", "Yagyu Tadashi",  "Ouyang Xiong", "Ouyang Xiong", "Ouyang Xiong",  "Ouyang Xiong",
                 "Yagyu Tadashi", "Elder Xuanjing", "Yagyu Tadashi", "Zhang Tianjian", "Tan Keyong", "Yagyu Tadashi",
                 "Ouyang Xiong", "Ouyang Xiong", "Yagyu Tadashi", "Elder Xuanjing", "Zhang Tianjian", "Yagyu Tadashi",
                 "Elder Xuanjing", "Yagyu Tadashi", "Elder Xuanjing", "Tan Keyong", "Zhang Tianjian", "Ouyang Xiong",
                 "Yagyu Tadashi", "Yagyu Tadashi"
                 ],
                [[_("Yagyu-san, I'll take on Ouyang Xiong and Tan Keyong!"), partial(self.yagyu_tournament_choice,1)],
                 [_("Yagyu-san, I'll take on Ouyang Xiong and Zhang Tianjian!"), partial(self.yagyu_tournament_choice,2)],
                 [_("Yagyu-san, I'll take on Tan Keyong and Zhang Tianjian!"), partial(self.yagyu_tournament_choice,3)],
                 [_("Sneak away..."), partial(self.yagyu_tournament_choice,4)]]
            )
            return



        self.game.yagyu_clan_win = Toplevel(self.game.mapWin)
        self.game.yagyu_clan_win.title(_("Yagyu Clan"))

        bg = PhotoImage(file="Yagyu Clan BG.png")
        self.w = bg.width()
        self.h = bg.height()

        self.canvas = Canvas(self.game.yagyu_clan_win, width=self.w, height=self.h)
        self.canvas.pack()
        self.canvas.create_image(0, 0, anchor=NW, image=bg)


        if self.game.taskProgressDic["yagyu_clan"] not in [-4, -16, -100, -101, 222222222]:
            self.game.yagyu_clanF1 = Frame(self.canvas)
            pic1 = PhotoImage(file="Ashikaga Yoshiro_icon_large.ppm")
            imageButton1 = Button(self.game.yagyu_clanF1, command=self.clicked_on_ashikaga_yoshiro)
            imageButton1.grid(row=0, column=0)
            imageButton1.config(image=pic1)
            label = Label(self.game.yagyu_clanF1, text=_("Ashikaga\nYoshiro"))
            label.grid(row=1, column=0)
            self.game.yagyu_clanF1.place(x=self.w * 1 // 4 - pic1.width() // 2, y=self.h // 4 - pic1.height() // 2)

            self.game.yagyu_clanF2 = Frame(self.canvas)
            pic2 = PhotoImage(file="Yamamoto Takeshi_icon_large.ppm")
            imageButton2 = Button(self.game.yagyu_clanF2, command=self.clicked_on_yamamoto_takeshi)
            imageButton2.grid(row=0, column=0)
            imageButton2.config(image=pic2)
            label = Label(self.game.yagyu_clanF2, text=_("Yamamoto\nTakeshi"))
            label.grid(row=1, column=0)
            self.game.yagyu_clanF2.place(x=self.w * 3 // 4 - pic2.width() // 2, y=self.h // 4 - pic2.height() // 2)

        self.game.yagyu_clanF3 = Frame(self.canvas)
        pic3 = PhotoImage(file="yagyu_clan_inner_court_icon.png")
        imageButton3 = Button(self.game.yagyu_clanF3, command=self.go_inner_court)
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        label = Label(self.game.yagyu_clanF3, text=_("Inner Court"))
        label.grid(row=1, column=0)
        self.game.yagyu_clanF3.place(x=self.w * 3 // 4 - pic3.width() // 2, y=self.h // 2 + pic3.height() // 4)


        menu_frame = self.game.create_menu_frame(self.game.yagyu_clan_win)
        menu_frame.pack()
        self.game.yagyu_clan_win_F1 = Frame(self.game.yagyu_clan_win)
        self.game.yagyu_clan_win_F1.pack()

        self.game.yagyu_clan_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.yagyu_clan_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.yagyu_clan_win.winfo_width(), self.game.yagyu_clan_win.winfo_height()
        self.game.yagyu_clan_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.yagyu_clan_win.focus_force()
        self.game.yagyu_clan_win.mainloop()  ###


    def clicked_on_ashikaga_yoshiro(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if self.game.taskProgressDic["yagyu_clan"] == 3:
            self.game.taskProgressDic["yagyu_clan"] = 4
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Most honorable friend! Are you ready for our first task?"),
                 _("Ready as ever!"),
                 _("Excellent! The first step in Yagyu-san's plan is to take down Shaolin."),
                 _("To ensure our success, we'll need someone to sneak into Shaolin at night and poison the Abbot's food."),
                 _("We're going to kill the Shaolin Abbot?"),
                 _("Not quite; the poison is not lethal. It will only weaken him."),
                 _("Once the Abbot is weakened, we will go and challenge Shaolin."), #Row 2
                 _("And after we defeat the elders and the Abbot himself, we will force them to become a sub-sect of the Yagyu Clan."),
                 _("This way, our clan will almost double in strength and number."),
                 _("But what if they refuse?"),
                 _("No worries, we have back-up plans... We'll do whatever is necessary for them to comply..."),
                 _("Sounds simple enough... When do I start?"),
                 _("As soon as you're ready. Take this Ninja Suit and also the poison..."),#Row 3
                 _("*Received 'Ninja Suit' x 1, 'Centipede Poison Powder' x 5.*"),
                 _("I look forward to good news from you, my friend!")],
                ["Ashikaga Yoshiro", "you", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "you", "Ashikaga Yoshiro",
                 "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "you", "Ashikaga Yoshiro", "you",
                 "Ashikaga Yoshiro", "Blank", "Ashikaga Yoshiro"]
            )
            self.game.add_item(_("Ninja Suit"))
            self.game.add_item(_("Centipede Poison Powder"), 5)
            return

        elif self.game.taskProgressDic["yagyu_clan"] == 10:
            self.game.taskProgressDic["yagyu_clan"] = 11
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Everything is in order. I have poisoned the Abbot of Shaolin!"),
                 _("I knew you would not disappoint us, faithful companion!"),
                 _("Now that we have removed the threat, we can proceed with the next part of the plan..."),
                 _("I'm all ears!"),
                 _("Let us go and greet those monks, shall we?"),
                 _("Yamamoto and I will meet you there..."),
                 _("But first, a gift for you, my friend."),
                 _("*You receive the Helmet of Bishamonten.*")
                ],
                ["you", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "you", "Ashikaga Yoshiro", "Ashikaga Yoshiro",
                 "Ashikaga Yoshiro", "Blank"]
            )
            self.game.add_item(_("Helmet of Bishamonten"))
            return

        elif self.game.taskProgressDic["yagyu_clan"] == 11:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Our plan is going smoothly! We will meet you at Shaolin!"),],
                ["Ashikaga Yoshiro"]
            )
            return

        elif self.game.taskProgressDic["yagyu_clan"] == 20:
            self.game.taskProgressDic["yagyu_clan"] = 21
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Forgive me. I failed to poison the Abbot. He saw through my disguise..."),
                 _("It's quite alright, my friend. Seeing that you escaped unscathed, I doubt the Abbot is a real threat anyway."),
                 _("We can execute the next step of our plan. Are you ready?"),
                 _("Prepared to take orders!"),
                 _("Let us go and greet those monks, shall we?"),
                 _("Yamamoto and I will meet you there..."),
                ],
                ["you", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "you", "Ashikaga Yoshiro", "Ashikaga Yoshiro"]
            )
            return

        elif self.game.taskProgressDic["yagyu_clan"] == 21:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Our plan must continue! We will meet you at Shaolin!"),],
                ["Ashikaga Yoshiro"]
            )
            return

        elif self.game.taskProgressDic["yagyu_clan"] == 30:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("What do we do next?"),
                 _("With the Abbot under our control, the Shaolin monks have choice but to submit to us."),
                 _("Even though they may not fully obey our orders, they definitely won't dare to stand in our way."),
                 _("As such, we've eliminated a strong opposition to our plan to become the Supreme Sect in the Martial Arts Community."),
                 _("Our next step is to ensure cooperation from the righteous sects and any sect that may prove to be an obstacle."),
                 _("I have sent letters to Wudang Sect, Dragon Sect, Shanhu Sect, and Huashan Sect and invited them to a friendly tournament."), #Row 2
                 _("These are all renowned sects, so I'm sure their leaders will be quite strong."),
                 _("This is where we'll need your assistance, dear friend."),
                 _("Once they bend the knee to the Yagyu Clan, we will be unstoppable!"),
                 _("Suppose they refuse, even after we win?"),
                 _("Hattori-san and his ninjas are prepared to ambush them at the snap of a finger."), #Row 3
                 _("They'll be on our territory, which means they're as good as caged turtles, hahahahaha!"),
                 _("Good, I don't want those righteous sects causing any trouble..."),
                 _("If it were up to me, I'd get rid of them right away, but I suppose it can't hurt to give them a chance to join us."),
                 _("Much of this depends on your victory during the tournament, my friend."),
                 _("Please, take some time to prepare; you have a month before the tournament begins."),
                 _("Don't worry, just be ready for an entertaining show...")],
                ["you", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro",
                 "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "you",
                 "Ashikaga Yoshiro", "Ashikaga Yoshiro", "you", "you", "Ashikaga Yoshiro", "Ashikaga Yoshiro", "you"]
            )
            self.game.taskProgressDic["yagyu_clan"] = max([self.game.gameDate,31])
            return


        for widget in self.game.yagyu_clan_win_F1.winfo_children():
            widget.destroy()

        if self.game.taskProgressDic["yagyu_clan"] in [4,5,6,7]:
            Button(self.game.yagyu_clan_win_F1, text=_("What do I need to do again?"),
                   command=partial(self.talk_to_ashikaga_yoshiro, 11)).grid(row=1, column=0)
            Button(self.game.yagyu_clan_win_F1, text=_("I lost my Ninja Suit."),
                   command=partial(self.talk_to_ashikaga_yoshiro, 12)).grid(row=2, column=0)
            Button(self.game.yagyu_clan_win_F1, text=_("I need more poison."),
                   command=partial(self.talk_to_ashikaga_yoshiro, 13)).grid(row=3, column=0)
        else:
            Button(self.game.yagyu_clan_win_F1, text=_("Talk"),
                   command=self.talk_to_ashikaga_yoshiro).grid(row=1, column=0)

            Button(self.game.yagyu_clan_win_F1, text=_("Spar (Ashikaga Yoshiro)"),
                   command=self.spar_with_ashikaga_yoshiro).grid(row=2, column=0)


    def talk_to_ashikaga_yoshiro(self, choice=0):
        if choice == 0:
            if self.game.taskProgressDic["yagyu_clan"] <= -3:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_win,
                    "Yagyu Clan BG.png",
                    [_("Talking to you puts my honor and reputation at risk."),
                     _("Depart from my presence before my loyalty and courage become tainted by your cowardice!"),],
                    ["Ashikaga Yoshiro", "Ashikaga Yoshiro"]
                )
            elif self.game.taskProgressDic["yagyu_clan"] >= 3:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_win,
                    "Yagyu Clan BG.png",
                    [_("Greetings, noble companion! Your courage and strength are most admirable!"),
                     _("Here, you will always find a receptive audience for your words of wisdom!")],
                    ["Ashikaga Yoshiro", "Ashikaga Yoshiro"]
                )
            else:
                r = randrange(3)
                if r == 0:
                    self.game.generate_dialogue_sequence(
                        self.game.yagyu_clan_win,
                        "Yagyu Clan BG.png",
                        [_("Righteousness, heroic courage, benevolence, respect, honesty, honour, loyalty, and self control..."),
                         _("These are the 8 virtues of Bushido.")],
                        ["Ashikaga Yoshiro", "Ashikaga Yoshiro"]
                    )
                elif r == 1:
                    self.game.generate_dialogue_sequence(
                        self.game.yagyu_clan_win,
                        "Yagyu Clan BG.png",
                        [_("Shaolin and Wudang are often praised in this country, but I have reservations about their abilities in combat."),
                         _("They are perceived as strong because they lack strong rivals. It does not take much strength to lift a hair."),
                         _("It does not take sharp eyes to see the sun and moon. It does not take sharp ears to hear the thunderclap.")],
                        ["Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro"]
                    )
                else:
                    self.game.generate_dialogue_sequence(
                        self.game.yagyu_clan_win,
                        "Yagyu Clan BG.png",
                        [_("Few can match the strength and discipline of a samurai."),
                         _("The mighty Yagyu clan will prevail over all its foes!")],
                        ["Ashikaga Yoshiro", "Ashikaga Yoshiro"]
                    )

        elif choice == 11:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Go to Shaolin and poison the Abbot's food."),
                 _("Once you've succeeded, let me know."), ],
                ["Ashikaga Yoshiro", "Ashikaga Yoshiro"]
            )

        elif choice == 12:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Ah, you jest, my friend! It would be shameful for a warrior to lose his clothes."),
                 _("Hahahaha... Indeed, I was only joking! Alright, off I go!"),
                 _("May Bishamonten aid you!")],
                ["Ashikaga Yoshiro", "you", "Ashikaga Yoshiro"]
            )

        elif choice == 13:
            if not self.game.check_for_item(_("Centipede Poison Powder"), 5):
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_win,
                    "Yagyu Clan BG.png",
                    [_("Ah, no worries, I've got plenty of those. Here take some..."),
                     _("*Received some Centipede Poison Powder.*"),
                     _("Thank you!"),
                     _("May Bishamonten be with you!")],
                    ["Ashikaga Yoshiro", "Blank", "you", "Ashikaga Yoshiro"]
                )
                self.game.inv[_("Centipede Poison Powder")] = 5

            else:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_win,
                    "Yagyu Clan BG.png",
                    [_("It seems that you have enough to get the job done, but should you use them up, do come back and see me."),],
                    ["Ashikaga Yoshiro"]
                )


    def spar_with_ashikaga_yoshiro(self):
        if self.game.taskProgressDic["yagyu_clan"] <= -3:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("So you wish to spar, eh?"),
                 _("That is a privilege reserved for friends of the Yagyu Clan. Regretfully, you no longer fall into that category.")],
                ["Ashikaga Yoshiro", "Ashikaga Yoshiro"]
            )
        elif self.game.taskProgressDic["yagyu_clan"] <= 10:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Very well. I will show you the strength of the mighty Yagyu Clan!")],
                ["Ashikaga Yoshiro"]
            )

            opp = character(_("Ashikaga Yoshiro"), 20,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Hachiman Blade"), level=5)],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Kenjutsu II"), level=7)],
                            preferences=[2,2,4,3,4])

            self.game.battleMenu(self.game.you, opp, "Yagyu Clan BG.png",
                                 "Sakura_Variation.mp3", fromWin=self.game.yagyu_clan_win,
                                 battleType="training", destinationWinList=[self.game.yagyu_clan_win] * 3)



    def clicked_on_yamamoto_takeshi(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if self.game.taskProgressDic["yagyu_clan"] in [11,21]:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Time to show those monks what happens when they exclude meat from their diet. Ahahahaha!"),],
                ["Yamamoto Takeshi"]
            )
            return

        for widget in self.game.yagyu_clan_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.yagyu_clan_win_F1, text=_("Talk"),
               command=self.talk_to_yamamoto_takeshi).grid(row=1, column=0)

        Button(self.game.yagyu_clan_win_F1, text=_("Spar (Yamamoto Takeshi)"),
               command=self.spar_with_yamamoto_takeshi).grid(row=2, column=0)


    def talk_to_yamamoto_takeshi(self):
        if self.game.taskProgressDic["yagyu_clan"] <= -3:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("I would sooner deliver a monologue to a dunghill than to converse with you...")],
                ["Yamamoto Takeshi"]
            )
        elif self.game.taskProgressDic["yagyu_clan"] >= 3:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Welcome, honorable friend! It is always a pleasure to see you!"),
                 _("Come! Sit! Have some tea, and let us talk of many delightful things!"),],
                ["Yamamoto Takeshi", "Yamamoto Takeshi"]
            )
        else:
            r = randrange(3)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_win,
                    "Yagyu Clan BG.png",
                    [_("Righteousness, heroic courage, benevolence, respect, honesty, honour, loyalty, and self control..."),
                     _("These are the 8 virtues of Bushido.")],
                    ["Yamamoto Takeshi", "Yamamoto Takeshi"]
                )
            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_win,
                    "Yagyu Clan BG.png",
                    [_("A true warrior does not back down from a challenge."),
                     _("I will happily accept an invitation to a duel from you at any time.")],
                    ["Yamamoto Takeshi", "Yamamoto Takeshi"]
                )
            else:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_win,
                    "Yagyu Clan BG.png",
                    [_("I have heard of Shaolin and Wudang, but in my eyes, they are nothing."),
                     _("I can easily crush them with my strength.")],
                    ["Yamamoto Takeshi", "Yamamoto Takeshi"]
                )


    def spar_with_yamamoto_takeshi(self):
        if self.game.taskProgressDic["yagyu_clan"] <= -3:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Do you take me to be an idle sluggard?"),
                 _("I have better things to do than to spar with a dishonorable coward. Now, begone!"),],
                ["Yamamoto Takeshi", "Yamamoto Takeshi"]
            )
        elif self.game.taskProgressDic["yagyu_clan"] <= 10:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan BG.png",
                [_("Very well. I will show you the strength of the mighty Yagyu Clan!")],
                ["Yamamoto Takeshi"]
            )

            opp = character(_("Yamamoto Takeshi"), 19,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Hachiman Blade"), level=4)],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Kenjutsu II"), level=5)],
                            preferences=[2,4,2,3,2])

            self.game.battleMenu(self.game.you, opp, "Yagyu Clan BG.png",
                                 "Sakura_Variation.mp3", fromWin=self.game.yagyu_clan_win,
                                 battleType="training", destinationWinList=[self.game.yagyu_clan_win] * 3)


    def go_inner_court(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        if self.game.taskProgressDic["yagyu_clan"] in [222222222]:
            messagebox.showinfo("", _("You've already searched the Yagyu Clan. There's nothing left to do here."))
            return

        if self.game.taskProgressDic["yagyu_clan"] <= -3 and self.game.taskProgressDic["yagyu_clan"] not in [-100,-101]:
            if _("Ninja Suit") in [eq.name for eq in self.game.equipment]:
                response = messagebox.askquestion("", "Sneak in at night?")
                if response == "yes":
                    self.sneak_into_inner_court()
                    return
            else:
                if self.game.taskProgressDic["yagyu_clan"] == -4:
                    messagebox.showinfo("", _("You should probably sneak in instead, now that the Yagyu Clan is hostile towards you."))
                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Yagyu Clan BG.png",
                        [_("You are no longer a friend of the Yagyu Clan."),
                         _("Yagyu-san does not wish to see you. Leave."),
                         _("(Hmmm... Looks like I will have to sneak in at night later...)")],
                        ["Ashikaga Yoshiro", "Ashikaga Yoshiro", "you"]
                    )
            return
        elif self.game.taskProgressDic["yagyu_clan"] == -1:
            self.game.yagyu_clan_win.withdraw()
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan BG.png",
                [_("One moment! I'm afraid we cannot let you go further."),
                 _("Yagyu-san and his daughter only receive guests who are worth their time."),
                 _("Oh? And how do you know that I'm not worth their time?"),
                 _("You can prove yourself worthy once we assess your skills in combat through a quick spar."),
                 _("If you lack the courage, then please don't bother.")],
                ["Ashikaga Yoshiro", "Ashikaga Yoshiro", "you", "Yamamoto Takeshi", "Yamamoto Takeshi"]
            )
            response = messagebox.askquestion("", _("Challenge Yamamoto and Ashikaga to a quick spar?"))
            if response == "yes":
                opp_list = []
                opp = character(_("Yamamoto Takeshi"), 19,
                                sml=[special_move(_("Kendo"), level=10),
                                     special_move(_("Hachiman Blade"), level=4)],
                                skills=[skill(_("Bushido"), level=10),
                                        skill(_("Kenjutsu I"), level=10),
                                        skill(_("Kenjutsu II"), level=5)],
                                preferences=[2, 4, 2, 3, 2])
                opp_list.append(opp)
                opp = character(_("Ashikaga Yoshiro"), 20,
                                sml=[special_move(_("Kendo"), level=10),
                                     special_move(_("Hachiman Blade"), level=5)],
                                skills=[skill(_("Bushido"), level=10),
                                        skill(_("Kenjutsu I"), level=10),
                                        skill(_("Kenjutsu II"), level=7)],
                                preferences=[2, 2, 4, 3, 4])
                opp_list.append(opp)

                self.game.battleID = "yagyu_clan_initial_challenge"
                self.game.battleMenu(self.game.you, opp_list, "Yagyu Clan BG.png",
                                     "Sakura_Variation.mp3", fromWin=self.game.yagyu_clan_win,
                                     battleType="training", destinationWinList=[self.game.yagyu_clan_win] * 3,
                                     postBattleCmd=self.post_battle)
            else:
                self.game.yagyu_clan_win.deiconify()

            return

        elif self.game.taskProgressDic["yagyu_clan"] == 0:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan BG.png",
                [_("Yagyu-san! We have an honored guest who wishes to see you."),
                 _("Ah, welcome, my name is Yagyu Tadashi, a descendant of the Yagyu Clan."),
                 _("The Yagyu Clan is renowned in Japan for producing the finest Samurai in the land."),
                 _("You must be quite skilled, having defeated my men: Ashikaga and Yamamoto. May I have the honor of knowing your name?"),
                 _("Greetings, Yagyu-san, my name is {}. A great pleasure to meet you!").format(self.game.character),
                 _("Ah, {}-san, welcome indeed! Please meet my daughter, Kasumi, as well.").format(self.game.character),
                 _("{}-san, nice to meet you.").format(self.game.character), #Row 2
                 _("Why, I am most honored, beautiful lady."),
                 _("We traveled a long distance from Japan to make the name of the Yagyu Clan known among all nations."),
                 _("There is much we plan to accomplish, but please, do have a seat and enjoy some tea."),
                 _("We can speak of those matters whenever you are ready.")],
                ["Ashikaga Yoshiro", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "you", "Yagyu Tadashi",
                 "Yagyu Kasumi", "you", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi"
                 ]
            )
            self.game.taskProgressDic["yagyu_clan"] = 1

        elif self.game.taskProgressDic["yagyu_clan"] == -100:
            self.game.add_item(_("Gold"), 50000)
            self.game.add_item(_("Smoke Bomb"), 1000)
            self.game.add_item(_("Ninja Suit"), 20)
            self.game.taskProgressDic["yagyu_clan"] = -101
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan Inner Court BG.png",
                [_("*After a thorough search, you find the following items: Smoke Bomb x 1000, Gold x 50000, Ninja Suit x 20*"),
                 _("*You also find 'The Art of Ninjutsu: Volume II' and 'Ninjutsu Mastery: Volumes I, II, & III'.*"),
                 _("*After careful studying, you learn the skills: 'Ninjutsu I', 'Ninjutsu II', and 'Ninjutsu III'.*")],
                ["Blank", "Blank", "Blank"]
            )
            for ns in [_("Ninjutsu I"), _("Ninjutsu II"), _("Ninjutsu III")]:
                new_skill = skill(ns)
                self.game.learn_skill(new_skill)
            self.game.yagyu_clan_win.deiconify()
            return

        elif self.game.taskProgressDic["yagyu_clan"] == -101:
            messagebox.showinfo("", _("There's nothing more to be found here."))
            self.game.yagyu_clan_win.deiconify()
            return


        self.game.yagyu_clan_win.withdraw()
        self.game.yagyu_clan_inner_court_win = Toplevel(self.game.yagyu_clan_win)
        self.game.yagyu_clan_inner_court_win.title(_("Inner Court"))
    
        bg = PhotoImage(file="Yagyu Clan Inner Court BG.png")
        self.w = bg.width()
        self.h = bg.height()
    
        self.canvas = Canvas(self.game.yagyu_clan_inner_court_win, width=self.w, height=self.h)
        self.canvas.pack()
        self.canvas.create_image(0, 0, anchor=NW, image=bg)
    
        self.game.yagyu_clan_inner_courtF1 = Frame(self.canvas)
        pic1 = PhotoImage(file="Yagyu Tadashi_icon_large.ppm")
        imageButton1 = Button(self.game.yagyu_clan_inner_courtF1, command=self.clicked_on_yagyu_tadashi)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.game.yagyu_clan_inner_courtF1, text=_("Yagyu Tadashi"))
        label.grid(row=1, column=0)
        self.game.yagyu_clan_inner_courtF1.place(x=self.w * 1 // 4 - pic1.width() // 2, y=self.h // 4 - pic1.height() // 2)


        self.game.yagyu_clan_inner_courtF2 = Frame(self.canvas)
        pic2 = PhotoImage(file="Yagyu Kasumi_icon_large.ppm")
        imageButton2 = Button(self.game.yagyu_clan_inner_courtF2, command=self.clicked_on_yagyu_kasumi)
        imageButton2.grid(row=0, column=0)
        imageButton2.config(image=pic2)
        label = Label(self.game.yagyu_clan_inner_courtF2, text=_("Yagyu Kasumi"))
        label.grid(row=1, column=0)
        self.game.yagyu_clan_inner_courtF2.place(x=self.w * 3 // 4 - pic2.width() // 2, y=self.h // 4 - pic2.height() // 2)


        self.game.yagyu_clan_inner_courtF3 = Frame(self.canvas)
        pic3 = PhotoImage(file="yagyu_clan_icon_large.png")
        imageButton3 = Button(self.game.yagyu_clan_inner_courtF3,
                              command=partial(self.game.winSwitch, self.game.yagyu_clan_inner_court_win, self.game.yagyu_clan_win))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        label = Label(self.game.yagyu_clan_inner_courtF3, text=_("Yagyu Clan"))
        label.grid(row=1, column=0)
        self.game.yagyu_clan_inner_courtF3.place(x=self.w * 1 // 4 - pic3.width() // 2, y=self.h // 2 + pic3.height() // 4)


        self.game.yagyu_clan_inner_courtF4 = Frame(self.canvas)
        pic4 = PhotoImage(file="yagyu_dungeon_icon_large.png")
        imageButton4 = Button(self.game.yagyu_clan_inner_courtF4, command=self.go_yagyu_dungeon)
        imageButton4.grid(row=0, column=0)
        imageButton4.config(image=pic4)
        label = Label(self.game.yagyu_clan_inner_courtF4, text=_("Yagyu Dungeon"))
        label.grid(row=1, column=0)
        if self.game.taskProgressDic["yagyu_clan_join"] >= 1:
            self.game.yagyu_clan_inner_courtF4.place(x=self.w * 3 // 4 - pic3.width() // 2,
                                                     y=self.h // 2 + pic3.height() // 4)
        else:
            self.game.yagyu_clan_inner_courtF4.place(x=-3000, y=-3000)

        menu_frame = self.game.create_menu_frame(self.game.yagyu_clan_inner_court_win)
        menu_frame.pack()
        self.game.yagyu_clan_inner_court_win_F1 = Frame(self.game.yagyu_clan_inner_court_win)
        self.game.yagyu_clan_inner_court_win_F1.pack()
    
        self.game.yagyu_clan_inner_court_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.yagyu_clan_inner_court_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.yagyu_clan_inner_court_win.winfo_width(), self.game.yagyu_clan_inner_court_win.winfo_height()
        self.game.yagyu_clan_inner_court_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.yagyu_clan_inner_court_win.focus_force()
        self.game.yagyu_clan_inner_court_win.mainloop()  ###


    def sneak_into_inner_court(self):
        if self.game.taskProgressDic["yagyu_clan"] == -3:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan Inner Court Night BG.png",
                [_("Everything is in order, Yagyu-san. Hattori-san's men left yesterday."),
                 _("Good, good... We will depart first thing tomorrow morning."),
                 _("Remember, we must succeed or else it means shame and dishonor to the Yagyu Clan."),
                 _("Don't worry, Yagyu-san! Those plant-eating baldies are no match for us!"),
                 _("I will show no mercy! Soon, the name 'Shaolin' will become a part of history, ahahahahaha!"),
                 _("Very good, I have full confidence in you two... and Hattori-san's men as well..."),
                 _("Ok now, go get some rest. We need to wake up early tomorrow..."),
                 _("(I'd better go to Shaolin and warn them...)")],
                ["Ashikaga Yoshiro", "Yagyu Tadashi", "Yagyu Tadashi", "Yamamoto Takeshi", "Ashikaga Yoshiro",
                 "Yagyu Tadashi", "Yagyu Tadashi", "you"]
            )
            self.game.yagyu_clanF1.place(x=-3000, y=-3000)
            self.game.yagyu_clanF2.place(x=-3000, y=-3000)
            self.game.taskProgressDic["yagyu_clan"] = -4

        elif self.game.taskProgressDic["yagyu_clan"] in [-13, -14]:
            self.game.taskProgressDic["yagyu_clan"] = -15
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan Inner Court Night BG.png",
                [_("Hahahahaha! Those pathetic bald heads were completely caught off guard!"),
                 _("Hattori-san's men are indeed experts at night attacks..."),
                 _("We are most fortunate to have these men of the shadows as companions."),
                 _("I am impressed with your foresight as well, Yagyu-san."),
                 _("You are always one step ahead of the opponent!"),
                 _("Now that we have their Abbot as prisoner, those monks will surely give in to our demands."), #Row 2
                 _("That, or the Abbot himself may do so for fear of his life, hahahahaha!"),
                 _("Don't underestimate the Abbot of Shaolin. He's a tough nut to crack."),
                 _("I hear that he still hasn't softened up even after hours of torture..."),
                 _("But no matter... I can wait. We have other plans to carry out in the mean time..."),
                 _("Father, is it true? Did you really take the Shaolin Abbot captive?"), #Row 3
                 _("Kasumi, why are you not asleep at this hour? I am in the middle of an important discussion."),
                 _("Please, go to bed. We'll talk later..."),
                 _("I heard some of your men bragging that Hattori-san's ninjas captured the Shaolin Abbot."),
                 _("I want to know the truth, and I want to hear it from you."),
                 _("Kasumi... Remember that this is a war we're fighting..."), #Row 4
                 _("There will no doubt be casualties and sacrifices..."),
                 _("Now, I have nothing but respect and admiration for the Abbot, and I--"),
                 _("Then why did you use such a dishonorable method to make him prisoner?"),
                 _("Dishonorable?! You would call you father dishonorable??"),
                 _("In war, we will do what it takes to win... We are far outnumbered compared to all the sects in China."), #Row 5
                 _("We must be wise, strategic... otherwise, defeat and utter disgrace await us!"),
                 _("Don't you think it's disgraceful to strike a man from behind while he is down? How can you--")],
                ["Ashikaga Yoshiro", "Yagyu Tadashi", "Yagyu Tadashi", "Yamamoto Takeshi", "Yamamoto Takeshi",
                 "Ashikaga Yoshiro", "Yamamoto Takeshi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi",
                 "Yagyu Kasumi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Kasumi", "Yagyu Kasumi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Kasumi", "Yagyu Tadashi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Kasumi"]
            )

            self.game.currentEffect = "sfx_smack.wav"
            self.game.startSoundEffectThread()
            time.sleep(.5)

            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan Inner Court Night BG.png",
                [_("Kyaahhh!"),
                 _("SILENCE! How dare you insult your father?!"),
                 _("It is a disgraceful, indeed, that I've raised you this way..."),
                 _("Oh, ancestors of the Yagyu Clan, why?"),
                 _("Why have you sent me a daughter when I asked for sons?"),
                 _("Is there no hope left for the future generations? Oh, how my heart bleeds at the thought..."), #Row 2
                 _("Ashikaga, Yamamoto, let's go..."),
                 _("Kasumi, I want you to really think about the foolish things you've said today."),
                 _("Until you make an apology, you are not allowed to go anywhere."),
                 _("*Yagyu and his men walk off into the distance.*"),
                 _("Pssst! Kasumi!"), #Row 3
                 _("W-who are you?!"),
                 _("Oh... right..."),
                 _("*You uncover your face.*"),
                 _("{}-san! What are you doing here? And why are you dressed like that?").format(self.game.character),
                 _("Ahhh, well... They won't let me in during the day haha..."),
                 _("Do you know where they've kept the Abbot?"),
                 _("I'm not sure, but if I had to guess, probably the dungeon..."), #Row 4
                 _("Dungeon? Where is that?"),
                 _("It's a secret dungeon where Hattori-san and his ninjas train."),
                 _("The entrance is hidden behind one of the bookshelves in the study."),
                 _("You need to pull out the thickest book on the 2nd row from the bottom; then you'll see a keyhole."),
                 _("Once you insert the key, the bookshelf will move aside and the entrance will be in front of you."),
                 _("Hmmm, I'm guessing you don't have the key..."),
                 _("Don't worry, {}-san. I'll steal it for you when I get a chance.").format(self.game.character), #Row 5
                 _("I will hide it under this bush behind you once I find it. You can retrieve it anytime you want."),
                 _("Thanks, Kasumi! But... Are you sure you're ok with this? Aren't you betraying your father?"),
                 _("I know my father would be furious if he found out, but I really can't let him continue hurting innocent people..."),
                 _("Ok, I'm going to go now before he gets suspicious. Be careful, {}-san!").format(self.game.character)],
                ["Yagyu Kasumi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Blank",
                 "you", "Yagyu Kasumi", "you", "Blank", "Yagyu Kasumi", "you", "you",
                 "Yagyu Kasumi", "you", "Yagyu Kasumi", "Yagyu Kasumi", "Yagyu Kasumi", "Yagyu Kasumi", "you",
                 "Yagyu Kasumi", "Yagyu Kasumi", "you", "Yagyu Kasumi", "Yagyu Kasumi"]
            )
            self.game.yagyu_clan_win.deiconify()

        elif self.game.taskProgressDic["yagyu_clan"] == -15:
            self.game.taskProgressDic["yagyu_clan"] = -16
            self.game.add_item(_("Dungeon Key"))
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan Inner Court Night BG.png",
                [_("(Ok, let's see if Kasumi found the key... It should be around this bush here...)"),
                 _("*You find a key hidden in the bush.*"),
                 _("(Alright, time to get out of here and find Elder Xuanjing...)"),
                 _("I'm not going to ask you again! What did you do with the key?!"),
                 _("It's too late, Father. There's no way you can retrieve the key now."),
                 _("You... How dare you...")],
                ["you", "Blank", "you", "Yagyu Tadashi", "Yagyu Kasumi", "Yagyu Tadashi"]
            )

            self.game.currentEffect = "sfx_smack.wav"
            self.game.startSoundEffectThread()
            time.sleep(.25)
            self.game.currentEffect = "sfx_smack.wav"
            self.game.startSoundEffectThread()
            time.sleep(.25)

            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan Inner Court Night BG.png",
                [_("Ahhhhhh!"),
                 _("You traitor! You disgrace!"),
                 _("I never imagined one day my daughter would help the enemy!"),
                 _("I'm sorry, Father, but I must do the right thing."),
                 _("SILENCE! Are you so thick-skinned that you still dare talk back to me?!"),
                 _("Since you are so concerned about the old monk, why don't you go in the dungeon with him?"), #Row 2
                 _("Yamamoto... Lock her up with the Abbot..."),
                 _("But Yagyu-san, she--"),
                 _("I said LOCK HER UP!"),
                 _("Y-yes, Yagyu-san..."),
                 _("Oh, by the way, Kasumi, if you are hoping that {} will come rescue you, you'd better wake up.").format(self.game.character), #Row 3
                 _("In fact, I hope he comes. It'll save me a lot of trouble hunting him down..."),
                 _("(I'd better go back to Shaolin right away and get Elder Xuanjing to help.)"),
                 _("(Together, we might be able to rescue Abbot and Kasumi...)")],
                ["Yagyu Kasumi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Kasumi", "Yagyu Tadashi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Yamamoto Takeshi", "Yagyu Tadashi", "Yamamoto Takeshi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "you", "you"]
            )


        else:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_win,
                "Yagyu Clan Study BG.png",
                [_("*You sneak into a room filled with books/scrolls on shelves and chests.*")],
                ["Blank"],
                [[_("Search the shelves."), partial(self.yagyu_clan_search,1)],
                 [_("Search the chests."), partial(self.yagyu_clan_search,2)]]
            )


    def yagyu_clan_search(self, option):

        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        if random()*self.game.luck/50 <= .5:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Study BG.png",
                [_("*After a quick search, you find nothing interesting.*")],
                ["Blank"]
            )

        elif option == 1:
            if _("Yagyu Family Tree") not in self.game.inv and random() <= .5:
                self.game.add_item(_("Yagyu Family Tree"))
                self.game.generate_dialogue_sequence(
                    None,
                    "Yagyu Clan Study BG.png",
                    [_("*You find a scroll with the Yagyu Clan Family Tree.*")],
                    ["Blank"]
                )
            else:
                if random() <= .5:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Yagyu Clan Study BG.png",
                        [_("*After a quick search, you find nothing interesting.*")],
                        ["Blank"]
                    )
                elif _("Smoke Bomb") not in [m.name for m in self.game.sml]:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Yagyu Clan Study BG.png",
                        [_("*You find a copy of 'The Art of Ninjutsu: Volume I'.*"),
                         _("*You read it and learn how to use smoke bombs.*")],
                        ["Blank", "Blank"]
                    )
                    new_move = special_move(name=_("Smoke Bomb"))
                    self.game.learn_move(new_move)
                else:
                    self.game.generate_dialogue_sequence(
                        None,
                        "Yagyu Clan Study BG.png",
                        [_("*After a quick search, you find nothing interesting.*")],
                        ["Blank"]
                    )

        else:
            r = pickOne([0,0,0,0,0,0,1,1,2,2,3,3,3,4,4,4,5,6])
            if r == 0:
                item = _("Smoke Bomb")
                item_amount = randrange(10,26)
            elif r == 1:
                item = _("Emerald")
                item_amount = randrange(1,3)
            elif r == 2:
                item = _("Topaz")
                item_amount = randrange(1,2)
            elif r == 3:
                item = _("Spider Poison Powder")
                item_amount = randrange(5,11)
            elif r == 4:
                item = _("Smoke Bomb")
                item_amount = randrange(15,21)
            elif r == 5:
                item = _("Chrysocolla")
                item_amount = 1
            elif r == 6:
                item = _("Amethyst")
                item_amount = 1
            else:
                item = _("Gold")
                item_amount = int(randrange(500,701)*(self.game.luck+100)/100)

            self.game.add_item(item, item_amount)
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Study BG.png",
                [_("*You search some chests and find '{}' x {}.*").format(item, item_amount)],
                ["Blank"]
            )

        r = random() * (self.game.luck + 50) / 100
        if r >= .8:
            self.game.yagyu_clan_win.deiconify()

        else:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Study BG.png",
                [_("Hey! What are you doing here?!"),
                 _("...")],
                ["Yagyu Samurai", "you"]
            )

            opp = character(_("Yagyu Samurai"), 13,
                            sml=[special_move(_("Kendo"), level=8)],
                            skills=[skill(_("Bushido"), level=8),
                                    skill(_("Kenjutsu I"), level=8)],
                            multiple=5)

            self.game.battleMenu(self.game.you, opp, "Yagyu Clan BG.png",
                                 "jy_suspenseful1.mp3", fromWin=self.game.yagyu_clan_win,
                                 battleType="normal", destinationWinList=[self.game.yagyu_clan_win] * 3)




    def clicked_on_yagyu_kasumi(self):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        for widget in self.game.yagyu_clan_inner_court_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.yagyu_clan_inner_court_win_F1, text=_("Talk"),
               command=self.talk_to_yagyu_kasumi).grid(row=1, column=0)

        Button(self.game.yagyu_clan_inner_court_win_F1, text=_("Spar (Yagyu Kasumi)"),
               command=self.spar_with_yagyu_kasumi).grid(row=2, column=0)

        if self.game.taskProgressDic["wife_intimacy"] == -1:
            Button(self.game.yagyu_clan_inner_court_win_F1, text=_("Let's take a walk together!"),
                   command=partial(self.talk_to_yagyu_kasumi,1)).grid(row=3, column=0)


    def talk_to_yagyu_kasumi(self, choice=0):
        if choice == 0:
            r = randrange(4)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("I miss the beautiful cherry blossoms in Japan..."),
                     _("If you ever visit, perhaps we can go admire the scenery together?")],
                    ["Yagyu Kasumi", "Yagyu Kasumi"]
                )
            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Sometimes, I think my father worries too much about the clan name..."),
                     _("Maybe it's something that's important to men, but for me, I just want to live a peaceful life with the one that I love."),
                     _("Have you found such a person?"),
                     _("Not yet, but perhaps soon...")],
                    ["Yagyu Kasumi", "Yagyu Kasumi", "you", "Yagyu Kasumi"]
                )
            else:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Heart and trustiness;\nTighten our relationship;\nInseparable"),
                     _("Hmmm? Who are you talking about?"),
                     _("Ah, don't mind me... It's just a haiku...")],
                    ["Yagyu Kasumi", "you", "Yagyu Kasumi"]
                )

        elif choice == 1:
            if self.game.taskProgressDic["yagyu_clan"] < 2:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Perhaps another time. I'm not feeling too well right now").format(self.game.character)],
                    ["Yagyu Kasumi"]
                )
            elif self.game.taskProgressDic["yagyu_clan"] >= 2:
                r = randrange(4)
                if r == 0 or r == 1:
                    self.game.generate_dialogue_sequence(
                        self.game.yagyu_clan_inner_court_win,
                        "Yagyu Clan Inner Court BG.png",
                        [_("*You walk around the garden with Kasumi, chatting about cherry blossoms, tea, and other pleasant diversions.*"),
                         _("Thank you for spending time with me, {}-san. I truly value our friendship.").format(self.game.character),
                         _("Same here! Let's do this again sometime.")],
                        ["Blank", "Yagyu Kasumi", "you"]
                    )
                elif r == 2:
                    self.game.generate_dialogue_sequence(
                        self.game.yagyu_clan_inner_court_win,
                        "Yagyu Clan Inner Court BG.png",
                        [_("*You walk around the garden with Kasumi, telling her about your past adventures.*"),
                         _("I love hearing your stories, {}-san! You have many interesting experiences.").format(self.game.character),
                         _("I have many more things to tell you! Maybe next time?"),
                         _("Yes, please do visit again soon!")],
                        ["Blank", "Yagyu Kasumi", "you", "Yagyu Kasumi"]
                    )
                else:
                    self.game.generate_dialogue_sequence(
                        self.game.yagyu_clan_inner_court_win,
                        "Yagyu Clan Inner Court BG.png",
                        [_("Do excuse me, {}-san! My father has given me some important errands..").format(self.game.character),
                         _("Let's try for another time?")],
                        ["Yagyu Kasumi", "Yagyu Kasumi"]
                    )


    def spar_with_yagyu_kasumi(self):
        self.game.generate_dialogue_sequence(
            self.game.yagyu_clan_inner_court_win,
            "Yagyu Clan Inner Court BG.png",
            [_("Please execuse me, {}-san. Women in the family are not permitted to learn martial arts.").format(self.game.character),
             _("If you are interested, Ashikaga-san and Yamamoto-san can entertain you.")],
            ["Yagyu Kasumi", "Yagyu Kasumi"]
        )


    def clicked_on_yagyu_tadashi(self):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        for widget in self.game.yagyu_clan_inner_court_win_F1.winfo_children():
            widget.destroy()

        Button(self.game.yagyu_clan_inner_court_win_F1, text=_("Talk"),
               command=self.talk_to_yagyu_tadashi).grid(row=1, column=0)

        Button(self.game.yagyu_clan_inner_court_win_F1, text=_("Spar (Yagyu Tadashi)"),
               command=self.spar_with_yagyu_tadashi).grid(row=2, column=0)


        if self.game.taskProgressDic["yagyu_clan_join"] == 1 and self.game.taskProgressDic["yagyu_clan"] not in [111111111]:
            Button(self.game.yagyu_clan_inner_court_win_F1, text=_("What should I do again?"),
                   command=partial(self.talk_to_yagyu_tadashi,12)).grid(row=3, column=0)
        elif self.game.taskProgressDic["yagyu_clan"] not in [111111111]:
            Button(self.game.yagyu_clan_inner_court_win_F1, text=_("What do you plan to do in China?"),
                   command=partial(self.talk_to_yagyu_tadashi, 2)).grid(row=3, column=0)

        if self.game.taskProgressDic["yagyu_clan_join"] == 1 and self.game.taskProgressDic["yagyu_clan"] not in [111111111]:
            Button(self.game.yagyu_clan_inner_court_win_F1, text=_("Learn"),
                   command=partial(self.talk_to_yagyu_tadashi,13)).grid(row=4, column=0)
        elif self.game.taskProgressDic["yagyu_clan"] not in [111111111]:
            Button(self.game.yagyu_clan_inner_court_win_F1, text=_("Can I join your clan?"),
                   command=partial(self.talk_to_yagyu_tadashi,3)).grid(row=4, column=0)


    def talk_to_yagyu_tadashi(self, choice=0):
        if choice == 0:
            r = randrange(5)
            if r == 0:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Predator and prey;\nOnly the mighty prevail;\nThe weak shall perish")],
                    ["Yagyu Tadashi"]
                )
            elif r == 1:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Of flowers, the cherry blossom; of men, the warrior...")],
                    ["Yagyu Tadashi"]
                )
            elif r == 2:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("I will not let my ancestors down."),
                     _("The name of Yagyu shall thrive, even if it means death for me.")],
                    ["Yagyu Tadashi", "Yagyu Tadashi"]
                )
            else:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("You must remember this."),
                     _("When you strike, you hit or miss."),
                     _("Miss, and you may die."),
                     _("The fundamental things of war... taught to a samurai...")],
                    ["Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi"]
                )

        elif choice == 2:
            if self.game.taskProgressDic["yagyu_clan"] < 2:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("To make the name of Yagyu great across the land, of course."),
                     _("As for the details, we can speak of that another time. Come! Take some tea!")],
                    ["Yagyu Tadashi", "Yagyu Tadashi"]
                )
            elif self.game.taskProgressDic["yagyu_clan"] == 2:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("I am glad you asked, my friend!"),
                     _("After sparring with you, I've decided to invite you to join my cause."),
                     _("There is much work that needs to be done in order to ensure that all peoples revere the Yagyu Clan."),
                     _("My plan is to overthrow Shaolin and take its place as the #1 sect in the Martial Arts Community."),
                     _("That's just an initial step; once we've gained the attention and respect of all the sects, we can slowly form alliances."),
                     _("Those who wish to learn the Way of the Warrior and the deadly techniques of Ninjutsu will need to pledge their allegiance to the Yagyu Clan."),
                     _("From there, we can spread our fame and control through the Martial Arts Community!"), #Row 2
                     _("These warriors I've brought with me are the top Samurai in Japan. Their loyalty and strength are unmatched."),
                     _("Furthermore, I have the support of Hattori Hasegawa, descendant of the renowned ninja Hattori Hanzo."),
                     _("He and his men will gather crucial information that will ensure successful execution of our plans."),
                     _("They will also assassinate anyone who gets in our way."),
                     _("Victory is imminent! And you can be a part of this, {}-san!").format(self.game.character),
                     _("What do you say? I very much look forward to our collaboration!"),
                     ],
                    ["Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi",
                     "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi"],
                    [[_("I would be most honored to assist you in any way I can!"), partial(self.yagyu_invitation_decision, 0)],
                     [_("I cannot allow you to do this!"), partial(self.yagyu_invitation_decision, 1)],
                     [_("I need some time to think about it..."), partial(self.yagyu_invitation_decision, 2)]]
                )
            elif self.game.taskProgressDic["yagyu_clan"] >= 3:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Fame and honor lie before us, my friend!"),
                     _("I yearn to share the joy of a glorious victory with you!")],
                    ["Yagyu Tadashi", "Yagyu Tadashi"]
                )


        elif choice == 3:
            if self.game.taskProgressDic["yagyu_clan"] < 3:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Hmmm, I'm not sure I can make that decision at the moment."),
                     _("Perhaps we can discuss this at a later time...")],
                    ["Yagyu Tadashi", "Yagyu Tadashi"]
                )
                messagebox.showinfo("", _("You must agree to help Yagyu Tadashi with his vision of grandeur before he considers accepting you into the clan."))

            elif self.game.taskProgressDic["join_sect"] == 800:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Most auspicious day, my friend! You could not have spoken truer words!"),
                     _("My heart is overjoyed! Welcome! The Yagyu Clan has gained another strong warrior today!"),
                     _("The pleasure is mine, Yagyu-san. I will bring honor and fame to the clan!"),
                     _("Brilliant! Starting from today, you will have access to the Yagyu Dungeon, where you can train with Hattori-san's men."),
                     _("If you wish, I can also teach you a thing or two..."),
                     _("I will train hard and make myself useful as soon as I can!")],
                    ["Yagyu Tadashi", "Yagyu Tadashi", "you", "Yagyu Tadashi", "Yagyu Tadashi", "you"]
                )
                self.game.taskProgressDic["yagyu_clan_join"] = 1
                self.game.yagyu_clan_inner_courtF4.place(x=self.w * 3 // 4 - 116 // 2,
                                                         y=self.h // 2 + 116 // 4)
                self.clicked_on_yagyu_tadashi()

            else:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("You have a solid foundation, but unfortunately, I see that you've joined a sect previously."),
                     _("As such, I cannot accept you into the Yagyu Clan.")],
                    ["Yagyu Tadashi", "Yagyu Tadashi"]
                )
                messagebox.showinfo("", _("You need to have chosen the 'Rogue' option when joining a sect to be accepted into the Yagyu Clan."))

        elif choice == 12:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_inner_court_win,
                "Yagyu Clan Inner Court BG.png",
                [_("Speak to Ashikaga. He will give you precise instructions.")],
                ["Yagyu Tadashi"]
            )

        elif choice == 13:
            if _("Kendo") not in [m.name for m in self.game.sml]:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Mastery of the sword is essential for any warrior."),
                     _("Here, allow me to teach you..."),
                     _("*You learned a new move: 'Kendo'!*")],
                    ["Yagyu Tadashi", "Yagyu Tadashi", "Blank"]
                )
                new_move = special_move(name=_("Kendo"))
                self.game.learn_move(new_move)

            elif _("Bushido") not in [s.name for s in self.game.skills]:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Righteousness, heroic courage, benevolence, respect, honesty, honour, loyalty, and self control..."),
                     _("A true warrior must lack none of these. Now listen carefully..."),
                     _("*You learned a new skill: 'Bushido'!*")],
                    ["Yagyu Tadashi", "Yagyu Tadashi", "Blank"]
                )
                new_skill = skill(name=_("Bushido"))
                self.game.learn_skill(new_skill)

            else:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("I have nothing more to teach you at the moment."),
                     _("Perhaps you can learn a few interesting techniques from Hattori-san's ninjas...")],
                    ["Yagyu Tadashi", "Yagyu Tadashi"]
                )



    def yagyu_invitation_decision(self, choice):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()
        if choice == 0:
            self.game.taskProgressDic["yagyu_clan"] = 3
            self.game.chivalry -= 50
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_inner_court_win,
                "Yagyu Clan Inner Court BG.png",
                [_("Your chivalry -50."),
                 _("Excellent! Let us march forwards in steadfast alliance!"),
                 _("Our enemies will tremble before our might!"),
                 _("Indeed, Yagyu-san! Now, what is my first mission?"),
                 _("You waste no time, my friend! I like it!"),
                 _("Speak to Ashikaga; he will tell you the details. I look forward to news of your success!")],
                ["Blank", "Yagyu Tadashi", "Yagyu Tadashi","you", "Yagyu Tadashi", "Yagyu Tadashi"]
            )

        elif choice == 1:
            self.game.taskProgressDic["yagyu_clan"] = -3
            self.game.chivalry += 20
            messagebox.showinfo("", _("Your chivalry +20!"))
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_inner_court_win,
                "Yagyu Clan Inner Court BG.png",
                [_("I strongly urge you to reconsider--"),
                 _("No need. I cannot do such a dishonorable thing. It would disturb the peace within the Martial Arts Community."),
                 _("Plus, it is naive to think that all sects will bow to you."),
                 _("What a shame... So we end on a note of disagreement..."),
                 _("I hope you do not live to regret your decision..."),
                 _("Unfortunately, I must inform you that you are no longer welcome here."),
                 _("Father, isn't that a bit too--"),
                 _("Silence, Kasumi! It is not a proper time for you to speak."), #Row 2
                 _("Yamamoto... Please escort the guest out..."),
                 _("Yes, Yagyu-san."),
                 _("{}-san, please...").format(self.game.character),
                 _("Very well, take care, Yagyu-san. I sincerely hope you will reconsider your plans...")],
                ["Yagyu Tadashi", "you", "you", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Kasumi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Yamamoto Takeshi", "Yamamoto Takeshi", "you"]
            )
            self.game.winSwitch(self.game.yagyu_clan_inner_court_win, self.game.yagyu_clan_win)

        else:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_inner_court_win,
                "Yagyu Clan Inner Court BG.png",
                [_("Not a problem, my friend!"),
                 _("I realize this is not a simple decision to make. Please, take some time to think about it."),
                 _("I hope to hear from you soon.")],
                ["Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi"]
            )


    def spar_with_yagyu_tadashi(self):
        self.game.generate_dialogue_sequence(
            self.game.yagyu_clan_inner_court_win,
            "Yagyu Clan Inner Court BG.png",
            [_("I believe it is my duty to entertain my guests."),
             _("Let us exchange ideas and techniques so that we may both grow stronger as a result.")],
            ["Yagyu Tadashi", "Yagyu Tadashi"]
        )

        opp = character(_("Yagyu Tadashi"), 22,
                        sml=[special_move(_("Kendo"), level=10),
                             special_move(_("Hachiman Blade"), level=10)],
                        skills=[skill(_("Bushido"), level=10),
                                skill(_("Kenjutsu I"), level=10),
                                skill(_("Kenjutsu II"), level=10),
                                skill(_("Ninjutsu I"), level=10)],
                        preferences=[1, 3, 3, 1, 1])

        self.game.battleID = "yagyu_tadashi_spar"

        self.game.battleMenu(self.game.you, opp, "Yagyu Clan Inner Court BG.png",
                             "Sakura_Variation.mp3", fromWin=self.game.yagyu_clan_inner_court_win,
                             battleType="training", destinationWinList=[self.game.yagyu_clan_inner_court_win] * 3,
                             postBattleCmd=self.post_battle)


    def post_battle(self, stage):
        self.game.battleID = None
        if stage == 1:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan BG.png",
                [_("I am impressed by your strength."),
                 _("Please, do come in to have some tea with Yagyu-san. I believe he will have much to discuss with you.")],
                ["Ashikaga Yoshiro", "Ashikaga Yoshiro"]
            )
            self.game.taskProgressDic["yagyu_clan"] = 0
            self.go_inner_court()


        elif stage == -1:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan BG.png",
                [_("Hahahahaha! It seems that you still have room for improvement."),
                 _("But do not be discouraged. After all, warriors of the Yagyu Clan are far superior to commoners."),
                 _("Instead, take pride in the fact that you were considered worthy to spar with us.")],
                ["Ashikaga Yoshiro", "Ashikaga Yoshiro", "Ashikaga Yoshiro"]
            )
            self.game.yagyu_clan_win.deiconify()


        elif stage == 2:
            if self.game.taskProgressDic["yagyu_clan"] == 1:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Impressive! I am intrigued by some of your techniques!"),
                     _("Likewise, Yagyu-san!"),
                     _("I have not sparred such a strong opponent in quite a while. What an honor!")],
                    ["Yagyu Tadashi", "you", "Yagyu Tadashi"]
                )
                self.game.taskProgressDic["yagyu_clan"] = 2

            else:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_clan_inner_court_win,
                    "Yagyu Clan Inner Court BG.png",
                    [_("Impressive! You continue to improve every day!")],
                    ["Yagyu Tadashi"]
                )

        elif stage == -2:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_clan_inner_court_win,
                "Yagyu Clan Inner Court BG.png",
                [_("I managed to win by a stroke of luck. I may not be so fortunate next time!")],
                ["Yagyu Tadashi"]
            )


    def post_dungeon_mini_game(self, stage):
        if stage == 1:
            self.game.taskProgressDic["yagyu_dungeon"] = 2
            self.game.saveGame(auto_save=True)
            time.sleep(.25)
            self.game.mini_game_in_progress = True
            self.game.mini_game = yagyu_dungeon_level_2_mini_game(self.game, self)
            self.game.mini_game.new()
        elif stage == 2:
            self.game.taskProgressDic["yagyu_dungeon"] = 3
            self.game.saveGame(auto_save=True)
            time.sleep(.25)
            self.game.mini_game_in_progress = True
            self.game.mini_game = yagyu_dungeon_level_3_mini_game(self.game, self)
            self.game.mini_game.new()
        elif stage == 3:
            self.game.taskProgressDic["yagyu_dungeon"] = 4
            self.game.saveGame(auto_save=True)
            time.sleep(.25)
            self.game.stopSoundtrack()
            self.game.currentBGM = "txdy_yagyu.mp3"
            self.game.startSoundtrackThread()
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Dungeon BG.png",
                [_("You have come at last..."),
                 _("Where is the Abbot? And where is Kasumi?"),
                 _("They are in the door behind me... You are free to rescue them, if you can get past me, that is..."),
                 _("But you'd be a fool to think you can escape alive. I have long anticipated your coming."),
                 _("Oh? And why is that?"),
                 _("Did you really think Hattori's ninjas were unable to detect your presence?"),
                 _("And were you so naive to think Kasumi could steal the keys from me if I hadn't purposely let her?"), #Row 2
                 _("All of this was a trap to lure you in... It's too bad. We could've been comrades."),
                 _("Instead, you chose to oppose me. Now you die. Then no one will stand in my way."),
                 _("Too much talk. Where's the action?"),
                 _("Very well... Ashikaga, Yamamoto, please warm up with our guest here..."),
                 _("Our pleasure, Yagyu-san...")],
                ["Yagyu Tadashi", "you", "Yagyu Tadashi", "Yagyu Tadashi", "you", "Yagyu Tadashi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "you", "Yagyu Tadashi", "Ashikaga & Yamamoto"]
            )

            opp = character(_("Ashikaga & Yamamoto"), 22,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Hachiman Blade"), level=8)],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Kenjutsu II"), level=7)],
                            multiple=2)
            self.game.battleID = "ashikaga_yamamoto"
            cmd = lambda:self.post_dungeon_mini_game(4)
            self.game.battleMenu(self.game.you, opp, "Yagyu Clan Dungeon BG.png",
                                 "txdy_yagyu.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[None] * 3,
                                 postBattleCmd=cmd)

        elif stage == 4:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Dungeon BG.png",
                [_("I'm sick of playing around with you..."),
                 _("*As Ashikaga and Yamamoto lay on the ground, groaning, you forcefully jab their Tianding acupuncture points.*"),
                 _("*Immediately, their eyes roll to the back of their heads, and they breathed their last.*"),
                 _("NOOOO!!!"),
                 _("That's what happens when you are a coward and send your men in before you."),
                 _("They... they've been with me for nearly 2 decades... I..."),
                 _("I promised them honor and riches... Never have I seen such faithful and courageous men..."),
                 _("I had planned on ruling the Martial Arts community with them by my side... but now..."), #Row 2
                 _("What is the meaning of all this? There is no one worthy with whom to share the glory and splendor..."),
                 _("Was I really wrong...?"),
                 _("You were far too ambitious. Do you not realize the impossibility of what you're trying to accomplish?"),
                 _("Now release the Abbot and Kasumi before I take you out too..."),
                 _("Go... go ahead... they are in the room behind me... Do what you wish, but please..."),
                 _("Allow me to bury Ashikaga and Yamamoto. It's the last thing I can do for them..."), #Row 3
                 _("I have no interest in beating their dead bodies."),
                 _("Thank you..."),
                 _("*Yagyu Tadashi bends down to pick up the bodies of his men while you rush into the room behind him.*")],
                ["you", "Blank", "Blank", "Yagyu Tadashi", "you", "Yagyu Tadashi", "Yagyu Tadashi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "you", "you", "Yagyu Tadashi",
                 "Yagyu Tadashi", "you", "Yagyu Tadashi", "Blank"]
            )

            self.game.currentEffect = "door_unlock.wav"
            self.game.startSoundEffectThread()
            time.sleep(.25)

            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Dungeon BG.png",
                [_("Abbot! Kasumi! I've come to rescue you!"),
                 _("{}-san! I knew you would come!").format(self.game.character),
                 _("Kasumi, are you ok?"),
                 _("Yeah, I am my father's daughter after all... The Abbot's not looking too good though."),
                 _("He's been here far longer than me and enduring far more beatings."),
                 _("*You glance over at the Abbot, who appears to be unresponsive, and notice the many bruises and cuts on his body.*"),
                 _("Let me untie you guys so we can get out of here."),
                 _("*You untie the ropes that are binding Kasumi.*"),
                 _("Thanks, {}-san! Did my father try to hurt you?").format(self.game.character), #Row 2
                 _("Uh, he... yes, but I think we've come to an agreement..."),
                 _("I'll carry the Abbot; you can walk on your own, right?"),
                 _("Yeah, I can."),
                 _("*You bend down and begin untying the Abbot.*"),
                 _("I'd better get him to Nongyuan Valley soon. He looks seriously injured...")],
                ["you", "Yagyu Kasumi", "you", "Yagyu Kasumi", "Yagyu Kasumi", "Blank", "you", "Blank",
                 "Yagyu Kasumi", "you", "you", "Yagyu Kasumi", "Blank", "you"]
            )

            self.game.stopSoundtrack()
            self.game.currentEffect = "sfx_sword6short.wav"
            self.game.startSoundEffectThread()
            self.game.currentBGM = "xlfd_suspense.mp3"
            self.game.startSoundtrackThread()
            time.sleep(.25)

            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Dungeon BG.png",
                [_("*You feel a sharp blade enter your body from behind. You roll forward instinctively and turn around to see Kasumi holding a small knife.*"),
                 _("K-Kasumi... Why the heck did you do that?"),
                 _("Hahahahahaha!"),
                 _("Ahahahahaha! Well done, Hattori-san."),
                 _("Ha-Hattori....??"),
                 _("You poor fool... Looks like you still don't get it..."),
                 _("Well, here's a little surprise for you. I, Yagyu Tadashi, never married nor had any children."),
                 _("The Kasumi that you know is but a fake persona. HAHAHAHAHAHA!"), #Row 2
                 _("Then she... she..."),
                 _("Ah yes, when it comes to disguises, none is more proficient than the ninja!"),
                 _("Hattori-san is so skilled that not even Ashikaga or Yamamoto knew..."),
                 _("Since you're about to be a dead man, I thought I'd let you in on the secret... Hahahahahaha!"),
                 _("{}-san, since you've been so kind to me, perhaps I should reveal my true self to you...").format(self.game.character)
                 ],
                ["Blank", "you", "Yagyu Kasumi", "Yagyu Tadashi", "you", "Yagyu Tadashi", "Yagyu Tadashi",
                 "Yagyu Tadashi", "you", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Kasumi"]
            )

            self.game.currentEffect = "sfx_attack_missed2.wav"
            self.game.startSoundEffectThread()

            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Dungeon BG.png",
                [_("How do I look, {}-san? Hahahahahaha!").format(self.game.character),
                 _("You... you creep..."),
                 _("I've been so eager to crush you with my own hands. Now the day has finally come..."),
                 _("Go ahead and do the honors, Hattori-san. This will be a fantastic show for me.")
                 ],
                ["Hattori Hasegawa", "you", "Hattori Hasegawa", "Yagyu Tadashi"]
            )

            opp_list = []
            opp = character(_("Hattori Hasegawa"), 26 + self.game.ptc,
                            sml=[special_move(_("Hachiman Blade"), level=10),
                                 special_move(_("Kusarigama"), level=10),
                                 special_move(_("Superior Smoke Bomb"), level=10),
                                 special_move(_("Hensojutsu"), level=10),],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Ninjutsu I"), level=10),
                                    skill(_("Ninjutsu II"), level=10),
                                    skill(_("Ninjutsu III"), level=10)])
            opp_list.append(opp)
            opp = character(_("Yagyu Tadashi"), 26 + self.game.ptc,
                            sml=[special_move(_("Kendo"), level=10),
                                 special_move(_("Hachiman Blade"), level=10)],
                            skills=[skill(_("Bushido"), level=10),
                                    skill(_("Kenjutsu I"), level=10),
                                    skill(_("Kenjutsu II"), level=10),
                                    skill(_("Ninjutsu I"), level=10)],
                            preferences=[1, 3, 3, 1, 1])
            opp_list.append(opp)

            self.game.battleID = "yagyu_hattori_battle"
            cmd = lambda: self.post_dungeon_mini_game(5)
            self.game.battleMenu(self.game.you, opp_list, "Yagyu Clan Dungeon BG.png",
                                 "xlfd_suspense.mp3", fromWin=None,
                                 battleType="task", destinationWinList=[None] * 3,
                                 postBattleCmd=cmd)

        elif stage == 5:
            self.game.battleID = None
            self.game.stopSoundtrack()
            self.game.taskProgressDic["yagyu_clan"] = -100
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Inner Court Night BG.png",
                [_("*You carry the injured Abbot out and meet up with Elder Xuanjing*"),
                 _("{}! Abbot! Quick! Give them some Big Recovery Pills and take them back to the temple!").format(self.game.character),
                 ],
                ["Blank", "Elder Xuanjing"]
            )
            self.game.restore(0,0,full=True)
            self.game.currentBGM = "jy_shaolin1.mp3"
            self.game.startSoundtrackThread()
            self.game.generate_dialogue_sequence(
                None,
                "Shaolin.png",
                [_("*3 days later*"),
                 _("{}, all of Shaolin owes you a big favor.").format(self.game.character),
                 _("You risked your life to save the Abbot, and the Martial Arts community is at peace once again because of you."),
                 _("I will make sure that everyone hears of your bravery."),
                 _("Glad I could help, Elder Xuanjing. How's the Abbot doing?"),
                 _("He was poisoned and beatened pretty badly, but luckily, he knows the Tendon Changing Technique."),
                 _("With good rest and proper care, he will recover fully in 2-3 months."),
                 _("Very relieved to hear that! By the way, what happened to the Yagyu Clan?"), #Row 2
                 _("We took care of most of the samurai, and with their leaders dead, the clan is vacant now."),
                 _("You should go back there when you get a chance. Maybe you'll find some valuable things."),
                 _("Great idea! I'll be sure to do that. Thank you again for taking care of me."),
                 _("Farewell, my friend.")
                 ],
                ["Blank","Elder Xuanjing","Elder Xuanjing","Elder Xuanjing","you","Elder Xuanjing","Elder Xuanjing",
                 "you", "Elder Xuanjing", "Elder Xuanjing", "you", "Elder Xuanjing"]
            )

            self.game.chivalry += 100
            messagebox.showinfo("", _("Your chivalry + 100!"))
            self.game.mapWin.deiconify()


    def picked_up_dungeon_chest(self):
        self.game.add_item(_("Smoke Bomb"), 300)
        if _("Smoke Bomb") in [m.name for m in self.game.sml]:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Study BG.png",
                [_("You open the chest and find 'The Art of Ninjutsu: Volume I' along with 300 Smoke Bombs."),
                 _("You read the book and learn the move 'Superior Smoke Bomb'.")],
                ["Blank", "Blank"],
            )
            new_move = special_move(name=_("Superior Smoke Bomb"))
            self.game.learn_move(new_move)
        else:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Study BG.png",
                [_("You open the chest and find 'The Art of Ninjutsu: Volume I' along with 300 Smoke Bombs."),
                 _("You read the book and learn the move 'Smoke Bomb'.")],
                ["Blank", "Blank"],
            )
            new_move = special_move(name=_("Smoke Bomb"))
            self.game.learn_move(new_move)

        self.game.mini_game.running = True


    def go_yagyu_dungeon(self):

        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        self.game.stopSoundtrack()
        self.game.currentBGM = "jy_suspenseful1.mp3"
        self.game.startSoundtrackThread()

        self.game.yagyu_clan_inner_court_win.withdraw()
        self.game.yagyu_dungeon_win = Toplevel(self.game.yagyu_clan_inner_court_win)
        self.game.yagyu_dungeon_win.title(_("Yagyu Dungeon"))

        bg = PhotoImage(file="Yagyu Clan Dungeon BG.png")
        self.w = bg.width()
        self.h = bg.height()

        self.canvas = Canvas(self.game.yagyu_dungeon_win, width=self.w, height=self.h)
        self.canvas.pack()
        self.canvas.create_image(0, 0, anchor=NW, image=bg)

        self.game.yagyu_dungeonF1 = Frame(self.canvas)
        pic1 = PhotoImage(file="Hattori Ninja_icon_large.ppm")
        imageButton1 = Button(self.game.yagyu_dungeonF1, command=self.clicked_on_hattori_ninja)
        imageButton1.grid(row=0, column=0)
        imageButton1.config(image=pic1)
        label = Label(self.game.yagyu_dungeonF1, text=_("Hattori Ninja"))
        label.grid(row=1, column=0)
        self.game.yagyu_dungeonF1.place(x=self.w * 1 // 4 - pic1.width() // 2,
                                                 y=self.h // 4 - pic1.height() // 2)

        self.game.yagyu_dungeonF3 = Frame(self.canvas)
        pic3 = PhotoImage(file="yagyu_clan_inner_court_icon.png")
        imageButton3 = Button(self.game.yagyu_dungeonF3,
                              command=partial(self.game.winSwitch, self.game.yagyu_dungeon_win,
                                              self.game.yagyu_clan_inner_court_win))
        imageButton3.grid(row=0, column=0)
        imageButton3.config(image=pic3)
        label = Label(self.game.yagyu_dungeonF3, text=_("Inner Court"))
        label.grid(row=1, column=0)
        self.game.yagyu_dungeonF3.place(x=self.w * 1 // 4 - pic3.width() // 2,
                                                 y=self.h // 2 + pic3.height() // 4)


        menu_frame = self.game.create_menu_frame(self.game.yagyu_dungeon_win)
        menu_frame.pack()
        self.game.yagyu_dungeon_win_F1 = Frame(self.game.yagyu_dungeon_win)
        self.game.yagyu_dungeon_win_F1.pack()

        self.game.yagyu_dungeon_win.protocol("WM_DELETE_WINDOW", self.game.on_exit)
        self.game.yagyu_dungeon_win.update_idletasks()
        toplevel_w, toplevel_h = self.game.yagyu_dungeon_win.winfo_width(), self.game.yagyu_dungeon_win.winfo_height()
        self.game.yagyu_dungeon_win.geometry(
            "+%d+%d" % (SCREEN_WIDTH // 2 - toplevel_w // 2, 5))
        self.game.yagyu_dungeon_win.focus_force()
        self.game.yagyu_dungeon_win.mainloop()  ###


    def clicked_on_hattori_ninja(self):
        self.game.currentEffect = "button-20.mp3"
        self.game.startSoundEffectThread()

        for widget in self.game.yagyu_dungeon_win_F1.winfo_children():
            widget.destroy()

        move_names = [m.name for m in self.game.sml]
        if _("Smoke Bomb") in move_names or _("Superior Smoke Bomb") in move_names:
            Button(self.game.yagyu_dungeon_win_F1, text=_("I need more smoke bombs."),
                   command=partial(self.talk_to_hattori_ninja, 1)).grid(row=1, column=0)

        Button(self.game.yagyu_dungeon_win_F1, text=_("Talk"),
               command=partial(self.talk_to_hattori_ninja, 2)).grid(row=2, column=0)

        Button(self.game.yagyu_dungeon_win_F1, text=_("Practice Smoke Bombs"),
               command=partial(self.talk_to_hattori_ninja, 3)).grid(row=3, column=0)

        Button(self.game.yagyu_dungeon_win_F1, text=_("Practice Ninjutsu"),
               command=partial(self.talk_to_hattori_ninja, 4)).grid(row=4, column=0)


    def talk_to_hattori_ninja(self, choice):
        if choice == 1:
            if self.game.check_for_item(_("Smoke Bomb"), 50):
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_dungeon_win,
                    "Yagyu Clan Dungeon BG.png",
                    [_("It looks like you have plenty of smoke bombs left. Come back when you need more.")],
                    ["Hattori Ninja"]
                )
            else:
                self.game.inv[_("Smoke Bomb")] = 50
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_dungeon_win,
                    "Yagyu Clan Dungeon BG.png",
                    [_("Here's some more for you."),
                     _("*Received some Smoke Bombs.*")],
                    ["Hattori Ninja", "Blank"]
                )

        elif choice == 2:
            self.game.generate_dialogue_sequence(
                self.game.yagyu_dungeon_win,
                "Yagyu Clan Dungeon BG.png",
                [_("There's no time to talk, only to train.")],
                ["Hattori Ninja"]
            )

        elif choice == 3:
            if _("Superior Smoke Bomb") not in [m.name for m in self.game.sml]:
                self.game.generate_dialogue_sequence(
                    self.game.yagyu_dungeon_win,
                    "Yagyu Clan Dungeon BG.png",
                    [_("Allow me to teach you the true technique for using Smoke Bombs."),
                     _("*The Hattori Ninja teaches you 'Superior Smoke Bomb'.*"),
                     _("Thanks!"),
                     _("Now, let's get you some practice...")],
                    ["Hattori Ninja", "Blank", "you", "Hattori Ninja"]
                )

                new_move = special_move(name=_("Superior Smoke Bomb"))
                self.game.learn_move(new_move)

            self.game.yagyu_dungeon_win.withdraw()
            smoke_bomb_move = [m for m in self.game.sml if _("Superior Smoke Bomb") in m.name][0]

            self.game.mini_game_in_progress = True
            if smoke_bomb_move.level >= 5:
                self.game.mini_game = hattori_smoke_bomb_training_level_2_mini_game(self.game, self)
            else:
                self.game.mini_game = hattori_smoke_bomb_training_level_1_mini_game(self.game, self)
            self.game.mini_game.new()

        elif choice == 4:
            self.game.yagyu_dungeon_win.withdraw()
            if _("Ninjutsu I") not in [s.name for s in self.game.skills]:
                self.game.generate_dialogue_sequence(
                    None,
                    "Yagyu Clan Dungeon BG.png",
                    [_("So you wish to learn the way of the Ninja, eh?"),
                     _("Agility and stealth are the most important skills for a ninja."),
                     _("Why don't you try to navigate through this dungeon?"),
                     _("Prove your competency by getting through within the allotted time."),],
                    ["Hattori Ninja", "Hattori Ninja", "Hattori Ninja", "Hattori Ninja"]
                )

                self.game.mini_game_in_progress = True
                self.game.mini_game = hattori_ninjutsu_level_1_mini_game(self.game, self)
                #self.game.mini_game = hattori_ninjutsu_level_2_mini_game(self.game, self)
                self.game.mini_game.new()

            else:
                ninjutsu_skill = [s for s in self.game.skills if s.name == _("Ninjutsu I")][0]
                if ninjutsu_skill.level == 10:
                    if _("Superior Smoke Bomb") in [m.name for m in self.game.sml]:
                        num_enemies = 3
                        if _("Ninjutsu II") not in [s.name for s in self.game.skills]:
                            self.game.generate_dialogue_sequence(
                                None,
                                "Yagyu Clan Dungeon BG.png",
                                [_("So... you have mastered the fundamentals of Ninjutsu."),
                                 _("Let's see if you are ready to learn something more advanced...")],
                                ["Hattori Ninja", "Hattori Ninja"]
                            )
                        else:
                            ninjutsu_skill = [s for s in self.game.skills if s.name == _("Ninjutsu II")][0]
                            num_enemies = 3 + ninjutsu_skill.level//4

                        self.game.mini_game_in_progress = True
                        self.game.mini_game = hattori_ninjutsu_level_4_mini_game(self.game, self, num_enemies)
                        self.game.mini_game.new()

                    else:
                        self.game.generate_dialogue_sequence(
                            None,
                            "Yagyu Clan Dungeon BG.png",
                            [_("So... you have mastered the fundamentals of Ninjutsu."),
                             _("Before continuing, come talk to me about how to use smoke bombs...")],
                            ["Hattori Ninja", "Hattori Ninja"]
                        )

                else:
                    self.game.generate_dialogue_sequence(
                        self.game.yagyu_dungeon_win,
                        "Yagyu Clan Dungeon BG.png",
                        [_("Let's have you try something more challenging...")],
                        ["Hattori Ninja"]
                    )

                    self.game.mini_game_in_progress = True
                    self.game.mini_game = hattori_ninjutsu_level_3_mini_game(self.game, self, 150 - ninjutsu_skill.level*10)
                    self.game.mini_game.new()


    def post_ninjutsu_training(self, level_passed):
        if level_passed == 1:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Dungeon BG.png",
                [_("Not bad for a warm up..."),
                 _("Let's see how you do in the next one..."),],
                ["Hattori Ninja", "Hattori Ninja"]
            )
            self.game.mini_game_in_progress = True
            self.game.mini_game = hattori_ninjutsu_level_2_mini_game(self.game, self)
            self.game.mini_game.new()

        elif level_passed == 2:
            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan Dungeon BG.png",
                [_("I see there's some potential in you... Very well..."),
                 _("I shall teach you the fundamentals of Ninjutsu."),
                 _("*You learned a new skill: 'Ninjutsu I'!*"),
                 _("Come speak to me again once you are ready to learn something more advanced.")],
                ["Hattori Ninja", "Hattori Ninja", "Blank", "Hattori Ninja"]
            )
            new_skill = skill(_("Ninjutsu I"))
            self.game.learn_skill(new_skill)
            self.game.yagyu_dungeon_win.deiconify()

        elif level_passed == 3:
            ninjutsu_skill = [s for s in self.game.skills if s.name == _("Ninjutsu I")][0]
            if ninjutsu_skill.level < 10:
                ninjutsu_skill.upgrade_skill(1)
                messagebox.showinfo("", _("Your level in 'Ninjutsu I' increased by 1!"))
            self.game.yagyu_dungeon_win.deiconify()

        elif level_passed == 4:
            if _("Ninjutsu II") in [s.name for s in self.game.skills]:
                ninjutsu_skill = [s for s in self.game.skills if s.name == _("Ninjutsu II")][0]
                if ninjutsu_skill.level < 10:
                    ninjutsu_skill.upgrade_skill(1)
                    messagebox.showinfo("", _("Your level in 'Ninjutsu II' increased by 1!"))
                else:
                    messagebox.showinfo("", _("You have already mastered 'Ninjutsu II'! There's nothing more to be gained."))
            else:
                self.game.generate_dialogue_sequence(
                    None,
                    "Yagyu Clan Dungeon BG.png",
                    [_("Excellent! You have not disappointed me!"),
                     _("I will now teach you advanced Ninjutsu."),
                     _("*You learned a new skill: 'Ninjutsu II'!*")],
                    ["Hattori Ninja", "Hattori Ninja", "Blank"]
                )
                new_skill = skill(_("Ninjutsu II"))
                self.game.learn_skill(new_skill)

            self.game.yagyu_dungeon_win.deiconify()


    def post_smoke_bomb_training(self, level_passed):
        smoke_bomb_move = [m for m in self.game.sml if _("Superior Smoke Bomb") in m.name][0]
        if level_passed == 1:
            smoke_bomb_move.gain_exp(300)
            self.game.yagyu_dungeon_win.deiconify()
            messagebox.showinfo("", _("Gained 300 experience in 'Superior Smoke Bomb'!"))
        elif level_passed == 2:
            smoke_bomb_move.gain_exp(500)
            self.game.yagyu_dungeon_win.deiconify()
            messagebox.showinfo("", _("Gained 500 experience in 'Superior Smoke Bomb'!"))


    def exit_training(self):
        self.game.mini_game_in_progress = False
        self.game.restore(0, 0, full=True)
        pg.display.quit()
        self.game.yagyu_dungeon_win.deiconify()


    def yagyu_tournament_choice(self, choice):
        self.game.dialogueWin.quit()
        self.game.dialogueWin.destroy()

        opp_ouyang_xiong = character(_("Ouyang Xiong"), 26 + self.game.ptc,
                                        sml=[special_move(_("Violent Dragon Palm"), level=10),
                                             special_move(_("Dragon Roar"), level=10)],
                                        skills=[skill(_("Burst of Potential"), level=10),
                                                skill(_("Basic Agility Technique"), level=10)],
                                        preferences=[5,5,3,3,2])

        opp_tan_keyong = character(_("Tan Keyong"), 26 + self.game.ptc,
                                  sml=[special_move(_("Shanhu Fist"), level=10),
                                       special_move(_("Demon Suppressing Blade"), level=10),],
                                  skills=[skill(_("Divine Protection"), level=10),
                                          skill(_("Leaping Over Mountain Peaks"), level=10),
                                          skill(_("Basic Agility Technique"), level=10)])

        opp_zhang_tianjian = character(_("Zhang Tianjian"), 26 + self.game.ptc,
                                          sml=[special_move(_("Taichi Fist"), level=10),
                                               special_move(_("Taichi Sword"), level=10)],
                                          skills=[skill(_("Wudang Agility Technique"), level=10),
                                                  skill(_("Pure Yang Qi Skill"), level=10),
                                                  skill(_("Taichi 18 Forms"), level=10),
                                                  skill(_("Basic Agility Technique"), level=10)])

        cmd = lambda:self.post_yagyu_tournament(choice)
        if choice == 1:
            self.game.battleMenu(self.game.you, [opp_ouyang_xiong, opp_tan_keyong], "Yagyu Clan BG.png",
                                 "txdy_yagyu.mp3", fromWin=None, battleType="task", destinationWinList=[None] * 3,
                                 postBattleCmd=cmd)
        elif choice == 2:
            self.game.battleMenu(self.game.you, [opp_ouyang_xiong, opp_zhang_tianjian], "Yagyu Clan BG.png",
                                 "txdy_yagyu.mp3", fromWin=None, battleType="task", destinationWinList=[None] * 3,
                                 postBattleCmd=cmd)
        elif choice == 3:
            self.game.battleMenu(self.game.you, [opp_tan_keyong, opp_zhang_tianjian], "Yagyu Clan BG.png",
                                 "txdy_yagyu.mp3", fromWin=None, battleType="task", destinationWinList=[None] * 3,
                                 postBattleCmd=cmd)
        elif choice == 4:
            messagebox.showinfo("", "You sneak away in the midst of all the chaos...")
            self.game.mapWin.deiconify()
            self.game.taskProgressDic["yagyu_clan"] = 999999999


    def post_yagyu_tournament(self, stage):
        if stage <= 3:
            if stage == 1: #beat 2 sect leaders; Yagyu emerges victorious
                opp_name_a = "Ouyang Xiong"
                opp_name_b = "Tan Keyong"
                opp_name_c = "Zhang Tianjian"

            elif stage == 2: #beat 2 sect leaders; Yagyu emerges victorious
                opp_name_a = "Ouyang Xiong"
                opp_name_b = "Zhang Tianjian"
                opp_name_c = "Tan Keyong"

            else: #beat 2 sect leaders; Yagyu emerges victorious
                opp_name_a = "Tan Keyong"
                opp_name_b = "Zhang Tianjian"
                opp_name_c = "Ouyang Xiong"

            self.game.generate_dialogue_sequence(
                None,
                "Yagyu Clan BG.png",
                [_("Enough! Two of your leaders are down. If you continue to resist, I will no longer show any mercy."),
                 _("He is right... They are too strong for us..."),
                 _("It's a shame that your skills in combat were not put to good use, but alas, we have lost."),
                 _("I have nothing to say..."),
                 _("*You look smugly at {} and {}, both of whom have blood running from their mouths.*").format(opp_name_a, opp_name_b),
                 _("You have heard your leaders! Stop your futile resistance and acknowledge your new Supreme Leader..."),
                 _("*Sigh* This is black day for the Martial Arts Community indeed..."),
                 _("Thank you, {}-san. You have been of tremendous help in our achievement today.").format(self.game.character),
                 _("Now, all people of Wulin, I henceforth declare that today, I, Yagyu Tadashi, leader of the Yagyu Clan"),
                 # Row 2
                 _("Accept my new position as the Supreme Leader of Wulin! I am humbled by this most prestigious honor!"),
                 _("I hope we can all look past the unhappy quarrels that preceeded this and move forwards together!"),
                 _("Under my leadership, I assure you that Wulin will become a more peaceful and prosperous place for martial artists across the land!"),
                 ],
                ["you", opp_name_a, opp_name_b, opp_name_b, "Blank", "you", opp_name_c, "Yagyu Tadashi",
                 "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi", "Yagyu Tadashi"],
                [[_("I, {}, shall serve you faithfully, Supreme Leader!").format(self.game.character), partial(self.post_yagyu_tournament,11)],
                 [_("No, wait! I should be the Supreme Leader!"), partial(self.post_yagyu_tournament,21)]]
            )

        else:
            self.game.dialogueWin.quit()
            self.game.dialogueWin.destroy()
            if stage == 11: #let Yagyu become Supreme Leader
                self.game.taskProgressDic["yagyu_clan"] = 111111111
                self.game.taskProgressDic["supreme_leader"] = 0
                self.game.generate_dialogue_sequence(
                    None,
                    "Yagyu Clan BG.png",
                    [_("My heart is overjoyed at hearing those words, dear friend!"),
                     _("Now, as for the rest of you gentlemen, I take it then that you will honor your words?"),
                     _("Yes, we will accept this outcome for now. We hope you will not disappoint us."),
                     _("If you abuse your power and commit dishonorable acts, Wudang will be the first to rise up against you."),
                     _("That's right. If you turn out to be worse than what I perceive you to be, I would rather die than submit to you."),
                     _("I assure you that I will do my best to uphold honor in the Martial Arts Community."),
                     _("Why, if for nothing else, to bring glory and fame to my ancestors and the Yagyu Clan."), #Row 2
                     _("You will see; within a few months, peace and prosperity will thrive under my leadership!"),
                     _("Perhaps you ought to demonstrate your sincerity by releasing the Abbot?"),
                     _("Oh, yes, of course! As I have said before, Abbot is free to go anytime he wishes."),
                     _("In fact, you can find him in the inner court enjoying a cup of tea right now."),
                     _("Please, do join him if you wish, or you can take your leave at any time."), #Row 3
                     _("We appreciate your... kind offer... but we have some pressing matters to attend to."),
                     _("We will meet up with the Abbot now and head back to Shaolin. Farewell."),
                     _("Farewell, dear Shaolin monks, Yagyu Clan has enjoyed this privilege of hosting your Abbot."),
                     _("We too, then, will head home."),
                     _("As will we; take care."), #Row 4
                     _("*One by one, the sects say their goodbyes before departing.*")
                     ],
                    ["Yagyu Tadashi", "Yagyu Tadashi", "Tan Keyong", "Zhang Tianjian", "Ouyang Xiong", "Yagyu Tadashi",
                     "Yagyu Tadashi", "Yagyu Tadashi", "Elder Xuanjing", "Yagyu Tadashi", "Yagyu Tadashi",
                     "Yagyu Tadashi", "Elder Xuanjing", "Elder Xuanjing", "Yagyu Tadashi", "Tan Keyong",
                     "Zhang Tianjian", "Blank"],
                )

                if self.game.taskProgressDic["yagyu_clan_join"] == 1:

                    new_move = special_move(name=_("Hachiman Blade"))
                    self.game.learn_move(new_move)
                    new_skill = skill(name=_("Ninjutsu III"))
                    self.game.learn_skill(new_skill)

                    self.game.generate_dialogue_sequence(
                        None,
                        "Yagyu Clan BG.png",
                        [_("Ahhh... A glorious victory! This would not be possible if it were not for you, my friend!"),
                         _("You will be rewarded heavily for your contribution!"),
                         _("*Yagyu Tadashi teaches you 'Hachiman Blade'.*"),
                         _("I, Hattori Hasegawa, am truly impressed by your loyalty and strength!"),
                         _("Allow me to show you the skills of a true ninja!"),
                         _("*Hattori Hasegawa teaches you 'Ninjutsu III'.*"),
                         _("Thank you for your generosity and trust! I will forever be loyal to the Yagyu Clan!")
                         ],
                        ["Yagyu Tadashi", "Yagyu Tadashi", "Blank", "Hattori Hasegawa", "Hattori Hasegawa",
                         "Blank", "you"],
                    )

                else:
                    new_move = special_move(name=_("Hachiman Blade"))
                    self.game.learn_move(new_move)
                    new_skill = skill(name=_("Ninjutsu I"))
                    self.game.learn_skill(new_skill)

                    self.game.generate_dialogue_sequence(
                        None,
                        "Yagyu Clan BG.png",
                        [_("Ahhh... A glorious victory! This would not be possible if it were not for you, my friend!"),
                         _("You will be rewarded heavily for your contribution!"),
                         _("*Yagyu Tadashi teaches you 'Hachiman Blade'.*"),
                         _("I, Hattori Hasegawa, am truly impressed by your loyalty and strength!"),
                         _("Allow me to show you the skills of a true ninja!"),
                         _("*Hattori Hasegawa teaches you 'Ninjutsu I'.*"),
                         _("Thank you for your generosity and trust! I will forever be loyal to the Yagyu Clan!")
                         ],
                        ["Yagyu Tadashi", "Yagyu Tadashi", "Blank", "Hattori Hasegawa", "Hattori Hasegawa",
                         "Blank", "you"],
                    )

                self.__init__(self.game)


            elif stage == 21: #claim Supreme Leader yourself!
                self.game.chivalry -= 80
                self.game.generate_dialogue_sequence(
                    None,
                    "Yagyu Clan BG.png",
                    [_("Your chivalry -80."),
                     _("What are you talking about, {}-san? This is no time to joke.").format(self.game.character),
                     _("Well, yeah, I'm not joking. I should be the one to claim the title of Supreme Leader of Wulin!"),
                     _("After all, I did all the hard work of beating everyone..."),
                     _("This young man does make a valid point..."),
                     _("Yeah, he seems to be the most powerful person here as far as I can tell."),
                     _("{}-san, have you forgotten the purpose of our collaboration? It was to bring honor to--").format(self.game.character),
                     _("The Yagyu Clan, yeah, I know..."), 
                     _("Then why--"), #Row 2
                     _("Because I deserve it, that's why... If you've got a problem, then let your fists do the talking."),
                     _("You... you treacherous villain! How could you betray my trust?!"),
                     _("Blah blah blah... You're not gonna talk me out of this, so don't even bother..."),
                     _("Very well. I have many witnesses here. It is you who challenged me. You are asking for this."),
                     _("Yes, I am. It's quite simple. If you beat me, then no one else can stand in your way of claiming the title of Supreme Leader."),
                     _("You are a fool who does not know the immensity of heaven and earth..."),
                     _("Today, only one of us lives. Yamamoto, Ashikaga..."),#Row 3
                     _("We will show you the consequences of betraying an alliance.")
                     ],
                    ["Blank", "Yagyu Tadashi", "you", "you", "Tan Keyong", "Ouyang Xiong", "Yagyu Tadashi", "you",
                     "Yagyu Tadashi", "you", "Yagyu Tadashi", "you", "Yagyu Tadashi", "you", "Yagyu Tadashi",
                     "Yagyu Tadashi", "Ashikaga & Yamamoto"],
                )

                cmd = lambda: self.post_yagyu_tournament(22)
                opp_yamamoto_ashikaga = character(_("Ashikaga & Yamamoto"), 22,
                                                    sml=[special_move(_("Kendo"), level=10),
                                                         special_move(_("Hachiman Blade"), level=8)],
                                                    skills=[skill(_("Bushido"), level=10),
                                                            skill(_("Kenjutsu I"), level=10),
                                                            skill(_("Kenjutsu II"), level=7)],
                                                    multiple=2)
                self.game.battleMenu(self.game.you, opp_yamamoto_ashikaga, "Yagyu Clan BG.png",
                                     "txdy_yagyu.mp3", fromWin=None, battleType="task",
                                     destinationWinList=[None] * 3,
                                     postBattleCmd=cmd)


            elif stage == 22: #defeated ashikaga & yamamoto

                cmd = lambda: self.post_yagyu_tournament(23)
                self.game.generate_dialogue_sequence(
                    None,
                    "Yagyu Clan BG.png",
                    [_("*With 2 successive blows to the back of the head, you end Ashikaga and Yamamoto's lives.*"),
                     _("No!!! You... AHHHHHHhHH!!!"),
                     _("Yagyu-san, I will help you!")],
                    ["Blank", "Yagyu Tadashi", "Hattori Hasegawa"],
                )

                opp_hattori = character(_("Hattori Hasegawa"), 26 + self.game.ptc,
                                sml=[special_move(_("Hachiman Blade"), level=10),
                                     special_move(_("Kusarigama"), level=10),
                                     special_move(_("Superior Smoke Bomb"), level=10),
                                     special_move(_("Hensojutsu"), level=10), ],
                                skills=[skill(_("Bushido"), level=10),
                                        skill(_("Kenjutsu I"), level=10),
                                        skill(_("Ninjutsu I"), level=10),
                                        skill(_("Ninjutsu II"), level=10),
                                        skill(_("Ninjutsu III"), level=10)])

                opp_yagyu = character(_("Yagyu Tadashi"), 26 + self.game.ptc,
                                sml=[special_move(_("Kendo"), level=10),
                                     special_move(_("Hachiman Blade"), level=10)],
                                skills=[skill(_("Bushido"), level=10),
                                        skill(_("Kenjutsu I"), level=10),
                                        skill(_("Kenjutsu II"), level=10),
                                        skill(_("Ninjutsu I"), level=10)],
                                preferences=[1, 3, 3, 1, 1])

                self.game.battleMenu(self.game.you, [opp_yagyu, opp_hattori], "Yagyu Clan BG.png",
                                     "txdy_yagyu.mp3", fromWin=None, battleType="task",
                                     destinationWinList=[None] * 3,
                                     postBattleCmd=cmd)


            elif stage == 23:  # become Supreme Leader after wiping out Yagyu Clan
                self.game.taskProgressDic["yagyu_clan"] = 222222222
                self.game.taskProgressDic["supreme_leader"] = 1
                self.game.generate_dialogue_sequence(
                    None,
                    "Yagyu Clan BG.png",
                    [_("In-incredible... Truly incredible... Even my ninjutsu skills are no match for you... *Spits blood*"),
                     _("*After a desperate gasp for air, Hattori Hasegawa breathed his last.*"),
                     _("You... traitorous... disgraceful..."),
                     _("*Without even looking at Yagyu Tadashi's writhing body on the ground, you stamp your foot on his neck.*"),
                     _("*The resounding crack gives you assurance that he is no more.*"),
                     _("Now... Anyone else have a problem with me being Supreme Leader?"),
                     _("You've annihilated a true threat to the Martial Arts Community, and your martial prowess is unmatched."),
                     _("Truly, no one else is more deserving of such a title."), #Row 2
                     _("We have no right to object either. We only hope that you will use your new position for the good of Wulin."),
                     _("Likewise, we pray that you will be a chivalrous leader."),
                     _("Very good! In that case, you all may head back to your respective homes. Expect me to be visiting you soon."),
                     _("Oh, and Shaolin monks, you guys can take your Abbot back. I have no interest in keeping him."),
                     _("We will let go of the past, but do not expect to regain our trust quickly."),
                     _("*After all the sects have departed, you conduct a thorough search of Yagyu Clan.*")],
                    ["Hattori Hasegawa", "Blank", "Yagyu Tadashi", "Blank", "Blank", "you", "Ouyang Xiong",
                     "Ouyang Xiong", "Zhang Tianjian", "Tan Keyong", "you", "you", "Elder Xuanjing", "Blank", "Blank"],
                )

                self.game.add_item(_("Gold"), 50000)
                self.game.add_item(_("Smoke Bomb"), 1000)
                self.game.add_item(_("Ninja Suit"), 20)
                self.game.generate_dialogue_sequence(
                    None,
                    "Yagyu Clan Inner Court BG.png",
                    [_("*You find the following items: Smoke Bomb x 1000, Gold x 50000, Ninja Suit x 20*"),
                     _("*You also find 'The Art of Ninjutsu: Volume II' and 'Ninjutsu Mastery: Volumes I, II, & III'.*"),
                     _("*After careful studying, you learn the skills: 'Ninjutsu I', 'Ninjutsu II', and 'Ninjutsu III'.*")],
                    ["Blank", "Blank", "Blank"]
                )
                for ns in [_("Ninjutsu I"), _("Ninjutsu II"), _("Ninjutsu III")]:
                    new_skill = skill(ns)
                    self.game.learn_skill(new_skill)

                self.__init__(self.game)